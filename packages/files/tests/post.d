/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.mapfiles.post;

import std.file;
import std.datetime;

import trial.interfaces;
import trial.discovery.spec;

import fluent.asserts;


import vibeauth.data.token;
import vibe.http.router;

import ogm.files.api;
import ogm.crates.all;
import ogm.files.configuration;
import vibe.service.configuration.general;

import crate.base;
import crate.collection.memory;
import crate.auth.usercollection;
import crate.http.router;
import tests.fixtures;

import gis_collective.hmq.broadcast.memory;

alias s = Spec!({
  MemoryBroadcast broadcast;
  URLRouter router;
  Json data;

  describe("Add map files", {
    beforeEach({
      data = `{
        "name": "test.csv",
        "contentType": "plain/text",
        "data": "aGVsbG8K",
        "chunk": 0,
        "size": 1000
      }`.parseJsonString;

      setupTestData();

      router = new URLRouter;
      FilesConfiguration configuration;
      configuration.baseUrl = "http://localhost";

      broadcast = new MemoryBroadcast();
      router.crateSetup.setupFileApi(crates, configuration, broadcast);
    });

    it("fails when the file id is not found", {
      auto beforeModels = crates.mapFile.get.size;

      data["_id"] = "1";

      request(router)
        .post("/maps/000000000000000000000001/files")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectHeader("Access-Control-Allow-Origin", "*")
          .expectHeader("Access-Control-Allow-Methods", "OPTIONS, POST")
          .expectStatusCode(404)
          .send(data)
          .end((Response response) => () {
            response.bodyString.should.equal("{\"errors\":[{\"description\":\"There is no item with id `1`\",\"status\":404,\"title\":\"Crate not found\"}]}");
            crates.mapFile.get.size.should.equal(beforeModels);
          });
    });

    describe("when a file has a chunk", {
      string id;

      beforeEach({
        request(router)
        .post("/maps/000000000000000000000001/files")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .send(data)
          .end((Response response) => () {
            id = response.bodyJson["file"].to!string;
          });
      });

      it("can add another chunk", {
        auto beforeModels = crates.mapFile.get.size;
        data["_id"] = id;

        request(router)
          .post("/maps/000000000000000000000001/files")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectHeader("Access-Control-Allow-Origin", "*")
          .expectHeader("Access-Control-Allow-Methods", "OPTIONS, POST")
          .expectStatusCode(200)
          .send(data)
          .end((Response response) => () {
            response.bodyString.should.equal(`{ "file": "000000000000000000000001" }`);
            crates.mapFile.get.size.should.equal(beforeModels);

            auto mapChunks = cast(MockGridFsChunks) crates.mapChunks;
            mapChunks.insertedData.toJson["n"].to!size_t.should.equal(0);
            mapChunks.insertedData.toJson["data"].to!string.should.equal("aGVsbG8K");
          });
      });
    });

    describeCredentialsRule("add new files to joined maps", "yes", "map files", ["administrator", "owner", "leader", "member"], (string type) {
      auto beforeModels = crates.mapFile.get.size;

      request(router)
        .post("/maps/000000000000000000000001/files")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectHeader("Access-Control-Allow-Origin", "*")
          .expectHeader("Access-Control-Allow-Methods", "OPTIONS, POST")
          .expectStatusCode(200)
          .send(data)
          .end((Response response) => () {
            response.bodyString.should.equal(`{ "file": "000000000000000000000001" }`);
            crates.mapFile.get.size.should.equal(beforeModels + 1);

            auto mapChunks = cast(MockGridFsChunks) crates.mapChunks;
            mapChunks.insertedData.toJson["n"].to!size_t.should.equal(0);
            mapChunks.insertedData.toJson["data"].to!string.should.equal("aGVsbG8K");
          });
    });

    describeCredentialsRule("add new files to joined maps", "no", "map files", "guest", (string type) {
      auto beforeModels = crates.mapFile.get.size;

      request(router)
        .post("/maps/000000000000000000000001/files")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectHeader("Access-Control-Allow-Origin", "*")
          .expectHeader("Access-Control-Allow-Methods", "OPTIONS, POST")
          .expectStatusCode(401)
          .send(data)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"errors":[
              {"description":"You can't upload files for this map.","status":401,"title":"Unauthorized"}]}`.parseJsonString);
            crates.mapFile.get.size.should.equal(beforeModels);
          });
    });

    describeCredentialsRule("add new files to joined maps", "-", "map files", "no rights", (string type) {
      auto beforeModels = crates.mapFile.get.size;

      request(router)
        .post("/maps/000000000000000000000001/files")
          .expectHeader("Access-Control-Allow-Origin", "*")
          .expectHeader("Access-Control-Allow-Methods", "OPTIONS, POST")
          .expectStatusCode(401)
          .send(data)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{ "error": "Authorization required" }`.parseJsonString);
            crates.mapFile.get.size.should.equal(beforeModels);
          });
    });

    describeCredentialsRule("add new files to other maps", "no", "map files", ["owner", "leader", "member", "guest"], (string type) {
      auto beforeModels = crates.mapFile.get.size;

      request(router)
        .post("/maps/000000000000000000000004/files")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectHeader("Access-Control-Allow-Origin", "*")
          .expectHeader("Access-Control-Allow-Methods", "OPTIONS, POST")
          .expectStatusCode(401)
          .send(data)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"errors":[
              {"description":"You can't upload files for this map.","status":401,"title":"Unauthorized"}]}`.parseJsonString);
            crates.mapFile.get.size.should.equal(beforeModels);
          });
    });

    describeCredentialsRule("add new files to other maps", "yes", "map files", "administrator", (string type) {
      auto beforeModels = crates.mapFile.get.size;

      request(router)
        .post("/maps/000000000000000000000004/files")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectHeader("Access-Control-Allow-Origin", "*")
          .expectHeader("Access-Control-Allow-Methods", "OPTIONS, POST")
          .expectStatusCode(200)
          .send(data)
          .end((Response response) => () {
            response.bodyString.should.equal(`{ "file": "000000000000000000000001" }`);
            crates.mapFile.get.size.should.equal(beforeModels + 1);
          });
    });

    describeCredentialsRule("add new files to other maps", "no", "map files", "no rights", (string type) {
      auto beforeModels = crates.mapFile.get.size;

      request(router)
        .post("/maps/000000000000000000000004/files")
          .expectHeader("Access-Control-Allow-Origin", "*")
          .expectHeader("Access-Control-Allow-Methods", "OPTIONS, POST")
          .expectStatusCode(401)
          .send(data)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{ "error": "Authorization required" }`.parseJsonString);
            crates.mapFile.get.size.should.equal(beforeModels);
          });
    });
  });
});
