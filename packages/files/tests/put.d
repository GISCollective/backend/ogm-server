/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.mapfiles.put;

import std.file;
import std.datetime;

import trial.interfaces;
import trial.discovery.spec;

import fluent.asserts;

import vibeauth.data.token;
import vibe.http.router;

import ogm.files.api;
import ogm.crates.all;
import ogm.files.configuration;
import vibe.service.configuration.general;

import crate.base;
import crate.collection.memory;
import crate.auth.usercollection;
import crate.http.router;
import tests.fixtures;

import gis_collective.hmq.broadcast.memory;

alias s = Spec!({
  MemoryBroadcast broadcast;
  URLRouter router;
  string data;

  describe("Updating map files", {
    beforeEach({
      setupTestData();

      router = new URLRouter;
      FilesConfiguration configuration;
      configuration.baseUrl = "http://localhost";

      broadcast = new MemoryBroadcast();
      router.crateSetup.setupFileApi(crates, configuration, broadcast);

      crates.mapFile.addItem(`{
        "map": "000000000000000000000001",
        "file": "99",
        "name": "United Nations District Map.csv"
      }`.parseJsonString);

      crates.mapFile.addItem(`{
        "map": "000000000000000000000004",
        "file": "99",
        "name": "United Nations District Map.csv"
      }`.parseJsonString);
    });

    describeCredentialsRule("update options for files of joined maps", "yes", "map files", ["administrator", "owner", "leader", "member"], (string type) {
      auto beforeModels = crates.mapFile.get.size;

      Json data = `{"mapFile":{
        "file":"http://localhost/mapfiles/000000000000000000000001/file",
        "name":"other.csv",
        "options":{
          "fields":[
            { "key": "name", "preview": [], "destination": "" },
            { "key": "longitude", "preview": [], "destination": "position.lon" },
            { "key": "latitude", "preview": [], "destination": "position.lat" },
            { "key": "size", "preview": [], "destination": "attributes.test" },
            { "key": "color", "preview": [], "destination": "attributes.color" }
          ]
        },
        "map":"000000000000000000000001"}}`.parseJsonString;

      request(router)
        .put("/mapfiles/000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .send(data)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"mapFile":{
              "_id": "000000000000000000000001",
              "file":"http://localhost/mapfiles/000000000000000000000001/file",
              "name":"unknown",
              "map":"000000000000000000000001",
              "size": 0,
              "canEdit": true,
              "options":{
                "analyzedAt": "0001-01-01T00:00:00+00:00",
                "extraIcons": [],
                "uuid": "",
                "updateBy": "",
                "crs": "",
                "destinationMap": "",
                "fields":[
                  { "key": "name", "preview": [], "destination": "" },
                  { "key": "longitude", "preview": [], "destination": "position.lon" },
                  { "key": "latitude", "preview": [], "destination": "position.lat" },
                  { "key": "size", "preview": [], "destination": "attributes.test" },
                  { "key": "color", "preview": [], "destination": "attributes.color" }
                ]
              }}}`.parseJsonString);
          });
    });

    describeCredentialsRule("update options for files of joined maps", "no", "map files", "guest", (string type) {
      auto beforeModels = crates.mapFile.get.size;

      Json data = `{"mapFile":{
        "file":"http://localhost/mapfiles/000000000000000000000001/file",
        "name":"other.csv",
        "options":{
          "fields":[
            { "key": "name", "preview": [], "destination": "" },
            { "key": "longitude", "preview": [], "destination": "position.lon" },
            { "key": "latitude", "preview": [], "destination": "position.lat" },
            { "key": "size", "preview": [], "destination": "attributes.test" },
            { "key": "color", "preview": [], "destination": "attributes.color" }
          ]
        },
        "map":"000000000000000000000001"}}`.parseJsonString;

      request(router)
        .put("/mapfiles/000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(401)
          .send(data)
          .end((Response response) => () {
            response.bodyJson.should.equal((`{ "errors": [{
                "description": "You can't update files for this map.",
                "status": 401,
                "title": "Unauthorized"
              }]
            }`).parseJsonString);
          });
    });

    describeCredentialsRule("update options for files of joined maps", "-", "map files", "no rights", (string type) {
      auto beforeModels = crates.mapFile.get.size;

      Json data = `{"mapFile":{
        "file":"http://localhost/mapfiles/000000000000000000000001/file",
        "name":"other.csv",
        "options":{
          "fields":[
            { "key": "name", "preview": [], "destination": "" },
            { "key": "longitude", "preview": [], "destination": "position.lon" },
            { "key": "latitude", "preview": [], "destination": "position.lat" },
            { "key": "size", "preview": [], "destination": "attributes.test" },
            { "key": "color", "preview": [], "destination": "attributes.color" }
          ]
        },
        "map":"000000000000000000000001"}}`.parseJsonString;

      request(router)
        .put("/mapfiles/000000000000000000000001")
          .expectStatusCode(401)
          .send(data)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{ "error": "Authorization required" }`.parseJsonString);
          });
    });

    describeCredentialsRule("update options for files of other maps", "no", "map files", ["owner", "leader", "member", "guest"], (string type) {
      auto beforeModels = crates.mapFile.get.size;

      Json data = `{"mapFile":{
        "file":"http://localhost/mapfiles/000000000000000000000002/file",
        "name":"other.csv",
        "options":{
          "fields":[
            { "key": "name", "preview": [], "destination": "" },
            { "key": "longitude", "preview": [], "destination": "position.lon" },
            { "key": "latitude", "preview": [], "destination": "position.lat" },
            { "key": "size", "preview": [], "destination": "attributes.test" },
            { "key": "color", "preview": [], "destination": "attributes.color" }
          ]
        },
        "map":"000000000000000000000002"}}`.parseJsonString;

      request(router)
        .put("/mapfiles/000000000000000000000002")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(401)
          .send(data)
          .end((Response response) => () {
            response.bodyJson.should.equal((`{ "errors": [{
                "description": "You can't update files for this map.",
                "status": 401,
                "title": "Unauthorized"
              }]
            }`).parseJsonString);
          });
    });

    describeCredentialsRule("update options for files of other maps", "yes", "map files", "administrator", (string type) {
      auto beforeModels = crates.mapFile.get.size;

      Json data = `{"mapFile":{
        "file":"http://localhost/mapfiles/000000000000000000000002/file",
        "name":"other.csv",
        "map":"000000000000000000000002",
        "options":{
          "fields":[
            { "key": "name", "preview": [], "destination": "" },
            { "key": "longitude", "preview": [], "destination": "position.lon" },
            { "key": "latitude", "preview": [], "destination": "position.lat" },
            { "key": "size", "preview": [], "destination": "attributes.test" },
            { "key": "color", "preview": [], "destination": "attributes.color" }
          ]
        }
      }}`.parseJsonString;

      request(router)
        .put("/mapfiles/000000000000000000000002")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .send(data)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"mapFile":{
              "_id": "000000000000000000000002",
              "file":"http://localhost/mapfiles/000000000000000000000002/file",
              "name":"unknown",
              "map":"000000000000000000000004",
              "size": 0,
              "canEdit": true,
              "options":{
                "analyzedAt": "0001-01-01T00:00:00+00:00",
                "uuid": "",
                "updateBy": "",
                "crs": "",
                "destinationMap": "",
                "extraIcons": [],
                "fields": [
                  { "key": "name", "preview": [], "destination": "" },
                  { "key": "longitude", "preview": [], "destination": "position.lon" },
                  { "key": "latitude", "preview": [], "destination": "position.lat" },
                  { "key": "size", "preview": [], "destination": "attributes.test" },
                  { "key": "color", "preview": [], "destination": "attributes.color" },
                ]
              }}}`.parseJsonString);
          });
    });

    describeCredentialsRule("update options for files of other maps", "no", "map files", "no rights", (string type) {
      auto beforeModels = crates.mapFile.get.size;

      request(router)
        .post("/maps/000000000000000000000004/files")
          .header("Content-Type", "multipart/form-data; boundary=---------------------------1679210007493422029835825155")
          .expectHeader("Access-Control-Allow-Origin", "*")
          .expectHeader("Access-Control-Allow-Methods", "OPTIONS, POST")
          .expectStatusCode(401)
          .send(data)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{ "error": "Authorization required" }`.parseJsonString);
            crates.mapFile.get.size.should.equal(beforeModels);
          });
    });
  });
});
