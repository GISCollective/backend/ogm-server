/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.mapfiles.get.anonymous;

import std.path;
import std.file;
import std.digest.md;
import std.datetime;


import vibeauth.data.token;
import vibe.http.router;
import vibe.data.json;

import ogm.files.configuration;
import ogm.files.api;
import ogm.crates.all;

import crate.auth.usercollection;
import crate.base;
import crate.collection.memory;
import crate.resource.file;
import tests.fixtures;

import gis_collective.hmq.broadcast.memory;

Token bearerToken;

void createUserData(UserCrateCollection userCollection) {
  UserModel user;
  user.firstName = "John";
  user.lastName = "Doe";
  user.username = "test";
  user.email = "user@gmail.com";
  user._id = "1";

  userCollection.createUser(user, "password");
  bearerToken = userCollection.createToken("user@gmail.com", Clock.currTime + 3600.seconds, ["doStuff"], "Bearer");
}

alias s = Spec!({
  URLRouter router;
  MemoryBroadcast broadcast;

  describe("when there is no token", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      FilesConfiguration configuration;
      configuration.baseUrl = "http://localhost";

      broadcast = new MemoryBroadcast();
      router.crateSetup.setupFileApi(crates, configuration, broadcast);

      crates.mapFile.addItem(`{
        "map": "000000000000000000000001",
        "file": "99"
      }`.parseJsonString);
    });

    it("should not be able to download the file", {
      router
        .request
        .get("/mapfiles/000000000000000000000001/file")
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
        });
    });

    describe("and the metadata is requested", {
      it("should return an error", {
        router
          .request
          .get("/mapfiles/000000000000000000000001/meta")
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
          });
      });
    });
  });
});
