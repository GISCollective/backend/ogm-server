/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.mapfiles.get.admin;

import std.path;
import std.file;
import std.digest.md;
import std.datetime;

import vibeauth.data.token;
import vibe.http.router;
import vibe.data.json;

import ogm.files.configuration;
import ogm.files.api;
import ogm.crates.all;

import crate.auth.usercollection;
import crate.base;
import crate.collection.memory;
import crate.resource.file;
import tests.fixtures;

import gis_collective.hmq.broadcast.memory;

import fluent.asserts;

alias s = Spec!({
  URLRouter router;
  MemoryBroadcast broadcast;

  describe("with an admin token", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      FilesConfiguration configuration;
      configuration.baseUrl = "http://localhost";

      broadcast = new MemoryBroadcast();
      router.crateSetup.setupFileApi(crates, configuration, broadcast);

      crates.mapFile.addItem(`{
        "map": "000000000000000000000001",
        "file": "99"
      }`.parseJsonString);
    });

    it("should be able to download the file", {
      router
        .request
        .get("/mapfiles/000000000000000000000001/file")
        .header("Authorization", "Bearer " ~ administratorToken.name)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyString.should.equal(``);
        });
    });

    describe("and the metadata is requested", {
      it("should return the meta", {
        router
          .request
          .get("/mapfiles/000000000000000000000001/meta")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{}`.parseJsonString);
          });
      });
    });

    describe("and the analyze action is triggered", {
      Json message;

      beforeEach({
        void testHandler(const Json value) @trusted {
          message = value;
          auto item = crates.mapFile.getItem("000000000000000000000001").and.exec.front;

          item["options"]["fields"].length.should.equal(0);
          item["options"]["fields"] = `[{}]`.parseJsonString;
          crates.mapFile.updateItem(item);
        }

        broadcast.register("mapFile.analyze", &testHandler);
      });

      it("should trigger a message when is called", {
        auto item = crates.mapFile.getItem("000000000000000000000001").and.exec.front;
        item["options"] = `{"fields": [{}] }`.parseJsonString;
        crates.mapFile.updateItem(item);

        router
          .request
          .get("/mapfiles/000000000000000000000001/analyze")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            message.should.equal(`{
              "uid": "000000000000000000000001",
              "id": "000000000000000000000001"
            }`.parseJsonString);
            response.bodyJson.should.equal(`{}`.parseJsonString);
          });
      });
    });

    describe("and the analyze action timed out", {
      Json message;

      beforeEach({
        void testHandler(const Json value) @trusted {
          message = value;
          auto item = crates.mapFile.getItem("000000000000000000000001").and.exec.front;
          item["options"]["analyzedAt"] = "2050-04-12T22:34:03.424Z";

          crates.mapFile.updateItem(item);
        }

        broadcast.register("mapFile.analyze", &testHandler);
      });

      it("should trigger a message when is called and retirn an empty body", {
        auto item = crates.mapFile.getItem("000000000000000000000001").and.exec.front;
        item["options"] = `{"fields": [{}] }`.parseJsonString;
        crates.mapFile.updateItem(item);

        router
          .request
          .get("/mapfiles/000000000000000000000001/analyze")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            message.should.equal(`{
              "uid": "000000000000000000000001",
              "id": "000000000000000000000001"
            }`.parseJsonString);
            response.bodyJson.should.equal(`{}`.parseJsonString);
          });
      });
    });

    describe("and the preview action is triggered", {
      Json message;
      Json metaItem;

      beforeEach({
        metaItem = `{
          "type": "mapFile.preview",
          "itemId": "000000000000000000000001",
          "data": {}
        }`.parseJsonString;

        void testHandler(const Json value) @trusted {
          message = value;

          crates.meta.get.size.should.equal(0);
          crates.meta.addItem(metaItem);
        }

        broadcast.register("mapFile.preview", &testHandler);
      });

      it("should trigger a message when is called", {
        crates.meta.addItem(metaItem);

        router
          .request
          .get("/mapfiles/000000000000000000000001/preview")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            message.should.equal(`{
              "uid": "000000000000000000000001",
              "id": "000000000000000000000001"
            }`.parseJsonString);
            response.bodyJson.should.equal(`{}`.parseJsonString);
          });
      });
    });
  });
});
