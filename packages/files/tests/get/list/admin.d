/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.mapfiles.get.list.admin;

import std.path;
import std.file;
import std.digest.md;
import std.datetime;


import vibeauth.data.token;
import vibe.http.router;
import vibe.data.json;

import ogm.files.configuration;
import ogm.files.api;
import ogm.crates.all;

import crate.auth.usercollection;
import crate.base;
import crate.collection.memory;
import crate.resource.file;
import tests.fixtures;

import gis_collective.hmq.broadcast.memory;

Token bearerToken;

void createUserData(UserCrateCollection userCollection) {
  UserModel user;
  user.firstName = "John";
  user.lastName = "Doe";
  user.username = "test";
  user.email = "user@gmail.com";
  user._id = "1";

  userCollection.createUser(user, "password");
  bearerToken = userCollection.createToken("user@gmail.com", Clock.currTime + 3600.seconds, ["doStuff"], "Bearer");
}

alias s = Spec!({
  URLRouter router;
  MemoryBroadcast broadcast;

  describe("when there is an admin token", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      FilesConfiguration configuration;
      configuration.baseUrl = "http://localhost";

      broadcast = new MemoryBroadcast();
      router.crateSetup.setupFileApi(crates, configuration, broadcast);

      crates.mapFile.addItem(`{
        "map": "000000000000000000000001",
        "file": "99"
      }`.parseJsonString);
    });

    it("should return sites from all maps", {
      router
        .request
        .get("/mapfiles")
        .header("Authorization", "Bearer " ~ administratorToken.name)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"mapFiles":[{
            "_id": "000000000000000000000001",
            "size": 0,
            "file": "http://localhost/mapfiles/000000000000000000000001/file",
            "map": "000000000000000000000001",
            "name": "unknown",
            "canEdit": true
          }]}`.parseJsonString);
        });
    });

    it("should query items by map id", {
      router
        .request
        .get("/mapfiles?map=000000000000000000000001")
        .header("Authorization", "Bearer " ~ administratorToken.name)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"mapFiles":[{
            "_id": "000000000000000000000001",
            "size": 0,
            "file": "http://localhost/mapfiles/000000000000000000000001/file",
            "map": "000000000000000000000001",
            "name": "unknown",
            "canEdit": true
          }]}`.parseJsonString);
        });
    });

    it("should return an empty list when the map does not exist", {
      router
        .request
        .get("/mapfiles?map=000000000000000000000002")
        .header("Authorization", "Bearer " ~ administratorToken.name)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"mapFiles":[]}`.parseJsonString);
        });
    });

    it("should return a map file by id", {
      router
        .request
        .get("/mapfiles/000000000000000000000001")
        .header("Authorization", "Bearer " ~ administratorToken.name)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"mapFile":{
            "_id": "000000000000000000000001",
            "size": 0,
            "file": "http://localhost/mapfiles/000000000000000000000001/file",
            "map": "000000000000000000000001",
            "name": "unknown",
            "canEdit": true
          }}`.parseJsonString);
        });
    });

    describe("and the metadata is requested", {
      it("should return an empty object if the meta does not exist", {
        router
          .request
          .get("/mapfiles/000000000000000000000001/meta")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{}`.parseJsonString);
          });
      });

      it("should return the data if the meta exists", {
        crates.meta.addItem(`{
          "type": "mapFile.import",
          "model": "MapFile",
          "itemId": "000000000000000000000001",
          "changeIndex": 0,
          "data": {
            "isRunning": true,
            "log": "nothing"
          }}`.parseJsonString);

        router
          .request
          .get("/mapfiles/000000000000000000000001/meta")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{ "isRunning": true, "log": "nothing" }`.parseJsonString);
          });
      });
    });

    describe("and the log is requested", {
      it("should return an error if the batch job does not exist", {
        router
          .request
          .get("/mapfiles/000000000000000000000001/log")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{ "errors": [{
              "description": "You can't get logs for a file without a batch job",
              "status": 401,
              "title": "Unauthorized"
            }]}`.parseJsonString);
          });
      });

      it("should return the data if the meta exists", {
        crates.batchJob.addItem(`{
          "name": "mapFile.import 000000000000000000000001",
          "runHistory": []
        }`.parseJsonString);

        crates.log.addItem(`{
          "timestamp": 9999,
          "reference": "000000000000000000000001",
          "level": "info",
          "message": "the log message"
        }`.parseJsonString);

        router
          .request
          .get("/mapfiles/000000000000000000000001/log")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "id": "000000000000000000000001",
              "log": "[1970-01-01T02:46:39Z][info] the log message"
            }`.parseJsonString);
          });
      });
    });
  });
});
