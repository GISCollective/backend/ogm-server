/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.mapfiles.importFile;

import std.path;
import std.file;
import std.array;
import std.digest.md;

import crate.base;
import vibe.service.stats;
import crate.collection.memory;
import crate.resource.file;

import trial.interfaces;
import trial.discovery.spec;

import fluent.asserts;

import ogm.files.configuration;
import ogm.files.validator;
import ogm.crates.all;
import ogm.test.fixtures;
import ogm.files.api;

import tests.fixtures;

import gis_collective.hmq.broadcast.memory;

import vibe.data.json;

alias s = Spec!({
  describe("map file import", {
    URLRouter router;
    MemoryBroadcast broadcast;
    bool called;
    Json receivedValue;

    beforeEach({
      called = false;
      receivedValue = Json.emptyObject;

      setupTestData();

      router = new URLRouter;
      FilesConfiguration configuration;
      configuration.baseUrl = "http://localhost";

      broadcast = new MemoryBroadcast();
      router.crateSetup.setupFileApi(crates, configuration, broadcast);

      broadcast.register("mapFile.import", (const Json value) @safe {
        receivedValue = value;
        called = true;
      });

      crates.mapFile.addItem(`{
        "map": "000000000000000000000002",
        "file": "99"
      }`.parseJsonString);

      crates.mapFile.addItem(`{
        "map": "000000000000000000000004",
        "file": "99"
      }`.parseJsonString);
    });

    describe("when a batch job does not exist", {

      it("should respond with an error", {
        called.should.equal(false);

        request(router)
          .post("/mapfiles/000000000000000000000002/import")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(401)
            .end((Response response) => () {
              response.bodyJson.should.equal(`{
                "errors": [{
                  "description": "You can't import a file without a batch job",
                  "status": 401,
                  "title": "Unauthorized"
                }
              ]}`.parseJsonString);
            });
      });
    });

    describe("when a batch job is already running", {

      beforeEach({
        BatchJob job;
        job.name = "mapFile.import 000000000000000000000001";
        job.visibility = map1.visibility;

        RunEntry run;
        run.status = BatchJobStatus.running;
        run.ping = Clock.currTime;

        job.runHistory ~= run;

        crates.batchJob.addItem(job.serializeToJson);
      });

      it("should respond with an error", {
        called.should.equal(false);

        request(router)
          .post("/mapfiles/000000000000000000000001/import")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(401)
            .end((Response response) => () {
              response.bodyJson.should.equal(`{
                "errors": [{
                  "description": "The file import is already running",
                  "status": 401,
                  "title": "Unauthorized"
                }
              ]}`.parseJsonString);
            });
      });
    });

    describe("when a batch job is already running and the ping was 1 minute ago", {
      beforeEach({
        BatchJob job;
        job.name = "mapFile.import 000000000000000000000001";
        job.visibility = map1.visibility;

        RunEntry run;
        run.status = BatchJobStatus.running;
        run.ping = Clock.currTime - 2.minutes;

        job.runHistory ~= run;

        crates.batchJob.addItem(job.serializeToJson);

        job.name = "mapFile.import 000000000000000000000002";
        crates.batchJob.addItem(job.serializeToJson);
      });

      it("should trigger the job", {
        called.should.equal(false);

        request(router)
          .post("/mapfiles/000000000000000000000001/import")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(202)
            .end((Response response) => () {
              called.should.equal(true);
              receivedValue.should.equal((`{
                "id": "000000000000000000000001",
                "userId": "` ~ userId["administrator"] ~ `"
              }`).parseJsonString);
            });
      });

      describeCredentialsRule("trigger cancel import tasks for joined maps", "yes", "map files", ["administrator", "owner", "leader", "member"], (string type) {
        auto beforeModels = crates.mapFile.get.size;

        request(router)
          .post("/mapfiles/000000000000000000000001/cancelImport")
            .header("Authorization", "Bearer " ~ userTokenList[type])
            .expectStatusCode(202)
            .end((Response response) => () {
              response.bodyString.should.equal(`{"message":"The import job is cancelled."}`);

              crates.batchJob.getItem("000000000000000000000001").exec.front["runHistory"][0]["status"].to!string
                .should.equal("cancelled");
            });
      });

      describeCredentialsRule("trigger cancel import tasks for joined maps", "no", "map files", "guest", (string type) {
        auto beforeModels = crates.mapFile.get.size;

        request(router)
          .post("/mapfiles/000000000000000000000001/cancelImport")
            .header("Authorization", "Bearer " ~ userTokenList[type])
            .expectStatusCode(401)
            .end((Response response) => () {
              called.should.equal(false);
              response.bodyJson.should.equal(`{ "errors": [ {
                "description": "You can't cancel import files for this map.",
                "status": 401,
                "title": "Unauthorized"
              }]}`.parseJsonString);
            });
      });

      describeCredentialsRule("trigger cancel import tasks for joined maps", "-", "map files", "no rights", (string type) {
        auto beforeModels = crates.mapFile.get.size;

        request(router)
          .post("/mapfiles/000000000000000000000001/cancelImport")
            .expectStatusCode(401)
            .end((Response response) => () {
              called.should.equal(false);
              response.bodyJson.should.equal(`{ "error": "Authorization required" }`.parseJsonString);
            });
      });

      describeCredentialsRule("trigger cancel import tasks for other maps", "no", "map files", ["owner", "leader", "member", "guest"], (string type) {
        auto beforeModels = crates.mapFile.get.size;

        request(router)
          .post("/mapfiles/000000000000000000000002/cancelImport")
            .header("Authorization", "Bearer " ~ userTokenList[type])
            .expectStatusCode(401)
            .end((Response response) => () {
              called.should.equal(false);
              response.bodyJson.should.equal((`{ "errors": [{
                  "description": "You can't cancel import files for this map.",
                  "status": 401,
                  "title": "Unauthorized"
                }]
              }`).parseJsonString);
            });
      });

      describeCredentialsRule("trigger cancel import tasks for other maps", "yes", "map files", "administrator", (string type) {
        auto beforeModels = crates.mapFile.get.size;

        request(router)
          .post("/mapfiles/000000000000000000000002/cancelImport")
            .header("Authorization", "Bearer " ~ userTokenList[type])
            .expectStatusCode(202)
            .end((Response response) => () {
              response.bodyString.should.equal(`{"message":"The import job is cancelled."}`);

              crates.batchJob.getItem("000000000000000000000002").exec.front["runHistory"][0]["status"].to!string
                .should.equal("cancelled");
            });
      });

      describeCredentialsRule("trigger cancel import tasks for other maps", "no", "map files", "no rights", (string type) {
        auto beforeModels = crates.mapFile.get.size;

        request(router)
          .post("/mapfiles/000000000000000000000002/cancelImport")
            .expectStatusCode(401)
            .end((Response response) => () {
              called.should.equal(false);
              response.bodyJson.should.equal(`{ "error": "Authorization required" }`.parseJsonString);
            });
      });
    });

    describe("when a batch job is scheduled", {
      beforeEach({
        BatchJob job;
        job.name = "mapFile.import 000000000000000000000001";
        job.visibility = map1.visibility;

        RunEntry run;
        run.status = BatchJobStatus.scheduled;
        run.ping = Clock.currTime - 2.minutes;

        job.runHistory ~= run;

        crates.batchJob.addItem(job.serializeToJson);
      });

      it("should cancel import tasks", {
        auto beforeModels = crates.mapFile.get.size;

        request(router)
          .post("/mapfiles/000000000000000000000001/cancelImport")
            .header("Authorization", "Bearer " ~ userTokenList["administrator"])
            .expectStatusCode(202)
            .end((Response response) => () {
              response.bodyString.should.equal(`{"message":"The import job is cancelled."}`);

              crates.batchJob.getItem("000000000000000000000001").exec.front["runHistory"][0]["status"].to!string
                .should.equal("cancelled");
            });
      });
    });

    describe("when a batch job is preparing", {
      beforeEach({
        BatchJob job;
        job.name = "mapFile.import 000000000000000000000001";
        job.visibility = map1.visibility;

        RunEntry run;
        run.status = BatchJobStatus.preparing;
        run.ping = Clock.currTime - 2.minutes;

        job.runHistory ~= run;

        crates.batchJob.addItem(job.serializeToJson);
      });

      it("should cancel import tasks", {
        auto beforeModels = crates.mapFile.get.size;

        request(router)
          .post("/mapfiles/000000000000000000000001/cancelImport")
            .header("Authorization", "Bearer " ~ userTokenList["administrator"])
            .expectStatusCode(202)
            .end((Response response) => () {
              response.bodyString.should.equal(`{"message":"The import job is cancelled."}`);

              crates.batchJob.getItem("000000000000000000000001").exec.front["runHistory"][0]["status"].to!string
                .should.equal("cancelled");
            });
      });
    });

    describe("when a batch job exists", {
      beforeEach({
        BatchJob job;
        job.name = "mapFile.import 000000000000000000000001";
        job.visibility = map1.visibility;

        RunEntry run;
        run.status = BatchJobStatus.scheduled;

        job.runHistory ~= run;

        crates.batchJob.addItem(job.serializeToJson);

        job.name = "mapFile.import 000000000000000000000002";
        crates.batchJob.addItem(job.serializeToJson);
      });

      describeCredentialsRule("trigger import tasks for joined maps", "yes", "map files", ["administrator", "owner", "leader", "member"], (string type) {
        auto beforeModels = crates.mapFile.get.size;

        request(router)
          .post("/mapfiles/000000000000000000000001/import")
            .header("Authorization", "Bearer " ~ userTokenList[type])
            .expectStatusCode(202)
            .end((Response response) => () {
              called.should.equal(true);
              receivedValue.should.equal((`{
                "id": "000000000000000000000001",
                "userId": "` ~ userId[type] ~ `"
              }`).parseJsonString);
            });
      });

      describeCredentialsRule("trigger import tasks for joined maps", "no", "map files", "guest", (string type) {
        auto beforeModels = crates.mapFile.get.size;

        request(router)
          .post("/mapfiles/000000000000000000000001/import")
            .header("Authorization", "Bearer " ~ userTokenList[type])
            .expectStatusCode(401)
            .end((Response response) => () {
              called.should.equal(false);
              response.bodyJson.should.equal(`{ "errors": [ {
                "description": "You can't import files for this map.",
                "status": 401,
                "title": "Unauthorized"
              }]}`.parseJsonString);
            });
      });

      describeCredentialsRule("trigger import tasks for joined maps", "-", "map files", "no rights", (string type) {
        auto beforeModels = crates.mapFile.get.size;

        request(router)
          .post("/mapfiles/000000000000000000000001/import")
            .expectStatusCode(401)
            .end((Response response) => () {
              called.should.equal(false);
              response.bodyJson.should.equal(`{ "error": "Authorization required" }`.parseJsonString);
            });
      });

      describeCredentialsRule("trigger import tasks for other maps", "no", "map files", ["owner", "leader", "member", "guest"], (string type) {
        auto beforeModels = crates.mapFile.get.size;

        request(router)
          .post("/mapfiles/000000000000000000000002/import")
            .header("Authorization", "Bearer " ~ userTokenList[type])
            .expectStatusCode(401)
            .end((Response response) => () {
              called.should.equal(false);
              response.bodyJson.should.equal((`{ "errors": [{
                  "description": "You can't import files for this map.",
                  "status": 401,
                  "title": "Unauthorized"
                }]
              }`).parseJsonString);
            });
      });

      describeCredentialsRule("trigger import tasks for other maps", "yes", "map files", "administrator", (string type) {
        auto beforeModels = crates.mapFile.get.size;

        request(router)
          .post("/mapfiles/000000000000000000000002/import")
            .header("Authorization", "Bearer " ~ userTokenList[type])
            .expectStatusCode(202)
            .end((Response response) => () {
              called.should.equal(true);
              receivedValue.should.equal((`{
                "id": "000000000000000000000002",
                "userId": "` ~ userId[type] ~ `"
              }`).parseJsonString);
            });
      });

      describeCredentialsRule("trigger import tasks for other maps", "no", "map files", "no rights", (string type) {
        auto beforeModels = crates.mapFile.get.size;

        request(router)
          .post("/mapfiles/000000000000000000000002/import")
            .expectStatusCode(401)
            .end((Response response) => () {
              called.should.equal(false);
              response.bodyJson.should.equal(`{ "error": "Authorization required" }`.parseJsonString);
            });
      });

      it("should not trigger a task when importing missing files", {
        bool called;
        Json receivedValue;

        broadcast.register("mapFile.import", (const Json value) @safe {
          receivedValue = value;
          called = true;
        });

        request(router)
          .post("/mapfiles/000000000000000000000099/import")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(404)
            .end((Response response) => () {
              called.should.equal(false);
            });
      });
    });
  });
});
