/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.files.import_;

import crate.base;
import crate.error;
import gis_collective.hmq.broadcast.base;

import ogm.crates.all;
import ogm.http.request;

import vibe.http.router;
import vibe.data.json;

import std.exception;
import std.algorithm;
import std.array;
import std.conv;
import std.datetime;

class FileImport {
  private {
    OgmCrates crates;
    IBroadcast broadcast;
    Json scheduledRun;
  }

  this(OgmCrates crates, IBroadcast broadcast) {
    this.crates = crates;
    this.broadcast = broadcast;

    scheduledRun = `{
      "time": "",
      "ping": "",
      "ignored": 0,
      "total": 0,
      "errors": 0,
      "sent": 0,
      "duration": 0,
      "processed": 0,
      "runId": "",
      "status": "scheduled"
    }`.parseJsonString;
  }

  string getMapId(ObjectId id) {
    auto mapFileRange = crates.mapFile.get.where("_id").equal(id).and.exec;

    enforce!CrateNotFoundException(!mapFileRange.empty, "The requested file does not exist.");
    return mapFileRange.front["map"].to!string;
  }

  Json getJob(string id, string errorMessage) {
    string key = "mapFile.import " ~ id;

    auto jobs = crates.batchJob.get.where("name").equal(key).and.exec.array;
    enforce!UnauthorizedException(jobs.length > 0, errorMessage);

    return jobs[0];
  }

  ///
  void handleCancelImport(HTTPServerRequest req, HTTPServerResponse response) {
    scope request = RequestUserData(req);
    auto mapId = getMapId(ObjectId.fromString(request.itemId));

    auto job = getJob(request.itemId, "There is no a batch job to cancel");

    foreach(size_t index, run; job["runHistory"]) {
      if(run["status"] == "running" || run["status"] == "scheduled" || run["status"] == "preparing") {
        job["runHistory"][index]["status"] = "cancelled";
      }
    }

    batchJobCrate.updateItem(job);
    response.writeJsonBody(["message": "The import job is cancelled."], 202);
  }

  ///
  void handleImport(HTTPServerRequest req, HTTPServerResponse response) {
    scope request = RequestUserData(req);

    auto job = getJob(request.itemId, "You can't import a file without a batch job");

    auto len = job["runHistory"].length;
    if(len > 0) {
      auto lastRun = job["runHistory"][len - 1];

      if("ping" in lastRun && lastRun["status"] == "running") {
        auto diff = Clock.currTime - SysTime.fromISOExtString(lastRun["ping"].to!string);

        if(diff > 1.minutes) {
          lastRun["status"] = "timeout";
          batchJobCrate.updateItem(job);
        }
      }

      enforce!UnauthorizedException(lastRun["status"] != "running", "The file import is already running");
    }

    scheduledRun["runId"] = job["_id"] ~ "." ~ len.to!string;
    job["runHistory"] ~= scheduledRun;
    batchJobCrate.updateItem(job);

    auto value = Json.emptyObject;
    value["id"] = req.params["id"];
    value["userId"] = request.userId;

    broadcast.push("mapFile.import", value);
    response.writeJsonBody(["message": "The file was queued successfully for import."], 202);
  }

  string convert(Json logEntry) {
    string time;

    try time = SysTime.fromUnixTime(logEntry["timestamp"].to!long).toUTC.toISOExtString;
    catch(Exception) {
      time = "unknown time";
    }

    return `[` ~ time ~ `][` ~ logEntry["level"].to!string ~ `] ` ~ logEntry["message"].to!string;
  }

  void handleLog(HTTPServerRequest req, HTTPServerResponse response) {
    scope request = RequestUserData(req);
    auto job = getJob(request.itemId, "You can't get logs for a file without a batch job");
    auto value = Json.emptyObject;

    value["id"] = req.params["id"];

    string time = "unknown time";

    value["log"] = crates.log.get
      .where("reference").equal(job["_id"].to!string)
      .and
      .sort("timestamp" , 1)
      .exec.map!(a => this.convert(a))
      .joiner("\n")
      .array
      .serializeToJson;

    response.writeJsonBody(value, 200);
  }
}