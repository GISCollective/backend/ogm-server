/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.files.operations.analyze;

import crate.base;
import crate.http.operations.base;
import crate.http.operations.getItem;

import vibe.core.core;
import vibe.data.json;
import vibe.http.server;

import ogm.http.request;
import ogm.models.meta;
import ogm.models.batchjob;
import ogm.middleware.ResourceMiddleware;
import ogm.crates.all;

import gis_collective.hmq.broadcast.base;
import std.array;
import std.datetime;

class AnalyzeOperation : GetItemApiOperation!DefaultStorage {

  static {
    size_t timeout = 120;
    size_t interval = 1000;
  }

  private {
    IBroadcast broadcast;
    OgmCrates crates;
  }

  this(OgmCrates crates, IBroadcast broadcast) {
    CrateRule rule;
    this.broadcast = broadcast;

    rule.request.path = "/mapfiles/:id/analyze";
    rule.request.method = HTTPMethod.GET;
    this.crates = crates;

    super(crates.mapFile, rule);
  }

  override void handle(DefaultStorage storage) {
    auto item = this.prepareItemOperation!"getItem"(storage);
    auto res = storage.response;

    if(storage.response.headerWritten) {
      return;
    }
    this.applyRule(storage);

    auto value = Json.emptyObject;
    string id = item["_id"].to!string;
    value["id"] = id;
    value["uid"] = id;

    if(item["options"].type == Json.Type.object && item["options"]["fields"].type == Json.Type.array && item["options"]["fields"].length > 0) {
      item["options"]["fields"] = Json.emptyArray;
      crates.mapFile.updateItem(item);
    }

    auto now = Clock.currTime;
    broadcast.push("mapFile.analyze", value);

    int index = 0;
    while(index <= timeout) {
      index++;
      sleep(interval.msecs);

      item = crates.mapFile.getItem(id).and.exec.front;

      if(item["options"].type != Json.Type.object) {
        continue;
      }

      if(item["options"]["analyzedAt"].type == Json.Type.string && SysTime.fromISOExtString(item["options"]["analyzedAt"].to!string) >= now) {
        break;
      }

      if(item["options"]["fields"].type == Json.Type.array && item["options"]["fields"].length > 0) {
        break;
      }
    }

    res.writeJsonBody(Json.emptyObject, 200);
  }
}
