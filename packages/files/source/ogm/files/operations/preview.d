/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.files.operations.preview;

import crate.base;
import crate.http.operations.base;
import crate.http.operations.getItem;

import vibe.core.core;
import vibe.data.json;
import vibe.http.server;

import ogm.http.request;
import ogm.models.meta;
import ogm.models.batchjob;
import ogm.middleware.ResourceMiddleware;
import ogm.crates.all;

import gis_collective.hmq.broadcast.base;
import std.array;
import std.datetime;

class PreviewOperation : GetItemApiOperation!DefaultStorage {

  private {
    static {
      size_t timeout = 200;
      size_t interval = 1000;
    }

    Json previewMissing;
    IBroadcast broadcast;
    OgmCrates crates;
  }

  this(OgmCrates crates, IBroadcast broadcast) {
    CrateRule rule;
    this.broadcast = broadcast;
    this.crates = crates;

    rule.request.path = "/mapfiles/:id/preview";
    rule.request.method = HTTPMethod.GET;
    previewMissing = `{
      "errors": [{
        "title": "Invalid preview.",
        "description": "The file preview can not be generated.",
        "status": 404
      }]
    }`.parseJsonString;

    super(crates.mapFile, rule);
  }

  override void handle(DefaultStorage storage) {
    auto item = this.prepareItemOperation!"getItem"(storage);
    auto res = storage.response;

    if(storage.response.headerWritten) {
      return;
    }
    this.applyRule(storage);

    auto value = Json.emptyObject;
    value["id"] = item["_id"];
    value["uid"] = item["_id"];
    string itemId = item["_id"].to!string;

    auto metaRange = crates.meta.get
        .where("type").equal("mapFile.preview").and
        .where("itemId").equal(itemId).and
        .withProjection(["_id"])
        .limit(1)
        .exec;

    if(!metaRange.empty) {
      crates.meta.deleteItem(metaRange.front["_id"].to!string);
    }

    broadcast.push("mapFile.preview", value);

    int index = 0;
    while(index <= timeout) {
      index++;
      sleep(interval.msecs);

      auto exists = crates.meta.get
        .where("type").equal("mapFile.preview").and
        .where("itemId").equal(itemId).and
        .size;

      if(exists) break;
    }

    sleep(interval.msecs);

    metaRange = crates.meta.get
        .where("type").equal("mapFile.preview").and
        .where("itemId").equal(itemId).and
        .exec;

    if(metaRange.empty) {
      res.writeJsonBody(previewMissing, 404);
      return;
    }

    res.writeJsonBody(metaRange.front["data"], 200);
  }
}
