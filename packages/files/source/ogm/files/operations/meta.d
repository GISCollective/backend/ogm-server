/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.files.operations.meta;

import crate.base;
import crate.http.operations.base;
import crate.http.operations.getItem;

import vibe.data.json;
import vibe.http.server;

import ogm.http.request;
import ogm.models.meta;
import ogm.models.batchjob;
import ogm.middleware.ResourceMiddleware;
import ogm.crates.all;

import std.array;

class MetadataOperation : GetItemApiOperation!DefaultStorage {
  private {
    IResourceMiddleware[] resourceMiddlewares;
  }

  this(OgmCrates crates) {
    this.resourceMiddlewares = resourceMiddlewares;

    CrateRule rule;

    rule.request.path = "/mapfiles/:id/meta";
    rule.request.method = HTTPMethod.GET;

    super(crates.mapFile, rule);
  }

  override void handle(DefaultStorage storage) {
    auto item = this.prepareItemOperation!"getItem"(storage);
    auto res = storage.response;

    if(storage.response.headerWritten) {
      return;
    }
    this.applyRule(storage);

    string _id = item["_id"].to!string;
    string key = "mapFile.import " ~ _id;
    auto jobs = batchJobCrate.get.where("name").equal(key).and.exec.array;

    if(jobs.length > 0) {
      auto len = jobs[0]["runHistory"].length;

      if(len > 0) {
        res.writeJsonBody(jobs[0]["runHistory"][len - 1], 200);
        return;
      }
    }

    auto range = metaCrate.get
      .where("itemId").equal(_id).and
      .where("type").equal("mapFile.import").and
      .exec;

    if(range.empty) {
      res.writeJsonBody(Json.emptyObject, 200);
      return;
    }

    res.writeJsonBody(range.front["data"], 200);
  }
}
