/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.files.filter;

import std.stdio;
import std.exception;
import std.file;
import std.path;
import std.conv;
import std.algorithm;

import crate.collection.memory;
import crate.base;
import crate.error;

import vibe.http.router;
import vibe.data.json;

/// Filter used for map files
class FileFilter {

  struct Parameters {
    @describe("If it's set, only items related to the map will be returned.")
    string map;
  }

  /// Filter that checks if the requested map file exists.
  @get
  IQuery get(IQuery selector, Parameters params, HTTPServerRequest request) {
    if(params.map != "") {
      selector.where("map").equal(ObjectId.fromString(params.map));
    }

    return selector;
  }
}
