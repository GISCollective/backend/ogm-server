/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.files.mapRights;

import vibe.http.router;
import crate.base;
import crate.error;

import std.algorithm;
import std.exception;

import ogm.crates.all;
import ogm.http.request;

class MapRights {

  private {
    OgmCrates crates;
  }

  this(OgmCrates crates) {
    this.crates = crates;
  }


  @patch @replace
  void checkUpdateRights(HTTPServerRequest req, HTTPServerResponse res) {
    checkFileIdRights!"update"(req, res);
  }

  void checkFileIdRights(string action)(HTTPServerRequest req, HTTPServerResponse) {
    scope request = RequestUserData(req);
    auto mapFileRange = crates.mapFile.get.where("_id").equal(ObjectId.fromString(request.itemId)).and.exec;

    enforce!CrateNotFoundException(!mapFileRange.empty, "The requested file does not exist.");

    auto id = mapFileRange.front["map"].to!string;

    checkSession!action(request, id);
  }

  void checkMapIdRights(string action)(HTTPServerRequest req, HTTPServerResponse) {
    auto request = RequestUserData(req);
    string id = request.itemId;

    checkSession!action(request, id);
  }

  void checkSession(string action)(RequestUserData request, string id) {
    if(request.isAdmin) {
      return;
    }

    auto session = request.session(crates);

    immutable hasOwnerMaps = session.owner.maps.map!(a => a == id).canFind(true);
    immutable hasLeaderMaps = session.leader.maps.map!(a => a == id).canFind(true);
    immutable hasMemberMaps = session.member.maps.map!(a => a == id).canFind(true);

    enum message = "You can't " ~ action ~ " files for this map.";
    enforce!UnauthorizedException(hasOwnerMaps || hasLeaderMaps || hasMemberMaps, message);
  }
}