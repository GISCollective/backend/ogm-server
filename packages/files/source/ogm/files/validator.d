/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.files.validator;

import std.stdio;
import std.range;
import std.array;
import std.conv;
import std.algorithm;
import std.exception;
import std.string;

import ogm.models.all;
import ogm.crates.all;

import crate.base;
import crate.validation.csv;
import crate.collection.csv;


class MapCsvValidator {

  private {
    string path;
    OgmCrates crates;
  }

  this(string path, OgmCrates crates) {
    this.path = path;
    this.crates = crates;
  }

  string[] issues() {
    auto file = File(path, "r");
    auto lines = file.byLine;

    Head[string] header;

    if(!lines.empty) {
      header = lines.front.to!string.getHeader;
    }

    string[] result;
    string[] missing;
    enum expectedFields = [ "maps", "name", "description", "position.lon", "position.lat", "icons", "isPublished", "attributes" ];

    foreach(field; expectedFields) {
      if(field !in header) {
        missing ~= "`" ~ field ~ "`";
      }
    }

    if(missing.length > 0) {
      string msg = "Columns " ~ missing.join(", ") ~ " are missing.";
      return [ msg ];
    }

    if(result.length > 0) {
      return result;
    }

    scope lineValidator = new CsvLineValidator(header);
    lineValidator.add("position.lat", new CoordinateValidator());
    lineValidator.add("position.lon", new CoordinateValidator());

    scope iconValidator = new IconsValidator(crates.icon);
    lineValidator.add("icons", iconValidator);
    lineValidator.add("icon", iconValidator);

    auto records = csvLineRange(path);

    result = records
      .enumerate(1UL)
      .map!(a =>
        lineValidator
          .validate(a.value.to!string)
          .map!(b => "on line #" ~ a.index.to!string ~ " " ~ b))
      .joiner
      .array;

    return result;
  }
}

class CoordinateValidator : IValueValidator {

  /// Check if the value is valid and return a list of error messages
  string[] validate(string value) {
    if(isNumeric(value)) {
      return [];
    }

    return [ "`" ~ value ~ "` is not valid" ];
  }
}

class IconsValidator : IValueValidator {

  private {
    string[string] validIcons;
  }

  this(Crate!Icon icons) {
    debug enforce(icons !is null);

    foreach(icon; icons.get.exec) {
      auto name = icon["name"].to!string;
      validIcons[name.toLower] = name;
    }
  }

  /// Check if the value is valid and return a list of error messages
  string[] validate(string value) {
    string[] result;
    auto iconList = value.split(",").map!(a => a.strip.toLower);
    string[] missingIcons;

    foreach(icon; iconList) {
      if(icon !in validIcons) {
        missingIcons ~= icon;
      }
    }

    if(missingIcons.length == 1) {
      string message = "`" ~ missingIcons[0] ~ "` is not valid";
      result ~= message;
    } else if(missingIcons.length > 1) {
      string message = missingIcons.map!(a => "`"~a~"`").join(", ").to!string ~ " are not valid";
      result ~= message;
    }

    return result;
  }
}
