/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.files.api;

import vibe.data.json;

import ogm.http.request;
import ogm.auth;
import ogm.files.filter;
import ogm.files.configuration;
import ogm.files.mapper;
import ogm.files.operations.meta;
import ogm.files.operations.analyze;
import ogm.files.operations.preview;

import crate.http.cors;
import crate.http.router;
import crate.auth.middleware;
import crate.resource.file;
import crate.http.router;
import crate.base;
import crate.error;
import crate.error;

import gis_collective.hmq.broadcast.base;

import vibe.http.router;

import std.conv;
import std.algorithm;
import std.array;
import std.file;
import std.path;
import std.base64;
import std.functional;

import ogm.middleware.userdata;
import ogm.middleware.adminrequest;
import ogm.middleware.modelinfo;
import ogm.files.mapRights;
import ogm.crates.all;
import ogm.files.import_;

import ogm.models.mapFile;

///
class FileUpload {
  private {
    OgmCrates crates;
  }

  this(OgmCrates crates) {
    this.crates = crates;
  }

  /// Upload file handler
  void handle(HTTPServerRequest req, HTTPServerResponse response) {
    scope request = RequestUserData(req);
    scope session = request.session(crates);

    enforce!UnauthorizedException(!session.guest.maps.canFind(request.itemId), "You can't upload files for this map.");

    Json newFile;

    ubyte[] data = Base64.decode(req.json["data"].to!string);
    size_t chunkSize = req.json["size"].to!string.to!size_t;

    enforce!CrateValidationException(chunkSize >= data.length, "The data size does not match the chunk size");

    MapFile mapFile;

    if(req.json["_id"].type == Json.Type.string) {
      auto jsonMapFile = crates.mapFile.getItem(req.json["_id"].to!string).and.exec.front;

      mapFile = LazyMapFile(jsonMapFile, (&itemResolver).toDelegate).toType();
    } else {
      mapFile.file = new MapFileResource;
      mapFile.file.contentType = req.json["contentType"].to!string;
      mapFile.file.fileName = req.json["name"].to!string;
      mapFile.file.chunkSize = chunkSize;
      mapFile.file.update();
    }

    mapFile.file.setChunk(req.json["chunk"].to!size_t, data);

    auto value = LazyMapFile.fromModel(mapFile).toJson();
    value["map"] = request.itemId;


    if(req.json["_id"].type == Json.Type.string) {
      newFile = crates.mapFile.updateItem(value);
    } else {
      newFile = crates.mapFile.addItem(value);
    }

    response.headers["Content-Type"] = "application/json";
    response.writeBody(`{ "file": "` ~ newFile["_id"].to!string ~ `" }`, 200);
  }
}

///
void setupFileApi(T)(CrateRouter!T crateRouter, OgmCrates crates, FilesConfiguration configuration, IBroadcast broadcast) {
  MapFileSettings.baseUrl = configuration.baseUrl;
  MapFileSettings.files = crates.mapFiles;
  MapFileSettings.chunks = crates.mapChunks;

  crateGetters["Map"] = &crates.map.getItem;
  crateGetters["Team"] = &crates.team.getItem;
  crateGetters["Picture"] = &crates.picture.getItem;
  crateGetters["IconSet"] = &crates.iconSet.getItem;

  auto auth = Authentication.instance(crates.user);
  auto adminRequest = new AdminRequest(crates.user);
  auto userDataMiddleware = UserDataMiddleware.instance(crates);

  auto fileMapper = new FileMapper(configuration.baseUrl, crates);

  auto fileFilter = new FileFilter;
  auto fileUpload = new FileUpload(crates);
  auto mapRights = new MapRights(crates);
  auto fileImport = new FileImport(crates, broadcast);

  auto metadataOperation = new MetadataOperation(crates);
  auto analyzeOperation = new AnalyzeOperation(crates, broadcast);
  auto previewOperation = new PreviewOperation(crates, broadcast);

  metaCrate = crates.meta;
  batchJobCrate = crates.batchJob;

  auto corsPostFile = Cors(crateRouter.router, "/maps/:id/files");
  crateRouter.router.post("/maps/:id/files", corsPostFile.add(HTTPMethod.POST, requestErrorHandler(&auth.privateDataMiddleware.mandatory)));
  crateRouter.router.post("/maps/:id/files", requestErrorHandler(&adminRequest.any));
  crateRouter.router.post("/maps/:id/files", requestErrorHandler(&userDataMiddleware.any));
  crateRouter.router.post("/maps/:id/files", requestErrorHandler(&mapRights.checkMapIdRights!"upload"));
  crateRouter.router.post("/maps/:id/files", requestErrorHandler(&fileUpload.handle));

  auto corsPostImportFile = Cors(crateRouter.router, "/mapfiles/:id/log");
  crateRouter.router.post("/mapfiles/:id/import", corsPostImportFile.add(HTTPMethod.POST, requestErrorHandler(&auth.privateDataMiddleware.mandatory)));
  crateRouter.router.post("/mapfiles/:id/import", requestErrorHandler(&adminRequest.any));
  crateRouter.router.post("/mapfiles/:id/import", requestErrorHandler(&userDataMiddleware.any));
  crateRouter.router.post("/mapfiles/:id/import", requestErrorHandler(&mapRights.checkFileIdRights!"import"));

  auto corsPostCancelImportFile = Cors(crateRouter.router, "/mapfiles/:id/cancelImport");
  crateRouter.router.post("/mapfiles/:id/cancelImport", corsPostCancelImportFile.add(HTTPMethod.POST, requestErrorHandler(&auth.privateDataMiddleware.mandatory)));
  crateRouter.router.post("/mapfiles/:id/cancelImport", requestErrorHandler(&adminRequest.any));
  crateRouter.router.post("/mapfiles/:id/cancelImport", requestErrorHandler(&userDataMiddleware.any));
  crateRouter.router.post("/mapfiles/:id/cancelImport", requestErrorHandler(&mapRights.checkFileIdRights!"cancel import"));
  crateRouter.router.post("/mapfiles/:id/cancelImport", requestErrorHandler(&fileImport.handleCancelImport));
  crateRouter.router.post("/mapfiles/:id/import", requestErrorHandler(&fileImport.handleImport));

  auto corsPostLogFile = Cors(crateRouter.router, "/mapfiles/:id/import");
  crateRouter.router.get("/mapfiles/:id/log", corsPostLogFile.add(HTTPMethod.POST, requestErrorHandler(&auth.privateDataMiddleware.mandatory)));
  crateRouter.router.get("/mapfiles/:id/log", requestErrorHandler(&adminRequest.any));
  crateRouter.router.get("/mapfiles/:id/log", requestErrorHandler(&userDataMiddleware.any));
  crateRouter.router.get("/mapfiles/:id/log", requestErrorHandler(&mapRights.checkFileIdRights!"get log for"));
  crateRouter.router.get("/mapfiles/:id/log", requestErrorHandler(&fileImport.handleLog));

  crateRouter.prepare(crates.mapFile)
    .withCustomOperation(metadataOperation)
    .withCustomOperation(analyzeOperation)
    .withCustomOperation(previewOperation)
    .and(auth.privateDataMiddleware)
    .and(adminRequest)
    .and(userDataMiddleware)
    .and(mapRights)
    .and(fileFilter)
    .and(fileMapper);

}
