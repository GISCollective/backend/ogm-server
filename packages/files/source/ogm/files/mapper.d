/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.files.mapper;

import crate.base;
import crate.url;
import crate.http.request;
import vibe.data.json;
import vibe.http.server;

import std.path;
import ogm.crates.all;
import ogm.http.request;
import ogm.models.mapFile;

///
class FileMapper {

  private {
    string baseUrl;
    OgmCrates crates;
  }

  this(string baseUrl, OgmCrates crates) {
    this.baseUrl = baseUrl;
    this.crates = crates;
  }

  @patch @replace
  void fixFields(HTTPServerRequest req) {
    scope request = RequestUserData(req);

    if("mapFile" !in req.json) {
      return;
    }

    scope storedData = crates.mapFile.getItem(request.itemId).exec.front;

    storedData["options"] = req.json["mapFile"]["options"];
    req.json["mapFile"] = storedData;
  }

  @mapper
  Json mapper(HTTPServerRequest req, const Json item) @trusted nothrow {
    Json result = item;

    try {
      auto fileUrl = this.baseUrl.replaceHost(req.host, req.tls);

      auto file = new MapFileResource(item["file"].to!string);
      result["canEdit"] = true;
      result["file"] = fileUrl.buildPath("mapfiles", item["_id"].to!string, "file");
      result["size"] = file.size;
      result["name"] = file.fileName;

      if(result["name"] == "") {
        result["name"] = "unknown";
      }

    } catch(Exception e) {}

    return result;
  }
}
