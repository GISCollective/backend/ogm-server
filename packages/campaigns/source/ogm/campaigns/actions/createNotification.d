/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.campaigns.actions.createNotification;

import ogm.http.request;
import ogm.crates.all;
import std.algorithm;

import vibe.http.router;
import vibe.data.json;
import vibeauth.data.user;

import crate.error;
import crate.base;
import crate.http.operations.base;
import ogm.campaigns.actions.CampaignAction;

class CampaignCreateNotificationRequest: CampaignAction {
  ///
  this(OgmCrates crates) {
    super(crates, "createNotification");
  }

  ///
  override void handle(DefaultStorage storage) {
    super.handle(storage);

    auto res = storage.response;

    if(res.headerWritten) {
      return;
    }

    auto id = storage.properties.itemId;
    auto slug = "notification-campaign-add-" ~ id;
    auto exists = crates.article.get
      .where("slug")
      .equal(slug)
      .and.size > 0;

    enforce!ForbiddenException(!exists, "The notification already exists.");

    auto range = crates.article.get
      .where("slug")
      .equal("notification-campaign-add-default")
      .and.exec;

    enforce!CrateNotFoundException(!range.empty, "There is no template. Create an article with the `notification-campaign-add-default` slug.");

    auto tpl = range.front;
    tpl.remove("_id");
    tpl["slug"] = slug;
    tpl["visibility"] = storage.item["visibility"];
    tpl["visibility"]["isPublic"] = false;
    tpl["visibility"]["isDefault"] = false;

    crates.article.addItem(tpl);

    res.writeBody(``, 201, "text/plain");
  }
}
