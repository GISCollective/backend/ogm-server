/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.campaigns.actions.removeNotification;

import ogm.http.request;
import ogm.crates.all;
import std.algorithm;

import vibe.http.router;
import vibe.data.json;
import vibeauth.data.user;

import crate.error;
import crate.base;
import crate.http.operations.base;
import ogm.campaigns.actions.CampaignAction;

class CampaignRemoveNotificationRequest: CampaignAction {
  ///
  this(OgmCrates crates) {
    super(crates, "removeNotification");
  }

  ///
  override void handle(DefaultStorage storage) {
    super.handle(storage);

    auto res = storage.response;

    if(res.headerWritten) {
      return;
    }

    auto id = storage.properties.itemId;
    auto slug = "notification-campaign-add-" ~ id;
    auto range = crates.article.get
      .where("slug")
      .equal(slug)
      .and.exec;

    enforce!ForbiddenException(!range.empty, "The notification does not exist.");

    crates.article.deleteItem(range.front["_id"].to!string);

    res.writeBody(``, 201, "text/plain");
  }
}
