/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.campaigns.actions.CampaignAction;

import ogm.http.request;
import ogm.crates.all;
import std.algorithm;

import vibe.http.router;
import vibe.data.json;
import vibeauth.data.user;

import crate.error;
import crate.base;
import crate.http.operations.base;

class CampaignAction: CrateOperation!DefaultStorage {
  protected {
    OgmCrates crates;
  }

  ///
  this(OgmCrates crates, string name) {
    this.crates = crates;

    CrateRule rule;
    rule.request.path = "/campaigns/:id/" ~ name;
    rule.request.method = HTTPMethod.POST;

    super(crates.campaign, rule);
  }

  ///
  override void handle(DefaultStorage storage) {
    storage.item = this.prepareItemOperation!"getItem"(storage);

    auto request = RequestUserData(storage.request);
    enforce!UnauthorizedException(request.isAuthenticated, "Authorization required");

    auto session = request.session(crates);

    auto id = storage.properties.itemId;
    enforce!CrateNotFoundException(request.isAdmin || session.all.campaigns.canFind(id), "There is no item with id `" ~ id ~ "`");
    enforce!ForbiddenException(request.isAdmin || session.owner.campaigns.canFind(id), "You don't have enough rights.");
  }
}
