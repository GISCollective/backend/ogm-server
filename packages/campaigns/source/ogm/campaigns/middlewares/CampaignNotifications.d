/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.campaigns.middlewares.CampaignNotifications;

import ogm.relationFix;

import vibe.data.json;
import vibe.http.server;
import std.algorithm;
import crate.base;
import ogm.crates.all;
import ogm.http.request;

class CampaignNotifications {
  OgmCrates crates;

  this(OgmCrates crates) {
    this.crates = crates;
  }

  @mapper
  void mapper(HTTPServerRequest req, ref Json item) {
    auto request = RequestUserData(req);

    auto id = item["_id"].to!string;

    if(!request.isAdmin && !request.session(crates).all.campaigns.canFind(id)) {
      return;
    }

    auto slug = "notification-campaign-add-" ~ id;
    auto range = crates.article.get
      .where("slug")
      .equal(slug)
      .and.exec;

    if(range.empty) {
      return;
    }

    auto article = range.front;

    item["notifications"] = Json.emptyObject;
    item["notifications"]["add"] = article["_id"];
  }
}
