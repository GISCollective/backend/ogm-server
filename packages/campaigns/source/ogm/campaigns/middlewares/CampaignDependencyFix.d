/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.campaigns.middlewares.CampaignDependencyFix;

import ogm.relationFix;

import vibe.data.json;
import crate.base;

class CampaignDependencyFix {

  @mapper
  void mapper(ref Json item) @trusted nothrow {
    fixRelationList!"icons"(item);
    fixRelationList!"optionalIcons"(item);
  }
}
