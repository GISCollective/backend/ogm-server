/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.campaigns.middlewares.CampaignArea;

import ogm.relationFix;

import vibe.data.json;
import vibe.http.server;
import std.algorithm;
import std.array;
import std.datetime;
import crate.base;
import crate.json;
import crate.lazydata.base;
import ogm.crates.all;
import ogm.http.request;

class CampaignArea {
  OgmCrates crates;

  LazyField!Json publicDefaultBaseMaps;

  this(OgmCrates crates) {
    this.crates = crates;

    this.publicDefaultBaseMaps = lazyField({
      return crates.baseMap
        .get
        .where("visibility.isPublic").equal(true).and
        .where("visibility.isDefault").equal(true).and
        .withProjection(["_id"])
        .exec
        .map!(a => a["_id"].to!string)
        .array
        .serializeToJson;
    }, 5.seconds);
  }

  @mapper
  void mapper(HTTPServerRequest req, ref Json item) {
    if(!item.exists("map.isEnabled") || item["map"]["isEnabled"] == false) {
      return;
    }

    auto mapId = item["map"]["map"];

    auto range = this.crates.map.get.where("_id").equal(ObjectId.fromJson(mapId)).and.exec;

    if(range.empty) {
      return;
    }

    auto map = range.front;

    item["area"] = map["area"];
    item["baseMaps"] = map["baseMaps"];

    if("useCustomList" !in item["baseMaps"] || item["baseMaps"]["useCustomList"] == false) {
      item["baseMaps"]["list"] = publicDefaultBaseMaps.compute;
    }
  }
}
