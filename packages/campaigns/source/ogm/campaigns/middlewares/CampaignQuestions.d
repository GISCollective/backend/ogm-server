/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.campaigns.middlewares.CampaignQuestions;

import ogm.relationFix;

import vibe.data.json;
import vibe.http.server;
import std.algorithm;
import crate.base;
import crate.json;
import ogm.crates.all;
import ogm.http.request;
import gis_collective.hmq.log;
import ogm.models.campaign;


enum fields = ["position", "name", "description", "icons", "pictures", "sounds"];
enum string[string] defaultQuestion = [
  "position": "Location",
  "name": "What's the name of this place?",
  "description": "What do you like about this place?",
  "icons": "Please select an icon from the list",
  "pictures": "Pictures",
  "sounds": "Sounds"
];
enum string[string] type = [
  "position": "geo json",
  "name": "short text",
  "description": "long text",
  "icons": "icons",
  "pictures": "pictures",
  "sounds": "sounds"
];

class CampaignQuestions {
  OgmCrates crates;

  this(OgmCrates crates) {
    this.crates = crates;
  }

  @mapper
  void createQuestionsFromOptions(HTTPServerRequest req, ref Json item) {
    if(item["questions"].type == Json.Type.array && item["questions"].length > 0) {
      return;
    }

    item["questions"] = Json.emptyArray;

    foreach(name; fields) {
      auto question = Question(defaultQuestion[name], "", type[name], name);
      item["questions"] ~= question.serializeToJson;
    }

    auto options = item["options"];

    try {
      if(options["location"]["customQuestion"]) {
        item["questions"][0]["question"] = options["location"]["label"];
      }
    } catch(Exception e) {
      error(e);
    }

    try {
      item["questions"][0]["isRequired"] = true;
      item["questions"][0]["isVisible"] = true;
      item["questions"][0]["options"] = `{ "allowGps": true, "allowManual": true, "allowAddress": true, "allowExistingFeature": false }`.parseJsonString;
    } catch(Exception e) {
      error(e);
    }

    try {
      if(options["name"]["customQuestion"]) {
        item["questions"][1]["question"] = options["name"]["label"];
      }
    } catch(Exception e) {
      error(e);
    }

    try {
      item["questions"][1]["isRequired"] = true;
      item["questions"][1]["isVisible"] = options["showNameQuestion"] || options["name"]["customQuestion"];
    } catch(Exception e) {
      error(e);
    }

    try {
      if(options["description"]["customQuestion"]) {
        item["questions"][2]["question"] = options["description"]["label"];
      }
    } catch(Exception e) {
      error(e);
    }

    try {
      item["questions"][2]["isRequired"] = true;
      item["questions"][2]["isVisible"] = options["showDescriptionQuestion"] || options["description"]["customQuestion"];
    } catch(Exception e) {
      error(e);
    }

    try {
      if(options["icons"]["customQuestion"]) {
        item["questions"][3]["question"] = options["icons"]["label"];
      }
    } catch(Exception e) {
      error(e);
    }

    try {
      item["questions"][3]["isRequired"] = false;
      item["questions"][3]["isVisible"] = item["optionalIcons"].length > 0;
      item["questions"][3]["options"] = item["optionalIcons"];
    } catch(Exception e) {
      error(e);
    }

    try {
      if(options["pictures"]["customQuestion"]) {
        item["questions"][4]["question"] = options["pictures"]["label"];
      }
    } catch(Exception e) {
      error(e);
    }

    try {
      item["questions"][4]["isRequired"] = options["pictures"]["isMandatory"];
      item["questions"][4]["isVisible"] = true;
    } catch(Exception e) {
      error(e);
    }
  }
}
