/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.campaigns.api;

import vibe.http.router;
import crate.http.router;
import crate.base;
import crate.error;

import ogm.auth;
import ogm.crates.all;

import ogm.middleware.adminrequest;
import ogm.middleware.userdata;

import ogm.filter.pagination;
import ogm.filter.searchterm;
import ogm.filter.visibility;
import ogm.filter.searchtag;
import ogm.middleware.GlobalAccess;
import ogm.campaigns.middlewares.CampaignDependencyFix;
import ogm.campaigns.middlewares.CampaignNotifications;
import ogm.campaigns.middlewares.CampaignArea;
import ogm.campaigns.actions.createNotification;
import ogm.campaigns.actions.removeNotification;
import ogm.middleware.SortingMiddleware;
import ogm.campaigns.middlewares.CampaignQuestions;

import ogm.middleware.DefaultCover;
import ogm.middleware.modelinfo;
import ogm.middleware.IdsFilter;

///
void setupCampaignApi(T)(CrateRouter!T crateRouter, OgmCrates crates) {
  PictureFileSettings.files = crates.pictureFiles;
  PictureFileSettings.chunks = crates.pictureChunks;

  auto auth = Authentication.instance(crates.user);
  auto adminRequest = new AdminRequest(crates.user);

  auto globalAccess = GlobalAccessMiddleware.instance(crates);
  auto userDataMiddleware = UserDataMiddleware.instance(crates);
  auto searchTermFilter = new SearchTermFilter;
  auto searchTagFilter = new SearchTagFilter;
  auto paginationFilter = new PaginationFilter;
  auto dependencyFix = new CampaignDependencyFix;
  auto visibilityFilter = new VisibilityFilter!"campaigns"(crates, crates.campaign);
  auto defaultCover = new DefaultCoverMiddleware(crates, "campaign");
  auto idsFilter = new IdsFilter(crates);
  auto createNotification = new CampaignCreateNotificationRequest(crates);
  auto removeNotification = new CampaignRemoveNotificationRequest(crates);
  auto campaignNotifications = new CampaignNotifications(crates);
  auto campaignArea = new CampaignArea(crates);
  auto campaignQuestions = new CampaignQuestions(crates);
  auto sorting = new SortingMiddleware(crates, "info.lastChangeOn", -1);

  auto modelInfo = new ModelInfoMiddleware("campaign", crates.campaign);

  crateGetters["Team"] = &crates.team.getItem;
  crateGetters["Picture"] = &crates.picture.getItem;
  crateGetters["IconSet"] = &crates.iconSet.getItem;
  crateGetters["Icon"] = &crates.icon.getItem;
  crateGetters["Map"] = &crates.map.getItem;

  metaCrate = crates.meta;
  batchJobCrate = crates.batchJob;

  auto privateAuth = auth.privateDataMiddleware;
  auto mandatoryAuth = &privateAuth.mandatory;

  auto preparedRoute =
    crateRouter.prepare(crates.campaign)
      .withCustomOperation(createNotification)
      .withCustomOperation(removeNotification)
      .and(auth.publicDataMiddleware)
      .and(adminRequest)
      .and(userDataMiddleware)
      .and(modelInfo)
      .and(globalAccess)
      .and(visibilityFilter)
      .and(campaignNotifications)
      .and(dependencyFix)
      .and(campaignArea)
      .and(campaignQuestions)
      .and(defaultCover)
      .and(idsFilter)
      .and(sorting)
      .and(searchTermFilter)
      .and(searchTagFilter)
      .and(paginationFilter);
}
