/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.campaigns.delete_;

import tests.fixtures;

import std.algorithm;
import std.array;
import ogm.campaigns.api;

alias suite = Spec!({
  URLRouter router;

  describe("DELETE", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupCampaignApi(crates);
    });

    describe("delete campaigns owned by your teams", {

      describeCredentialsRule("delete campaigns owned by your teams", "yes", "campaigns", ["owner", "administrator"], (string type) {
        auto initialSize = crates.campaign.get.size;

        router
          .request
          .delete_("/campaigns/000000000000000000000002")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(204)
          .end((Response response) => () {
            crates.campaign.get.size.should.equal(initialSize - 1);
          });
      });

      describeCredentialsRule("delete campaigns owned by your teams", "no", "campaigns", ["leader", "member", "guest"], (string type) {
        auto initialSize = crates.campaign.get.size;

        router
          .request
          .delete_("/campaigns/000000000000000000000002")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal((`{"errors": [{
                "description": "You don't have enough rights to remove ` ~ "`000000000000000000000002`" ~ `.",
                "status": 403,
                "title": "Forbidden"
              }]}`).parseJsonString);
            crates.campaign.get.size.should.equal(initialSize);
          });
      });

      describeCredentialsRule("delete campaigns owned by your teams", "-", "campaigns", "no rights", {
        auto initialSize = crates.campaign.get.size;

        router
          .request
          .delete_("/campaigns/000000000000000000000002")
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
                "error": "Authorization required"
              }`.parseJsonString);
            crates.campaign.get.size.should.equal(initialSize);
          });
      });
    });

    describe("delete campaigns owned by other teams", {

      describeCredentialsRule("delete campaigns owned by other teams", "yes", "campaigns", ["administrator"], (string type) {
        auto initialSize = crates.campaign.get.size;

        router
          .request
          .delete_("/campaigns/000000000000000000000004")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(204)
          .end((Response response) => () {
            crates.campaign.get.size.should.equal(initialSize - 1);
          });
      });

      describeCredentialsRule("delete campaigns owned by other teams", "no", "campaigns", ["owner", "leader", "member", "guest"], (string type) {
        auto initialSize = crates.campaign.get.size;

        router
          .request
          .delete_("/campaigns/000000000000000000000004")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(404)
          .end((Response response) => () {
            response.bodyJson.should.equal((`{"errors": [{
                "description": "Item ` ~ "`000000000000000000000004`" ~ ` not found.",
                "status": 404,
                "title": "Crate not found"
              }]}`).parseJsonString);
            crates.campaign.get.size.should.equal(initialSize);
          });
      });

      describeCredentialsRule("delete campaigns owned by other teams", "no", "campaigns", "no rights", {
        auto initialSize = crates.campaign.get.size;

        router
          .request
          .delete_("/campaigns/000000000000000000000004")
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
                "error": "Authorization required"
              }`.parseJsonString);
            crates.campaign.get.size.should.equal(initialSize);
          });
      });
    });
  });
});