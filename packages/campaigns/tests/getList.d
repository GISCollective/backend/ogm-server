/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.campaigns.getList;

import tests.fixtures;

import std.algorithm;
import std.array;
import ogm.campaigns.api;

alias suite = Spec!({
  URLRouter router;
  Json data;

  describe("GET List", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupCampaignApi(crates);
    });

    describe("with the ids query param", {
      it("returns only the items that have the requested ids", {
        router
        .request
        .get("/campaigns?ids=000000000000000000000001,000000000000000000000003,000000000000000000000002")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["campaigns"]
            .byValue
            .map!(a => a["_id"].to!string)
            .should
            .equal(["000000000000000000000001", "000000000000000000000003", "000000000000000000000002"]);
        });
      });

      it("returns only the items that the user can access", {
        router
        .request
        .get("/campaigns?ids=000000000000000000000001,000000000000000000000003,000000000000000000000002,000000000000000000000004")
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["campaigns"]
            .byValue
            .map!(a => a["_id"].to!string)
            .should
            .equal(["000000000000000000000001", "000000000000000000000003"]);
        });
      });
    });

    describeCredentialsRule("get the list of public campaigns", "yes", "campaigns", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/campaigns")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["campaigns"].byValue.map!(a => a["_id"].to!string).should.contain(["000000000000000000000001", "000000000000000000000003"]);
        });
    });

    describeCredentialsRule("get the list of public campaigns", "yes", "campaigns", "no rights", {
      router
        .request
        .get("/campaigns")
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["campaigns"].byValue.map!(a => a["_id"].to!string).should.contain(["000000000000000000000001", "000000000000000000000003"]);
        });
    });


    describeCredentialsRule("get the list of the team campaigns", "yes", "campaigns", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/campaigns")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["campaigns"].byValue.map!(a => a["_id"].to!string).should.contain(["000000000000000000000001", "000000000000000000000002"]);
        });
    });

    describeCredentialsRule("get the list of the team campaigns", "-", "campaigns", "no rights", {
      router
        .request
        .get("/campaigns")
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["campaigns"].byValue.map!(a => a["_id"].to!string).should.not.contain("000000000000000000000002");
        });
    });


    describeCredentialsRule("get the list of private campaigns owned by other teams", "yes", "campaigns", "administrator", (string type) {
      router
        .request
        .get("/campaigns")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["campaigns"].byValue.map!(a => a["_id"].to!string).should.contain("000000000000000000000004");
        });
    });

    describeCredentialsRule("get the list of private campaigns owned by other teams", "no", "campaigns", ["owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/campaigns")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["campaigns"].byValue.map!(a => a["_id"].to!string).should.not.contain("000000000000000000000004");
        });
    });

    describeCredentialsRule("get the list of private campaigns owned by other teams", "no", "campaigns", "no rights", {
      router
        .request
        .get("/campaigns")
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["campaigns"].byValue.map!(a => a["_id"].to!string).should.not.contain("000000000000000000000004");
        });
    });
  });
});
