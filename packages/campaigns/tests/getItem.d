/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.campaigns.getItem;


import tests.fixtures;

import std.algorithm;
import std.array;
import ogm.defaults.teams;
import ogm.defaults.articles;
import ogm.campaigns.api;

alias suite = Spec!({
  URLRouter router;
  Json data;

  describe("GET Item", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupCampaignApi(crates);
    });

    describeCredentialsRule("get a public campaign", "yes", "campaigns", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/campaigns/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          auto item = `{
            "campaign": {
              "_id": "000000000000000000000001",
              "map": {
                "isEnabled": true,
                "map": "000000000000000000000001"
              },
              "info": {
                "changeIndex": 0,
                "createdOn": "2015-01-01T00:00:00Z",
                "lastChangeOn": "2015-01-01T00:00:00Z",
                "originalAuthor": "",
                "author": ""
              },
              "visibility": {
                "isDefault": false,
                "isPublic": true,
                "team": "000000000000000000000001"
              },
              "area": {
                "coordinates": [[[-70, 30], [-70, 50], [-80, 50], [-80, 30], [-70, 30]]],
                "type": "Polygon"
              },
              "baseMaps": {
                "list": [],
                "useCustomList": false
              },
              "article": "",
              "startDate": "0001-01-01T00:00:00+00:00",
              "endDate": "0001-01-01T00:00:00+00:00",
              "name": "Campaign 1",
              "cover": "000000000000000000000002",
              "icons": [],
              "questions": [],
              "contributorQuestions": [],
              "optionalIcons": [],
              "options": {
                "description": {
                  "customQuestion": false,
                  "label": ""
                },
                "descriptionLabel": "",
                "featureNamePrefix": "",
                "icons": {
                  "customQuestion": false,
                  "label": ""
                },
                "iconsLabel": "",
                "name": {
                  "customQuestion": false,
                  "label": ""
                },
                "pictures": {
                  "customQuestion": false,
                  "isMandatory": false,
                  "label": ""
                },
                "nameLabel": "",
                "registrationMandatory": false,
                "showDescriptionQuestion": false,
                "showNameQuestion": false
              },
            }
          }`.parseJsonString;

          item["campaign"]["canEdit"] = type == "administrator" || type == "owner";
          response.bodyJson["campaign"]["questions"] = item["campaign"]["questions"];

          response.bodyJson.should.equal(item);
        });
    });

    describeCredentialsRule("get a public campaign", "yes", "campaigns", "no rights", {
      router
        .request
        .get("/campaigns/000000000000000000000001")
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          auto item = `{
            "campaign": {
              "_id": "000000000000000000000001",
              "map": {
                "isEnabled": true,
                "map": "000000000000000000000001"
              },
              "info": {
                "changeIndex": 0,
                "createdOn": "2015-01-01T00:00:00Z",
                "lastChangeOn": "2015-01-01T00:00:00Z",
                "originalAuthor": "",
                "author": ""
              },
              "visibility": {
                "isDefault": false,
                "isPublic": true,
                "team": "000000000000000000000001"
              },
              "article": "",
              "area": {
                "coordinates": [[[-70, 30], [-70, 50], [-80, 50], [-80, 30], [-70, 30]]],
                "type": "Polygon"
              },
              "baseMaps": {
                "list": [],
                "useCustomList": false
              },
              "startDate": "0001-01-01T00:00:00+00:00",
              "endDate": "0001-01-01T00:00:00+00:00",
              "name": "Campaign 1",
              "cover": "000000000000000000000002",
              "icons": [],
              "questions": [],
              "contributorQuestions": [],
              "optionalIcons": [],
              "options": {
                "description": {
                  "customQuestion": false,
                  "label": ""
                },
                "descriptionLabel": "",
                "featureNamePrefix": "",
                "icons": {
                  "customQuestion": false,
                  "label": ""
                },
                "iconsLabel": "",
                "name": {
                  "customQuestion": false,
                  "label": ""
                },
                "pictures": {
                  "customQuestion": false,
                  "isMandatory": false,
                  "label": ""
                },
                "nameLabel": "",
                "registrationMandatory": false,
                "showDescriptionQuestion": false,
                "showNameQuestion": false
              },
            }
          }`.parseJsonString;
          response.bodyJson["campaign"]["questions"] = item["campaign"]["questions"];

          response.bodyJson.should.equal(item);
        });
    });


    describeCredentialsRule("get a private campaign owned by own team", "yes", "campaigns", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/campaigns/000000000000000000000002")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          auto item = `{
            "campaign": {
              "_id": "000000000000000000000002",
              "map": {
                "isEnabled": true,
                "map": "000000000000000000000002"
              },
              "info": {
                "changeIndex": 0,
                "createdOn": "2015-01-01T00:00:00Z",
                "lastChangeOn": "2015-01-01T00:00:00Z",
                "originalAuthor": "",
                "author": ""
              },
              "visibility": {
                "isDefault": false,
                "isPublic": false,
                "team": "000000000000000000000002"
              },
              "article": "",
              "area": {
                "coordinates": [[[-7, 3], [-7, 5], [-8, 5], [-8, 3], [-7, 3]]],
                "type": "Polygon"
              },
              "baseMaps": {
                "list": [],
                "useCustomList": false
              },
              "questions": [],
              "contributorQuestions": [],
              "startDate": "0001-01-01T00:00:00+00:00",
              "endDate": "0001-01-01T00:00:00+00:00",
              "name": "Campaign 2",
              "cover": "000000000000000000000002",
              "icons": [],
              "optionalIcons": [],
              "options": {
                "description": {
                  "customQuestion": false,
                  "label": ""
                },
                "descriptionLabel": "",
                "featureNamePrefix": "",
                "icons": {
                  "customQuestion": false,
                  "label": ""
                },
                "iconsLabel": "",
                "name": {
                  "customQuestion": false,
                  "label": ""
                },
                "pictures": {
                  "customQuestion": false,
                  "isMandatory": false,
                  "label": ""
                },
                "nameLabel": "",
                "registrationMandatory": false,
                "showDescriptionQuestion": false,
                "showNameQuestion": false
              },
            }
          }`.parseJsonString;

          item["campaign"]["canEdit"] = type == "administrator" || type == "owner";
          response.bodyJson["campaign"]["questions"] = item["campaign"]["questions"];

          response.bodyJson.should.equal(item);
        });
    });

    describeCredentialsRule("get a private campaign owned by own team", "-", "campaigns", "no rights", {
      router
        .request
        .get("/campaigns/000000000000000000000002")
        .send(data)
        .expectStatusCode(404)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{"errors": [{
            "description": "There is no item with id ` ~ "`000000000000000000000002`" ~ `",
            "status": 404,
            "title": "Crate not found"
          }]}`).parseJsonString);
        });
    });


    describeCredentialsRule("get a private campaign owned by other team", "yes", "campaigns", ["administrator"], (string type) {
      router
        .request
        .get("/campaigns/000000000000000000000004")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          auto item = `{
            "campaign": {
              "_id": "000000000000000000000004",
              "map": {
                "isEnabled": true,
                "map": "000000000000000000000004"
              },
              "info": {
                "changeIndex": 0,
                "createdOn": "2015-01-01T00:00:00Z",
                "lastChangeOn": "2015-01-01T00:00:00Z",
                "originalAuthor": "",
                "author": ""
              },
              "visibility": {
                "isDefault": false,
                "isPublic": false,
                "team": "000000000000000000000004"
              },
              "area": {
                "coordinates": [[[-7, 3], [-7, 5], [-8, 5], [-8, 3], [-7, 3]]],
                "type": "Polygon"
              },
              "baseMaps": {
                "list": [],
                "useCustomList": false
              },
              "article": "",
              "startDate": "0001-01-01T00:00:00+00:00",
              "endDate": "0001-01-01T00:00:00+00:00",
              "name": "Campaign 4",
              "cover": "000000000000000000000002",
              "icons": [],
              "questions": [],
              "contributorQuestions": [],
              "optionalIcons": [],
              "options": {
                "description": {
                  "customQuestion": false,
                  "label": ""
                },
                "descriptionLabel": "",
                "featureNamePrefix": "",
                "icons": {
                  "customQuestion": false,
                  "label": ""
                },
                "iconsLabel": "",
                "name": {
                  "customQuestion": false,
                  "label": ""
                },
                "pictures": {
                  "customQuestion": false,
                  "isMandatory": false,
                  "label": ""
                },
                "nameLabel": "",
                "registrationMandatory": false,
                "showDescriptionQuestion": false,
                "showNameQuestion": false
              },
            }
          }`.parseJsonString;


          item["campaign"]["canEdit"] = type == "administrator" || type == "owner";
          response.bodyJson["campaign"]["questions"] = item["campaign"]["questions"];

          response.bodyJson.should.equal(item);
        });
    });

    describeCredentialsRule("get a private campaign owned by other team", "no", "campaigns", ["owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/campaigns/000000000000000000000004")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(data)
        .expectStatusCode(404)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{"errors": [{
            "description": "There is no item with id ` ~ "`000000000000000000000004`" ~ `",
            "status": 404,
            "title": "Crate not found"
          }]}`).parseJsonString);
        });
    });

    describeCredentialsRule("get a private campaign owned by other team", "no", "campaigns", "no rights", {
      router
        .request
        .get("/campaigns/000000000000000000000004")
        .send(data)
        .expectStatusCode(404)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{"errors": [{
            "description": "There is no item with id ` ~ "`000000000000000000000004`" ~ `",
            "status": 404,
            "title": "Crate not found"
          }]}`).parseJsonString);
        });
    });

    describe("for a campaign with an add notification", {
      beforeEach({
        crates.setupDefaultTeams;
        crates.setupDefaultArticles;
        auto article = crates.article.get
          .where("slug")
          .equal("notification-campaign-add-default")
          .and.exec.front.clone;

        article["slug"] = "notification-campaign-add-000000000000000000000001";
        crates.article.addItem(article);
      });

      it("adds the article id for an admin user", {
        router
          .request
          .get("/campaigns/000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["campaign"]["notifications"].should.equal(`{
              "add": "000000000000000000000017"
            }`.parseJsonString);
          });
      });

      it("adds the article id for a guest user", {
        router
          .request
          .get("/campaigns/000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList["guest"])
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["campaign"]["notifications"].should.equal(`{
              "add": "000000000000000000000017"
            }`.parseJsonString);
          });
      });

      it("adds the article id for a guest user", {
        router
        .request
        .get("/campaigns/000000000000000000000001")
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["campaign"]["notifications"].type.should.equal(Json.Type.undefined);
        });
      });
    });

    it("copies the area from the map when it is enabled", {
      router
        .request
        .get("/campaigns/000000000000000000000001")
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["campaign"]["area"].toString.should.equal(`{"type":"Polygon","coordinates":[[[-70,30],[-70,50],[-80,50],[-80,30],[-70,30]]]}`);
        });
    });

    describe("initializing questions from options", {
      it("copies the default questions from options when they are not set", {
        auto campaign = crates.campaign.getItem("000000000000000000000001").and.exec.front;

        campaign["optionalIcons"] = ["000000000000000000000001"].serializeToJson;
        campaign["options"] = `{
          "showNameQuestion": true,
          "showDescriptionQuestion": true,
          "descriptionLabel": "",
          "nameLabel": "",
          "registrationMandatory": false,
          "name": {
            "customQuestion": false,
            "label": ""
          },
          "iconsLabel": "",
          "icons": {
            "customQuestion": false,
            "label": ""
          },
          "featureNamePrefix": "",
          "description": {
            "customQuestion": false,
            "label": ""
          },
          "pictures": {
            "customQuestion": false,
            "isMandatory": true,
            "label": ""
          }
        }`.parseJsonString;

        crates.campaign.updateItem(campaign);

        router
          .request
          .get("/campaigns/000000000000000000000001")
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["campaign"]["questions"][0].should.equal(`{
              "isRequired": true,
              "isVisible": true,
              "type": "geo json",
              "help": "",
              "question": "Location",
              "name": "position",
              "options": {
                "allowGps": true,
                "allowManual": true,
                "allowAddress": true,
                "allowExistingFeature": false
              }
            }`.parseJsonString);

            response.bodyJson["campaign"]["questions"][1].should.equal(`{
              "isRequired": true,
              "isVisible": true,
              "type": "short text",
              "help": "",
              "question": "What's the name of this place?",
              "name": "name"
            }`.parseJsonString);

            response.bodyJson["campaign"]["questions"][2].should.equal(`{
              "isRequired": true,
              "isVisible": true,
              "type": "long text",
              "help": "",
              "question": "What do you like about this place?",
              "name": "description"
            }`.parseJsonString);

            response.bodyJson["campaign"]["questions"][3].should.equal(`{
              "isRequired": false,
              "isVisible": true,
              "type": "icons",
              "help": "",
              "question": "Please select an icon from the list",
              "name": "icons",
              "options": ["000000000000000000000001"]
            }`.parseJsonString);

            response.bodyJson["campaign"]["questions"][4].should.equal(`{
              "isRequired": true,
              "isVisible": true,
              "type": "pictures",
              "help": "",
              "question": "Pictures",
              "name": "pictures"
            }`.parseJsonString);

            response.bodyJson["campaign"]["questions"][5].should.equal(`{
              "isRequired": false,
              "isVisible": false,
              "type": "sounds",
              "help": "",
              "question": "Sounds",
              "name": "sounds"
            }`.parseJsonString);
          });
      });

      it("copies the custom questions from options when they are set", {
        auto campaign = crates.campaign.getItem("000000000000000000000001").and.exec.front;

        campaign["optionalIcons"] = ["000000000000000000000001"].serializeToJson;
        campaign["options"] = `{
          "showNameQuestion": true,
          "showDescriptionQuestion": true,
          "descriptionLabel": "old description",
          "nameLabel": "old name",
          "registrationMandatory": false,
          "location": {
            "customQuestion": true,
            "label": "new location"
          },
          "name": {
            "customQuestion": true,
            "label": "new name"
          },
          "iconsLabel": "",
          "icons": {
            "customQuestion": true,
            "label": "new icons"
          },
          "featureNamePrefix": "",
          "description": {
            "customQuestion": true,
            "label": "new description"
          },
          "pictures": {
            "customQuestion": true,
            "isMandatory": true,
            "label": "new pictures"
          }
        }`.parseJsonString;

        crates.campaign.updateItem(campaign);

        router
          .request
          .get("/campaigns/000000000000000000000001")
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["campaign"]["questions"][0].should.equal(`{
              "isRequired": true,
              "isVisible": true,
              "type": "geo json",
              "help": "",
              "question": "new location",
              "name": "position",
              "options": {
                "allowGps": true,
                "allowManual": true,
                "allowAddress": true,
                "allowExistingFeature": false
              }
            }`.parseJsonString);

            response.bodyJson["campaign"]["questions"][1].should.equal(`{
              "isRequired": true,
              "isVisible": true,
              "type": "short text",
              "help": "",
              "question": "new name",
              "name": "name"
            }`.parseJsonString);

            response.bodyJson["campaign"]["questions"][2].should.equal(`{
              "isRequired": true,
              "isVisible": true,
              "type": "long text",
              "help": "",
              "question": "new description",
              "name": "description"
            }`.parseJsonString);

            response.bodyJson["campaign"]["questions"][3].should.equal(`{
              "isRequired": false,
              "isVisible": true,
              "type": "icons",
              "help": "",
              "question": "new icons",
              "name": "icons",
              "options": ["000000000000000000000001"]
            }`.parseJsonString);

            response.bodyJson["campaign"]["questions"][4].should.equal(`{
              "isRequired": true,
              "isVisible": true,
              "type": "pictures",
              "help": "",
              "question": "new pictures",
              "name": "pictures"
            }`.parseJsonString);

            response.bodyJson["campaign"]["questions"][5].should.equal(`{
              "isRequired": false,
              "isVisible": false,
              "type": "sounds",
              "help": "",
              "question": "Sounds",
              "name": "sounds"
            }`.parseJsonString);
          });
      });
    });

    it("does not copy the area from the map when it is disabled", {
      auto campaign = crates.campaign.get.where("_id").equal(ObjectId.fromString("000000000000000000000001")).and.exec.front;
      campaign["map"]["isEnabled"] = false;

      crates.campaign.updateItem(campaign);

      router
      .request
      .get("/campaigns/000000000000000000000001")
      .send(data)
      .expectStatusCode(200)
      .end((Response response) => () {
        response.bodyJson["campaign"]["area"].toString.should.equal(`null`);
      });
    });

    it("does not copy the area from the map when the campaign map field is not set", {
      auto campaign = crates.campaign.get.where("_id").equal(ObjectId.fromString("000000000000000000000001")).and.exec.front.clone;
      campaign.remove("map");

      crates.campaign.updateItem(campaign);

      router
        .request
        .get("/campaigns/000000000000000000000001")
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["campaign"]["area"].toString.should.equal(`null`);
        });
    });

    it("fills in the default basemaps", {
      crates.baseMap.addItem(`{
        "visibility": {
          "isDefault": true,
          "isPublic": true
        }
      }`.parseJsonString);

      auto campaign = crates.campaign.get.where("_id").equal(ObjectId.fromString("000000000000000000000001")).and.exec.front;

      router
        .request
        .get("/campaigns/000000000000000000000001")
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          import std.stdio;
          response.bodyJson["campaign"]["baseMaps"].writeln;
          response.bodyJson["campaign"]["baseMaps"].should.equal(`{
            "list": ["000000000000000000000001"],
            "useCustomList": false
          }`.parseJsonString);
        });
    });
  });
});
