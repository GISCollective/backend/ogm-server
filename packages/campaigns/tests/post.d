/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.campaigns.post;

import tests.fixtures;

import std.algorithm;
import std.array;
import ogm.campaigns.api;

alias suite = Spec!({
  URLRouter router;
  Json data;
  Json dataOtherTeam;

  describe("POST", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupCampaignApi(crates);

      data = `{ "campaign": {
        "name":"new name",
        "article": "",
        "icons": [],
        "cover": "000000000000000000000001",
        "visibility": {
          "team":"000000000000000000000001",
          "isPublic": true
        }
      }}`.parseJsonString;

      dataOtherTeam = `{ "campaign": {
        "name":"new name",
        "article": "",
        "icons": [],
        "cover": "000000000000000000000001",
        "visibility": {
          "team":"000000000000000000000003",
          "isPublic": true
        }
      }}`.parseJsonString;
    });

    describeCredentialsRule("create campaigns for your teams", "yes", "campaigns", ["owner", "administrator"], (string type) {
      router
        .request
        .post("/campaigns")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["campaign"]["_id"].to!string.should.equal("000000000000000000000005");
          response.bodyJson["campaign"]["canEdit"].to!string.should.equal("true");
          response.bodyJson["campaign"]["name"].to!string.should.equal("new name");
        });
    });

    describeCredentialsRule("create campaigns for your teams", "no", "campaigns", ["leader", "member", "guest"], (string type) {
      router
        .request
        .post("/campaigns")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(data)
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{"errors": [{
            "description": "You don't have enough rights to add an item.",
            "status": 403,
            "title": "Forbidden"
          }]}`).parseJsonString);
        });
    });

    describeCredentialsRule("create campaigns for your teams", "-", "campaigns", "no rights", {
      router
        .request
        .post("/campaigns")
        .send(data)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "error": "Authorization required"
          }`.parseJsonString);
        });
    });


    describeCredentialsRule("create campaigns for other teams", "yes", "campaigns", ["administrator"], (string type) {
      router
        .request
        .post("/campaigns")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(dataOtherTeam)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["campaign"]["_id"].to!string.should.equal("000000000000000000000005");
          response.bodyJson["campaign"]["canEdit"].to!string.should.equal("true");
          response.bodyJson["campaign"]["name"].to!string.should.equal("new name");
        });
    });

    describeCredentialsRule("create campaigns for other teams", "no", "campaigns", ["owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .post("/campaigns")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(dataOtherTeam)
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{"errors": [{
            "description": "You can't add an item for the team ` ~ "`000000000000000000000003`" ~ `.",
            "status": 403,
            "title": "Forbidden"
          }]}`).parseJsonString);
        });
    });

    describeCredentialsRule("create campaigns for other teams", "no", "campaigns", "no rights", {
      router
        .request
        .post("/campaigns")
        .send(dataOtherTeam)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "error": "Authorization required"
          }`.parseJsonString);
        });
    });
  });
});
