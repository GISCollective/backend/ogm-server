/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.campaigns.patch;

import tests.fixtures;

import std.algorithm;
import std.array;
import ogm.campaigns.api;

alias suite = Spec!({
  URLRouter router;
  Json data;

  describe("PATCH", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupCampaignApi(crates);

      data = `{ "campaign": {
        "name":"new name"
      }}`.parseJsonString;
    });

    describeCredentialsRule("patch campaigns owned by your teams", "yes", "campaigns", ["owner", "administrator"], (string type) {
      router
        .request
        .patch("/campaigns/000000000000000000000002")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["campaign"]["_id"].to!string.should.equal("000000000000000000000002");
          response.bodyJson["campaign"]["canEdit"].to!string.should.equal("true");
          response.bodyJson["campaign"]["name"].to!string.should.equal("new name");
        });
    });

    describeCredentialsRule("patch campaigns owned by your teams", "no", "campaigns", ["leader", "member", "guest"], (string type) {
      router
        .request
        .patch("/campaigns/000000000000000000000002")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(data)
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{"errors": [{
            "description": "You don't have enough rights to edit ` ~ "`000000000000000000000002`" ~ `.",
            "status": 403,
            "title": "Forbidden"
          }]}`).parseJsonString);
        });
    });

    describeCredentialsRule("patch campaigns owned by your teams", "-", "campaigns", "no rights", {
      router
        .request
        .patch("/campaigns/000000000000000000000002")
        .send(data)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "error": "Authorization required"
          }`.parseJsonString);
        });
    });


    describeCredentialsRule("patch campaigns owned by other teams", "yes", "campaigns", ["administrator"], (string type) {
      router
        .request
        .patch("/campaigns/000000000000000000000003")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["campaign"]["_id"].to!string.should.equal("000000000000000000000003");
          response.bodyJson["campaign"]["canEdit"].to!string.should.equal("true");
          response.bodyJson["campaign"]["name"].to!string.should.equal("new name");
        });
    });

    describeCredentialsRule("patch campaigns owned by other teams", "no", "campaigns", ["owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .patch("/campaigns/000000000000000000000003")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(data)
        .expectStatusCode(404)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{"errors": [{
            "description": "Item ` ~ "`000000000000000000000003`" ~ ` not found.",
            "status": 404,
            "title": "Crate not found"
          }]}`).parseJsonString);
        });
    });

    describeCredentialsRule("patch campaigns owned by other teams", "no", "campaigns", "no rights", {
      router
        .request
        .patch("/campaigns/000000000000000000000003")
        .send(data)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "error": "Authorization required"
          }`.parseJsonString);
        });
    });
  });
});
