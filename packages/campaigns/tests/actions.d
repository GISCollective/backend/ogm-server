/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.campaigns.actions;

import tests.fixtures;

import std.algorithm;
import std.array;
import ogm.defaults.teams;
import ogm.defaults.articles;
import ogm.campaigns.api;

alias suite = Spec!({
  URLRouter router;
  Json data;

  describe("operations", {
    beforeEach({
      setupTestData();
      crates.setupDefaultTeams;
      crates.setupDefaultArticles;
      router = new URLRouter;
      router.crateSetup.setupCampaignApi(crates);

      data = `{}`.parseJsonString;
    });

    describe("the createNotification action", {
      describeCredentialsRule("create campaign notification for your teams", "yes", "campaigns", ["owner", "administrator"], (string type) {
        router
          .request
          .post("/campaigns/000000000000000000000002/createNotification")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .send(data)
          .expectStatusCode(201)
          .end((Response response) => () {
            auto range = crates.article.get
              .where("slug")
              .equal("notification-campaign-add-000000000000000000000002")
              .and.exec;

            range.empty.should.equal(false);
            auto article = range.front;
            article["slug"].should.equal(`notification-campaign-add-000000000000000000000002`);
            article["title"].should.equal(`Thank you for your contribution!`);
            article["visibility"].should.equal(`{
              "isDefault": false,
              "isPublic": false,
              "team": "000000000000000000000002"
            }`.parseJsonString);
          });
      });

      describeCredentialsRule("create campaign notification for your teams", "no", "campaigns", ["leader", "member", "guest"], (string type) {
        router
          .request
          .post("/campaigns/000000000000000000000002/createNotification")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .send(data)
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal((`{"errors": [{
              "description": "You don't have enough rights.",
              "status": 403,
              "title": "Forbidden"
            }]}`).parseJsonString);
          });
      });

      describeCredentialsRule("create campaign notification for your teams", "-", "campaigns", "no rights", {
        router
          .request
          .put("/campaigns/000000000000000000000002")
          .send(data)
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "error": "Authorization required"
            }`.parseJsonString);
          });
      });


      describeCredentialsRule("create campaign notification for other teams", "yes", "campaigns", ["administrator"], (string type) {
        router
          .request
          .post("/campaigns/000000000000000000000003/createNotification")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .send(data)
          .expectStatusCode(201)
          .end;
      });

      describeCredentialsRule("create campaign notification for other teams", "no", "campaigns", ["owner", "leader", "member", "guest"], (string type) {
        router
          .request
          .post("/campaigns/000000000000000000000003/createNotification")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .send(data)
          .expectStatusCode(404)
          .end((Response response) => () {
            response.bodyJson.should.equal((`{"errors": [{
              "description": "There is no item with id ` ~ "`000000000000000000000003`" ~ `",
              "status": 404,
              "title": "Crate not found"
            }]}`).parseJsonString);
          });
      });

      describeCredentialsRule("create campaign notification for other teams", "no", "campaigns", "no rights", {
        router
          .request
          .post("/campaigns/000000000000000000000003/createNotification")
          .send(data)
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"errors": [{
              "description": "Authorization required",
              "status": 401,
              "title": "Unauthorized"
            }]}`.parseJsonString);
          });
      });

      describe("when the campaign has a notification", {
        beforeEach({
          auto article = crates.article.get
            .where("slug")
            .equal("notification-campaign-add-default")
            .and.exec.front.clone;
          article["slug"] = "notification-campaign-add-000000000000000000000002";
          crates.article.addItem(article);
        });

        it("returns an error", {
          router
            .request
            .post("/campaigns/000000000000000000000002/createNotification")
            .header("Authorization", "Bearer " ~ userTokenList["administrator"])
            .send(data)
            .expectStatusCode(403)
            .end((Response response) => () {
              response.bodyJson.should.equal(`{"errors": [{
                "description": "The notification already exists.",
                "status": 403,
                "title": "Forbidden"
              }]}`.parseJsonString);
            });
        });
      });
    });

    describe("the removeNotification action", {
      describe("when the campaign has a notification", {
        beforeEach({
          auto article = crates.article.get
            .where("slug")
            .equal("notification-campaign-add-default")
            .and.exec.front.clone;

          article["slug"] = "notification-campaign-add-000000000000000000000002";
          crates.article.addItem(article);

          article = article.clone;
          article["slug"] = "notification-campaign-add-000000000000000000000003";
          crates.article.addItem(article);
        });

        describeCredentialsRule("remove campaign notification for your teams", "yes", "campaigns", ["owner", "administrator"], (string type) {
          router
            .request
            .post("/campaigns/000000000000000000000002/removeNotification")
            .header("Authorization", "Bearer " ~ userTokenList[type])
            .send(data)
            .expectStatusCode(201)
            .end((Response response) => () {
              auto range = crates.article.get
                .where("slug")
                .equal("notification-campaign-add-000000000000000000000002")
                .and.exec;

              range.empty.should.equal(true);
            });
        });

        describeCredentialsRule("remove campaign notification for your teams", "no", "campaigns", ["leader", "member", "guest"], (string type) {
          router
            .request
            .post("/campaigns/000000000000000000000002/removeNotification")
            .header("Authorization", "Bearer " ~ userTokenList[type])
            .send(data)
            .expectStatusCode(403)
            .end((Response response) => () {
              response.bodyJson.should.equal((`{"errors": [{
                "description": "You don't have enough rights.",
                "status": 403,
                "title": "Forbidden"
              }]}`).parseJsonString);
            });
        });

        describeCredentialsRule("remove campaign notification for your teams", "-", "campaigns", "no rights", {
          router
            .request
            .put("/campaigns/000000000000000000000002")
            .send(data)
            .expectStatusCode(401)
            .end((Response response) => () {
              response.bodyJson.should.equal(`{
                "error": "Authorization required"
              }`.parseJsonString);
            });
        });


        describeCredentialsRule("remove campaign notification for other teams", "yes", "campaigns", ["administrator"], (string type) {
          router
            .request
            .post("/campaigns/000000000000000000000003/removeNotification")
            .header("Authorization", "Bearer " ~ userTokenList[type])
            .send(data)
            .expectStatusCode(201)
            .end;
        });

        describeCredentialsRule("remove campaign notification for other teams", "no", "campaigns", ["owner", "leader", "member", "guest"], (string type) {
          router
            .request
            .post("/campaigns/000000000000000000000003/removeNotification")
            .header("Authorization", "Bearer " ~ userTokenList[type])
            .send(data)
            .expectStatusCode(404)
            .end((Response response) => () {
              response.bodyJson.should.equal((`{"errors": [{
                "description": "There is no item with id ` ~ "`000000000000000000000003`" ~ `",
                "status": 404,
                "title": "Crate not found"
              }]}`).parseJsonString);
            });
        });

        describeCredentialsRule("remove campaign notification for other teams", "no", "campaigns", "no rights", {
          router
            .request
            .post("/campaigns/000000000000000000000003/removeNotification")
            .send(data)
            .expectStatusCode(401)
            .end((Response response) => () {
              response.bodyJson.should.equal(`{"errors": [{
                "description": "Authorization required",
                "status": 401,
                "title": "Unauthorized"
              }]}`.parseJsonString);
            });
        });
      });

      it("returns an error", {
        router
          .request
          .post("/campaigns/000000000000000000000002/removeNotification")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .send(data)
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"errors": [{
              "description": "The notification does not exist.",
              "status": 403,
              "title": "Forbidden"
            }]}`.parseJsonString);
          });
      });
    });
  });
});
