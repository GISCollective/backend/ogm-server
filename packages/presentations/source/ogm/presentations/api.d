/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.presentations.api;

import vibe.http.router;
import vibe.core.log;
import vibe.data.json;

import crate.http.router;
import crate.base;
import ogm.defaults.presentations;

import ogm.auth;
import ogm.crates.all;

import ogm.middleware.adminrequest;
import ogm.middleware.userdata;
import ogm.middleware.modelinfo;
import ogm.middleware.StaticDbContent;
import ogm.middleware.SlugMiddleware;
import ogm.middleware.PublisherTeamMiddleware;
import ogm.filter.visibility;

///
void setupPresentationApi(T)(CrateRouter!T crateRouter, OgmCrates crates) {
  crateGetters["Team"] = &crates.team.getItem;
  crateGetters["Picture"] = &crates.picture.getItem;

  auto staticDbContent = new StaticDbContent("presentation", CanGetAll.admin);
  auto auth = Authentication.instance(crates.user);
  auto adminRequest = new AdminRequest(crates.user);

  auto userDataMiddleware = UserDataMiddleware.instance(crates);
  auto modelInfo = new ModelInfoMiddleware("presentation", crates.presentation);
  auto slugMiddleware = new SlugMiddleware(crates.presentation);
  auto visibilityFilter = new VisibilityFilter!"presentations"(crates, crates.presentation);
  auto publisherTeamMiddleware = new PublisherTeamMiddleware("presentation", crates.team);

  auto preparedRoute = crateRouter.prepare(crates.presentation);

  preparedRoute.and(auth.publicDataMiddleware);
  preparedRoute.and(adminRequest);
  preparedRoute.and(publisherTeamMiddleware);
  preparedRoute.and(userDataMiddleware);
  preparedRoute.and(slugMiddleware);
  preparedRoute.and(modelInfo);
  preparedRoute.and(visibilityFilter);
}

void setupDefaultPresentations(OgmCrates crates) {
  crates.checkDefaultPresentations;
}
