/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.presentations.getlist;

import tests.fixtures;
import vibe.http.common;
import ogm.defaults.presentations;
import ogm.defaults.teams;
import ogm.presentations.api;

alias suite = Spec!({
  URLRouter router;

  describe("Get presentation list", {

    beforeEach({
      setupTestData();
      router = new URLRouter;

      crates.setupDefaultTeams;
      crates.checkDefaultPresentations;

      router.crateSetup.setupPresentationApi(crates);
      setupDefaultPresentations(crates);

      auto presentation = crates.presentation.getItem("000000000000000000000001").and.exec.front;
      presentation["visibility"]["isPublic"] = true;

      crates.presentation.updateItem(presentation);
    });

    describeCredentialsRule("get the public presentation list", "yes", "presentations", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      auto initialSize = crates.presentation.get.size;

      router
        .request
        .get("/presentations")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["presentations"].byValue.map!(a => a["_id"].to!string).should.equal(["000000000000000000000001"]);
        });
    });

    describeCredentialsRule("get the public presentation list", "yes", "presentations", "no rights", {
      router
        .request
        .get("/presentations")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["presentations"].byValue.map!(a => a["_id"].to!string).should.equal(["000000000000000000000001"]);
        });
    });

    describe("when there is a private presentation", {
      beforeEach({
        setupTestData();
        router = new URLRouter;

        crates.setupDefaultTeams;
        crates.checkDefaultPresentations;

        router.crateSetup.setupPresentationApi(crates);
        setupDefaultPresentations(crates);

        auto presentation = crates.presentation.getItem("000000000000000000000001").and.exec.front;
        presentation["visibility"]["isPublic"] = false;
        presentation["visibility"]["team"] = "000000000000000000000001";

        crates.presentation.updateItem(presentation);
      });

      describeCredentialsRule("get the private presentation list", "yes", "presentations", ["administrator", "owner", "leader", "member", "guest"], (string type) {
        auto initialSize = crates.presentation.get.size;

        router
          .request
          .get("/presentations")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["presentations"].byValue.map!(a => a["_id"].to!string).should.equal(["000000000000000000000001"]);
          });
      });

      describeCredentialsRule("get the private presentation list", "no", "presentations", "no rights", {
        router
          .request
          .get("/presentations")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["presentations"].byValue.map!(a => a["_id"].to!string).should.equal([]);
          });
      });

      describeCredentialsRule("get the own presentation list", "yes", "presentations", ["administrator", "owner", "leader", "member", "guest"], (string type) {
        auto editablepresentation = crates.presentation.getItem("000000000000000000000001").and.exec.front;
        editablepresentation["info"]["originalAuthor"] = userId[type];
        crates.presentation.updateItem(editablepresentation);

        router
          .request
          .get("/presentations")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["presentations"].byValue.map!(a => a["_id"].to!string).should.equal(["000000000000000000000001"]);
          });
      });

      describeCredentialsRule("get the own presentation list", "-", "presentations", "no rights", {});
    });
  });
});
