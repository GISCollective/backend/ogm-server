/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.presentations.post;

import tests.fixtures;
import vibe.http.common;
import ogm.defaults.presentations;
import ogm.defaults.teams;
import ogm.presentations.api;

alias suite = Spec!({
  URLRouter router;

  describe("Creating presentations", {
    Json presentation;

    beforeEach({
      setupTestData();
      router = new URLRouter;

      crates.setupDefaultTeams;
      crates.checkDefaultPresentations;

      router.crateSetup.setupPresentationApi(crates);
      setupDefaultPresentations(crates);
      presentation = `{ "presentation": {
        "name": "test",
        "slug": "test",
        "layout": "000000000000000000000001",
        "cols": [],
        "visibility": {
          "team":"000000000000000000000001",
          "isPublic": true,
          "isDefault": false
        }
      }}`.parseJsonString;

      auto team = crates.team.getItem("000000000000000000000001").and.exec.front;
      team["isPublisher"] = true;
      crates.team.updateItem(team);
    });

    describeCredentialsRule("create presentations for publisher teams", "yes", "presentations", ["administrator", "owner"], (string type) {
      auto initialSize = crates.presentation.get.size;

      router
        .request
        .post("/presentations")
        .send(presentation)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          crates.presentation.get.size.should.equal(initialSize + 1);
        });
    });

    describeCredentialsRule("create presentations for publisher teams", "no", "presentations", ["leader", "member", "guest"], (string type) {
      auto initialSize = crates.presentation.get.size;

      router
        .request
        .post("/presentations")
        .send(presentation)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "errors": [{
            "description": "You don't have enough rights to add an item.",
            "status": 403,
            "title": "Forbidden"
          }]}`.parseJsonString);
        });
    });

    describeCredentialsRule("create presentations for publisher teams", "-", "presentations", "no rights", {
      router
        .request
        .post("/presentations")
        .send(presentation)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
        });
    });

    describe("for a non publisher team", {
      beforeEach({
        auto team = crates.team.getItem("000000000000000000000001").and.exec.front;
        team["isPublisher"] = false;
        crates.team.updateItem(team);
      });

      describeCredentialsRule("create presentations for non publisher teams", "yes", "presentations", ["administrator"], (string type) {
        auto initialSize = crates.presentation.get.size;

        router
          .request
          .post("/presentations")
          .send(presentation)
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .end((Response response) => () {
            crates.presentation.get.size.should.equal(initialSize + 1);
          });
      });

      describeCredentialsRule("create presentations for non publisher teams", "no", "presentations", ["owner", "leader", "member", "guest"], (string type) {
        auto initialSize = crates.presentation.get.size;

        router
          .request
          .post("/presentations")
          .send(presentation)
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{ "errors": [{
              "description": "Presentations can be created only for publisher teams.",
              "status": 403,
              "title": "Forbidden"
            }]}`.parseJsonString);
          });
      });

      describeCredentialsRule("create presentations for non publisher teams", "-", "presentations", "no rights", {
        router
          .request
          .post("/presentations")
          .send(presentation)
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
          });
      });
    });
  });
});
