/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.presentations.getitem;

import tests.fixtures;
import vibe.http.common;
import ogm.defaults.presentations;
import ogm.defaults.teams;
import ogm.presentations.api;

alias suite = Spec!({
  URLRouter router;

  describe("get presentations", {
    beforeEach({
      setupTestData();
      router = new URLRouter;

      crates.setupDefaultTeams;
      crates.checkDefaultPresentations;

      router.crateSetup.setupPresentationApi(crates);
      setupDefaultPresentations(crates);

      auto presentation = crates.presentation.getItem("000000000000000000000001").and.exec.front;
      presentation["visibility"]["isPublic"] = true;
      presentation["visibility"]["team"] = "000000000000000000000001";
      crates.presentation.updateItem(presentation);
    });



    describeCredentialsRule("get public presentation by id", "yes", "presentations", ["administrator", "owner"], (string type) {
      auto initialSize = crates.presentation.get.size;

      router
        .request
        .get("/presentations/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["presentation"]["_id"].should.equal("000000000000000000000001");
          response.bodyJson["presentation"]["canEdit"].to!bool.should.equal(true);
        });
    });

    describeCredentialsRule("get public presentation by id", "yes", "presentations", ["leader", "member", "guest"], (string type) {
      auto initialSize = crates.presentation.get.size;

      router
        .request
        .get("/presentations/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["presentation"]["_id"].should.equal("000000000000000000000001");
          response.bodyJson["presentation"]["canEdit"].to!bool.should.equal(false);
        });
    });

    describeCredentialsRule("get public presentation by id", "yes", "presentations", "no rights", {
      router
        .request
        .get("/presentations/000000000000000000000001")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["presentation"]["_id"].should.equal("000000000000000000000001");
          response.bodyJson["presentation"]["canEdit"].to!bool.should.equal(false);
        });
    });

    describeCredentialsRule("get public presentation by slug", "yes", "presentations", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      auto initialSize = crates.presentation.get.size;

      router
        .request
        .get("/presentations/presentation--welcome?team=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["presentation"]["_id"].should.equal("000000000000000000000001");
        });
    });

    describeCredentialsRule("get public presentation by slug", "yes", "presentations", "no rights", {
      router
        .request
        .get("/presentations/presentation--welcome?team=000000000000000000000001")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["presentation"]["_id"].should.equal("000000000000000000000001");
        });
    });

    describe("when there is a private presentation", {
      beforeEach({
        auto presentation = crates.presentation.getItem("000000000000000000000001").and.exec.front;
        presentation["visibility"]["isPublic"] = false;
        presentation["visibility"]["team"] = "000000000000000000000001";

        crates.presentation.updateItem(presentation);
      });

      describeCredentialsRule("get private presentations", "yes", "presentations", ["administrator", "owner", "leader", "member", "guest"], (string type) {
        auto initialSize = crates.presentation.get.size;

        router
          .request
          .get("/presentations/000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["presentation"]["_id"].should.equal("000000000000000000000001");
          });
      });

      describeCredentialsRule("get private presentations", "no", "presentations", "no rights", {
        router
          .request
          .get("/presentations/000000000000000000000001")
          .expectStatusCode(404);
      });
    });
  });
});
