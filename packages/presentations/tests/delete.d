/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.presentations.delete_;

import tests.fixtures;
import vibe.http.common;
import ogm.defaults.presentations;
import ogm.defaults.teams;
import ogm.presentations.api;

alias suite = Spec!({
  URLRouter router;

  describe("Deleting presentations", {
    beforeEach({
      setupTestData();
      router = new URLRouter;

      crates.setupDefaultTeams;
      crates.checkDefaultPresentations;

      router.crateSetup.setupPresentationApi(crates);
      setupDefaultPresentations(crates);

      auto presentation = crates.presentation.getItem("000000000000000000000001").and.exec.front;
      presentation["visibility"]["isPublic"] = true;
      presentation["visibility"]["team"] = "000000000000000000000001";
      crates.presentation.updateItem(presentation);
    });


    describeCredentialsRule("delete any team presentations", "yes", "presentations", ["administrator", "owner"], (string type) {
      auto initialSize = crates.presentation.get.size;

      router
        .request
        .delete_("/presentations/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end((Response response) => () {
          crates.presentation.get.size.should.equal(initialSize - 1);
          crates.presentation.get.where("_id").equal(ObjectId.fromString("000000000000000000000001")).and.exec.empty.should.equal(true);
        });
    });

    describeCredentialsRule("delete any team presentations", "no", "presentations", ["leader", "member", "guest"], (string type) {
      auto initialSize = crates.presentation.get.size;

      router
        .request
        .delete_("/presentations/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          crates.presentation.get.size.should.equal(initialSize);
          crates.presentation.get.where("_id").equal(ObjectId.fromString("000000000000000000000001")).and.exec.empty.should.equal(false);
        });
    });

    describeCredentialsRule("delete any team presentations", "-", "presentations", "no rights", {
      router
        .request
        .delete_("/presentations/000000000000000000000001")
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.toPrettyString.should.equal(`{"error": "Authorization required"}`.parseJsonString.toPrettyString);
        });
    });

    describeCredentialsRule("delete own presentations", "yes", "presentations", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      auto presentation = crates.presentation.getItem("000000000000000000000001").and.exec.front;
      presentation["info"]["originalAuthor"] = userId[type];
      crates.presentation.updateItem(presentation);

      auto initialSize = crates.presentation.get.size;

      router
        .request
        .delete_("/presentations/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end((Response response) => () {
          crates.presentation.get.size.should.equal(initialSize - 1);
          crates.presentation.get.where("_id").equal(ObjectId.fromString("000000000000000000000001")).and.exec.empty.should.equal(true);
        });
    });

    describeCredentialsRule("delete own presentations", "-", "presentations", "no rights", {});
  });
});
