/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.presentations.put;

import tests.fixtures;
import vibe.http.common;
import ogm.defaults.presentations;
import ogm.defaults.teams;
import ogm.presentations.api;

alias suite = Spec!({
  URLRouter router;

  describe("Updating presentations", {
    Json presentation;

    beforeEach({
      setupTestData();
      router = new URLRouter;

      crates.setupDefaultTeams;
      crates.checkDefaultPresentations;

      router.crateSetup.setupPresentationApi(crates);
      setupDefaultPresentations(crates);
      presentation = `{ "presentation": {
        "name": "test",
        "slug": "test",
        "layout": "000000000000000000000001",
        "cols": [],
        "visibility": {
          "team":"000000000000000000000001",
          "isPublic": true,
          "isDefault": false
        }
      }}`.parseJsonString;


      auto storedPresentation = crates.presentation.getItem("000000000000000000000001").and.exec.front;
      storedPresentation["visibility"]["isPublic"] = true;
      storedPresentation["visibility"]["team"] = "000000000000000000000001";
      crates.presentation.updateItem(storedPresentation);
    });

    describeCredentialsRule("update team presentations", "yes", "presentations", ["administrator", "owner"], (string type) {
      auto initialSize = crates.presentation.get.size;

      router
        .request
        .put("/presentations/000000000000000000000001")
        .send(presentation)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          crates.presentation.get.size.should.equal(initialSize);
          response.bodyJson["presentation"]["name"].to!string.should.equal("test");
        });
    });

    describeCredentialsRule("update team presentations", "no", "presentations", [ "leader", "member", "guest"], (string type) {
      auto initialSize = crates.presentation.get.size;

      router
        .request
        .put("/presentations/000000000000000000000001")
        .send(presentation)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{ "errors": [{
            "description": "You don't have enough rights to edit ` ~ "`000000000000000000000001`" ~ `.",
            "status": 403,
            "title": "Forbidden"
          }]}`).parseJsonString);
        });
    });

    describeCredentialsRule("update team presentations", "-", "presentations", "no rights", {
      router
        .request
        .put("/presentations/000000000000000000000001")
        .send(presentation)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
        });
    });

    describeCredentialsRule("update own presentations", "yes", "presentations", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      auto editablepresentation = crates.presentation.getItem("000000000000000000000001").and.exec.front;
      editablepresentation["info"]["originalAuthor"] = userId[type];
      crates.presentation.updateItem(editablepresentation);

      auto initialSize = crates.presentation.get.size;

      router
        .request
        .put("/presentations/000000000000000000000001")
        .send(presentation)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          crates.presentation.get.size.should.equal(initialSize);
          response.bodyJson["presentation"]["name"].to!string.should.equal("test");
        });
    });

    describeCredentialsRule("update own presentations", "-", "presentations", "no rights", {});
  });
});
