/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.tiles.TileJson;

import crate.base;
import crate.error;
import crate.http.cors;
import crate.http.queryParams;
import crate.http.request;
import geo.json;

import std.exception;
import std.algorithm;
import std.path;
import std.array : array;

import geo.TileJSON;

import ogm.crates.all;
import ogm.http.request;

import vibe.http.server;
import vibe.data.json;
import vibe.core.log;

struct TileJsonSettings {
  static {
    string apiUrl;
    string frontendUrl;
  }
}

class TileJson {

  private {
    OgmCrates crates;
  }

  this(OgmCrates crates) {
    this.crates = crates;
  }

  ///
  void options(HTTPServerRequest, HTTPServerResponse response) {
    response.addHeaderValue("Access-Control-Allow-Origin", ["*"]);
    response.addHeaderValue("Access-Control-Allow-Methods", ["GET"]);
    response.addHeaderValue("Access-Control-Allow-Headers", ["Content-Type", "Authorization"]);

    response.writeBody("", 204);
  }

  ///
  void fillLayers(ref TileJSON!"3.0.0" tileJson) {
    TileJSON!"3.0.0".VectorLayer pointsLayer;
    pointsLayer.id = "points";
    pointsLayer.fields["_id"] = "String";
    pointsLayer.fields["icons"] = "String";
    pointsLayer.fields["name"] = "String";
    pointsLayer.fields["primaryIcon"] = "String";
    pointsLayer.fields["canView"] = "String";
    pointsLayer.fields["type"] = "String";
    pointsLayer.fields["visibility"] = "Integer";
    pointsLayer.minzoom = 0;
    pointsLayer.maxzoom = 16;
    pointsLayer.description = "Unclustered sites";

    TileJSON!"3.0.0".VectorLayer linesLayer;
    linesLayer.id = "lines";
    linesLayer.fields["_id"] = "String";
    linesLayer.fields["icons"] = "String";
    linesLayer.fields["name"] = "String";
    linesLayer.fields["primaryIcon"] = "String";
    linesLayer.fields["canView"] = "String";
    linesLayer.fields["type"] = "String";
    linesLayer.fields["visibility"] = "Integer";
    linesLayer.minzoom = 0;
    linesLayer.maxzoom = 16;
    linesLayer.description = "Lines";

    TileJSON!"3.0.0".VectorLayer poligonsLayer;
    poligonsLayer.id = "polygons";
    poligonsLayer.fields["_id"] = "String";
    poligonsLayer.fields["icons"] = "String";
    poligonsLayer.fields["name"] = "String";
    poligonsLayer.fields["primaryIcon"] = "String";
    poligonsLayer.fields["canView"] = "String";
    poligonsLayer.fields["type"] = "String";
    poligonsLayer.fields["visibility"] = "Integer";
    poligonsLayer.minzoom = 0;
    poligonsLayer.maxzoom = 16;
    poligonsLayer.description = "Polygons";

    TileJSON!"3.0.0".VectorLayer groupsLayer;
    groupsLayer.id = "groups";
    groupsLayer.fields["idList"] = "List of strings";
    groupsLayer.fields["count"] = "Integer";
    groupsLayer.fields["pendingCount"] = "Integer";
    groupsLayer.fields["level"] = "Integer";
    groupsLayer.minzoom = 0;
    groupsLayer.maxzoom = 16;

    TileJSON!"3.0.0".VectorLayer debugLayer;
    debugLayer.id = "debug";
    debugLayer.fields["size"] = "Integer";
    debugLayer.fields["name"] = "String";
    debugLayer.minzoom = 0;
    debugLayer.maxzoom = 16;

    tileJson.vector_layers ~= pointsLayer;
    tileJson.vector_layers ~= linesLayer;
    tileJson.vector_layers ~= poligonsLayer;
    tileJson.vector_layers ~= groupsLayer;
    tileJson.vector_layers ~= debugLayer;
  }

  ///
  void respondMapTileJson(string mapId, HTTPServerResponse res) {
    TileJSON!"3.0.0" tileJson;

    auto map = crates.map.getItem(mapId).exec.front;
    Json team;

    try {
      team = crates.team.getItem(map["visibility"]["team"].to!string).exec.front;
    } catch(Exception) {
      logError("The map `%s` has no team", mapId);
    }

    import std.stdio;
    writeln("area", map["area"]);

    Polygon polygon = deserializeJson!Polygon(map["area"]);

    if(polygon.coordinates.length > 0) {
      auto firstPair = polygon.coordinates[0][0];
      tileJson.bounds = [firstPair[0], firstPair[1], firstPair[0], firstPair[1]];

      foreach(pair; polygon.coordinates[0]) {
        if(pair[0] < tileJson.bounds[0]) {
          tileJson.bounds[0] = pair[0];
        }

        if(pair[1] < tileJson.bounds[1]) {
          tileJson.bounds[1] = pair[1];
        }

        if(pair[0] > tileJson.bounds[2]) {
          tileJson.bounds[2] = pair[0];
        }

        if(pair[1] > tileJson.bounds[3]) {
          tileJson.bounds[3] = pair[1];
        }
      }

      tileJson.center[0] = tileJson.bounds[0] + (tileJson.bounds[2] - tileJson.bounds[0]) / 2;
      tileJson.center[1] = tileJson.bounds[1] + (tileJson.bounds[3] - tileJson.bounds[1]) / 2;
    }

    tileJson.name = map["name"].to!string;
    tileJson.description = map["description"].to!string;
    tileJson.version_ = "1.0." ~ map["info"]["changeIndex"].to!string;
    tileJson.tiles ~= buildPath(TileJsonSettings.apiUrl, "tiles/{z}/{x}/{y}?format=vt&map=" ~ mapId);

    if(team.type == Json.Type.object) {
      tileJson.attribution = `<a href="` ~
        buildPath(TileJsonSettings.frontendUrl, "browse/teams/" ~ team["_id"].to!string) ~ `" target="_blank">&copy; ` ~ team["name"].to!string ~ `</a>`;
    }

    this.fillLayers(tileJson);

    res.writeJsonBody(tileJson, 200);
  }

  ///
  void respondAllTileJson(HTTPServerResponse res) {
    TileJSON!"3.0.0" tileJson;

    string name;
    Json[] names = crates.preference.get
      .where("name").equal("appearance.name").and
      .withProjection(["value"])
      .distinct("value")
      .array;

    if(names.length > 0) {
      name = names[0].to!string;
    }

    tileJson.name = name;
    tileJson.description = "";
    tileJson.version_ = "1.0.0";
    tileJson.tiles ~= buildPath(TileJsonSettings.apiUrl, "tiles/{z}/{x}/{y}?format=vt");

    tileJson.attribution = `<a href="` ~
        buildPath(TileJsonSettings.frontendUrl, `help/about" target="_blank">&copy; ` ~ name ~ `</a>`);

    this.fillLayers(tileJson);

    res.writeJsonBody(tileJson, 200);
  }

  ///
  void get(HTTPServerRequest req, HTTPServerResponse res) {
    string mapId;

    if("map" in req.query) {
      mapId = req.query["map"];
    } else {
      respondAllTileJson(res);
      return;
    }

    scope request = RequestUserData(req);

    bool isPrivate = false;
    bool isPublic = false;

    if(request.isAdmin) {
      isPublic = crates.map.get
        .where("_id").equal(ObjectId.fromString(mapId)).and
        .size > 0;
    } else {
      isPublic = crates.map.get
        .where("_id").equal(ObjectId.fromString(mapId)).and
        .where("visibility.isPublic").equal(true).and
        .size > 0;
    }

    if(!isPublic) {
      isPrivate = request.session(crates).all.mapsIds.map!(a => a.toString).canFind(mapId);
    }

    enforce!CrateNotFoundException(isPublic || isPrivate, "There is no map with the id `" ~ mapId ~ "`.");

    respondMapTileJson(mapId, res);
  }
}