/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.tiles.collections.tileData;

import ogm.tiles.collections.cluster;
import ogm.tiles.collections.plain;
import ogm.tiles.collections.debug_;

import vibe.http.server;
import ogm.models.tileLayer;

import geo.vector;

import std.array;

class TileData {

  ClusterCollection points;

  PlainCollection!("lines", int[][]) lines;

  PlainCollection!("polygons", int[][][]) polygons;

  DebugCollection debugCollection;

  this() {
    points.groupGrid = 20;
    points.groupSize = points.layerSize / points.groupGrid;
  }

  ///
  void flushJson(HTTPServerResponse response) {
    response.contentType = "application/json";
    response.statusCode = 200;

    response.bodyWriter.write(`{`);

    points.flushJson(response);

    response.bodyWriter.write(`,`);

    lines.flushJson(response);

    response.bodyWriter.write(`,`);
    polygons.flushJson(response);

    response.bodyWriter.write(`}`);
    response.bodyWriter.finalize;
  }

  ///
  void flushVt(HTTPServerResponse response) {
    response.contentType = "application/vnd.mapbox-vector-tile";
    response.statusCode = 200;

    points.flushVt(response);
    lines.flushVt(response);
    polygons.flushVt(response);

    debugCollection.flushVt(response);
  }

  void flushSvg(HTTPServerResponse response) {
    //auto tile = enableGroups ? getTile() : getUngroupedTile();

    //response.writeBody(renderer.render(tile), 200, "image/svg+xml");
  }
}


class UnclusteredTileData : TileData {

  this() {
    points.groupGrid = 0;
    points.groupSize = 0;
    points.ignoreGrouping = true;
  }
}
