/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.tiles.collections.debug_;

import std.conv;
import std.array;
import geo.vector;
import geo.proto;

import vibe.http.server;

///
struct DebugCollection {

  string name;

  ///
  enum layerSize = 4096;

  ///
  void flushVt(HTTPServerResponse response) {
    auto data = toLayer.toProtoBuf.array;

    response.bodyWriter.write(ProtoKey(3, WireType.fixedLength).value.raw);
    response.bodyWriter.write(Varint(data.length).raw);
    response.bodyWriter.write(data);
  }

  Layer toLayer() {
    Layer layer;
    layer.name = "debug";
    layer.extent = layerSize;

    foreach(size; [0., 10., 20., 30.]) {
      int diff = (4096. * (size / 100.)).to!int;

      layer.addFeature(
        VectorFeature.GeomType.lineString,
        Geometry.fromLine([[1 + diff, 1 + diff], [1 + diff, 4095 - diff]])
      );
      layer.addProperty("size", size);

      layer.addFeature(
        VectorFeature.GeomType.lineString,
        Geometry.fromLine([[1 + diff, 1 + diff], [4095 - diff, 1 + diff]])
      );
      layer.addProperty("size", size);

      layer.addFeature(
        VectorFeature.GeomType.lineString,
        Geometry.fromLine([[1 + diff, 4095 - diff], [4095 - diff, 4095 - diff]])
      );
      layer.addProperty("size", size);

      layer.addFeature(
        VectorFeature.GeomType.lineString,
        Geometry.fromLine([[4095 - diff, 1 + diff], [4095 - diff, 4095 - diff]])
      );
      layer.addProperty("size", size);
    }

    layer.addFeature(VectorFeature.GeomType.point, Geometry.fromPoint([layerSize/2, layerSize/2]));
    layer.addProperty("name", this.name);

    return layer;
  }
}