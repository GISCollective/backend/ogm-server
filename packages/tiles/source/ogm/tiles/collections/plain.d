/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.tiles.collections.plain;

import ogm.models.tileLayer;
import geo.vector;
import geo.proto;

import vibe.http.server;
import vibe.data.json;

import std.range;
import std.algorithm;
import std.conv;

import gis_collective.hmq.log;

///
struct PlainCollection(string key, T) {

  ///
  enum layerSize = 4096;

  ///
  T[] points;

  PropertyTable[] properties;

  ///
  void add(T points, PropertyTable properties) {
    this.points ~= points;
    this.properties ~= properties;
  }

  ///
  void flushJson(HTTPServerResponse response) {
    response.bodyWriter.write(`"`);
    response.bodyWriter.write(key);
    response.bodyWriter.write(`":[`);

    foreach(index, coordinates; points) {
      if(index > 0) {
        response.bodyWriter.write(`,`);
      }

      response.bodyWriter.write(`{"points": `);
      response.bodyWriter.write(coordinates.serializeToJsonString);
      response.bodyWriter.write(`,"properties":`);
      response.bodyWriter.write(properties[index].serializeToJsonString);
      response.bodyWriter.write(`}`);
    }

    response.bodyWriter.write(`]`);
  }

  ///
  void flushVt(HTTPServerResponse response) {
    auto data = toLayer.toProtoBuf.array;

    response.bodyWriter.write(ProtoKey(3, WireType.fixedLength).value.raw);
    response.bodyWriter.write(Varint(data.length).raw);
    response.bodyWriter.write(data);
  }

  ///
  Layer toLayer() {
    Layer layer;
    layer.name = key;
    layer.extent = layerSize;

    foreach(index, coordinates; points) {
      try {
        VectorFeature feature;

        static if(is(T == int[])) {
          feature.type = VectorFeature.GeomType.point;
          feature.geometry = Geometry.fromPoint(coordinates);
        }

        static if(is(T == int[][])) {
          feature.type = VectorFeature.GeomType.lineString;
          feature.geometry = Geometry.fromLine(coordinates);
        }

        static if(is(T == int[][][])) {
          if(coordinates.length == 0 || coordinates[0].length == 0) {
            continue;
          }

          feature.type = VectorFeature.GeomType.polygon;
          feature.geometry = Geometry.fromPolygon(coordinates[0]);
        }

        layer.features ~= feature;

        foreach(key, value; properties[index]) {
          layer.addProperty(key, value);
        }
      } catch(Exception e) {
        error(e);
      }
    }

    return layer;
  }
}