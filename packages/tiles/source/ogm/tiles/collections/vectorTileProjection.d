/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.tiles.collections.vectorTileProjection;

import ogm.tiles.collections.tileData;
import ogm.models.feature;

import std.conv;
import std.algorithm;
import std.array;

import vibe.data.json;
import vibe.data.bson;

import geo.conv;
import geo.xyz;
import geo.epsg;
import geo.json;
import geo.algorithm;
import std.datetime;

import geo.algo.bboxClip;
import ogm.models.tileLayer;

version(unittest) {
  import fluent.asserts;
}

struct VectorTileProjection {
  ZXY path;

  enum acceptedPolygonArea = 0.0005;
  enum acceptedLineArea = 0.00005;

  private {
    enum size = 4096;
    enum border = 5;

    BBox!double bbox;
    BBox!int bboxTile;

    double bboxArea;
    TileData tileData;
  }

  this(ZXY path, TileData tileData) {
    this.bbox = path.toBBox(-0.2);
    this.bboxTile = BBox!int(-1, size + 1, -1, size + 1);
    bboxArea = ZXY(path.z, 0, 0).toBBox.area;

    tileData.debugCollection.name = path.z.to!string ~ "/" ~ path.x.to!string ~ "/" ~ path.y.to!string;

    this.path = path;
    this.tileData = tileData;
  }

  void add(ref Point point, ref PropertyTable properties, ref FeatureDecorators decorators, bool ignoreIfCluster = false) {
    if(!bbox.contains(point.coordinates)) {
      return;
    }

    if(!this.isVisible(decorators)) {
      return;
    }

    if("type" !in properties) {
      properties["type"] = "point";
    }

    auto tilePoint = point.coordinates.toVectorTileCoordinates(path.x, path.y, path.z, this.size);

    tileData.points.add(tilePoint, properties, ignoreIfCluster);
  }

  void add(ref MultiPoint points, ref PropertyTable properties, ref FeatureDecorators decorators) {
    foreach(coordinates; points.coordinates) {
      auto tmp = Point(coordinates);
      add(tmp, properties, decorators);
    }
  }

  void add(ref LineString line, ref PropertyTable properties, ref FeatureDecorators decorators) {
    if(!this.isVisible(decorators)) {
      return;
    }

    auto lineBox = BBox!double(line.coordinates);
    auto area = lineBox.area / bboxArea;

    version(unittest) {} else debug {
      properties["relativeArea"] = area.to!string;
      properties["bboxArea"] = bboxArea.to!string;
      properties["lineArea"] = lineBox.area.to!string;
    }

    if("type" !in properties) {
      properties["type"] = "line";
    }

    foreach(coordinates; line.coordinates.clipLine(bbox)) {
      auto tileLine = coordinates
        .toVectorTileCoordinates(path.x, path.y, path.z, size, 3);

      if(area > acceptedLineArea) {
        tileData.lines.add(tileLine, properties);
      }
    }

    addLineMarkers(line.coordinates, properties, decorators);
  }

  void add(ref MultiLineString line, ref PropertyTable properties, ref FeatureDecorators decorators) {
    foreach(lineCoordinates; line.coordinates) {
      auto tmp = LineString(lineCoordinates);
      add(tmp, properties, decorators);
    }
  }

  void add(ref Polygon polygon, ref PropertyTable properties, ref FeatureDecorators decorators) {
    addPolygon(polygon.coordinates, properties, decorators);
  }

  void addPolygon(ref PositionT!double[][] coordinates, ref PropertyTable properties, ref FeatureDecorators decorators) {
    auto begin = Clock.currTime;

    if(coordinates.length == 0 || coordinates[0].length <= 2) {
      return;
    }

    if(coordinates[0].length > 1000) {
      debug {
        import std.stdio;
        writeln("Ignoring large polygon", properties);
      }

      return;
    }

    if(!this.isVisible(decorators)) {
      return;
    }

    if("type" !in properties) {
      properties["type"] = "polygon";
    }

    const polygonBox = BBox!double(coordinates[0]);
    const area = polygonBox.area / bboxArea;

    version(unittest) {} else debug {
      properties["relativeArea"] = area.to!string;
      properties["bboxArea"] = bboxArea.to!string;
      properties["polygonArea"] = polygonBox.area.to!string;
    }

    const areaInPoints = area * 4096;

    if(areaInPoints > 4 && decorators.showAsLineAfterZoom > path.z) {
      const intersectedCoordinates = coordinates.clipPolygon(bbox);

      auto tilePolygon = intersectedCoordinates.
        toVectorTileCoordinates(path.x, path.y, path.z, size, 3);

      tileData.polygons.add(tilePolygon, properties);
    }

    addPolygonMarkers(coordinates, properties, decorators);
  }

  void addMultiPolygon(Bson coordinates, ref PropertyTable properties, ref FeatureDecorators decorators) {
    auto begin = Clock.currTime;

    foreach(i; 0..coordinates.length) {
      auto tmp = coordinates[i].deserializeBson!(PositionT!double[][]);
      addPolygon(tmp, properties, decorators);
    }
  }

  void add(ref MultiPolygon polygon, ref PropertyTable properties, ref FeatureDecorators decorators) {
    auto begin = Clock.currTime;

    foreach(i; 0..polygon.coordinates.length) {
      addPolygon(polygon.coordinates[i], properties, decorators);
    }
  }

  void add(ref GeoJsonGeometry geometry, ref PropertyTable properties, ref FeatureDecorators decorators) {
    auto begin = Clock.currTime;

    if(!this.isVisible(decorators)) {
      return;
    }

    if(geometry.type == "Point") {
      auto tmp = geometry.to!Point;
      this.add(tmp, properties, decorators);
    }
    if(geometry.type == "MultiPoint") {
      auto tmp = geometry.to!MultiPoint;
      this.add(tmp, properties, decorators);
    }

    if(geometry.type == "LineString") {
      auto tmp = geometry.to!LineString;
      this.add(tmp, properties, decorators);
    }

    if(geometry.type == "MultiLineString") {
      auto tmp = geometry.to!MultiLineString;
      this.add(tmp, properties, decorators);
    }

    if(geometry.type == "Polygon") {
      auto tmp = geometry.to!Polygon;
      this.add(tmp, properties, decorators);
    }

    if(geometry.type == "MultiPolygon") {
      auto tmp = geometry.to!MultiPolygon;
      this.add(tmp, properties, decorators);
    }
  }

  void add(Bson geoBson, ref Json feature, FeatureDecorators decorators) {
    auto begin = Clock.currTime;
    import std.stdio;

    if(!this.isVisible(decorators)) {
      return;
    }

    PropertyTable properties;

    properties["_id"] = feature["_id"].to!string;

    if("featureId" in properties) {
      properties["_id"] = feature["featureId"].to!string;
    }

    properties["name"] = feature["name"].to!string;
    properties["visibility"] = feature["visibility"].to!string;

    if("canView" in feature) {
      properties["canView"] = feature["canView"].to!string;
    }

    if(feature["icons"].length > 0) {
      properties["icons"] = feature["icons"].byValue.map!(a => a.to!string).join(",");
      properties["primaryIcon"] = feature["icons"].byValue.front.to!string;
    } else {
      properties["icons"] = "";
      properties["primaryIcon"] = "";
    }

    if(geoBson["type"].get!string == "MultiPolygon") {
      addMultiPolygon(geoBson["coordinates"], properties, decorators);
    } else if(geoBson["type"].get!string == "Polygon") {
      auto tmp = geoBson["coordinates"].deserializeBson!(PositionT!double[][]);
      addPolygon(tmp, properties, decorators);
    } else {
      auto tmp = GeoJsonGeometry.fromBson(geoBson);
      add(tmp, properties, decorators);
    }
  }

  bool isVisible(FeatureDecorators decorators) nothrow {
    if(decorators.minZoom > path.z) {
      return false;
    }

    if(decorators.maxZoom < path.z) {
      return false;
    }

    return true;
  }

  void addLineMarkers(Position[] points, PropertyTable lineProperties, FeatureDecorators decorators) {
    auto lineLen = points.distance;
    auto sizeInPoints = (lineLen / bbox.dx) * 4096;

    if(sizeInPoints <= 3 && !decorators.keepWhenSmall) {
      return;
    }

    double interval = bbox.dx;

    if(lineLen < bbox.dx) {
      interval = lineLen / 2;
    } else if(lineLen < bbox.dx * 2) {
      interval = lineLen / 3;
    }

    PropertyTable properties;
    foreach(key, val; lineProperties) {
      properties[key] = val;
    }

    if("type" !in properties) {
      properties["type"] = "line";
    }

    properties["type"] ~= "Marker";

    foreach(coordinates; points.lineSplit(interval)) {
      auto tmp = Point(coordinates);
      add(tmp, properties, decorators, true);
    }
  }

  void addPolygonMarkers(ref Position[][] points, ref PropertyTable properties, ref FeatureDecorators decorators) {
    const polygonBox = BBox!double(points[0]);
    const area = polygonBox.area / bboxArea;
    const areaInPoints = area * 4096;

    if(areaInPoints <= 4 && !decorators.keepWhenSmall) {
      return;
    }

    if(areaInPoints <= 4) {
      auto tmp = Point(points[0].centroid);
      add(tmp, properties, decorators, true);
      return;
    }

    if(decorators.showAsLineAfterZoom <= path.z) {
      foreach (line; points) {
        auto tmp = LineString(line);
        add(tmp, properties, decorators);
      }

      return;
    }

    if(decorators.center.length == 2) {
      auto tmp = Point(points[0].centroid);
      add(tmp, properties, decorators, true);
    }
  }
}

/// Adding a line should be ignored when it is too short
unittest {
  FeatureDecorators decorators;
  decorators.keepWhenSmall = false;

  auto line = LineString([
    [ 13.450833004640478, 52.41379927147025 ],
    [ 13.451198285661233, 52.41313880961721 ]].toPositions);

  auto container = VectorTileProjection(ZXY(0,0,0), new TileData);
  PropertyTable properties;

  container.add(line, properties, decorators);
  container.tileData.points.groups.length.should.equal(0);
  container.tileData.points.groups.length.should.equal(0);
  container.tileData.lines.points.length.should.equal(0);
  container.tileData.polygons.points.length.should.equal(0);
}

/// Adding a point outside the zoom limit is ignored
unittest {
  FeatureDecorators decorators;
  decorators.keepWhenSmall = false;
  decorators.minZoom = 3;
  decorators.maxZoom = 10;

  auto point = Point(Position(1.5615606307983398, 41.38734606503736));
  PropertyTable properties;

  auto container1 = VectorTileProjection(ZXY(0,0,0), new TileData);
  container1.add(point, properties, decorators);
  container1.tileData.points.groups.length.should.equal(0);
  container1.tileData.points.groups.length.should.equal(0);

  auto container2 = VectorTileProjection(ZXY(16, 33052, 24477), new TileData);
  container2.add(point, properties, decorators);
  container2.tileData.points.groups.length.should.equal(0);
  container2.tileData.points.groups.length.should.equal(0);
}

/// Adding a line should not be ignored when the decorator keepWhenSmall is true
unittest {
  FeatureDecorators decorators;
  decorators.keepWhenSmall = true;

  auto line = LineString([
    [ 13.450833004640478, 52.41379927147025 ],
    [ 13.451198285661233, 52.41313880961721 ]].toPositions);

  auto container = VectorTileProjection(ZXY(0,0,0), new TileData);
  PropertyTable properties;

  container.add(line, properties, decorators);
  container.tileData.points.groups.length.should.equal(1);
  container.tileData.points.groups.length.should.equal(1);
  container.tileData.lines.points.length.should.equal(0);
  container.tileData.polygons.points.length.should.equal(0);
}

/// Adding a line should add the markers when showLineMarkers decorator is true
// unittest {
//   FeatureDecorators decorators;
//   decorators.showLineMarkers = true;

//   auto line = LineString([
//     [ 13.450833004640478, 52.41379927147025 ],
//     [ 13.451198285661233, 52.41313880961721 ]].toPositions);

//   auto container = VectorTileProjection(ZXY(20, 563466, 344395), new TileData);
//   PropertyTable properties;

//   container.add(line, properties, decorators);
//   container.tileData.points.groups.length.should.equal(1);
//   container.tileData.points.groups.length.should.equal(1);
//   container.tileData.lines.points.length.should.equal(1);
//   container.tileData.polygons.points.length.should.equal(0);
//}

/// Adding a polygon should be ignored when it is too small
unittest {
  FeatureDecorators decorators;
  decorators.keepWhenSmall = false;

  auto polygon = Polygon([[
    [ 13.450795412063597, 52.41391178145157 ],
    [ 13.45003366470337, 52.41324427990303 ],
    [ 13.45125675201416, 52.41309376345206 ],
    [ 13.451943397521973, 52.41368928205793 ],
    [ 13.450795412063597, 52.41391178145157 ]
  ]].toPositions);

  auto container = VectorTileProjection(ZXY(0,0,0), new TileData);
  PropertyTable properties;

  container.add(polygon, properties, decorators);
  container.tileData.points.groups.length.should.equal(0);
  container.tileData.points.groups.length.should.equal(0);
  container.tileData.lines.points.length.should.equal(0);
  container.tileData.polygons.points.length.should.equal(0);
}

/// Adding a polygon should not be ignored when the decorator keepWhenSmall is true
unittest {
  FeatureDecorators decorators;
  decorators.keepWhenSmall = true;

  auto polygon = Polygon([[
    [ 13.450795412063597, 52.41391178145157 ],
    [ 13.45003366470337, 52.41324427990303 ],
    [ 13.45125675201416, 52.41309376345206 ],
    [ 13.451943397521973, 52.41368928205793 ],
    [ 13.450795412063597, 52.41391178145157 ]
  ]].toPositions);

  auto container = VectorTileProjection(ZXY(0,0,0), new TileData);
  PropertyTable properties;

  container.add(polygon, properties, decorators);
  container.tileData.points.groups.length.should.equal(1);
  container.tileData.points.groups.length.should.equal(1);
  container.tileData.lines.points.length.should.equal(0);
  container.tileData.polygons.points.length.should.equal(0);
}

/// Adding a polygon should add the center point when is set
unittest {
  FeatureDecorators decorators;
  decorators.center = [ 13.450795412063597, 52.41391178145157 ];

  auto polygon = Polygon([[
    [ 13.450795412063597, 52.41391178145157 ],
    [ 13.45003366470337, 52.41324427990303 ],
    [ 13.45125675201416, 52.41309376345206 ],
    [ 13.451943397521973, 52.41368928205793 ],
    [ 13.450795412063597, 52.41391178145157 ]
  ]].toPositions);

  auto container = VectorTileProjection(ZXY(20, 563466, 344395), new TileData);
  PropertyTable properties;

  container.add(polygon, properties, decorators);
  container.tileData.points.groups.length.should.equal(1);
  container.tileData.points.groups.length.should.equal(1);
  container.tileData.lines.points.length.should.equal(0);
  container.tileData.polygons.points.length.should.equal(1);
}

/// Adding a polygon should add the center point when is not set
unittest {
  FeatureDecorators decorators;

  auto polygon = Polygon([[
    [ 13.450795412063597, 52.41391178145157 ],
    [ 13.45003366470337, 52.41324427990303 ],
    [ 13.45125675201416, 52.41309376345206 ],
    [ 13.451943397521973, 52.41368928205793 ],
    [ 13.450795412063597, 52.41391178145157 ]
  ]].toPositions);

  auto container = VectorTileProjection(ZXY(20, 563466, 344395), new TileData);
  PropertyTable properties;

  container.add(polygon, properties, decorators);
  container.tileData.points.groups.length.should.equal(0);
  container.tileData.points.groups.length.should.equal(0);
  container.tileData.lines.points.length.should.equal(0);
  container.tileData.polygons.points.length.should.equal(1);
}

/// Adding a polygon should add a line without the center when showAsLineAfterZoom matches the zoom level
// unittest {
//   FeatureDecorators decorators;
//   decorators.center = [ 13.450795412063597, 52.41391178145157 ];
//   decorators.showAsLineAfterZoom = 20;

//   auto polygon = Polygon([[
//     [ 13.450795412063597, 52.41391178145157 ],
//     [ 13.45003366470337, 52.41324427990303 ],
//     [ 13.45125675201416, 52.41309376345206 ],
//     [ 13.451943397521973, 52.41368928205793 ],
//     [ 13.450795412063597, 52.41391178145157 ]
//   ]].toPositions);

//   auto container = VectorTileProjection(ZXY(20, 563466, 344394), new TileData);
//   PropertyTable properties;

//   container.add(polygon, properties, decorators);
//   container.tileData.points.groups.length.should.equal(0);
//   container.tileData.points.groups.length.should.equal(0);
//   container.tileData.lines.points.length.should.equal(1);
//   container.tileData.polygons.points.length.should.equal(0);
// }

/// Adding a point near the tile bottom border it should match the same group
unittest {
  FeatureDecorators decorators;
  auto point = Point(Position(29.885559, 0.209481));

  auto container1 = VectorTileProjection(ZXY(3,4,3), new TileData);
  auto container2 = VectorTileProjection(ZXY(3,4,4), new TileData);
  PropertyTable properties;

  container1.add(point, properties, decorators);
  container2.add(point, properties, decorators);

  container1.tileData.points.groups.byKey.array.serializeToJson
    .should.equal(`[[13, 20]]`.parseJsonString);

  container2.tileData.points.groups.byKey.array.serializeToJson
    .should.equal(`[[13, 0]]`.parseJsonString);

  container1.tileData.points.properties.byKey.array.serializeToJson
    .should.equal(`[[2720, 4076]]`.parseJsonString);

  container2.tileData.points.properties.byKey.array.serializeToJson
    .should.equal(`[[2720,-20]]`.parseJsonString);
}

/// Adding a point near the tile right border it should match the same group
unittest {
  FeatureDecorators decorators;
  auto point = Point(Position(2.80849, 48.9505));

  auto container1 = VectorTileProjection(ZXY(8,129,87), new TileData);
  auto container2 = VectorTileProjection(ZXY(8,130,87), new TileData);
  PropertyTable properties;

  container1.add(point, properties, decorators);
  container2.add(point, properties, decorators);

  container1.tileData.points.groups.byKey.array.serializeToJson
    .should.equal(`[[20,19]]`.parseJsonString);

  container2.tileData.points.groups.byKey.array.serializeToJson
    .should.equal(`[[0,19]]`.parseJsonString);

  container1.tileData.points.properties.byKey.array.serializeToJson
    .should.equal(`[[4084,3971]]`.parseJsonString);

  container2.tileData.points.properties.byKey.array.serializeToJson
    .should.equal(`[[-12,3971]]`.parseJsonString);
}

/// Adding 3 points at a distance more than 20 points should not be clustered
unittest {
  FeatureDecorators decorators;

  auto point1 = Point(Position(-73.97648,  40.786248));
  auto point2 = Point(Position(-73.976428, 40.780654));
  auto point3 = Point(Position(-73.974935, 40.783024));

  auto container1 = VectorTileProjection(ZXY(12,1206,1539), new TileData);
  auto container2 = VectorTileProjection(ZXY(12,1206,1538), new TileData);
  PropertyTable properties;

  container1.add(point1, properties, decorators);
  container1.add(point2, properties, decorators);
  container1.add(point3, properties, decorators);

  container2.add(point1, properties, decorators);
  container2.add(point2, properties, decorators);
  container2.add(point3, properties, decorators);

  container1.tileData.points.groups.byKey.array.serializeToJson
    .should.equal(`[[7,-1], [6,-2], [6,0]]`.parseJsonString);

  container1.tileData.points.properties.byKey.array.serializeToJson
    .should.equal(`[[1280,-7], [1350,-153], [1278,-352]]`.parseJsonString);


  container2.tileData.points.groups.byKey.array.serializeToJson
    .should.equal(`[[6,20], [7,19], [6,18]]`.parseJsonString);

  container2.tileData.points.properties.byKey.array.serializeToJson
    .should.equal(`[[1278,3744], [1350,3943], [1280,4089]]`.parseJsonString);
}

/// points on 2 neigbouring tiles example 1
unittest {
  FeatureDecorators decorators;

  auto point1 = Point(Position(2.169512604976188, 41.38126108240903));
  auto point2 = Point(Position(1.560227, 41.38619));

  auto container1 = VectorTileProjection(ZXY(2,1,1), new TileData);
  auto container2 = VectorTileProjection(ZXY(2,2,1), new TileData);
  PropertyTable properties;

  container1.add(point1, properties, decorators);
  container1.add(point2, properties, decorators);

  container2.add(point1, properties, decorators);
  container2.add(point2, properties, decorators);

  container1.tileData.points.groups.byKey.array.serializeToJson
    .should.equal(`[[20,10]]`.parseJsonString);

  container1.tileData.points.properties.byKey.array.serializeToJson
    .should.equal(`[[4167,2023], [4194,2023]]`.parseJsonString);


  container2.tileData.points.groups.byKey.array.serializeToJson
    .should.equal(`[[0,10]]`.parseJsonString);

  container2.tileData.points.properties.byKey.array.serializeToJson
    .should.equal(`[[71,2023], [98,2023]]`.parseJsonString);
}

/// points on 2 neigbouring tiles example 2
unittest {
  FeatureDecorators decorators;

  auto point1 = Point(Position(1.560227, 41.38619));
  auto point2 = Point(Position(1.538130446090772, 41.43604353418604));
  auto point3 = Point(Position(1.538130446090772, 41.43604353418604));

  auto container1 = VectorTileProjection(ZXY(4,8,5), new TileData);
  auto container2 = VectorTileProjection(ZXY(4,8,6), new TileData);
  PropertyTable properties;

  container1.add(point1, properties, decorators);
  container1.add(point2, properties, decorators);
  container1.add(point3, properties, decorators);

  container2.add(point1, properties, decorators);
  container2.add(point2, properties, decorators);
  container2.add(point3, properties, decorators);

  point1.to!string.should.equal("Point([1.56023, 41.3862])");
  container1.tileData.points.groups.byKey.array.serializeToJson
    .should.equal(`[[1,20]]`.parseJsonString);

  container1.tileData.points.properties.byKey.array.serializeToJson
    .should.equal(`[[280,3985], [284,3997]]`.parseJsonString);


  container2.tileData.points.groups.byKey.array.serializeToJson
    .should.equal(`[[1,0]]`.parseJsonString);

  container2.tileData.points.properties.byKey.array.serializeToJson
    .should.equal(`[[284,-99], [280,-111]]`.parseJsonString);
}
