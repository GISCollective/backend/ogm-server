/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.tiles.collections.cluster;

import ogm.models.tileLayer;
import geo.vector;
import geo.proto;

import vibe.http.server;
import vibe.data.json;

import std.range;
import std.algorithm;
import std.conv;
import std.math;

import gis_collective.hmq.log;

version(unittest) {
  import fluent.asserts;
}

///
struct ClusterCollection {

  ///
  enum layerSize = 4096;

  ///
  struct Group {
    size_t count;
    size_t pendingCount;
    string[] idList;
  }

  ///
  uint groupGrid = 20;

  ///
  long groupSize;

  ///
  bool ignoreGrouping;

  Group[int[2]] groups;
  PropertyTable[int[2]] properties;

  ///
  int[2] toGroupKey(IntPosition position) {
    double[2] dblPosition;
    int[2] newPosition;

    if(groupSize <= 1) {
      newPosition[0] = position[0];
      newPosition[1] = position[1];
      return newPosition;
    }

    long xRest = position[0] / layerSize;
    long yRest = position[1] / layerSize;

    dblPosition[0] = (xRest * groupGrid) + cast(double) (position[0] % layerSize) / cast(double) groupSize;

    if(position[1] < 0) {
      yRest--;
      dblPosition[1] = (yRest * groupGrid) + ((cast(double) layerSize + cast(double) position[1]) % layerSize) / cast(double) groupSize;
    } else {
      dblPosition[1] = (yRest * groupGrid) + cast(double) (position[1] % layerSize) / cast(double) groupSize;
    }

    newPosition[0] = round(dblPosition[0]).to!int;
    newPosition[1] = round(dblPosition[1]).to!int;

    return newPosition;
  }

  ///
  int[2] updateGroupPosition(IntPosition position) {
    int[2] newPosition;

    newPosition[0] = position[0] * 4 + 2;
    newPosition[1] = position[1] * 4 + 2;

    if(position[0] % 2 == 0) {
      newPosition[1]++;
    } else {
      newPosition[1]--;
    }

    return newPosition;
  }

  ///
  void add(int[] position, PropertyTable properties, bool ignoreIfCluster = false) {
    auto groupKey = toGroupKey(position);

    if(ignoreIfCluster && groupKey in groups) {
      return;
    }

    if(ignoreGrouping && groupKey in groups) {
      return;
    }

    int[2] fixedPosition = [position[0], position[1]];
    this.properties[fixedPosition] = properties;

    if(groupKey !in groups) {
      groups[groupKey] = Group();
    }

    bool shouldAdd = true;

    bool canView = "canView" in properties && properties["canView"] == "true";
    bool isPending = "visibility" in properties && properties["visibility"] == "-1";

    if(isPending && !canView) {
      shouldAdd = false;
    }

    groups[groupKey].count++;
    if(groups[groupKey].count < 5 && "_id" in properties && shouldAdd) {
      groups[groupKey].idList ~= properties["_id"];
    }

    if(isPending) {
      groups[groupKey].pendingCount++;
    }
  }

  ///
  void flushJson(HTTPServerResponse response) {
    string glue;

    response.bodyWriter.write(`"points":[`);
    foreach(position, propertyTable; properties) {
      auto groupKey = toGroupKey(position);

      if(groups[groupKey].count == 1) {
        response.bodyWriter.write(glue);
        response.bodyWriter.write(`{"key": {"x": `);
        response.bodyWriter.write(position[0].to!string);
        response.bodyWriter.write(`,"y":`);
        response.bodyWriter.write(position[1].to!string);
        response.bodyWriter.write(`},"properties":`);
        response.bodyWriter.write(propertyTable.serializeToJsonString);
        response.bodyWriter.write(`}`);

        glue = ",";
      }
    }

    response.bodyWriter.write(`],"groups":[`);
    glue = "";
    foreach(position, group; groups) {
      if(groups[position].count > 1) {

        auto groupPosition = updateGroupPosition(position);

        response.bodyWriter.write(glue);
        response.bodyWriter.write(`{"key": {"x": `);
        response.bodyWriter.write(groupPosition[0].to!string);
        response.bodyWriter.write(`,"y":`);
        response.bodyWriter.write(groupPosition[1].to!string);
        response.bodyWriter.write(`},"properties":{ "count": "`);
        response.bodyWriter.write(group.count.to!string);
        response.bodyWriter.write(`","pendingCount":"`);
        response.bodyWriter.write(group.pendingCount.to!string);
        response.bodyWriter.write(`","idList":"`);
        response.bodyWriter.write(group.idList.join(","));
        response.bodyWriter.write(`"}}`);

        glue = ",";
      }
    }


    response.bodyWriter.write(`]`);
  }

  ///
  void flushVt(HTTPServerResponse response) {
    auto data = toLayer.toProtoBuf.array;

    response.bodyWriter.write(ProtoKey(3, WireType.fixedLength).value.raw);
    response.bodyWriter.write(Varint(data.length).raw);
    response.bodyWriter.write(data);

    data = toClusterLayer.toProtoBuf.array;

    response.bodyWriter.write(ProtoKey(3, WireType.fixedLength).value.raw);
    response.bodyWriter.write(Varint(data.length).raw);
    response.bodyWriter.write(data);
  }

  ///
  Layer toLayer() {
    Layer layer;
    layer.name = "points";
    layer.extent = layerSize;

    foreach(position, propertyTable; properties) {
      auto groupKey = toGroupKey(position);

      if(groups[groupKey].count == 1 || groupSize <= 1) {
        VectorFeature feature;
        feature.type = VectorFeature.GeomType.point;
        feature.geometry = Geometry.fromPoint(position);

        layer.features ~= feature;

        foreach(key, value; propertyTable) {
          layer.addProperty(key, value);
        }
      }
    }

    return layer;
  }

  ///
  Layer toClusterLayer() {
    Layer groupLayer;
    groupLayer.name = "groups";
    groupLayer.extent = groupGrid * 4;

    if(groupSize <= 1) {
      return groupLayer;
    }

    foreach(position, group; groups) {
      if(group.count > 1) {
        VectorFeature feature;
        feature.type = VectorFeature.GeomType.point;
        feature.geometry = Geometry.fromPoint(updateGroupPosition(position));

        auto totalFeatures = group.count + group.count;

        groupLayer.features ~= feature;
        groupLayer.addProperty("count", group.count);
        groupLayer.addProperty("pendingCount", group.pendingCount);
        groupLayer.addProperty("level", totalFeatures.toGroupLevel);
        groupLayer.addProperty("idList", group.idList.sort.join(","));
      }
    }

    return groupLayer;
  }
}

/// adding a feature with "canView" = true should be added to the id list
unittest {
  ClusterCollection collection;
  int[2] position = [0, 0];
  PropertyTable properties;
  properties["canView"] = "true";
  properties["visibility"] = "-1";
  properties["_id"] = "id";

  collection.add(position, properties);

  collection.groups.length.should.equal(1);
  collection.groups[position].count.should.equal(1);
  collection.groups[position].pendingCount.should.equal(1);
  collection.groups[position].idList.should.equal(["id"]);
}

/// adding a feature with "visibility" = 0 should be added to the id list
unittest {
  ClusterCollection collection;
  int[2] position = [0, 0];
  PropertyTable properties;
  properties["_id"] = "id";
  properties["visibility"] = "0";

  collection.add(position, properties);

  collection.groups.length.should.equal(1);
  collection.groups[position].count.should.equal(1);
  collection.groups[position].pendingCount.should.equal(0);
  collection.groups[position].idList.should.equal(["id"]);
}

/// adding a feature with "visibility" = 1 should be added to the id list
unittest {
  ClusterCollection collection;
  int[2] position = [0, 0];
  PropertyTable properties;
  properties["_id"] = "id";
  properties["visibility"] = "1";

  collection.add(position, properties);

  collection.groups.length.should.equal(1);
  collection.groups[position].count.should.equal(1);
  collection.groups[position].pendingCount.should.equal(0);
  collection.groups[position].idList.should.equal(["id"]);
}

/// adding a feature with "visibility" = -1 should not be added to the id list
unittest {
  ClusterCollection collection;
  int[2] position = [0, 0];
  PropertyTable properties;
  properties["visibility"] = "-1";
  properties["_id"] = "id";

  collection.add(position, properties);

  collection.groups.length.should.equal(1);
  collection.groups[position].count.should.equal(1);
  collection.groups[position].pendingCount.should.equal(1);
  collection.groups[position].idList.length.should.equal(0);
}

/// adding a feature twice with ignoreIfCluster true does not create a cluster
unittest {
  ClusterCollection collection;
  int[2] position = [0, 0];
  PropertyTable properties;
  properties["canView"] = "true";
  properties["_id"] = "id";

  collection.add(position, properties, true);
  collection.add(position, properties, true);

  collection.groups.length.should.equal(1);
  collection.groups[position].count.should.equal(1);
  collection.groups[position].pendingCount.should.equal(0);
  collection.groups[position].idList.should.equal(["id"]);
}

ulong toGroupLevel(ulong count) {
  if (count > 0 && count < 10) {
    return 0;
  }

  if (count >= 10 && count < 30) {
    return 1;
  }

  if (count >= 30 && count < 50) {
    return 2;
  }

  if (count >= 50 && count < 100) {
    return 3;
  }

  if (count >= 100 && count < 200) {
    return 4;
  }

  if (count >= 200 && count < 500) {
    return 5;
  }

  if (count >= 500 && count < 1000) {
    return 6;
  }

  if (count >= 1000) {
    return 7;
  }

  if (count >= 2000) {
    return 8;
  }

  return 9;
}