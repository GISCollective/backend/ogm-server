/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.tiles.api;

import ogm.middleware.GlobalAccess;
import vibe.http.router;
import crate.http.router;
import crate.base;
import crate.error;

import ogm.auth;
import ogm.crates.all;

import ogm.filter.indirectvisibility;
import ogm.middleware.userdata;
import ogm.middleware.adminrequest;
import ogm.features.middlewares.mapper;
import ogm.features.middlewares.FeatureMaskMapper;
import ogm.tiles.tiles;
import ogm.tiles.TileJson;
import ogm.middleware.features.GetFeaturesFilter;
import ogm.tiles.operations.MapStyle;
import ogm.icons.TaxonomyTree;
import ogm.icons.IconCache;
import ogm.middleware.IconFieldMapper;

///
void setupTilesApi(T)(CrateRouter!T crateRouter, OgmCrates crates, string apiUrl, string frontendUrl) {
  TileJsonSettings.apiUrl = apiUrl;
  TileJsonSettings.frontendUrl = frontendUrl;

  auto cache = new IconCache(&crates.icon.get, &crates.iconSet.getItem);
  auto iconTree = new TaxonomyTree(&crates.icon.get, &crates.iconSet.getItem, cache);

  auto auth = Authentication.instance(crates.user);
  auto userDataMiddleware = UserDataMiddleware.instance(crates);
  auto adminRequest = new AdminRequest(crates.user);

  auto featuresFilter = new GetFeaturesFilter(auth.getCollection, crates, iconTree);
  auto featureMapper = new FeatureMapper(crates);
  auto featureMaskMapper = new FeatureMaskMapper(crates);
  auto iconFieldMapper = new IconFieldMapper(crates);

  featureMapper.ignoreIssues = true;
  featureMapper.keepCanViewFlag = true;

  auto tiles = new Tiles(crates, featuresFilter, featureMapper, featureMaskMapper, iconFieldMapper);
  auto tileJson = new TileJson(crates);

  crateRouter.router.get("/tiles/*", &auth.oAuth2.permisiveAuth);
  crateRouter.router.get("/tiles/*", &adminRequest.any);
  crateRouter.router.get("/tiles/*", &userDataMiddleware.any);

  crateRouter.router.get("/tiles/about.json", requestErrorHandler(&tileJson.get));
  crateRouter.router.match(HTTPMethod.OPTIONS, "/tiles/about.json", requestErrorHandler(&tileJson.options));

  crateRouter.router.get("/tiles/:z/:x/:y", requestErrorHandler(&tiles.get));
  crateRouter.router.match(HTTPMethod.OPTIONS, "/tiles/:z/:x/:y", requestErrorHandler(&tiles.options));

  crateGetters["Team"] = &crates.team.getItem;
  crateGetters["Picture"] = &crates.picture.getItem;
  crateGetters["IconSet"] = &crates.iconSet.getItem;

  auto globalAccess = GlobalAccessMiddleware.instance(crates);

  auto mapStyle = new MapStyle(crates);

  crateRouter
    .prepare(crates.map)
    .withCustomOperation(mapStyle)
    .and(auth.publicDataMiddleware)
    .and(adminRequest)
    .and(userDataMiddleware)
    .and(globalAccess);
}
