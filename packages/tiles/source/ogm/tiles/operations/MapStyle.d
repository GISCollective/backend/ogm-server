/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.tiles.operations.MapStyle;

import vibe.http.router;
import vibe.data.json;
import vibe.core.core;

import crate.http.operations.base;
import crate.base;
import crate.error;

import ogm.http.request;
import ogm.crates.all;
import ogm.models.mapFile;
import ogm.http.request;
import ogm.rights;
import ogm.vt;

import std.exception;
import std.functional;
import std.datetime;
import std.algorithm;
import std.uuid;
import std.conv;
import std.path;
import std.range;

import gis_collective.hmq.broadcast.base;
import geo.style;
import ogm.tiles.TileJson;
import vibe.http.client;
import vibe.core.log;
import vibe.stream.operations;

class MapStyle: CrateOperation!DefaultStorage {
  protected {
    OgmCrates crates;
  }

  this(OgmCrates crates) {
    this.crates = crates;

    CrateRule rule;

    rule.request.path = "/tiles/:id/style.json";
    rule.request.method = HTTPMethod.GET;

    super(crates.map, rule);
  }

  void addRasterBaseMap(ref MapboxStyle style, string name, string[] tiles, long size, string attribution, int min = -1, int max = -1) {
    style.sources[name] = Json.emptyObject;
    style.sources[name]["type"] = "raster";
    style.sources[name]["attribution"] = attribution;
    style.sources[name]["tileSize"] = size;
    style.sources[name]["tiles"] = tiles.serializeToJson;

    MapBoxLayer layer;
    layer.id = name ~ "_raster";
    layer.type = MapBoxLayer.Type.raster;
    layer.source = name;

    if(min >= 0) {
      layer.minzoom = min;
    }

    if(max > 0) {
      layer.maxzoom = max;
    }

    style.layers ~= layer;
  }

  BaseMap getBaseMap(string mapId) {
    string[] baseMapList;

    if(mapId != "_") {
      auto mapRange = crates.map.get.where("_id").equal(ObjectId.fromString(mapId)).and.limit(1).exec;

      if(!mapRange.empty) {
        baseMapList = mapRange.front["baseMaps"]["list"].deserializeJson!(string[]);
      }
    }

    InputRange!Json baseMapRange;
    if(baseMapList.length) {
      baseMapRange = crates.baseMap.get
        .where("_id")
        .equal(ObjectId.fromString(baseMapList[0])).and
        .limit(1)
        .exec;
    } else {
      baseMapRange = crates.baseMap.get
        .where("visibility.isDefault")
        .equal(true).and
        .sort("defaultOrder", 1)
        .limit(1)
        .exec;
    }

    if(baseMapRange.empty) {
      return BaseMap();
    }

    return LazyBaseMap(baseMapRange.front, (&itemResolver).toDelegate).toType;
  }

  void addOSMBaseMap(ref MapboxStyle style, MapLayer layer) {
    int min = -1;
    int max = -1;

    if("visibility" in layer.options && layer.options["visibility"].type == Json.Type.array && layer.options["visibility"].length == 2) {
      min = layer.options["visibility"][0].to!int;
      max = layer.options["visibility"][1].to!int;
    }

    addRasterBaseMap(style, "osm", [
        "https://a.tile.openstreetmap.org/{z}/{x}/{y}.png",
        "https://b.tile.openstreetmap.org/{z}/{x}/{y}.png",
        "https://c.tile.openstreetmap.org/{z}/{x}/{y}.png"
      ], 256, `<a href="https://www.openstreetmap.org/copyright">© OpenStreetMap contributors</a>`, min, max);
  }

  void addStamenBaseMap(ref MapboxStyle style, MapLayer layer) {
    int min = -1;
    int max = -1;

    if("visibility" in layer.options && layer.options["visibility"].type == Json.Type.array && layer.options["visibility"].length == 2) {
      min = layer.options["visibility"][0].to!int;
      max = layer.options["visibility"][1].to!int;
    }

    if(layer.options["layer"] == "toner") {
      addRasterBaseMap(style, "stamen_toner", ["https://stamen-tiles.a.ssl.fastly.net/toner/{z}/{x}/{y}.png"], 256,
        `Map tiles by <a href="http://stamen.com">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>. Data by <a href="http://openstreetmap.org">OpenStreetMap</a>, under <a href="http://www.openstreetmap.org/copyright">ODbL</a>.`,
        min, max);
    }

    if(layer.options["layer"] == "terrain") {
      addRasterBaseMap(style, "stamen_terrain", ["https://stamen-tiles.a.ssl.fastly.net/terrain/{z}/{x}/{y}.jpg"], 256,
        `Map tiles by <a href="http://stamen.com">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>. Data by <a href="http://openstreetmap.org">OpenStreetMap</a>, under <a href="http://www.openstreetmap.org/copyright">ODbL</a>.`,
        min, max);
    }

    if(layer.options["layer"] == "watercolor") {
      addRasterBaseMap(style, "stamen_watercolor", ["https://stamen-tiles.a.ssl.fastly.net/watercolor/{z}/{x}/{y}.jpg"], 256,
        "Map tiles by <a href=\"http://stamen.com\">Stamen Design</a>, under <a href=\"http://creativecommons.org/licenses/by/3.0\">CC BY 3.0</a>. Data by <a href=\"http://openstreetmap.org\">OpenStreetMap</a>, under <a href=\"http://creativecommons.org/licenses/by-sa/3.0\">CC BY SA</a>.",
        min, max);
    }
  }

  void addRasterBaseMap(ref MapboxStyle style, string name, MapLayer layer, string attribution) {
    string url = layer.options["url"].to!string;
    int min = -1;
    int max = -1;

    if("visibility" in layer.options && layer.options["visibility"].type == Json.Type.array && layer.options["visibility"].length == 2) {
      min = layer.options["visibility"][0].to!int;
      max = layer.options["visibility"][1].to!int;
    }

    addRasterBaseMap(style, name, [ url ], 256, attribution, min, max);
  }

  void loadMapBoxBaseMap(ref MapboxStyle style, MapLayer layer) {
    auto jsonResponse = downloadMapBoxBaseMap(layer.serializeToJson);

    addMapBoxBaseMap(style, jsonResponse);
  }

  void addMapBoxBaseMap(ref MapboxStyle style, Json remoteStyle) {
    auto remoteStyleDecoded = remoteStyle.deserializeJson!MapboxStyle;

    foreach (string key, source; remoteStyleDecoded.sources) {
      style.sources[key] = source;
    }

    style.layers ~= remoteStyleDecoded.layers;
  }

  void addBaseMap(ref MapboxStyle style, string mapId) {
    auto baseMap = getBaseMap(mapId);

    string attribution = baseMap.attributions.map!(a => `<a href="` ~ a.url ~ `">` ~ a.name ~ `</a>`).join(" ");

    foreach (size_t i, layer; baseMap.layers) {
      if(layer.type == "Open Street Maps") {
        addOSMBaseMap(style, layer);
      }

      if(layer.type == "XYZ") {
        addRasterBaseMap(style, baseMap.name ~ "_" ~ i.to!string, layer, attribution);
      }

      if(layer.type == "Stamen") {
        addStamenBaseMap(style, layer);
      }

      if(layer.type == "MapBox") {
        loadMapBoxBaseMap(style, layer);
      }
    }
  }

  ///
  override void handle(DefaultStorage storage) {
    auto itemId = getPathVar!"id"(storage.request.requestPath, rule.request.path);

    auto res = storage.response;
    auto req = storage.request;

    if(res.headerWritten) {
      return;
    }

    if(itemId != "_") {
      storage.item = this.prepareItemOperation!"getItem"(storage);
      validateMapAccess(crates, storage.item, req);
    }

    auto resolver = toDelegate(&itemResolver);

    auto map = LazyMap(storage.item, resolver).toType;

    MapboxStyle style;

    style.name = map.name;
    style.version_ = 8;
    style.zoom = 1;
    style.sprite = buildPath(TileJsonSettings.apiUrl, "maps", itemId, "sprite");
    style.glyphs = buildPath(TileJsonSettings.apiUrl, "fonts/{fontstack}/{range}.pbf");
    style.sources[itemId] = Json.emptyObject;
    style.sources[itemId]["type"] = "vector";

    if(itemId != "_") {
      style.sources[itemId]["url"] = buildPath(TileJsonSettings.apiUrl, "tiles/about.json?map=" ~ itemId);
    } else {
      style.sources[itemId]["url"] = buildPath(TileJsonSettings.apiUrl, "tiles/about.json");
    }

    this.addBaseMap(style, itemId);

    MapBoxLayer pointsLayer;
    pointsLayer.id = itemId ~ "_Points";
    pointsLayer.type = MapBoxLayer.Type.symbol;
    pointsLayer.source = itemId;
    pointsLayer.sourceLayer = "points";
    pointsLayer.layout.iconAllowOverlap = false;
    pointsLayer.layout.iconIgnorePlacement = false;
    pointsLayer.layout.iconImage = "{primaryIcon}";
    pointsLayer.layout.iconSize.base = 0.8;
    pointsLayer.layout.textAnchor = MapBoxLayer.SymbolAnchor.top;
    pointsLayer.layout.textField = "{name}";
    pointsLayer.layout.textOffset.base = [0, 1];
    pointsLayer.layout.textSize.base = 12;

    pointsLayer.paint.textColor.base = "rgba(0, 0, 0, 1)";
    pointsLayer.paint.textTranslateAnchor = MapBoxLayer.Anchor.map,
    pointsLayer.paint.textHaloColor.base = "rgba(255, 255, 255, 1)",
    pointsLayer.paint.textHaloWidth.base = 1;

    MapBoxLayer groupsLayer;
    groupsLayer.id = itemId ~ "_Groups";
    groupsLayer.type = MapBoxLayer.Type.symbol;
    groupsLayer.source = itemId;
    groupsLayer.sourceLayer = "groups";
    groupsLayer.layout.iconAllowOverlap = true;
    groupsLayer.layout.iconIgnorePlacement = true;
    groupsLayer.layout.iconImage = "group-{level}";
    groupsLayer.layout.iconOptional = true;
    groupsLayer.layout.iconSize.base = 0.7;
    groupsLayer.layout.iconAllowOverlap = false;

    groupsLayer.layout.textAllowOverlap = false;
    groupsLayer.layout.textField = "{count}";
    groupsLayer.layout.textIgnorePlacement = false;
    groupsLayer.layout.textLineHeight.base = 1;
    groupsLayer.layout.textOptional = true;
    groupsLayer.layout.textPadding.base = 0;
    groupsLayer.layout.textSize.base = 10;

    groupsLayer.paint.textColor.base = "rgba(255, 0, 0, 1)";

    style.layers ~= pointsLayer;
    style.layers ~= groupsLayer;

    res.writeJsonBody(style.serializeToJson, 200);
  }
}
