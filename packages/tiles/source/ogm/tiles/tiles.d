/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.tiles.tiles;

import geo.vector;
import geo.xyz;
import geo.json;
import geo.conv;
import geo.svg.vectorTiles;
import geo.style;
import geo.algorithm;
import geo.geometries;
import geo.epsg;

import ogm.crates.all;
import vibe.http.router;
import vibe.data.json;
import vibe.data.bson;
import vibe.core.log;

import std.array;
import std.conv;
import std.algorithm;
import std.range;
import std.stdio;
import std.datetime;

import ogm.filter.visibility;
import ogm.features.middlewares.mapper;
import ogm.features.middlewares.FeatureMaskMapper;
import ogm.middleware.features.GetFeaturesFilter;
import ogm.middleware.IconFieldMapper;
import ogm.models.tileLayer;

import ogm.tiles.collections.tileData;
import ogm.tiles.collections.vectorTileProjection;

import crate.base;
import crate.http.cors;
import crate.http.queryParams;
import crate.http.request;
import cachetools.cachelru;
import ogm.filter.mapFilter;
import core.memory;
import vibe.core.core;


version(unittest) {
  import fluent.asserts;
}

struct TileOptions {
  string format = "json";
  VisibilityParameters visibilityParams;
  MapFilter.Parameters mapFilterParams;

  uint z;
  uint x;
  uint y;

  static CacheLRU!(string, Json) cache;
  OgmCrates crates;

  static this() {
    this.cache = new CacheLRU!(string, Json);
    cache.size = 2048;      // keep 2048 elements in cache
    cache.ttl = 10.seconds; // set 60 seconds TTL for items in cache
  }

  this(T, U)(T query, U params, OgmCrates crates) {
    this.crates = crates;

    if("format" in query) {
      format = query["format"];
    }

    z = params["z"].to!uint;
    x = params["x"].to!uint;
    y = params["y"].to!uint;

    visibilityParams = query.parseQueryParams!(VisibilityParameters);
    mapFilterParams = query.parseQueryParams!(MapFilter.Parameters);

    if(mapFilterParams.map == "") {
      mapFilterParams.onMainMap = true;
    }
  }

  ClusterMode cluster() {
    if(mapFilterParams.onMainMap) {
      return ClusterMode.hexagons;
    }

    auto tmp = cache.get(mapFilterParams.map);
    Json cluster;

    if(tmp.isNull) {
      const map = this.crates.map.getItem(mapFilterParams.map).withProjection([ "cluster" ]).and.exec.front;

      cluster = map["cluster"];

      cache.put(mapFilterParams.map, cluster);
    } else {
      cluster = tmp.get;
    }

    if(cluster.type != Json.Type.object) {
      return ClusterMode.hexagons;
    }

    if(cluster["mode"].type != Json.Type.int_) {
      return ClusterMode.hexagons;
    }

    return cluster["mode"].deserializeJson!ClusterMode;
  }

  bool onMainMap() {
    return mapFilterParams.onMainMap;
  }

  string map() {
    return mapFilterParams.map;
  }

  auto zxy() {
    return ZXY(z, x, y);
  }
}

class Tiles {
  private {
    OgmCrates crates;
    GetFeaturesFilter featuresFilter;
    FeatureMapper mapper;
    VisibilityFilter!("features", true, true) visibility;
    FeatureMaskMapper maskMapper;
    MapFilter mapFilter;
    IconFieldMapper iconMapper;
  }

  this(OgmCrates crates, GetFeaturesFilter filter, FeatureMapper mapper, FeatureMaskMapper maskMapper, IconFieldMapper iconMapper) {
    this.crates = crates;
    this.featuresFilter = filter;
    this.mapper = mapper;
    this.iconMapper = iconMapper;
    this.maskMapper = maskMapper;
    this.visibility = new VisibilityFilter!("features", true, true)(crates, crates.feature);
    this.mapFilter = new MapFilter(crates);
  }

  ///
  void options(HTTPServerRequest req, HTTPServerResponse response) {
    response.addHeaderValue("Access-Control-Allow-Origin", ["*"]);
    response.addHeaderValue("Access-Control-Allow-Methods", ["GET"]);
    response.addHeaderValue("Access-Control-Allow-Headers", ["Content-Type", "Authorization"]);

    response.writeBody("", 204);
  }

  auto getFeatureSelector(ref TileOptions options, HTTPServerRequest req) {
    auto selector = crates.feature.get;

    auto mapFilterParams = req.query.parseQueryParams!(MapFilter.Parameters);

    debug auto begin = Clock.currTime;

    selector = visibility.get(selector, options.visibilityParams, req);
    debug writeln("visibility exec in ", Clock.currTime - begin);

    selector = mapFilter.get(selector, mapFilterParams, req);
    debug writeln("mapFilter exec in ", Clock.currTime - begin);

    selector = featuresFilter.getWrapper(selector, req);
    debug writeln("featuresFilter exec in ", Clock.currTime - begin);

    return selector;
  }

  auto getSimpleFeatureSelector(ref TileOptions options, HTTPServerRequest req) {
    auto selector = crates.simpleFeature.get;

    auto mapFilterParams = req.query.parseQueryParams!(MapFilter.Parameters);

    debug auto begin = Clock.currTime;

    selector = visibility.get(selector, options.visibilityParams, req);
    debug writeln("visibility exec in ", Clock.currTime - begin);

    selector = mapFilter.get(selector, mapFilterParams, req);
    debug writeln("mapFilter exec in ", Clock.currTime - begin);

    selector = featuresFilter.getWrapper(selector, req);
    debug writeln("featuresFilter exec in ", Clock.currTime - begin);

    selector.where("z").equal(options.z).and
            .where("x").equal(options.x).and
            .where("y").equal(options.y);

    return selector;
  }

  auto applyMapper(HTTPServerRequest req, ref Json item, bool isPositionField) {
    this.visibility.mapper(req, item);
    this.visibility.canViewMapper(req, item);

    if(isPositionField) {
      this.maskMapper.maskMapper(req, item);
    }

    this.iconMapper.mapper(req, item);
    this.mapper.mapper(req, item);
  }

  FeatureDecorators decorators(ref Json item) {
    FeatureDecorators decorators;

    if(item["decorators"].type == Json.Type.object) {
      decorators = item["decorators"].deserializeJson!FeatureDecorators;
    }

    return decorators;
  }

  ///
  void get(HTTPServerRequest req, HTTPServerResponse res) {
    enum keys = ["_id", "name", "unmasked", "icons", "maps", "visibility", "computedVisibility"];

    GC.disable();
    scope(exit) {
      GC.enable;
    }

    auto begin = Clock.currTime;
    auto tmpBegin = begin;

    auto options = TileOptions(req.query, req.params, crates);

    string positionField = "position";

    if(!options.onMainMap) {
      positionField = featuresFilter.getPositionField(req);
    }

    double[][][] polygonBuffer = [[
      [0, 0],
      [0, 0],
      [0, 0],
      [0, 0],
      [0, 0],
    ]];

    polygonBuffer[0].fillTilePolygon(options.z, options.x, options.y, 0.2);

    auto pointsSelector = getFeatureSelector(options, req);
    pointsSelector.where(positionField ~ "Box").intersectsPolygon(polygonBuffer);
    // pointsSelector.where(positionField ~ ".type").anyOf(["Point", "MultiPoint"]);
    pointsSelector.where("isLarge").not.equal(true);
    pointsSelector.withProjection([ "_id", "name", "position", "unmasked", "icons", "maps", "visibility", "computedVisibility" ]);

    auto features = pointsSelector.execBson.array;

    version(unittest) {} else
    debug writeln("Query exec in ", Clock.currTime - tmpBegin);

    TileData tileData;

    if(options.cluster == ClusterMode.none) {
      tileData = new UnclusteredTileData;
    } else {
      tileData = new TileData;
    }

    debug writeln("tile data init in ", Clock.currTime - tmpBegin);

    auto container = VectorTileProjection(options.zxy, tileData);

    debug writeln("container init in ", Clock.currTime - tmpBegin);

    const isPositionField = positionField != "position";

    foreach(rawItem; features) {
      auto item = Json.emptyObject;
      static foreach(key; keys) {
        item[key] = rawItem[key].toJson;
      }

      try {
        this.applyMapper(req, item, isPositionField);
        auto decorators = this.decorators(item);

        container.add(rawItem["position"], item, decorators);
      } catch(Exception e) {
        logError(e.message);
      }

      debug writeln("raw features added in ", Clock.currTime - tmpBegin);
    }

    debug writeln("all points added in ", Clock.currTime - tmpBegin);


    // if(!options.onMainMap) {
    //   auto notPointSelector = getSimpleFeatureSelector(options, req);
    //   notPointSelector.withProjection([ "_id", "featureId", "position", "name", "unmasked", "icons", "maps", "visibility", "computedVisibility" ]);

    //   auto notPoints = notPointSelector.execBson;

    //   foreach(rawItem; notPoints) {
    //     auto item = Json.emptyObject;
    //     static foreach(key; keys) {
    //       item[key] = rawItem[key].toJson;
    //     }

    //     try {
    //       this.applyMapper(req, item, isPositionField);
    //       auto decorators = this.decorators(item);

    //       container.add(rawItem["position"], item, decorators);
    //     } catch(Exception e) {
    //       logError(e.message);
    //     }
    //   }
    // }

    if(options.format == "vt") {
      tileData.flushVt(res);
    } else {
      tileData.flushJson(res);
    }

    version(unittest) {} else
    debug writeln( options.z, "/", options.x, "/", options.y, " TILE GENERATED in ", Clock.currTime - begin);
  }
}
