/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tile.style;

import tests.fixtures;

import std.algorithm;
import std.array;
import std.file;
import std.path;
import ogm.tiles.operations.MapStyle;

import vibe.data.json;
import geo.style;
import geo.vector;
import ogm.tiles.api;

alias suite = Spec!({
  URLRouter router;

  describe("GET the `style.json` file", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupTilesApi(crates, "http://localhost", "http://frontend");
    });

    it("sets the map name as the style name", {
      auto map = crates.map.getItem("000000000000000000000001").and.exec.front;
      map["name"] = "map name";
      crates.map.updateItem(map);

      router
        .request
        .get("/tiles/000000000000000000000001/style.json")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["name"].should.equal("map name");
        });
    });

    it("sets the map sprite as the style sprite", {
      router
        .request
        .get("/tiles/000000000000000000000001/style.json")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["sprite"].should.equal("http://localhost/maps/000000000000000000000001/sprite");
        });
    });

    it("sets the mapbox glyphs as the style glyphs", {
      router
        .request
        .get("/tiles/000000000000000000000001/style.json")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["glyphs"].should.equal("http://localhost/fonts/{fontstack}/{range}.pbf");
        });
    });

    it("sets the layer styles", {
      router
        .request
        .get("/tiles/000000000000000000000001/style.json")
        .expectStatusCode(200)
        .end((Response response) => () {
          auto layers = response.bodyJson["layers"];

          layers.length.should.equal(2);
          layers[0]["id"].should.equal("000000000000000000000001_Points");
          layers[1]["id"].should.equal("000000000000000000000001_Groups");
        });
    });

    it("sets the tiles.json as data source", {
      auto map = crates.map.getItem("000000000000000000000001").and.exec.front;
      map["name"] = "map name";
      crates.map.updateItem(map);

      router
        .request
        .get("/tiles/000000000000000000000001/style.json")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["sources"].should.equal(`{
            "000000000000000000000001": {
              "type": "vector",
              "url": "http://localhost/tiles/about.json?map=000000000000000000000001"
            }
          }`.parseJsonString);
        });
    });

    describe("a public map", {
      beforeEach({
        auto map = crates.map.getItem("000000000000000000000001").and.exec.front;
        map["visibility"]["isPublic"] = true;
        crates.map.updateItem(map);
      });

      describeCredentialsRule("get the style json of a public map", "yes", "tiles", ["administrator", "owner", "leader", "member", "guest"], (string type) {
        router
          .request
          .get("/tiles/000000000000000000000001/style.json")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .end();
      });

      describeCredentialsRule("get the style json of a public map", "yes", "tiles", "no rights", {
        router
          .request
          .get("/tiles/000000000000000000000001/style.json")
          .expectStatusCode(200)
          .end();
      });
    });

    describe("a private map", {
      beforeEach({
        auto map = crates.map.getItem("000000000000000000000001").and.exec.front;
        map["visibility"]["isPublic"] = false;
        crates.map.updateItem(map);
      });

      describeCredentialsRule("get the style json of a private map", "yes", "tiles", ["administrator", "owner", "leader", "member", "guest"], (string type) {
        router
          .request
          .get("/tiles/000000000000000000000001/style.json")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .end();
      });

      describeCredentialsRule("get the style json of a private map", "no", "tiles", "no rights", {
        router
          .request
          .get("/tiles/000000000000000000000001/style.json")
          .expectStatusCode(404)
          .end();
      });
    });

    describe("the MapStyle operation", {
      MapStyle mapStyle;
      Json map;
      Json baseMap;

      beforeEach({
        mapStyle = new MapStyle(crates);

        baseMap = `{
          "layers":[{"type":"Open Street Maps","options":{}}],
          "defaultOrder":0,
          "icon": "",
          "visibility":{"isDefault":true,"isPublic":true,"team":"000000000000000000000001"},
          "name":"Open Street Maps",
          "attributions":[]
        }`.parseJsonString;

        map = crates.map.getItem("000000000000000000000001").and.exec.front;
        map["baseMaps"] = `{ "useCustomList": true, "list": ["000000000000000000000001"] }`.parseJsonString;

        crates.map.updateItem(map);
        baseMap = crates.baseMap.addItem(baseMap);
      });

      describe("addBaseMap", {
        it("adds the osm base map when is the first custom map", {
          MapboxStyle style;

          mapStyle.addBaseMap(style, map["_id"].to!string);

          style.sources.keys.should.equal(["osm"]);
          style.sources["osm"].should.equal(`{
            "type": "raster",
            "tiles": [
              "https://a.tile.openstreetmap.org/{z}/{x}/{y}.png",
              "https://b.tile.openstreetmap.org/{z}/{x}/{y}.png",
              "https://c.tile.openstreetmap.org/{z}/{x}/{y}.png"
            ],
            "tileSize": 256,
            "attribution": "<a href=\"https://www.openstreetmap.org/copyright\">© OpenStreetMap contributors</a>"
          }`.parseJsonString);
        });

        it("adds a raster base map when is the first custom map", {
          baseMap["name"] = `zenith`;
          baseMap["attributions"] = `[{"name": "zenith", "url": "https://www.example.com"}]`.parseJsonString;
          baseMap["layers"] = `[{
            "type": "XYZ",
            "options": {
              "tilePixelRatio": "3",
              "tileSize": 512,
              "visibility": [ 3, 22 ],
              "url": "https://www.example.com/data/tile/Z{z}/{y}/{x}.png"
          }}]`.parseJsonString;

          MapboxStyle style;
          mapStyle.addBaseMap(style, map["_id"].to!string);

          style.sources.keys.should.equal(["zenith_0"]);
          style.sources["zenith_0"].should.equal(`{
            "type": "raster",
            "tiles": [
              "https://www.example.com/data/tile/Z{z}/{y}/{x}.png"
            ],
            "tileSize": 256,
            "attribution": "<a href=\"https://www.example.com\">zenith<\/a>"
          }`.parseJsonString);

          style.layers.length.should.equal(1);
          style.layers[0].id.should.equal(`zenith_0_raster`);
        });

        it("adds stamen watercolor base map when is the first custom map", {
          baseMap["layers"] = `[{
            "type": "Stamen",
            "options": {
              "layer": "watercolor"
          }}]`.parseJsonString;

          MapboxStyle style;
          mapStyle.addBaseMap(style, map["_id"].to!string);

          style.sources.keys.should.equal(["stamen_watercolor"]);
          style.sources["stamen_watercolor"].should.equal(`{
            "type": "raster",
            "tiles": [
              "https://stamen-tiles.a.ssl.fastly.net/watercolor/{z}/{x}/{y}.jpg"
            ],
            "tileSize": 256,
            "attribution": "Map tiles by <a href=\"http://stamen.com\">Stamen Design</a>, under <a href=\"http://creativecommons.org/licenses/by/3.0\">CC BY 3.0</a>. Data by <a href=\"http://openstreetmap.org\">OpenStreetMap</a>, under <a href=\"http://creativecommons.org/licenses/by-sa/3.0\">CC BY SA</a>."
          }`.parseJsonString);

          style.layers.length.should.equal(1);
          style.layers[0].id.should.equal("stamen_watercolor_raster");
        });

        it("adds stamen toner base map when is the first custom map", {
          baseMap["layers"] = `[{
            "type": "Stamen",
            "options": {
              "layer": "toner"
          }}]`.parseJsonString;

          MapboxStyle style;
          mapStyle.addBaseMap(style, map["_id"].to!string);

          style.sources.keys.should.equal(["stamen_toner"]);
          style.sources["stamen_toner"].should.equal(`{
            "type": "raster",
            "tiles": [
              "https://stamen-tiles.a.ssl.fastly.net/toner/{z}/{x}/{y}.png"
            ],
            "tileSize": 256,
            "attribution": "Map tiles by <a href=\"http://stamen.com\">Stamen Design</a>, under <a href=\"http://creativecommons.org/licenses/by/3.0\">CC BY 3.0</a>. Data by <a href=\"http://openstreetmap.org\">OpenStreetMap</a>, under <a href=\"http://www.openstreetmap.org/copyright\">ODbL</a>."
          }`.parseJsonString);

          style.layers.length.should.equal(1);
          style.layers[0].id.should.equal("stamen_toner_raster");
        });

        it("adds stamen terrain base map when is the first custom map", {
          baseMap["layers"] = `[{
            "type": "Stamen",
            "options": {
              "layer": "terrain"
          }}]`.parseJsonString;

          MapboxStyle style;
          mapStyle.addBaseMap(style, map["_id"].to!string);

          style.sources.keys.should.equal(["stamen_terrain"]);
          style.sources["stamen_terrain"].should.equal(`{
            "type": "raster",
            "tiles": [
              "https://stamen-tiles.a.ssl.fastly.net/terrain/{z}/{x}/{y}.jpg"
            ],
            "tileSize": 256,
            "attribution": "Map tiles by <a href=\"http://stamen.com\">Stamen Design</a>, under <a href=\"http://creativecommons.org/licenses/by/3.0\">CC BY 3.0</a>. Data by <a href=\"http://openstreetmap.org\">OpenStreetMap</a>, under <a href=\"http://www.openstreetmap.org/copyright\">ODbL</a>."
          }`.parseJsonString);

          style.layers.length.should.equal(1);
          style.layers[0].id.should.equal(`stamen_terrain_raster`);
        });

        it("adds the osm base map when it is default and the map is using the default list", {
          map["baseMaps"] = `{ "useCustomList": false, "list": [] }`.parseJsonString;

          crates.map.updateItem(map);

          MapboxStyle style;

          mapStyle.addBaseMap(style, map["_id"].to!string);

          style.sources.keys.should.equal(["osm"]);
          style.sources["osm"].should.equal(`{
            "type": "raster",
            "tiles": [
              "https://a.tile.openstreetmap.org/{z}/{x}/{y}.png",
              "https://b.tile.openstreetmap.org/{z}/{x}/{y}.png",
              "https://c.tile.openstreetmap.org/{z}/{x}/{y}.png"
            ],
            "tileSize": 256,
            "attribution": "<a href=\"https://www.openstreetmap.org/copyright\">© OpenStreetMap contributors</a>"
          }`.parseJsonString);
        });

        it("adds the osm base map when it is default and there is no map requested", {
          map["baseMaps"] = `{ "useCustomList": false, "list": [] }`.parseJsonString;

          crates.map.updateItem(map);

          MapboxStyle style;

          mapStyle.addBaseMap(style, "_");

          style.sources.keys.should.equal(["osm"]);
          style.sources["osm"].should.equal(`{
            "type": "raster",
            "tiles": [
              "https://a.tile.openstreetmap.org/{z}/{x}/{y}.png",
              "https://b.tile.openstreetmap.org/{z}/{x}/{y}.png",
              "https://c.tile.openstreetmap.org/{z}/{x}/{y}.png"
            ],
            "tileSize": 256,
            "attribution": "<a href=\"https://www.openstreetmap.org/copyright\">© OpenStreetMap contributors</a>"
          }`.parseJsonString);
        });
      });

      describe("addMapBoxBaseMap", {
        MapboxStyle style;
        Json remoteStyle;

        beforeEach({
          style = MapboxStyle();

          remoteStyle = buildPath(".", "tests", "_fixtures", "mapbox_style.json").read.to!string.parseJsonString;
        });

        it("adds the remote layers", {
          mapStyle.addMapBoxBaseMap(style, remoteStyle);
          style.layers.length.should.equal(60);
        });

        it("adds the types to the remote layers", {
          mapStyle.addMapBoxBaseMap(style, remoteStyle);
          style.layers.map!(a => a.type.to!string).array.should.equal([
            "background", "fill", "fill", "fill", "fill", "fill", "fill", "fill", "fill", "line", "line", "symbol",
            "symbol", "fill", "hillshade", "line", "line", "symbol", "fill", "fill", "fill", "fill", "fill", "line",
            "line", "line", "line", "line", "line", "line", "line", "line", "symbol", "symbol", "line", "line", "line",
            "line", "line", "line", "line", "line", "fill", "symbol", "symbol", "symbol", "symbol", "symbol", "symbol",
            "symbol", "symbol", "symbol", "symbol", "symbol", "symbol", "symbol", "symbol", "symbol", "symbol", "symbol"
          ]);

          style.serializeToJson["layers"].byValue.map!(a => a["type"].to!string).array.should.equal([
            "background", "fill", "fill", "fill", "fill", "fill", "fill", "fill", "fill", "line", "line", "symbol",
            "symbol", "fill", "hillshade", "line", "line", "symbol", "fill", "fill", "fill", "fill", "fill", "line",
            "line", "line", "line", "line", "line", "line", "line", "line", "symbol", "symbol", "line", "line", "line",
            "line", "line", "line", "line", "line", "fill", "symbol", "symbol", "symbol", "symbol", "symbol", "symbol",
            "symbol", "symbol", "symbol", "symbol", "symbol", "symbol", "symbol", "symbol", "symbol", "symbol", "symbol"
          ]);
        });

        it("adds the remote sources", {
          mapStyle.addMapBoxBaseMap(style, remoteStyle);

          style.sources["composite"]["url"].to!string.should.equal("mapbox://mapbox.mapbox-terrain-v2,mapbox.mapbox-streets-v8,mapbox.mapbox-bathymetry-v1,mslee.0fc2f90a");
          style.sources["mapbox://mapbox.terrain-rgb"]["url"].to!string.should.equal("mapbox://mapbox.terrain-rgb");
        });
      });
    });
  });
});
