/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tile.tile;

import tests.fixtures;

import std.algorithm;
import std.array;

import geo.vector;
import ogm.tiles.tiles;
import ogm.tiles.api;

alias suite = Spec!({
  URLRouter router;

  describe("GET a tile", {
    beforeEach({
      setupTestData();

      router = new URLRouter;
      router.crateSetup.setupTilesApi(crates, "http://localhost", "http://frontend");
      TileOptions.cache.clear;
    });

    describe("when the tile does not match any sites", {
      it("should return an empty layer list", {
        router
          .request
          .get("/tiles/16/640/390?format=json")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson
              .should.equal(`{ "points":[], "polygons": [], "lines": [], "groups":[] }`.parseJsonString);
          });
      });

      it("should get the empty tile in vt format", {
        router
          .request
          .get("/tiles/16/640/390")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyRaw.readTile.serializeToJson
              .should.equal(`{ "layers": [] }`.parseJsonString);
          });
      });
    });

    describe("when all maps are marked as hidden on the main map", {
      beforeEach({
        auto map = crates.map.getItem("000000000000000000000001").exec.front;
        map["hideOnMainMap"] = true;
        crates.map.updateItem(map);

        map = crates.map.getItem("000000000000000000000002").exec.front;
        map["hideOnMainMap"] = true;
        crates.map.updateItem(map);

        map = crates.map.getItem("000000000000000000000003").exec.front;
        map["hideOnMainMap"] = true;
        crates.map.updateItem(map);

        map = crates.map.getItem("000000000000000000000004").exec.front;
        map["hideOnMainMap"] = true;
        crates.map.updateItem(map);
      });

      it("should not return any site for an owner user", {
        router
          .request
          .get("/tiles/0/0/0?format=json")
          .header("Authorization", "Bearer " ~ userTokenList["owner"])
          .expectStatusCode(200)
          .end((Response response) => () {
            auto tile = response.bodyJson;
            tile["points"].length.should.equal(0);
            tile["lines"].length.should.equal(0);
            tile["polygons"].length.should.equal(0);
            tile["groups"].length.should.equal(1);
          });
      });

      it("should not return any site for an admin user", {
        router
          .request
          .get("/tiles/0/0/0?format=json")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            auto tile = response.bodyJson;
            tile["points"].length.should.equal(0);
            tile["lines"].length.should.equal(0);
            tile["polygons"].length.should.equal(0);
            tile["groups"].length.should.equal(1);
          });
      });
    });

    describe("when there is a masked feature for own team", {
      beforeEach({
        auto feature1 = crates.feature.getItem("000000000000000000000001").and.exec.front;
        feature1["unmasked"] = `{
          "type": "Point",
          "coordinates": [5,5]
        }`.parseJsonString;
        feature1["unmaskedBox"] = `{
          "type": "Point",
          "coordinates": [5,5]
        }`.parseJsonString;

        crates.feature.updateItem(feature1);

        auto map1 = crates.map.getItem("000000000000000000000001").and.exec.front;
        map1["mask"] = `{
          "isEnabled": true
        }`.parseJsonString;

        crates.map.updateItem(map1);
      });

      describeCredentialsRule("get map tiles with masked data without a map selector", "yes", "map tiles", ["administrator", "owner", "leader", "member", "guest"], (string type) {
        router
          .request
          .get("/tiles/0/0/0?format=json")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .end((Response response) => () {
            auto tile = response.bodyJson;
            tile["points"].length.should.equal(0);
            tile["lines"].length.should.equal(0);
            tile["polygons"].length.should.equal(0);

            tile["groups"].length.should.equal(1);
            tile["groups"][0]["key"].should.equal(`{ "y": 43, "x": 42 }`.parseJsonString);
            tile["groups"][0]["properties"]["idList"].to!string.should.contain(`000000000000000000000001`);
            tile["groups"][0]["properties"]["idList"].to!string.should.contain(`000000000000000000000003`);
          });
      });

      describeCredentialsRule("get map tiles with masked data without a map selector", "yes", "map tiles", "no rights", {
        router
          .request
          .get("/tiles/0/0/0?format=json")
          .expectStatusCode(200)
          .end((Response response) => () {
            auto tile = response.bodyJson;
            tile["points"].length.should.equal(0);
            tile["lines"].length.should.equal(0);
            tile["polygons"].length.should.equal(0);

            tile["groups"].length.should.equal(1);
            tile["groups"][0]["key"].should.equal(`{ "y": 43, "x": 42 }`.parseJsonString);
            tile["groups"][0]["properties"]["idList"].to!string.should.contain(`000000000000000000000001`);
            tile["groups"][0]["properties"]["idList"].to!string.should.contain(`000000000000000000000003`);
          });
      });


      describeCredentialsRule("get map tiles with unmasked data with a map selector for own team", "yes", "map tiles", ["administrator", "owner", "leader", "member", "guest"], (string type) {
        router
          .request
          .get("/tiles/0/0/0?format=json&map=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .end((Response response) => () {
            auto tile = response.bodyJson;

            tile["points"].length.should.equal(1);
            tile["lines"].length.should.equal(0);
            tile["polygons"].length.should.equal(0);
            tile["groups"].length.should.equal(0);

            tile["points"].should.equal(`[{
              "key":{"y":2030,"x":2065},
              "properties":{
                "_id":"000000000000000000000001",
                "type":"point",
                "visibility": "1",
                "canView": "true",
                "icons":"000000000000000000000001",
                "primaryIcon":"000000000000000000000001",
                "name":"site1"}}]`.parseJsonString);
          });
      });

      describeCredentialsRule("get map tiles with unmasked data with a map selector for own team", "-", "map tiles", "no rights", {
        router
          .request
          .get("/tiles/0/0/0?format=json&map=000000000000000000000001")
          .expectStatusCode(200)
          .end((Response response) => () {
            auto tile = response.bodyJson;
            tile["points"].length.should.equal(1);
            tile["lines"].length.should.equal(0);
            tile["polygons"].length.should.equal(0);
            tile["groups"].length.should.equal(0);

            tile["points"].should.equal(`[{
              "key":{"y":2030,"x":2065},
              "properties":{
                "_id":"000000000000000000000001",
                "type":"point",
                "visibility": "1",
                "canView": "true",
                "icons":"000000000000000000000001",
                "primaryIcon":"000000000000000000000001",
                "name":"site1"}}]`.parseJsonString);
          });
      });
    });

    describe("when there is a masked feature for other teams", {
      beforeEach({
        auto feature = crates.feature.getItem("000000000000000000000003").and.exec.front;
        feature["unmasked"] = `{
          "type": "Point",
          "coordinates": [5,5]
        }`.parseJsonString;
        feature["unmaskedBox"] = `{
          "type": "Point",
          "coordinates": [5,5]
        }`.parseJsonString;

        crates.feature.updateItem(feature);

        auto map = crates.map.getItem("000000000000000000000003").and.exec.front;
        map["mask"] = `{
          "isEnabled": true
        }`.parseJsonString;

        crates.map.updateItem(map);
      });

      describeCredentialsRule("get map tiles with unmasked data with a map selector for other teams", "yes", "map tiles", ["administrator"], (string type) {
        router
          .request
          .get("/tiles/0/0/0?format=json&map=000000000000000000000003")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .end((Response response) => () {
            auto tile = response.bodyJson;

            tile["points"].length.should.equal(1);
            tile["lines"].length.should.equal(0);
            tile["polygons"].length.should.equal(0);
            tile["groups"].length.should.equal(0);

            tile["points"].should.equal(`[{
              "key":{"y":2028,"x":2067},
              "properties":{
                "_id":"000000000000000000000003",
                "type":"point",
                "visibility": "1",
                "canView": "true",
                "icons":"000000000000000000000001,000000000000000000000002",
                "primaryIcon":"000000000000000000000001",
                "name":"site3"}}]`.parseJsonString);
          });
      });

      describeCredentialsRule("get map tiles with unmasked data with a map selector for other teams", "no", "map tiles", ["owner", "leader", "member", "guest"], (string type) {
        router
          .request
          .get("/tiles/0/0/0?format=json&map=000000000000000000000003")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .end((Response response) => () {
            auto tile = response.bodyJson;

            tile["points"].length.should.equal(1);
            tile["lines"].length.should.equal(0);
            tile["polygons"].length.should.equal(0);
            tile["groups"].length.should.equal(0);

            tile["points"].should.equal(`[{
              "key":{"y":2028,"x":2067},
              "properties":{
                "_id":"000000000000000000000003",
                "type":"point",
                "visibility": "1",
                "canView":"true",
                "icons":"000000000000000000000001,000000000000000000000002",
                "primaryIcon":"000000000000000000000001",
                "name":"site3"}}]`.parseJsonString);
          });
      });

      describeCredentialsRule("get map tiles with unmasked data with a map selector for other teams", "no", "map tiles", "no rights", {
        router
          .request
          .get("/tiles/0/0/0?format=json&map=000000000000000000000003")
          .expectStatusCode(200)
          .end((Response response) => () {
            auto tile = response.bodyJson;
            tile["points"].length.should.equal(1);
            tile["lines"].length.should.equal(0);
            tile["polygons"].length.should.equal(0);
            tile["groups"].length.should.equal(0);

            tile["points"].should.equal(`[{
              "key":{"y":2028,"x":2067},
              "properties":{
                "_id":"000000000000000000000003",
                "type":"point",
                "visibility": "1",
                "canView": "true",
                "icons":"000000000000000000000001,000000000000000000000002",
                "primaryIcon":"000000000000000000000001",
                "name":"site3"}}]`.parseJsonString);
          });
      });
    });

    describe("when there is a pending feature", {
      beforeEach({
        auto feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
        feature["visibility"] = -1;
        crates.feature.updateItem(feature);
      });

      describeCredentialsRule("get map tiles with pending data", "yes", "map tiles", ["administrator", "owner", "leader", "member", "guest"], (string type) {
        router
          .request
          .get("/tiles/0/0/0?format=json")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .end((Response response) => () {
            auto tile = response.bodyJson;
            tile["points"].length.should.equal(0);
            tile["lines"].length.should.equal(0);
            tile["polygons"].length.should.equal(0);

            tile["groups"].length.should.equal(1);
            tile["groups"][0]["key"].should.equal(`{ "y": 43, "x": 42 }`.parseJsonString);
            tile["groups"][0]["properties"]["idList"].to!string.should.contain(`000000000000000000000001`);
            tile["groups"][0]["properties"]["idList"].to!string.should.contain(`000000000000000000000003`);
          });
      });

      describeCredentialsRule("get map tiles with pending data", "yes", "map tiles", "no rights", {
        router
          .request
          .get("/tiles/0/0/0?format=json")
          .expectStatusCode(200)
          .end((Response response) => () {
            auto tile = response.bodyJson;
            tile["points"].length.should.equal(0);
            tile["lines"].length.should.equal(0);
            tile["polygons"].length.should.equal(0);

            tile["groups"].length.should.equal(1);
            tile["groups"][0]["key"].should.equal(`{ "y": 43, "x": 42 }`.parseJsonString);
            tile["groups"][0]["properties"]["idList"].to!string.should.contain(`000000000000000000000003`);
            tile["groups"][0]["properties"]["count"].to!string.should.equal("2");
            tile["groups"][0]["properties"]["pendingCount"].to!string.should.equal("1");
          });
      });
    });

    describe("when there is a map without clustering", {
      beforeEach({
        auto map = crates.map.getItem("000000000000000000000001").exec.front;
        map["cluster"] = `{ "mode": 0, "map": "" }`.parseJsonString;
        crates.map.updateItem(map);

        createFeature(
          Feature(ObjectId.fromString("12"),
                [map1],
                "another site",
                Json("another site"),
                GeoJsonGeometry.fromJson(`{ "type": "Point", "coordinates": [1.5, 1.5] }`.parseJsonString),
                ModelInfo(
                  SysTime.fromISOExtString("2015-01-01T00:00:00Z"),
                  SysTime.fromISOExtString("2015-01-01T00:00:00Z"),
                  0,"",
                  "000000000000000000000001"),
                [],
                [],
                [icon1],
                VisibilityEnum.Public));
      });

      it("does not return clusters", {
        router
          .request
          .get("/tiles/0/0/0?format=json&map=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            auto tile = response.bodyJson;

            tile["lines"].length.should.equal(0);
            tile["polygons"].length.should.equal(0);
            tile["groups"].length.should.equal(0);
            tile["points"].should.equal(`[{
              "key":{"y":2030,"x":2065},
              "properties": {
                "_id":"000000000000000000000001",
                "visibility":"1",
                "type":"point",
                "icons":"000000000000000000000001",
                "primaryIcon":"000000000000000000000001",
                "canView":"true",
                "name":"site1"
              }
            }]`.parseJsonString);
          });
      });
    });

    describe("when there is a public map with a feature without visibility", {
      beforeEach({
        auto feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
        feature["visibility"] = Json();
        crates.feature.updateItem(feature);

        feature = crates.feature.getItem("000000000000000000000004").and.exec.front;
        feature["visibility"] = Json();
        crates.feature.updateItem(feature);
      });

      it("does not add the feature", {
        router
          .request
          .get("/tiles/0/0/0?format=json&map=000000000000000000000001")
          .expectStatusCode(200)
          .end((Response response) => () {
            auto tile = response.bodyJson;
            tile["points"].length.should.equal(1);
            tile["lines"].length.should.equal(0);
            tile["polygons"].length.should.equal(0);
            tile["groups"].length.should.equal(0);
          });
      });
    });

    describeCredentialsRule("get map tiles with public data", "yes", "map tiles", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/tiles/0/0/0?format=json")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto tile = response.bodyJson;
          tile["points"].length.should.equal(0);
          tile["lines"].length.should.equal(0);
          tile["polygons"].length.should.equal(0);

          tile["groups"].length.should.equal(1);
          tile["groups"][0]["key"].should.equal(`{ "y": 43, "x": 42 }`.parseJsonString);
          tile["groups"][0]["properties"]["idList"].to!string.should.contain(`000000000000000000000001`);
          tile["groups"][0]["properties"]["idList"].to!string.should.contain(`000000000000000000000003`);
        });
    });

    describeCredentialsRule("get map tiles with public data", "yes", "map tiles", "no rights", {
      router
        .request
        .get("/tiles/0/0/0?format=json")
        .expectStatusCode(200)
        .end((Response response) => () {
          auto tile = response.bodyJson;
          tile["points"].length.should.equal(0);
          tile["lines"].length.should.equal(0);
          tile["polygons"].length.should.equal(0);

          tile["groups"].length.should.equal(1);
          tile["groups"][0]["key"].should.equal(`{ "y": 43, "x": 42 }`.parseJsonString);
          tile["groups"][0]["properties"]["idList"].to!string.should.contain(`000000000000000000000001`);
          tile["groups"][0]["properties"]["idList"].to!string.should.contain(`000000000000000000000003`);
        });
    });


    describeCredentialsRule("get map tiles with private features owned by the team", "yes", "map tiles", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/tiles/0/0/0?format=json")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto tile = response.bodyJson;
          tile["points"].length.should.equal(0);
          tile["lines"].length.should.equal(0);
          tile["polygons"].length.should.equal(0);

          tile["groups"].length.should.equal(1);
          tile["groups"][0]["key"].should.equal(`{ "y": 43, "x": 42 }`.parseJsonString);
          tile["groups"][0]["properties"]["idList"].to!string.should.contain(`000000000000000000000002`);
        });
    });

    describeCredentialsRule("get map tiles with private features owned by the team", "-", "map tiles", "no rights", {
      router
        .request
        .get("/tiles/0/0/0?format=json")
        .expectStatusCode(200)
        .end((Response response) => () {
          auto tile = response.bodyJson;
          tile["points"].length.should.equal(0);
          tile["lines"].length.should.equal(0);
          tile["polygons"].length.should.equal(0);

          tile["groups"].length.should.equal(1);
          tile["groups"][0]["key"].should.equal(`{ "y": 43, "x": 42 }`.parseJsonString);
          tile["groups"][0]["properties"]["idList"].to!string.should.not.contain(`000000000000000000000002`);
          tile["groups"][0]["properties"]["idList"].to!string.should.not.contain(`000000000000000000000004`);
        });
    });


    describeCredentialsRule("get map tiles with private features owned by other teams", "no", "map tiles", ["owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/tiles/0/0/0?format=json")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto tile = response.bodyJson;
          tile["points"].length.should.equal(0);
          tile["lines"].length.should.equal(0);
          tile["polygons"].length.should.equal(0);

          tile["groups"].length.should.equal(1);
          tile["groups"][0]["key"].should.equal(`{ "y": 43, "x": 42 }`.parseJsonString);
          tile["groups"][0]["properties"]["idList"].to!string.should.not.contain(`000000000000000000000004`);
        });
    });

    describeCredentialsRule("get map tiles with private features owned by other teams", "yes", "map tiles", "administrator", (string type) {
      router
        .request
        .get("/tiles/0/0/0?format=json")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto tile = response.bodyJson;
          tile["points"].length.should.equal(0);
          tile["lines"].length.should.equal(0);
          tile["polygons"].length.should.equal(0);

          tile["groups"].length.should.equal(1);
          tile["groups"][0]["key"].should.equal(`{ "y": 43, "x": 42 }`.parseJsonString);
          tile["groups"][0]["properties"]["idList"].to!string.should.contain(`000000000000000000000004`);
        });
    });

    describeCredentialsRule("get map tiles with private features owned by other teams", "-", "map tiles", "no rights", {
      router
        .request
        .get("/tiles/0/0/0?format=json")
        .expectStatusCode(200)
        .end((Response response) => () {
          auto tile = response.bodyJson;
          tile["points"].length.should.equal(0);
          tile["lines"].length.should.equal(0);
          tile["polygons"].length.should.equal(0);

          tile["groups"].length.should.equal(1);
          tile["groups"][0]["key"].should.equal(`{ "y": 43, "x": 42 }`.parseJsonString);
          tile["groups"][0]["properties"]["idList"].to!string.should.not.contain(`000000000000000000000002`);
          tile["groups"][0]["properties"]["idList"].to!string.should.not.contain(`000000000000000000000004`);
        });
    });


    describeCredentialsRule("get map tiles for a private map owned by the team", "yes", "map tiles", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/tiles/0/0/0?format=json&map=000000000000000000000002")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto tile = response.bodyJson;
          tile["lines"].length.should.equal(0);
          tile["polygons"].length.should.equal(0);
          tile["groups"].length.should.equal(0);

          tile["points"].should.equal(`[{
            "key":{"y":2029,"x":2066},
            "properties":{
              "_id":"000000000000000000000002",
              "type":"point",
              "visibility": "0",
              "icons":"000000000000000000000001,000000000000000000000002",
              "primaryIcon":"000000000000000000000001",
              "name":"site2",
              "canView": "true"
            }
          }]`.parseJsonString);
        });
    });

    describeCredentialsRule("get map tiles for a private map owned by the team", "-", "map tiles", "no rights", {
      router
        .request
        .get("/tiles/0/0/0?format=json&map=000000000000000000000002")
        .expectStatusCode(200)
        .end((Response response) => () {
          auto tile = response.bodyJson;

          tile["points"].length.should.equal(0);
          tile["lines"].length.should.equal(0);
          tile["polygons"].length.should.equal(0);
          tile["groups"].length.should.equal(0);
        });
    });


    describeCredentialsRule("get map tiles for a private map owned by other team", "no", "map tiles", ["owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/tiles/0/0/0?format=json&map=000000000000000000000004")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto tile = response.bodyJson;

          tile["lines"].length.should.equal(0);
          tile["polygons"].length.should.equal(0);
          tile["groups"].length.should.equal(0);
          tile["points"].length.should.equal(0);
        });
    });

    describeCredentialsRule("get map tiles for a private map owned by other team", "yes", "map tiles", "administrator", (string type) {
      router
        .request
        .get("/tiles/0/0/0?format=json&map=000000000000000000000004")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto tile = response.bodyJson;

          tile["lines"].length.should.equal(0);
          tile["polygons"].length.should.equal(0);
          tile["groups"].length.should.equal(0);
          tile["points"].should.equal(`[{
            "key":{"y":2027,"x":2068},
            "properties":{
              "_id":"000000000000000000000004",
              "type":"point",
              "visibility": "0",
              "canView": "true",
              "icons":"000000000000000000000001,000000000000000000000002",
              "primaryIcon":"000000000000000000000001",
              "name":"site4"
            }
          }]`.parseJsonString);
        });
    });

    describeCredentialsRule("get map tiles for a private map owned by other team", "-", "map tiles", "no rights", {
      router
        .request
        .get("/tiles/0/0/0?format=json&map=000000000000000000000004")
        .expectStatusCode(200)
        .end((Response response) => () {
          auto tile = response.bodyJson;

          tile["points"].length.should.equal(0);
          tile["lines"].length.should.equal(0);
          tile["polygons"].length.should.equal(0);
          tile["groups"].length.should.equal(0);
        });
    });


    describeCredentialsRule("filter public map tiles by icons", "yes", "map tiles", ["owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/tiles/0/0/0?format=json&icons=000000000000000000000002")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto tile = response.bodyJson;
          tile["points"].length.should.equal(0);
          tile["lines"].length.should.equal(0);
          tile["polygons"].length.should.equal(0);
          tile["groups"].length.should.equal(1);
          tile["groups"][0]["key"].should.equal(`{ "y": 43, "x": 42 }`.parseJsonString);
          tile["groups"][0]["properties"]["idList"].to!string.should.contain(`000000000000000000000002`);
          tile["groups"][0]["properties"]["idList"].to!string.should.contain(`000000000000000000000003`);

          tile["groups"][0]["properties"]["idList"].to!string.should.not.contain(`000000000000000000000001`);
          tile["groups"][0]["properties"]["idList"].to!string.should.not.contain(`000000000000000000000004`);
        });
    });


    describeCredentialsRule("filter public map tiles by icons", "yes", "map tiles", "administrator", (string type) {
      router
        .request
        .get("/tiles/0/0/0?format=json&icons=000000000000000000000002")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto tile = response.bodyJson;
          tile["points"].length.should.equal(0);
          tile["lines"].length.should.equal(0);
          tile["polygons"].length.should.equal(0);
          tile["groups"].length.should.equal(1);
          tile["groups"][0]["key"].should.equal(`{ "y": 43, "x": 42 }`.parseJsonString);
          tile["groups"][0]["properties"]["idList"].to!string.should.contain(`000000000000000000000002`);
          tile["groups"][0]["properties"]["idList"].to!string.should.contain(`000000000000000000000003`);
          tile["groups"][0]["properties"]["idList"].to!string.should.contain(`000000000000000000000004`);

          tile["groups"][0]["properties"]["idList"].to!string.should.not.contain(`000000000000000000000001`);
        });
    });

    describeCredentialsRule("filter public map tiles by icons", "yes", "map tiles", "no rights", {
      router
        .request
        .get("/tiles/0/0/0?format=json&icons=000000000000000000000002")
        .expectStatusCode(200)
        .end((Response response) => () {
          auto tile = response.bodyJson;
          tile["points"].length.should.equal(1);
          tile["lines"].length.should.equal(0);
          tile["polygons"].length.should.equal(0);

          tile["points"][0].should.equal(`{
            "key": {
              "y": 2028,
              "x": 2067
            },
            "properties": {
              "_id": "000000000000000000000003",
              "type": "point",
              "visibility": "1",
              "canView": "true",
              "icons": "000000000000000000000002,000000000000000000000001",
              "primaryIcon": "000000000000000000000002",
              "name": "site3"
            }}`.parseJsonString);
        });
    });
  });
});
