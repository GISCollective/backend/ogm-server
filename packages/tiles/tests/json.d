/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.tile.json;

import tests.fixtures;

import std.algorithm;
import std.array;

import geo.vector;
import ogm.tiles.api;

alias suite = Spec!({
  URLRouter router;

  describe("GET the `tiles.json` file", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupTilesApi(crates, "http://localhost", "http://frontend");
    });

    it("should return the tile json for all the stored points", {
      router
        .request
        .get("/tiles/about.json")
        .expectStatusCode(200)
        .end((Response response) => () {
          auto result = response.bodyJson;
          result["bounds"] = result["bounds"].to!string;

          result.should.equal((`{
            "attribution": "<a href=\"http://frontend/help/about\" target=\"_blank\">&copy; </a>",
            "bounds": "[-180,-85.05112877980659,180,85.0511287798066]",
            "center": [-76.275329586789, 39.153492567373, 8],
            "data": [],
            "description": "",
            "fillzoom": 16,
            "grids": [],
            "legend": "",
            "maxzoom": 16,
            "minzoom": 0,
            "name": "",
            "scheme": "xyz",
            "temlpate": "",
            "tilejson": "3.0.0",
            "tiles": ["http://localhost/tiles/{z}/{x}/{y}?format=vt"],
            "vector_layers": [{
              "description": "Unclustered sites",
              "fields": { "_id": "String", "icons": "String", "visibility": "Integer", "name": "String", "primaryIcon": "String","type": "String", "canView": "String"},
              "id": "points",
              "maxzoom": 16,
              "minzoom": 0
            }, {
              "description": "Lines",
              "fields": { "_id": "String", "icons": "String", "visibility": "Integer", "name": "String", "primaryIcon": "String","type": "String", "canView": "String"},
              "id": "lines",
              "maxzoom": 16,
              "minzoom": 0
            }, {
              "description": "Polygons",
              "fields": { "_id": "String", "icons": "String", "visibility": "Integer", "name": "String", "primaryIcon": "String","type": "String", "canView": "String"},
              "id": "polygons",
              "maxzoom": 16,
              "minzoom": 0
            }, {
              "description": "",
              "fields": { "count": "Integer", "pendingCount": "Integer", "idList": "List of strings","level": "Integer" },
              "id": "groups",
              "maxzoom": 16,
              "minzoom": 0
            }, {
              "description": "",
              "fields": { "size": "Integer", "name": "String" },
              "id": "debug",
              "maxzoom": 16,
              "minzoom": 0
            }],
            "version": "1.0.0"
          }`).parseJsonString);
        });
    });

    describe("a public map", {
      beforeEach({
        auto map = crates.map.getItem("000000000000000000000001").and.exec.front;
        map["visibility"]["isPublic"] = true;
        crates.map.updateItem(map);
      });

      describeCredentialsRule("get the tile json of a public map", "yes", "tiles", ["administrator", "owner", "leader", "member", "guest"], (string type) {
        router
          .request
          .get("/tiles/about.json?map=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .end();
      });

      describeCredentialsRule("get the tile json of a public map", "yes", "tiles", "no rights", {
        router
          .request
          .get("/tiles/about.json?map=000000000000000000000001")
          .expectStatusCode(200)
          .end();
      });
    });

    describe("a private map", {
      beforeEach({
        auto map = crates.map.getItem("000000000000000000000001").and.exec.front;
        map["visibility"]["isPublic"] = false;
        crates.map.updateItem(map);
      });

      describeCredentialsRule("get the tile json of a private map", "yes", "tiles", ["administrator", "owner", "leader", "member", "guest"], (string type) {
        router
          .request
          .get("/tiles/about.json?map=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .end((Response response) => () {
            import std.stdio;
            writeln(response.bodyString);
          });
      });

      describeCredentialsRule("get the tile json of a private map", "no", "tiles", "no rights", {
        router
          .request
          .get("/tiles/about.json?map=000000000000000000000001")
          .expectStatusCode(404)
          .end();
      });
    });
  });
});
