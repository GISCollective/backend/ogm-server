/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.icons.middleware.attributes;

import ogm.crates.all;
import ogm.models.icon;
import ogm.http.request;
import ogm.icons.TaxonomyTree;

import vibe.http.router;
import vibe.data.json;

import crate.base;
import crate.error;

import std.exception;
import std.string;
import std.algorithm;
import std.array;

class IconAttributesMiddleware {
  struct Parameters {
    @describe("Get the icons without the attributes. If is true the `attributes` field will be missing from the response")
    @example("true", "don't return the attributes")
    @example("false", "return the attributes")
    bool withoutAttributes;
  }

  private {
    TaxonomyTree iconsTree;
    OgmCrates crates;
  }

  this(OgmCrates crates, TaxonomyTree iconsTree) {
    this.iconsTree = iconsTree;
  }

  /// Sets the projection to remove the icons atttributes
  @get
  IQuery get(IQuery selector, Parameters parameters, HTTPServerRequest req) {
    if(parameters.withoutAttributes) {
      selector.withProjection([
        "_id", "iconSet", "category", "subcategory", "name", "description", "image",
        "parent", "allowMany", "otherNames", "localName", "styles", "visibility"
      ]);
    }

    return selector;
  }

  /// Remove attributes that can be found in parent icons
  @replace
  void replace(HTTPServerRequest req) {
    if("icon" !in req.json) {
      return;
    }

    auto item = req.json["icon"];

    if("parent" !in item || item["parent"] == "") {
      return;
    }

    string[] visibleIconSets;
    scope request = RequestUserData(req);
    item["_id"] = request.itemId;

    const iconSet = item["iconSet"].to!string;
    Json[] attributes;

    auto currentAttributes = iconsTree.getIconNode(item["parent"].to!string).attributes;

    foreach(attribute; req.json["icon"]["attributes"]) {
      attribute.remove("isInherited");
      attribute.remove("from");

      this.validateAttribute(attribute);

      if(this.isUpdated(attribute, currentAttributes, request.itemId)) {
        attributes ~= attribute;
      }
    }

    req.json["icon"]["attributes"] = attributes;
  }

  private bool isUpdated(Json attribute, Json[] currentAttributes, string iconId) {
    if(attribute.type != Json.Type.object) {
      return false;
    }

    auto matched = currentAttributes.filter!(a => a.type == Json.Type.object && a["name"] == attribute["name"]);

    if(matched.empty) {
      return true;
    }

    auto matchedAttribute = matched.front;

    static immutable keys = ["help", "isPrivate", "isRequired", "displayName", "type", "options" ];

    foreach(key; keys) {
      if(key in attribute && matchedAttribute[key] != attribute[key]) {
        return true;
      }
    }

    return false;
  }

  private void validateAttribute(Json attribute) {
    auto name = attribute["name"].to!string;

    enforce!CrateValidationException(name.indexOf('.') == -1, "The attributes are not allowed to have the '.' char in their name.");
  }
}
