/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.icons.middleware.computedVisibility;

import ogm.crates.all;
import crate.base;
import vibe.data.json;
import vibe.http.server;

class ComputedVisibilityMiddleware {
  private {
    OgmCrates crates;
  }

  ///
  this(OgmCrates crates) {
    this.crates = crates;
  }

  @create @put
  void checkCampaignAccess(HTTPServerRequest req) {
    updateVisibility(req.json["icon"]);
  }

  void updateVisibility(ref Json icon) {
    auto iconSet = crates.iconSet.getItem(icon["iconSet"].to!string)
      .and.withProjection(["visibility"])
      .and.exec.front;

    icon["visibility"] = iconSet["visibility"];
  }
}
