/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.icons.middleware.IconParentMiddleware;;

import ogm.crates.all;
import ogm.models.icon;
import ogm.http.request;

import vibe.http.router;
import vibe.data.json;

import crate.base;
import crate.error;

import std.exception;
import std.string;

class IconParentMiddlware {
  private {
    OgmCrates crates;
  }

  ///
  this(OgmCrates crates) {
    this.crates = crates;
  }

  string getIconImage(ObjectId iconId) {
    auto range = crates.icon.get.where("_id").equal(iconId).and.exec;

    enforce!CrateValidationException(!range.empty, "The selected parent icon does not exist.");

    auto parentIcon = range.front;
    if(parentIcon["image"]["useParent"] == true) {
      return getIconImage(ObjectId(parentIcon["parent"]));
    }

    return parentIcon["image"]["value"].to!string;
  }

  void checkIcon(ref Json icon) {
    if(icon["image"].type != Json.Type.object) {
      icon["image"] = Json.emptyObject;
    }

    auto hasParent = icon["parent"].type == Json.Type.string && icon["parent"].to!string.strip != "";

    if(icon["image"]["useParent"].type != Json.Type.bool_ || !hasParent) {
      icon["image"]["useParent"] = false;
    }

    if(icon["image"]["useParent"] == true) {
      icon["image"]["value"] = getIconImage(ObjectId(icon["parent"]));
    }
  }

  /// If the icon parent is not set, an empty string will be set
  @create @put
  void updateImage(HTTPServerRequest req) {
    if("icon" !in req.json) {
      return;
    }

    checkIcon(req.json["icon"]);
  }
}
