/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.icons.middleware.output;

import ogm.crates.all;
import ogm.models.icon;
import ogm.http.request;

import vibe.http.router;
import vibe.data.json;

import crate.error;
import crate.base;
import crate.http.request;

import std.exception;
import std.string;
import std.algorithm;
import std.array;

import ogm.icons.CategoryTree;
import ogm.icons.nodes.INode;
import ogm.icons.mappers.IconMappers;
import ogm.middleware.translation;
import ogm.TimedCache;

/// Controls how the icons are returned to the client
class IconOutput {
  private {
    IconMappers iconMappers;
    CategoryTree iconsTree;
    TimedCache!string iconSetCache;
    OgmCrates crates;
  }

  this(IconMappers iconMappers, CategoryTree iconsTree, OgmCrates crates) {
    this.iconMappers = iconMappers;
    this.iconsTree = iconsTree;
    this.crates = crates;
    this.iconSetCache = new TimedCache!string(&this.getIconSetName);
  }

  string getIconSetName(string id) {

    auto range = crates.iconSet.get.where("_id").equal(ObjectId.fromString(id)).and.withProjection(["name"]).exec;

    if(range.empty) {
      return "unknown";
    }

    return range.front["name"].to!string;
  }

  ///
  @get
  IQuery get(IQuery selector, HTTPServerRequest request, HTTPServerResponse response, ModelTranslationMiddleware!(Icon).Parameters params) {
    if("group" !in request.query || request.query["group"] != "true") {
      return selector;
    }

    bool withSet = "grupset" in request.query && request.query["grupset"] == "true";

    Json result = Json.emptyObject;
    result["categories"] = Json.emptyObject;
    result["icons"] = Json.emptyArray;

    Json[string] cache;
    cache["#"] = Json.emptyObject;
    cache["#"]["icons"] = Json.emptyArray;
    cache["#"]["categories"] = Json.emptyObject;

    string[string] dictionary;

    auto iconNodes = selector.withProjection(["_id"]).distinct("_id")
      .map!(a => iconsTree.getIconNode(a.to!string))
      .array;

    TreeNode[5] path;

    size_t getPath(INode node, INode iconNode) {
      size_t index;

      while(node.hasParent) {
        path[index] = TreeNode(node.name, node.id);
        node = node.parent;
        index++;
      }

      if(withSet) {
        auto setId = iconNode.setId;

        if(setId != "") {
          path[index] = TreeNode(iconSetCache.get(setId), setId);
          index++;
        }
      }

      return index;
    }

    void check(ref TreeNode[] nodePath) {
      if(nodePath.length == 0) {
        return;
      }

      const nodeId = nodePath[0].id;

      if(nodeId in cache) {
        return;
      }

      Json tmp = result;
      foreach_reverse(item; nodePath) {
        string key = item.name;
        string id = item.id;

        if(key in dictionary) {
          key = dictionary[key];
        }

        if(key !in tmp["categories"]) {
          tmp["categories"][key] = Json.emptyObject;
          tmp["categories"][key]["categories"] = Json.emptyObject;
          tmp["categories"][key]["icons"] = Json.emptyArray;

          cache[id] = tmp["categories"][key];
        }

        tmp = tmp["categories"][key];
      }
    }

    foreach(node; iconNodes) {
      auto mappedItem = iconsTree.getIcon(node.id);
      auto originalCategory = mappedItem["category"].to!string;
      auto originalSubcategory = mappedItem["subcategory"].to!string;

      iconMappers.mapper(request, mappedItem, params);

      dictionary[originalCategory] = mappedItem["category"].to!string;
      dictionary[originalSubcategory] = mappedItem["subcategory"].to!string;


      auto pathLength = getPath(node.parent, node);
      auto localPath = path[0..pathLength];

      check(localPath);

      auto id = localPath.length == 0 ? "#" : localPath[0].id;

      enforce(id in cache, "There is a problem with the icon cache.");
      cache[id]["icons"] ~= mappedItem;
    }

    result["icons"] = cache["#"]["icons"];

    response.writeJsonBody(result, 200);

    return selector;
  }
}

struct TreeNode {
  string name;
  string id;
}