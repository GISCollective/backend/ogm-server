/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.icons.middleware.iconname;

import ogm.crates.all;
import ogm.models.icon;
import ogm.http.request;

import vibe.http.router;
import vibe.data.json;

import crate.base;
import crate.error;

import std.exception;
import std.string : strip;

class IconNameMiddleware {
  private {
    OgmCrates crates;
  }

  ///
  this(OgmCrates crates) {
    this.crates = crates;
  }

  /// If the icon name is changed than the old name is added to the otherNames field
  @any
  // @update @replace @patch
  IQuery update(IQuery selector, HTTPServerRequest req) {
    auto request = RequestUserData(req);

    if(req.json.type != Json.Type.object || "icon" !in req.json) {
      return selector;
    }

    auto icon = crates.icon.getItem(request.itemId).exec.front;
    enforce!CrateValidationException("name" in req.json["icon"], "The `icon.name` property is not set.");

    string name = req.json["icon"]["name"].to!string.strip;
    req.json["icon"]["name"] = name;

    if("otherNames" !in req.json["icon"] || req.json["icon"]["otherNames"].type != Json.Type.array) {
      req.json["icon"]["otherNames"] = Json.emptyArray;
    }

    if(name != icon["name"].to!string) {
      req.json["icon"]["otherNames"] ~= icon["name"].to!string;
    }

    return selector;
  }
}
