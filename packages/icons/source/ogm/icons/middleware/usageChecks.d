/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.icons.middleware.usageChecks;

import ogm.crates.all;
import ogm.http.request;

import vibe.http.router;
import vibe.data.json;

import crate.base;
import crate.error;
import std.exception;
import std.conv;

class IconUsageChecksMiddleware {
  private {
    OgmCrates crates;
  }

  ///
  this(OgmCrates crates) {
    this.crates = crates;
  }

  ///
  @delete_
  void delete_(HTTPServerRequest req) {
    auto iconId = req.params["id"];

    auto usageCount = crates.feature.get.where("icons").arrayContains(ObjectId.fromString(iconId)).and.size;
    enforce!CrateValidationException(usageCount == 0, "The icon can not be removed because is used by `" ~ usageCount.to!string ~ "` features.");

    usageCount = crates.campaign.get.where("optionalIcons").arrayContains(ObjectId.fromString(iconId)).and.size;
    enforce!CrateValidationException(usageCount == 0, "The icon can not be removed because is used by `" ~ usageCount.to!string ~ "` campaigns.");

    usageCount = crates.campaign.get.where("icons").arrayContains(ObjectId.fromString(iconId)).and.size;
    enforce!CrateValidationException(usageCount == 0, "The icon can not be removed because is used by `" ~ usageCount.to!string ~ "` campaigns.");
  }
}
