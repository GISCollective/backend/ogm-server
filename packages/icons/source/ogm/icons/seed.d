/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.icons.seed;

import std.file;
import std.path;
import std.string;

import crate.base;
import ogm.crates.all;
import vibe.data.json;
import vibe.core.log;


void setupDefaultIcons(OgmCrates crates) {
  auto iconSetId = crates.setupDefaultIconSet;

  if(iconSetId.type != Json.Type.string) {
    logInfo("The default icon set will not be updated.");
    return;
  }

  logInfo("Checking default icons for the icon set %s.", iconSetId);
  auto iconSet = IconSet(ObjectId.fromJson(iconSetId));

  auto files = dirEntries(buildPath("defaults", "icons"), "*.json", SpanMode.depth);

  foreach (file; files) {
    if(file.name.endsWith("set.json")) {
      continue;
    }

    auto iconJson = file.name.readText.parseJsonString;
    auto icon = LazyIcon.fromJson(iconJson);
    icon.iconSet = iconSet;
    icon.image.value = IconFile.fromString(iconJson["image"]["value"].to!string);

    auto iconRange = crates.icon.get
      .where("name").equal(icon.name).and
      .where("iconSet").equal(iconSet._id).and
      .exec;

    if(iconRange.empty) {
      crates.icon.addItem(icon.toJson);
    } else {
      icon._id = ObjectId.fromJson(iconRange.front["_id"]);
      crates.icon.updateItem(icon.toJson);
    }
  }
}

Json setupDefaultIconSet(OgmCrates crates) {
  auto iconSetJson = buildPath("defaults", "icons", "set.json").readText.parseJsonString;
  auto iconSet = LazyIconSet.fromJson(iconSetJson);

  auto iconSetRange = crates.iconSet.get.where("name").equal("GISCollective").and.exec;

  if(!iconSetRange.empty) {
    logInfo("The default icon set already exists with id %s.", iconSetRange.front["_id"]);
    return iconSetRange.front["_id"];
  }

  auto teamRange = crates.team.get.where("isDefault").equal(true).and.exec;
  iconSet.visibility.team = Team(ObjectId.fromJson(teamRange.front["_id"]));
  iconSet.name = "GISCollective";

  logInfo("The default icon set does not exist. Creating one for team `%s`.", teamRange.front["_id"]);
  auto item = crates.iconSet.addItem(iconSet.toJson);
  return item["_id"];
}
