/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.icons.api;

import vibe.http.router;
import crate.http.router;
import crate.base;
import crate.http.router;

import ogm.auth;
import ogm.crates.all;
import crate.resource.migration;

import ogm.middleware.adminrequest;
import ogm.middleware.userdata;
import ogm.middleware.translation;
import ogm.middleware.attributes.IconAttributesMapper;
import ogm.filter.visibility;

import ogm.filter.pagination;
import ogm.filter.searchterm;
import ogm.icons.mappers.IconMappers;
import ogm.icons.middleware.iconname;
import ogm.icons.middleware.output;
import ogm.icons.middleware.attributes;
import ogm.icons.middleware.IconParentMiddleware;
import ogm.icons.configuration;
import ogm.icons.CategoryTree;
import ogm.icons.TaxonomyTree;
import ogm.icons.IconCache;
import ogm.middleware.icons.IconRestoreImage;
import ogm.middleware.icons.IconFilter;
import ogm.middleware.features.GetFeaturesFilter;
import ogm.models.icon;
import ogm.icons.middleware.usageChecks;
import ogm.icons.middleware.computedVisibility;
import ogm.filter.mapFilter;
import ogm.middleware.SortingMiddleware;

public import ogm.icons.seed;

///
void setupIconApi(T)(CrateRouter!T crateRouter, OgmCrates crates, IconsConfiguration configuration) {
  IconSettings.baseUrl = configuration.baseUrl;
  IconSettings.path = configuration.location;

  IconSettings.files = crates.iconFiles;
  IconSettings.chunks = crates.iconChunks;
  PictureFileSettings.files = crates.pictureFiles;
  PictureFileSettings.chunks = crates.pictureChunks;

  auto auth = Authentication.instance(crates.user);

  auto cache = new IconCache(&crates.icon.get, &crates.iconSet.getItem);

  auto categoryTree = new CategoryTree(&crates.icon.get, &crates.iconSet.getItem, cache);
  auto taxonomyTree = new TaxonomyTree(&crates.icon.get, &crates.iconSet.getItem, cache);

  auto featuresFilter = new GetFeaturesFilter(auth.getCollection, crates, taxonomyTree);

  auto adminRequest = new AdminRequest(crates.user);
  auto userDataMiddleware = UserDataMiddleware.instance(crates);
  auto iconNameMiddleware = new IconNameMiddleware(crates);

  auto visibility = new VisibilityFilter!("features", true, true)(crates, crates.feature);
  auto mapFilter = new MapFilter(crates);
  auto iconFilter = new IconFilter(crates, featuresFilter, visibility, mapFilter);
  auto iconRestoreImage = new IconRestoreImage(crates);
  auto iconMappers = new IconMappers(crates, taxonomyTree);
  auto iconParent = new IconParentMiddlware(crates);
  auto iconAttributes = new IconAttributesMiddleware(crates, taxonomyTree);
  auto iconOutput = new IconOutput(iconMappers, categoryTree, crates);
  auto translate = new ModelTranslationMiddleware!Icon(crates);
  auto searchTermFilter = new SearchTermFilter;
  auto usageChecks = new IconUsageChecksMiddleware(crates);
  auto computedVisibility = new ComputedVisibilityMiddleware(crates);
  auto sorting = new SortingMiddleware(crates, "order", 1);

  auto paginationFilter = new PaginationFilter;

  crateGetters["IconSet"] = &crates.iconSet.getItem;
  crateGetters["Team"] = &crates.team.getItem;
  crateGetters["Picture"] = &crates.picture.getItem;

  crateRouter.prepare(crates.icon)
    .and(crates.icon)
    .and(auth.publicDataMiddleware)
    .and(adminRequest)
    .and(userDataMiddleware)
    .and(iconNameMiddleware)
    .and(iconAttributes)
    .and(iconRestoreImage)
    .and(iconParent)
    .and(iconFilter)
    .and(iconMappers)
    .and(usageChecks)
    .and(computedVisibility)
    .and(searchTermFilter)
    .and(translate)
    .and(iconOutput)
    .and(paginationFilter)
    .and(sorting);
}
