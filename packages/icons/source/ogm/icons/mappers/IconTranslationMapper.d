/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.icons.mappers.IconTranslationMapper;

import ogm.crates.all;
import ogm.models.icon;
import ogm.http.request;
import ogm.selectors.icons;

import vibe.http.router;
import vibe.data.json;

import crate.base;
import crate.error;
import crate.lazydata.base;
import crate.http.request;

import std.algorithm;
import std.array;
import std.conv;
import std.datetime;
import std.path;

///
class IconTranslationMapper {
  private {
    OgmCrates crates;
  }

  this(OgmCrates crates) {
    this.crates = crates;
  }

  @mapper
  void mapper(ref Json result) {
    result["localName"] = result["name"];

    if("attributes" !in result) {
      return;
    }

    foreach(attribute; result["attributes"]) {
      if("displayName" !in attribute || attribute["displayName"] == "") {
        attribute["displayName"] = attribute["name"];
      }
    }
  }
}
