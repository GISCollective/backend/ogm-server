/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.icons.mappers.IconMappers;

import ogm.icons.TaxonomyTree;
import ogm.icons.mappers.IconEditMapper;
import ogm.icons.mappers.IconImageMapper;
import ogm.icons.mappers.IconTranslationMapper;
import ogm.icons.mappers.IconStyleMapper;
import ogm.middleware.attributes.IconAttributesMapper;
import ogm.middleware.translation;
import ogm.icons.mappers.IconTranslationMapper;

import ogm.crates.all;

import crate.base;
import crate.http.request;
import crate.http.queryParams;

import vibe.data.json;
import vibe.http.router;

/// A mapper that wraps all the icon mappers
class IconMappers {

  private {
    IconEditMapper editMapper;
    IconImageMapper imageMapper;
    IconAttributesMapper attributesMapper;
    IconTranslationMapper translationMapper;
    IconStyleMapper styleMapper;
    ModelTranslationMiddleware!Icon translate;
  }

  this(OgmCrates crates, TaxonomyTree taxonomyTree) {
    editMapper = new IconEditMapper(crates);
    imageMapper = new IconImageMapper(crates, taxonomyTree);
    attributesMapper = new IconAttributesMapper(crates, taxonomyTree);
    styleMapper = new IconStyleMapper(crates);
    translate = new ModelTranslationMiddleware!Icon(crates);
    translationMapper = new IconTranslationMapper(crates);
  }

  @mapper
  void mapper(HTTPServerRequest req, ref Json result, ModelTranslationMiddleware!(Icon).Parameters params) {
    editMapper.mapper(req, result);
    imageMapper.mapper(req, result);
    result = attributesMapper.mapper(req, result);
    translationMapper.mapper(result);
    translate.mapper(req, result, params);
    styleMapper.mapper(req, result);
  }
}
