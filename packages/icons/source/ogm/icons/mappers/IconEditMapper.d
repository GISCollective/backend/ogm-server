/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.icons.mappers.IconEditMapper;

import ogm.crates.all;
import ogm.models.icon;
import ogm.http.request;
import ogm.selectors.icons;

import vibe.http.router;
import vibe.data.json;
import vibe.core.log;

import crate.base;
import crate.error;
import crate.lazydata.base;
import crate.http.request;

import std.algorithm;
import std.conv;
///
class IconEditMapper {
  private {
    OgmCrates crates;
  }

  this(OgmCrates crates) {
    this.crates = crates;
  }

  @mapper
  void mapper(HTTPServerRequest req, ref Json result) @trusted nothrow {
    try {
      scope request = RequestUserData(req);
      scope session = request.session(crates);

      if(request.isAdmin) {
        result["canEdit"] = true;
      } else if(request.userId != "@anonymous") {
        immutable set = result["iconSet"].to!string;
        result["canEdit"] = session.owner.iconSets.canFind(set);
      }
    } catch(Exception e) {
      try logError("Error on adding `canEdit`: %s", e.message); catch(Exception) {}
    }
  }
}
