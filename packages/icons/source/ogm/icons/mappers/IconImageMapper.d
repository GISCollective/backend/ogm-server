/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.icons.mappers.IconImageMapper;

import ogm.icons.TaxonomyTree;
import ogm.crates.all;
import ogm.models.icon;
import ogm.http.request;
import ogm.selectors.icons;

import vibe.http.router;
import vibe.data.json;

import crate.base;
import crate.http.request;
import crate.url;
import gis_collective.hmq.log;

import std.path;

///
class IconImageMapper {

  private {
    TaxonomyTree iconsTree;
    OgmCrates crates;
  }

  this(OgmCrates crates, TaxonomyTree iconsTree) {
    this.iconsTree = iconsTree;
    this.crates = crates;
  }

  string getFirstParentIconImage(string iconId, string[] iconSetIds) {
    auto parentIds = iconsTree.parentIconIds(iconId, iconSetIds);

    foreach(id; parentIds) {
      auto icon = iconsTree.getIcon(id);

      if(icon["image"].type == Json.Type.object && icon["image"]["value"].type == Json.Type.string) {
        return icon["_id"].to!string;
      }
    }

    return iconId;
  }

  ///
  void mapper(HTTPServerRequest req, ref Json result) @trusted nothrow {
    try {
      string iconId = result["_id"].to!string;
      auto baseUrl = IconSettings.baseUrl.replaceHost(req.host, req.tls);

      if("image" !in result || result["image"].type != Json.Type.object) {
        result["image"] = Json.emptyObject;
        result["image"]["value"] = "";
      }

      if(result["image"]["value"].type != Json.Type.string) {
        result["image"]["value"] = "";
      }

      if(result["image"]["value"] != "") {
        result["image"]["value"] = baseUrl.buildPath("icons", iconId, "image", "value");
      }

      if(result["image"]["useParent"].type != Json.Type.bool_) {
        result["image"]["useParent"] = false;
      }

      if(result["image"]["useParent"] == true) {
        scope request = RequestUserData(req);
        auto iconSetIds = request.isAdmin ? ["*"] : request.session(crates).all.iconSets;

        iconId = this.getFirstParentIconImage(iconId, iconSetIds);
        result["image"]["value"] = baseUrl.buildPath("icons", iconId, "image", "value");
      }
    } catch(Exception e) {
      error(e);
    }
  }
}
