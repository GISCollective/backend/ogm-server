/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.icons.mappers.IconStyleMapper;

import ogm.crates.all;
import ogm.models.icon;
import ogm.http.request;
import ogm.selectors.icons;
import gis_collective.hmq.log;

import vibe.http.router;
import vibe.data.json;

import crate.base;
import crate.error;
import crate.lazydata.base;
import crate.http.request;

import std.algorithm;
import std.array;
import std.conv;
import std.datetime;
import std.path;

alias LazyStyle = LazyField!Json;

///
class IconStyleMapper {
  private {
    OgmCrates crates;
    LazyStyle[string] styles;
  }

  this(OgmCrates crates) {
    this.crates = crates;
  }

  void fillStyle(ref Json item) {
    string setId = item["iconSet"].to!string;

    if("styles" !in item) {
      item["styles"] = Json.emptyObject;
    }

    if("hasCustomStyle" !in item["styles"]) {
      item["styles"]["hasCustomStyle"] = false;
    }

    if(setId !in styles) {
      styles[setId] = LazyField!Json({
        auto r = crates.iconSet.get.where("_id").equal(ObjectId.fromString(setId)).and.withProjection(["styles"]).exec;

        if(r.empty) {
          return Json.emptyObject;
        }

        return r.front["styles"];
      }, 10.seconds);
    }

    item["styles"]["types"] = styles[setId].compute;
  }

  @mapper
  Json mapper(HTTPServerRequest req, ref Json result) @trusted nothrow {
    try {
      if("styles" !in result || "hasCustomStyle" !in result["styles"] || !result["styles"]["hasCustomStyle"]) {
        this.fillStyle(result);
      }
    } catch(Exception e) { error(e); }

    return result;
  }
}
