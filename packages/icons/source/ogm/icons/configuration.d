/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.icons.configuration;

import vibe.service.configuration.general;

///
struct MicroIconsConfig {
  /// Configurations for all services
  GeneralConfig general;

  ///
  IconsConfiguration icons;
}

struct IconsConfiguration {
  string location;
  string baseUrl;
}
