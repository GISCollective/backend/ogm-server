/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.icons.delete_;

import tests.fixtures;
import ogm.icons.configuration;
import ogm.icons.api;

alias suite = Spec!({
  describe("DELETE", {
    URLRouter router;

    beforeEach({
      setupTestData();

      IconsConfiguration config;
      config.baseUrl = "http://localhost";
      config.location = "./tmp-icons";

      router = new URLRouter;
      router.crateSetup.setupIconApi(crates, config);
    });

    describe("when the icon belongs to a feature", {
      it("responds with an error", {
        router
          .request
          .delete_("/icons/000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal("{
                \"errors\": [{
                  \"description\": \"The icon can not be removed because is used by `4` features.\",
                  \"status\": 400,
                  \"title\": \"Validation error\"
                }]
            }".parseJsonString);
          });
      });
    });

    describe("when an icon is set as optional icon to a campaign", {
      beforeEach({
        auto range = crates.feature.get.and.exec;

        foreach (feature; range) {
          crates.feature.deleteItem(feature["_id"].to!string);
        }

        auto campaign = crates.campaign.get.where("_id").equal("000000000000000000000001").and.exec.front;
        campaign["optionalIcons"] = ["000000000000000000000001"].serializeToJson;
        crates.campaign.updateItem(campaign);
      });

      it("responds with an error", {
        router
          .request
          .delete_("/icons/000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal("{
                \"errors\": [{
                  \"description\": \"The icon can not be removed because is used by `1` campaigns.\",
                  \"status\": 400,
                  \"title\": \"Validation error\"
                }]
            }".parseJsonString);
          });
      });
    });

    describe("when an icon is set as mandatory icon to a campaign", {
      beforeEach({
        auto range = crates.feature.get.and.exec;

        foreach (feature; range) {
          crates.feature.deleteItem(feature["_id"].to!string);
        }

        auto campaign = crates.campaign.get.where("_id").equal("000000000000000000000001").and.exec.front;
        campaign["icons"] = ["000000000000000000000001"].serializeToJson;
        crates.campaign.updateItem(campaign);
      });

      it("responds with an error", {
        router
          .request
          .delete_("/icons/000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal("{
                \"errors\": [{
                  \"description\": \"The icon can not be removed because is used by `1` campaigns.\",
                  \"status\": 400,
                  \"title\": \"Validation error\"
                }]
            }".parseJsonString);
          });
      });
    });

    describe("when an icon is not added to a feature", {
      beforeEach({
        auto range = crates.feature.get.and.exec;

        foreach (feature; range) {
          crates.feature.deleteItem(feature["_id"].to!string);
        }
      });

      describeCredentialsRule("delete team records", "yes", "icons", ["administrator", "owner"], (string type) {
        router
          .request
          .delete_("/icons/000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(204)
          .end((Response response) => () {
            crates.icon.get
              .where("_id")
              .equal(ObjectId.fromString("000000000000000000000001"))
              .and
              .exec
              .empty
              .should.equal(true);
          });
      });

      describeCredentialsRule("delete team records", "no", "icons", ["leader", "member", "guest"], (string type) {
        router
          .request
          .delete_("/icons/000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"errors\":[{
                \"description\": \"You don't have enough rights to remove `000000000000000000000001`.\",
                \"status\": 400,
                \"title\": \"Can not remove icon.\"}]}".parseJsonString);
          });
      });

      describeCredentialsRule("delete team records", "-", "icons", "no rights", {
        router
          .request
          .delete_("/icons/000000000000000000000001")
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{ "error": "Authorization required" }`.parseJsonString);
          });
      });


      describeCredentialsRule("delete other team records", "yes", "icons", "administrator", (string type) {
        router
          .request
          .delete_("/icons/000000000000000000000003")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(204)
          .end((Response response) => () {
            crates.icon.get
              .where("_id")
              .equal(ObjectId.fromString("000000000000000000000003"))
              .and
              .exec
              .empty
              .should.equal(true);
          });
      });

      describeCredentialsRule("delete other team records", "no", "icons", ["owner", "leader", "member", "guest"], (string type) {
        router
          .request
          .delete_("/icons/000000000000000000000003")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(404)
          .end((Response response) => () {
              response.bodyJson.should.equal("{\"errors\":[{
                \"description\": \"Icon `000000000000000000000003` not found.\",
                \"status\": 404,
                \"title\": \"Icon not found.\"}]}".parseJsonString);
          });
      });

      describeCredentialsRule("delete other team records", "-", "icons", "no rights", {
        router
          .request
          .delete_("/icons/000000000000000000000003")
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{ "error": "Authorization required" }`.parseJsonString);
          });
      });
    });

    describe("with an invalid token", {
      it("should return 400", {
        router
          .request
          .delete_("/icons/000000000000000000000001")
          .header("Authorization", "Bearer random")
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"error\": \"Invalid token.\"}".parseJsonString);
          });
      });
    });
  });
});
