/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.icons.attributes;

import tests.fixtures;
import std.path;
import ogm.icons.configuration;
import ogm.icons.api;

alias suite = Spec!({
  describe("GET", {
    URLRouter router;

    beforeEach({
      setupTestData();

      IconsConfiguration config;

      router = new URLRouter;
      router.crateSetup.setupIconApi(crates, config);

      IconSettings.baseUrl = "http://localhost/";
    });

    afterEach({
      IconSettings.baseUrl = "";
    });

    describe("for an icon with a public parent", {
      Json rootIconJson;
      Json iconLevel1Json;

      beforeEach({
        Icon rootIcon;
        rootIcon._id = ObjectId.fromString("5");
        rootIcon.name = "root";
        rootIcon.iconSet = iconSet1;
        rootIcon.attributes = [ IconAttribute("root attribute", "short text") ];

        rootIconJson = createIcon(rootIcon);

        Icon iconLevel1;
        iconLevel1._id = ObjectId.fromString("6");
        iconLevel1.name = "level1";
        iconLevel1.iconSet = iconSet1;
        iconLevel1.parent = rootIconJson["_id"].to!string;
        iconLevel1.attributes = [ IconAttribute("level1 attribute", "short text") ];
        iconLevel1.image.useParent = true;

        iconLevel1Json = createIcon(iconLevel1);
      });

      describe("without a token", {
        it("should return the icon from level1 with the inherited icons", {
          router
            .request
            .get("/icons/" ~ iconLevel1Json["_id"].to!string)
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["icon"]["attributes"].should.equal((`[{
                "help":"",
                "isPrivate":false,
                "isRequired": false,
                "name":"root attribute",
                "otherNames": [],
                "displayName":"root attribute",
                "type":"short text",
                "options":"",
                "isInherited": true,
                "from":{"icon":"000000000000000000000005","iconSet":"000000000000000000000001"}
              }, {
                "help":"",
                "isPrivate":false,
                "isRequired": false,
                "name":"level1 attribute",
                "otherNames": [],
                "displayName":"level1 attribute",
                "type":"short text",
                "options":"",
                "isInherited": false,
                "from":{"icon":"000000000000000000000006","iconSet":"000000000000000000000001"}
              }]`).parseJsonString);
            });
        });

        it("should set the parent image file if the requested icon has none set", {
          router
            .request
            .get("/icons/" ~ iconLevel1Json["_id"].to!string)
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["icon"]["image"]["value"].should.equal("http://localhost/icons/000000000000000000000006/image/value");
            });
        });
      });
    });

    describe("for an icon with a private parent", {
      Json rootIconJson;
      Json iconLevel1Json;

      beforeEach({
        auto privateIconSet = IconSet();

        privateIconSet._id = ObjectId.fromString("5");
        privateIconSet.visibility = Visibility(team1, false, false);
        privateIconSet.name = "private set";
        createIconSet(privateIconSet);

        Icon rootIcon;
        rootIcon._id = ObjectId.fromString("5");
        rootIcon.name = "root";
        rootIcon.iconSet = privateIconSet;
        rootIcon.attributes = [ IconAttribute("root attribute", "short text") ];

        rootIconJson = createIcon(rootIcon);

        Icon iconLevel1;
        iconLevel1._id = ObjectId.fromString("6");
        iconLevel1.name = "level1";
        iconLevel1.iconSet = iconSet1;
        iconLevel1.parent = rootIconJson["_id"].to!string;
        iconLevel1.attributes = [ IconAttribute("level1 attribute", "short text") ];

        iconLevel1Json = createIcon(iconLevel1);
      });

      describe("without a token", {
        it("should not add attributes from private inherited icons", {
          router
            .request
            .get("/icons/" ~ iconLevel1Json["_id"].to!string)
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["icon"]["attributes"].should.equal((`[{
                  "help":"",
                  "isPrivate":false,
                  "isRequired": false,
                  "name":"level1 attribute",
                  "otherNames": [],
                  "displayName":"level1 attribute",
                  "type":"short text",
                  "options":"",
                  "isInherited": false,
                  "from":{"icon":"000000000000000000000006","iconSet":"000000000000000000000001"}
                }]`).parseJsonString);
            });
        });
      });

      describe("with a guest token", {
        it("should add attributes from private inherited icons", {
          router
            .request
            .get("/icons/" ~ iconLevel1Json["_id"].to!string)
            .header("Authorization", "Bearer " ~ bearerGuestToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["icon"]["attributes"].should.equal((`[{
                "help":"",
                "isPrivate":false,
                "isRequired": false,
                "name":"root attribute",
                "otherNames": [],
                "displayName":"root attribute",
                "type":"short text",
                "options":"",
                "isInherited": true,
                "from":{"icon":"000000000000000000000005","iconSet":"000000000000000000000005"}
              }, {
                "help":"",
                "isPrivate":false,
                "isRequired": false,
                "name":"level1 attribute",
                "otherNames": [],
                "displayName":"level1 attribute",
                "type":"short text",
                "options":"",
                "isInherited": false,
                "from":{"icon":"000000000000000000000006","iconSet":"000000000000000000000001"}
              }]`).parseJsonString);
            });
        });
      });

      describe("with an admin token", {
        it("should add attributes from private inherited icons", {
          router
            .request
            .get("/icons/" ~ iconLevel1Json["_id"].to!string)
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["icon"]["attributes"].should.equal((`[{
                "help":"",
                "isPrivate":false,
                "isRequired": false,
                "name":"root attribute",
                "otherNames": [],
                "displayName":"root attribute",
                "type":"short text",
                "options":"",
                "isInherited": true,
                "from":{"icon":"000000000000000000000005","iconSet":"000000000000000000000005"}
              }, {
                "help":"",
                "isPrivate":false,
                "isRequired": false,
                "name":"level1 attribute",
                "otherNames": [],
                "displayName":"level1 attribute",
                "type":"short text",
                "options":"",
                "isInherited": false,
                "from":{"icon":"000000000000000000000006","iconSet":"000000000000000000000001"}
              }]`)
                .parseJsonString);
            });
        });
      });
    });
  });
});
