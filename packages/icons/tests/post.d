/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.icons.post;

import tests.fixtures;

import std.algorithm;
import std.array;
import ogm.icons.configuration;
import ogm.icons.api;

alias suite = Spec!({
  URLRouter router;
  Json data;
  Json dataOtherTeam;

  describe("POST", {
    beforeEach({
      setupTestData();

      IconsConfiguration config;
      config.baseUrl = "http://localhost";
      config.location = "./tmp-icons";

      router = new URLRouter;
      router.crateSetup.setupIconApi(crates, config);

      data = `{"icon": {
                "image": {
                  "useParent": false,
                  "value": ""
                },
                "subcategory": "new subcategory",
                "name": "new name",
                "category": "category3",
                "iconSet": "000000000000000000000001",
                "description": "",
                "attributes": [],
                "parent": "",
                "otherNames": [] }}`.parseJsonString;

      dataOtherTeam = `{"icon": {
                "image": {
                  "useParent": false,
                  "value": ""
                },
                "subcategory": "new subcategory",
                "name": "new name",
                "category": "new category",
                "iconSet": "000000000000000000000003",
                "description": "",
                "attributes": [],
                "parent": "",
                "otherNames": [] }}`.parseJsonString;
    });

    describeCredentialsRule("create for team", "yes", "icons", ["administrator", "owner"], (string type) {
      router
        .request
        .post("/icons")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"icon": {
            "_id": "000000000000000000000005",
            "image": {
              "useParent": false,
              "value": ""
            },
            "allowMany": false,
            "subcategory": "new subcategory",
            "name": "new name",
            "localName": "new name",
            "enforceSvg": false,
            "order": 999,
            "measurements": {
              "stretchX": [],
              "height": 0,
              "width": 0,
              "content": [],
              "stretchY": []
            },
            "category": "category3",
            "canEdit": true,
            "verticalIndex": 0,
            "minZoom": 0,
            "maxZoom": 20,
            "keepWhenSmall": true,
            "iconSet": "000000000000000000000001",
            "description": "",
            "attributes": [],
            "parent": "",
            "otherNames": [],
            "visibility": {
              "isDefault": true,
              "isPublic": true,
              "team": "000000000000000000000001"
            },
            "styles": {
              "hasCustomStyle": false,
              "types": {
                "lineMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                "polygonMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                "polygon": { "backgroundColor": "rgba(255,255,255,0.6)", "hideBackgroundOnZoom": false, "showAsLineAfterZoom": 99, "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
                "site": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                "lineLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
                "line": { "markerInterval": 700, "backgroundColor": "rgba(255,255,255,0.6)", "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
                "siteLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
                "polygonLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 }
              }
            }
          }}`.parseJsonString);
      });
    });

    describeCredentialsRule("create for team", "no", "icons", ["leader", "member", "guest"], (string type) {
      router
        .request
        .post("/icons")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(dataOtherTeam)
        .expectStatusCode(404)
        .end((Response response) => () {
          response.bodyJson.should.equal("{ \"errors\": [
              {
                \"description\": \"Can not add an icon for icon set `000000000000000000000003`.\",
                \"title\": \"Crate not found\",
                \"status\": 404
              }]}".parseJsonString);
        });
    });

    describeCredentialsRule("create for team", "-", "icons", "no rights", {
      router
        .request
        .post("/icons")
        .send(data)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "error": "Authorization required" }`.parseJsonString);
        });
    });


    describeCredentialsRule("create for other team", "yes", "icons", "administrator", (string type) {
      router
        .request
        .post("/icons")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(dataOtherTeam)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"icon": {
            "_id": "000000000000000000000005",
            "image": {"useParent": false, "value": "" },
            "allowMany": false,
            "subcategory": "new subcategory",
            "name": "new name",
            "order": 999,
            "localName": "new name",
            "category": "new category",
            "canEdit": true,
            "verticalIndex": 0,
            "minZoom": 0,
            "maxZoom": 20,
            "keepWhenSmall": true,
            "iconSet": "000000000000000000000003",
            "description": "",
            "attributes": [],
            "enforceSvg": false,
            "measurements": {
              "stretchX": [],
              "height": 0,
              "width": 0,
              "content": [],
              "stretchY": []
            },
            "parent": "",
            "otherNames": [],
            "visibility": {
              "isDefault": false,
              "isPublic": true,
              "team": "000000000000000000000003"
            },
            "styles": {
              "hasCustomStyle": false,
              "types": {
                "lineMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                "polygonMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                "polygon": { "showAsLineAfterZoom": 99, "backgroundColor": "rgba(255,255,255,0.6)", "hideBackgroundOnZoom": false, "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
                "site": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                "lineLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
                "line": { "markerInterval": 700, "backgroundColor": "rgba(255,255,255,0.6)", "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
                "siteLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
                "polygonLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 }
              }
            }
          }}`.parseJsonString);
      });
    });

    describeCredentialsRule("create for other team", "no", "icons", ["owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .post("/icons")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(dataOtherTeam)
        .expectStatusCode(404)
        .end((Response response) => () {
          response.bodyJson.should.equal("{ \"errors\": [
              {
                \"description\": \"Can not add an icon for icon set `000000000000000000000003`.\",
                \"title\": \"Crate not found\",
                \"status\": 404
              }]}".parseJsonString);
        });
    });

    describeCredentialsRule("create for other team", "-", "icons", "no rights", {
      router
        .request
        .post("/icons")
        .send(dataOtherTeam)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "error": "Authorization required" }`.parseJsonString);
        });
    });

    describe("a request without token", {
      it("should return an error", {
        router
          .request
          .post("/icons")
          .send(data)
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "error": "Authorization required"
            }`.parseJsonString);
          });
      });
    });
  });
});
