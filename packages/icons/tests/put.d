/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.icons.put;

import tests.fixtures;

import std.algorithm;
import std.array;
import std.file;
import ogm.icons.configuration;
import ogm.icons.api;

alias suite = Spec!({
  URLRouter router;
  Json data;
  Json dataWithImage;

  describe("PUT", {
    before({
      IconSettings.path = "./tmp-icons";

      if(!"./tmp-icons".exists) {
        mkdir("./tmp-icons");
      }
    });

    beforeEach({
      setupTestData();

      IconsConfiguration config;
      config.baseUrl = "http://localhost";
      config.location = "./tmp-icons";

      if(!"./tmp-icons".exists) {
        mkdir("./tmp-icons");
      }

      router = new URLRouter;
      router.crateSetup.setupIconApi(crates, config);

      data = `{ "icon":{
                "subcategory": "new subcategory",
                "name": "new name",
                "category": "category3",
                "iconSet": "000000000000000000000001",
                "description": "",
                "attributes": [],
                "parent": "" }}`.parseJsonString;

      dataWithImage = `{ "icon": {
                "subcategory": "new subcategory",
                "name": "new name",
                "category": "category3",
                "image": {"useParent": false, "value": "http://localhost/icons/000000000000000000000003/image/value"},
                "iconSet": "000000000000000000000001",
                "description": "",
                "attributes": [],
                "parent": "",
                "otherNames": [] }}`.parseJsonString;
    });


    describeCredentialsRule("edit other team records", "yes", "icons", "administrator", {
      router
        .request
        .put("/icons/000000000000000000000003")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "icon": {
            "image": { "useParent": false, "value": "" },
            "allowMany": false,
            "subcategory": "new subcategory",
            "name": "new name",
            "localName": "new name",
            "_id": "000000000000000000000003",
            "category": "category3",
            "canEdit": true,
            "order": 999,
            "iconSet": "000000000000000000000001",
            "description": "",
            "enforceSvg": false,
            "visibility": {
              "isDefault": true,
              "isPublic": true,
              "team": "000000000000000000000001"
            },
            "measurements": {
              "stretchX": [],
              "height": 0,
              "width": 0,
              "content": [],
              "stretchY": []
            },
            "attributes": [],
            "parent": "",
            "verticalIndex": 0,
            "minZoom": 0,
            "maxZoom": 20,
            "keepWhenSmall": true,
            "otherNames": [ "name3" ],
            "styles": {
              "hasCustomStyle": false,
              "types": {
                "lineMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                "polygonMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                "polygon": { "backgroundColor": "rgba(255,255,255,0.6)", "hideBackgroundOnZoom": false, "showAsLineAfterZoom": 99, "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
                "site": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                "lineLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
                "line": { "markerInterval": 700, "backgroundColor": "rgba(255,255,255,0.6)", "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
                "siteLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
                "polygonLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 }
              }
            }
          }}`.parseJsonString);
        });
    });

    describeCredentialsRule("edit other team records", "no", "icons", ["owner", "member", "guest", "leader"], (string type) {
      router
        .request
        .put("/icons/000000000000000000000003")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(data)
        .expectStatusCode(404)
        .end((Response response) => () {
            response.bodyJson.should.equal("{
              \"errors\": [
                {
                  \"description\": \"Icon `000000000000000000000003` not found.\",
                  \"title\": \"Icon not found.\",
                  \"status\": 404
                }
              ]
            }".parseJsonString);
        });
    });

    describeCredentialsRule("edit other team records", "-", "icons", "no rights", {
      router
        .request
        .put("/icons/000000000000000000000003")
        .send(data)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "error": "Authorization required" }`.parseJsonString);
        });
    });


    describeCredentialsRule("edit team records", "yes", "icons", ["administrator", "owner"], (string type) {
      router
        .request
        .put("/icons/000000000000000000000002")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "icon": {
            "image": { "useParent": false, "value": ""},
            "allowMany": false,
            "subcategory": "new subcategory",
            "name": "new name",
            "localName": "new name",
            "verticalIndex": 0,
            "minZoom": 0,
            "maxZoom": 20,
            "keepWhenSmall": true,
            "_id": "000000000000000000000002",
            "category": "category3",
            "canEdit": true,
            "iconSet": "000000000000000000000001",
            "description": "",
            "attributes": [],
            "order": 999,
            "parent": "",
            "enforceSvg": false,
            "visibility": {
              "isDefault": true,
              "isPublic": true,
              "team": "000000000000000000000001"
            },
            "measurements": {
              "stretchX": [],
              "height": 0,
              "width": 0,
              "content": [],
              "stretchY": []
            },
            "otherNames": [ "name2" ],
            "styles": {
              "hasCustomStyle": false,
              "types": {
                "lineMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                "polygonMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                "polygon": { "backgroundColor": "rgba(255,255,255,0.6)", "hideBackgroundOnZoom": false, "showAsLineAfterZoom": 99, "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
                "site": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                "lineLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
                "line": { "markerInterval": 700, "backgroundColor": "rgba(255,255,255,0.6)", "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
                "siteLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
                "polygonLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 }
              }
            }
          }}`.parseJsonString);
        });
    });

    describeCredentialsRule("edit team records", "no", "icons", ["member", "guest", "leader"], (string type) {
      router
        .request
        .put("/icons/000000000000000000000002")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(data)
        .expectStatusCode(400)
        .end((Response response) => () {
          response.bodyJson.should.equal("{\"errors\": [{
                  \"description\": \"You don't have enough rights to edit `000000000000000000000002`.\",
                  \"title\": \"Can not edit icon.\",
                  \"status\": 400
                }]}".parseJsonString);
        });
    });

    describeCredentialsRule("edit team records", "-", "icons", "no rights", {
      router
        .request
        .put("/icons/000000000000000000000002")
        .send(data)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "error": "Authorization required" }`.parseJsonString);
        });
    });

    describe("a request without token", {
      it("should return an error", {
        router
          .request
          .put("/icons/000000000000000000000003")
          .send(data)
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "error": "Authorization required"
            }`.parseJsonString);
          });
      });
    });

    describe("a request with an administrator token", {
      it("should ignore the image if it a new one is not provided", {
        router
          .request
          .put("/icons/000000000000000000000001")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .send(dataWithImage)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{ "icon": {
              "image": { "useParent": false, "value": "" },
              "allowMany": false,
              "subcategory": "new subcategory",
              "name": "new name",
              "localName": "new name",
              "verticalIndex": 0,
              "minZoom": 0,
              "maxZoom": 20,
              "keepWhenSmall": true,
              "_id": "000000000000000000000001",
              "category": "category3",
              "canEdit": true,
              "iconSet": "000000000000000000000001",
              "description": "",
              "attributes": [],
              "parent": "",
              "otherNames": [ "name1" ],
              "order": 999,
              "enforceSvg": false,
              "visibility": {
                "isDefault": true,
                "isPublic": true,
                "team": "000000000000000000000001"
              },
              "measurements": {
                "stretchX": [],
                "height": 0,
                "width": 0,
                "content": [],
                "stretchY": []
              },
              "styles": {
                "hasCustomStyle": false,
                "types": {
                  "lineMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                  "polygonMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                  "polygon": { "backgroundColor": "rgba(255,255,255,0.6)", "hideBackgroundOnZoom": false, "showAsLineAfterZoom": 99, "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
                  "site": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                  "lineLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
                  "line": { "markerInterval": 700, "backgroundColor": "rgba(255,255,255,0.6)", "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
                  "siteLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
                  "polygonLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 }
                }
              }
            }}`.parseJsonString);
          });
      });

      it("should accept a new image if it is in base64 format", {
        auto data = dataWithImage.clone;
        data["icon"]["image"]["value"] = "data:text/plain;base64,";

        router
          .request
          .put("/icons/000000000000000000000001")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            auto filename = crates.icon
              .getItem("000000000000000000000001")
              .exec.front["image"]["value"].to!string;

            response.bodyJson.should.equal(`{ "icon": {
              "image": { "useParent": false, "value": "http://localhost/icons/000000000000000000000001/image/value"},
              "allowMany": false,
              "verticalIndex": 0,
              "minZoom": 0,
              "maxZoom": 20,
              "keepWhenSmall": true,
              "subcategory": "new subcategory",
              "name": "new name",
              "localName": "new name",
              "_id": "000000000000000000000001",
              "category": "category3",
              "canEdit": true,
              "iconSet": "000000000000000000000001",
              "description": "",
              "attributes": [],
              "order": 999,
              "parent": "",
              "otherNames": [ "name1" ],
              "enforceSvg": false,
              "visibility": {
                "isDefault": true,
                "isPublic": true,
                "team": "000000000000000000000001"
              },
              "measurements": {
                "stretchX": [],
                "height": 0,
                "width": 0,
                "content": [],
                "stretchY": []
              },
              "styles": {
                "hasCustomStyle": false,
                "types": {
                  "lineMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                  "polygonMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                  "polygon": { "backgroundColor": "rgba(255,255,255,0.6)", "hideBackgroundOnZoom": false, "showAsLineAfterZoom": 99, "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
                  "site": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                  "lineLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
                  "line": { "markerInterval": 700, "backgroundColor": "rgba(255,255,255,0.6)", "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
                  "siteLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
                  "polygonLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 }
                }
              }
            }}`.parseJsonString);
          });
      });

      it("should accept an icon with a null parent", {
        auto data = dataWithImage.clone;
        data["icon"]["parent"] = null;

        router
          .request
          .put("/icons/000000000000000000000001")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{ "icon": {
              "image": { "useParent": false, "value": "" },
              "allowMany": false,
              "verticalIndex": 0,
              "minZoom": 0,
              "maxZoom": 20,
              "enforceSvg": false,
              "keepWhenSmall": true,
              "subcategory": "new subcategory",
              "name": "new name",
              "localName": "new name",
              "_id": "000000000000000000000001",
              "category": "category3",
              "canEdit": true,
              "iconSet": "000000000000000000000001",
              "description": "",
              "attributes": [],
              "order": 999,
              "parent": "",
              "otherNames": [ "name1" ],
              "visibility": {
                "isDefault": true,
                "isPublic": true,
                "team": "000000000000000000000001"
              },
              "measurements": {
                "stretchX": [],
                "height": 0,
                "width": 0,
                "content": [],
                "stretchY": []
              },
              "styles": {
                "hasCustomStyle": false,
                "types": {
                  "lineMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                  "polygonMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                  "polygon": { "backgroundColor": "rgba(255,255,255,0.6)", "hideBackgroundOnZoom": false, "showAsLineAfterZoom": 99, "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
                  "site": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                  "lineLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
                  "line": { "markerInterval": 700, "backgroundColor": "rgba(255,255,255,0.6)", "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
                  "siteLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
                  "polygonLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 }
                }
              }
            }}`.parseJsonString);
        });
      });

      it("should not clear the category and subcategory when the icon parent is set", {
        auto data = dataWithImage.clone;
        data["icon"]["parent"] = "000000000000000000000002";

        router
          .request
          .put("/icons/000000000000000000000001")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyString.should.not.startWith(`{"errors":[{"`);

            auto item = crates.icon.getItem("000000000000000000000001").exec.front;

            item["category"].to!string.should.equal("category3");
            item["subcategory"].to!string.should.equal("new subcategory");
        });
      });

      it("should be able to add an attribute", {
        auto data = dataWithImage.clone;
        data["icon"]["parent"] = "000000000000000000000002";
        data["icon"]["attributes"] = `[{ "name":"new test","type":"new test", "from": {} }]`.parseJsonString;

        router
          .request
          .put("/icons/000000000000000000000001")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyString.should.not.startWith(`{"errors":[{"`);
            auto icon = crates.icon.getItem("000000000000000000000001").exec.front;
            auto parent = crates.icon.getItem("000000000000000000000002").exec.front;

            icon["attributes"].length.should.equal(1);
            icon["attributes"][0].should.equal(`{
              "type": "new test",
              "name": "new test",
              "otherNames": [],
              "options": "",
              "isInherited": false,
              "help": "",
              "isPrivate": false,
              "isRequired": false,
              "displayName": "" }`.parseJsonString);
            parent["attributes"].length.should.equal(0);

            response.bodyJson["icon"]["attributes"].length.should.equal(1);
            response.bodyJson["icon"]["attributes"][0].should.equal(`{
              "type": "new test",
              "otherNames": [],
              "from": {
                "icon": "000000000000000000000001",
                "iconSet": "000000000000000000000001"
              },
              "isInherited": false,
              "help": "",
              "displayName": "new test",
              "isPrivate": false,
              "isRequired": false,
              "options": "",
              "name": "new test" }`.parseJsonString);
        });
      });

      it("does not allow adding attributes with the . char", {
        auto data = dataWithImage.clone;
        data["icon"]["parent"] = "000000000000000000000002";
        data["icon"]["attributes"] = `[{ "name":"new.test","type":"new test", "from": {} }]`.parseJsonString;

        router
          .request
          .put("/icons/000000000000000000000001")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .send(data)
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "errors":[{"description":"The attributes are not allowed to have the '.' char in their name.","status":400,"title":"Validation error"}]
            }`.parseJsonString);
        });
      });

      it("should be able to add a required attribute", {
        auto data = dataWithImage.clone;
        data["icon"]["parent"] = "000000000000000000000002";
        data["icon"]["attributes"] = `[{ "name":"new test","type":"new test", "from": {} }]`.parseJsonString;
        data["icon"]["attributes"][0]["isRequired"] = true;

        router
          .request
          .put("/icons/000000000000000000000001")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyString.should.not.startWith(`{"errors":[{"`);
            auto icon = crates.icon.getItem("000000000000000000000001").exec.front;
            auto parent = crates.icon.getItem("000000000000000000000002").exec.front;

            icon["attributes"].length.should.equal(1);
            icon["attributes"][0].should.equal(`{
              "type": "new test",
              "name": "new test",
              "otherNames": [],
              "options": "",
              "isInherited": false,
              "help": "",
              "isPrivate": false,
              "isRequired": true,
              "displayName": "" }`.parseJsonString);
            parent["attributes"].length.should.equal(0);

            response.bodyJson["icon"]["attributes"].length.should.equal(1);
            response.bodyJson["icon"]["attributes"][0].should.equal(`{
              "type": "new test",
              "otherNames": [],
              "from": {
                "icon": "000000000000000000000001",
                "iconSet": "000000000000000000000001"
              },
              "isInherited": false,
              "help": "",
              "displayName": "new test",
              "isPrivate": false,
              "isRequired": true,
              "options": "",
              "name": "new test" }`.parseJsonString);
        });
      });

      it("should not add attributes that are present in the parents attribute list when no property is changed", {
        auto data = dataWithImage.clone;
        data["icon"]["parent"] = "000000000000000000000002";
        data["icon"]["attributes"] = `[{ "name":"test","type":"test", "from": { "icon": "000000000000000000000002", "iconSet": "000000000000000000000002" } }]`
          .parseJsonString;

        auto parent = crates.icon.getItem("000000000000000000000002").exec.front;
        parent["attributes"] = data["icon"]["attributes"];
        crates.icon.updateItem(parent);

        router
          .request
          .put("/icons/000000000000000000000001")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyString.should.not.startWith(`{"errors":[{"`);
            auto icon = crates.icon.getItem("000000000000000000000001").exec.front;
            auto parent = crates.icon.getItem("000000000000000000000002").exec.front;

            icon["attributes"].length.should.equal(0);

            response.bodyJson["icon"]["attributes"].length.should.equal(1);
            response.bodyJson["icon"]["attributes"][0].should.equal(`{ "type": "test",
              "from": {
                "icon": "000000000000000000000002",
                "iconSet": "000000000000000000000002"
              },
              "isInherited": true,
              "displayName": "test",
              "name": "test" }`.parseJsonString);
        });
      });

      it("should add attributes that are present in the parents attribute list when the type is changed", {
        auto data = dataWithImage.clone;
        data["icon"]["parent"] = "000000000000000000000002";
        data["icon"]["attributes"] = `[{ "name":"test","type":"new test", "from": { "icon": "000000000000000000000002", "iconSet": "000000000000000000000002" } }]`
          .parseJsonString;

        auto parent = crates.icon.getItem("000000000000000000000002").exec.front;
        parent["attributes"] = `[{ "name":"test","type":"test" }]`.parseJsonString;
        crates.icon.updateItem(parent);

        router
          .request
          .put("/icons/000000000000000000000001")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyString.should.not.startWith(`{"errors":[{"`);
            auto icon = crates.icon.getItem("000000000000000000000001").exec.front;
            auto parent = crates.icon.getItem("000000000000000000000002").exec.front;

            icon["attributes"].length.should.equal(1);

            response.bodyJson["icon"]["attributes"].length.should.equal(1);
            response.bodyJson["icon"]["attributes"][0].should.equal(`{ "help": "",
              "otherNames": [],
              "isPrivate": false,
              "isRequired": false,
              "name": "test",
              "isInherited": true,
              "displayName": "test",
              "type": "new test",
              "options": "",
              "from": {
                "icon": "000000000000000000000002",
                "iconSet": "000000000000000000000002"
              }
            }`.parseJsonString);
        });
      });
    });
  });
});
