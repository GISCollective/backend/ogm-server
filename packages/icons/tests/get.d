/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.icons.get;

import tests.fixtures;
import std.path;
import ogm.icons.configuration;
import vibe.http.server;
import crate.json;
import ogm.icons.api;

alias suite = Spec!({
  describe("GET", {
    URLRouter router;

    Json icon1;
    Json icon2;
    Json icon3;
    Json icon4;

    Json icon1Readable;
    Json icon2Readable;
    Json icon3Readable;
    Json icon4Readable;

    Json icon1Editable;
    Json icon2Editable;
    Json icon3Editable;
    Json icon4Editable;

    beforeEach({
      setupTestData();

      IconsConfiguration config;

      router = new URLRouter;
      router.crateSetup.setupIconApi(crates, config);

      icon1 = `{
        "image": {
          "useParent": false,
          "value": ""
        },
        "order": 999,
        "allowMany": false,
        "subcategory": "subcategory",
        "name": "name1",
        "enforceSvg": false,
        "description": "description1",
        "localName": "name1",
        "_id": "000000000000000000000001",
        "category": "category",
        "iconSet": "000000000000000000000001",
        "verticalIndex": 0,
        "minZoom": 0,
        "maxZoom": 20,
        "keepWhenSmall": true,
        "measurements": {
          "stretchX": [],
          "height": 0,
          "width": 0,
          "content": [],
          "stretchY": []
        },
        "attributes": [
          { "type": "short text", "isInherited": false, "options": "", "isPrivate": false, "isRequired": false, "name": "phone", "otherNames": [], "displayName": "phone", "help": "" },
          { "type": "long text", "isInherited": false, "options": "", "isPrivate": false, "isRequired": false, "name": "program", "otherNames": [],"displayName": "program", "help": "" },
          { "type": "integer", "isInherited": false, "options": "", "isPrivate": false, "isRequired": false, "name": "max number of people", "otherNames": [],"displayName": "max number of people", "help": "" },
          { "type": "decimal", "isInherited": false, "options": "", "isPrivate": false, "isRequired": false, "name": "price", "otherNames": [],"displayName": "price", "help": "" },
          { "type": "boolean", "isInherited": false, "options": "", "isPrivate": false, "isRequired": false, "name": "kids-friendly", "otherNames": [], "displayName": "kids-friendly", "help": "" },
          { "type": "options", "isInherited": false, "options": "vegan,international,asian", "isPrivate": false, "isRequired": false, "name": "type of food", "otherNames": [], "displayName": "type of food", "help": "" }
        ],
        "parent": "",
        "otherNames": [ "other name 1" ],
        "styles": {
          "hasCustomStyle": false,
          "types": {
            "lineMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
            "polygonMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
            "polygon": { "backgroundColor": "rgba(255,255,255,0.6)", "hideBackgroundOnZoom": false, "showAsLineAfterZoom": 99, "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
            "site": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
            "lineLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
            "line": { "markerInterval": 700, "backgroundColor": "rgba(255,255,255,0.6)", "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
            "siteLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
            "polygonLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 }

          }
        },
        "visibility": {
          "isDefault": true,
          "isPublic": true,
          "team": "000000000000000000000001"
        }
      }`.parseJsonString;

      icon2 = `{
        "image": {
          "useParent": false,
          "value": ""
        },
        "enforceSvg": false,
        "allowMany": false,
        "subcategory": "subcategory2",
        "name": "name2",
        "description": "description2",
        "localName": "name2",
        "_id": "000000000000000000000002",
        "category": "category2",
        "iconSet": "000000000000000000000002",
        "attributes": [],
        "parent": "",
        "otherNames": [],
        "verticalIndex": 0,
        "minZoom": 0,
        "maxZoom": 20,
        "order": 999,
        "keepWhenSmall": true,
        "measurements": {
          "stretchX": [],
          "height": 0,
          "width": 0,
          "content": [],
          "stretchY": []
        },
        "styles": {
          "hasCustomStyle": false,
          "types": {
            "lineMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
            "polygonMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
            "polygon": { "backgroundColor": "rgba(255,255,255,0.6)", "hideBackgroundOnZoom": false, "showAsLineAfterZoom": 99, "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
            "site": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
            "lineLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
            "line": { "markerInterval": 700, "backgroundColor": "rgba(255,255,255,0.6)", "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
            "siteLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
            "polygonLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 }

          }
        },
        "visibility": {
          "isDefault": false,
          "isPublic": false,
          "team": "000000000000000000000002"
        }
      }`.parseJsonString;

      icon3 = `{
        "image": {
          "useParent": false,
          "value": ""
        },
        "enforceSvg": false,
        "allowMany": false,
        "subcategory": "subcategory3",
        "name": "name3",
        "description": "description3",
        "localName": "name3",
        "_id": "000000000000000000000003",
        "category": "category3",
        "iconSet": "000000000000000000000003",
        "attributes": [],
        "parent": "",
        "otherNames": [],
        "verticalIndex": 0,
        "minZoom": 0,
        "maxZoom": 20,
        "order": 999,
        "keepWhenSmall": true,
        "measurements": {
          "stretchX": [],
          "height": 0,
          "width": 0,
          "content": [],
          "stretchY": []
        },
        "styles": {
          "hasCustomStyle": false,
          "types": {
            "lineMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
            "polygonMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
            "polygon": { "backgroundColor": "rgba(255,255,255,0.6)", "hideBackgroundOnZoom": false, "showAsLineAfterZoom": 99, "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
            "site": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
            "lineLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
            "line": { "markerInterval": 700, "backgroundColor": "rgba(255,255,255,0.6)", "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
            "siteLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
            "polygonLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 }
          }
        },
        "visibility": {
          "isDefault": false,
          "isPublic": true,
          "team": "000000000000000000000003"
        }
      }`.parseJsonString;

      icon4 = `{
        "image": {
          "useParent": false,
          "value": ""
        },
        "enforceSvg": false,
        "allowMany": false,
        "subcategory": "subcategory4",
        "name": "name4",
        "description": "description4",
        "localName": "name4",
        "_id": "000000000000000000000004",
        "category": "category4",
        "iconSet": "000000000000000000000004",
        "attributes": [],
        "parent": "",
        "otherNames": [],
        "verticalIndex": 0,
        "minZoom": 0,
        "maxZoom": 20,
        "keepWhenSmall": true,
        "order": 999,
        "measurements": {
          "stretchX": [],
          "height": 0,
          "width": 0,
          "content": [],
          "stretchY": []
        },
        "styles": {
          "hasCustomStyle": false,
          "types": {
            "lineMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
            "polygonMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
            "polygon": { "backgroundColor": "rgba(255,255,255,0.6)", "hideBackgroundOnZoom": false, "showAsLineAfterZoom": 99, "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
            "site": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
            "lineLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
            "line": { "markerInterval": 700, "backgroundColor": "rgba(255,255,255,0.6)", "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
            "siteLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
            "polygonLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 }
          }
        },
        "visibility": {
          "isDefault": false,
          "isPublic": false,
          "team": "000000000000000000000004"
        }
      }`.parseJsonString;

      icon1Readable = icon1.clone;
      icon1Readable["canEdit"] = false;

      icon2Readable = icon2.clone;
      icon2Readable["canEdit"] = false;

      icon3Readable = icon3.clone;
      icon3Readable["canEdit"] = false;

      icon4Readable = icon4.clone;
      icon4Readable["canEdit"] = false;


      icon1Editable = icon1.clone;
      icon1Editable["canEdit"] = true;

      icon2Editable = icon2.clone;
      icon2Editable["canEdit"] = true;

      icon3Editable = icon3.clone;
      icon3Editable["canEdit"] = true;

      icon4Editable = icon4.clone;
      icon4Editable["canEdit"] = true;

      IconSettings.baseUrl = "http://localhost/";
    });

    afterEach({
      IconSettings.baseUrl = "";
    });

    it("returns the icons associated to a private map", {
      router
        .request
        .get("/icons?map=000000000000000000000002")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["icons"]
              .byValue
              .map!(a => a["_id"].to!string)
              .should
              .equal(["000000000000000000000001"]);
        });
    });

    describeCredentialsRule("view public records by id", "yes", "icons", ["administrator", "owner" ], (string type) {
      router
        .request
        .get("/icons/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{"icon": ` ~ icon1Editable.to!string ~ `}`).parseJsonString);
        });
    });

    describeCredentialsRule("view public records by id", "yes", "icons", ["leader", "member", "guest"], (string type) {
      router
        .request
        .get("/icons/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{"icon": ` ~ icon1Readable.to!string ~ `}`).parseJsonString);
        });
    });

    describeCredentialsRule("view public records by id", "yes", "icons", "no rights", {
      router
        .request
        .get("/icons/000000000000000000000001")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{"icon": ` ~ icon1.to!string ~ `}`).parseJsonString);
        });
    });

    describeCredentialsRule("view public records", "yes", "icons", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/icons")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          Json[] icons = cast(Json[]) response.bodyJson["icons"];
          string[] idList = icons.map!(a => a["_id"].to!string).array;

          idList.should.contain(["000000000000000000000001", "000000000000000000000003"]);
        });
    });

    describeCredentialsRule("view public records", "yes", "icons", "no rights", {
      router
        .request
        .get("/icons")
        .expectStatusCode(200)
        .end((Response response) => () {
          Json[] icons = cast(Json[]) response.bodyJson["icons"];
          string[] idList = icons.map!(a => a["_id"].to!string).array;

          idList.should.equal(["000000000000000000000001", "000000000000000000000003"]);
        });
    });


    describeCredentialsRule("view private own team records", "yes", "icons", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/icons")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          Json[] icons = cast(Json[]) response.bodyJson["icons"];
          string[] idList = icons.map!(a => a["_id"].to!string).array;

          idList.should.contain("000000000000000000000002");
        });
    });

    describeCredentialsRule("view private own team records", "-", "icons", "no rights", {
      router
        .request
        .get("/icons")
        .expectStatusCode(200)
        .end((Response response) => () {
          Json[] icons = cast(Json[]) response.bodyJson["icons"];
          string[] idList = icons.map!(a => a["_id"].to!string).array;

          idList.should.not.contain("000000000000000000000002");
        });
    });

    describeCredentialsRule("view private other team records", "yes", "icons", "administrator", {
      router
        .request
        .get("/icons?all=true")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(200)
        .end((Response response) => () {
          Json[] icons = cast(Json[]) response.bodyJson["icons"];
          string[] idList = icons.map!(a => a["_id"].to!string).array;

          idList.should.contain("000000000000000000000004");
        });
    });

    describeCredentialsRule("view private other team records", "no", "icons", ["owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/icons?all=true")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          Json[] icons = cast(Json[]) response.bodyJson["icons"];
          string[] idList = icons.map!(a => a["_id"].to!string).array;

          idList.should.not.contain("000000000000000000000004");
        });
    });

    describeCredentialsRule("view private other team records", "-", "icons", "no rights", {
      router
        .request
        .get("/icons?all=true")
        .expectStatusCode(200)
        .end((Response response) => () {
          Json[] icons = cast(Json[]) response.bodyJson["icons"];
          string[] idList = icons.map!(a => a["_id"].to!string).array;

          idList.should.not.contain("000000000000000000000004");
        });
    });

    describeCredentialsRule("view editable team records", "yes", "icons", "administrator", {
      router
        .request
        .get("/icons?edit=true&all=true")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(200)
        .end((Response response) => () {
          Json[] icons = cast(Json[]) response.bodyJson["icons"];
          string[] idList = icons.map!(a => a["_id"].to!string).array;

          idList.should.contain("000000000000000000000004");
        });
    });

    describeCredentialsRule("view editable team records", "no", "icons", ["owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/icons?edit=true")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          Json[] icons = cast(Json[]) response.bodyJson["icons"];
          string[] idList = icons.map!(a => a["_id"].to!string).array;

          idList.should.not.contain("000000000000000000000004");
        });
    });

    describeCredentialsRule("view editable team records", "-", "icons", "no rights", {
      router
        .request
        .get("/icons?edit=true")
        .expectStatusCode(400)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"errors":
            [{"description":"Can not find your icons.","status":400,"title":"Validation error"}]
          }`.parseJsonString);
        });
    });

    describe("without a token", {
      it("should not return the attributes when `withoutAttributes` is true", {
        auto icon = icon1.clone;
        icon.remove("attributes");
        icon.remove("verticalIndex");
        icon.remove("keepWhenSmall");
        icon.remove("minZoom");
        icon.remove("maxZoom");
        icon.remove("enforceSvg");
        icon.remove("measurements");
        icon.remove("order");

        router
          .request
          .get("/icons/000000000000000000000001?withoutAttributes=true")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal((`{"icon": ` ~ icon.to!string ~ `}`).parseJsonString);
          });
      });

      it("should get an icon image", {
        router
          .request
          .get("/icons/000000000000000000000001/image/value")
          .expectStatusCode(200)
          .end;
      });

      it("should allow etag headers", {
        router
          .request
          .customMethod!(HTTPMethod.OPTIONS)("/icons/000000000000000000000001/image/value")
          .expectHeader("Access-Control-Allow-Headers", "Content-Type, Authorization, If-None-Match")
          .expectStatusCode(204)
          .end;
      });

      it("should filter by icon names", {
        router
          .request
          .get("/icons?term=name1")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal((
              `{"icons": [
                ` ~ icon1.to!string ~ `]}`).parseJsonString);
          });
      });

      it("should select icons from a viewbox", {
        router
          .request
          .get("/icons?viewbox=1%2C1%2C2%2C2")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal((`{"icons":[` ~ icon1.to!string ~ `]}`)
              .parseJsonString);
          });
      });

      describe("the map selector", {
        it("should select icons associated to a map", {
          router
            .request
            .get("/icons?map=000000000000000000000001")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal((`{"icons":[` ~ icon1.to!string ~ `]}`)
                .parseJsonString);
            });
        });

        it("should return all the map.icons data when is set for an admin", {
          crates.meta.addItem(`{
            "type" : "Map.icons",
            "itemId" : "000000000000000000000001",
            "data" : {
              "icons" : [
                "000000000000000000000002",
                "000000000000000000000003"
              ]
            }
          }`.parseJsonString);

          router
            .request
            .get("/icons?map=000000000000000000000001")
            .header("Authorization", "Bearer " ~ userTokenList["administrator"])
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["icons"].length.should.equal(2);
              response.bodyJson["icons"][0]["_id"].to!string.should.equal("000000000000000000000002");
              response.bodyJson["icons"][1]["_id"].to!string.should.equal("000000000000000000000003");
            });
        });

        it("should return the public map.icons data when is set for a public user", {
          crates.meta.addItem(`{
            "type" : "Map.icons",
            "itemId" : "000000000000000000000001",
            "data" : {
              "icons" : [
                "000000000000000000000002",
                "000000000000000000000003"
              ]
            }
          }`.parseJsonString);

          router
            .request
            .get("/icons?map=000000000000000000000001")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["icons"].length.should.equal(1);
              response.bodyJson["icons"][0]["_id"].to!string.should.equal("000000000000000000000003");
            });
        });
      });

      describe("the iconSetsMap selector", {
        it("returns the icons belonging to the map when it has a custom list", {
          auto iconSet = crates.iconSet.getItem("000000000000000000000001").and.exec.front;
          iconSet["visibility"]["isPublic"] = true;
          crates.iconSet.updateItem(iconSet);

          auto map = crates.map.getItem("000000000000000000000001").and.exec.front;
          map["iconSets"] = `{"useCustomList":true,"list":["000000000000000000000003"]}`.parseJsonString;
          crates.map.updateItem(map);

          router
            .request
            .get("/icons?iconSetsMap=000000000000000000000001")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["icons"].byValue.map!(a => a["_id"].to!string).array.should.equal(["000000000000000000000003"]);
            });
        });

        it("returns nothing when selecting a map with a private icon set", {
          auto map = crates.map.getItem("000000000000000000000001").and.exec.front;
          map["iconSets"] = `{"useCustomList":true,"list":["000000000000000000000002"]}`.parseJsonString;
          crates.map.updateItem(map);

          router
            .request
            .get("/icons?iconSetsMap=000000000000000000000001")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["icons"].byValue.map!(a => a["_id"].to!string).array.should.equal([]);
            });
        });

        it("returns nothing when selecting a private map", {
          router
            .request
            .get("/icons?iconSetsMap=000000000000000000000002")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["icons"].byValue.map!(a => a["_id"].to!string).array.should.equal([]);
            });
        });
      });

      describe("the iconSetsCampaign selector", {
        it("returns the icons belonging to the map associated to a campaign when it has a custom list", {
          auto iconSet = crates.iconSet.getItem("000000000000000000000001").and.exec.front;
          iconSet["visibility"]["isPublic"] = true;
          crates.iconSet.updateItem(iconSet);

          auto map = crates.map.getItem("000000000000000000000001").and.exec.front;
          map["iconSets"] = `{"useCustomList":true,"list":["000000000000000000000003"]}`.parseJsonString;
          crates.map.updateItem(map);

          auto campaign = crates.campaign.getItem("000000000000000000000001").and.exec.front;
          campaign["map"] = `{ "isEnabled":true, "map": "000000000000000000000001" }`.parseJsonString;
          crates.campaign.updateItem(campaign);

          router
            .request
            .get("/icons?iconSetsCampaign=000000000000000000000001")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["icons"].byValue.map!(a => a["_id"].to!string).array.should.equal(["000000000000000000000003"]);
            });
        });

        it("returns the icons belonging to the private map associated to a campaign when it has a custom list", {
          auto iconSet = crates.iconSet.getItem("000000000000000000000001").and.exec.front;
          iconSet["visibility"]["isPublic"] = true;
          crates.iconSet.updateItem(iconSet);

          auto map = crates.map.getItem("000000000000000000000001").and.exec.front;
          map["visibility"]["isPublic"] = false;
          map["iconSets"] = `{"useCustomList":true,"list":["000000000000000000000003"]}`.parseJsonString;
          crates.map.updateItem(map);

          auto campaign = crates.campaign.getItem("000000000000000000000001").and.exec.front;
          campaign["map"] = `{ "isEnabled":true, "map": "000000000000000000000001" }`.parseJsonString;
          crates.campaign.updateItem(campaign);

          router
            .request
            .get("/icons?iconSetsCampaign=000000000000000000000001")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["icons"].byValue.map!(a => a["_id"].to!string).array.should.equal(["000000000000000000000003"]);
            });
        });

        it("returns the default icons when the campaign has no map", {
          auto iconSet = crates.iconSet.getItem("000000000000000000000001").and.exec.front;
          iconSet["visibility"]["isPublic"] = true;
          crates.iconSet.updateItem(iconSet);

          auto map = crates.map.getItem("000000000000000000000001").and.exec.front;
          map["iconSets"] = `{"useCustomList":true,"list":["000000000000000000000003"]}`.parseJsonString;
          crates.map.updateItem(map);

          auto campaign = crates.campaign.getItem("000000000000000000000001").and.exec.front;
          campaign["map"] = `{ "isEnabled":false, "map": "000000000000000000000001" }`.parseJsonString;
          crates.campaign.updateItem(campaign);

          router
            .request
            .get("/icons?iconSetsCampaign=000000000000000000000001")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["icons"].byValue.map!(a => a["_id"].to!string).array.should.equal(["000000000000000000000001"]);
            });
        });

        it("returns an empty list for a campaign that does not exist", {
          router
            .request
            .get("/icons?iconSetsCampaign=00000000000000000000000a")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["icons"].byValue.map!(a => a["_id"].to!string).array.should.equal([]);
            });
        });

        it("returns an empty list for a private campaign", {
          auto campaign = crates.campaign.getItem("000000000000000000000001").and.exec.front;
          campaign["visibility"]["isPublic"] = false;
          crates.campaign.updateItem(campaign);

          router
            .request
            .get("/icons?iconSetsCampaign=000000000000000000000001")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["icons"].byValue.map!(a => a["_id"].to!string).array.should.equal([]);
            });
        });

        it("returns the icons of a private campaign when requested by admin", {
          auto campaign = crates.campaign.getItem("000000000000000000000001").and.exec.front;
          campaign["visibility"]["isPublic"] = false;
          crates.campaign.updateItem(campaign);

          router
            .request
            .get("/icons?iconSetsCampaign=000000000000000000000001")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["icons"].byValue.map!(a => a["_id"].to!string).array.should.equal(["000000000000000000000001"]);
            });
        });

        it("returns the icons of a private campaign when requested by owner", {
          auto campaign = crates.campaign.getItem("000000000000000000000001").and.exec.front;
          campaign["visibility"]["isPublic"] = false;
          crates.campaign.updateItem(campaign);

          router
            .request
            .get("/icons?iconSetsCampaign=000000000000000000000001")
            .header("Authorization", "Bearer " ~ userTokenList["owner"])
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["icons"].byValue.map!(a => a["_id"].to!string).array.should.equal(["000000000000000000000001"]);
            });
        });

        it("returns an empty list for a private campaign when requested by another user", {
          auto campaign = crates.campaign.getItem("000000000000000000000001").and.exec.front;
          campaign["visibility"]["isPublic"] = false;
          crates.campaign.updateItem(campaign);

          router
            .request
            .get("/icons?iconSetsCampaign=000000000000000000000001")
            .header("Authorization", "Bearer " ~ otherToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["icons"].byValue.map!(a => a["_id"].to!string).array.should.equal([]);
            });
        });
      });

      describe("when a feature is masked", {
        beforeEach({
          crates.feature.deleteItem("000000000000000000000002");
          crates.feature.deleteItem("000000000000000000000004");

          auto feature = crates.feature.getItem("000000000000000000000003").and.exec.front;
          feature["unmasked"] = `{
            "type": "Point",
            "coordinates": [5,5]
          }`.parseJsonString;

          crates.feature.updateItem(feature);

          feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
          feature["unmasked"] = `{
            "type": "Point",
            "coordinates": [5,5]
          }`.parseJsonString;

          crates.feature.updateItem(feature);

          auto map = crates.map.getItem("000000000000000000000003").and.exec.front;
          map["mask"] = `{
            "isEnabled": true
          }`.parseJsonString;

          crates.map.updateItem(map);

          map = crates.map.getItem("000000000000000000000001").and.exec.front;
          map["mask"] = `{
            "isEnabled": true
          }`.parseJsonString;

          crates.map.updateItem(map);
        });

        it("should select icons from a viewbox using the masked geometries", {
          router
            .request
            .get("/icons?viewbox=1%2C1%2C2%2C2&map=000000000000000000000001")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal((`{"icons":[` ~ icon1.to!string ~ `]}`)
                .parseJsonString);
            });
        });

        it("should select no icons from a viewbox when it matches the unmasked geometry", {
          router
            .request
            .get("/icons?viewbox=3%2C3%2C6%2C6&map=000000000000000000000001")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal((`{"icons":[]}`)
                .parseJsonString);
            });
        });
      });
    });

    describe("with an invalid token", {
      it("should not return any icon", {
        router
          .request
          .get("/icons")
          .header("Authorization", "Bearer random")
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"error\": \"Invalid token.\"}".parseJsonString);
          });
      });
    });

    describe("with an admin token", {
      it("should return the image of a private icon using a session token", {
        router
          .request
          .get("/icons/000000000000000000000004/image/value")
          .header("User-Agent", "something")
          .header("Cookie", "ember_simple_auth-session=%7B%22authenticated%22%3A%7B%22access_token%22%3A%22" ~ administratorToken.name ~ "%22%7D%7D")
          .expectStatusCode(200)
          .end;
      });

      it("should return all icons from a set" , {
        router
          .request
          .get("/icons?iconSet=000000000000000000000002&edit=true")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal((`{"icons": [
              ` ~ icon2Editable.to!string ~ `
            ]}`).parseJsonString);
          });
      });

      it("should return all icons from two sets" , {
        router
          .request
          .get("/icons?iconSet=000000000000000000000002,000000000000000000000003")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal((`{"icons": [
              ` ~ icon2Editable.to!string ~ `,
              ` ~ icon3Editable.to!string ~ `
              ]}`).parseJsonString);
          });
      });

      it("should group icons by category without translations", {
        router
          .request
          .get("/icons?iconSet=000000000000000000000002,000000000000000000000003&group=true")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal((`{ "icons": [],
              "categories": {
                "category2": {
                  "icons": [],
                  "categories": {
                    "subcategory2": {
                      "icons": [ ` ~ icon2Editable.to!string ~ ` ],
                      "categories": {}
                    }}},
                "category3": {
                "icons": [],
                "categories": {
                  "subcategory3": {
                    "icons": [ ` ~ icon3Editable.to!string ~ ` ],
                    "categories": {}
                  }
                }}}}`).parseJsonString);
          });
      });

      it("groups icons by category having the first category, the icon set name", {
        router
          .request
          .get("/icons?iconSet=000000000000000000000002,000000000000000000000003&group=true&grupset=true")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal((`{
                "icons": [],
                "categories": {
                  "set2": {
                    "icons": [],
                    "categories": {
                      "category2": {
                        "icons": [],
                        "categories": {
                          "subcategory2": {
                            "icons": [ ` ~ icon2Editable.to!string ~ ` ],
                            "categories": {}
                          }
                        }
                      }
                    }
                  },
                  "set3": {
                    "icons": [],
                    "categories": {
                      "category3": {
                        "icons": [],
                        "categories": {
                          "subcategory3": {
                            "icons": [ ` ~ icon3Editable.to!string ~ ` ],
                            "categories": {}
                          }
                        }
                      }
                    }
                  }
                }
              }`).parseJsonString);
          });
      });

      it("should group icons by category with translations", {
        auto translation = `{
          "modelName": "Icon",
          "itemId": "000000000000000000000002",
          "locale": "ro-ro",

          "fields": {
            "localName": "ro name",
            "description": "ro description",
            "category": "ro category",
            "subcategory": "ro subcategory"
          }
        }`.parseJsonString;

        crates.fieldTranslation.addItem(translation);

        router
          .request
          .get("/icons?iconSet=000000000000000000000002&group=true&locale=ro-ro")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            Json responseData = response.bodyJson;

            responseData.exists("categories.ro category.categories.ro subcategory").should.equal(true);
            responseData.exists("categories.category.categories.subcategory").should.equal(false);
          });
      });

      it("should return filter the icons by category" , {
        router
          .request
          .get("/icons?category=category2")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal((`{"icons": [ ` ~ icon2Editable.to!string ~ ` ]}`).parseJsonString);
          });
      });

      it("should return filter the icons by subcategory" , {
        router
          .request
          .get("/icons?subcategory=subcategory2")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal((`{"icons": [
              ` ~ icon2Editable.to!string ~ `
            ]}`).parseJsonString);
          });
      });

      it("should select icons from a viewbox", {
        router
          .request
          .get("/icons?viewbox=1%2C1%2C2%2C2")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal((`{"icons":[` ~ icon1Editable.to!string ~ `,` ~ icon2Editable.to!string ~ `]}`)
              .parseJsonString);
          });
      });

      describe("when there is an iconset with a style and an icon without", {
        beforeEach({
          auto set = crates.iconSet.getItem("000000000000000000000001").exec.front;
          set["styles"] = `{
            "lineMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
            "polygonMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
            "polygon": { "backgroundColor": "rgba(255,255,255,0.6)", "hideBackgroundOnZoom": false, "showAsLineAfterZoom": 99, "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
            "site": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
            "lineLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
            "line": { "markerInterval": 700, "backgroundColor": "rgba(255,255,255,0.6)", "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
            "siteLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
            "polygonLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 }
          }`.parseJsonString;

          crates.iconSet.updateItem(set);
        });

        it("should return the icon with the icon set style", {
          router
            .request
            .get("/icons/000000000000000000000001")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["icon"]["styles"].should.equal(`{
                "hasCustomStyle": false,
                "types": {
                  "lineMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                  "polygonMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                  "polygon": { "backgroundColor": "rgba(255,255,255,0.6)", "hideBackgroundOnZoom": false, "showAsLineAfterZoom": 99, "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
                  "site": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                  "lineLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
                  "line": { "markerInterval": 700, "backgroundColor": "rgba(255,255,255,0.6)", "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
                  "siteLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
                  "polygonLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 }
                }
              }`.parseJsonString);
            });
        });
      });

      describe("when there is no iconset and an icon without style", {
        beforeEach({
          crates.iconSet.deleteItem("000000000000000000000001");
          crates.iconSet.deleteItem("000000000000000000000002");
          crates.iconSet.deleteItem("000000000000000000000003");
          crates.iconSet.deleteItem("000000000000000000000004");
        });

        it("should return the icon with the icon set style", {
          router
            .request
            .get("/icons/000000000000000000000001")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["icon"]["styles"].should.equal(`{
                "hasCustomStyle": false,
                "types": {
                }
              }`.parseJsonString);
            });
        });
      });

      describe("when there is an iconset and an icon with styles", {
        beforeEach({
          auto set = crates.iconSet.getItem("000000000000000000000001").exec.front;
          set["styles"] = `{ "styles": {
              "lineMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
              "polygonMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
              "polygon": { "backgroundColor": "rgba(255,255,255,0.6)", "hideBackgroundOnZoom": false, "showAsLineAfterZoom": 99, "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
              "site": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
              "lineLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
              "line": { "backgroundColor": "rgba(255,255,255,0.6)", "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
              "siteLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
              "polygonLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 }
            }}`.parseJsonString;

          crates.iconSet.updateItem(set);

          auto icon = crates.icon.getItem("000000000000000000000001").exec.front;
          icon["order"] = 999;
          icon["styles"] = `{
            "hasCustomStyle": true,
            "types": {
              "lineMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
              "polygonMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
              "polygon": { "backgroundColor": "rgba(255,255,255,0.6)", "hideBackgroundOnZoom": false, "showAsLineAfterZoom": 99, "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
              "site": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
              "lineLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
              "line": { "markerInterval": 700, "backgroundColor": "rgba(255,255,255,0.6)", "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
              "siteLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
              "polygonLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 }
            }}`.parseJsonString;

          crates.icon.updateItem(icon);
        });

        it("should return the icon with it's own style", {
          router
            .request
            .get("/icons/000000000000000000000001")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["icon"]["styles"].should.equal(`{
                "hasCustomStyle": true,
                "types": {
                  "lineMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                  "polygonMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                  "polygon": { "backgroundColor": "rgba(255,255,255,0.6)", "hideBackgroundOnZoom": false, "showAsLineAfterZoom": 99, "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
                  "site": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff"},
                  "lineLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
                  "line": { "markerInterval": 700, "backgroundColor": "rgba(255,255,255,0.6)", "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
                  "siteLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
                  "polygonLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 }
              }}`.parseJsonString);
            });
        });
      });

      describe("when there is an icon with no category", {
        beforeEach({
          auto icon = crates.icon.getItem("000000000000000000000001").exec.front;
          icon["category"] = "";

          crates.icon.updateItem(icon);
        });

        it("should return the icon when category is `not set`", {
          router
            .request
            .get("/icons?category=not set")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              auto result = icon1Editable.clone;
              result["category"] = "";
              result["subcategory"] = "subcategory";

              response.bodyJson.should.equal((`{"icons": [` ~ result.to!string~ `]}`).parseJsonString);
            });
        });

        it("should group icons by category", {
          auto icon = icon1Editable.clone;
          icon["category"] = "";
          icon["image"]["value"] = "";

          router
            .request
            .get("/icons?iconSet=000000000000000000000001&group=true")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal((`{"icons": [` ~ icon.to!string ~ `], "categories": {}}`).parseJsonString);
            });
        });
      });

      describe("when there is an icon with no subcategory", {
        beforeEach({
          auto icon = crates.icon.getItem("000000000000000000000001").exec.front;
          icon["category"] = "category";
          icon["subcategory"] = "";

          crates.icon.updateItem(icon);
        });

        it("should return the icon when category is `not set`", {
          router
            .request
            .get("/icons?subcategory=not set")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              auto result = icon1Editable.clone;
              result["category"] = "category";
              result["image"]["value"] = "";
              result["subcategory"] = "";

              response.bodyJson.should.equal((`{"icons": [` ~ result.to!string ~ `]}`).parseJsonString);
            });
        });
      });

      describe("when there is an icon with a null image value", {
        beforeEach({
          auto icon = crates.icon.getItem("000000000000000000000001").exec.front;
          icon["image"]["value"] = null;

          crates.icon.updateItem(icon);
        });

        it("should return an empty string for the url", {
          router
            .request
            .get("/icons/000000000000000000000001")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["icon"]["image"]["value"].should.equal("");
            });
        });
      });

      describe("when there is an icon with an image id", {
        beforeEach({
          auto icon = crates.icon.getItem("000000000000000000000001").exec.front;
          icon["image"]["value"] = "000000000000000000000001";

          crates.icon.updateItem(icon);
        });

        it("should return an empty string for the url", {
          router
            .request
            .get("/icons/000000000000000000000001")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["icon"]["image"]["value"].should.equal("http://localhost/icons/000000000000000000000001/image/value");
            });
        });
      });

      describe("when a feature is masked", {
        beforeEach({
          crates.feature.deleteItem("000000000000000000000002");
          crates.feature.deleteItem("000000000000000000000004");

          auto feature = crates.feature.getItem("000000000000000000000003").and.exec.front;
          feature["unmasked"] = `{
            "type": "Point",
            "coordinates": [5,5]
          }`.parseJsonString;

          crates.feature.updateItem(feature);

          feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
          feature["unmasked"] = `{
            "type": "Point",
            "coordinates": [5,5]
          }`.parseJsonString;

          crates.feature.updateItem(feature);

          auto map = crates.map.getItem("000000000000000000000003").and.exec.front;
          map["mask"] = `{
            "isEnabled": true
          }`.parseJsonString;

          crates.map.updateItem(map);

          map = crates.map.getItem("000000000000000000000001").and.exec.front;
          map["mask"] = `{
            "isEnabled": true
          }`.parseJsonString;

          crates.map.updateItem(map);
        });

        it("should select no icons from a viewbox when it matches the masked geometries", {
          router
            .request
            .get("/icons?viewbox=1%2C1%2C2%2C2&map=000000000000000000000001")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal((`{"icons":[]}`)
                .parseJsonString);
            });
        });

        it("should select icons from a viewbox when it matches the unmasked geometry", {
          router
            .request
            .get("/icons?viewbox=3%2C3%2C6%2C6&map=000000000000000000000001")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal((`{"icons":[` ~ icon1Editable.to!string ~ `]}`)
              .parseJsonString);
            });
        });
      });
    });
  });
});
