/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.icons.IconParentMiddleware;

import tests.fixtures;

import std.algorithm;
import std.array;
import ogm.icons.configuration;
import ogm.icons.api;
import ogm.icons.middleware.IconParentMiddleware;

alias suite = Spec!({
  URLRouter router;
  Json data;
  Json dataOtherTeam;

  describe("IconParentMiddleware", {
    beforeEach({
      setupTestData();

      IconsConfiguration config;
      config.baseUrl = "http://localhost";
      config.location = "./tmp-icons";

      router = new URLRouter;
      router.crateSetup.setupIconApi(crates, config);

      data = `{"icon": {
                "image": {
                  "useParent": false,
                  "value": ""
                },
                "subcategory": "new subcategory",
                "name": "new name",
                "category": "category3",
                "iconSet": "000000000000000000000001",
                "description": "",
                "attributes": [],
                "parent": "",
                "otherNames": [] }}`.parseJsonString;

      dataOtherTeam = `{"icon": {
                "image": {
                  "useParent": false,
                  "value": ""
                },
                "subcategory": "new subcategory",
                "name": "new name",
                "category": "new category",
                "iconSet": "000000000000000000000003",
                "description": "",
                "attributes": [],
                "parent": "",
                "otherNames": [] }}`.parseJsonString;
    });

    it("sets the parent icon value when image.useParent is true", {
      auto instance = new IconParentMiddlware(crates);

      auto parentIcon = crates.icon.getItem("000000000000000000000001").and.exec.front;
      parentIcon["image"]["value"] = "00000000000000000000000a";
      crates.icon.updateItem(parentIcon);
      auto dataWithParent = data["icon"].clone;
      dataWithParent["parent"] = "000000000000000000000001";
      dataWithParent["image"]["useParent"] = true;

      instance.checkIcon(dataWithParent);

      dataWithParent["image"].should.equal(`{
        "useParent": true,
        "value": "00000000000000000000000a"
      }`.parseJsonString);
      dataWithParent["parent"].to!string.should.equal(`000000000000000000000001`);
    });

    it("sets the root icon value when the parent icon's image.useParent is true", {
      auto instance = new IconParentMiddlware(crates);

      auto rootIcon = crates.icon.getItem("000000000000000000000002").and.exec.front;
      rootIcon["image"]["value"] = "00000000000000000000000a";
      crates.icon.updateItem(rootIcon);

      auto parentIcon = crates.icon.getItem("000000000000000000000001").and.exec.front;
      parentIcon["parent"] = "000000000000000000000002";
      parentIcon["image"]["useParent"] = true;
      parentIcon["image"]["value"] = "";
      crates.icon.updateItem(parentIcon);

      auto dataWithParent = data["icon"].clone;
      dataWithParent["parent"] = "000000000000000000000001";
      dataWithParent["image"]["useParent"] = true;

      instance.checkIcon(dataWithParent);

      dataWithParent["image"].should.equal(`{
        "useParent": true,
        "value": "00000000000000000000000a"
      }`.parseJsonString);
      dataWithParent["parent"].to!string.should.equal(`000000000000000000000001`);
    });

    it("does not sets the parent icon value when image.useParent is false", {
      auto instance = new IconParentMiddlware(crates);

      auto parentIcon = crates.icon.getItem("000000000000000000000001").and.exec.front;
      parentIcon["image"]["value"] = "00000000000000000000000a";
      crates.icon.updateItem(parentIcon);
      auto dataWithParent = data["icon"].clone;
      dataWithParent["parent"] = "000000000000000000000001";
      dataWithParent["image"]["useParent"] = false;

      instance.checkIcon(dataWithParent);

      dataWithParent["image"].should.equal(`{
        "useParent": false,
        "value": ""
      }`.parseJsonString);
      dataWithParent["parent"].to!string.should.equal(`000000000000000000000001`);
    });
  });
});
