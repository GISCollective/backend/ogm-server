/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.icons.translations;

import tests.fixtures;
import std.path;
import std.algorithm;
import vibe.data.json;
import ogm.icons.configuration;
import ogm.icons.api;

alias suite = Spec!({
  describe("translations", {
    URLRouter router;
    beforeEach({
      setupTestData();

      IconsConfiguration config;

      router = new URLRouter;
      router.crateSetup.setupIconApi(crates, config);

      IconSettings.baseUrl = "http://localhost/";
    });

    describe("when a translation exists", {
      beforeEach({
        auto translation = `{
          "modelName": "Icon",
          "itemId": "000000000000000000000001",
          "locale": "ro-ro",

          "fields": {
            "localName": "ro name",
            "description": "ro description",

            "attributes": [
              { "displayName": "ro phone" },
              { "displayName": "ro program" },
              { "displayName": "ro max number of people" },
              { "displayName": "ro price" },
              { "displayName": "ro kids-friendly" },
              { "displayName": "ro type of food" }
            ]
          }
        }`.parseJsonString;

        crates.fieldTranslation.addItem(translation);
      });

      it("should replace the displayNames and the description when the locale query param is set", {
        router
          .request
          .get("/icons/000000000000000000000001?locale=ro-ro")
          .expectStatusCode(200)
          .end((Response response) => () {
            auto icon = response.bodyJson["icon"];

            icon["name"].should.equal("name1");
            icon["localName"].should.equal("ro name");
            icon["description"].should.equal("ro description");

            Json[] attributes = cast(Json[]) icon["attributes"];

            attributes.map!(a => a["name"].to!string)
              .should.equal([ "phone", "program", "max number of people", "price", "kids-friendly", "type of food" ]);

            attributes.map!(a => a["displayName"].to!string)
              .should.equal([ "ro phone", "ro program", "ro max number of people", "ro price", "ro kids-friendly", "ro type of food" ]);
          });
      });

      it("should replace the displayNames and the description when the header param is set", {
        router
          .request
          .get("/icons/000000000000000000000001")
          .header("Accept-Language", "ro-ro")
          .expectStatusCode(200)
          .end((Response response) => () {
            auto icon = response.bodyJson["icon"];

            icon["name"].should.equal("name1");
            icon["localName"].should.equal("ro name");
            icon["description"].should.equal("ro description");

            Json[] attributes = cast(Json[]) icon["attributes"];

            attributes.map!(a => a["name"].to!string)
              .should.equal([ "phone", "program", "max number of people", "price", "kids-friendly", "type of food" ]);

            attributes.map!(a => a["displayName"].to!string)
              .should.equal([ "ro phone", "ro program", "ro max number of people", "ro price", "ro kids-friendly", "ro type of food" ]);
          });
      });

      it("should update the existing translation when the locale is set on icon update", {
        Json data = `{"icon": {
                "image": { "useParent": false, "value": "000000000000000000000001" },
                "subcategory": "new subcategory",
                "name": "new name",
                "localName": "new local name",
                "category": "category3",
                "iconSet": "000000000000000000000001",
                "description": { "blocks": [
                  {
                    "type": "heading",
                    "data": {
                      "text": "some translated text"
                    }
                  }
                ]},
                "attributes": [{
                  "name": "new ro phone",
                  "type": "short text",
                  "displayName": "new ro phone"
                }],
                "parent": "",
                "otherNames": [] }}`.parseJsonString;

        router
          .request
          .put("/icons/000000000000000000000001?locale=ro-ro")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            crates.fieldTranslation.get.size.should.equal(1);
            crates.fieldTranslation.get.exec.front.should.equal(`{
              "modelName": "Icon",
              "locale": "ro-ro",
              "itemId": "000000000000000000000001",
              "fields": {
                "localName": "new local name",
                "category": "category3",
                "subcategory": "new subcategory",
                "description": { "blocks": [
                  {
                    "type": "heading",
                    "data": {
                      "text": "some translated text"
                    }
                  }
                ]},
                "attributes": [{
                  "displayName": "new ro phone"
                }]},
              "_id": "000000000000000000000001"
            }`.parseJsonString);
          });
      });

      describe("when an icon is not added to a feature", {
        beforeEach({
          auto range = crates.feature.get.and.exec;

          foreach (feature; range) {
            crates.feature.deleteItem(feature["_id"].to!string);
          }
        });

        it("should delete the existing translation when the icon is deleted", {
          router
            .request
            .delete_("/icons/000000000000000000000001")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(204)
            .end((Response response) => () {
              crates.fieldTranslation.get.size.should.equal(0);
            });
        });
      });

      it("should return error on patch with locale", {
        Json data = `{"icon": { "image":  { "useParent": false, "value": "000000000000000000000001" },
                "subcategory": "new subcategory",
                "name": "new name",
                "localName": "new local name",
                "category": "category3",
                "iconSet": "000000000000000000000001",
                "description": "",
                "attributes": [{
                  "name": "new ro phone",
                  "type": "short text",
                  "displayName": "new ro phone"
                }],
                "parent": "",
                "otherNames": [] }}`.parseJsonString;

        router
          .request
          .patch("/icons/000000000000000000000001?locale=ro-ro")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .send(data)
          .expectStatusCode(404)
          .end((Response response) => () {
            crates.fieldTranslation.get.size.should.equal(1);
          });
      });
    });

    describe("when a translation does not exist", {
      it("should not create a translation when the locale is not set", {
        Json data = `{"icon": {
                "image": { "useParent": false, "value": "000000000000000000000001" },
                "subcategory": "new subcategory",
                "name": "new name",
                "category": "category3",
                "iconSet": "000000000000000000000001",
                "description": "",
                "attributes": [],
                "parent": "",
                "otherNames": [] }}`.parseJsonString;

        router
          .request
          .post("/icons")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            crates.fieldTranslation.get.size.should.equal(0);
          });
      });

      it("should create a translation when the locale is set on icon create", {
        Json data = `{"icon": {
                "image": { "useParent": false, "value": "000000000000000000000001"},
                "subcategory": "new subcategory",
                "name": "new name",
                "category": "category3",
                "iconSet": "000000000000000000000001",
                "description": "",
                "attributes": [],
                "parent": "",
                "otherNames": [] }}`.parseJsonString;

        router
          .request
          .post("/icons?locale=ro-ro")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            crates.fieldTranslation.get.size.should.equal(1);
            auto result = parseJsonString(crates.fieldTranslation.get.exec.front.to!string);

            result.should.equal(`{
              "modelName": "Icon",
              "locale": "ro-ro",
              "itemId": "000000000000000000000005",
              "info": {
                "createdOn": "0001-01-01T00:00:00+00:00",
                "changeIndex": 0,
                "lastChangeOn": "0001-01-01T00:00:00+00:00",
                "originalAuthor": "",
                "author": ""
              },
              "fields": {
                "category": "category3",
                "subcategory": "new subcategory"
              },
              "_id": "000000000000000000000001"
            }`.parseJsonString);
          });
      });

      it("should create a translation when the locale is set on icon update", {
        Json data = `{"icon": { "image": { "useParent": false, "value": "000000000000000000000001" },
                "subcategory": "new subcategory",
                "name": "new name",
                "category": "category3",
                "iconSet": "000000000000000000000001",
                "description": "new description",
                "attributes": [],
                "parent": "",
                "otherNames": [] }}`.parseJsonString;
        Json originalValue = crates.icon.getItem("000000000000000000000001").exec.front.clone;
        originalValue["image"]["value"] = "";
        originalValue["canEdit"] = true;

        router
          .request
          .put("/icons/000000000000000000000001?locale=ro-ro")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            crates.fieldTranslation.get.size.should.equal(1);
            crates.fieldTranslation.get.exec.front.should.equal(`{
              "modelName": "Icon",
              "locale": "ro-ro",
              "itemId": "000000000000000000000001",
              "info": {
                "createdOn": "0001-01-01T00:00:00+00:00",
                "changeIndex": 0,
                "lastChangeOn": "0001-01-01T00:00:00+00:00",
                "originalAuthor": "",
                "author": ""
              },
              "fields": {
                "description": "new description",
                "category": "category3",
                "subcategory": "new subcategory"
              },
              "_id": "000000000000000000000001"
            }`.parseJsonString);

            crates.icon.getItem("000000000000000000000001").exec.front.should.equal(originalValue);
          });
      });
    });
  });
});
