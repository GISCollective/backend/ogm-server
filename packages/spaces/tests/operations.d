/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.spaces.operations;

import tests.fixtures;
import vibe.http.common;
import ogm.spaces.api;
import ogm.defaults.pages;
import ogm.defaults.spaces;
import ogm.defaults.teams;

alias suite = Spec!({
  URLRouter router;

  describe("operations", {
    string aboutId;
    Json space;

    beforeEach({
      setupTestData();
      router = new URLRouter;

      crates.setupDefaultTeams;
      crates.checkDefaultSpaces(GeneralConfig());
      crates.checkDefaultPages;

      router.crateSetup.setupSpaceApi(crates, "http://localhost/");

      aboutId = crates.page.get.where("slug").equal("about").and.exec.front["_id"].to!string;

      auto page = crates.page.getItem("000000000000000000000001").and.exec.front;
      page["visibility"]["isPublic"] = true;
      page["visibility"]["team"] = "000000000000000000000001";
      crates.page.updateItem(page);

      space = crates.space.getItem("000000000000000000000001").and.exec.front;
      space["visibility"]["team"] = "000000000000000000000001";
      crates.space.updateItem(space);
    });

    describe("the pages operation", {
      it("can get the pages with their ids", {
        auto page = `{
          "space": "000000000000000000000001",
          "name": "name",
          "slug": "new-page",
          "info": { "changeIndex": 1 }
        }`.parseJsonString;

        crates.page.addItem(page);

        router
          .request
          .get("/spaces/000000000000000000000001/pages")
          .header("Authorization", "Bearer " ~ userTokenList["owner"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["new-page"].should.equal("000000000000000000000007");
          });
      });

      it("can get the pages categories with their ids", {
        auto page1 = `{
          "space": "000000000000000000000001",
          "name": "name",
          "slug": "new-page-1",
          "info": { "changeIndex": 1 },
          "categories": ["category 1"]
        }`.parseJsonString;

        crates.page.addItem(page1);

        auto page2 = `{
          "space": "000000000000000000000001",
          "name": "name",
          "slug": "new-page-2",
          "info": { "changeIndex": 1 },
          "categories": ["category 2", "category 3"]
        }`.parseJsonString;

        crates.page.addItem(page2);

        auto page3 = `{
          "space": "000000000000000000000001",
          "name": "name",
          "slug": "new-page-3",
          "info": { "changeIndex": 1 }
        }`.parseJsonString;

        crates.page.addItem(page3);

        router
          .request
          .get("/spaces/000000000000000000000001/categories")
          .header("Authorization", "Bearer " ~ userTokenList["owner"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "category 1": "000000000000000000000007",
              "category 2": "000000000000000000000008",
              "category 3": "000000000000000000000008"
            }`.parseJsonString);
          });
      });

      it("can get the space style when it exists", {
        crates.meta.addItem(`{
          "type": "cssStyle",
          "model": "Space",
          "itemId": "000000000000000000000001",
          "changeIndex": 0,
          "data": {
            "cssVars": "some vars"
          }}`.parseJsonString);

        router
          .request
          .get("/spaces/000000000000000000000001/style")
          .header("Authorization", "Bearer " ~ userTokenList["owner"])
          .header("Content-Type", " ")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyString.should.equal("some vars");
          });
      });
    });

    describe("the space summary", {
      it("can get the summary of a private space", {
        space["visibility"]["isPublic"] = false;
        space["domain"] = "giscollective.com";
        space["logo"] = "logo";

        crates.space.updateItem(space);

        router
          .request
          .get("/spaces/_/summary?domain=giscollective.com")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "spaceSummary": {
                "isPublic": false,
                "logo": "logo",
                "name": "My service"
              }
            }`.parseJsonString);
          });
      });

      it("returns an error when the domain is not found", {
        router
          .request
          .get("/spaces/_/summary?domain=missing.com")
          .expectStatusCode(404)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "errors": [{
                "description": "The requested domain does not belong to a space.",
                "status": 404,
                "title": "Space not found"
              }]
            }`.parseJsonString);
          });
      });

      it("returns an error when the query param is not set", {
        router
          .request
          .get("/spaces/_/summary")
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "errors": [{
                "description": "The 'domain' query parameter is required.",
                "status": 403,
                "title": "Forbidden"
              }]
            }`.parseJsonString);
          });
      });
    });
  });
});
