/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.spaces.getlist;

import tests.fixtures;
import vibe.http.common;
import ogm.spaces.api;
import ogm.defaults.spaces;
import ogm.defaults.teams;
import std.algorithm;

alias suite = Spec!({
  URLRouter router;

  describe("Get space list", {
    beforeEach({
      setupTestData();
      router = new URLRouter;

      crates.setupDefaultTeams;
      crates.checkDefaultSpaces( GeneralConfig());

      router.crateSetup.setupSpaceApi(crates, "http://localhost/");

      auto space = crates.space.getItem("000000000000000000000001").and.exec.front;
      space["visibility"]["isPublic"] = true;

      crates.space.updateItem(space);
    });

    describeCredentialsRule("get the public space list", "yes", "spaces", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      auto initialSize = crates.space.get.size;

      router
        .request
        .get("/spaces")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["spaces"].byValue.map!(a => a["_id"].to!string).should.contain("000000000000000000000001");
        });
    });

    describeCredentialsRule("get the public space list", "yes", "spaces", "no rights", {
      router
        .request
        .get("/spaces")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["spaces"].byValue.map!(a => a["_id"].to!string).should.contain("000000000000000000000001");
        });
    });

    describe("when there is a private space", {
      beforeEach({
        setupTestData();
        router = new URLRouter;

        crates.setupDefaultTeams;
        crates.checkDefaultSpaces(GeneralConfig());

        router.crateSetup.setupSpaceApi(crates, "http://localhost/");

        auto space = crates.space.getItem("000000000000000000000001").and.exec.front;
        space["visibility"]["isPublic"] = false;
        space["visibility"]["team"] = "000000000000000000000001";

        crates.space.updateItem(space);
      });

      describeCredentialsRule("get the private space list", "yes", "spaces", ["administrator", "owner", "leader", "member", "guest"], (string type) {
        auto initialSize = crates.space.get.size;

        router
          .request
          .get("/spaces")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["spaces"].byValue.map!(a => a["_id"].to!string).should.contain("000000000000000000000001");
          });
      });

      describeCredentialsRule("get the private space list", "no", "spaces", "no rights", {
        router
          .request
          .get("/spaces")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["spaces"].byValue.map!(a => a["_id"].to!string).should.not.contain("000000000000000000000001");
          });
      });

      describeCredentialsRule("get the own space list", "yes", "spaces", ["administrator", "owner", "leader", "member", "guest"], (string type) {
        auto editablespace = crates.space.getItem("000000000000000000000001").and.exec.front;
        editablespace["info"]["originalAuthor"] = userId[type];
        crates.space.updateItem(editablespace);

        router
          .request
          .get("/spaces")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["spaces"].byValue.map!(a => a["_id"].to!string).should.contain("000000000000000000000001");
          });
      });

      describeCredentialsRule("get the own space list", "-", "spaces", "no rights", {});
    });

    describe("when there is a default and a regular space", {
      beforeEach({
        auto space = crates.space.getItem("000000000000000000000001").and.exec.front;
        space["isTemplate"] = false;
        crates.space.updateItem(space);

        auto otherSpace = space.clone;

        otherSpace["visibility"]["isDefault"] = false;
        otherSpace["domain"] = "test.org";
        otherSpace["isTemplate"] = false;
        crates.space.addItem(otherSpace);
      });

      it("can get only the default one", {
        router
          .request
          .get("/spaces?default=true")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["spaces"].byValue.map!(a => a["_id"].to!string).should.equal(["000000000000000000000001"]);
          });
      });

      it("gets nothing when the template query param is set", {
        router
          .request
          .get("/spaces?tpl=true")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["spaces"].length.should.equal(0);
          });
      });

      it("can match the space by domain", {
        router
          .request
          .get("/spaces?domain=test.org")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["spaces"].byValue.map!(a => a["_id"].to!string).should.equal(["000000000000000000000002"]);
          });
      });
    });

    describe("when there is a template and a regular space", {
      beforeEach({
        auto space = crates.space.getItem("000000000000000000000001").and.exec.front;
        space["isTemplate"] = false;
        crates.space.updateItem(space);

        auto otherSpace = space.clone;
        otherSpace["isTemplate"] = true;
        otherSpace["domain"] = "test.org";
        crates.space.addItem(otherSpace);
      });

      it("can get only the tpl one", {
        router
          .request
          .get("/spaces?tpl=true")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["spaces"].byValue.map!(a => a["_id"].to!string).should.equal(["000000000000000000000002"]);
          });
      });
    });
  });
});
