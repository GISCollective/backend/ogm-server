/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.spaces.clone;

import tests.fixtures;
import vibe.http.common;
import ogm.spaces.api;
import ogm.defaults.pages;
import ogm.defaults.spaces;
import ogm.defaults.teams;
import ogm.defaults.articles;
import ogm.spaces.middleware.spaceTemplate;

alias suite = Spec!({
  URLRouter router;

  describe("clone", {
    string aboutId;
    Json space;

    beforeEach({
      setupTestData();
      router = new URLRouter;

      crates.setupDefaultTeams;
      crates.checkDefaultSpaces(GeneralConfig());
      crates.checkDefaultPages;
      crates.setupDefaultArticles(true);

      router.crateSetup.setupSpaceApi(crates, "http://localhost/");

      aboutId = crates.page.get.where("slug").equal("about").and.exec.front["_id"].to!string;

      auto page = crates.page.getItem("000000000000000000000001").and.exec.front;
      page["visibility"]["isPublic"] = true;
      page["visibility"]["team"] = "000000000000000000000001";
      crates.page.updateItem(page);

      space = crates.space.getItem("000000000000000000000001").and.exec.front;
      space["visibility"]["team"] = "000000000000000000000001";
      crates.space.updateItem(space);
    });

    describe("pictures", {
      it("can clone a page with a picture", {
        auto picture = crates.picture.addItem(`{
          "name": "test",
          "owner": "@system",
          "picture": "http://localhost:9091/pictures/000000000000000000000003/picture"
        }`.parseJsonString);

        auto page = `{
          "space": "000000000000000000000001",
          "name": "name",
          "slug": "new-page",
          "info": { "changeIndex": 1 },
          "cols": [{
            "container": 1,
            "col": 2,
            "row": 3,
            "data": {
              "source": {
                "useSelectedModel": false,
                "model": "picture",
                "id": "",
              },
            },
            "type": "picture",
            "gid": "",
            "name": ""
          }]
        }`.parseJsonString;

        page["cols"][0]["data"]["source"]["id"] = picture["_id"];

        auto middleware = new SpaceTemplateMiddleware(crates);
        auto result = middleware.clonePageCols(page["cols"], "99", "66", space["info"]);

        result.should.equal(`[{
          "col": 2,
          "container": 1,
          "data": {
            "source": {
              "id": "000000000000000000000004",
              "model": "picture",
              "useSelectedModel": false
            }
          },
          "gid": "",
          "name": "",
          "row": 3,
          "type": "picture"
        }]`.parseJsonString);

        auto storedPicture = crates.picture.getItem(result[0]["data"]["source"]["id"].to!string).and.exec.front;

        expect(storedPicture["meta"]["link"]).to.equal(`{
          "model": "page",
          "modelId": "99"
        }`.parseJsonString);
      });

      it("does not clone a picture with a missing source", {
        auto picture = crates.picture.addItem(`{
          "name": "test",
          "owner": "@system",
          "picture": "http://localhost:9091/pictures/000000000000000000000003/picture"
        }`.parseJsonString);

        auto page = `{
          "space": "000000000000000000000001",
          "name": "name",
          "slug": "new-page",
          "info": { "changeIndex": 1 },
          "cols": [{
            "container": 1,
            "col": 2,
            "row": 3,
            "data": {
              "source": {
                "useSelectedModel": false,
                "model": "picture",
                "id": "",
              },
            },
            "type": "picture",
            "gid": "",
            "name": ""
          }]
        }`.parseJsonString;

        auto middleware = new SpaceTemplateMiddleware(crates);
        auto result = middleware.clonePageCols(page["cols"], "99", "66", space["info"]);

        result.should.equal(`[{
          "col": 2,
          "container": 1,
          "data": {
            "source": {
              "id": "",
              "model": "picture",
              "useSelectedModel": false
            }
          },
          "gid": "",
          "name": "",
          "row": 3,
          "type": "picture"
        }]`.parseJsonString);
      });
    });

    describe("articles", {
      it("can clone a page with an article", {
        auto article = crates.article.getItem("000000000000000000000001").exec.front.clone;
        article["visibility"]["isPublic"] = true;
        article["visibility"]["team"] = "000000000000000000000001";
        article["slug"] = "about--test";

        crates.article.updateItem(article);

        auto page = `{
          "space": "000000000000000000000001",
          "name": "name",
          "slug": "new-page",
          "info": { "changeIndex": 1 },
          "cols": [{
            "container": 1,
            "col": 2,
            "row": 3,
            "data": {
              "source": {
                "useSelectedModel": false,
                "model": "article",
                "id": "000000000000000000000001",
              },
            },
            "type": "article",
            "gid": "",
            "name": ""
          }]
        }`.parseJsonString;

        auto middleware = new SpaceTemplateMiddleware(crates);
        auto result = middleware.clonePageCols(page["cols"], "99", "66", space["info"]);

        result.should.equal(`[{
          "col": 2,
          "container": 1,
          "data": {
            "source": {
              "id": "000000000000000000000006",
              "model": "article",
              "useSelectedModel": false
            }
          },
          "gid": "",
          "name": "",
          "row": 3,
          "type": "article"
        }]`.parseJsonString);

        auto storedArticle = crates.article.getItem(result[0]["data"]["source"]["id"].to!string).and.exec.front;

        expect(storedArticle["info"]).to.equal(space["info"]);

        expect(storedArticle["visibility"]).to.equal(`{
          "isDefault": false,
          "isPublic": true,
          "team": "66"
        }`.parseJsonString);
      });

      it("can clone a page with a missing article", {
        auto page = `{
          "space": "000000000000000000000001",
          "name": "name",
          "slug": "new-page",
          "info": { "changeIndex": 1 },
          "cols": [{
            "container": 1,
            "col": 2,
            "row": 3,
            "data": {
              "source": {
                "useSelectedModel": false,
                "model": "article",
                "id": "000000000000000000000091",
              },
            },
            "type": "article",
            "gid": "",
            "name": ""
          }]
        }`.parseJsonString;

        auto middleware = new SpaceTemplateMiddleware(crates);
        auto result = middleware.clonePageCols(page["cols"], "99", "66", space["info"]);

        result.should.equal(`[{
          "col": 2,
          "container": 1,
          "data": {
            "source": {
              "model": "article",
              "useSelectedModel": false
            }
          },
          "gid": "",
          "name": "",
          "row": 3,
          "type": "article"
        }]`.parseJsonString);
      });

      it("reuses the article if it has an existing team slug", {
        auto article = crates.article.getItem("000000000000000000000001").exec.front;
        article["visibility"]["isPublic"] = true;
        article["visibility"]["team"] = "000000000000000000000001";
        article["slug"] = "about--test";

        crates.article.updateItem(article);

        auto teamArticle = crates.article.getItem("000000000000000000000002").exec.front;
        teamArticle["visibility"]["isPublic"] = true;
        teamArticle["visibility"]["team"] = "000000000000000000000066";
        teamArticle["slug"] = "about--test";

        crates.article.updateItem(teamArticle);

        teamArticle = crates.article.getItem("000000000000000000000002").exec.front;

        auto page = `{
          "space": "000000000000000000000001",
          "name": "name",
          "slug": "new-page",
          "info": { "changeIndex": 1 },
          "cols": [{
            "container": 1,
            "col": 2,
            "row": 3,
            "data": {
              "source": {
                "useSelectedModel": false,
                "model": "article",
                "id": "000000000000000000000001",
              },
            },
            "type": "article",
            "gid": "",
            "name": ""
          }]
        }`.parseJsonString;

        auto middleware = new SpaceTemplateMiddleware(crates);
        auto result = middleware.clonePageCols(page["cols"], "99", "000000000000000000000066", space["info"]);

        result[0]["data"]["source"].should.equal(`{
          "id": "000000000000000000000002",
          "model": "article",
          "useSelectedModel": false
        }`.parseJsonString);
      });
    });

    describe("links", {
      it("fixes the page ids", {
        auto page = `{
          "space": "000000000000000000000001",
          "name": "name",
          "slug": "new-page",
          "info": { "changeIndex": 1 },
          "cols": [{
            "container": 1,
            "col": 2,
            "row": 3,
            "data": {
              "destination": {
                "pageId": "0000000000000000000000aa",
              },
            },
            "type": "article",
            "gid": "",
            "name": ""
          }]
        }`.parseJsonString;

        auto middleware = new SpaceTemplateMiddleware(crates);
        string[string] clonedPages;
        clonedPages["0000000000000000000000aa"] = "0000000000000000000000bb";

        auto result = middleware.fixPageIds(page["cols"], clonedPages);

        result.should.equal(`[{
          "container": 1,
          "col": 2,
          "row": 3,
          "data": {
            "destination": {
              "pageId": "0000000000000000000000bb",
            },
          },
          "type": "article",
          "gid": "",
          "name": ""
        }]`.parseJsonString);
      });

      it("replaces unknown pages with an empty string", {
        auto page = `{
          "space": "000000000000000000000001",
          "name": "name",
          "slug": "new-page",
          "info": { "changeIndex": 1 },
          "cols": [{
            "container": 1,
            "col": 2,
            "row": 3,
            "data": {
              "source": {
                "model": "page",
                "id": "0000000000000000000000a",
              },
            },
            "type": "article",
            "gid": "",
            "name": ""
          }]
        }`.parseJsonString;

        auto middleware = new SpaceTemplateMiddleware(crates);
        string[string] clonedPages;
        clonedPages["0000000000000000000000aa"] = "0000000000000000000000bb";

        auto result = middleware.fixPageIds(page["cols"], clonedPages);

        result.should.equal(`[{
          "container": 1,
          "col": 2,
          "row": 3,
          "data": {
            "source": {
              "model": "page",
              "id": "",
            },
          },
          "type": "article",
          "gid": "",
          "name": ""
        }]`.parseJsonString);
      });

      it("replaces known pages with the new id string", {
        auto page = `{
          "space": "000000000000000000000001",
          "name": "name",
          "slug": "new-page",
          "info": { "changeIndex": 1 },
          "cols": [{
            "container": 1,
            "col": 2,
            "row": 3,
            "data": {
              "source": {
                "model": "page",
                "id": "0000000000000000000000aa",
              },
            },
            "type": "article",
            "gid": "",
            "name": ""
          }]
        }`.parseJsonString;

        auto middleware = new SpaceTemplateMiddleware(crates);
        string[string] clonedPages;
        clonedPages["0000000000000000000000aa"] = "0000000000000000000000bb";

        auto result = middleware.fixPageIds(page["cols"], clonedPages);

        result.should.equal(`[{
          "container": 1,
          "col": 2,
          "row": 3,
          "data": {
            "source": {
              "model": "page",
              "id": "0000000000000000000000bb",
            },
          },
          "type": "article",
          "gid": "",
          "name": ""
        }]`.parseJsonString);
      });
    });
  });
});
