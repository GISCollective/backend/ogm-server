/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.spaces.post;

import tests.fixtures;
import vibe.http.common;
import ogm.spaces.api;
import ogm.defaults.spaces;
import ogm.defaults.teams;
import ogm.calendar;
import std.conv;

alias suite = Spec!({
  URLRouter router;

  describe("Creating spaces", {
    Json space;

    beforeEach({
      setupTestData();
      router = new URLRouter;

      crates.setupDefaultTeams;

      auto calendarMock = new CalendarMock("2021-01-12T16:20:11Z");
      SysCalendar.instance = calendarMock;

      router.crateSetup.setupSpaceApi(crates, "http://localhost/");
      setupDefaultSpaces(crates, GeneralConfig());

      crates.preference.addItem(`{
        "name": "spaces.domains",
        "isSecret": true,
        "value": "giscollective.com,mapp.green"
      }`.parseJsonString);

      space = `{ "space": {
        "name": "test",
        "slug": "test-post",
        "domain": "test-post.giscollective.com",
        "menu": {
          "items": []
        },
        "visibility": {
          "team":"000000000000000000000001",
          "isPublic": true,
          "isDefault": false
        }
      }}`.parseJsonString;
    });

    it("does not allow admins to create spaces with the allowDomainChange set to true", {
      space["space"]["allowDomainChange"] = true;
      router
        .request
        .post("/spaces")
        .send(space)
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["space"]["allowDomainChange"].should.equal(false);
        });
    });

    it("does not allow owners to create spaces with the allowDomainChange set to true", {
      space["space"]["allowDomainChange"] = true;
      router
        .request
        .post("/spaces")
        .send(space)
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["space"]["allowDomainChange"].should.equal(false);
        });
    });

    it("creates a page when a new space is created", {
      auto initialSize = crates.space.get.size;

      router
        .request
        .post("/spaces")
        .send(space)
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto page = crates.page.get.where("space").equal(response.bodyJson["space"]["_id"].to!string).and.exec.front;
          auto space = crates.space.getItem(response.bodyJson["space"]["_id"].to!string).and.exec.front;

          space["landingPageId"].should.equal("000000000000000000000001");
          response.bodyJson["space"]["landingPageId"].should.equal("000000000000000000000001");

          page.should.equal(`{
            "_id": "000000000000000000000001",
            "cols": [],
            "info": {
              "author": "000000000000000000000005",
              "changeIndex": 0,
              "createdOn": "2021-01-12T16:20:11Z",
              "lastChangeOn": "2021-01-12T16:20:11Z",
              "originalAuthor": "000000000000000000000005"
            },
            "name": "Homepage",
            "slug": "",
            "space": "000000000000000000000002",
            "visibility": {
              "isDefault": false,
              "isPublic": true,
              "team": "000000000000000000000001"
            }
          }`.parseJsonString);

        });
    });

    it("sets the domain status to pending", {
      auto initialSize = crates.space.get.size;

      router
        .request
        .post("/spaces")
        .send(space)
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto space = crates.space.getItem(response.bodyJson["space"]["_id"].to!string).and.exec.front;

          space["domainStatus"].should.equal("pending");
        });
    });

    it("can not create a template space", {
      space["space"]["isTemplate"] = true;

      router
        .request
        .post("/spaces")
        .send(space)
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(400)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "errors": [{
              "description": "You can not create template spaces.",
              "status": 400,
              "title": "Validation error"
            }]
          }`.parseJsonString);
        });
    });

    describeCredentialsRule("create for own team", "yes", "spaces", ["administrator"], (string type) {
      auto initialSize = crates.space.get.size;

      router
        .request
        .post("/spaces")
        .send(space)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          crates.space.get.size.should.equal(initialSize + 1);
        });
    });

    describeCredentialsRule("create for own team", "yes", "spaces", ["owner"], (string type) {
      auto initialSize = crates.space.get.size;

      router
        .request
        .post("/spaces")
        .send(space)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["space"]["_id"].should.equal(`000000000000000000000002`);
        });
    });


    describeCredentialsRule("create for own team", "no", "spaces", ["leader", "member", "guest"], (string type) {
      auto initialSize = crates.space.get.size;

      router
        .request
        .post("/spaces")
        .send(space)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"errors":[{"description":"You don't have enough rights to add an item.","status":403,"title":"Forbidden"}]}`.parseJsonString);
        });
    });

    describeCredentialsRule("create for own team", "-", "spaces", "no rights", {
      router
        .request
        .post("/spaces")
        .send(space)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
        });
    });

    describe("when a template space exists", {
      Json templateSpace;
      Json[] pages;

      beforeEach({
        templateSpace = crates.space.addItem(`{
          "hasDomainValidated": false,
          "domainValidationKey": "key",
          "allowDomainChange": true,
          "landingPageId": "",
          "name": "",
          "slug": "main",
          "domain": "",
          "matomoSiteId": "",
          "matomoUrl": "",
          "menu": {},
          "info": {
            "changeIndex": 0,
            "originalAuthor": "@unknown",
            "author": "@unknown",
            "createdOn": "2022-03-09T20:03:25.872Z",
            "lastChangeOn": "2022-03-09T20:03:25.872Z"
          },
          "visibility": { "isDefault": true, "isPublic": true },
          "attributions": [{ "url": "https://giscollective.com/", "name": "© GISCollective" }],
          "cols": {},
          "logo": null,
          "isTemplate": true
        }`.parseJsonString);

        foreach(size_t i; 0..10) {
          auto page = parseJsonString(`{
            "name" : "Page ` ~ i.to!string ~ `",
            "slug" : "page-` ~ i.to!string ~ `",
            "cols" : [ ],
            "containers" : [ ],
            "layoutContainers" : [ ],
            "space" : ""
          }`);

          page["info"] = templateSpace["info"];
          page["visibility"] = templateSpace["visibility"];
          page["space"] = templateSpace["_id"];

          page = crates.page.addItem(page);

          pages ~= [page];
        }

        templateSpace["landingPageId"] = pages[0]["_id"];
        templateSpace["globalMapPageId"] = pages[1]["_id"];
      });

      it("stores the new ids to the database", {
        space["space"]["template"] = templateSpace["_id"];

        router
          .request
          .post("/spaces")
          .send(space)
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            auto space = crates.space.getItem(response.bodyJson["space"]["_id"].to!string).and.exec.front;

            expect(space).to.equal(response.bodyJson["space"]);
        });
      });

      it("copies the landing page", {
        space["space"]["template"] = templateSpace["_id"];

        router
          .request
          .post("/spaces")
          .send(space)
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            auto pageId = response.bodyJson["space"]["landingPageId"].to!string;
            auto page = crates.page.getItem(pageId).and.exec.front;

            expect(page["space"]).to.equal(response.bodyJson["space"]["_id"]);
            expect(page).to.equal(`{
              "_id": "000000000000000000000011",
              "cols": [],
              "containers": [],
              "info": {
                "author": "000000000000000000000005",
                "changeIndex": 0,
                "createdOn": "2021-01-12T16:20:11Z",
                "lastChangeOn": "2021-01-12T16:20:11Z",
                "originalAuthor": "000000000000000000000005"
              },
              "layoutContainers": [],
              "name": "Page 0",
              "slug": "page-0",
              "space": "000000000000000000000003",
              "visibility": {
                "isDefault": false,
                "isPublic": true,
                "team": "000000000000000000000001"
              }
            }`.parseJsonString);
        });
      });

      it("copies the globalMap page", {
        space["space"]["template"] = templateSpace["_id"];

        router
          .request
          .post("/spaces")
          .send(space)
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            auto pageId = response.bodyJson["space"]["globalMapPageId"].to!string;
            auto page = crates.page.getItem(pageId).and.exec.front;

            expect(page["space"]).to.equal(response.bodyJson["space"]["_id"]);
            expect(page).to.equal(`{
              "_id": "000000000000000000000012",
              "cols": [],
              "containers": [],
              "info": {
                "author": "000000000000000000000005",
                "changeIndex": 0,
                "createdOn": "2021-01-12T16:20:11Z",
                "lastChangeOn": "2021-01-12T16:20:11Z",
                "originalAuthor": "000000000000000000000005"
              },
              "layoutContainers": [],
              "name": "Page 1",
              "slug": "page-1",
              "space": "000000000000000000000003",
              "visibility": {
                "isDefault": false,
                "isPublic": true,
                "team": "000000000000000000000001"
              }
            }`.parseJsonString);
        });
      });

      it("copies the template space maps once", {
        space["space"]["template"] = templateSpace["_id"];

        router
          .request
          .post("/spaces")
          .send(space)
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            auto id = response.bodyJson["space"]["_id"].to!string;
            auto pages = crates.page.get.where("space").equal(ObjectId.fromString(id)).and.exec.map!(a => a["_id"].to!string).join(",");

            expect(pages).to.equal("000000000000000000000011,000000000000000000000012,000000000000000000000013,000000000000000000000014," ~
              "000000000000000000000015,000000000000000000000016,000000000000000000000017,000000000000000000000018,000000000000000000000019," ~
              "000000000000000000000020");
        });
      });

      it("ignores the globalMap page when the template does not have one", {
        space["space"]["template"] = templateSpace["_id"];

        templateSpace.remove("globalMapPageId");
        crates.space.updateItem(templateSpace);

        router
          .request
          .post("/spaces")
          .send(space)
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            auto pageId = response.bodyJson["space"]["globalMapPageId"].to!string;

            expect(pageId).to.equal("");
        });
      });

      it("returns an error when the space is not a template", {
        space["space"]["template"] = templateSpace["_id"];

        templateSpace["isTemplate"] = false;
        crates.space.updateItem(templateSpace);

        router
          .request
          .post("/spaces")
          .send(space)
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            expect(response.bodyJson).to.equal(`{}`.parseJsonString);
        });
      });
    });
  });
});
