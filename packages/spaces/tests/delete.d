/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.spaces.delete_;

import tests.fixtures;
import vibe.http.common;
import ogm.spaces.api;
import ogm.defaults.spaces;
import ogm.defaults.teams;

alias suite = Spec!({
  URLRouter router;

  describe("Deleting spaces", {
    beforeEach({
      setupTestData();
      router = new URLRouter;

      crates.setupDefaultTeams;
      crates.checkDefaultSpaces(GeneralConfig());

      router.crateSetup.setupSpaceApi(crates, "http://localhost/");

      auto space = crates.space.getItem("000000000000000000000001").and.exec.front;
      space["visibility"]["isPublic"] = true;
      space["visibility"]["team"] = "000000000000000000000001";
      crates.space.updateItem(space);
    });

    describeCredentialsRule("delete any team spaces", "yes", "spaces", ["administrator", "owner"], (string type) {
      auto initialSize = crates.space.get.size;

      router
        .request
        .delete_("/spaces/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end((Response response) => () {
          crates.space.get.where("_id").equal(ObjectId.fromString("000000000000000000000001")).and.exec.empty.should.equal(true);
          crates.space.get.size.should.equal(initialSize - 1);
        });
    });

    describeCredentialsRule("delete any team spaces", "no", "spaces", ["leader", "member", "guest"], (string type) {
      auto initialSize = crates.space.get.size;

      router
        .request
        .delete_("/spaces/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          crates.space.get.size.should.equal(initialSize);
          crates.space.get.where("_id").equal(ObjectId.fromString("000000000000000000000001")).and.exec.empty.should.equal(false);
        });
    });

    describeCredentialsRule("delete any team spaces", "-", "spaces", "no rights", {
      router
        .request
        .delete_("/spaces/000000000000000000000001")
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.toPrettyString.should.equal(`{"error": "Authorization required"}`.parseJsonString.toPrettyString);
        });
    });

    describeCredentialsRule("delete own spaces", "yes", "spaces", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      auto space = crates.space.getItem("000000000000000000000001").and.exec.front;
      space["info"]["originalAuthor"] = userId[type];
      crates.space.updateItem(space);

      auto initialSize = crates.space.get.size;

      router
        .request
        .delete_("/spaces/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end((Response response) => () {
          crates.space.get.size.should.equal(initialSize - 1);
          crates.space.get.where("_id").equal(ObjectId.fromString("000000000000000000000001")).and.exec.empty.should.equal(true);
        });
    });

    describeCredentialsRule("delete own spaces", "-", "spaces", "no rights", {});
  });
});
