/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.spaces.sitemap;

import tests.fixtures;
import vibe.http.common;
import ogm.spaces.api;
import ogm.defaults.pages;
import ogm.defaults.spaces;
import ogm.defaults.teams;
import ogm.spaces.operations.SiteMapOperation;
import ogm.defaults.articles;

string sortLines(string value) {
  return value.split("\n").sort.filter!(a => a.strip() != "").array.join("\n");
}

alias suite = Spec!({
  URLRouter router;

  describe("site map", {
    string aboutId;
    Json space;

    beforeEach({
      setupTestData();
      router = new URLRouter;

      crates.setupDefaultTeams;
      crates.checkDefaultSpaces(GeneralConfig());
      crates.checkDefaultPages;

      router.crateSetup.setupSpaceApi(crates, "http://localhost/");

      aboutId = crates.page.get.where("slug").equal("about").and.exec.front["_id"].to!string;

      auto page = crates.page.getItem("000000000000000000000001").and.exec.front;
      page["visibility"]["isPublic"] = true;
      page["visibility"]["team"] = "000000000000000000000001";
      crates.page.updateItem(page);

      space = crates.space.getItem("000000000000000000000001").and.exec.front;
      space["visibility"]["team"] = "000000000000000000000001";
      crates.space.updateItem(space);
    });

    describe("txt", {
      it("returns an empty file for a private space", {
        space["visibility"]["isPublic"] = false;
        space["domain"] = "giscollective.com";

        crates.space.updateItem(space);

        router
          .request
          .get("/spaces/" ~ space["_id"].to!string ~ "/sitemap.txt")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyString.should.equal(``);
          });
      });

      it("returns a sitemap for the current domain when the id is not set", {
        router
          .request
          .get("/spaces/_/sitemap.txt")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyString.sortLines.should.equal(
              "https://localhost/about\n" ~
              "https://localhost/campaigns\n" ~
              "https://localhost/help/faq\n" ~
              "https://localhost/help/privacy\n" ~
              "https://localhost/help/terms\n" ~
              "https://localhost/help/updates"
            );
          });
      });

      it("returns a sitemap at the root", {
        router
          .request
          .get("/sitemap.txt")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyString.sortLines.should.equal(
              "https://localhost/about\n" ~
              "https://localhost/campaigns\n" ~
              "https://localhost/help/faq\n" ~
              "https://localhost/help/privacy\n" ~
              "https://localhost/help/terms\n" ~
              "https://localhost/help/updates"
            );
          });
      });

      it("returns a sitemap for a space with pages without vars", {
        router
          .request
          .get("/spaces/" ~ space["_id"].to!string ~ "/sitemap.txt")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyString.sortLines.should.equal(
              "https://localhost/about\n" ~
              "https://localhost/campaigns\n" ~
              "https://localhost/help/faq\n" ~
              "https://localhost/help/privacy\n" ~
              "https://localhost/help/terms\n" ~
              "https://localhost/help/updates"
            );
          });
      });

      it("returns a sitemap replacing the public maps ids for a default space", {
        space["visibility"]["isDefault"] = true;
        crates.space.updateItem(space);

        auto page = `{
          "space": "000000000000000000000001",
          "name": "name",
          "slug": "map--:map-id",
          "info": { "changeIndex": 1 }
        }`.parseJsonString;
        page["space"] = space["_id"];

        crates.page.addItem(page);

        router
          .request
          .get("/spaces/" ~ space["_id"].to!string ~ "/sitemap.txt")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyString.sortLines.should.equal(
              "https://localhost/about\n" ~
              "https://localhost/campaigns\n" ~
              "https://localhost/help/faq\n" ~
              "https://localhost/help/privacy\n" ~
              "https://localhost/help/terms\n" ~
              "https://localhost/help/updates\n" ~
              "https://localhost/map/000000000000000000000001\n" ~
              "https://localhost/map/000000000000000000000003"
            );
          });
      });

      it("returns a sitemap replacing the public calendar ids for a default space", {
        space["visibility"]["isDefault"] = true;
        crates.space.updateItem(space);

        auto page = `{
          "space": "000000000000000000000001",
          "name": "name",
          "slug": "calendar--:calendar-id",
          "info": { "changeIndex": 1 }
        }`.parseJsonString;
        page["space"] = space["_id"];

        crates.page.addItem(page);

        router
          .request
          .get("/spaces/" ~ space["_id"].to!string ~ "/sitemap.txt")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyString.sortLines.should.equal(
              "https://localhost/about\n" ~
              "https://localhost/calendar/000000000000000000000001\n" ~
              "https://localhost/calendar/000000000000000000000003\n" ~
              "https://localhost/campaigns\n" ~
              "https://localhost/help/faq\n" ~
              "https://localhost/help/privacy\n" ~
              "https://localhost/help/terms\n" ~
              "https://localhost/help/updates"
            );
          });
      });

      it("returns a sitemap replacing the public icon set ids for a default space", {
        space["visibility"]["isDefault"] = true;
        crates.space.updateItem(space);

        auto page = `{
          "space": "000000000000000000000001",
          "name": "name",
          "slug": "icon-set--:icon-set-id",
          "info": { "changeIndex": 1 }
        }`.parseJsonString;
        page["space"] = space["_id"];

        crates.page.addItem(page);

        router
          .request
          .get("/spaces/" ~ space["_id"].to!string ~ "/sitemap.txt")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyString.sortLines.should.equal(
              "https://localhost/about\n" ~
              "https://localhost/campaigns\n" ~
              "https://localhost/help/faq\n" ~
              "https://localhost/help/privacy\n" ~
              "https://localhost/help/terms\n" ~
              "https://localhost/help/updates\n" ~
              "https://localhost/icon-set/000000000000000000000001\n" ~
              "https://localhost/icon-set/000000000000000000000003"
            );
          });
      });

      it("returns a sitemap replacing the public articles ids for a default space", {
        crates.setupDefaultArticles;

        auto article = crates.article.getItem("000000000000000000000001").exec.front.clone;
        article["visibility"]["isPublic"] = true;
        article["visibility"]["team"] = "000000000000000000000001";
        article["slug"] = "test";
        article["categories"] = ["some-category"].serializeToJson;
        crates.article.updateItem(article);

        space["visibility"]["isDefault"] = true;
        crates.space.updateItem(space);

        auto page = `{
          "space": "000000000000000000000001",
          "name": "name",
          "slug": "article--:article-id",
          "info": { "changeIndex": 1 },
          "categories": [ "some-category", "other" ]
        }`.parseJsonString;
        page["space"] = space["_id"];

        crates.page.addItem(page);

        router
          .request
          .get("/spaces/" ~ space["_id"].to!string ~ "/sitemap.txt")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyString.sortLines.should.equal(
              "https://localhost/about\n" ~
              "https://localhost/article/test\n" ~
              "https://localhost/campaigns\n" ~
              "https://localhost/help/faq\n" ~
              "https://localhost/help/privacy\n" ~
              "https://localhost/help/terms\n" ~
              "https://localhost/help/updates"
            );
          });
      });

      it("returns a sitemap replacing the public features ids for a default space", {
        space["visibility"]["isDefault"] = true;
        crates.space.updateItem(space);

        auto page = `{
          "space": "000000000000000000000001",
          "name": "name",
          "slug": "features--:feature-id",
          "info": { "changeIndex": 1 },
          "categories": [ ]
        }`.parseJsonString;
        page["space"] = space["_id"];

        crates.page.addItem(page);

        router
          .request
          .get("/spaces/" ~ space["_id"].to!string ~ "/sitemap.txt")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyString.sortLines.should.equal(
              "https://localhost/about\n" ~
              "https://localhost/campaigns\n" ~
              "https://localhost/features/000000000000000000000001\n" ~
              "https://localhost/features/000000000000000000000003\n" ~
              "https://localhost/help/faq\n" ~
              "https://localhost/help/privacy\n" ~
              "https://localhost/help/terms\n" ~
              "https://localhost/help/updates"
            );
          });
      });

      it("returns a sitemap replacing the public icons ids for a default space", {
        space["visibility"]["isDefault"] = true;
        crates.space.updateItem(space);

        auto page = `{
          "space": "000000000000000000000001",
          "name": "name",
          "slug": "icons--:icon-id",
          "info": { "changeIndex": 1 },
          "categories": []
        }`.parseJsonString;
        page["space"] = space["_id"];

        crates.page.addItem(page);

        router
          .request
          .get("/spaces/" ~ space["_id"].to!string ~ "/sitemap.txt")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyString.sortLines.should.equal(
              "https://localhost/about\n" ~
              "https://localhost/campaigns\n" ~
              "https://localhost/help/faq\n" ~
              "https://localhost/help/privacy\n" ~
              "https://localhost/help/terms\n" ~
              "https://localhost/help/updates\n" ~
              "https://localhost/icons/000000000000000000000001\n" ~
              "https://localhost/icons/000000000000000000000003"
            );
          });
      });

      it("returns a sitemap replacing the public events ids for a default space", {
        crates.event.addItem(`{
          "name": "test",
          "description": "some description",
          "calendar": "000000000000000000000001",
          "location": {
            "type": "Feature",
            "value": "000000000000000000000001"
          },
          "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" },
          "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
        }`.parseJsonString);

        space["visibility"]["isDefault"] = true;
        crates.space.updateItem(space);

        auto page = `{
          "space": "000000000000000000000001",
          "name": "name",
          "slug": "events--:event-id",
          "info": { "changeIndex": 1 },
          "categories": []
        }`.parseJsonString;
        page["space"] = space["_id"];

        crates.page.addItem(page);

        router
          .request
          .get("/spaces/" ~ space["_id"].to!string ~ "/sitemap.txt")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyString.sortLines.should.equal(
              "https://localhost/about\n" ~
              "https://localhost/campaigns\n" ~
              "https://localhost/events/000000000000000000000001\n" ~
              "https://localhost/help/faq\n" ~
              "https://localhost/help/privacy\n" ~
              "https://localhost/help/terms\n" ~
              "https://localhost/help/updates"
            );
          });
      });
    });

    describe("xml", {
      it("returns an empty file for a private space", {
        space["visibility"]["isPublic"] = false;
        space["domain"] = "giscollective.com";

        crates.space.updateItem(space);

        router
          .request
          .get("/spaces/" ~ space["_id"].to!string ~ "/sitemap.xml")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyString.should.equal(``);
          });
      });

      it("returns a sitemap for a space with pages without vars", {
        router
          .request
          .get("/spaces/" ~ space["_id"].to!string ~ "/sitemap.xml")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyString.should.contain(`<loc>https://localhost/help/updates</loc>`);
            response.bodyString.should.contain(`<loc>https://localhost/help/faq</loc>`);
          });
      });

      it("returns a sitemap at the root", {
        router
          .request
          .get("/sitemap.xml")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyString.should.contain(`<loc>https://localhost/help/updates</loc>`);
            response.bodyString.should.contain(`<loc>https://localhost/help/faq</loc>`);
          });
      });

      it("returns a sitemap replacing the public features ids for a default space", {
        space["visibility"]["isDefault"] = true;
        crates.space.updateItem(space);

        auto page = `{
          "space": "000000000000000000000001",
          "name": "name",
          "slug": "features--:feature-id",
          "info": { "changeIndex": 1 },
          "categories": [ ]
        }`.parseJsonString;
        page["space"] = space["_id"];

        crates.page.addItem(page);

        router
          .request
          .get("/spaces/" ~ space["_id"].to!string ~ "/sitemap.xml")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyString.should.contain(`<loc>https://localhost/features/000000000000000000000001</loc>`);
            response.bodyString.should.contain(`<loc>https://localhost/features/000000000000000000000003</loc>`);
          });
      });

      describe("for a large dataset", {
        beforeEach({
          foreach(size_t i; 0..10_012) {
            crates.feature.addItem((`{
              "info": {
              },
              "name": "22, Some Straße, Luisenstadt",
              "maps": ["000000000000000000000001"],
              "icons": ["1"],
              "computedVisibility": {
                "team": "000000000000000000000001",
              },
              "visibility": 1,
              "position": { "type": "Point", "coordinates": [1.6, 1.6] }
            }`).parseJsonString);
          }
        });

        it("renders the index by default", {
          space["visibility"]["isDefault"] = true;
          crates.space.updateItem(space);

          auto page = `{
            "space": "000000000000000000000001",
            "name": "name",
            "slug": "features--:feature-id",
            "info": { "changeIndex": 1 },
            "categories": [ ]
          }`.parseJsonString;
          crates.page.addItem(page);

          router
          .request
          .get("/spaces/" ~ space["_id"].to!string ~ "/sitemap.xml")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyString.should.equal(`<?xml version="1.0" encoding="UTF-8"?>` ~ "\n" ~
              `<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">` ~ "\n" ~
              `<sitemap>` ~ "\n" ~
              `  <loc>https://localhost/sitemap.xml?page=0</loc>` ~ "\n" ~
              `</sitemap>` ~ "\n" ~
              `<sitemap>` ~ "\n" ~
              `  <loc>https://localhost/sitemap.xml?page=1</loc>` ~ "\n" ~
              `</sitemap>` ~ "\n" ~
              `<sitemap>` ~ "\n" ~
              `  <loc>https://localhost/sitemap.xml?page=2</loc>` ~ "\n" ~
              `</sitemap>` ~ "\n" ~
              `</sitemapindex>`);
          });
        });

        it("does not add the features when page=0", {
          space["visibility"]["isDefault"] = true;
          crates.space.updateItem(space);

          auto page = `{
            "space": "000000000000000000000001",
            "name": "name",
            "slug": "features--:feature-id",
            "info": { "changeIndex": 1 },
            "categories": [ ]
          }`.parseJsonString;
          crates.page.addItem(page);

          router
          .request
          .get("/spaces/" ~ space["_id"].to!string ~ "/sitemap.xml?page=0")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyString.should.not.contain(`sitemapindex`);
            response.bodyString.should.not.contain(`features`);
          });
        });

        it("only adds features when page=2", {
          space["visibility"]["isDefault"] = true;
          crates.space.updateItem(space);

          auto page = `{
            "space": "000000000000000000000001",
            "name": "name",
            "slug": "features--:feature-id",
            "info": { "changeIndex": 1 },
            "categories": [ ]
          }`.parseJsonString;
          crates.page.addItem(page);

          router
          .request
          .get("/spaces/" ~ space["_id"].to!string ~ "/sitemap.xml?page=2")
          .expectStatusCode(200)
          .end((Response response) => () {
            import std.stdio;
            writeln(response.bodyString);
            response.bodyString.should.not.contain(`<loc>https://localhost/help/updates</loc>`);
          });
        });
      });
    });

    describe("getIds", {
      it("returns all public maps for a default space", {
        auto result = getIds(crates.map, true, "", []);

        result.map!(a => a.loc).array.should.equal(["000000000000000000000001", "000000000000000000000003"]);
      });

      it("returns only ids matching the categories when set", {
        auto result = getIds(crates.map, true, "", ["000000000000000000000003"]);

        result.map!(a => a.loc).array.should.equal(["000000000000000000000003"]);
      });

      it("returns the team public maps for a normal space", {
        auto result = getIds(crates.map, false, "000000000000000000000001", []);

        result.map!(a => a.loc).array.should.equal(["000000000000000000000001"]);
      });
    });
  });
});
