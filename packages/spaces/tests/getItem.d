/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.spaces.getItem;

import tests.fixtures;
import vibe.http.common;
import ogm.spaces.api;
import ogm.defaults.spaces;
import ogm.defaults.teams;
import ogm.defaults.pages;

alias suite = Spec!({
  URLRouter router;
  string aboutId;

  describe("get spaces", {
    beforeEach({
      setupTestData();
      router = new URLRouter;

      crates.setupDefaultTeams;
      crates.checkDefaultSpaces(GeneralConfig());

      router.crateSetup.setupSpaceApi(crates, "http://localhost/");

      auto space = crates.space.getItem("000000000000000000000001").and.exec.front;
      space["visibility"]["isPublic"] = true;
      space["visibility"]["team"] = "000000000000000000000001";
      crates.space.updateItem(space);
    });

    describeCredentialsRule("get public space by id", "yes", "spaces", ["administrator", "owner"], (string type) {
      auto initialSize = crates.space.get.size;

      router
        .request
        .get("/spaces/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["space"]["_id"].should.equal("000000000000000000000001");
          response.bodyJson["space"]["canEdit"].to!bool.should.equal(true);
        });
    });

    describeCredentialsRule("get public space by id", "yes", "spaces", ["leader", "member", "guest"], (string type) {
      auto initialSize = crates.space.get.size;

      router
        .request
        .get("/spaces/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["space"]["_id"].should.equal("000000000000000000000001");
          response.bodyJson["space"]["canEdit"].to!bool.should.equal(false);
        });
    });

    describeCredentialsRule("get public space by id", "yes", "spaces", "no rights", {
      router
        .request
        .get("/spaces/000000000000000000000001")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["space"]["_id"].should.equal("000000000000000000000001");
          response.bodyJson["space"]["canEdit"].to!bool.should.equal(false);
        });
    });

    describe("when there is a private space", {
      beforeEach({
        auto space = crates.space.getItem("000000000000000000000001").and.exec.front;
        space["visibility"]["isPublic"] = false;
        space["visibility"]["team"] = "000000000000000000000001";

        crates.space.updateItem(space);
      });

      describeCredentialsRule("get private spaces", "yes", "spaces", ["administrator", "owner", "leader", "member", "guest"], (string type) {
        auto initialSize = crates.space.get.size;

        router
          .request
          .get("/spaces/000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["space"]["_id"].should.equal("000000000000000000000001");
          });
      });

      describeCredentialsRule("get private spaces", "no", "spaces", "no rights", {
        router
          .request
          .get("/spaces/000000000000000000000001")
          .expectStatusCode(404);
      });
    });

    describe("landingPageId field fix", {
      it("does not return the landingPageId when the value is an empty string", {
        router
          .request
          .get("/spaces/000000000000000000000001")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["space"]["landingPageId"].type.should.equal(Json.Type.undefined);
          });
      });

      it("does not return the landingPageId when the value is an id of a missing page", {
        auto space = crates.space.getItem("000000000000000000000001").and.exec.front;
        space["landingPageId"] = "0000000000000000000000aa";
        crates.space.updateItem(space);

        router
          .request
          .get("/spaces/000000000000000000000001")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["space"]["landingPageId"].type.should.equal(Json.Type.undefined);
          });
      });

      it("does not return the landingPageId when the value is an id of a missing page", {
        crates.checkDefaultPages;

        auto space = crates.space.getItem("000000000000000000000001").and.exec.front;
        space["landingPageId"] = "000000000000000000000001";
        crates.space.updateItem(space);

        router
          .request
          .get("/spaces/000000000000000000000001")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["space"]["landingPageId"].should.equal("000000000000000000000001");
          });
      });
    });

    describe("the space mapper", {
      it("adds the style property", {
        router
          .request
          .get("/spaces/000000000000000000000001")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["space"]["style"].to!string.should.equal("http://localhost/spaces/000000000000000000000001/style");
          });
      });
    });
  });
});
