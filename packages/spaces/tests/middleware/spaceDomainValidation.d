module tests.spaces.middleware.spaceDomainValidation;

import tests.fixtures;
import vibe.http.common;
import ogm.defaults.spaces;
import ogm.defaults.teams;
import ogm.spaces.middleware.spaceDomainValidation;
import crate.error;

alias suite = Spec!({
  describe("SpaceDomainValidationMiddleware", {
    SpaceDomainValidationMiddleware middleware;
    Json space;
    Json anotherSpace;

    beforeEach({
      setupTestData();
      crates.setupDefaultTeams;
      crates.checkDefaultSpaces(GeneralConfig());

      space = crates.space.getItem("000000000000000000000001").and.exec.front;
      space["visibility"]["isPublic"] = true;
      space["visibility"]["team"] = "000000000000000000000001";
      space["allowDomainChange"] = false;
      crates.space.updateItem(space);
      space = space.clone;
      space["domain"] = "new.domain.com";

      anotherSpace = crates.space.addItem(space.clone);

      middleware = new SpaceDomainValidationMiddleware(crates);

      crates.preference.addItem(`{
        "name": "spaces.domains",
        "isSecret": true,
        "value": "giscollective.com,mapp.green"
      }`.parseJsonString);
    });

    it("does not allow creating a space without a domain", {
      space.remove("domain");
      middleware.validateNewRecord(space).should.throwException!CrateValidationException.withMessage("The domain is not set. Set the domain before creating a new space.");
    });

    it("throws when the domain is invalid", {
      space["domain"] = "invalid domain name";
      middleware.validateNewRecord(space).should.throwException!CrateValidationException.withMessage("The domain name is invalid.");
    });

    it("accepts a slug with lower case chars, numbers and dashes", {
      space["slug"] = "lower-chars-and-dashes-2";
      space["domain"] = "lower-chars-and-dashes-2.giscollective.com";

      middleware.validateNewRecord(space).should.not.throwAnyException;
    });

    it("does not accept a custom domain when allowDomainChange is false", {
      space["allowDomainChange"] = false;

      space["domain"] = "example.com";
      middleware.validateNewRecord(space).should.throwException!CrateValidationException.withMessage("The space is not allowed to use a custom domain.");

      space["domain"] = "other.subdomain.giscollective.com";
      middleware.validateNewRecord(space).should.throwException!CrateValidationException.withMessage("The space is not allowed to use a custom domain.");

      space["domain"] = "name.other.com";
      middleware.validateNewRecord(space).should.throwException!CrateValidationException.withMessage("The space is not allowed to use a custom domain.");
    });

    it("adds the domainValidationKey when is not set", {
      space["slug"] = "test-domain";
      space["domain"] = "test-domain.giscollective.com";
      middleware.validateNewRecord(space);

      space["domainValidationKey"].to!string.length.should.not.equal(0);
      space["domainValidationKey"].to!string.should.not.equal("null");
    });

    describe("validateRecord", {
      it("accepts a custom domain when allowDomainChange is true", {
        space["slug"] = "name";
        space["allowDomainChange"] = true;

        space["domain"] = "example.com";
        middleware.validateRecord(space).should.not.throwAnyException;
      });

      it("does not acept a domain when is taken by another space", {
        anotherSpace["domain"] = "example.com";
        crates.space.updateItem(anotherSpace);

        space["slug"] = "name";
        space["allowDomainChange"] = true;

        space["domain"] = "example.com";
        middleware.validateRecord(space).should.throwException!CrateValidationException.withMessage("The domain is taken by another space.");
      });
    });

    describe("copyOriginalData", {
      it("copies the allowDomainChange when is admin is false", {
        space["allowDomainChange"] = true;
        middleware.copyOriginalData(space, false);

        space["allowDomainChange"].should.equal(false);
      });

      it("does not copy the allowDomainChange when is admin is true", {
        space["allowDomainChange"] = true;
        middleware.copyOriginalData(space, true);

        space["allowDomainChange"].should.equal(true);
      });
    });
  });
});
