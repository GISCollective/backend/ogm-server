module tests.spaces.middleware.spaceStyleOperation;

import tests.fixtures;
import vibe.http.common;
import ogm.defaults.spaces;
import ogm.defaults.teams;
import ogm.spaces.middleware.SpaceStyleOperation;
import crate.error;

alias suite = Spec!({
  describe("SpaceStyleOperation", {
    SpaceStyleOperation operation;
    Json space;

    beforeEach({
      setupTestData();
      crates.setupDefaultTeams;
      crates.checkDefaultSpaces(GeneralConfig());

      space = crates.space.getItem("000000000000000000000001").and.exec.front;
      space["visibility"]["isPublic"] = true;
      space["visibility"]["team"] = "000000000000000000000001";
      space["allowDomainChange"] = false;
      crates.space.updateItem(space);
      space = space.clone;
      space["domain"] = "new.domain.com";

      operation = new SpaceStyleOperation(crates);
    });

    it("returns the meta values when it exists", {
      crates.meta.addItem(`{
        "type": "cssStyle",
        "model": "Space",
        "itemId": "000000000000000000000001",
        "changeIndex": 0,
        "data": {
          "cssVars": "some vars"
        }}`.parseJsonString);


      operation.style("000000000000000000000001").should.equal("some vars");
    });

    it("returns an empty string when the meta does not have cssVars", {
      crates.meta.addItem(`{
        "type": "cssStyle",
        "model": "Space",
        "itemId": "000000000000000000000001",
        "changeIndex": 0,
        "data": {}
      }`.parseJsonString);

      operation.style("000000000000000000000001").should.equal("");
    });

    it("returns an empty string when the meta does not exist", {
      operation.style("000000000000000000000001").should.equal("");
    });
  });
});
