/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.spaces.put;

import tests.fixtures;
import vibe.http.common;
import ogm.spaces.api;
import ogm.defaults.spaces;
import ogm.defaults.teams;

alias suite = Spec!({
  URLRouter router;

  describe("Updating spaces", {
    Json space;

    beforeEach({
      setupTestData();
      router = new URLRouter;

      router.crateSetup.setupSpaceApi(crates, "http://localhost/");

      crates.setupDefaultTeams;
      crates.checkDefaultSpaces(GeneralConfig());

      auto storedSpace = crates.space.getItem("000000000000000000000001").and.exec.front;
      storedSpace["visibility"]["team"] = "000000000000000000000001";
      storedSpace["allowDomainChange"] = false;
      storedSpace["isTemplate"] = false;
      crates.space.updateItem(storedSpace);

      crates.preference.addItem(`{
        "name": "spaces.domains",
        "isSecret": true,
        "value": "giscollective.com,mapp.green"
      }`.parseJsonString);

      space = `{ "space": {
        "_id": "000000000000000000000001",
        "name": "test",
        "slug": "test",
        "domain": "test.giscollective.com",
        "allowDomainChange": false,
        "isTemplate": false,
        "domainStatus": "ready",
        "menu": {
          "items": []
        },
        "visibility": {
          "team":"000000000000000000000001",
          "isPublic": true,
          "isDefault": true
        }
      }}`.parseJsonString;
    });

    it("does not create a page when a space is updated", {
      auto initialSize = crates.space.get.size;

      router
        .request
        .put("/spaces/000000000000000000000001")
        .send(space)
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto page = crates.page.get.where("space").equal(response.bodyJson["space"]["_id"].to!string).and.exec;
          page.empty.should.equal(true);
        });
    });

    it("allows admins to update the allowDomainChange flag", {
      space["space"]["allowDomainChange"] = true;
      space["space"]["domain"] = "localhost";

      router
        .request
        .put("/spaces/000000000000000000000001")
        .send(space)
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["space"]["allowDomainChange"].should.equal(true);
          response.bodyJson["space"]["domainStatus"].should.equal("ready");
        });
    });

    it("does not allow owners to update the allowDomainChange flag", {
      space["space"]["allowDomainChange"] = true;

      router
        .request
        .put("/spaces/000000000000000000000001")
        .send(space)
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["space"]["allowDomainChange"].should.equal(false);
        });
    });

    describeCredentialsRule("set a as template", "yes", "spaces", ["administrator"], (string type) {
      space["space"]["isTemplate"] = true;

      router
        .request
        .put("/spaces/000000000000000000000001")
        .send(space)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["space"]["isTemplate"].to!string.should.equal("true");
        });
    });

    describeCredentialsRule("set a as template", "no", "spaces", ["owner"], (string type) {
      space["space"]["isTemplate"] = true;

      router
        .request
        .put("/spaces/000000000000000000000001")
        .send(space)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(400)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "errors": [{
              "description": "You can not change the isTemplate flag.",
              "status": 400,
              "title": "Validation error"
            }]
          }`.parseJsonString);
        });
    });

    describeCredentialsRule("set a as template", "no", "spaces", ["leader", "member", "guest"], (string type) {
      space["space"]["isTemplate"] = true;

      router
        .request
        .put("/spaces/000000000000000000000001")
        .send(space)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal("{
            \"errors\": [{
              \"description\": \"You don't have enough rights to edit `000000000000000000000001`.\",
              \"status\": 403,
              \"title\": \"Forbidden\"
            }]
          }".parseJsonString);
        });
    });

    describeCredentialsRule("update team spaces", "yes", "spaces", ["administrator", "owner"], (string type) {
      auto initialSize = crates.space.get.size;

      router
        .request
        .put("/spaces/000000000000000000000001")
        .send(space)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          crates.space.get.size.should.equal(initialSize);
          response.bodyJson["space"]["name"].to!string.should.equal("test");
        });
    });

    describeCredentialsRule("update team spaces", "no", "spaces", [ "leader", "member", "guest"], (string type) {
      auto initialSize = crates.space.get.size;

      router
        .request
        .put("/spaces/000000000000000000000001")
        .send(space)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{ "errors": [{
            "description": "You don't have enough rights to edit ` ~ "`000000000000000000000001`" ~ `.",
            "status": 403,
            "title": "Forbidden"
          }]}`).parseJsonString);
        });
    });

    describeCredentialsRule("update team spaces", "-", "spaces", "no rights", {
      router
        .request
        .put("/spaces/000000000000000000000001")
        .send(space)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
        });
    });

    describeCredentialsRule("update own spaces", "yes", "spaces", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      auto editablespace = crates.space.getItem("000000000000000000000001").and.exec.front;
      editablespace["info"]["originalAuthor"] = userId[type];
      crates.space.updateItem(editablespace);

      auto initialSize = crates.space.get.size;

      crateGetters["Team"] = &crates.team.getItem;
      crateGetters["Picture"] = &crates.picture.getItem;

      router
        .request
        .put("/spaces/000000000000000000000001")
        .send(space)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          crates.space.get.size.should.equal(initialSize);
          response.bodyJson["space"]["name"].to!string.should.equal("test");
        });
    });

    describeCredentialsRule("update own spaces", "-", "spaces", "no rights", {});
  });
});
