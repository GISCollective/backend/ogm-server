/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.spaces.api;

import vibe.http.router;
import vibe.core.log;
import vibe.data.json;

import crate.http.router;
import crate.base;
import ogm.defaults.spaces;

import ogm.auth;
import ogm.crates.all;

import ogm.filter.visibility;
import ogm.middleware.adminrequest;
import ogm.middleware.modelinfo;
import ogm.middleware.PublisherTeamMiddleware;
import ogm.middleware.SlugMiddleware;
import ogm.middleware.userdata;
import ogm.operations.space.PagesMap;
import ogm.spaces.middleware.LandingPageCreateMiddleware;
import ogm.spaces.middleware.landingPageIdFix;
import ogm.spaces.middleware.spaceDomainValidation;
import ogm.spaces.middleware.spaceFilters;
import ogm.spaces.middleware.SpaceMapper;
import ogm.spaces.middleware.SpaceStyleOperation;
import ogm.spaces.middleware.spaceTemplate;
import vibe.service.configuration.general;
import ogm.spaces.operations.PagesCategoryOperation;
import ogm.spaces.operations.SpaceSummaryOperation;
import ogm.spaces.operations.SiteMapOperation;

///
void setupSpaceApi(T)(CrateRouter!T crateRouter, OgmCrates crates, string apiUrl) {
  crateGetters["Team"] = &crates.team.getItem;
  crateGetters["Picture"] = &crates.picture.getItem;

  auto auth = Authentication.instance(crates.user);
  auto adminRequest = new AdminRequest(crates.user);

  auto userDataMiddleware = UserDataMiddleware.instance(crates);
  auto modelInfo = new ModelInfoMiddleware("space", crates.space);
  auto visibilityFilter = new VisibilityFilter!"spaces"(crates, crates.space);
  auto spaceDomainValidation = new SpaceDomainValidationMiddleware(crates);
  auto pagesMapOperation = new PagesMapOperation(crates);
  auto pagesCategoryOperation = new PagesCategoryOperation(crates);
  auto spaceSummaryOperation = new SpaceSummaryOperation(crates);
  auto spaceStyle = new SpaceStyleOperation(crates);
  auto landingPageIdFix = new LandingPageIdFixMiddleware(crates);
  auto spaceMapper = new SpaceMapper(apiUrl);
  auto spaceFilters = new SpaceFiltersMiddleware(crates);
  auto create = new LandingPageCreateMiddleware(crates);
  auto spaceTemplate = new SpaceTemplateMiddleware(crates);
  auto siteMapTxtOperation = new SiteMapTxtOperation(crates);
  auto siteMapTxtRootOperation = new SiteMapTxtOperation(crates, "/sitemap.txt");
  auto siteMapXmlOperation = new SiteMapXmlOperation(crates);
  auto siteMapXmlRootOperation = new SiteMapXmlOperation(crates, "/sitemap.xml");

  auto preparedRoute = crateRouter
    .prepare(crates.space)
    .withCustomOperation(pagesMapOperation)
    .withCustomOperation(pagesCategoryOperation)
    .withCustomOperation(spaceSummaryOperation)
    .withCustomOperation(spaceStyle)
    .withCustomOperation(siteMapTxtOperation)
    .withCustomOperation(siteMapTxtRootOperation)
    .withCustomOperation(siteMapXmlOperation)
    .withCustomOperation(siteMapXmlRootOperation)
    .and(auth.publicDataMiddleware)
    .and(adminRequest)
    .and(userDataMiddleware)
    .and(modelInfo)
    .and(visibilityFilter)
    .and(landingPageIdFix)
    .and(spaceFilters)
    .and(spaceDomainValidation)
    .and(spaceTemplate)
    .and(create)
    .and(spaceMapper);
}
