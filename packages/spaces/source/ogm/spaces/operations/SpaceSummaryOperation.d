/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.spaces.operations.SpaceSummaryOperation;

import std.datetime;
import std.path;
import std.string;
import std.range;
import std.algorithm;

import ogm.crates.all;
import crate.ctfe;
import crate.http.operations.base;
import crate.base;
import crate.error;
import crate.json;
import vibe.http.router;
import vibe.data.json;
import crate.collection.memory;
import crate.api.rest.serializer;

class SpaceSummaryOperation : CrateOperation!DefaultStorage {
  private {
    OgmCrates crates;
  }

  this(OgmCrates crates) {
    this.crates = crates;

    CrateRule rule;
    rule.request.path = "/spaces/_/summary";
    rule.request.method = HTTPMethod.GET;
    rule.response.mime = "application/json";
    rule.response.statusCode = 200;

    super(crates.space, rule);
  }

  override void handle(DefaultStorage storage) {
    auto result = Json.emptyObject;

    if("domain" !in storage.request.query) {
      result["errors"] = `[{
        "description": "The 'domain' query parameter is required.",
        "status": 403,
        "title": "Forbidden"
      }]`.parseJsonString;

      storage.response.writeJsonBody(result, 403, "text/json");
      return;
    }

    auto spaceRange = this.crates.space.get.where("domain").equal(storage.request.query["domain"]).and.exec;

    if(spaceRange.empty) {
      result["errors"] = `[{
        "description": "The requested domain does not belong to a space.",
        "status": 404,
        "title": "Space not found"
      }]`.parseJsonString;

      storage.response.writeJsonBody(result, 404, "text/json");
      return;
    }

    auto space = spaceRange.front;

    result["spaceSummary"] = Json.emptyObject;
    result["spaceSummary"]["name"] = space["name"];
    result["spaceSummary"]["isPublic"] = space["visibility"]["isPublic"];
    result["spaceSummary"]["logo"] = space["logo"];

    storage.response.writeJsonBody(result, 200, "text/json");
  }
}