/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.spaces.operations.PagesCategoryOperation;

import std.datetime;
import std.path;
import std.string;
import std.range;
import std.algorithm;

import ogm.crates.all;
import crate.ctfe;
import crate.http.operations.base;
import crate.base;
import crate.error;
import crate.json;
import vibe.http.router;
import vibe.data.json;
import crate.collection.memory;
import crate.api.rest.serializer;

class PagesCategoryOperation : CrateOperation!DefaultStorage {
  private {
    OgmCrates crates;
  }

  this(OgmCrates crates) {
    this.crates = crates;

    CrateRule rule;
    rule.request.path = "/spaces/:id/categories";
    rule.request.method = HTTPMethod.GET;
    rule.response.mime = "application/json";
    rule.response.statusCode = 200;

    super(crates.space, rule);
  }

  Json getPagesCategories(string id) {
    Json result = Json.emptyObject;

    auto pages = this.crates.page.get.where("space").equal(ObjectId.fromString(id)).and.exec;

    foreach (page; pages) {
      if(page["categories"].type != Json.Type.array) {
        continue;
      }

      foreach (Json category; page["categories"]) {
        result[category.to!string] = page["_id"].to!string;
      }
    }

    return result;
  }

  override void handle(DefaultStorage storage) {
    storage.item = this.prepareItemOperation!"getItem"(storage);

    if(storage.response.headerWritten) {
      return;
    }

    this.applyRule(storage);

    auto map = this.getPagesCategories(storage.item["_id"].to!string);

    storage.response.writeJsonBody(map, 200, "text/json");
  }
}