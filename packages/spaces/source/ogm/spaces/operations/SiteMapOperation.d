/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.spaces.operations.SiteMapOperation;

import std.datetime;
import std.path;
import std.string;
import std.range;
import std.algorithm;
import std.conv;

import ogm.crates.all;
import crate.ctfe;
import crate.http.operations.base;
import crate.base;
import crate.error;
import crate.json;
import vibe.http.router;
import vibe.data.json;
import crate.collection.memory;
import ogm.calendar;
import crate.api.rest.serializer;
import dxml.util;

struct SiteMapUrl {
  string loc;
  SysTime lastmod;
  string changefreq = "daily";
  int priority = 5;

  string toString() {
    auto lastmodValue = lastmod;
    if(lastmodValue.year < 2000) {
      lastmodValue.year = 2000;
    }

    return `  <url>
    <loc>` ~ encodeText(loc).array ~ `</loc>
    <lastmod>` ~ encodeText(lastmodValue.toISOExtString).array ~ `</lastmod>
    <changefreq>` ~ encodeText(changefreq).array ~ `</changefreq>
    <priority>` ~ (priority.to!double/10).to!string ~ `</priority>
  </url>`;
  }
}

enum pageSize = 10_000;

// can convert a site map url to a xml string
unittest {
  import fluent.asserts;

  auto calendarMock = new CalendarMock("2023-01-12T16:20:11Z");
  SysCalendar.instance = calendarMock;

  SiteMapUrl("https://test.com/test", SysCalendar.instance.now, "daily", 9).toString
    .should.equal(
`  <url>
    <loc>https://test.com/test</loc>
    <lastmod>2023-01-12T16:20:11Z</lastmod>
    <changefreq>daily</changefreq>
    <priority>0.9</priority>
  </url>`);
}

/// it uses the year 2000 for old dates
unittest {
  import fluent.asserts;

  auto calendarMock = new CalendarMock("1999-01-12T16:20:11Z");
  SysCalendar.instance = calendarMock;

  SiteMapUrl("https://test.com/test", SysCalendar.instance.now, "daily", 9).toString
    .should.equal(
`  <url>
    <loc>https://test.com/test</loc>
    <lastmod>2000-01-12T16:20:11Z</lastmod>
    <changefreq>daily</changefreq>
    <priority>0.9</priority>
  </url>`);
}

SiteMapUrl fromArticle(Json a) {
  auto id = (a["slug"].type == Json.Type.string && a["slug"].to!string != "") ? a["slug"].to!string : a["_id"].to!string;

  SysTime lastmod = SysCalendar.instance.now;

  if("info" in a && "lastChangeOn" in a["info"]) {
    lastmod = SysTime.fromISOExtString(a["info"]["lastChangeOn"].to!string);
  }

  return SiteMapUrl(id, lastmod);
}

SiteMapUrl fromRecord(Json a) {
  auto id = a["_id"].to!string;

  SysTime lastmod = SysCalendar.instance.now;

  if("info" in a && "lastChangeOn" in a["info"]) {
    lastmod = SysTime.fromISOExtString(a["info"]["lastChangeOn"].to!string);
  }

  return SiteMapUrl(id, lastmod);
}

SiteMapUrl[] getIds(CrateAccess crate, bool isDefault, string team, string[] categories) {
  auto query = crate.get.where("visibility.isPublic").equal(true);

  if(!isDefault) {
    query = query.and.where("visibility.team").equal(ObjectId(team));
  }

  auto ids = categories.filter!(a => isObjectId(a)).map!(a => ObjectId(a)).array;

  if(ids.length) {
    query = query.and.where("_id").anyOf(ids);
  }

  auto range = query.and.sort("_id", 1).withProjection(["_id", "info"]).and.exec;

  return range.map!(a => fromRecord(a)).array;
}


SiteMapUrl[] getArticleIds(CrateAccess crate, bool isDefault, string team, string[] categories) {
  auto query = crate.get.where("visibility.isPublic").equal(true).and.where("visibility.team").equal(ObjectId(team)).and;

  if(categories.length) {
    auto orQuery = query.or;
    size_t i;

    foreach(category; categories) {
      if(i > 0) {
        orQuery = orQuery.or;
      }

      orQuery.where("categories").arrayContains(category);

      i++;
    }
  }

  auto range = query.sort("_id", 1).withProjection(["_id", "slug", "info"]).and.exec;

  return range.map!(a => fromArticle(a)).array;
}

SiteMapUrl[] getFeatureIds(CrateAccess crate, string parentField, bool isDefault, string team, string[] categories) {
  auto query = crate.get.where("visibility").equal(1);

  if(!isDefault) {
    query = query.and.where("computedVisibility.team").equal(ObjectId(team));
  }

  auto ids = categories.filter!(a => isObjectId(a)).map!(a => ObjectId(a)).array;

  if(ids.length) {
    auto orQuery = query.or;
    size_t i;

    foreach(id; ids) {
      if(i > 0) {
        orQuery = orQuery.or;
      }

      orQuery.where(parentField).arrayContains(id);

      i++;
    }
  }

  auto range = query.and.sort("_id", 1).withProjection(["_id", "info"]).and.exec.array;

  return range.map!(a => fromRecord(a)).array;
}

size_t getFeatureCount(CrateAccess crate, bool isDefault, string team, string[] categories) {
  auto query = crate.get.where("visibility").equal(1);

  if(!isDefault) {
    query = query.and.where("computedVisibility.team").equal(ObjectId(team));
  }

  auto ids = categories.filter!(a => isObjectId(a)).map!(a => ObjectId(a)).array;

  if(ids.length) {
    auto orQuery = query.or;
    size_t i;

    foreach(id; ids) {
      if(i > 0) {
        orQuery = orQuery.or;
      }

      orQuery.where("map").arrayContains(id);

      i++;
    }
  }

  return query.and.sort("_id", 1).withProjection(["_id", "info"]).and.size;
}

SiteMapUrl[] getRecordIds(CrateAccess crate, string parentField, bool isDefault, string team, string[] categories) {
  auto query = crate.get.where("visibility.isPublic").equal(true);

  if(!isDefault) {
    query = query.and.where("visibility.team").equal(ObjectId(team));
  }

  auto ids = categories.filter!(a => isObjectId(a)).map!(a => ObjectId(a)).array;

  if(ids.length) {
    query = query.and.where(parentField).anyOf(ids);
  }

  auto range = query.and.sort("_id", 1).withProjection(["_id"]).and.exec.array;

  return range.map!(a => fromRecord(a)).array;
}

class SiteMapTxtOperation : CrateOperation!DefaultStorage {
  private {
    OgmCrates crates;
  }

  this(OgmCrates crates) {
    this(crates, "/spaces/:id/sitemap.txt");
  }

  this(OgmCrates crates, string path) {
    this.crates = crates;

    CrateRule rule;
    rule.request.path = path;
    rule.request.method = HTTPMethod.GET;
    rule.response.mime = "text/plain";
    rule.response.statusCode = 200;

    super(crates.space, rule);
  }

  string[] getPagePaths(Json page, Json space) {
    auto pieces = page["slug"].to!string.split("--");
    auto baseUrl = "https://" ~ space["domain"].to!string ~ "/";

    if(pieces.length > 0 && pieces[0].length > 0 && pieces[0][0] == '_') {
      return [];
    }

    if(!pieces.canFind!(a => a.length > 0 && a[0] == ':')) {
      return [baseUrl ~ pieces.join("/")];
    }

    string[] result;

    bool isDefault = space["visibility"]["isDefault"].to!bool;
    string team = space["visibility"]["team"].to!string;
    string[] categories;

    if(page["categories"].type == Json.Type.array) {
      categories = page["categories"].deserializeJson!(string[]);
    }

    SiteMapUrl[] urlList;

    foreach(size_t i; 0..pieces.length) {
      if(pieces[i] == ":map-id") {
        urlList = getIds(crates.map, isDefault, team, categories);
      }

      if(pieces[i] == ":campaign-id" || pieces[i] == ":survey-id") {
        urlList = getIds(crates.campaign, isDefault, team, categories);
      }

      if(pieces[i] == ":calendar-id") {
        urlList = getIds(crates.calendar, isDefault, team, categories);
      }

      if(pieces[i] == ":icon-set-id") {
        urlList = getIds(crates.iconSet, isDefault, team, categories);
      }

      if(pieces[i] == ":article-id") {
        urlList = getArticleIds(crates.article, isDefault, team, categories);
      }

      if(pieces[i] == ":feature-id") {
        urlList = getFeatureIds(crates.feature, "maps", isDefault, team, categories);
      }

      if(pieces[i] == ":icon-id") {
        urlList = getRecordIds(crates.icon, "iconSet", isDefault, team, categories);
      }

      if(pieces[i] == ":event-id") {
        urlList = getRecordIds(crates.event, "calendar", isDefault, team, categories);
      }

      foreach(url; urlList) {
        pieces[i] = url.loc;
        result ~= baseUrl ~ pieces.join("/");
      }
    }

    return result;
  }

  override void handle(DefaultStorage storage) {
    string id = getPathVar!"id"(storage.request.requestPath.to!string, rule.request.path);

    if(id != "_" && id != "") {
      storage.item = this.prepareItemOperation!"getItem"(storage);

      if(storage.response.headerWritten) {
        return;
      }

      this.applyRule(storage);
    }

    if(id == "_" || id == "") {
      auto range = crates.space.get.where("domain").equal(storage.request.host.split(":")[0]).and.exec;

      if(range.empty) {
        storage.response.writeBody("", 200, "text/plain");
        return;
      }

      storage.item = range.front;
    }

    if(storage.item["visibility"]["isPublic"] == false) {
      storage.response.writeBody("", 200, "text/plain");
      return;
    }

    auto pages = this.crates.page.get
      .where("space").equal(ObjectId(storage.item["_id"])).and
      .sort("_id", 1)
      .exec;
    auto response = storage.response;

    response.contentType = "text/plain";
    response.statusCode = 200;

    foreach (page; pages) {
      auto pathList = this.getPagePaths(page, storage.item).join("\n");

      response.bodyWriter.write(pathList);
      response.bodyWriter.write("\n");
    }

    response.bodyWriter.finalize;
  }
}

class SiteMapXmlOperation : CrateOperation!DefaultStorage {
  private {
    OgmCrates crates;
  }

  this(OgmCrates crates) {
    this(crates, "/spaces/:id/sitemap.xml");
  }

  this(OgmCrates crates, string path) {
    this.crates = crates;

    CrateRule rule;
    rule.request.path = path;
    rule.request.method = HTTPMethod.GET;
    rule.response.mime = "application/xml";
    rule.response.statusCode = 200;

    super(crates.space, rule);
  }

  SiteMapUrl[] getPagePaths(Json page, Json space, bool showFeaturesOnly, ref size_t featureIndex, size_t startFeature, size_t endFeature) {
    auto pieces = page["slug"].to!string.split("--");
    auto baseUrl = "https://" ~ space["domain"].to!string ~ "/";

    if(pieces.length > 0 && pieces[0].length > 0 && pieces[0][0] == '_') {
      return [];
    }

    if(!pieces.canFind!(a => a.length > 0 && a[0] == ':')) {
      auto url = fromRecord(page);
      url.loc = baseUrl ~ pieces.join("/");

      return [ url ];
    }

    SiteMapUrl[] result;

    bool isDefault = space["visibility"]["isDefault"].to!bool;
    string team = space["visibility"]["team"].to!string;
    string[] categories;

    if(page["categories"].type == Json.Type.array) {
      categories = page["categories"].deserializeJson!(string[]);
    }

    SiteMapUrl[] urlList;

    foreach(size_t i; 0..pieces.length) {
      if(!showFeaturesOnly) {
        if(pieces[i] == ":map-id") {
          urlList = getIds(crates.map, isDefault, team, categories);
        }

        if(pieces[i] == ":campaign-id" || pieces[i] == ":survey-id") {
          urlList = getIds(crates.campaign, isDefault, team, categories);
        }

        if(pieces[i] == ":calendar-id") {
          urlList = getIds(crates.calendar, isDefault, team, categories);
        }

        if(pieces[i] == ":icon-set-id") {
          urlList = getIds(crates.iconSet, isDefault, team, categories);
        }

        if(pieces[i] == ":article-id") {
          urlList = getArticleIds(crates.article, isDefault, team, categories);
        }

        if(pieces[i] == ":icon-id") {
          urlList = getRecordIds(crates.icon, "iconSet", isDefault, team, categories);
        }

        if(pieces[i] == ":event-id") {
          urlList = getRecordIds(crates.event, "calendar", isDefault, team, categories);
        }
      }

      if(pieces[i] == ":feature-id") {
        auto idList = getFeatureIds(crates.feature, "maps", isDefault, team, categories);

        foreach(id; idList) {
          featureIndex++;

          if(featureIndex < startFeature || featureIndex > endFeature) {
            continue;
          }

          urlList ~= id;
        }
      }

      foreach(url; urlList) {
        pieces[i] = url.loc;
        url.loc = baseUrl ~ pieces.join("/");

        result ~= url;
      }
    }

    return result;
  }

  void handleSiteMap(DefaultStorage storage) {
    size_t startFeature;
    size_t endFeature;
    bool showFeaturesOnly = false;

    if("page" !in storage.request.query) {
      endFeature = pageSize;
    }

    if("page" in storage.request.query) {
      auto page = storage.request.query["page"].to!size_t;

      if(page == 0) {
        startFeature = size_t.max;
        endFeature = size_t.max;
      } else {
        startFeature = (page - 1) * pageSize;
        endFeature = startFeature + pageSize;
        showFeaturesOnly = true;
      }
    }

    auto pages = this.crates.page.get
      .where("space").equal(ObjectId(storage.item["_id"])).and
      .sort("_id", 1)
      .exec;
    auto response = storage.response;
    size_t featureIndex;

    response.contentType = "application/xml";
    response.statusCode = 200;

    response.bodyWriter.write(`<?xml version="1.0" encoding="UTF-8"?>` ~ "\n" ~
    `<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">`~ "\n");

    foreach (page; pages) {
      if(showFeaturesOnly && !page["slug"].to!string.canFind(":feature-id")) {
        continue;
      }

      auto pathList = this.getPagePaths(page, storage.item, showFeaturesOnly, featureIndex, startFeature, endFeature).map!(a => a.toString).array.join("\n");

      response.bodyWriter.write(pathList);
      response.bodyWriter.write("\n");
    }

    response.bodyWriter.write(`</urlset>`);
    response.bodyWriter.finalize;
  }

  void handleSiteIndex(DefaultStorage storage, size_t featureCount) {
    auto baseUrl = "https://" ~ storage.item["domain"].to!string ~ "/";
    auto pages = this.crates.page.get
      .where("space").equal(ObjectId(storage.item["_id"])).and
      .sort("_id", 1)
      .exec;
    auto response = storage.response;

    response.contentType = "application/xml";
    response.statusCode = 200;

    response.bodyWriter.write(`<?xml version="1.0" encoding="UTF-8"?>` ~ "\n" ~
    `<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">`~ "\n" ~
    `<sitemap>` ~ "\n" ~
    `  <loc>` ~ baseUrl ~ `sitemap.xml?page=0</loc>` ~ "\n" ~
    `</sitemap>` ~ "\n");

    auto pageCount = featureCount / pageSize + (featureCount % pageSize ? 1 : 0);

    foreach (size_t i; 0 .. pageCount) {
      response.bodyWriter.write(`<sitemap>` ~ "\n" ~
    `  <loc>` ~ baseUrl ~ `sitemap.xml?page=` ~ (i+1).to!string ~ `</loc>` ~ "\n" ~
    `</sitemap>` ~ "\n");
    }

    response.bodyWriter.write(`</sitemapindex>`);
    response.bodyWriter.finalize;
  }

  override void handle(DefaultStorage storage) {
    auto id = getPathVar!"id"(storage.request.requestPath.to!string, rule.request.path);

    if(id != "_" && id != "") {
      storage.item = this.prepareItemOperation!"getItem"(storage);

      if(storage.response.headerWritten) {
        return;
      }

      this.applyRule(storage);
    }

    if(id == "_" || id == "") {
      auto range = crates.space.get.where("domain").equal(storage.request.host.split(":")[0]).and.exec;

      if(range.empty) {
        storage.response.writeBody("", 200, "application/xml");
        return;
      }

      storage.item = range.front;
    }

    if(storage.item["visibility"]["isPublic"] == false) {
      storage.response.writeBody("", 200, "application/xml");
      return;
    }

    size_t featureCount;

    bool isDefault = storage.item["visibility"]["isDefault"].to!bool;
    string team = storage.item["visibility"]["team"].to!string;
    auto pages = this.crates.page.get
      .where("space").equal(ObjectId(storage.item["_id"])).and
      .exec;

    string[] categories;

    foreach(page; pages) {
      string slug = page["slug"].to!string;

      if(!slug.canFind(":feature-id")) {
        continue;
      }

      if(page["categories"].type == Json.Type.array) {
        categories = page["categories"].deserializeJson!(string[]);
      } else {
        categories = [];
      }

      featureCount += getFeatureCount(crates.feature, isDefault, team, categories);
    }

    if(featureCount > pageSize && "page" !in storage.request.query) {
      handleSiteIndex(storage, featureCount);
      return;
    }

    handleSiteMap(storage);
  }
}