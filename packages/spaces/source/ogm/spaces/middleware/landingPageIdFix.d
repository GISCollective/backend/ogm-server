module ogm.spaces.middleware.landingPageIdFix;

import vibe.data.json;
import crate.base;
import ogm.crates.all;

class LandingPageIdFixMiddleware {
  private {
    OgmCrates crates;
  }

  ///
  this(OgmCrates crates) {
    this.crates = crates;
  }

  @mapper
  void fixLandingPageId(ref Json record) {
    if(!isObjectId(record["landingPageId"])) {
      record.remove("landingPageId");
      return;
    }

    const count = crates.page.get.where("_id").equal(ObjectId.fromJson(record["landingPageId"])).and.size;

    if(count == 0) {
      record.remove("landingPageId");
    }
  }
}