module ogm.spaces.middleware.spaceFilters;


import ogm.crates.all;
import ogm.http.request;

import vibe.http.router;
import vibe.data.json;

import ogm.http.request;

import crate.error;
import crate.base;
import crate.json;

import std.regex;
import std.string;
import std.algorithm;
import std.uuid;
import std.ascii;

class SpaceFiltersMiddleware {
  struct Params {
    @describe("Filter records by domain")
    string domain;
  }

  private {
    OgmCrates crates;
  }

  ///
  this(OgmCrates crates) {
    this.crates = crates;
  }

  @getList @getItem
  void paramsFilter(HTTPServerRequest req, IQuery selector, Params params) {
    auto request = RequestUserData(req);

    if(params.domain != "") {
      selector.where("domain").equal(params.domain);
    }
  }
}