module ogm.spaces.middleware.spaceTemplate;

import ogm.crates.all;
import ogm.http.request;

import vibe.http.router;
import vibe.data.json;

import ogm.http.request;
import gis_collective.hmq.log;

import crate.error;
import crate.base;
import crate.json;

import std.regex;
import std.string;
import std.algorithm;
import std.uuid;
import std.ascii;
import std.array;

class SpaceTemplateMiddleware {
  struct Params {
    @describe("Filter template spaces")
    string tpl;
  }

  private {
    OgmCrates crates;
  }

  ///
  this(OgmCrates crates) {
    this.crates = crates;
  }

  Json clonePage(Json sourceId, const ref Json space) {
    Json page;

    try {
      page = this.crates.page.getItem(sourceId.to!string).and.exec.front.clone;
    } catch(Exception e) {
      return Json("");
    }

    page.remove("_id");
    page["info"] = space["info"];
    page["visibility"]["team"] = space["visibility"]["team"];
    page["visibility"]["isDefault"] = false;
    page["space"] = space["_id"];

    page = this.crates.page.addItem(page);

    page["cols"] = this.clonePageCols(page["cols"], page["_id"].to!string, page["visibility"]["team"].to!string, page["info"]);

    this.crates.page.updateItem(page);

    return page["_id"];
  }

  Json clonePageCols(Json cols, string pageId, string teamId, ref Json info) {
    return cols.byValue.map!(a => cloneDataSource(a, pageId, teamId, info)).array.serializeToJson;
  }

  Json cloneDataSource(Json col, string recordId, string teamId, ref Json info) {
    if("data" !in col) {
      return col;
    }

    if("source" !in col["data"]) {
      return col;
    }

    auto model = col["data"]["source"]["model"].to!string;

    if(model == "picture" && isObjectId(col["data"]["source"]["id"])) {
      auto picture = crates.picture.clone(col["data"]["source"]["id"].to!string);
      picture["meta"]["link"] = Json.emptyObject;

      if(picture["meta"]["link"]["model"].type != Json.Type.string) {
        picture["meta"]["link"]["model"] = "page";
      }
      picture["meta"]["link"]["modelId"] = recordId;

      col["data"]["source"]["id"] = picture["_id"];
    }

    if(model == "article" && isObjectId(col["data"]["source"]["id"])) {
      auto article = cloneArticle(col["data"]["source"]["id"].to!string, teamId, info);

      col["data"]["source"]["id"] = article["_id"];
    }

    return col;
  }

  Json cloneArticle(string articleId, string teamId, ref Json info) {
    Json article;

    try {
      article = crates.article.getItem(articleId).and.exec.front;
    } catch(Exception e) {
      return Json.emptyObject;
    }

    auto teamArticle = crates.article.get
      .where("slug").equal(article["slug"].to!string).and
      .where("visibility.team").equal(ObjectId(teamId)).and
      .and.exec;

    string teamSlug = teamArticle.empty ? "" : teamArticle.front["slug"].to!string;

    if(teamSlug != "" && teamSlug == article["slug"]) {
      return teamArticle.front;
    }

    article.remove("_id");
    article["info"] = info;
    article["visibility"]["team"] = teamId;

    return crates.article.addItem(article);
  }

  Json fixPageIds(Json cols, ref string[string] clonedPages) {
    foreach(size_t index, col; cols) {
      cols[index] = fixPageColsIds(col, clonedPages);
    }

    return cols;
  }

  Json fixPageColsIds(Json colSource, ref string[string] clonedPages) {
    auto col = colSource.clone;

    if(col.exists("data.source.model") && col.exists("data.source.id") && col["data"]["source"]["model"] == "page") {
      auto value = col["data"]["source"]["id"].to!string;

      if(value in clonedPages) {
        col["data"]["source"]["id"] = clonedPages[value];
      } else {
        col["data"]["source"]["id"] = "";
      }
    }

    auto flatCol = col.toFlatJson;

    foreach(string key, Json value; flatCol) {
      if(!key.endsWith(".pageId")) {
        continue;
      }

      if(value.to!string in clonedPages) {
        flatCol[key] = clonedPages[value.to!string];
      }
    }

    return flatCol.toNestedJson;
  }

  @getList @getItem
  IQuery paramsFilter(IQuery selector, Params params) {
    if(params.tpl == "true") {
      selector.where("isTemplate").equal(true);
    }

    return selector;
  }

  @mapper
  void createNewSpace(ref Json item, HTTPServerRequest request) {
    if(request.method != HTTPMethod.POST) {
      return;
    }

    if("space" !in request.json) {
      return;
    }

    if("template" !in request.json["space"]) {
      return;
    }

    auto templateId = request.json["space"]["template"].to!string;
    auto templateSpace = crates.space.getItem(templateId).and.exec.front;

    enforce!CrateValidationException("isTemplate" in templateSpace && templateSpace["isTemplate"].to!bool, "The template space must be a template.");

    string[string] clonedPages;

    item["landingPageId"] = this.clonePage(templateSpace["landingPageId"], item);
    clonedPages[templateSpace["landingPageId"].to!string] = item["landingPageId"].to!string;

    item["globalMapPageId"] = this.clonePage(templateSpace["globalMapPageId"], item);
    clonedPages[templateSpace["globalMapPageId"].to!string] = item["globalMapPageId"].to!string;

    item["cols"] = templateSpace["cols"];
    item["layoutContainers"] = templateSpace["layoutContainers"];
    item["cover"] = templateSpace["cover"];
    item["logo"] = templateSpace["logo"];
    item["logoSquare"] = templateSpace["logoSquare"];

    auto pages = crates.page.get.where("space").equal(ObjectId.fromString(templateId)).and.exec;

    foreach(page; pages) {
      if(page["_id"].to!string in clonedPages) {
        continue;
      }

      try {
        clonedPages[page["_id"].to!string] = this.clonePage(page["_id"], item).to!string;
      } catch(Exception e) {
        error(e);
      }
    }

    foreach(id; clonedPages.byValue) {
      if(!isObjectId(id)) {
        continue;
      }

      auto page = this.crates.page.getItem(id).and.exec.front;
      page["cols"] = fixPageIds(page["cols"], clonedPages);

      this.crates.page.updateItem(page);
    }

    foreach(string key, Json col; item["cols"]) {
      item["cols"][key] = cloneDataSource(item["cols"][key], item["_id"].to!string, item["visibility"]["team"].to!string, item["info"]);
      item["cols"][key] = fixPageColsIds(item["cols"][key], clonedPages);
    }

    this.crates.space.updateItem(item);
  }
}