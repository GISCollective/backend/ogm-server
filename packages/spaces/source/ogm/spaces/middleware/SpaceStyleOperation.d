module ogm.spaces.middleware.SpaceStyleOperation;

import crate.http.operations.base;
import ogm.crates.all;
import ogm.http.request;

import vibe.http.router;
import vibe.data.json;

import ogm.http.request;

import crate.error;
import crate.base;
import crate.json;

class SpaceStyleOperation : CrateOperation!DefaultStorage {
  private {
    OgmCrates crates;
  }

  ///
  this(OgmCrates crates) {
    this.crates = crates;

    CrateRule rule;
    rule.request.path = "/spaces/:id/style";
    rule.request.method = HTTPMethod.GET;
    rule.response.mime = "text/css";
    rule.response.statusCode = 200;

    super(crates.space, rule);
  }

  string style(string id) {
    auto range = crates.meta.get
      .where("type").equal("cssStyle").and
      .where("model").equal("Space").and
      .where("itemId").equal(id).and
      .exec;

    if(range.empty) {
      return "";
    }

    if("data" !in range.front || "cssVars" !in range.front["data"]) {
      return "";
    }

    return range.front["data"]["cssVars"].to!string;
  }

  override void handle(DefaultStorage storage) {
    auto id = getPathVar!"id"(storage.request.requestPath, rule.request.path);

    auto style = this.style(id);

    storage.response.writeBody(style, 200, "text/css");
  }
}