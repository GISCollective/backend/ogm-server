module ogm.spaces.middleware.SpaceMapper;

import vibe.http.router;
import vibe.data.json;
import crate.base;
import crate.url;
import std.path;

class SpaceMapper {
  string baseUrl;

  this(string baseUrl) {
    this.baseUrl = baseUrl;
  }

  @mapper
  void addStyleProperty(HTTPServerRequest req, ref Json item) {
    auto styleBaseUrl = this.baseUrl.replaceHost(req.host, req.tls);

    item["style"] = styleBaseUrl.buildPath("spaces", item["_id"].to!string, "style");
  }
}