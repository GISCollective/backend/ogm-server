module ogm.spaces.middleware.spaceDomainValidation;


import ogm.crates.all;
import ogm.http.request;

import vibe.http.router;
import vibe.data.json;

import ogm.http.request;

import crate.error;
import crate.base;
import crate.json;

import std.regex;
import std.string;
import std.algorithm;
import std.uuid;
import std.ascii;

class SpaceDomainValidationMiddleware {
  private {
    OgmCrates crates;
  }

  ///
  this(OgmCrates crates) {
    this.crates = crates;
  }

  @create
  void createNewSpace(HTTPServerRequest request) {
    if("space" !in request.json) {
      return;
    }

    validateNewRecord(request.json["space"]);
    request.json["space"]["domainStatus"] = "pending";
  }

  @put @patch
  void updateSpace(HTTPServerRequest req) {
    if("space" !in req.json) {
      return;
    }

    scope request = RequestUserData(req);

    req.json["space"]["_id"] = request.itemId;

    copyOriginalData(req.json["space"], request.isAdmin);
    validateRecord(req.json["space"]);
  }

  void copyOriginalData(ref Json space, bool isAdmin) {
    if(isAdmin) {
      return;
    }

    auto spaceRange = crates.space.get.where("_id").equal(ObjectId.fromJson(space["_id"])).and.exec;

    space["allowDomainChange"] = spaceRange.front["allowDomainChange"];
    space["domainStatus"] = spaceRange.front["domainStatus"];
  }

  void validateRecord(ref Json space) {
    auto domain = space["domain"].to!string.strip;
    auto id = space["_id"].to!string;
    Json currentSpace;

    if(id.isObjectId) {
      auto currentSpaceRange = crates.space.get.where("_id").equal(ObjectId(id)).and.exec;
      currentSpace = currentSpaceRange.empty ? Json.emptyObject : currentSpaceRange.front;
    }

    enforce!CrateValidationException(domain != "" && domain != "null", "The domain is not set. Set the domain before creating a new space.");

    foreach (char ch; domain) {
      enforce!CrateValidationException(ch.isAlphaNum || ch == '.' || ch == '-' || ch == '_', "The domain name is invalid.");
    }

    auto currentDomainRange = crates.space.get.where("domain").equal(domain).and.exec;

    if(!currentDomainRange.empty && id.isObjectId) {
      enforce!CrateValidationException(currentDomainRange.front["_id"] == id, "The domain is taken by another space.");
    }

    auto domainsRange = crates.preference.get.where("name").equal("spaces.domains").and.exec;
    enforce!CrateValidationException(!domainsRange.empty, "The spaces domains preference is not set.");

    auto availableDomains = domainsRange.front["value"].to!string.split(",");
    auto allowDomainChange = space["allowDomainChange"].to!bool;

    if(!allowDomainChange) {
      auto pointIndex = domain.indexOf('.');
      auto parentDomain = domain[pointIndex + 1 ..$];

      enforce!CrateValidationException(availableDomains.canFind(parentDomain), "The space is not allowed to use a custom domain.");
    }

    if(currentSpace.type == Json.Type.object && currentSpace["domain"].type == Json.Type.string && currentSpace["domain"] == domain) {
      return;
    }

    space["domain"] = domain;
    space["domainStatus"] = "pending";
  }

  void validateNewRecord(ref Json space) {
    space["allowDomainChange"] = false;
    space.remove("_id");

    validateRecord(space);

    auto uuid = randomUUID();
    space["domainValidationKey"] = uuid.toString();
  }
}