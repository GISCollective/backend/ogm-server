module ogm.spaces.middleware.LandingPageCreateMiddleware;

import crate.http.operations.base;
import vibe.data.json;
import crate.base;
import crate.error;
import std.exception;
import ogm.crates.all;
import ogm.models.page;
import vibe.http.server;
import crate.lazydata.base;
import ogm.http.request;

class LandingPageCreateMiddleware {
  private {
    OgmCrates crates;
  }

  ///
  this(OgmCrates crates) {
    this.crates = crates;
  }

  @create
  void createLandingPage(HTTPServerRequest request) {
    enforce!CrateValidationException(request.json["space"]["isTemplate"] != true, "You can not create template spaces.");
  }

  @put @patch
  void isTemplateFlagValidation(RequestProperties properties, HTTPServerRequest req) {
    scope request = RequestUserData(req);
    auto space = crates.space.getItem(properties.itemId).and.exec.front;

    if(!request.isAdmin) {
      enforce!CrateValidationException(req.json["space"]["isTemplate"] == space["isTemplate"], "You can not change the isTemplate flag.");
    }
  }

  @mapper
  void createLandingPage(ref Json item, RequestProperties properties, HTTPServerRequest request) {
    if(request.method != HTTPMethod.POST) {
      return;
    }

    if("template" in request.json["space"]) {
      return;
    }

    auto page = Json.emptyObject;
    page["info"] = item["info"];
    page["visibility"] = item["visibility"];
    page["cols"] = Json.emptyArray;
    page["name"] = "Homepage";
    page["slug"] = "";
    page["space"] = properties.itemId;

    page = crates.page.addItem(page);

    item["landingPageId"] = page["_id"];

    crates.space.updateItem(item);
  }
}