/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.sounds.mapper;

import crate.attributes;
import crate.url;
import ogm.crates.all;
import ogm.models.sound;
import std.path;
import vibe.data.json;
import vibe.http.server;

class SoundsMapper {
  OgmCrates crates;

  this(OgmCrates crates) {
    this.crates = crates;
  }

  @put @replace
  void checkUpdatedSound(HTTPServerRequest request) {
    if("sound" !in request.json || "sound" !in request.json["sound"]) {
      return;
    }

    const oldValue = crates.sound.getItem(request.params["id"]).and.withProjection(["sound"]).exec.front;

    request.json["sound"]["sound"] = oldValue["sound"];
  }

  @mapper
  void mapper(HTTPServerRequest req, ref Json item) @safe {
    auto baseUrl = SoundFileSettings.baseUrl.replaceHost(req.host, req.tls);

    item["sound"] = baseUrl.buildPath("sounds", item["_id"].to!string, "sound");
  }
}
