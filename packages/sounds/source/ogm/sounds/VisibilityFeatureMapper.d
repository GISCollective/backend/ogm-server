/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.sounds.VisibilityFeatureMapper;

import ogm.crates.all;
import crate.attributes;
import vibe.http.server;
import vibe.data.json;
import std.exception;

class VisibilityFeatureMapper {
  OgmCrates crates;

  this(OgmCrates crates) {
    this.crates = crates;
  }

  @create @put @patch
  void updateVisibility(HTTPServerRequest request) {
    if("sound" !in request.json) {
      return;
    }

    auto item = request.json["sound"];

    if(item["campaignAnswer"].type == Json.Type.string && item["campaignAnswer"] != "") {
      auto campaignAnswerId = item["campaignAnswer"].to!string;
      auto campaignAnswer = crates.campaignAnswer.getItem(campaignAnswerId).and.exec.front;
      auto campaign = crates.campaign.getItem(campaignAnswer["campaign"].to!string).and.exec.front;

      request.json["sound"]["visibility"] = campaign["visibility"];
      request.json["sound"]["visibility"]["isPublic"] = false;
      request.json["sound"]["visibility"]["isDefault"] = false;

      return;
    }

    if("feature" !in item || item["feature"] == "" || item["feature"] == null) {
      return;
    }

    auto featureId = item["feature"].to!string;
    auto feature = crates.feature.getItem(featureId).and.exec.front;

    enforce("maps" in feature, "The selected feature has no map list.");
    enforce(feature["maps"].length > 0, "The selected feature has no maps in the list.");

    auto mapId = feature["maps"][0].to!string;
    auto map = crates.map.getItem(mapId).and.exec.front;

    request.json["sound"]["visibility"] = map["visibility"];
    request.json["sound"]["visibility"]["isPublic"] = feature["visibility"] == 1;
    request.json["sound"]["visibility"]["isDefault"] = false;
  }
}