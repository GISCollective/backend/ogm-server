/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.sounds.configuration;

import vibe.service.configuration.general;

///
struct MicroSoundsConfig {
  /// Configurations for all services
  GeneralConfig general;

  ///
  SoundsConfiguration sounds;
}

///
struct SoundsConfiguration {
  string location;
  string baseUrl;
}
