/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.sounds.api;

import ogm.auth;

import crate.auth.middleware;
import crate.http.router;
import crate.base;

import vibe.http.router;

import ogm.middleware.adminrequest;
import ogm.sounds.mapper;
import ogm.crates.all;
import ogm.crates.defaults;
import ogm.middleware.userdata;
import ogm.sounds.configuration;
import ogm.models.sound;
import ogm.auth;
import ogm.filter.visibility;
import ogm.middleware.modelinfo;
import ogm.sounds.VisibilityFeatureMapper;

///
void setupSoundApi(T)(CrateRouter!T crateRouter, OgmCrates crates, SoundsConfiguration configuration) {
  crateGetters["Team"] = &crates.team.getItem;
  crateGetters["Picture"] = &crates.picture.getItem;

  SoundFileSettings.path = configuration.location;
  SoundFileSettings.baseUrl = configuration.baseUrl;
  SoundFileSettings.files = crates.soundFiles;
  SoundFileSettings.chunks = crates.soundChunks;

  auto soundsMapper = new SoundsMapper(crates);

  auto auth = Authentication.instance(crates.user);
  auto adminRequest = new AdminRequest(crates.user);
  auto userDataMiddleware = UserDataMiddleware.instance(crates);
  auto visibilityFeatureMapper = new VisibilityFeatureMapper(crates);
  auto visibilityFilter = new VisibilityFilter!("sounds", true)(crates, crates.sound);
  auto modelInfo = new ModelInfoMiddleware("sound", crates.sound);

  crateRouter
    .add(crates.sound,
      auth.publicContributionMiddleware,
      adminRequest,
      userDataMiddleware,
      visibilityFeatureMapper,
      visibilityFilter,
      soundsMapper,
      modelInfo);
}
