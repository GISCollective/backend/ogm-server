/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.sounds.put;

import ogm.models.sound;
import ogm.sounds.configuration;
import tests.fixtures;

import std.algorithm;
import std.array;
import ogm.sounds.api;

alias suite = Spec!({
  URLRouter router;

  describe("PUT", {
    Json data;

    beforeEach({
      setupTestData();
      auto configuration = SoundsConfiguration("/here", "http://localhost:9091/");

      router = new URLRouter;
      router.crateSetup.setupSoundApi(crates, configuration);

      auto info = ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z"));

      createSound(Sound(ObjectId.fromString("1"), "name1", VisibilityOptional(Team(ObjectId.fromString("1")), true), info, new SoundFile));
      createSound(Sound(ObjectId.fromString("2"), "name2", VisibilityOptional(Team(ObjectId.fromString("2")), false), info, new SoundFile));
      createSound(Sound(ObjectId.fromString("3"), "name3", VisibilityOptional(Team(ObjectId.fromString("3")), true), info, new SoundFile));
      createSound(Sound(ObjectId.fromString("4"), "name4", VisibilityOptional(Team(ObjectId.fromString("4")), false), info, new SoundFile));

      data = Json.emptyObject;
      data["sound"] = crates.sound.get.where("_id").equal(ObjectId.fromString("2")).and.exec.front;

      info.author = userId["member"];
      info.originalAuthor = userId["member"];
      createSound(Sound(ObjectId.fromString("5"), "name5", VisibilityOptional(Team(ObjectId.fromString("2")), false), info, new SoundFile));
    });

    describeCredentialsRule("update team sounds", "yes", "sounds", ["administrator", "owner"], (string type) {
      data["sound"]["name"] = "new name";
      router
        .request
        .put("/sounds/000000000000000000000002")
        .send(data)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"sound": {
            "_id": "000000000000000000000002",
            "visibility": {
              "isDefault": false,
              "isPublic": false,
              "team": "000000000000000000000002"
            },
            "info": {
              "changeIndex": 0,
              "createdOn": "2015-01-01T00:00:00Z",
              "lastChangeOn": "2015-01-01T00:00:00Z",
              "originalAuthor": "",
              "author": ""
            },
            "feature": "",
            "campaignAnswer": "",
            "sourceUrl": "",
            "sound": "http://localhost:9091/sounds/000000000000000000000002/sound",
            "canEdit": true,
            "name": "new name",
            "attributions": ""
          }}`.parseJsonString);
        });
    });

    describeCredentialsRule("update team sounds", "no", "sounds", ["leader", "member", "guest"], (string type) {
      data["sound"]["name"] = "new name";
      router
        .request
        .put("/sounds/000000000000000000000002")
        .send(data)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal("{\"errors\": [{
            \"description\": \"You don't have enough rights to edit `000000000000000000000002`.\",
            \"status\": 403,
            \"title\": \"Forbidden\"
          }]}".parseJsonString);
        });
    });

    describeCredentialsRule("update team sounds", "-", "sounds", "no rights", {
      router
        .request
        .put("/sounds/000000000000000000000002")
        .send(data)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "error": "Authorization required"
          }`.parseJsonString);
        });
    });

    it("should update own sounds", {
      data["sound"]["name"] = "new name";
      router
        .request
        .put("/sounds/000000000000000000000005")
        .send(data)
        .header("Authorization", "Bearer " ~ userTokenList["member"])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto result = response.bodyJson;
          auto expectedResult = `{"sound": {
            "_id": "000000000000000000000005",
            "visibility": {
              "isDefault": false,
              "isPublic": false,
              "team": "000000000000000000000002"
            },
            "info": {
              "changeIndex": 1,
              "createdOn": "2015-01-01T00:00:00Z",
              "lastChangeOn": "2015-01-01T00:00:00Z",
              "originalAuthor": "000000000000000000000002",
              "author": "000000000000000000000002"
            },
            "sound": "http://localhost:9091/sounds/000000000000000000000005/sound",
            "canEdit": true,
            "feature": "",
            "campaignAnswer": "",
            "sourceUrl": "",
            "name": "new name",
            "attributions": ""
          }}`.parseJsonString;
          expectedResult["sound"]["info"]["lastChangeOn"] = result["sound"]["info"]["lastChangeOn"];

          result.should.equal(expectedResult);
      });
    });

    it("should not replace the sound file", {
      data["sound"]["sound"] = "new sound";

      router
        .request
        .put("/sounds/000000000000000000000005")
        .send(data)
        .header("Authorization", "Bearer " ~ userTokenList["member"])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto result = response.bodyJson;
          auto expectedResult = `{"sound": {
            "_id": "000000000000000000000005",
            "visibility": {
              "isDefault": false,
              "isPublic": false,
              "team": "000000000000000000000002"
            },
            "info": {
              "changeIndex": 1,
              "createdOn": "2015-01-01T00:00:00Z",
              "lastChangeOn": "2015-01-01T00:00:00Z",
              "originalAuthor": "000000000000000000000002",
              "author": "000000000000000000000002"
            },
            "sound": "http://localhost:9091/sounds/000000000000000000000005/sound",
            "canEdit": true,
            "sourceUrl": "",
            "feature": "",
            "campaignAnswer": "",
            "name": "name2",
            "attributions": ""
          }}`.parseJsonString;
          expectedResult["sound"]["info"]["lastChangeOn"] = result["sound"]["info"]["lastChangeOn"];

          result.should.equal(expectedResult);
      });
    });
  });
});