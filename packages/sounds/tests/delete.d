/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.sounds.delete_;

import ogm.models.sound;
import ogm.sounds.configuration;
import tests.fixtures;

import std.algorithm;
import std.array;
import ogm.sounds.api;


alias suite = Spec!({
  URLRouter router;

  describe("DELETE", {
    beforeEach({
      setupTestData();
      auto configuration = SoundsConfiguration("/here", "http://localhost:9091/");

      router = new URLRouter;
      router.crateSetup.setupSoundApi(crates, configuration);

      auto info = ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z"));

      createSound(Sound(ObjectId.fromString("1"), "name1", VisibilityOptional(Team(ObjectId.fromString("1")), true), info, new SoundFile));
      createSound(Sound(ObjectId.fromString("2"), "name2", VisibilityOptional(Team(ObjectId.fromString("2")), false), info, new SoundFile));
      createSound(Sound(ObjectId.fromString("3"), "name3", VisibilityOptional(Team(ObjectId.fromString("3")), true), info, new SoundFile));
      createSound(Sound(ObjectId.fromString("4"), "name4", VisibilityOptional(Team(ObjectId.fromString("4")), false), info, new SoundFile));

      info.author = userId["member"];
      info.originalAuthor = userId["member"];
      createSound(Sound(ObjectId.fromString("5"), "name5", VisibilityOptional(Team(ObjectId.fromString("2")), false), info, new SoundFile));
    });

    describeCredentialsRule("delete team sounds", "yes", "sounds", ["administrator", "owner"], (string type) {
      router
        .request
        .delete_("/sounds/000000000000000000000002")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end((Response response) => () {
          crates.sound.get.exec
            .filter!(a => a["_id"] == "000000000000000000000002")
              .array.length.should.equal(0);
        });
    });

    describeCredentialsRule("delete team sounds", "no", "sounds", ["leader", "member", "guest"], (string type) {
      router
        .request
        .delete_("/sounds/000000000000000000000002")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          crates.sound.get.exec
            .filter!(a => a["_id"] == "000000000000000000000002")
              .array.length.should.equal(1);

          response.bodyJson.should.equal("{
              \"errors\": [{
                  \"description\": \"You don't have enough rights to remove `000000000000000000000002`.\",
                  \"title\": \"Forbidden\",
                  \"status\": 403
                }]}".parseJsonString);
        });
    });

    describeCredentialsRule("delete team sounds", "-", "sounds", "no rights", {
      router
        .request
        .delete_("/sounds/000000000000000000000002")
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "error": "Authorization required"
          }`.parseJsonString);
        });
    });

    it("should delete own sounds", {
      router
        .request
        .delete_("/sounds/000000000000000000000005")
        .header("Authorization", "Bearer " ~ userTokenList["member"])
        .expectStatusCode(204)
        .end((Response response) => () {
          crates.sound.get.exec
            .filter!(a => a["_id"] == "000000000000000000000005")
              .array.length.should.equal(0);
        });
    });
  });
});