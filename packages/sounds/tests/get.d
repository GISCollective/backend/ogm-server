/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.sounds.get;

import ogm.models.sound;
import ogm.sounds.configuration;
import tests.fixtures;

import std.algorithm;
import std.array;
import ogm.sounds.api;

alias suite = Spec!({
  URLRouter router;

  describe("GET", {
    beforeEach({
      setupTestData();
      auto configuration = SoundsConfiguration("/here", "http://localhost:9091/");

      router = new URLRouter;
      router.crateSetup.setupSoundApi(crates, configuration);

      auto info = ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z"));

      createSound(Sound(ObjectId.fromString("1"), "name1", VisibilityOptional(Team(ObjectId.fromString("1")), true), info, new SoundFile));
      createSound(Sound(ObjectId.fromString("2"), "name2", VisibilityOptional(Team(ObjectId.fromString("2")), false), info, new SoundFile));
      createSound(Sound(ObjectId.fromString("3"), "name3", VisibilityOptional(Team(ObjectId.fromString("3")), true), info, new SoundFile));
      createSound(Sound(ObjectId.fromString("4"), "name4", VisibilityOptional(Team(ObjectId.fromString("4")), false), info, new SoundFile));
    });

    describeCredentialsRule("listen public sounds", "yes", "sounds", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/sounds")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          Json[] sounds = cast(Json[]) response.bodyJson["sounds"];
          string[] idList = sounds.map!(a => a["_id"].to!string).array;

          idList.should.contain(["000000000000000000000001", "000000000000000000000003"]);

          if(type != "administrator") {
            idList.should.not.contain("000000000000000000000004");
          }
        });
    });

    describeCredentialsRule("listen public sounds", "yes", "sounds", "no rights", {
      router
        .request
        .get("/sounds")
        .expectStatusCode(200)
        .end((Response response) => () {
          Json[] sounds = cast(Json[]) response.bodyJson["sounds"];
          string[] idList = sounds.map!(a => a["_id"].to!string).array;

          idList.should.equal(["000000000000000000000001", "000000000000000000000003"]);
        });
    });

    describeCredentialsRule("listen team sounds", "yes", "sounds", "administrator", {
      router
        .request
        .get("/sounds?edit=true&all=true")
        .header("Authorization", "Bearer " ~ administratorToken.name)
        .expectStatusCode(200)
        .end((Response response) => () {
          auto sounds = cast(Json[]) response.bodyJson["sounds"];

          string[] idList = sounds.map!(a => a["_id"].to!string).array;
          idList.should.equal(["000000000000000000000001","000000000000000000000002","000000000000000000000003","000000000000000000000004"]);
        });
    });

    describeCredentialsRule("listen team sounds", "yes", "sounds", ["owner", "leader", "member", "guest"], {
      router
        .request
        .get("/sounds?edit=true")
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .expectStatusCode(200)
        .end((Response response) => () {
          Json[] sounds = cast(Json[]) response.bodyJson["sounds"];
          string[] idList = sounds.map!(a => a["_id"].to!string).array;

          idList.should.equal(["000000000000000000000001", "000000000000000000000002"]);
        });
    });

    describeCredentialsRule("listen team sounds", "-", "sounds", "no rights", { });

    it("should update the sound file url", {
      router
        .request
        .get("/sounds/000000000000000000000001")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "sound": {
              "_id": "000000000000000000000001",
              "visibility": {
                "isDefault": false,
                "isPublic": true,
                "team": "000000000000000000000001"
              },
              "info": {
                "changeIndex": 0,
                "createdOn": "2015-01-01T00:00:00Z",
                "lastChangeOn": "2015-01-01T00:00:00Z",
                "originalAuthor": "",
                "author": ""
              },
              "sound": "http://localhost:9091/sounds/000000000000000000000001/sound",
              "name": "name1",
              "sourceUrl": "",
              "feature": "",
              "campaignAnswer": "",
              "attributions": ""
            }
          }`.parseJsonString);
        });
    });

    it("can get a sound using a cookie", {
      router
        .request
        .get("/sounds/000000000000000000000002")
        .header("Cookie", `ember_simple_auth-session=%7B%22authenticated%22%3A%7B%7D%7D; ember_simple_auth-session=%7B%22authenticated%22%3A%7B%22authenticator%22%3A%22authenticator%3Aoauth2%22%2C%22access_token%22%3A%22` ~ userTokenList["administrator"] ~ `%22%2C%22token_type%22%3A%22Bearer%22%2C%22expires_in%22%3A3600%2C%22expires_at%22%3A1736278903027%2C%22refresh_token%22%3A%22809a853d-ffa2-ab37-f7b2-6a08948a5b43%22%7D%7D; ember_simple_auth-session-expiration_time=1209600`)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "sound": {
              "_id": "000000000000000000000002",
              "visibility": {
                "isDefault": false,
                "isPublic": false,
                "team": "000000000000000000000002"
              },
              "info": {
                "changeIndex": 0,
                "createdOn": "2015-01-01T00:00:00Z",
                "lastChangeOn": "2015-01-01T00:00:00Z",
                "originalAuthor": "",
                "author": ""
              },
              "sound": "http://localhost:9091/sounds/000000000000000000000002/sound",
              "name": "name2",
              "sourceUrl": "",
              "feature": "",
              "campaignAnswer": "",
              "attributions": "",
              "canEdit": true
            }
          }`.parseJsonString);
        });
    });
  });
});

