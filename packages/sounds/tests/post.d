/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.sounds.post;

import ogm.models.sound;
import ogm.sounds.configuration;
import tests.fixtures;

import std.algorithm;
import std.array;
import ogm.sounds.api;

alias suite = Spec!({
  URLRouter router;

  describe("POST", {
    Json data;

    beforeEach({
      setupTestData();
      auto configuration = SoundsConfiguration("/here", "http://localhost:9091/");

      router = new URLRouter;
      router.crateSetup.setupSoundApi(crates, configuration);

      auto info = ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z"));

      data = Json.emptyObject;
      data["sound"] = `{
        "visibility": {
          "isDefault": false,
          "isPublic": false,
          "team": "000000000000000000000001"
        },
        "sound": "data:audio/mpeg;base64,Y2hhbmdlMg==",
        "canEdit": true,
        "name": "new name"
      }`.parseJsonString;
    });

    describeCredentialsRule("add mp3 sounds to a public team", "yes", "sounds", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .post("/sounds")
        .send(data)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto expected = `{"sound": {
            "_id": "000000000000000000000001",
            "visibility": {
              "isDefault": false,
              "isPublic": false,
              "team": "000000000000000000000001"
            },
            "info": {
              "changeIndex": 0,
              "createdOn": "2020-12-13T14:54:02Z",
              "lastChangeOn": "2020-12-13T14:54:02Z",
              "originalAuthor": "000000000000000000000004",
              "author": "000000000000000000000004"
            },
            "sound": "http://localhost:9091/sounds/000000000000000000000001/sound",
            "canEdit": true,
            "feature": "",
            "campaignAnswer": "",
            "sourceUrl": "",
            "name": "new name",
            "attributions": ""
          }}`.parseJsonString;

          auto result = response.bodyJson;
          expected["sound"]["info"]["originalAuthor"] = userId[type];
          expected["sound"]["info"]["author"] = userId[type];
          expected["sound"]["info"]["createdOn"] = result["sound"]["info"]["createdOn"];
          expected["sound"]["info"]["lastChangeOn"] = result["sound"]["info"]["lastChangeOn"];

          result.should.equal(expected);
        });
    });

    describeCredentialsRule("add mp3 sounds to a public team", "yes", "sounds", "no rights", {
      router
        .request
        .post("/sounds")
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          auto expected = `{"sound": {
            "_id": "000000000000000000000001",
            "visibility": {
              "isDefault": false,
              "isPublic": false,
              "team": "000000000000000000000001"
            },
            "info": {
              "changeIndex": 0,
              "createdOn": "2020-12-13T14:54:02Z",
              "lastChangeOn": "2020-12-13T14:54:02Z",
              "originalAuthor": "@anonymous",
              "author": "@anonymous"
            },
            "sound": "http://localhost:9091/sounds/000000000000000000000001/sound",
            "sourceUrl": "",
            "feature": "",
            "campaignAnswer": "",
            "name": "new name",
            "attributions": ""
          }}`.parseJsonString;

          auto result = response.bodyJson;
          expected["sound"]["info"]["createdOn"] = result["sound"]["info"]["createdOn"];
          expected["sound"]["info"]["lastChangeOn"] = result["sound"]["info"]["lastChangeOn"];

          result.should.equal(expected);
        });
    });


    describeCredentialsRule("add mp3 sounds to owned private teams", "yes", "sounds", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      data["sound"]["visibility"]["team"] = "000000000000000000000002";

      router
        .request
        .post("/sounds")
        .send(data)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto expected = `{"sound": {
            "_id": "000000000000000000000001",
            "visibility": {
              "isDefault": false,
              "isPublic": false,
              "team": "000000000000000000000002"
            },
            "info": {
              "changeIndex": 0,
              "createdOn": "2020-12-13T14:54:02Z",
              "lastChangeOn": "2020-12-13T14:54:02Z",
              "originalAuthor": "000000000000000000000004",
              "author": "000000000000000000000004"
            },
            "sound": "http://localhost:9091/sounds/000000000000000000000001/sound",
            "canEdit": true,
            "feature": "",
            "campaignAnswer": "",
            "sourceUrl": "",
            "name": "new name",
            "attributions": ""
          }}`.parseJsonString;

          auto result = response.bodyJson;
          expected["sound"]["info"]["originalAuthor"] = userId[type];
          expected["sound"]["info"]["author"] = userId[type];
          expected["sound"]["info"]["createdOn"] = result["sound"]["info"]["createdOn"];
          expected["sound"]["info"]["lastChangeOn"] = result["sound"]["info"]["lastChangeOn"];

          result.should.equal(expected);
        });
    });

    describeCredentialsRule("add mp3 sounds to owned private teams", "-", "sounds", "no rights", {
      data["sound"]["visibility"]["team"] = "000000000000000000000002";

      router
        .request
        .post("/sounds")
        .send(data)
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal("{
            \"errors\": [{
              \"description\": \"There is no team with id `000000000000000000000002`.\",
              \"status\": 403,
              \"title\": \"Forbidden\"
          }]}".parseJsonString);
        });
    });


    describeCredentialsRule("add mp3 sounds to other private teams", "yes", "sounds", "administrator", (string type) {
      data["sound"]["visibility"]["team"] = "000000000000000000000004";

      router
        .request
        .post("/sounds")
        .send(data)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto expected = `{"sound": {
            "_id": "000000000000000000000001",
            "visibility": {
              "isDefault": false,
              "isPublic": false,
              "team": "000000000000000000000004"
            },
            "info": {
              "changeIndex": 0,
              "createdOn": "2020-12-13T14:54:02Z",
              "lastChangeOn": "2020-12-13T14:54:02Z",
              "originalAuthor": "000000000000000000000004",
              "author": "000000000000000000000004"
            },
            "feature": "",
            "campaignAnswer": "",
            "sourceUrl": "",
            "sound": "http://localhost:9091/sounds/000000000000000000000001/sound",
            "canEdit": true,
            "name": "new name",
            "attributions": ""
          }}`.parseJsonString;

          auto result = response.bodyJson;
          expected["sound"]["info"]["originalAuthor"] = userId[type];
          expected["sound"]["info"]["author"] = userId[type];
          expected["sound"]["info"]["createdOn"] = result["sound"]["info"]["createdOn"];
          expected["sound"]["info"]["lastChangeOn"] = result["sound"]["info"]["lastChangeOn"];

          result.should.equal(expected);
        });
    });

    describeCredentialsRule("add mp3 sounds to other private teams", "yes", "sounds", ["owner", "leader", "member", "guest"], (string type) {
      data["sound"]["visibility"]["team"] = "000000000000000000000004";

      router
        .request
        .post("/sounds")
        .send(data)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal("{
            \"errors\": [{
              \"description\": \"There is no team with id `000000000000000000000004`.\",
              \"status\": 403,
              \"title\": \"Forbidden\"
          }]}".parseJsonString);
        });
    });

    describeCredentialsRule("add mp3 sounds to other private teams", "-", "sounds", "no rights", {
      data["sound"]["visibility"]["team"] = "000000000000000000000004";

      router
        .request
        .post("/sounds")
        .send(data)
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal("{
            \"errors\": [{
              \"description\": \"There is no team with id `000000000000000000000004`.\",
              \"status\": 403,
              \"title\": \"Forbidden\"
          }]}".parseJsonString);
        });
    });

    it("accepts sounds without visibility and with a feature id", {
      data["sound"].remove("visibility");
      data["sound"]["feature"] = "000000000000000000000001";

      router
        .request
        .post("/sounds")
        .send(data)
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto expected = `{"sound": {
            "_id": "000000000000000000000001",
            "visibility": {
              "isDefault": false,
              "isPublic": true,
              "team": "000000000000000000000001"
            },
            "info": {
              "changeIndex": 0,
              "createdOn": "2020-12-13T14:54:02Z",
              "lastChangeOn": "2020-12-13T14:54:02Z",
              "originalAuthor": "000000000000000000000004",
              "author": "000000000000000000000004"
            },
            "sound": "http://localhost:9091/sounds/000000000000000000000001/sound",
            "canEdit": true,
            "sourceUrl": "",
            "feature": "000000000000000000000001",
            "campaignAnswer": "",
            "name": "new name",
            "attributions": ""
          }}`.parseJsonString;

          auto result = response.bodyJson;
          expected["sound"]["info"]["originalAuthor"] = userId["administrator"];
          expected["sound"]["info"]["author"] = userId["administrator"];
          expected["sound"]["info"]["createdOn"] = result["sound"]["info"]["createdOn"];
          expected["sound"]["info"]["lastChangeOn"] = result["sound"]["info"]["lastChangeOn"];

          result.should.equal(expected);
        });
    });

    it("accepts sounds without visibility and with a campaign answer id", {
      createCampaignAnswer(CampaignAnswer(ObjectId.fromString("000000000000000000000001")));

      auto campaign = Campaign(ObjectId.fromString("000000000000000000000001"));
      campaign.visibility.team = Team(ObjectId.fromString("000000000000000000000001"));

      auto answer = CampaignAnswer(ObjectId.fromString("000000000000000000000002"));
      answer.campaign = campaign;

      createCampaign(campaign);
      createCampaignAnswer(answer);

      data["sound"].remove("visibility");
      data["sound"]["campaignAnswer"] = "000000000000000000000002";

      router
        .request
        .post("/sounds")
        .send(data)
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto expected = `{"sound": {
            "_id": "000000000000000000000001",
            "visibility": {
              "isDefault": false,
              "isPublic": false,
              "team": "000000000000000000000001"
            },
            "info": {
              "changeIndex": 0,
              "createdOn": "2020-12-13T14:54:02Z",
              "lastChangeOn": "2020-12-13T14:54:02Z",
              "originalAuthor": "000000000000000000000004",
              "author": "000000000000000000000004"
            },
            "sound": "http://localhost:9091/sounds/000000000000000000000001/sound",
            "canEdit": true,
            "sourceUrl": "",
            "feature": "",
            "campaignAnswer": "000000000000000000000002",
            "name": "new name",
            "attributions": ""
          }}`.parseJsonString;

          auto result = response.bodyJson;
          expected["sound"]["info"]["originalAuthor"] = userId["administrator"];
          expected["sound"]["info"]["author"] = userId["administrator"];
          expected["sound"]["info"]["createdOn"] = result["sound"]["info"]["createdOn"];
          expected["sound"]["info"]["lastChangeOn"] = result["sound"]["info"]["lastChangeOn"];

          result.should.equal(expected);
        });
    });
  });
});