/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.fixtures;

public import fluent.asserts;
import fluentasserts.vibe.json;
public import trial.discovery.spec;

public import crate.auth.usercollection;

import std.traits;

public import vibe.service.configuration.general;
public import ogm.auth;
public import ogm.crates.all;
public import ogm.test.fixtures;
public import ogm.crates.defaults;

public import crate.http.router;
public import gis_collective.hmq.broadcast.memory;

import trial.interfaces;

import std.file;
import std.string;
import std.algorithm;
import std.array;

import vibe.core.log;

shared static this() {
  foreach(logger; getLoggers()) {
    deregisterLogger(logger);
  }

  setLogLevel(LogLevel.trace);
}

private static Json credentialsRule = Json.emptyObject;

static this() {
  LifeCycleListeners.instance.add(new CredentialsRuleWriter);

  if(!"../../config/about/accessRights.json".exists) {
    return;
  }

  auto tmp = "../../config/about/accessRights.json".readText.parseJsonString;

  foreach(string model; tmp.keys) {
    if(model !in credentialsRule) {
      credentialsRule[model] = tmp[model];
    }
  }
}


class CredentialsRuleWriter : ILifecycleListener {
  enum actionSize = 62;
  enum valueSize = 17;

  void begin(ulong testCount) {}

  void end (SuiteResult[]) {
    if(credentialsRule.type != Json.Type.object) {
      return;
    }

    std.file.write("../../config/about/accessRights.json", credentialsRule.toPrettyString);

    string mdResult =
      "---\n" ~
      "title: Access rights\n" ~
      "layout: default\n" ~
      "nav_order: 1\n" ~
      "parent: Resources\n" ~
      "---\n\n" ~
      "# Access rights\n" ~
      "{: .no_toc }\n\n" ~
      `<details open markdown="block">` ~ "\n" ~
      "  <summary>\n" ~
      "    Table of contents\n" ~
      "  </summary>\n" ~
      "  {: .text-delta }\n" ~
      "1. TOC\n" ~
      "{:toc}\n" ~
      "</details>";


    foreach(string model; credentialsRule.keys.sort) {
      auto actionList = credentialsRule[model];

      mdResult ~= "\n\n\n\n## " ~ model.capitalize ~ "\n\n";

      /// Get all levels for the model
      size_t[string] levels = [ "no rights": 0, "guest": 1, "member": 2, "leader": 3, "owner": 4 ];
      string[][string] rows;
      size_t i = levels.length;

      foreach(string action, valueByLevel; actionList) {
        foreach(string level, _; valueByLevel) {
          if(level !in levels) {
            levels[level] = i;
            i++;
          }
        }
      }

      string[] levelsByIndex;
      levelsByIndex.length = levels.length;

      foreach(string level, index; levels) {
        levelsByIndex[index] = level;
      }

      /// Create a table rows
      foreach(string action, valueByLevel; actionList) {
        rows[action] = [];
        foreach(_; levels) {
          rows[action] ~= "*unknown*";
        }

        foreach(string level, value; valueByLevel) {
          auto levelIndex = levels[level];
          rows[action][levelIndex] = value.to!string;
        }
      }

      /// Create the md table
      mdResult ~= "| " ~ leftJustify("action", actionSize - 2) ~ " |";

      foreach(string level; levelsByIndex) {
        mdResult ~= " " ~  leftJustify(level, valueSize) ~ " |";
      }

      mdResult ~= "\n " ~ leftJustify("", actionSize - 1, '-') ~ " |";

      foreach(string level; levelsByIndex) {
        mdResult ~= " " ~ leftJustify("", valueSize, '-') ~ " |";
      }

      auto actions = rows.keys.sort;

      foreach(string action; actions) {
        mdResult ~= "\n";
        mdResult ~= leftJustify(action, actionSize) ~ " | ";

        foreach(value; rows[action]) {
          mdResult ~= leftJustify(value, valueSize) ~ " | ";
        }
      }
    }

    mdResult ~= "\n";

    std.file.write("../../config/about/accessRights.md", mdResult);
  }

  void update () {}
}


void describeCredentialsRule(string action, string value, string model, string level) {
  if(credentialsRule.type != Json.Type.object) {
    credentialsRule = Json.emptyObject;
  }

  if(credentialsRule[model].type != Json.Type.object) {
    credentialsRule[model] = Json.emptyObject;
  }

  if(credentialsRule[model][action].type != Json.Type.object) {
    credentialsRule[model][action] = Json.emptyObject;
  }

  if(credentialsRule[model][action][level].type != Json.Type.object) {
    credentialsRule[model][action][level] = Json.emptyObject;
  }

  credentialsRule[model][action][level] = value;
}

void describeCredentialsRule(string action, string value, string model, string level, void delegate() test) {
  describeCredentialsRule(action, value, model, level);

  try {
    string message = "should " ~ action ~ " for a " ~ level ~ " user";

    if(value == "no") {
      message = "should not " ~ action ~ " for a " ~ level ~ " user";
    }

    it(message, test);
  } catch(Throwable t) {
    credentialsRule[model][action][level] = value ~ " **(failed test)**";
    throw t;
  }
}

void describeCredentialsRule(string action, string value, string model, string level, void delegate(string type) test) {
  describeCredentialsRule(action, value, model, level);

  try {
    string message = "should " ~ action ~ " for a " ~ level ~ " user";

    if(value == "no") {
      message = "should not " ~ action ~ " for a " ~ level ~ " user";
    }

    it(message, { test(level); });
  } catch(Throwable t) {
    credentialsRule[model][action][level] = value ~ " **(failed test)**";
    throw t;
  }
}

void describeCredentialsRule(T)(string action, string value, string model, T test) {
  describeCredentialsRule(action, value, model, userTypes, test);
}

void describeCredentialsRule(T)(string action, string value, string model, string[] types, T test) {
  foreach (type; types) {
    describeCredentialsRule(action, value, model, type, test);
  }
}