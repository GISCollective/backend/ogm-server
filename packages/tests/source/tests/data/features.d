/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.data.features;

import tests.fixtures;
import vibe.http.common;

Json site1, site2, site3, site4;
Json site1Readable, site2Readable, site3Readable, site4Readable;
Json site1Editable, site2Editable, site3Editable, site4Editable;

static this() {
  site1 = `{"_id":"000000000000000000000001",
    "contributors": [], "visibility": 1, "name": "site1","attributes": {"name1": {}},
    "info": { "originalAuthor": "000000000000000000000001", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 },
    "position":{"coordinates":[1.5,1.5],"type":"Point"},
    "isLarge": false,
    "icons":["000000000000000000000001"],"description":{
      "blocks": [{
        "type": "paragraph",
        "data": {
          "text": "description of site1"
        }
      }]
    },
    "source": {
      "type": "",
      "remoteId": "",
      "modelId": "",
      "syncAt": "0001-01-01T00:00:00+00:00"
    },
    "decorators": {
      "center": [],
      "showAsLineAfterZoom": 99,
      "showLineMarkers": false,
      "useDefault": true,
      "keepWhenSmall": true,
      "changeIndex": 0,
      "minZoom": 0,
      "maxZoom": 20
    },
    "computedVisibility": {
      "isDefault": false,
      "isPublic": true,
      "team": "000000000000000000000001"
    },
    "attributes": { "name1": { "program": "Luni-Vineri:9-18", "phone": "123456", "type of food": "international",
    "price": "11.4", "kids-friendly": "true", "max number of people": "23"}},
    "pictures":[],"sounds": [],"maps":["000000000000000000000001"]}`
    .parseJsonString;


  site2 = `{"_id":"000000000000000000000002",
    "contributors": [], "visibility": 0, "name": "site2","attributes": {},
    "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 },
    "position":{"coordinates":[1.6,1.6],"type":"Point"},
    "isLarge": false,
    "icons":["000000000000000000000001", "000000000000000000000002"],
    "source": {
      "type": "",
      "remoteId": "",
      "modelId": "",
      "syncAt": "0001-01-01T00:00:00+00:00"
    },
    "decorators": {
      "center": [],
      "showAsLineAfterZoom": 99,
      "showLineMarkers": false,
      "useDefault": true,
      "keepWhenSmall": true,
      "changeIndex": 0,
      "minZoom": 0,
      "maxZoom": 20
    },
    "computedVisibility": {
      "isDefault": false,
      "isPublic": false,
      "team": "000000000000000000000002"
    },
    "description":{
      "blocks": [{
        "type": "paragraph",
        "data": {
          "text": "description of site2"
        }
      }]
    },
    "pictures":[],"sounds": [],"maps":["000000000000000000000002"]}`
    .parseJsonString;

  site3 = `{"_id":"000000000000000000000003",
    "contributors": [], "visibility": 1, "name": "site3","attributes": {"name2": {},"name1": {}},
    "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 },
    "position":{"coordinates":[1.7,1.7],"type":"Point"},
    "isLarge": false,
    "icons":["000000000000000000000001", "000000000000000000000002"],"description":{
      "blocks": [{
        "type": "paragraph",
        "data": {
          "text": "description of site3"
        }
      }]
    },
    "computedVisibility": {
      "isDefault": false,
      "isPublic": true,
      "team": "000000000000000000000003"
    },
    "source": {
      "type": "",
      "remoteId": "",
      "modelId": "",
      "syncAt": "0001-01-01T00:00:00+00:00"
    },
    "decorators": {
      "center": [],
      "showAsLineAfterZoom": 99,
      "showLineMarkers": false,
      "useDefault": true,
      "keepWhenSmall": true,
      "changeIndex": 0,
      "minZoom": 0,
      "maxZoom": 20
    },
    "pictures":[],"sounds": [],"maps":["000000000000000000000003"]}`
    .parseJsonString;

  site4 = `{"_id":"000000000000000000000004",
    "contributors": [], "visibility": 1, "name": "site4","attributes": {"name2": {},"name1": {}},
    "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 },
    "position":{"coordinates":[1.8,1.8],"type":"Point"},
    "isLarge": false,
    "icons":["000000000000000000000001", "000000000000000000000002"],"description":{
      "blocks": [{
        "type": "paragraph",
        "data": {
          "text": "description of site4"
        }
      }]
    },
    "computedVisibility": {
      "isDefault": false,
      "isPublic": true,
      "team": "000000000000000000000004"
    },
    "source": {
      "type": "",
      "remoteId": "",
      "modelId": "",
      "syncAt": "0001-01-01T00:00:00+00:00"
    },
    "decorators": {
      "center": [],
      "showAsLineAfterZoom": 99,
      "showLineMarkers": false,
      "useDefault": true,
      "keepWhenSmall": true,
      "changeIndex": 0,
      "minZoom": 0,
      "maxZoom": 20
    },
    "pictures":[],"sounds": [],"maps":["000000000000000000000004"]}`
    .parseJsonString;

  site1Editable = site1.clone;
  site1Editable["canEdit"] = true;

  site2Editable = site2.clone;
  site2Editable["canEdit"] = true;

  site3Editable = site3.clone;
  site3Editable["canEdit"] = true;

  site4Editable = site4.clone;
  site4Editable["canEdit"] = true;

  site1Readable = site1.clone;
  site1Readable["canEdit"] = false;

  site2Readable = site2.clone;
  site2Readable["attributes"] = `{ "name1": {}, "name2": {} }`.parseJsonString;
  site2Readable["canEdit"] = false;

  site3Readable = site3.clone;
  site3Readable["canEdit"] = false;

  site4Readable = site4.clone;
  site4Readable["canEdit"] = false;
}
