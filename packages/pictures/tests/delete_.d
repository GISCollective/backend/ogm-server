/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.pictures.delete_;

import ogm.pictures.configuration;
import tests.fixtures;
import vibe.http.common;
import ogm.pictures.api;

alias suite = Spec!({
  URLRouter router;

  describe("deleting a picture", {
    Json picture;

    beforeEach({
      setupTestData();
      router = new URLRouter;
      auto config = PicturesConfiguration();
      config.baseUrl = "http://localhost:9091/";
      router.crateSetup.setupPictureApi(crates, config);
      setupDefaultPictures(crates);

      picture = `{
        "name": "test",
        "owner": "@system",
        "picture": "http://localhost:9091/pictures/000000000000000000000003/picture"
      }`.parseJsonString;

      crates.picture.addItem(picture);

      picture = `{
        "name": "test",
        "picture": "http://localhost:9091/pictures/000000000000000000000004/picture"
      }`.parseJsonString;

      crates.picture.addItem(picture);

      picture = `{
        "name": "test",
        "owner": "000000000000000000000001",
        "picture": "http://localhost:9091/pictures/000000000000000000000005/picture"
      }`.parseJsonString;

      crates.picture.addItem(picture);
    });

    describe("with an admin token", {
      it("should not delete a @system picture", {
        router
          .request
          .delete_("/pictures/000000000000000000000003")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(400)
          .end((Response response) => () {
            crates.picture.get.size.should.equal(5);

            response.bodyJson.should.equal(`{
              "errors": [{
                  "description": "You can't delete system pictures.",
                  "status": 400,
                  "title": "Validation error"
                }
              ]}`.parseJsonString);
          });
      });

      it("should not delete a picture without owner", {
        router
          .request
          .delete_("/pictures/000000000000000000000004")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(204)
          .end((Response response) => () {
            crates.picture.get.size.should.equal(4);
          });
      });

      it("should delete a picture owned by other users", {
        router
          .request
          .delete_("/pictures/000000000000000000000005")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(204)
          .end((Response response) => () {
            crates.picture.get.size.should.equal(4);
          });
      });
    });

    describe("with an normal user", {
      it("should not delete a picture without owner", {
        router
          .request
          .delete_("/pictures/000000000000000000000004")
          .header("Authorization", "Bearer " ~ bearerLeaderToken.name)
          .expectStatusCode(400)
          .end((Response response) => () {
            crates.picture.get.size.should.equal(5);

            response.bodyJson.should.equal(`{
              "errors": [{
                  "description": "The picture owner is not set.",
                  "status": 400,
                  "title": "Validation error"
                }
              ]}`.parseJsonString);
          });
      });

      it("should not delete a @system picture", {
        router
          .request
          .delete_("/pictures/000000000000000000000003")
          .header("Authorization", "Bearer " ~ bearerLeaderToken.name)
          .expectStatusCode(400)
          .end((Response response) => () {
            crates.picture.get.size.should.equal(5);

            response.bodyJson.should.equal(`{
              "errors": [{
                  "description": "You can't delete system pictures.",
                  "status": 400,
                  "title": "Validation error"
                }
              ]}`.parseJsonString);
          });
      });

      it("should delete own pictures", {
        router
          .request
          .delete_("/pictures/000000000000000000000005")
          .header("Authorization", "Bearer " ~ bearerLeaderToken.name)
          .expectStatusCode(204)
          .end((Response response) => () {
            crates.picture.get.size.should.equal(4);
          });
      });

      it("should not delete other users pictures", {
        router
          .request
          .delete_("/pictures/000000000000000000000005")
          .header("Authorization", "Bearer " ~ bearerGuestToken.name)
          .expectStatusCode(400)
          .end((Response response) => () {
            crates.picture.get.size.should.equal(5);

            response.bodyJson.should.equal(`{
              "errors": [{
                  "description": "You can't delete this picture.",
                  "status": 400,
                  "title": "Validation error"
                }
              ]}`.parseJsonString);
          });
      });
    });
  });
});
