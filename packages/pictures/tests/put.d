/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.pictures.put;

import ogm.pictures.configuration;
import tests.fixtures;
import vibe.http.common;
import ogm.pictures.api;

alias suite = Spec!({
  URLRouter router;

  describe("replacing a picture", {
    Json picture;

    beforeEach({
      setupTestData();
      router = new URLRouter;
      auto config = PicturesConfiguration();
      config.baseUrl = "http://localhost:9091/";
      router.crateSetup.setupPictureApi(crates, config);
      setupDefaultPictures(crates);

      picture = `{ "picture": {
        "name": "test",
        "owner": "",
        "picture": "http://localhost:9091/pictures/000000000000000000000001/picture",
        "meta": {}
      }}`.parseJsonString;

      crates.picture.addItem(picture["picture"]);
    });

    describe("with an admin token", {
      it("should update a picture name", {
        router
          .request
          .put("/pictures/000000000000000000000001")
          .send(picture)
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "picture": {
                "_id": "000000000000000000000001",
                "owner": "000000000000000000000005",
                "picture": "http://localhost:9091/pictures/000000000000000000000001/picture",
                "name": "test",
                "meta": {
                  "attributions": "",
                  "format": "",
                  "height": 0,
                  "mime": "",
                  "properties": {},
                  "renderMode": "",
                  "width": 0,
                  "disableOptimization": false,
                  "disableOptimizationReason": ""
                }
              }}`.parseJsonString);
          });
      });

      it("should not update the picture width", {
        auto pictureData = picture.clone;
        pictureData["picture"]["meta"]["width"] = 1;

        router
          .request
          .put("/pictures/000000000000000000000001")
          .send(pictureData)
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "picture": {
                "_id": "000000000000000000000001",
                "owner": "000000000000000000000005",
                "picture": "http://localhost:9091/pictures/000000000000000000000001/picture",
                "name": "test",
                "meta": {
                  "attributions": "",
                  "format": "",
                  "height": 0,
                  "mime": "",
                  "properties": {},
                  "renderMode": "",
                  "width": 0,
                  "disableOptimization": false,
                  "disableOptimizationReason": ""
                }
              }}`.parseJsonString);
          });
      });
    });

    describe("when there is an annonymous picture added to a feature", {
      Json storedPicture;

      beforeEach({
        picture = `{ "picture": {
          "name": "test",
          "owner": "@anonymous",
          "picture": "http://localhost:9091/pictures/000000000000000000000001/picture"
        }}`.parseJsonString;

        storedPicture = crates.picture.addItem(picture["picture"]);

        auto feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
        feature["pictures"] = [storedPicture["_id"]].serializeToJson;
        feature["info"]["originalAuthor"] = "@some";
        crates.feature.updateItem(feature);
      });

      it("keeps the owner when a leader updates the value", {
        router
          .request
          .put("/pictures/000000000000000000000004")
          .send(picture)
          .header("Authorization", "Bearer " ~ bearerLeaderToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["picture"]["owner"].to!string.should.equal(`@anonymous`);
          });
      });

      it("keeps the owner when an owner updates the value", {
        router
          .request
          .put("/pictures/000000000000000000000004")
          .send(picture)
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["picture"]["owner"].to!string.should.equal(`@anonymous`);
          });
      });

      it("keeps the owner when a member updates the value", {
        router
          .request
          .put("/pictures/000000000000000000000004")
          .send(picture)
          .header("Authorization", "Bearer " ~ bearerMemberToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["picture"]["owner"].to!string.should.equal(`@anonymous`);
          });
      });

      it("can be updated by the member when it is the owner", {
        auto feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
        feature["info"]["originalAuthor"] = userId["member"];
        crates.feature.updateItem(feature);

        router
          .request
          .put("/pictures/000000000000000000000004")
          .send(picture)
          .header("Authorization", "Bearer " ~ bearerMemberToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "picture": {
                "_id": "000000000000000000000004",
                "owner": "@anonymous",
                "picture": "http://localhost:9091/pictures/000000000000000000000004/picture",
                "name": "test",
                "meta": {
                  "attributions": "",
                  "format": "",
                  "height": 0,
                  "mime": "",
                  "properties": {},
                  "renderMode": "",
                  "width": 0,
                  "disableOptimization": false,
                  "disableOptimizationReason": ""
                }
              }}`.parseJsonString);
          });
      });
    });
  });
});
