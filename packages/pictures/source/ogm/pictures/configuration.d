/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.pictures.configuration;

import vibe.service.configuration.general;

///
struct MicroPicturesConfig {
  /// Configurations for all services
  GeneralConfig general;

  ///
  PicturesConfiguration pictures;
}

///
struct PicturesConfiguration {
  string location;
  string baseUrl;

  string defaultPicture;
  string defaultLogo;
  string defaultCover;
}
