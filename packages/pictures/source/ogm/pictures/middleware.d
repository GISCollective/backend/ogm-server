/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.pictures.middleware;

import ogm.http.request;
import ogm.models.picture;
import ogm.crates.all;

import vibe.http.server;
import vibe.data.json;
import std.conv;
import std.path;
import std.string;
import std.exception;
import std.algorithm;

import crate.error;
import crate.http.request;
import crate.base;
import crate.auth.usercollection;
import crate.url;

class PictureMiddleware {
  private {
    UserCrateCollection users;
    Crate!Picture picture;
    OgmCrates crates;
  }

  this(UserCrateCollection users, OgmCrates crates) {
    this.users = users;
    this.picture = crates.picture;
    this.crates = crates;
  }

  /// Middleware applied for create routes
  @create
  void create(HTTPServerRequest req) {
    auto request = RequestUserData(req);

    if(req.json.type != Json.Type.object) {
      return;
    }

    req.json["picture"]["owner"] = request.userId;
  }

  /// Middleware applied for replace routes
  @replace @patch
  void replace(HTTPServerRequest req) {
    create(req);

    auto request = RequestUserData(req);
    auto session = request.session(crates);

    auto item = picture.getItem(req.params["id"]).exec;
    enforce!CrateNotFoundException(!item.empty, "Can not find the picture");

    auto storedPicture = item.front;

    if(storedPicture["meta"].type != Json.Type.object) {
      storedPicture["meta"] = Json.emptyObject;
    }

    if(req.json["picture"]["meta"].type != Json.Type.object) {
      req.json["picture"]["meta"] = storedPicture["meta"];
    }

    req.json["picture"]["hash"] = storedPicture["hash"];
    req.json["picture"]["picture"] = storedPicture["picture"];

    foreach(field; ["width", "height", "mime", "format", "properties"]) {
      req.json["picture"]["meta"][field] = storedPicture["meta"]["field"];
    }

    if(request.isAdmin) {
      return;
    }

    req.json["picture"]["owner"] = storedPicture["owner"];

    auto featureRange = crates.feature.get.where("pictures").arrayContains(ObjectId.fromString(req.params["id"])).and.exec;

    if(!featureRange.empty) {
      auto feature = featureRange.front;

      if(feature["info"]["originalAuthor"] == request.userId) {
        return;
      }

      auto maps = feature["maps"].deserializeJson!(string[]);
      auto writingMaps = session.owner.maps ~ session.leader.maps;

      foreach (map; maps) {
        if(writingMaps.canFind(map)) {
          return;
        }
      }
    }

    enforce!CrateValidationException(request.userId != "@anonymous", "You can't change the picture.");
  }

  @delete_
  void delete_(HTTPServerRequest req, HTTPServerResponse res) {
    auto request = RequestUserData(req);

    auto pic = picture.getItem(req.params["id"]).exec.front;

    if(!request.isAdmin) {
      enforce!CrateValidationException(pic["owner"].type == Json.Type.string, "The picture owner is not set.");
      enforce!CrateValidationException(pic["owner"] != "@system", "You can't delete system pictures.");
      enforce!CrateValidationException(pic["owner"] == request.userId, "You can't delete this picture.");

      return;
    }

    if(pic["owner"] == "@system") {
      throw new CrateValidationException("You can't delete system pictures.");
    }
  }

  @mapper
  Json mapper(HTTPServerRequest req, const Json item) @safe {
    Json result = item;

    auto baseUrl = PictureFileSettings.baseUrl.replaceHost(req.host, req.tls);

    result["picture"] = baseUrl.buildPath("pictures", result["_id"].to!string, "picture");

    return result;
  }
}
