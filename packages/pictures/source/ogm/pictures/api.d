/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.pictures.api;

import ogm.auth;

import crate.http.cors;
import crate.http.router;
import crate.auth.middleware;
import crate.resource.file;
import crate.resource.migration;

import vibe.http.router;
import vibe.core.log;
import vibe.core.file;
import vibe.data.json;
import vibe.stream.wrapper;

import std.conv;
import std.array;
import std.file;
import std.path;

import ogm.middleware.adminrequest;
import ogm.crates.all;
import ogm.crates.defaults;
import ogm.pictures.middleware;
import ogm.pictures.configuration;
import ogm.models.picture;
import ogm.middleware.userdata;
import ogm.filter.pagination;

import ogm.auth;

///
void setupPictureApi(T)(CrateRouter!T crateRouter, OgmCrates crates, PicturesConfiguration configuration) {
  PictureFileSettings.path = configuration.location;
  PictureFileSettings.baseUrl = configuration.baseUrl;
  PictureFileSettings.defaultPicture = configuration.defaultPicture;
  PictureFileSettings.defaultCover = configuration.defaultCover;
  PictureFileSettings.defaultLogo = configuration.defaultLogo;
  PictureFileSettings.files = crates.pictureFiles;
  PictureFileSettings.chunks = crates.pictureChunks;

  auto auth = Authentication.instance(crates.user);
  auto pictureMiddleware = new PictureMiddleware(auth.getCollection, crates);
  auto adminRequest = new AdminRequest(crates.user);
  auto userDataMiddleware = UserDataMiddleware.instance(crates);
  auto paginationFilter = new PaginationFilter;

  crateRouter
    .prepare(crates.picture)
    .and(auth.publicContributionMiddleware)
    .and(adminRequest)
    .and(userDataMiddleware)
    .and(pictureMiddleware)
    .and(paginationFilter);
}

void setupDefaultPictures(OgmCrates crates) {
  crates.picture.addDefault("image/svg+xml", "logo", PictureFileSettings.defaultLogo);
  crates.picture.addDefault("image/jpeg", "cover", PictureFileSettings.defaultCover);
  crates.picture.addDefault("image/jpeg", "default", PictureFileSettings.defaultPicture);
}
