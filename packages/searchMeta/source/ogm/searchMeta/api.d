/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.searchMeta.api;

import vibe.http.router;
import vibe.data.json;

import crate.http.router;
import crate.base;

import ogm.auth;
import ogm.crates.all;
import ogm.middleware.StaticDbContent;
import ogm.middleware.adminrequest;
import ogm.middleware.userdata;
import ogm.filter.visibility;
import ogm.searchMeta.middlewares.SearchMetaFilter;
import ogm.filter.pagination;

///
void setupSearchMetaApi(T)(CrateRouter!T crateRouter, OgmCrates crates) {
  auto staticDbContent = new StaticDbContent("searchMeta", false, CanGetAll.anyone);
  auto auth = Authentication.instance(crates.user);
  auto adminRequest = new AdminRequest(crates.user);

  auto userDataMiddleware = UserDataMiddleware.instance(crates);
  auto visibilityFilter = new VisibilityFilter!"searchMeta"(crates, crates.searchMeta);
  auto searchMetaFilter = new SearchMetaFilter(crates);
  auto paginationFilter = new PaginationFilter;

  auto preparedRoute = crateRouter.prepare(crates.searchMeta)
    .and(auth.publicDataMiddleware)
    .and(adminRequest)
    .and(staticDbContent)
    .and(userDataMiddleware)
    .and(visibilityFilter)
    .and(searchMetaFilter)
    .and(paginationFilter);
}
