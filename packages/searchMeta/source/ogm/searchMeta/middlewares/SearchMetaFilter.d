module ogm.searchMeta.middlewares.SearchMetaFilter;

import ogm.crates.all;
import crate.base;
import crate.error;
import stemmer.english;
import stemmer.cleaner;
import std.array;
import std.algorithm;
import std.string;
import std.exception;
import vibe.http.router;
import ogm.http.request;
import crate.collection.memory;
import std.math;

class SearchMetaFilter {
  struct Parameters {
    @describe("Required. keywords separated by ' '")
    @example("consistency consolations fluently", "Get records that contain the 'consistency', 'consolations' and 'fluently' keywords.")
    string q;

    @describe("Filter which models to query")
    @example("Feature", "search for map features")
    @example("Map", "search for maps")
    @example("Campaign", "search for campaigns")
    @example("Icon", "search for icons")
    @example("IconSet", "search for icon sets")
    @example("Team", "search for team")
    string model;

    @describe("Find records related to a map")
    string map;

    @describe("Start showing the items ordered by distance from the provided longitude. If you pass `lon` you also need to pass a value for `lat`.")
    @example("13.4063", "Sort items by distance from Berlin.")
    double lon;

    @describe("Start showing the items ordered by distance from the provided latitude. If you pass `lat` you also need to pass a value for `lon`.")
    @example("52.4886", "Sort items by distance from Berlin.")
    double lat;
  }

  private {
    OgmCrates crates;
    EnStemmer stemmer = new EnStemmer;
    MemoryQuery emptySelector;
  }

  ///
  this(OgmCrates crates) {
    this.crates = crates;
    this.emptySelector = new MemoryQuery();
  }

  size_t countMatches(ref string[] query) {
    return crates.searchMeta.get.where("keywords").containsAll(query).and.size;
  }

  size_t countMatches(ref string keyword) {
    return crates.searchMeta.get.where("keywords").arrayContains(keyword).and.size;
  }

  string[] findMatchByRemovingOne(ref string[] query) {
    foreach(index; 0..query.length) {
      auto newQuery = query[0..index] ~ query[index+1..$];

      if(countMatches(newQuery) > 0) {
        return newQuery;
      }
    }

    return [];
  }

  string[] findCombination(ref string[] query) {
    if(countMatches(query) > 0) {
      return query;
    }

    auto result = findMatchByRemovingOne(query);
    if(result.length > 0) {
      return result;
    }

    foreach(index; 0..query.length) {
      auto newQuery = query[0..index] ~ query[index+1..$];
      result = findMatchByRemovingOne(newQuery);

      if(result.length > 0) {
        return result;
      }
    }

    return query;
  }

  string[] filterMissingKeywords(ref string[] query) {
    string[] result;

    foreach(ref string keyword; query) {
      if(countMatches(keyword) > 0) {
        result ~= keyword;
      }
    }

    return result;
  }

  bool canViewMap(string id, RequestUserData request) {
    if(request.isAdmin) {
      return true;
    }

    auto map = crates.map.getItem(id).and.exec.front;

    if(map["visibility"]["isPublic"] == false) {
      auto session = request.session(crates);
      return session.all.maps.canFind(id);
    }

    return true;
  }

  void applyParameters(IQuery selector, Parameters parameters) {
    string[] q = parameters.q.clean.split(" ").map!(a => stemmer.get(a.toLower)).array;

    enforce!CrateValidationException(q.length > 0, "You must send at least one search query");

    q = filterMissingKeywords(q);
    string[] query = findCombination(q);

    selector.where("keywords").containsAll(query);

    if(parameters.model) {
      selector.where("relatedModel").equal(parameters.model);
    }

    if(!isNaN(parameters.lat) && !isNaN(parameters.lon)) {
      selector.where("feature.centroid").near(parameters.lon, parameters.lat, 0);
    }
  }

  @getList
  IQuery get(IQuery selector, Parameters parameters, HTTPServerRequest req) {
    auto request = RequestUserData(req);

    applyParameters(selector, parameters);

    if(parameters.map) {
      if(canViewMap(parameters.map, request)) {
        selector.where("feature.maps").arrayContains(parameters.map);
      } else {
        return emptySelector;
      }
    }

    selector.sort("lastChangeOn", -1);

    return selector;
  }
}