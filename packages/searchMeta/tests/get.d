/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.searchMeta.get;

import tests.fixtures;
import vibe.http.common;
import ogm.searchMeta.api;
import ogm.middleware.adminrequest;
import ogm.middleware.userdata;
import ogm.defaults.teams;

alias suite = Spec!({
  URLRouter router;

  describe("searching metadata", {
    beforeEach({
      setupTestData();
      router = new URLRouter;

      crates.setupDefaultTeams;
      router.crateSetup.setupSearchMetaApi(crates);

      crates.searchMeta.addItem(`{
        "name": "test",
        "title": "",
        "description": "",
        "relatedId": "",
        "keywords": [],
        "relatedModel": "",
        "visibility": {
          "team":"000000000000000000000001",
          "isPublic": true,
          "isDefault": false
        }
      }`.parseJsonString);

      crates.searchMeta.addItem(`{
        "name": "test",
        "title": "",
        "description": "",
        "relatedId": "",
        "keywords": [],
        "relatedModel": "",
        "visibility": {
          "team":"000000000000000000000001",
          "isPublic": false,
          "isDefault": false
        }
      }`.parseJsonString);

      crates.searchMeta.addItem(`{
        "name": "test",
        "title": "",
        "description": "",
        "relatedId": "",
        "keywords": [],
        "relatedModel": "",
        "visibility": {
          "team":"000000000000000000000003",
          "isPublic": false,
          "isDefault": false
        }
      }`.parseJsonString);
    });

    describeCredentialsRule("searching for public records", "yes", "search metadata", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      auto initialSize = crates.presentation.get.size;

      router
        .request
        .get("/searchmetas/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "searchMeta": {
            "_id":"000000000000000000000001",
            "name": "test",
            "title": "",
            "description": "",
            "relatedId": "",
            "canEdit": false,
            "relatedModel": "",
            "keywords": [],
            "visibility": {
              "team":"000000000000000000000001",
              "isPublic": true,
              "isDefault": false
            }
          } }`.parseJsonString);
        });
    });

    describeCredentialsRule("searching for public records", "yes", "search metadata", "no rights", {
      router
        .request
        .get("/searchmetas/000000000000000000000001")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "searchMeta": {
            "_id":"000000000000000000000001",
            "name": "test",
            "canEdit": false,
            "title": "",
            "description": "",
            "relatedId": "",
            "relatedModel": "",
            "keywords": [],
            "visibility": {
              "team":"000000000000000000000001",
              "isPublic": true,
              "isDefault": false
            }
          } }`.parseJsonString);
        });
    });


    describeCredentialsRule("searching for private records owned by own team", "yes", "search metadata", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      auto initialSize = crates.presentation.get.size;

      router
        .request
        .get("/searchmetas/000000000000000000000002")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "searchMeta": {
            "_id":"000000000000000000000002",
            "name": "test",
            "title": "",
            "description": "",
            "relatedId": "",
            "canEdit": false,
            "relatedModel": "",
            "keywords": [],
            "visibility": {
              "team":"000000000000000000000001",
              "isPublic": false,
              "isDefault": false
            }
          } }`.parseJsonString);
        });
    });

    describeCredentialsRule("searching for private records owned by own team", "-", "search metadata", "no rights", {
      router
        .request
        .get("/searchmetas/000000000000000000000002")
        .expectStatusCode(404)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{
            "errors": [{
            "description": "There is no item with id ` ~ "`000000000000000000000002`" ~ `",
            "status": 404,
            "title": "Crate not found"
          }]}`).parseJsonString);
        });
    });


    describeCredentialsRule("searching for private records owned by other team", "yes", "search metadata", "administrator", (string type) {
      auto initialSize = crates.presentation.get.size;

      router
        .request
        .get("/searchmetas/000000000000000000000003")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "searchMeta": {
            "_id":"000000000000000000000003",
            "name": "test",
            "title": "",
            "description": "",
            "relatedId": "",
            "canEdit": false,
            "relatedModel": "",
            "keywords": [],
            "visibility": {
              "team":"000000000000000000000003",
              "isPublic": false,
              "isDefault": false
            }
          } }`.parseJsonString);
        });
    });

    describeCredentialsRule("searching for private records owned by other team", "no", "search metadata", ["owner", "leader", "member", "guest"], (string type) {
      auto initialSize = crates.presentation.get.size;

      router
        .request
        .get("/searchmetas/000000000000000000000003")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(404)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{
            "errors": [{
            "description": "There is no item with id ` ~ "`000000000000000000000003`" ~ `",
            "status": 404,
            "title": "Crate not found"
          }]}`).parseJsonString);
        });
    });

    describeCredentialsRule("searching for private records owned by other team", "-", "search metadata", "no rights", {
      router
        .request
        .get("/searchmetas/000000000000000000000003")
        .expectStatusCode(404)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{
            "errors": [{
            "description": "There is no item with id ` ~ "`000000000000000000000003`" ~ `",
            "status": 404,
            "title": "Crate not found"
          }]}`).parseJsonString);
        });
    });
  });
});
