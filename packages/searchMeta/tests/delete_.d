/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.searchMeta.delete_;

import tests.fixtures;
import vibe.http.common;
import ogm.searchMeta.api;
import ogm.middleware.adminrequest;
import ogm.middleware.userdata;
import ogm.defaults.teams;

alias suite = Spec!({
  URLRouter router;

  describe("Deleting search metadata", {
    Json searchMeta;

    beforeEach({
      setupTestData();
      router = new URLRouter;

      crates.setupDefaultTeams;
      router.crateSetup.setupSearchMetaApi(crates);

      crates.searchMeta.addItem(`{
        "name": "test",
        "title": "",
        "description": "",
        "relatedId": "",
        "relatedModel": "",
        "visibility": {
          "team":"000000000000000000000001",
          "isPublic": true,
          "isDefault": false
        }
      }`.parseJsonString);
    });

    describeCredentialsRule("delete search metadata", "no", "search metadata", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      auto initialSize = crates.presentation.get.size;

      router
        .request
        .delete_("/searchmetas/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "errors": [{
            "description": "You can't delete the 'searchMeta'.",
            "status": 403,
            "title": "Forbidden"
          }]}`.parseJsonString);
        });
    });

    describeCredentialsRule("delete search metadata", "no", "search metadata", "no rights", {
      router
        .request
        .delete_("/searchmetas/000000000000000000000001")
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
        });
    });
  });
});
