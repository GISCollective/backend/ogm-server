/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.searchMeta.getList;

import tests.fixtures;
import vibe.http.common;
import ogm.searchMeta.api;
import crate.test.mock_query;
import ogm.middleware.adminrequest;
import ogm.middleware.userdata;
import ogm.searchMeta.middlewares.SearchMetaFilter;
import ogm.defaults.teams;

alias suite = Spec!({
  URLRouter router;

  describe("the q query param", {
    beforeEach({
      setupTestData();
      router = new URLRouter;

      crates.setupDefaultTeams;
      router.crateSetup.setupSearchMetaApi(crates);

      crates.searchMeta.addItem(`{
        "name": "test",
        "title": "",
        "description": "",
        "relatedId": "",
        "keywords": ["consist", "consol", "fluentli"],
        "relatedModel": "",
        "visibility": {
          "team":"000000000000000000000001",
          "isPublic": true,
          "isDefault": false
        }
      }`.parseJsonString);

      crates.searchMeta.addItem(`{
        "name": "test",
        "title": "",
        "description": "",
        "relatedId": "",
        "keywords": ["consist", "a", "b"],
        "relatedModel": "",
        "visibility": {
          "team":"000000000000000000000001",
          "isPublic": false,
          "isDefault": false
        }
      }`.parseJsonString);

      crates.searchMeta.addItem(`{
        "name": "test",
        "title": "",
        "description": "",
        "relatedId": "",
        "keywords": [],
        "relatedModel": "",
        "visibility": {
          "team":"000000000000000000000003",
          "isPublic": false,
          "isDefault": false
        }
      }`.parseJsonString);
    });

    it("matches records by stemming the query", {
      auto initialSize = crates.presentation.get.size;

      router
        .request
        .get("/searchmetas?q=consistency consolations fluently")
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "searchMetas": [{
            "_id":"000000000000000000000001",
            "name": "test",
            "title": "",
            "description": "",
            "relatedId": "",
            "canEdit": false,
            "relatedModel": "",
            "keywords": ["consist", "consol", "fluentli"],
            "visibility": {
              "team":"000000000000000000000001",
              "isPublic": true,
              "isDefault": false
            }
          }]}`.parseJsonString);
        });
    });

    it("returns an error when the q parameter is missing", {
      auto initialSize = crates.presentation.get.size;

      router
        .request
        .get("/searchmetas")
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .expectStatusCode(400)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "errors": [{
              "description": "You must send at least one search query",
              "status": 400,
              "title": "Validation error"
            }]
          }`.parseJsonString);
        });
    });

    it("matches records by stemming the query with extra spaces", {
      auto initialSize = crates.presentation.get.size;

      router
        .request
        .get("/searchmetas?q=consistency  consolations   fluently")
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "searchMetas": [{
            "_id":"000000000000000000000001",
            "name": "test",
            "title": "",
            "description": "",
            "relatedId": "",
            "canEdit": false,
            "relatedModel": "",
            "keywords": ["consist", "consol", "fluentli"],
            "visibility": {
              "team":"000000000000000000000001",
              "isPublic": true,
              "isDefault": false
            }
          }]}`.parseJsonString);
        });
    });

    it("matches records when only 2 out of 3 keywords are found", {
      auto initialSize = crates.presentation.get.size;

      router
        .request
        .get("/searchmetas?q=consistency missing fluently")
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["searchMetas"].length.should.equal(1);
        });

      router
        .request
        .get("/searchmetas?q=missing consistency fluently")
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["searchMetas"].length.should.equal(1);
        });

      router
        .request
        .get("/searchmetas?q=consistency fluently missing")
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["searchMetas"].length.should.equal(1);
        });
    });

    it("matches records when only 1 out of 3 keywords are found", {
      auto initialSize = crates.presentation.get.size;

      router
        .request
        .get("/searchmetas?q=consistency missing other")
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["searchMetas"].length.should.equal(2);
        });

      router
        .request
        .get("/searchmetas?q=missing other fluently")
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["searchMetas"].length.should.equal(1);
        });

      router
        .request
        .get("/searchmetas?q=other fluently missing")
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["searchMetas"].length.should.equal(1);
        });
    });

    it("matches records when only 3 out of 4 keywords don't match any record", {
      auto initialSize = crates.presentation.get.size;

      router
        .request
        .get("/searchmetas?q=consistency this is missing")
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["searchMetas"].length.should.equal(2);
        });
    });
  });

  describe("the model query param", {
    beforeEach({
      setupTestData();
      router = new URLRouter;

      crates.setupDefaultTeams;
      router.crateSetup.setupSearchMetaApi(crates);

      crates.searchMeta.addItem(`{
        "name": "test",
        "title": "",
        "description": "",
        "relatedId": "",
        "keywords": ["consist", "consol", "fluentli"],
        "relatedModel": "Model1",
        "visibility": {
          "team":"000000000000000000000001",
          "isPublic": true,
          "isDefault": false
        }
      }`.parseJsonString);

      crates.searchMeta.addItem(`{
        "name": "test",
        "title": "",
        "description": "",
        "relatedId": "",
        "keywords": ["consist", "consol", "fluentli"],
        "relatedModel": "Model2",
        "visibility": {
          "team":"000000000000000000000001",
          "isPublic": false,
          "isDefault": false
        }
      }`.parseJsonString);

      crates.searchMeta.addItem(`{
        "name": "test",
        "title": "",
        "description": "",
        "relatedId": "",
        "keywords": [],
        "relatedModel": "",
        "visibility": {
          "team":"000000000000000000000003",
          "isPublic": false,
          "isDefault": false
        }
      }`.parseJsonString);
    });

    it("search for all records when there is no model query", {
      auto initialSize = crates.presentation.get.size;

      router
        .request
        .get("/searchmetas?q=consistency consolations fluently")
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "searchMetas": [{
            "_id":"000000000000000000000001",
            "name": "test",
            "title": "",
            "description": "",
            "relatedId": "",
            "canEdit": false,
            "relatedModel": "Model1",
            "keywords": ["consist", "consol", "fluentli"],
            "visibility": {
              "team":"000000000000000000000001",
              "isPublic": true,
              "isDefault": false
            }
          }, {
            "_id":"000000000000000000000002",
            "name": "test",
            "title": "",
            "description": "",
            "relatedId": "",
            "canEdit": false,
            "relatedModel": "Model2",
            "keywords": ["consist", "consol", "fluentli"],
            "visibility": {
              "team":"000000000000000000000001",
              "isPublic": false,
              "isDefault": false
            }
          }]}`.parseJsonString);
        });
    });

    it("search for records from a particular model when there is a model query", {
      auto initialSize = crates.presentation.get.size;

      router
        .request
        .get("/searchmetas?q=consistency consolations fluently&model=Model1")
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "searchMetas": [{
            "_id":"000000000000000000000001",
            "name": "test",
            "title": "",
            "description": "",
            "relatedId": "",
            "canEdit": false,
            "relatedModel": "Model1",
            "keywords": ["consist", "consol", "fluentli"],
            "visibility": {
              "team":"000000000000000000000001",
              "isPublic": true,
              "isDefault": false
            }
          }]}`.parseJsonString);
        });
    });
  });

  describe("the map query param", {

    beforeEach({
      setupTestData();
      router = new URLRouter;

      crates.setupDefaultTeams;
      router.crateSetup.setupSearchMetaApi(crates);

      crates.searchMeta.addItem(`{
        "name": "test",
        "title": "",
        "description": "",
        "relatedId": "",
        "keywords": ["consist", "consol", "fluentli"],
        "relatedModel": "Model1",
        "feature": {
          "maps": ["000000000000000000000005", "000000000000000000000001"]
        },
        "visibility": {
          "team":"000000000000000000000001",
          "isPublic": true,
          "isDefault": false
        }
      }`.parseJsonString);

      crates.searchMeta.addItem(`{
        "name": "test",
        "title": "",
        "description": "",
        "relatedId": "",
        "keywords": ["consist", "consol", "fluentli"],
        "relatedModel": "Model2",
        "feature": {
          "maps": ["000000000000000000000005", "000000000000000000000002"]
        },
        "visibility": {
          "team":"000000000000000000000001",
          "isPublic": false,
          "isDefault": false
        }
      }`.parseJsonString);
    });

    it("returns all records when there is no map query", {
      auto initialSize = crates.presentation.get.size;

      router
        .request
        .get("/searchmetas?q=consistency consolations fluently")
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "searchMetas": [{
            "_id":"000000000000000000000001",
            "name": "test",
            "title": "",
            "description": "",
            "relatedId": "",
            "canEdit": false,
            "relatedModel": "Model1",
            "keywords": ["consist", "consol", "fluentli"],
            "feature": {
              "maps": ["000000000000000000000005", "000000000000000000000001"]
            },
            "visibility": {
              "team":"000000000000000000000001",
              "isPublic": true,
              "isDefault": false
            }
          }, {
            "_id":"000000000000000000000002",
            "name": "test",
            "title": "",
            "description": "",
            "relatedId": "",
            "canEdit": false,
            "feature": {
              "maps": ["000000000000000000000005", "000000000000000000000002"]
            },
            "relatedModel": "Model2",
            "keywords": ["consist", "consol", "fluentli"],
            "visibility": {
              "team":"000000000000000000000001",
              "isPublic": false,
              "isDefault": false
            }
          }]}`.parseJsonString);
        });
    });

    it("returns records from a particular map when there is a map query", {
      auto initialSize = crates.presentation.get.size;

      router
        .request
        .get("/searchmetas?q=consistency consolations fluently&map=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "searchMetas": [{
            "_id":"000000000000000000000001",
            "name": "test",
            "title": "",
            "description": "",
            "relatedId": "",
            "canEdit": false,
            "relatedModel": "Model1",
            "keywords": ["consist", "consol", "fluentli"],
            "feature": {
              "maps": ["000000000000000000000005", "000000000000000000000001"]
            },
            "visibility": {
              "team":"000000000000000000000001",
              "isPublic": true,
              "isDefault": false
            }
          }]}`.parseJsonString);
        });
    });

    it("returns an empty list when the map is private and the user does not have access to it", {
      auto initialSize = crates.presentation.get.size;

      router
        .request
        .get("/searchmetas?q=consistency consolations fluently&map=000000000000000000000002")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "searchMetas": []}`.parseJsonString);
        });
    });

    it("returns an the related records when the map is peivate and the user has access to it", {
      auto initialSize = crates.presentation.get.size;

      router
        .request
        .get("/searchmetas?q=consistency consolations fluently&map=000000000000000000000002")
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["searchMetas"].length.should.equal(1);
        });
    });
  });

  describe("the lon lat query params", {
    SearchMetaFilter filter;
    MockQuery query;

    beforeEach({
      setupTestData();

      filter = new SearchMetaFilter(crates);
      query = new MockQuery;
    });

    it("search records near 1,2 when lon=1 and lat=2", {
      SearchMetaFilter.Parameters parameters;
      parameters.q = "query";
      parameters.lon = 1;
      parameters.lat = 2;

      filter.applyParameters(query, parameters);

      query.log.join(" ").should.equal("where keywords containsAll [] where feature.centroid near 1, 2, 0m");
    });

    it("does not search records near a point when when lon is not set", {
      SearchMetaFilter.Parameters parameters;
      parameters.q = "query";
      parameters.lat = 2;

      filter.applyParameters(query, parameters);

      query.log.join(" ").should.equal("where keywords containsAll []");
    });

    it("does not search records near a point when when lat is not set", {
      SearchMetaFilter.Parameters parameters;
      parameters.q = "query";
      parameters.lon = 2;

      filter.applyParameters(query, parameters);

      query.log.join(" ").should.equal("where keywords containsAll []");
    });
  });
});
