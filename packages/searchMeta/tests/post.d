/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.searchMeta.post;

import tests.fixtures;
import vibe.http.common;
import ogm.searchMeta.api;
import ogm.middleware.adminrequest;
import ogm.middleware.userdata;
import ogm.defaults.teams;

alias suite = Spec!({
  URLRouter router;

  describe("Creating search metadata", {
    Json searchMeta;

    beforeEach({
      setupTestData();
      router = new URLRouter;

      crates.setupDefaultTeams;
      router.crateSetup.setupSearchMetaApi(crates);

      searchMeta = `{ "searchMeta": {
        "name": "test",
        "title": "",
        "description": "",
        "relatedId": "",
        "relatedModel": "",
        "visibility": {
          "team":"000000000000000000000001",
          "isPublic": true,
          "isDefault": false
        }
      }}`.parseJsonString;
    });

    describeCredentialsRule("create search metadata", "no", "search metadata", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      auto initialSize = crates.presentation.get.size;

      router
        .request
        .post("/searchmetas")
        .send(searchMeta)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "errors": [{
            "description": "You can't create this 'searchMeta'.",
            "status": 403,
            "title": "Forbidden"
          }]}`.parseJsonString);
        });
    });

    describeCredentialsRule("create search metadata", "no", "search metadata", "no rights", {
      router
        .request
        .post("/searchmetas")
        .send(searchMeta)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
        });
    });
  });
});
