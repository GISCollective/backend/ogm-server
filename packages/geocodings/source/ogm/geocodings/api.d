/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.geocodings.api;

import vibe.http.router;
import vibe.data.json;
import crate.http.router;
import crate.base;
import crate.error;

import ogm.middleware.adminrequest;
import ogm.auth;
import ogm.crates.all;
import ogm.crates.geocoding;
import ogm.geocoding.IndexMaps;
import ogm.geocoding.nominatim;
import ogm.geocodings.GeocodingQuery;
import ogm.middleware.userdata;

import vibe.http.client;
import vibe.core.log;
import vibe.stream.operations;

///
void setupGeocodingApi(T)(CrateRouter!T crateRouter, OgmCrates crates, string serviceUrl) {

  string userAgent = `GISCollective at ` ~ serviceUrl;

  @safe
  Json getRequest(string strUrl) {
    Json result;

    requestHTTP(strUrl,
      (scope req) {
        req.headers["User-Agent"] = "Mozilla/5.0 (X11; Linux x86_64; rv:100.0) Gecko/20100101 Firefox/100.0";
        req.headers["Referer"] = serviceUrl;
        req.headers["Connection"] = "close";

        req.headers.remove("Accept-Encoding");
      },
      (scope res) {
        if(res.statusCode == 200) {
          result = res.readJson;
        } else {
          logError("getRequest ERROR: %s", res.bodyReader.readAllUTF8());
        }
      }
    );

    return result;
  }

  auto auth = Authentication.instance(crates.user);
  auto adminRequest = new AdminRequest(crates.user);
  auto userDataMiddleware = UserDataMiddleware.instance(crates);
  auto geocodingQuery = new GeocodingQuery(crates);

  crateGetters["Team"] = &crates.team.getItem;
  crateGetters["Icon"] = &crates.icon.getItem;
  crateGetters["Map"] = &crates.map.getItem;
  crateGetters["IconSet"] = &crates.iconSet.getItem;

  metaCrate = crates.meta;
  batchJobCrate = crates.batchJob;

  auto privateAuth = auth.privateDataMiddleware;

  GeocodingCrate.instance.resolvers = cast(GeocodingResolver[]) [
    new IndexMapsResolver(crates),
    new NominatimResolver(crates, &getRequest)
  ];

  auto preparedRoute = crateRouter.prepare(crates.geocoding);

  preparedRoute.and(auth.publicDataMiddleware);
  preparedRoute.and(adminRequest);
  preparedRoute.and(userDataMiddleware);
  preparedRoute.and(geocodingQuery);

  GeocodingCrate.breakSize = 4;
}

void setupDefaultGeocodings(OgmCrates crates) {
  import ogm.models.map;
  import ogm.models.visibility;
  import ogm.models.modelinfo;

  auto size = crates.map.get.where("name").equal("Nominatim cache").and.size;

  if(size > 0) {
    return;
  }

  auto teamId = crates.team.get.where("name").equal("General").and.exec.front["_id"];

  LazyMap map;

  map.name = "Nominatim cache";
  map.description = Json("");
  map.visibility = Visibility();
  map.iconSets = IconSets();
  map.baseMaps = BaseMaps();
  map.info = ModelInfo();
  map.cover = Picture();
  map.isIndex = true;
  map.license = License("Data © OpenStreetMap contributors, ODbL 1.0", "https://www.openstreetmap.org/copyright");

  auto jsonMap = map.toJson;
  jsonMap["visibility"]["team"] = teamId;

  crates.map.addItem(jsonMap);
}
