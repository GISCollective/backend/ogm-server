/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.geocodings.GeocodingQuery;

import crate.base;
import crate.error;
import crate.attributes;

import ogm.crates.all;

class GeocodingQuery {

  struct Parameters {
    @describe("Free-form query string to search for.")
    @example("pilkington avenue, birmingham", "Get features that matches the query.")
    string query;
  }

  private {
    OgmCrates crates;
    Exception shortQueryValidation;
    Exception notSupported;
  }

  this(OgmCrates crates) {
    this.crates = crates;
    this.shortQueryValidation = new CrateValidationException("The `query` parameter is too short.");
    this.notSupported = new CrateValidationException("The operation is not supported.");
  }

  @get
  IQuery get(IQuery selector, Parameters parameters) {
    if(parameters.query.length <= 1) {
      throw this.shortQueryValidation;
    }

    return selector.where("name").equal(parameters.query).and;
  }

  @delete_
  @getItem
  @patch
  @put
  @create
  void forbiddenOperation() {
    throw this.notSupported;
  }
}