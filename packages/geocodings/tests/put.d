/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.geocoding.put;

import tests.fixtures;
import vibe.http.common;
import ogm.geocodings.api;

alias suite = Spec!({
  URLRouter router;

  describe("updating geocoding items", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupGeocodingApi(crates, "");
    });

    describeCredentialsRule("update", "no", "geocoding", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .put("/geocodings/000000000000000000000001")
        .send(Json.emptyObject)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal((`{
              "errors": [{
                "description": "The operation is not supported.",
                "status": 400,
                "title": "Validation error"
              }]}`).parseJsonString);
          });
    });

    describeCredentialsRule("update", "no", "geocoding", "no rights", {
      router
        .request
        .put("/geocodings/000000000000000000000001")
        .send(Json.emptyObject)
        .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal((`{ "error": "Authorization required" }`).parseJsonString);
          });
    });
  });
});
