/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.geocodings.delete_;

import tests.fixtures;
import vibe.http.common;
import ogm.geocodings.api;

alias suite = Spec!({
  URLRouter router;

  describe("Deleting geocoding items", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupGeocodingApi(crates, "");
    });

    describeCredentialsRule("delete", "no", "geocoding", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .delete_("/geocodings/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal((`{
              "errors": [{
                "description": "The operation is not supported.",
                "status": 400,
                "title": "Validation error"
              }]}`).parseJsonString);
          });
    });

    describeCredentialsRule("delete", "no", "geocoding", "no rights", {
      router
        .request
        .delete_("/geocodings/000000000000000000000001")
        .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal((`{ "error": "Authorization required" }`).parseJsonString);
          });
    });
  });
});
