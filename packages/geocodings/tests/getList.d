/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.geocoding.getList;

import tests.fixtures;
import vibe.http.common;
import ogm.geocodings.api;

alias suite = Spec!({
  URLRouter router;

  describe("Get geocoding list", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupGeocodingApi(crates, "");
    });

    describeCredentialsRule("query without a term", "no", "geocoding", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/geocodings")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal((`{
              "errors": [{
                "description": "The ` ~ "`query`" ~ ` parameter is too short.",
                "status": 400,
                "title": "Validation error"
              }]}`).parseJsonString);
          });
    });

    describeCredentialsRule("query without a term", "no", "geocoding", "no rights", {
      router
        .request
        .get("/geocodings")
        .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal((`{
              "errors": [{
                "description": "The ` ~ "`query`" ~ ` parameter is too short.",
                "status": 400,
                "title": "Validation error"
              }]}`).parseJsonString);
          });
    });

    describeCredentialsRule("query with a term", "yes", "geocoding", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/geocodings?query=test")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"geocodings": []}`.parseJsonString);
          });
    });

    describeCredentialsRule("query with a term", "yes", "geocoding", "no rights", {
      router
        .request
        .get("/geocodings?query=test")
        .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"geocodings": []}`.parseJsonString);
          });
    });
  });
});
