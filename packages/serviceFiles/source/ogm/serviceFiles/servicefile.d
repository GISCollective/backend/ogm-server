/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.serviceFiles.servicefile;

import ogm.crates.all;
import vibe.http.router;
import vibe.data.json;

import std.array;
import std.conv;
import std.algorithm;
import std.range;
import std.datetime;
import std.string;

import crate.base;
import crate.http.cors;
import crate.resource.file;
import crate.resource.base;
import crate.lazydata.base;

import ogm.models.picture;

version(unittest) {
  import fluent.asserts;
}

struct ServiceFileSettings {
  static string path = "files/";
  static string baseUrl = "";
}

alias ServiceFile = CrateFileResource!(ServiceFileSettings);

/// Class used to compute site tiles
class ServiceFileHosting(Type) {
  private {
    OgmCrates crates;

    LazyField!string resourceId;
    LazyField!CrateResource resource;

    string spaceKey;
  }

  this(OgmCrates crates, string preferenceKey, string spaceKey, string defaultFile) {
    this.crates = crates;

    this.spaceKey = spaceKey;

    this.resourceId = LazyField!string({
      auto range = this.crates.preference.get.where("name").equal(preferenceKey).and.exec;

      if(range.empty) {
        return "";
      }

      return range.front["value"].to!string;
    }, 10.seconds);

    this.resource = LazyField!CrateResource({
      auto range = this.crates.picture.get.where("_id").equal(ObjectId.fromString(this.resourceId.compute)).and.exec;

      if(range.empty) {
        return cast(CrateResource) new ServiceFile(defaultFile);
      }

      return cast(CrateResource) new Type(range.front["picture"].to!string);
    }, 60.seconds);
  }

  ///
  void options(HTTPServerRequest req, HTTPServerResponse response) {
    response.headers["Access-Control-Allow-Origin"] = "*";
    response.headers["Access-Control-Allow-Methods"] = "GET";
    response.headers["Access-Control-Allow-Headers"] = "Content-Type, Authorization";

    response.writeBody("", 204);
  }

  CrateResource picture(HTTPServerRequest req) {
    string domain = req.host.split(":").front;
    auto spaceRange = crates.space.get.where("domain").equal(domain).and.exec;

    if(spaceRange.empty) {
      return this.resource.compute;
    }

    auto picture = crates.picture.getItem(spaceRange.front[spaceKey].to!string).and.exec.front;

    return cast(CrateResource) new Type(picture["picture"].to!string);
  }

  ///
  void get(HTTPServerRequest req, HTTPServerResponse response) {
    auto localResource = this.picture(req);

    response.headers["Content-Type"] = localResource.contentType;

    if(localResource.hasSize) {
      response.headers["Content-Length"] = localResource.size.to!string;
    }

    response.statusCode = 200;
    localResource.write(response.bodyWriter);
  }

  ///
  void getSize(HTTPServerRequest req, HTTPServerResponse response) {
    auto localResource = this.picture(req);

    if(localResource is null) {
      return;
    }

    localResource.httpHandler(req, response);
  }
}
