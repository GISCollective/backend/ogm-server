/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.serviceFiles.api;

import vibe.http.router;
import crate.http.router;
import crate.base;

import ogm.auth;
import ogm.crates.all;
import ogm.models.picture;

import ogm.files.configuration;

import ogm.serviceFiles.servicefile;

///
void setupServiceFiles(T)(CrateRouter!T crateRouter, OgmCrates crates, FilesConfiguration config) {
  ServiceFileSettings.path = config.location;

  auto logo = new ServiceFileHosting!AppIconFile(crates, "appearance.logo", "logoSquare", "logo.png");
  auto cover = new ServiceFileHosting!AppCoverFile(crates, "appearance.cover", "cover", "cover.jpg");

  crateRouter.router.match(HTTPMethod.OPTIONS, "/service/logo", &logo.options);
  crateRouter.router.match(HTTPMethod.GET, "/service/logo", &logo.get);

  crateRouter.router.match(HTTPMethod.OPTIONS, "/service/logo/:size", &logo.options);
  crateRouter.router.match(HTTPMethod.GET, "/service/logo/:size", &logo.getSize);

  crateRouter.router.match(HTTPMethod.OPTIONS, "/service/cover", &cover.options);
  crateRouter.router.match(HTTPMethod.GET, "/service/cover", &cover.get);

  crateRouter.router.match(HTTPMethod.OPTIONS, "/service/cover/:size", &cover.options);
  crateRouter.router.match(HTTPMethod.GET, "/service/cover/:size", &cover.getSize);
}
