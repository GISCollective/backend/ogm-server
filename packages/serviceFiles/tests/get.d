/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.servicefiles.get;

import fluent.asserts;
import trial.discovery.spec;

import vibe.data.json;
import vibe.data.bson;
import vibe.http.router;
import vibe.http.common;

import ogm.files.configuration;
import ogm.crates.all;
import ogm.models.picture;
import ogm.crates.defaults;
import ogm.serviceFiles.api;
import ogm.defaults.preferences;

import tests.fixtures;

import std.file;

URLRouter router;

alias suite = Spec!({

  describe("The logo image", {
    beforeEach({
      setupTestData();
      router = new URLRouter;

      FilesConfiguration config;
      config.location = "./tmp-files";

      if(!"./tmp-files".exists) {
        mkdir("tmp-files");
      }

      PictureFileSettings.files = new MockGridFsFiles;
      PictureFileSettings.chunks = new MockGridFsChunks;
      crates.picture.addDefault("image/svg+xml", "logo", "../../deploy/default.png");
      crates.picture.addDefault("image/jpeg", "cover", "../../deploy/default.png");
      crates.picture.addDefault("image/jpeg", "default", "../../deploy/default.png");

      setupDefaultPreferences(crates, GeneralConfig());

      router.crateSetup.setupServiceFiles(crates, config);
    });

    afterEach({
      if("./tmp-files".exists) {
        rmdirRecurse("./tmp-files");
      }
    });

    it("should return the right CORS", {
      router
        .request
        .customMethod!(HTTPMethod.OPTIONS)("/service/logo")
        .expectHeader("Access-Control-Allow-Methods", "GET")
        .expectStatusCode(204)
        .end((Response response) => () {
          response.bodyString.should.equal("");
        });
    });

    it("should return the default logo", {
      router
        .request
        .get("/service/logo")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyString.should.equal("");
        });
    });

    it("should return the resized default logo", {
      router
        .request
        .get("/service/logo/x-16")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyString.should.equal("");
        });
    });

    describe("with a custom picture", function() {
      beforeEach({
        Bson data = Bson.emptyObject;

        immutable(ubyte)[] binData = cast(immutable(ubyte)[])"custom logo";
        data["data"] = BsonBinData(BsonBinData.Type.generic, binData);

        MockGridFsChunks.items = [data];

        auto picture = Picture();
        picture.picture = new PictureFile("000000000000000000000001");

        auto result = crates.picture.addItem(picture.serializeToJson);
        auto pref = crates.preference.get.where("name").equal("appearance.logo").and.exec.front;
        pref["value"] = result["_id"];

        crates.preference.updateItem(pref);
      });

      it("should return the custom logo");/*, {
        router
          .request
          .get("/service/logo")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyString.should.equal("custom logo");
          });
      });*/
    });
  });
});
