/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.iconSets.requests.categories;

import ogm.filter.visibility;
import ogm.http.request;
import ogm.crates.all;
import ogm.iconSets.requests.iconsetrequest;
import ogm.icons.CategoryTree;

import vibe.http.router;
import vibe.data.json;


import crate.error;
import crate.base;

import std.algorithm;
import std.array;

///
class CategoriesRequest : IconSetRequest {

  ///
  this(OgmCrates crates, CategoryTree iconsTree, VisibilityFilter!"iconSets" visibility) {
    super(crates, iconsTree, visibility);
  }

  ///
  @getItem
  override void getItem(HTTPServerRequest req, HTTPServerResponse res, Json item) {
    Json localConvert(string a) {
      return convert!"category"(a);
    }

    auto categories = iconsTree.subset(item["_id"].to!string).getCategoryList.map!localConvert.array;

    res.writeJsonBody([ "iconSetCategories": categories ], 200);
  }
}
