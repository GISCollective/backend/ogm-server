/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.iconSets.requests.iconsetrequest;

import ogm.filter.visibility;
import ogm.http.request;
import ogm.crates.all;

import vibe.http.router;
import vibe.data.json;
import ogm.icons.CategoryTree;

import crate.error;
import crate.base;
import crate.http.queryParams;

///
class IconSetRequest {

  protected {
    OgmCrates crates;
    VisibilityFilter!"iconSets" visibility;
    CategoryTree iconsTree;
  }

  ///
  this(OgmCrates crates, CategoryTree iconsTree, VisibilityFilter!"iconSets" visibility) {
    this.crates = crates;
    this.iconsTree = iconsTree;
    this.visibility = visibility;
  }

  ///
  void options(HTTPServerRequest req, HTTPServerResponse res) {
    res.headers["Access-Control-Allow-Methods"] = "GET";
    res.headers["Access-Control-Allow-Origin"] = "*";
    res.headers["Access-Control-Allow-Headers"] = "Content-Type, Authorization";

    res.writeBody("", 204);
  }

  ///
  Json convert(string key)(string a) {
    Json data = Json.emptyObject;
    if(a == "") {
      data[key] = "not set";
    } else {
      data[key] = a;
    }

    return data;
  }

  ///
  void handler(HTTPServerRequest req, HTTPServerResponse res) @trusted {
    immutable id = req.params["id"];
    auto visibilityParams = req.query.parseQueryParams!(VisibilityParameters);

    auto query = this.visibility.get(crates.iconSet.getItem(id), visibilityParams, req);
    auto result = query.exec;

    enforce!CrateNotFoundException(!result.empty, "Item `" ~ id ~ "` not found.");

    this.getItem(req, res, result.front);
  }

  ///
  void getItem(HTTPServerRequest req, HTTPServerResponse res, Json item) {
  }
}
