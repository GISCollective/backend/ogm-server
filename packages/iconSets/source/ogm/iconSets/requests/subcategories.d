/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.iconSets.requests.subcategories;

import ogm.filter.visibility;
import ogm.http.request;
import ogm.crates.all;
import ogm.iconSets.requests.iconsetrequest;
import ogm.icons.CategoryTree;

import vibe.http.router;
import vibe.data.json;

import crate.error;
import crate.base;

import std.algorithm;
import std.array;

///
class SubcategoriesRequest : IconSetRequest {

  ///
  this(OgmCrates crates, CategoryTree iconsTree, VisibilityFilter!"iconSets" visibility) {
    super(crates, iconsTree, visibility);
  }

  ///
  override void getItem(HTTPServerRequest req, HTTPServerResponse res, Json item) {
    Json localConvert(string a) {
      return convert!"subcategory"(a);
    }

    import std.stdio;

    string[] subcategories;

    if("category" in req.query) {
      subcategories = iconsTree
        .subset(item["_id"].to!string)
        .getSubcategoryList(req.query["category"]);
    } else {
      subcategories = iconsTree
        .subset(item["_id"].to!string)
        .getSubcategoryList();
    }

    res.writeJsonBody([ "iconSetSubcategories": subcategories.map!localConvert.array ], 200);
  }
}
