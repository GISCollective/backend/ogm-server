/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.iconSets.api;

import vibe.http.router;
import crate.http.router;
import crate.base;
import crate.http.router;

import ogm.auth;
import ogm.crates.all;
import ogm.middleware.userdata;
import ogm.middleware.adminrequest;
import ogm.filter.visibility;
import ogm.filter.pagination;
import ogm.filter.searchterm;

import ogm.iconSets.requests.categories;
import ogm.iconSets.middleware.usageChecks;
import ogm.iconSets.middleware.cover;
import ogm.iconSets.requests.subcategories;
import ogm.iconSets.operations.CsvIconSetExport;
import ogm.iconSets.middleware.computedVisibility;
import ogm.middleware.GlobalAccess;
import ogm.middleware.IdsFilter;
import crate.error;

import ogm.icons.CategoryTree;

///
void setupIconSetApi(T)(CrateRouter!T crateRouter, OgmCrates crates) {
  auto iconsTree = new CategoryTree(&crates.icon.get, &crates.iconSet.getItem);
  auto auth = Authentication.instance(crates.user);
  auto userDataMiddleware = UserDataMiddleware.instance(crates);
  auto adminRequest = new AdminRequest(crates.user);
  auto visibilityFilter = new VisibilityFilter!"iconSets"(crates, crates.iconSet);
  auto paginationFilter = new PaginationFilter;
  auto searchTermFilter = new SearchTermFilter;
  auto globalAccess = GlobalAccessMiddleware.instance(crates);

  auto categories = new CategoriesRequest(crates, iconsTree, visibilityFilter);
  auto subcategories = new SubcategoriesRequest(crates, iconsTree, visibilityFilter);

  auto coverMiddleware = new CoverMiddleware(crates);

  auto idsFilter = new IdsFilter(crates);
  auto usageChecks = new IconSetUsageChecksMiddleware(crates);

  auto iconVisibilityMiddleware = new IconVisibilityMiddleware(crates);

  crateGetters["Team"] = &crates.team.getItem;
  crateGetters["Icon"] = &crates.icon.getItem;
  crateGetters["Picture"] = &crates.picture.getItem;

  auto csvIconSetExport = new CsvIconSetExport(crates);

  crateRouter
    .prepare(crates.iconSet)
    .withCustomOperation(csvIconSetExport)
    .and(crates.iconSet)
    .and(auth.publicDataMiddleware)
    .and(adminRequest)
    .and(userDataMiddleware)
    .and(globalAccess)
    .and(usageChecks)
    .and(searchTermFilter)
    .and(iconVisibilityMiddleware)
    .and(visibilityFilter)
    .and(idsFilter)
    .and(coverMiddleware)
    .and(paginationFilter);

  crateRouter.router.match(HTTPMethod.OPTIONS, "/iconsets/:id/*", &categories.options);
  crateRouter.router.match(HTTPMethod.GET, "/iconsets/:id/*", requestErrorHandler(&auth.oAuth2.permisiveAuth));
  crateRouter.router.match(HTTPMethod.GET, "/iconsets/:id/*", requestErrorHandler(&adminRequest.any));
  crateRouter.router.match(HTTPMethod.GET, "/iconsets/:id/*", requestErrorHandler(&userDataMiddleware.any));

  crateRouter.router.match(HTTPMethod.GET, "/iconsets/:id/categories", requestErrorHandler(&categories.handler));
  crateRouter.router.match(HTTPMethod.GET, "/iconsets/:id/subcategories", requestErrorHandler(&subcategories.handler));
}
