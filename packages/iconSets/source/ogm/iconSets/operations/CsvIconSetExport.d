/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.iconSets.operations.CsvIconSetExport;

import crate.http.operations.base;
import ogm.crates.all;
import ogm.models.icon;
import ogm.features.formats.csv;
import crate.base;
import vibe.http.server;
import ogm.markdown;
import vibe.data.json;
import ogm.csv;

class CsvIconSetExport : CrateOperation!DefaultStorage {
  OgmCrates crates;

  this(OgmCrates crates) {
    this.crates = crates;

    CrateRule rule;

    rule.request.path = "/iconsets/:id/csv";
    rule.request.method = HTTPMethod.GET;
    rule.response.statusCode = 200;
    rule.response.mime = "text/csv";
    rule.response.description = "Returns all icons in the set in a csv format.";

    super(crates.iconSet, rule);
  }

  override void handle(DefaultStorage storage) {

    storage.response.statusCode = rule.response.statusCode;
    auto header = new CsvHeader;
    header.add("_id");
    header.add("name");
    header.add("localName");
    header.add("category");
    header.add("subcategory");
    header.add("description");
    header.add("image");
    header.add("otherNames");
    header.reindex;

    storage.response.bodyWriter.write(header.toString);

    auto iconSet = prepareItemOperation!"getItem"(storage);
    auto iconSetId = ObjectId.fromString(storage.request.params["id"]);
    auto range = crates.icon.get.where("iconSet").equal(iconSetId).and.exec;

    foreach (icon; range) {
      auto row = header.toRow;

      row.addField("_id", icon["_id"]);
      row.addField("name", icon["name"]);
      row.addField("localName", icon["localName"]);
      row.addField("category", icon["category"]);
      row.addField("subcategory", icon["subcategory"]);

      if(icon["description"].type == Json.Type.string) {
        row.addField("description", icon["description"]);
      } else {
        row.addField("description", icon["description"].toMarkdown);
      }

      row.addField("image", IconSettings.baseUrl ~ `/icons/` ~ icon["_id"].to!string ~ `/image/value`);
      row.addList("otherNames", icon["otherNames"]);
      storage.response.bodyWriter.write(row.toString);
    }

    storage.response.bodyWriter.finalize;
  }
}