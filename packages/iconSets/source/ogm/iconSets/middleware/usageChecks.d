/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.iconSets.middleware.usageChecks;

import ogm.crates.all;
import ogm.http.request;

import vibe.http.router;
import vibe.data.json;

import crate.base;
import crate.error;
import std.exception;
import std.conv;

class IconSetUsageChecksMiddleware {
  private {
    OgmCrates crates;
  }

  ///
  this(OgmCrates crates) {
    this.crates = crates;
  }

  ///
  @delete_
  void delete_(HTTPServerRequest req) {
    auto iconSetId = req.params["id"];

    auto usageCount = crates.map.get.where("iconSets.list").arrayContains(ObjectId.fromString(iconSetId)).and.size;

    enforce!CrateValidationException(usageCount == 0, "The icon set can not be removed because is used by `" ~ usageCount.to!string ~ "` maps.");
  }
}
