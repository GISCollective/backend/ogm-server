/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.iconSets.middleware.cover;

import ogm.crates.all;
import ogm.http.request;

import vibe.http.router;
import vibe.data.json;

import crate.base;

class CoverMiddleware {
  private {
    OgmCrates crates;
  }

  ///
  this(OgmCrates crates) {
    this.crates = crates;
  }

  ///
  @replace @patch
  void replace(HTTPServerRequest req) {
    if("cover" !in req.json["iconSet"]) {
      return;
    }

    string newCover = req.json["iconSet"]["cover"].to!string;

    auto iconSet = crates.iconSet.getItem(req.params["id"]).exec.front;
    string coverId = iconSet["cover"].to!string;

    if(coverId != newCover) {
      crates.picture.removePicture(coverId);
    }
  }

  ///
  @delete_
  void delete_(HTTPServerRequest req, HTTPServerResponse res) {
    auto iconSet = crates.iconSet.getItem(req.params["id"]).exec.front;
    auto coverId = iconSet["cover"].to!string;

    crates.picture.removePicture(coverId);
  }

  @mapper
  Json mapper(const Json item) @safe {
    Json result = item;

    if("cover" in item && item["cover"] == "") {
      result.remove("cover");
    }

    return result;
  }
}
