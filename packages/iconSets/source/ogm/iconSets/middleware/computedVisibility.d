/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.iconSets.middleware.computedVisibility;

import ogm.crates.all;
import crate.base;
import vibe.data.json;
import vibe.http.server;
import ogm.http.request;

class IconVisibilityMiddleware {
  private {
    OgmCrates crates;
  }

  ///
  this(OgmCrates crates) {
    this.crates = crates;
  }

  @put @patch
  void updateIconsVisibility(HTTPServerRequest req) {
    auto request = RequestUserData(req);

    auto icons = crates.icon.get.where("iconSet").equal(ObjectId(request.itemId)).and.exec;
    auto visibility = req.json["iconSet"]["visibility"];

    foreach(icon; icons) {
      icon["visibility"] = visibility;
      crates.icon.updateItem(icon);
    }
  }
}
