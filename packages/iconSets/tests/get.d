/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.iconsets.get;

import tests.fixtures;
import ogm.iconSets.api;

import std.algorithm;
import std.array;
import ogm.models.icon;

alias suite = Spec!({
  URLRouter router;

  describe("GET", {
    beforeEach({
      setupTestData();
      crates.iconSet = new MemoryCrate!IconSet;

      router = new URLRouter;
      router.crateSetup.setupIconSetApi(crates);

      createIconSet(IconSet(ObjectId.fromString("1"), "name1", Json("description"), Visibility(Team(ObjectId.fromString("1")), true, true)));
      createIconSet(IconSet(ObjectId.fromString("2"), "name2", Json("description"), Visibility(Team(ObjectId.fromString("2")), false)));
      createIconSet(IconSet(ObjectId.fromString("3"), "name3", Json("description"), Visibility(Team(ObjectId.fromString("3")), true)));
      createIconSet(IconSet(ObjectId.fromString("4"), "name4", Json("description"), Visibility(Team(ObjectId.fromString("4")), false)));
    });

    describe("with the ids query param", {
      it("returns only the items that have the requested ids", {
        router
        .request
        .get("/iconsets?ids=000000000000000000000001,000000000000000000000003,000000000000000000000002")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["iconSets"]
            .byValue
            .map!(a => a["_id"].to!string)
            .should
            .equal(["000000000000000000000001", "000000000000000000000003", "000000000000000000000002"]);
        });
      });

      it("returns only the items that the user can access", {
        router
          .request
          .get("/iconsets?ids=000000000000000000000001,000000000000000000000003,000000000000000000000002,000000000000000000000004")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["iconSets"]
              .byValue
              .map!(a => a["_id"].to!string)
              .should
              .equal(["000000000000000000000001", "000000000000000000000003"]);
          });
      });
    });

    describe("with the usableByTeam query param", {
      it("returns the list of default and team sets", {
        router
          .request
          .get("/iconsets?usableByTeam=000000000000000000000002")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["iconSets"]
              .byValue
              .map!(a => a["_id"].to!string)
              .should
              .equal(["000000000000000000000001", "000000000000000000000002"]);
          });
      });

      it("returns the list of default and public team sets when there is no token", {
        createIconSet(IconSet(ObjectId.fromString("5"), "name5", Json("description"), Visibility(Team(ObjectId.fromString("2")), false)));

        router
          .request
          .get("/iconsets?usableByTeam=000000000000000000000003")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["iconSets"]
              .byValue
              .map!(a => a["_id"].to!string)
              .should
              .equal(["000000000000000000000001", "000000000000000000000003"]);
          });
      });

      it("returns the list of default and all team sets when there is team token", {
        createIconSet(IconSet(ObjectId.fromString("5"), "name5", Json("description"), Visibility(Team(ObjectId.fromString("2")), false)));

        router
          .request
          .get("/iconsets?usableByTeam=000000000000000000000002")
          .header("Authorization", "Bearer " ~ userTokenList["owner"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["iconSets"]
              .byValue
              .map!(a => a["_id"].to!string)
              .should
              .equal(["000000000000000000000001", "000000000000000000000002", "000000000000000000000005"]);
          });
      });
    });

    describeCredentialsRule("view public records", "yes", "icon sets", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/iconsets")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          Json[] iconSets = cast(Json[]) response.bodyJson["iconSets"];
          string[] idList = iconSets.map!(a => a["_id"].to!string).array;

          idList.should.contain(["000000000000000000000001", "000000000000000000000003"]);

          if(type != "administrator") {
            idList.should.not.contain("000000000000000000000004");
          }
        });
    });

    describeCredentialsRule("view public records", "yes", "icon sets", "no rights", {
      router
        .request
        .get("/iconsets")
        .expectStatusCode(200)
        .end((Response response) => () {
          Json[] iconSets = cast(Json[]) response.bodyJson["iconSets"];
          string[] idList = iconSets.map!(a => a["_id"].to!string).array;

          idList.should.contain(["000000000000000000000001", "000000000000000000000003"]);
        });
    });

    describeCredentialsRule("view all", "yes", "icon sets", "administrator", {
      router
        .request
        .get("/iconsets?edit=true&all=true")
        .header("Authorization", "Bearer " ~ administratorToken.name)
        .expectStatusCode(200)
        .end((Response response) => () {
          auto iconsets = cast(Json[]) response.bodyJson["iconSets"];
          iconsets.length.should.equal(4);

          Json[] iconSets = cast(Json[]) response.bodyJson["iconSets"];
          string[] idList = iconSets.map!(a => a["_id"].to!string).array;

          idList.should.contain(["000000000000000000000001", "000000000000000000000002", "000000000000000000000003", "000000000000000000000004"]);
        });
    });

    describeCredentialsRule("view all", "no", "icon sets", "owner", {
      router
        .request
        .get("/iconsets?edit=true&all=true")
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto iconsets = cast(Json[]) response.bodyJson["iconSets"];
          iconsets.length.should.equal(2);

          Json[] iconSets = cast(Json[]) response.bodyJson["iconSets"];
          string[] idList = iconSets.map!(a => a["_id"].to!string).array;

          idList.should.equal(["000000000000000000000001", "000000000000000000000002"]);
        });
    });

    describeCredentialsRule("view all", "no", "icon sets", ["leader", "member", "guest"], (string type) {
      router
        .request
        .get("/iconsets?edit=true&all=true")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          Json[] iconSets = cast(Json[]) response.bodyJson["iconSets"];
          string[] idList = iconSets.map!(a => a["_id"].to!string).array;

          idList.length.should.equal(0);
        });
    });

    describeCredentialsRule("view all", "no", "icon sets", "no rights", {
      router
        .request
        .get("/iconsets?edit=true&all=true")
        .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "errors": [
                {
                  "description": "You can't edit items.",
                  "title": "Forbidden",
                  "status": 403
                }
              ]
            }`.parseJsonString);
          });
    });

    describeCredentialsRule("view editable team records", "yes", "icon sets", "administrator", {
      router
        .request
        .get("/iconsets?edit=true")
        .header("Authorization", "Bearer " ~ administratorToken.name)
        .expectStatusCode(200)
        .end((Response response) => () {
          auto iconsets = cast(Json[]) response.bodyJson["iconSets"];
          iconsets.length.should.equal(0);
        });
    });

    describeCredentialsRule("view editable team records", "yes", "icon sets", "owner", {
      router
        .request
        .get("/iconsets?edit=true")
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto iconsets = cast(Json[]) response.bodyJson["iconSets"];
          iconsets.length.should.equal(2);

          Json[] iconSets = cast(Json[]) response.bodyJson["iconSets"];
          string[] idList = iconSets.map!(a => a["_id"].to!string).array;

          idList.should.equal(["000000000000000000000001", "000000000000000000000002"]);
        });
    });

    describeCredentialsRule("view editable team records", "no", "icon sets", ["leader", "member", "guest"], (string type) {
      router
        .request
        .get("/iconsets?edit=true")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          Json[] iconSets = cast(Json[]) response.bodyJson["iconSets"];
          string[] idList = iconSets.map!(a => a["_id"].to!string).array;

          idList.length.should.equal(0);
        });
    });

    describeCredentialsRule("view editable team records", "no", "icon sets", "no rights", {
      router
        .request
        .get("/iconsets?edit=true")
        .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "errors": [
                {
                  "description": "You can't edit items.",
                  "title": "Forbidden",
                  "status": 403
                }
              ]
            }`.parseJsonString);
          });
    });

    describeCredentialsRule("view team records", "yes", "icon sets", "administrator", {
      router
        .request
        .get("/iconsets")
        .header("Authorization", "Bearer " ~ administratorToken.name)
        .expectStatusCode(200)
        .end((Response response) => () {
          auto iconSets = cast(Json[]) response.bodyJson["iconSets"];

          string[] idList = iconSets.map!(a => a["_id"].to!string).array;
          idList.should.contain(["000000000000000000000001","000000000000000000000002","000000000000000000000003","000000000000000000000004"]);
        });
    });

    describeCredentialsRule("view team records", "yes", "icon sets", ["owner", "leader", "member", "guest"], {
      router
        .request
        .get("/iconsets")
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .expectStatusCode(200)
        .end((Response response) => () {
          Json[] iconSets = cast(Json[]) response.bodyJson["iconSets"];
          string[] idList = iconSets.map!(a => a["_id"].to!string).array;

          idList.should.contain(["000000000000000000000001", "000000000000000000000002"]);
        });
    });

    describeCredentialsRule("view team records", "-", "icon sets", "no rights", { });


    describeCredentialsRule("view default records", "yes", "icon sets", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/iconsets?default=true")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          Json[] iconSets = cast(Json[]) response.bodyJson["iconSets"];
          string[] idList = iconSets.map!(a => a["_id"].to!string).array;

          idList.should.equal(["000000000000000000000001"]);
        });
    });

    describeCredentialsRule("view default records", "yes", "icon sets", "no rights", {
      router
        .request
        .get("/iconsets?default=true")
        .expectStatusCode(200)
          .end((Response response) => () {
            Json[] iconSets = cast(Json[]) response.bodyJson["iconSets"];
            string[] idList = iconSets.map!(a => a["_id"].to!string).array;

            idList.should.equal(["000000000000000000000001"]);
          });
    });

    describe("without a token", {
      it("should return an error on getting sets when a token is not provided and the `edit` query string is present", {
        router
          .request
          .get("/iconsets?edit=true")
          .expectStatusCode(403)
            .end((Response response) => () {
              response.bodyJson.should.equal(`{
                "errors": [
                  {
                    "description": "You can't edit items.",
                    "title": "Forbidden",
                    "status": 403
                  }
                ]
              }`.parseJsonString);
            });
      });

      it("should return an error on getting a private set without a token", {
        router
          .request
          .get("/iconsets/000000000000000000000002")
          .expectStatusCode(404)
            .end((Response response) => () {
              response.bodyJson.should.equal("{
                \"errors\": [
                  {
                    \"description\": \"There is no item with id `000000000000000000000002`\",
                    \"title\": \"Crate not found\",
                    \"status\": 404
                  }
                ]
              }".parseJsonString);
            });
      });

      it("should return an error on getting categories of a private set without a token", {
        router
          .request
          .get("/iconsets/000000000000000000000002/categories")
          .expectStatusCode(404)
            .end((Response response) => () {
              response.bodyJson.should.equal("{
                \"errors\": [
                  {
                    \"description\": \"Item `000000000000000000000002` not found.\",
                    \"title\": \"Crate not found\",
                    \"status\": 404
                  }
                ]
              }".parseJsonString);
            });
      });

      it("should return an error on getting subcategories of a private set without a token", {
        router
          .request
          .get("/iconsets/000000000000000000000002/subcategories")
          .expectStatusCode(404)
            .end((Response response) => () {
              response.bodyJson.should.equal("{
                \"errors\": [
                  {
                    \"description\": \"Item `000000000000000000000002` not found.\",
                    \"title\": \"Crate not found\",
                    \"status\": 404
                  }
                ]
              }".parseJsonString);
            });
      });

      it("should return all categories for an icon set", {
        router
          .request
          .get("/iconsets/000000000000000000000001/categories")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"iconSetCategories": [{ "category": "category" }]}`.parseJsonString);
          });
      });

      it("should return all subcategories for an icon set", {
        router
          .request
          .get("/iconsets/000000000000000000000001/subcategories")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"iconSetSubcategories": [{ "subcategory": "subcategory" }]}`.parseJsonString);
          });
      });

      it("should filter by category and return only subcategories for a category icon set", {
        router
          .request
          .get("/iconsets/000000000000000000000001/subcategories?category=other")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"iconSetSubcategories": []}`.parseJsonString);
          });
      });

      describe("when there is an icon set with an icon with no category", {
        beforeEach({
          router = new URLRouter;
          router.crateSetup.setupIconSetApi(crates);

          auto icon = crates.icon.getItem("000000000000000000000001").exec.front;
          icon["category"] = "";

          crates.icon.updateItem(icon);
        });

        it("should return a `not set` string when the icon category is not set", {
          router
            .request
            .get("/iconsets/000000000000000000000001/categories")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal(`{"iconSetCategories": [{ "category": "not set" }]}`.parseJsonString);
            });
        });
      });

      describe("when there is an icon set with an icon with no subcategory", {
        beforeEach({
          router = new URLRouter;
          router.crateSetup.setupIconSetApi(crates);

          auto icon = crates.icon.getItem("000000000000000000000001").exec.front;
          icon["category"] = "category";
          icon["subcategory"] = "";

          crates.icon.updateItem(icon);
        });

        it("should return a `not set` string when the icon subcategory is not set", {
          router
            .request
            .get("/iconsets/000000000000000000000001/subcategories?category=category")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal(`{"iconSetSubcategories": [{ "subcategory": "not set" }]}`.parseJsonString);
            });
        });
      });
    });

    describe("with an invalid token", {
      it("should not return any icon sets", {
        router
          .request
          .get("/iconsets")
          .header("Authorization", "Bearer random")
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"error\": \"Invalid token.\"}".parseJsonString);
          });
      });
    });

    describe("operations", {
      beforeEach({
        IconSettings.baseUrl = "http://localhost/";
      });

      describe("csv", {
        it("can download the icons as a csv", {
          router
          .request
          .get("/iconsets/000000000000000000000001/csv")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyString.should.equal(
              `_id,name,localName,category,subcategory,description,image,otherNames` ~ "\n" ~
              `000000000000000000000001,name1,,category,subcategory,description1,http://localhost//icons/000000000000000000000001/image/value,other name 1` ~ "\n");
          });
        });
      });
    });
  });
});
