/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.iconsets.delete_;

import tests.fixtures;
import ogm.iconSets.api;

import std.algorithm;
import std.array;

alias suite = Spec!({
  URLRouter router;

  describe("DELETE", {

    beforeEach({
      setupTestData();
      crates.iconSet = new MemoryCrate!IconSet;

      router = new URLRouter;
      router.crateSetup.setupIconSetApi(crates);

      createIconSet(IconSet(ObjectId.fromString("1"), "name1", Json("description"), Visibility(Team(ObjectId.fromString("1")), true)));
      createIconSet(IconSet(ObjectId.fromString("2"), "name2", Json("description"), Visibility(Team(ObjectId.fromString("2")), false)));
      createIconSet(IconSet(ObjectId.fromString("3"), "name3", Json("description"), Visibility(Team(ObjectId.fromString("3")), true)));
      createIconSet(IconSet(ObjectId.fromString("4"), "name4", Json("description"), Visibility(Team(ObjectId.fromString("4")), false)));
    });

    it("can't delete an used record", {
      router
        .request
        .delete_("/iconsets/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(400)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{"errors":[
            {"description":"The icon set can not be removed because is used by ` ~ "`4`"~ ` maps.","status":400,"title":"Validation error"}
          ]}`).parseJsonString);
        });
    });

    describeCredentialsRule("delete team records", "yes", "icon sets", ["administrator", "owner"], (string type) {
      router
        .request
        .delete_("/iconsets/000000000000000000000002")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end((Response response) => () {
          crates.iconSet.get.exec
            .filter!(a => a["_id"] == "000000000000000000000002")
              .array.length.should.equal(0);
        });
    });

    describeCredentialsRule("delete team records", "no", "icon sets", ["leader", "member", "guest"], (string type) {
      router
        .request
        .delete_("/iconsets/000000000000000000000002")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          crates.iconSet.get.exec
            .filter!(a => a["_id"] == "000000000000000000000002")
              .array.length.should.equal(1);

          response.bodyJson.should.equal("{
              \"errors\": [{
                  \"description\": \"You don't have enough rights to remove `000000000000000000000002`.\",
                  \"title\": \"Forbidden\",
                  \"status\": 403
                }]}".parseJsonString);
        });
    });

    describeCredentialsRule("delete team records", "-", "icon sets", "no rights", {
      router
        .request
        .delete_("/iconsets/000000000000000000000002")
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "error": "Authorization required"
          }`.parseJsonString);
        });
    });


    describeCredentialsRule("delete other team records", "yes", "icon sets", "administrator", {
      router
        .request
        .delete_("/iconsets/000000000000000000000003")
        .header("Authorization", "Bearer " ~ administratorToken.name)
        .expectStatusCode(204)
        .end((Response response) => () {
          crates.iconSet.get.exec
            .filter!(a => a["_id"] == "000000000000000000000003")
              .array.length.should.equal(0);
        });
    });

    describeCredentialsRule("delete other team records", "no", "icon sets", ["owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .delete_("/iconsets/000000000000000000000003")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(404)
          .end((Response response) => () {
            response.bodyJson.should.equal("{
              \"errors\": [
                {
                  \"description\": \"Item `000000000000000000000003` not found.\",
                  \"title\": \"Crate not found\",
                  \"status\": 404
                }
              ]
            }".parseJsonString);

            crates.iconSet.get.exec
            .filter!(a => a["_id"] == "000000000000000000000003")
              .array.length.should.equal(1);
          });
    });

    describeCredentialsRule("delete other team records", "-", "icon sets", "no rights", {
      router
        .request
        .delete_("/iconsets/000000000000000000000003")
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "error": "Authorization required"
          }`.parseJsonString);
        });
    });

    describe("with an administrator token", {
      describe("for an icon set with a custom cover", {
        beforeEach({
          Picture customCover;
          customCover.name = "custom cover";
          customCover.owner = "";
          customCover.picture = new PictureFile;
          auto result = crates.picture.addItem(customCover.serializeToJson);
          customCover._id = ObjectId.fromString(result["_id"].to!string);

          auto iconSet = crates.iconSet.getItem("000000000000000000000003").exec.front;
          iconSet["cover"] = result["_id"];
          crates.iconSet.updateItem(iconSet);
        });

        it("should delete the cover map", {
          router
            .request
            .delete_("/iconsets/000000000000000000000003")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(204)
            .end((Response response) => () {
              crates.picture.get.exec
                .filter!(a => a["name"] == "custom cover")
                  .array.length.should.equal(0);
            });
        });
      });

      describe("for a icon set with a default cover", {
        Json result;

        beforeEach({
          Picture customCover;
          customCover.name = "system cover";
          customCover.owner = "@system";
          customCover.picture = new PictureFile;
          result = crates.picture.addItem(customCover.serializeToJson);
          customCover._id = ObjectId.fromString(result["_id"].to!string);

          auto iconSet = crates.iconSet.getItem("000000000000000000000003").exec.front;

          iconSet["cover"] = result["_id"];
          crates.iconSet.updateItem(iconSet);
        });

        it("should not delete the cover map", {
          router
            .request
            .delete_("/iconsets/000000000000000000000003")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(204)
            .end((Response response) => () {
              crates.picture.get.exec
                .filter!(a => a["_id"] == result["_id"])
                  .array.length.should.equal(1);
            });
        });
      });
    });
  });
});
