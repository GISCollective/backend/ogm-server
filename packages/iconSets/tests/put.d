/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.iconsets.put;

import tests.fixtures;
import ogm.iconSets.api;

import std.algorithm;
import std.array;

alias suite = Spec!({
  URLRouter router;
  Json data;

  describe("PUT", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      crates.iconSet = new MemoryCrate!IconSet;

      router.crateSetup.setupIconSetApi(crates);

      createIconSet(IconSet(ObjectId.fromString("1"), "name1", Json("description"), Visibility(Team(ObjectId.fromString("1")), true)));
      createIconSet(IconSet(ObjectId.fromString("2"), "name2", Json("description"), Visibility(Team(ObjectId.fromString("2")), false)));
      createIconSet(IconSet(ObjectId.fromString("3"), "name3", Json("description"), Visibility(Team(ObjectId.fromString("3")), true)));
      createIconSet(IconSet(ObjectId.fromString("4"), "name4", Json("description"), Visibility(Team(ObjectId.fromString("4")), false)));

      data = `{ "iconSet": {
        "description": "new description",
        "name": "new name",
        "visibility": {
          "isPublic": true,
          "team": "000000000000000000000001"
        }
        }}`.parseJsonString;
    });

    describeCredentialsRule("edit team records", "yes", "icon sets", ["administrator", "owner"], (string type) {
      router
        .request
        .put("/iconsets/000000000000000000000002")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "iconSet": {
            "description": "new description",
            "_id": "000000000000000000000002",
            "name": "new name",
            "canEdit": true,
            "ver": 0,
            "sprites": {},
            "visibility": {
              "isPublic": true,
              "team": "000000000000000000000001",
              "isDefault":false
            },
            "source": {
              "type": "",
              "remoteId": "",
              "modelId": "",
              "syncAt": "0001-01-01T00:00:00+00:00"
            },
            "styles": {
              "lineMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
              "polygonMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
              "polygon": { "backgroundColor": "rgba(255,255,255,0.6)", "hideBackgroundOnZoom": false, "showAsLineAfterZoom": 99, "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
              "site": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
              "lineLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
              "line": { "markerInterval": 700, "backgroundColor": "rgba(255,255,255,0.6)", "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
              "siteLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
              "polygonLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 }
          }}}`.parseJsonString);
        });
    });

    describeCredentialsRule("edit team records", "no", "icon sets", ["leader", "member", "guest"], (string type) {
      router
        .request
        .put("/iconsets/000000000000000000000002")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(data)
        .expectStatusCode(403)
        .end((Response response) => () {
            response.bodyJson.should.equal("{
              \"errors\": [
                {
                  \"description\": \"You don't have enough rights to edit `000000000000000000000002`.\",
                  \"title\": \"Forbidden\",
                  \"status\": 403
                }
              ]
            }".parseJsonString);
        });
    });

    describeCredentialsRule("edit team records", "-", "icon sets", "no rights", {
      router
        .request
        .put("/iconsets/000000000000000000000002")
        .send(data)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "error": "Authorization required"
          }`.parseJsonString);
        });
    });


    describeCredentialsRule("edit team other team records", "yes", "icon sets", "administrator", (string type) {
      router
        .request
        .put("/iconsets/000000000000000000000004")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "iconSet": {
              "description": "new description",
              "_id": "000000000000000000000004",
              "name": "new name",
              "canEdit": true,
              "sprites": {},
              "ver": 0,
              "source": {
                "type": "",
                "remoteId": "",
                "modelId": "",
                "syncAt": "0001-01-01T00:00:00+00:00"
              },
              "visibility": {
                "isPublic": true,
                "team": "000000000000000000000001",
                "isDefault":false
              },
              "styles": {
                "lineMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                "polygonMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                "polygon": { "backgroundColor": "rgba(255,255,255,0.6)", "hideBackgroundOnZoom": false, "showAsLineAfterZoom": 99, "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
                "site": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                "lineLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
                "line": { "markerInterval": 700, "backgroundColor": "rgba(255,255,255,0.6)", "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
                "siteLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
                "polygonLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 }
            }}}`.parseJsonString);
          });
    });

    describeCredentialsRule("edit team other team records", "no", "icon sets", ["owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .put("/iconsets/000000000000000000000004")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(data)
        .expectStatusCode(404)
        .end((Response response) => () {
            response.bodyJson.should.equal("{
              \"errors\": [
                {
                  \"description\": \"Item `000000000000000000000004` not found.\",
                  \"title\": \"Crate not found\",
                  \"status\": 404
                }
              ]
            }".parseJsonString);
        });
    });

    describeCredentialsRule("edit team other team records", "-", "icon sets", "no rights", {
      router
        .request
        .put("/iconsets/000000000000000000000004")
        .send(data)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "error": "Authorization required"
          }`.parseJsonString);
        });
    });


    describe("a request with an administrator token", {
      it("should change the default state of a set", {
        data["iconSet"]["visibility"]["isDefault"] = true;

        router
          .request
          .put("/iconsets/000000000000000000000003")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{ "iconSet": {
              "_id": "000000000000000000000003",
              "description": "new description",
              "name": "new name",
              "canEdit": true,
              "ver": 0,
              "sprites": {},
              "visibility": {
                "isPublic": true,
                "isDefault": true,
                "team": "000000000000000000000001"
              },
              "source": {
                "type": "",
                "remoteId": "",
                "modelId": "",
                "syncAt": "0001-01-01T00:00:00+00:00"
              },
              "styles": {
                "lineMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                "polygonMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                "polygon": { "backgroundColor": "rgba(255,255,255,0.6)", "hideBackgroundOnZoom": false, "showAsLineAfterZoom": 99, "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
                "site": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                "lineLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
                "line": { "markerInterval": 700, "backgroundColor": "rgba(255,255,255,0.6)", "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
                "siteLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
                "polygonLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 }
            }}}`.parseJsonString);
          });
      });

      describe("for a base map with a custom cover", {
        Json result;

        beforeEach({
          Picture customCover;
          customCover.name = "custom cover";
          customCover.owner = "";
          customCover.picture = new PictureFile;
          result = crates.picture.addItem(customCover.serializeToJson);
          customCover._id = ObjectId.fromString(result["_id"].to!string);

          auto iconSet = crates.iconSet.getItem("000000000000000000000003").exec.front;
          iconSet["cover"] = result["_id"];
          crates.iconSet.updateItem(iconSet);
        });

        it("should delete the cover map", {
          data["iconSet"]["cover"] = "000000000000000000000001";

          router
            .request
            .put("/iconsets/000000000000000000000003")
            .send(data)
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              crates.picture.get.exec
                .filter!(a => a["_id"] == result["_id"])
                  .array.length.should.equal(0);
            });
        });
      });

      describe("for a base map with a default cover", {
        Json result;

        beforeEach({
          Picture customCover;
          customCover.name = "system cover";
          customCover.owner = "@system";
          customCover.picture = new PictureFile;
          result = crates.picture.addItem(customCover.serializeToJson);
          customCover._id = ObjectId.fromString(result["_id"].to!string);

          auto iconSet = crates.iconSet.getItem("000000000000000000000003").exec.front;

          iconSet["cover"] = result["_id"];
          crates.iconSet.updateItem(iconSet);
        });

        it("should not delete the cover map", {
          data["iconSet"]["cover"] = "000000000000000000000001";

          router
            .request
            .put("/iconsets/000000000000000000000001")
            .send(data)
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              crates.picture.get.exec
                .filter!(a => a["_id"] == result["_id"])
                  .array.length.should.equal(1);
            });
        });
      });
    });

    describe("a request with an owner token", {
      it("should not change the default state of a set", {
        data["iconSet"]["visibility"]["isDefault"] = true;

        router
          .request
          .put("/iconsets/000000000000000000000002")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .send(data)
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal((`{"errors": [{
              "description": "You don't have enough rights to change the ` ~ "`isDefault`" ~ ` flag.",
              "status": 403,
              "title": "Forbidden"
            }]}`).parseJsonString);
          });
      });

      it("updates the visibility of icons from the set", {
        data["iconSet"]["visibility"]["isPublic"] = true;

        router
          .request
          .put("/iconsets/000000000000000000000002")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            auto icon = crates.icon.get
              .where("iconSet").equal("000000000000000000000002")
              .and.exec.front;

            expect(icon["visibility"]).to.equal(`{
              "isDefault": false,
              "isPublic": true,
              "team": "000000000000000000000001"
            }`.parseJsonString);
          });
      });
    });
  });
});
