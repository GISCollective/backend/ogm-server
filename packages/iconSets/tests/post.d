/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.iconsets.post;

import tests.fixtures;
import ogm.iconSets.api;

import std.algorithm;
import std.array;

alias suite = Spec!({
  URLRouter router;
  Json data;
  Json dataOtherTeam;

  describe("POST", {
    beforeEach({
      setupTestData();
      crates.iconSet = new MemoryCrate!IconSet;

      router = new URLRouter;
      router.crateSetup.setupIconSetApi(crates);

      data = `{"iconSet":{
              "description": "new description",
              "name": "new name",
              "visibility": {
                "isPublic": true,
                "team":"000000000000000000000001"
              }
            }}`.parseJsonString;

      dataOtherTeam = `{"iconSet": {
              "description": "new description",
              "name": "new name",
              "visibility": {
                "isPublic": true,
                "team":"000000000000000000000004"
              }
      }}`.parseJsonString;
    });

    describeCredentialsRule("create for team", "yes", "icon sets", ["administrator", "owner"], (string type) {
      router
        .request
        .post("/iconsets")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"iconSet": {
              "_id": "000000000000000000000001",
              "description": "new description",
              "name": "new name",
              "canEdit": true,
              "ver": 0,
              "sprites": {},
              "source": {
                "type": "",
                "remoteId": "",
                "modelId": "",
                "syncAt": "0001-01-01T00:00:00+00:00"
              },
              "visibility": {
                "isPublic": true,
                "isDefault":false,
                "team":"000000000000000000000001"
              },
              "styles": {
                "lineMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                "polygonMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                "polygon": { "backgroundColor": "rgba(255,255,255,0.6)", "hideBackgroundOnZoom": false, "showAsLineAfterZoom": 99, "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
                "site": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                "lineLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
                "line": { "markerInterval": 700, "backgroundColor": "rgba(255,255,255,0.6)", "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
                "siteLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
                "polygonLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 }
            } } }`.parseJsonString);
          });
    });

    describeCredentialsRule("create for team", "no", "icon sets", ["leader", "member", "guest"], (string type) {
      router
        .request
        .post("/iconsets")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(data)
        .expectStatusCode(403)
        .end((Response response) => () {
            response.bodyJson.should.equal("{
              \"errors\": [
                {
                  \"description\": \"You don't have enough rights to add an item.\",
                  \"title\": \"Forbidden\",
                  \"status\": 403
                }
              ]
            }".parseJsonString);
        });
    });

    describeCredentialsRule("create for team", "-", "icon sets", "no rights", {
      router
        .request
        .post("/iconsets")
        .send(data)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "error": "Authorization required"
          }`.parseJsonString);
        });
    });

    describeCredentialsRule("create for other team", "yes", "icon sets", "administrator", {
      router
        .request
        .post("/iconsets")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .send(dataOtherTeam)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"iconSet": {
              "_id": "000000000000000000000001",
              "description": "new description",
              "name": "new name",
              "canEdit": true,
              "sprites": {},
              "ver": 0,
              "source": {
                "type": "",
                "remoteId": "",
                "modelId": "",
                "syncAt": "0001-01-01T00:00:00+00:00"
              },
              "visibility": {
                "isPublic": true,
                "isDefault":false,
                "team":"000000000000000000000004"
              },
              "styles": {
                  "lineMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                  "polygonMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                  "polygon": { "backgroundColor": "rgba(255,255,255,0.6)", "hideBackgroundOnZoom": false, "showAsLineAfterZoom": 99, "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
                  "site": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                  "lineLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
                  "line": { "markerInterval": 700, "backgroundColor": "rgba(255,255,255,0.6)", "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
                  "siteLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
                  "polygonLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 }
                }}}`.parseJsonString);
          });
    });

    describeCredentialsRule("create for other team", "no", "icon sets", ["owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .post("/iconsets")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(dataOtherTeam)
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal("{
            \"errors\": [
              {
                \"description\": \"You can't add an item for the team `000000000000000000000004`.\",
                \"title\": \"Forbidden\",
                \"status\": 403
              }
            ]
          }".parseJsonString);
        });
    });

    describeCredentialsRule("create for other team", "-", "icon sets", "no rights", {
      router
        .request
        .post("/iconsets")
        .send(dataOtherTeam)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "error": "Authorization required"
          }`.parseJsonString);
        });
    });

    describe("a request without token", {
      it("should return an error", {
        router
          .request
          .post("/iconsets")
          .send(data)
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "error": "Authorization required"
            }`.parseJsonString);
          });
      });
    });

    describe("a request with an administrator token", {
      it("should add an icon set with the default flag", {
        data["iconSet"]["visibility"]["isDefault"] = true;

        router
          .request
          .post("/iconsets")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"iconSet": {
              "_id": "000000000000000000000001",
              "description": "new description",
              "name": "new name",
              "canEdit": true,
              "ver": 0,
              "sprites": {},
              "source": {
                "type": "",
                "remoteId": "",
                "modelId": "",
                "syncAt": "0001-01-01T00:00:00+00:00"
              },
              "visibility": {
                "isPublic": true,
                "isDefault": true,
                "team":"000000000000000000000001"
              },
              "styles": {
              "lineMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                "polygonMarker": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                "polygon": { "backgroundColor": "rgba(255,255,255,0.6)", "hideBackgroundOnZoom": false, "showAsLineAfterZoom": 99, "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
                "site": { "backgroundColor": "rgba(255,255,255,0.6)", "isVisible": true, "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#fff" },
                "lineLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
                "line": { "markerInterval": 700, "backgroundColor": "rgba(255,255,255,0.6)", "lineDash": [], "borderWidth": 1, "borderColor": "#fff" },
                "siteLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 },
                "polygonLabel": { "align_": "center", "offsetX": 0, "isVisible": false, "text": "wrap", "size": 10, "lineHeight": 1, "color": "#000", "borderColor": "#fff", "baseline": "bottom", "offsetY": 0, "weight": "bold", "borderWidth": 2 }
            }}}`.parseJsonString);
          });
      });
    });

    describe("a request with an owner token", {
      it("should not add an icon set with the default flag", {
        data["iconSet"]["visibility"]["isDefault"] = true;

        router
          .request
          .post("/iconsets")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .send(data)
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal("{
              \"errors\": [
                {
                  \"description\": \"You don't have enough rights to add a default item.\",
                  \"title\": \"Forbidden\",
                  \"status\": 403
                }
              ]
            }".parseJsonString);
          });
      });
    });
  });
});
