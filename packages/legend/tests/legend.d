/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.legend;

import tests.fixtures;
import ogm.legend.api;

alias suite = Spec!({
  describe("GET", {
    URLRouter router;
    before({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupLegendApi(crates);
    });

    describe("The legend", {
      it("should return an error if no query is provided", {
        router
          .request
          .get("/legends")
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "errors": [{
                  "description": "You must provide at least one query selector.",
                  "title": "Validation error",
                  "status": 400
                }]
              }`.parseJsonString);
          });
      });

      it("should select items from a viewbox", {
        router
          .request
          .get("/legends?viewbox=1%2C1%2C2%2C2")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"legends": [
              {"_id": 1, "name": "category", "count": 2,"subcategories": [
                { "count": 2, "uid": "category.subcategory", "icons": [{ "icon": "000000000000000000000001", "uid": "000000000000000000000001", "count": 2 }],"name": "subcategory"}
              ]},
              {"_id": 2, "name": "category2", "count": 1,"subcategories": [
                { "count": 1, "uid": "category2.subcategory2", "icons": [{ "icon": "000000000000000000000002", "uid": "000000000000000000000002", "count": 1 }],"name": "subcategory2"}
              ]}]}`
              .parseJsonString);
          });
      });

      describe("When there is a site with an invalid icon", {
        beforeEach({
          auto features = crates.feature.get.exec().array;

          foreach(feature; features) {
            feature["icons"] = `["440000000000000000000001"]`.parseJsonString;
            crates.feature.updateItem(feature);
          }
        });

        it("should ignore the site", {
          router
            .request
            .get("/legends?viewbox=0%2C0%2C2%2C2")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal(`{"legends": []}`
                .parseJsonString);
            });
        });
      });
    });
  });
});
