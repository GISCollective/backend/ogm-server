/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.legend.api;

import vibe.http.router;
import crate.http.router;
import crate.base;

import ogm.auth;
import ogm.crates.all;

import ogm.middleware.userdata;
import ogm.filter.pagination;
import ogm.filter.indirectvisibility;
import ogm.middleware.adminrequest;
import ogm.middleware.features.GetFeaturesFilter;
import ogm.middleware.features.SiteSubsetFilter;
import ogm.icons.TaxonomyTree;
import ogm.icons.IconCache;

///
void setupLegendApi(T)(CrateRouter!T crateRouter, OgmCrates crates) {

  auto cache = new IconCache(&crates.icon.get, &crates.iconSet.getItem);
  auto iconTree = new TaxonomyTree(&crates.icon.get, &crates.iconSet.getItem, cache);

  auto auth = Authentication.instance(crates.user);
  auto userDataMiddleware = UserDataMiddleware.instance(crates);
  auto adminRequest = new AdminRequest(crates.user);
  auto visibility = new IndirectVisibility(crates, crates.feature);
  auto featuresFilter = new GetFeaturesFilter(auth.getCollection, crates, iconTree);
  auto siteSubsetFilter = new SiteSubsetFilter();

  crateGetters["IconSet"] = &crates.iconSet.getItem;
  crateGetters["Team"] = &crates.team.getItem;
  crateGetters["Picture"] = &crates.picture.getItem;

  crateRouter
    .add(crates.legend,
      auth.publicDataMiddleware,
      adminRequest,
      userDataMiddleware,
      visibility,
      featuresFilter,
      siteSubsetFilter);
}
