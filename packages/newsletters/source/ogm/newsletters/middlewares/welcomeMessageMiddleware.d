module ogm.newsletters.middlewares.welcomeMessageMiddleware;


import crate.base;
import crate.error;
import std.algorithm;
import std.array;
import ogm.crates.all;
import vibe.data.json;
import vibe.http.router;

class WelcomeMessageMiddleware {
  OgmCrates crates;

  this(OgmCrates crates) {
    this.crates = crates;
  }

  void createWelcomeMessage(string id, Json info, Json visibility) {
    auto article = crates.article.get.where("type").equal(ArticleType.templateNewsletterWelcomeMessage).and.exec.front.clone;

    article["type"] = ArticleType.newsletterWelcomeMessage;
    article["relatedId"] = id;
    article["slug"] = ArticleType.newsletterWelcomeMessage ~ "-" ~ id;
    article["info"] = info;
    article["visibility"] = visibility;

    article.remove("_id");

    crates.article.addItem(article);
  }

  @mapper
  checkWelcomeMessage(Json item) {
    string id = item["_id"].to!string;

    auto range = getWelcomeMessageRange(crates, id);

    if(range.empty) {
      createWelcomeMessage(id, item["info"], item["visibility"]);
      range = getWelcomeMessageRange(crates, id);
    }

    item["welcomeMessage"] = range.front["_id"];
  }

  @delete_
  void deleteWelcomeMessage(HTTPServerRequest req) {
    auto newsletterId = req.params["id"];
    auto range = getWelcomeMessageRange(crates, newsletterId);

    foreach(Json record; range) {
      crates.article.deleteItem(record["_id"].to!string);
    }
  }
}