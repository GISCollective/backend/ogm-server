module ogm.newsletters.middlewares.deleteNewsletterMiddleware;

import crate.base;
import crate.error;
import std.algorithm;
import std.array;
import ogm.crates.all;
import vibe.data.json;
import vibe.http.router;

class DeleteNewsletterMiddleware {
  OgmCrates crates;

  this(OgmCrates crates) {
    this.crates = crates;
  }

  ///
  @delete_
  void deleteMatchingEmails(HTTPServerRequest req) {
    auto newsletterId = req.params["id"];
    auto _id = ObjectId.fromString(newsletterId);
    auto emailRange = crates.newsletterEmail.get.where("list").arrayContains(_id).and.exec;

    foreach(emailRecord; emailRange) {
      checkRecord(emailRecord, newsletterId);
    }
  }

  void checkRecord(ref Json record, ref string newsletterId) {
    record["list"] = record["list"].byValue.filter!(a => a != newsletterId).array;

    if(record["list"].length == 0) {
      crates.newsletterEmail.deleteItem(record["_id"].to!string);
    } else {
      crates.newsletterEmail.updateItem(record);
    }
  }
}