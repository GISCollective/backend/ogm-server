module ogm.newsletters.actions.unsubscribe;

import crate.base;
import crate.error;
import crate.http.operations.base;
import crate.http.operations.getItem;
import gis_collective.hmq.broadcast.base;
import ogm.crates.all;
import std.datetime;
import std.exception;
import std.algorithm;
import std.array;
import std.net.isemail;
import std.typecons : Yes, No;
import vibe.core.core;
import vibe.data.json;
import vibe.http.router;

class NewsletterUnsubscribeOperation : CrateOperation!DefaultStorage {
  OgmCrates crates;

  this(OgmCrates crates) {
    CrateRule rule;

    rule.request.path = "/newsletters/:id/unsubscribe";
    rule.request.method = HTTPMethod.POST;

    this.crates = crates;

    super(crates.newsletter, rule);
  }

  override void handle(DefaultStorage storage) {
    auto item = this.prepareItemOperation!"getItem"(storage);
    auto res = storage.response;

    if(storage.response.headerWritten) {
      return;
    }

    this.applyRule(storage);

    string email = storage.request.json["email"].to!string;
    auto query = crates.newsletterEmail.get;

    if(email.isObjectId) {
      query.where("_id").equal(ObjectId.fromString(email)).and.exec;
    } else {
      auto emailStatus = isEmail(email, No.checkDns, EmailStatusCode.any);
      enforce!CrateValidationException(emailStatus.valid, emailStatus.status);

      query.where("email").equal(email).and.exec;
    }

    auto recordRange = query.exec;

    if(!recordRange.empty) {
      Json record = recordRange.front;

      record["list"] = record["list"].byValue.filter!(a => a["newsletter"] != item["_id"]).array;

      if(record["list"].length == 0) {
        crates.newsletterEmail.deleteItem(record["_id"].to!string);
      } else {
        crates.newsletterEmail.updateItem(record);
      }
    }

    res.statusCode = 201;
    res.writeVoidBody;
  }
}