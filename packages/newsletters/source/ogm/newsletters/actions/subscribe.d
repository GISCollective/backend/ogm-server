module ogm.newsletters.actions.subscribe;

import crate.base;
import crate.error;
import crate.http.operations.base;
import crate.http.operations.getItem;
import gis_collective.hmq.broadcast.base;
import ogm.calendar;
import ogm.crates.all;
import ogm.newsletter;
import std.algorithm;
import std.datetime;
import std.exception;
import std.net.isemail;
import std.typecons : Yes, No;
import vibe.core.core;
import vibe.data.json;
import vibe.http.router;

class NewsletterSubscribeOperation : CrateOperation!DefaultStorage {
  IBroadcast broadcast;

  OgmCrates crates;

  this(OgmCrates crates, IBroadcast broadcast) {
    this.broadcast = broadcast;
    CrateRule rule;

    rule.request.path = "/newsletters/:id/subscribe";
    rule.request.method = HTTPMethod.POST;

    this.crates = crates;

    super(crates.newsletter, rule);
  }

  override void handle(DefaultStorage storage) {
    auto item = this.prepareItemOperation!"getItem"(storage);
    auto res = storage.response;

    if(storage.response.headerWritten) {
      return;
    }

    this.applyRule(storage);

    string email = storage.request.json["email"].to!string;

    subscribe(email, item["_id"].to!string, crates, broadcast);

    res.statusCode = 201;
    res.writeVoidBody;
  }
}
