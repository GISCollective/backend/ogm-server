module ogm.newsletters.actions.exportEmails;

import crate.base;
import crate.error;
import crate.http.operations.base;
import crate.http.operations.getItem;
import gis_collective.hmq.broadcast.base;
import ogm.calendar;
import ogm.crates.all;
import ogm.http.request;
import std.algorithm;
import std.array;
import std.datetime;
import std.conv;
import std.exception;
import std.net.isemail;
import std.typecons : Yes, No;
import vibe.core.core;
import vibe.data.json;
import vibe.http.router;

class NewsletterExportEmailsOperation : CrateOperation!DefaultStorage {
  OgmCrates crates;

  this(OgmCrates crates) {
    CrateRule rule;

    rule.request.path = "/newsletters/:id/export-emails";
    rule.request.method = HTTPMethod.GET;

    this.crates = crates;

    super(crates.newsletter, rule);
  }

  size_t added;
  size_t existing;
  size_t invalid;

  override void handle(DefaultStorage storage) {
    auto item = this.prepareItemOperation!"getItem"(storage);
    auto res = storage.response;

    if(storage.response.headerWritten) {
      return;
    }

    this.applyRule(storage);

    auto request = RequestUserData(storage.request);

    if(!request.isAdmin) {
      enforce!UnauthorizedException(request.isAuthenticated, "You must authenticate to do this.");

      auto session = request.session(crates);
      auto allNewsletters = session.owner.newsletters ~ session.leader.newsletters;

      enforce!UnauthorizedException(allNewsletters.canFind(item["_id"].to!string), "You must be an owner or leader to do this.");
    }

    res.contentType = "text/csv";
    res.statusCode = 200;

    res.bodyWriter.write("email,date added,read messages,links opened\n");

    auto newsletterId = item["_id"].to!string;

    auto query = crates.newsletterEmail.get;
    query.or.where("list").arrayFieldContains("newsletter", newsletterId);
    query.or.where("list").arrayFieldContains("newsletter", ObjectId(newsletterId));

    auto recordRange = query.and.exec;

    size_t readMessages;
    size_t linksOpened;

    foreach(record; recordRange) {
      auto email = record["email"].to!string;
      auto listItem = record["list"].byValue.find!(a => a["newsletter"] == newsletterId).front;

      readMessages = 0;
      linksOpened = 0;

      auto metricsRange = crates.metric.get
        .where("type").equal("articleView").and
        .where("labels.email").equal(email).and
        .where("labels.relatedId").equal(newsletterId).and
        .exec;

      foreach(metric; metricsRange) {
        readMessages += metric["value"].to!size_t;
      }

      metricsRange = crates.metric.get
        .where("type").equal("articleInteraction").and
        .where("labels.email").equal(email).and
        .where("labels.relatedId").equal(newsletterId).and
        .exec;

      foreach(metric; metricsRange) {
        linksOpened += metric["value"].to!size_t;
      }

      res.bodyWriter.write(email);
      res.bodyWriter.write(",");
      res.bodyWriter.write(listItem["joinedOn"].to!string);
      res.bodyWriter.write(",");
      res.bodyWriter.write(readMessages.to!string);
      res.bodyWriter.write(",");
      res.bodyWriter.write(linksOpened.to!string);
      res.bodyWriter.write("\n");
    }

    res.bodyWriter.finalize;
  }
}