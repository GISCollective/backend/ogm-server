module ogm.newsletters.actions.importEmails;

import crate.base;
import crate.error;
import crate.http.operations.base;
import crate.http.operations.getItem;
import gis_collective.hmq.broadcast.base;
import ogm.calendar;
import ogm.crates.all;
import std.algorithm;
import std.array;
import std.datetime;
import std.string;
import std.exception;
import std.net.isemail;
import std.typecons : Yes, No;
import vibe.core.core;
import vibe.data.json;
import vibe.http.router;
import ogm.http.request;

enum SubscribeResult {
  added = 1,
  existing = 2,
  invalid = 3
}

void unsubscribeAll(string newsletterId, OgmCrates crates) {
  auto query = crates.newsletterEmail.get;

  query.or.where("list").arrayFieldContains("newsletter", newsletterId);
  query.or.where("list").arrayFieldContains("newsletter", ObjectId(newsletterId));

  auto recordRange = query.and.exec;

  foreach(record; recordRange) {
    record["list"] = record["list"].byValue.filter!(a => a["newsletter"] != newsletterId).array;
    crates.newsletterEmail.updateItem(record);
  }
}

SubscribeResult subscribe(string email, Json newsletterId, OgmCrates crates) {
  auto emailStatus = isEmail(email, No.checkDns, EmailStatusCode.any);

  if(!emailStatus.valid) {
    return SubscribeResult.invalid;
  }

  auto recordRange = crates.newsletterEmail.get.where("email").equal(email).and.exec;
  Json record;

  if(recordRange.empty) {
    record = Json.emptyObject;
    record["email"] = email;
    record["list"] = Json.emptyArray;

    record = crates.newsletterEmail.addItem(record);
  } else {
    record = recordRange.front;
  }

  if(record["list"].byValue.map!(a => a["newsletter"]).canFind(newsletterId)) {
    return SubscribeResult.existing;
  }

  auto subscription = Json.emptyObject;

  subscription["newsletter"] = newsletterId;
  subscription["joinedOn"] = SysCalendar.instance.now.toISOExtString;

  record["list"].appendArrayElement(subscription);

  crates.newsletterEmail.updateItem(record);

  return SubscribeResult.added;
}

class NewsletterImportEmailsOperation : CrateOperation!DefaultStorage {
  OgmCrates crates;

  this(OgmCrates crates) {
    CrateRule rule;

    rule.request.path = "/newsletters/:id/import-emails";
    rule.request.method = HTTPMethod.POST;

    this.crates = crates;

    super(crates.newsletter, rule);
  }

  size_t added;
  size_t existing;
  size_t invalid;

  override void handle(DefaultStorage storage) {
    auto item = this.prepareItemOperation!"getItem"(storage);
    auto res = storage.response;

    if(storage.response.headerWritten) {
      return;
    }

    this.applyRule(storage);

    auto request = RequestUserData(storage.request);

    if(!request.isAdmin) {
      enforce!UnauthorizedException(request.isAuthenticated, "You must authenticate to do this.");

      auto session = request.session(crates);
      auto allNewsletters = session.owner.newsletters ~ session.leader.newsletters;

      enforce!UnauthorizedException(allNewsletters.canFind(item["_id"].to!string), "You must be an owner or leader to do this.");
    }

    string[] emails;

    if(storage.request.json["emails"].type == Json.Type.array) {
      emails = storage.request.json["emails"].deserializeJson!(string[]);
    }

    if(storage.request.json["emails"].type == Json.Type.string) {
      emails = storage.request.json["emails"].to!string.splitter!("a == ',' || a == ';' || a == ' ' || a == '\\n' || a == '\\t'")(' ').array;
    }

    if(storage.request.json["replace"] == true) {
      unsubscribeAll(item["_id"].to!string, crates);
    }

    foreach(email; emails) {
      auto niceEmail = email.strip.toLower.replace("'", "").replace(`"`, "");

      if(niceEmail == "") {
        continue;
      }

      auto result = subscribe(niceEmail, item["_id"], crates);

      if(result == SubscribeResult.added) {
        added++;
      }

      if(result == SubscribeResult.existing) {
        existing++;
      }

      if(result == SubscribeResult.invalid) {
        invalid++;
      }
    }

    auto stats = Json.emptyObject;
    stats["emailImportStats"] = Json.emptyObject;
    stats["emailImportStats"]["added"] = added;
    stats["emailImportStats"]["existing"] = existing;
    stats["emailImportStats"]["invalid"] = invalid;

    res.statusCode = 200;
    res.writeJsonBody(stats);
  }
}