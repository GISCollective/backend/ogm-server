/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.newsletters.api;

import vibe.http.router;
import vibe.core.log;
import vibe.data.json;
import vibe.service.configuration.general;

import crate.http.router;
import crate.base;
import ogm.defaults.pages;
import ogm.defaults.spaces;

import ogm.auth;
import ogm.crates.all;

import ogm.middleware.adminrequest;
import ogm.middleware.userdata;
import ogm.middleware.modelinfo;
import ogm.middleware.creationRightsFlag;
import ogm.filter.visibility;
import gis_collective.hmq.broadcast.base;
import ogm.newsletters.actions.subscribe;
import ogm.newsletters.actions.unsubscribe;
import ogm.newsletters.middlewares.deleteNewsletterMiddleware;
import ogm.newsletters.middlewares.welcomeMessageMiddleware;
import ogm.newsletters.actions.importEmails;
import ogm.newsletters.actions.exportEmails;

///
void setupNewsletterApi(T)(CrateRouter!T crateRouter, OgmCrates crates, IBroadcast broadcast) {
  crateGetters["Team"] = &crates.team.getItem;
  crateGetters["Picture"] = &crates.picture.getItem;

  auto auth = Authentication.instance(crates.user);
  auto adminRequest = new AdminRequest(crates.user);

  auto userDataMiddleware = UserDataMiddleware.instance(crates);
  auto modelInfo = new ModelInfoMiddleware("newsletter", crates.newsletter);
  auto visibilityFilter = new VisibilityFilter!"newsletters"(crates, crates.newsletter);
  auto creationRightsFlag = new CreationRightsFlag!"newsletter"(crates, "allowNewsletters");

  auto subscribe = new NewsletterSubscribeOperation(crates, broadcast);
  auto unsubscribe = new NewsletterUnsubscribeOperation(crates);
  auto deleteNewsletterMiddleware = new DeleteNewsletterMiddleware(crates);
  auto welcomeMessageMiddleware = new WelcomeMessageMiddleware(crates);
  auto importEmails = new NewsletterImportEmailsOperation(crates);
  auto exportEmails = new NewsletterExportEmailsOperation(crates);

  auto preparedRoute = crateRouter
    .prepare(crates.newsletter)
    .withCustomOperation(subscribe)
    .withCustomOperation(unsubscribe)
    .withCustomOperation(importEmails)
    .withCustomOperation(exportEmails)
    .and(auth.publicDataMiddleware)
    .and(adminRequest)
    .and(userDataMiddleware)
    .and(creationRightsFlag)
    .and(modelInfo)
    .and(visibilityFilter)
    .and(deleteNewsletterMiddleware)
    .and(welcomeMessageMiddleware);
}