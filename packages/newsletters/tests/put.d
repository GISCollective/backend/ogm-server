/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.newsletters.put;

import tests.fixtures;
import vibe.http.common;
import fluent.asserts;
import gis_collective.hmq.broadcast.memory;
import std.algorithm;
import ogm.defaults.teams;
import ogm.defaults.articles;
import ogm.newsletters.api;

alias suite = Spec!({
  URLRouter router;
  Json publicNewsletter;
  string publicNewsletterId;

  Json privateNewsletter;
  string privateNewsletterId;

  Json newsletterOtherTeam;
  string newsletterOtherTeamId;

  describe("updating newsletters", {
    beforeEach({
      setupTestData();
      auto broadcast = new MemoryBroadcast;
      router = new URLRouter;

      crates.setupDefaultTeams;
      crates.setupDefaultArticles;

      router.crateSetup.setupNewsletterApi(crates, broadcast);

      publicNewsletter = crates.newsletter.addItem(`{
        "name": "test",
        "description": "some description",
        "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" },
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);

      publicNewsletterId = publicNewsletter["_id"].to!string;

      newsletterOtherTeam = crates.newsletter.addItem(`{
        "name": "test",
        "visibility": { "isDefault": false, "isPublic": false, "team": "000000000000000000000004" },
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);

      newsletterOtherTeamId = newsletterOtherTeam["_id"].to!string;
    });

    describeCredentialsRule("update team newsletter", "yes", "newsletters", ["administrator", "owner"], (string type) {
      auto newsletterData = `{
        "newsletter": {
          "name": "new name",
          "description": "some description",
          "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" }
        }
      }`.parseJsonString;

      router
        .request
        .put("/newsletters/" ~ publicNewsletterId)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(newsletterData)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["newsletter"]["_id"].to!string.should.equal("000000000000000000000001");
        });
    });

    describeCredentialsRule("update team newsletter", "no", "newsletters", ["leader", "member", "guest"], (string type) {
      auto newsletterData = `{
        "newsletter": {
          "name": "new name",
          "description": "some description",
          "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" }
        }
      }`.parseJsonString;

      router
        .request
        .put("/newsletters/" ~ publicNewsletterId)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(newsletterData)
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{
            "errors": [{
              "description": "You don't have enough rights to edit ` ~ "`000000000000000000000001`" ~ `.",
              "status": 403,
              "title": "Forbidden"
            }]
          }`).parseJsonString);
        });
    });

    describeCredentialsRule("update team newsletter", "no", "newsletters", "no rights", {
      auto newsletterData = `{
        "newsletter": {
          "name": "new name",
          "description": "some description",
          "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" }
        }
      }`.parseJsonString;

      router
        .request
        .put("/newsletters/" ~ publicNewsletterId)
        .send(newsletterData)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
        });
    });


    describeCredentialsRule("update other team newsletter", "yes", "newsletters", ["administrator"], (string type) {
      auto newsletterData = `{
        "newsletter": {
          "name": "new name",
          "description": "some description",
          "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000004" }
        }
      }`.parseJsonString;

      router
        .request
        .put("/newsletters/" ~ newsletterOtherTeamId)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(newsletterData)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["newsletter"]["_id"].to!string.should.equal("000000000000000000000002");
        });
    });

    describeCredentialsRule("update other team newsletter", "no", "newsletters", ["owner", "leader", "member", "guest"], (string type) {
      auto newsletterData = `{
        "newsletter": {
          "name": "new name",
          "description": "some description",
          "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000004" }
        }
      }`.parseJsonString;

      router
        .request
        .put("/newsletters/" ~ newsletterOtherTeamId)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(newsletterData)
        .expectStatusCode(404)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{
            "errors": [{
              "description": "Item ` ~ "`000000000000000000000002`" ~ ` not found.",
              "status": 404,
              "title": "Crate not found"
            }]
          }`).parseJsonString);
        });
    });

    describeCredentialsRule("update other team newsletter", "no", "newsletters", "no rights", {
      auto newsletterData = `{
        "newsletter": {
          "name": "new name",
          "description": "some description",
          "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000004" }
        }
      }`.parseJsonString;

      router
        .request
        .put("/newsletters/" ~ newsletterOtherTeamId)
        .send(newsletterData)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
        });
    });
  });
});
