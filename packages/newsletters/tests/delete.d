/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.newsletters.delete_;

import tests.fixtures;
import vibe.http.common;
import fluent.asserts;
import gis_collective.hmq.broadcast.memory;
import std.algorithm;
import ogm.defaults.articles;
import ogm.defaults.teams;
import ogm.newsletters.api;

alias suite = Spec!({
  URLRouter router;
  Json publicNewsletter;
  string publicNewsletterId;

  Json newsletterOtherTeam;
  string newsletterOtherTeamId;

  describe("deleting newsletters", {
    beforeEach({
      setupTestData();
      auto broadcast = new MemoryBroadcast;
      router = new URLRouter;
      router.crateSetup.setupNewsletterApi(crates, broadcast);

      crates.setupDefaultTeams;
      crates.setupDefaultArticles;

      publicNewsletter = crates.newsletter.addItem(`{
        "name": "test",
        "description": "newsletter details",
        "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" },
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);

      publicNewsletterId = publicNewsletter["_id"].to!string;

      newsletterOtherTeam = crates.newsletter.addItem(`{
        "name": "test",
        "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000004" },
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);

      newsletterOtherTeamId = newsletterOtherTeam["_id"].to!string;
    });

    describeCredentialsRule("delete from own team", "yes", "newsletters", ["administrator", "owner"], (string type) {
      router
        .request
        .delete_("/newsletters/" ~ publicNewsletterId)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end;
    });

    describeCredentialsRule("delete from own team", "yes", "newsletters", ["leader", "member", "guest"], (string type) {
      router
        .request
        .delete_("/newsletters/" ~ publicNewsletterId)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end;
    });

    describeCredentialsRule("delete from own team", "no", "newsletters", "no rights", {
      router
        .request
        .delete_("/newsletters/" ~ publicNewsletterId)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
        });
    });


    describeCredentialsRule("delete from other teams", "yes", "newsletters", "administrator", (string type) {
      router
        .request
        .delete_("/newsletters/" ~ newsletterOtherTeamId)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204);
    });

    describeCredentialsRule("delete from other teams", "no", "newsletters", ["owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .delete_("/newsletters/" ~ newsletterOtherTeamId)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .end((Response response) => () {
          response.bodyJson.should.equal((`{
            "errors": [{
              "description": "Item ` ~ "`000000000000000000000002`" ~ ` not found.",
              "status": 404,
              "title": "Crate not found"
            }]
          }`).parseJsonString);
        });
    });

    describeCredentialsRule("delete from other teams", "no", "newsletters", "no rights", {
      router
        .request
        .delete_("/newsletters/" ~ newsletterOtherTeamId)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
        });
    });

    it("deletes newsletter emails that only have the current newsletter in the list", {
      crates.newsletterEmail.addItem(`{
        "_id": "000000000000000000000001",
        "email": "email@giscollective.com",
        "list": ["000000000000000000000001"]
      }`.parseJsonString);

      router
        .request
        .delete_("/newsletters/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .expectStatusCode(204)
        .end((Response response) => () {
          auto record = crates.newsletterEmail.get.where("email").equal("email@giscollective.com").and.exec;

          expect(record.empty).to.equal(true);
        });
    });

    it("does not delete news letter emails that don't have the newsletter in the list", {
      crates.newsletterEmail.addItem(`{
        "_id": "000000000000000000000001",
        "email": "email@giscollective.com",
        "list": ["000000000000000000000004"]
      }`.parseJsonString);

      router
        .request
        .delete_("/newsletters/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .expectStatusCode(204)
        .end((Response response) => () {
          auto record = crates.newsletterEmail.get.where("email").equal("email@giscollective.com").and.exec.front;

          expect(record).to.equal(`{
            "_id": "000000000000000000000001",
            "email": "email@giscollective.com",
            "list": ["000000000000000000000004"]
          }`.parseJsonString);
        });
    });

    it("removes the newsletter from the list when an email has more than one newsletter in the list", {
      crates.newsletterEmail.addItem(`{
        "_id": "000000000000000000000001",
        "email": "email@giscollective.com",
        "list": ["000000000000000000000004","000000000000000000000001"]
      }`.parseJsonString);

      router
        .request
        .delete_("/newsletters/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .expectStatusCode(204)
        .end((Response response) => () {
          auto record = crates.newsletterEmail.get.where("email").equal("email@giscollective.com").and.exec.front;

          expect(record).to.equal(`{
            "_id": "000000000000000000000001",
            "email": "email@giscollective.com",
            "list": ["000000000000000000000004"]
          }`.parseJsonString);
        });
    });

    it("removes the newsletter welcome message article", {
      crates.newsletterEmail.addItem(`{
        "_id": "000000000000000000000001",
        "email": "email@giscollective.com",
        "list": ["000000000000000000000004","000000000000000000000001"],
        "welcomeMessage": "000000000000000000000004",
      }`.parseJsonString);

      crates.article.addItem((`{
        "type": "newsletter-welcome-message",
        "relatedId": "` ~ publicNewsletterId ~ `"
      }`).parseJsonString);

      crates.article.addItem((`{
        "type": "newsletter-welcome-message",
        "relatedId": "` ~ newsletterOtherTeamId ~ `"
      }`).parseJsonString);

      router
        .request
        .delete_("/newsletters/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .expectStatusCode(204)
        .end((Response response) => () {
          auto idList = crates.article.get.where("type").equal("newsletter-welcome-message").and.exec.map!(a => a["relatedId"].to!string).array;

          expect(idList).to.equal(["000000000000000000000002"]);
        });
    });
  });
});
