module tests.newsletters.operations;

import tests.fixtures;
import vibe.http.common;
import std.datetime;
import ogm.calendar;
import ogm.defaults.teams;
import ogm.defaults.articles;
import ogm.newsletters.api;

alias suite = Spec!({
  URLRouter router;
  Json broadcastValue;

  describe("the subscribe operation", {
    Json publicNewsletter;
    Json privateNewsletter;

    MemoryBroadcast broadcast;

    beforeEach({
      setupTestData();

      auto calendarMock = new CalendarMock("2023-01-12T16:20:11Z");
      SysCalendar.instance = calendarMock;

      router = new URLRouter;

      crates.setupDefaultTeams;
      broadcast = new MemoryBroadcast;

      broadcastValue = Json.emptyObject;
      void testHandler(const Json value) @trusted {
        broadcastValue = value;
      }

      broadcast.register("newsletter.subscribe", &testHandler);

      crates.setupDefaultArticles;

      router.crateSetup.setupNewsletterApi(crates, broadcast);

      publicNewsletter = crates.newsletter.addItem(`{
        "name": "test",
        "description": "new description",
        "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" },
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);

      privateNewsletter = crates.newsletter.addItem(`{
        "name": "test",
        "visibility": { "isDefault": false, "isPublic": false, "team": "000000000000000000000001" },
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);
    });

    it("can trigger the job", {
      auto data = `{ "email": "email@giscollective.com" }`.parseJsonString;

      router
        .request
        .post("/newsletters/000000000000000000000001/subscribe")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .send(data)
        .expectStatusCode(201)
        .end((Response response) => () {
          broadcastValue.should.equal(`{
            "id": "000000000000000000000001",
            "email": "email@giscollective.com"
          }`.parseJsonString);
        });
    });

    it("creates a newsletter email record when it does not exist", {
      auto data = `{ "email": "email@giscollective.com" }`.parseJsonString;

      router
        .request
        .post("/newsletters/000000000000000000000001/subscribe")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .send(data)
        .expectStatusCode(201)
        .end((Response response) => () {
          auto record = crates.newsletterEmail.get.where("email").equal("email@giscollective.com").and.exec.front;

          expect(record).to.equal(`{
            "_id": "000000000000000000000001",
            "email": "email@giscollective.com",
            "list": [{
              "newsletter": "000000000000000000000001",
              "joinedOn": "2023-01-12T16:20:11Z",
            }]
          }`.parseJsonString);
        });
    });

    it("can subscribe an anonymous user to a public newsletter", {
      auto data = `{ "email": "email@giscollective.com" }`.parseJsonString;

      router
        .request
        .post("/newsletters/000000000000000000000001/subscribe")
        .send(data)
        .expectStatusCode(201)
        .end((Response response) => () {
          auto record = crates.newsletterEmail.get.where("email").equal("email@giscollective.com").and.exec.front;

          expect(record).to.equal(`{
            "_id": "000000000000000000000001",
            "email": "email@giscollective.com",
            "list": [{
              "newsletter": "000000000000000000000001",
              "joinedOn": "2023-01-12T16:20:11Z",
            }]
          }`.parseJsonString);
        });
    });

    it("can not subscribe an anonymous user to a private newsletter", {
      auto data = `{ "email": "email@giscollective.com" }`.parseJsonString;

      router
        .request
        .post("/newsletters/000000000000000000000002/subscribe")
        .send(data)
        .expectStatusCode(404)
        .end((Response response) => () {
          auto recordCount = crates.newsletterEmail.get.size;
          expect(recordCount).to.equal(0);

          expect(response.bodyJson).to.equal((`{
            "errors": [{
              "description": "There is no item with id ` ~ "`000000000000000000000002`" ~ `",
              "status": 404,
              "title": "Crate not found"
            }]
          }`).parseJsonString);
        });
    });

    it("adds the newsletter id to the list when an email record exists", {
      auto data = `{ "email": "email@giscollective.com" }`.parseJsonString;

      crates.newsletterEmail.addItem(`{
        "_id": "000000000000000000000001",
        "email": "email@giscollective.com",
        "list": [{
          "newsletter": "000000000000000000000004",
          "joinedOn": "2022-01-12T16:20:11Z",
        }]
      }`.parseJsonString);

      router
        .request
        .post("/newsletters/000000000000000000000001/subscribe")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .send(data)
        .expectStatusCode(201)
        .end((Response response) => () {
          auto record = crates.newsletterEmail.get.where("email").equal("email@giscollective.com").and.exec.front;

          expect(record).to.equal(`{
            "_id": "000000000000000000000001",
            "email": "email@giscollective.com",
            "list": [{
              "newsletter": "000000000000000000000004",
              "joinedOn": "2022-01-12T16:20:11Z",
            }, {
              "newsletter": "000000000000000000000001",
              "joinedOn": "2023-01-12T16:20:11Z",
            }]
          }`.parseJsonString);
        });
    });

    it("does not add twice a newsletter id", {
      auto data = `{ "email": "email@giscollective.com" }`.parseJsonString;

      crates.newsletterEmail.addItem(`{
        "_id": "000000000000000000000001",
        "email": "email@giscollective.com",
        "list": [{
          "newsletter": "000000000000000000000001",
          "joinedOn": "2023-01-12T16:20:11Z",
        }]
      }`.parseJsonString);

      router
        .request
        .post("/newsletters/000000000000000000000001/subscribe")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .send(data)
        .expectStatusCode(201)
        .end((Response response) => () {
          auto record = crates.newsletterEmail.get.where("email").equal("email@giscollective.com").and.exec.front;

          expect(record).to.equal(`{
            "_id": "000000000000000000000001",
            "email": "email@giscollective.com",
            "list": [{
              "newsletter": "000000000000000000000001",
              "joinedOn": "2023-01-12T16:20:11Z",
            }]
          }`.parseJsonString);
        });
    });

    it("returns an error when the email is invalid", {
      auto data = `{ "email": "invalid" }`.parseJsonString;

      router
        .request
        .post("/newsletters/000000000000000000000001/subscribe")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .send(data)
        .expectStatusCode(400)
        .end((Response response) => () {
          auto recordCount = crates.newsletterEmail.get.size;
          expect(recordCount).to.equal(0);

          expect(response.bodyJson).to.equal(`{
            "errors": [{
              "description": "Address has no domain part",
              "status": 400,
              "title": "Validation error"
            }]
          }`.parseJsonString);
        });
    });
  });

  describe("the unsubscribe operation", {
    Json publicNewsletter;
    Json privateNewsletter;

    MemoryBroadcast broadcast;

    beforeEach({
      setupTestData();
      router = new URLRouter;

      crates.setupDefaultTeams;
      broadcast = new MemoryBroadcast;

      broadcastValue = Json.emptyObject;
      void testHandler(const Json value) @trusted {
        broadcastValue = value;
      }

      router.crateSetup.setupNewsletterApi(crates, broadcast);

      publicNewsletter = crates.newsletter.addItem(`{
        "name": "test",
        "description": "new description",
        "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" },
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);

      privateNewsletter = crates.newsletter.addItem(`{
        "name": "test",
        "description": "new description",
        "visibility": { "isDefault": false, "isPublic": false, "team": "000000000000000000000001" },
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);
    });

    it("removes a newsletter email record when it only has the provided list", {
      auto data = `{ "email": "email@giscollective.com" }`.parseJsonString;

      crates.newsletterEmail.addItem(`{
        "_id": "000000000000000000000001",
        "email": "email@giscollective.com",
        "list": [{
          "newsletter": "000000000000000000000001",
          "joinedOn": "2023-01-12T16:20:11Z",
        }]
      }`.parseJsonString);

      router
        .request
        .post("/newsletters/000000000000000000000001/unsubscribe")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .send(data)
        .expectStatusCode(201)
        .end((Response response) => () {
          auto record = crates.newsletterEmail.get.where("email").equal("email@giscollective.com").and.exec;

          expect(record.empty).to.equal(true);
        });
    });

    it("can unsubscribe using the email id", {
      auto data = `{ "email": "000000000000000000000001" }`.parseJsonString;

      crates.newsletterEmail.addItem(`{
        "_id": "000000000000000000000001",
        "email": "email@giscollective.com",
        "list": [{
          "newsletter": "000000000000000000000001",
          "joinedOn": "2023-01-12T16:20:11Z",
        }]
      }`.parseJsonString);

      router
        .request
        .post("/newsletters/000000000000000000000001/unsubscribe")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .send(data)
        .expectStatusCode(201)
        .end((Response response) => () {
          auto record = crates.newsletterEmail.get.where("email").equal("email@giscollective.com").and.exec;

          expect(record.empty).to.equal(true);
        });
    });

    it("does not remove a newsletter email record when the newsletter is not in the list", {
      auto data = `{ "email": "email@giscollective.com" }`.parseJsonString;

      crates.newsletterEmail.addItem(`{
        "_id": "000000000000000000000001",
        "email": "email@giscollective.com",
        "list": [{
          "newsletter": "000000000000000000000004",
          "joinedOn": "2023-01-12T16:20:11Z",
        }]
      }`.parseJsonString);

      router
        .request
        .post("/newsletters/000000000000000000000001/unsubscribe")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .send(data)
        .expectStatusCode(201)
        .end((Response response) => () {
          auto record = crates.newsletterEmail.get.where("email").equal("email@giscollective.com").and.exec.front;

          expect(record).to.equal(`{
            "_id": "000000000000000000000001",
            "email": "email@giscollective.com",
            "list": [{
              "newsletter": "000000000000000000000004",
              "joinedOn": "2023-01-12T16:20:11Z",
            }]
          }`.parseJsonString);
        });
    });

    it("can not subscribe an anonymous user to a private newsletter", {
      auto data = `{ "email": "email@giscollective.com" }`.parseJsonString;

      router
        .request
        .post("/newsletters/000000000000000000000002/unsubscribe")
        .send(data)
        .expectStatusCode(404)
        .end((Response response) => () {
          auto recordCount = crates.newsletterEmail.get.size;
          expect(recordCount).to.equal(0);

          expect(response.bodyJson).to.equal((`{
            "errors": [{
              "description": "There is no item with id ` ~ "`000000000000000000000002`" ~ `",
              "status": 404,
              "title": "Crate not found"
            }]
          }`).parseJsonString);
        });
    });

    it("returns an error when the email is invalid", {
      auto data = `{ "email": "invalid" }`.parseJsonString;

      router
        .request
        .post("/newsletters/000000000000000000000001/unsubscribe")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .send(data)
        .expectStatusCode(400)
        .end((Response response) => () {
          auto recordCount = crates.newsletterEmail.get.size;
          expect(recordCount).to.equal(0);

          expect(response.bodyJson).to.equal(`{
            "errors": [{
              "description": "Address has no domain part",
              "status": 400,
              "title": "Validation error"
            }]
          }`.parseJsonString);
        });
    });
  });

  describe("the import-emails operation", {
    Json publicNewsletter;
    Json publicNewsletterOtherTeam;
    string publicNewsletterOtherTeamId;

    MemoryBroadcast broadcast;

    beforeEach({
      setupTestData();

      auto calendarMock = new CalendarMock("2023-01-12T16:20:11Z");
      SysCalendar.instance = calendarMock;

      router = new URLRouter;

      crates.setupDefaultTeams;
      crates.setupDefaultArticles;

      router.crateSetup.setupNewsletterApi(crates, broadcast);

      publicNewsletter = crates.newsletter.addItem(`{
        "name": "test",
        "description": "new description",
        "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" },
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);

      publicNewsletterOtherTeam = crates.newsletter.addItem(`{
        "name": "test",
        "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000004" },
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);

      publicNewsletterOtherTeamId = publicNewsletterOtherTeam["_id"].to!string;
    });

    describeCredentialsRule("import emails to own newsletter", "yes", "newsletters", ["administrator", "owner", "leader"], (string type) {
      auto data = `{ "emails": ["email@giscollective.com"] }`.parseJsonString;

      router
        .request
        .post("/newsletters/000000000000000000000001/import-emails")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          auto record = crates.newsletterEmail.get.where("email").equal("email@giscollective.com").and.exec.front;

          expect(record).to.equal(`{
            "_id": "000000000000000000000001",
            "email": "email@giscollective.com",
            "list": [{
              "newsletter": "000000000000000000000001",
              "joinedOn": "2023-01-12T16:20:11Z",
            }]
          }`.parseJsonString);

          expect(response.bodyJson).to.equal(`{ "emailImportStats": {
              "added": 1,
              "existing": 0,
              "invalid": 0
            }
          }`.parseJsonString);
        });
    });

    describeCredentialsRule("import emails to own newsletter", "no", "newsletters", ["member", "guest"], (string type) {
      auto data = `{ "emails": ["email@giscollective.com"] }`.parseJsonString;

      router
        .request
        .post("/newsletters/000000000000000000000001/import-emails")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(data)
        .expectStatusCode(401)
        .end((Response response) => () {
          auto range = crates.newsletterEmail.get.where("email").equal("email@giscollective.com").and.exec;

          expect(range.empty).to.equal(true);

          expect(response.bodyJson).to.equal(`{
            "errors": [{
              "description": "You must be an owner or leader to do this.",
              "status": 401,
              "title": "Unauthorized"
            }]
          }`.parseJsonString);
        });
    });

    describeCredentialsRule("import emails to own newsletter", "no", "newsletters", "no rights", {
      auto data = `{ "emails": ["email@giscollective.com"] }`.parseJsonString;

      router
        .request
        .post("/newsletters/000000000000000000000001/import-emails")
        .send(data)
        .expectStatusCode(401)
        .end((Response response) => () {
          auto range = crates.newsletterEmail.get.where("email").equal("email@giscollective.com").and.exec;

          expect(range.empty).to.equal(true);

          expect(response.bodyJson).to.equal(`{
            "errors": [{
              "description": "You must authenticate to do this.",
              "status": 401,
              "title": "Unauthorized"
            }]
          }`.parseJsonString);
        });
    });

    ////
    describeCredentialsRule("import emails to other teams newsletter", "yes", "newsletters", "administrator", (string type) {
      auto data = `{ "emails": ["email@giscollective.com"] }`.parseJsonString;

      router
        .request
        .post("/newsletters/" ~ publicNewsletterOtherTeamId ~ "/import-emails")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          auto record = crates.newsletterEmail.get.where("email").equal("email@giscollective.com").and.exec.front;

          expect(record).to.equal(`{
            "_id": "000000000000000000000001",
            "email": "email@giscollective.com",
            "list": [{
              "newsletter": "000000000000000000000002",
              "joinedOn": "2023-01-12T16:20:11Z",
            }]
          }`.parseJsonString);

          expect(response.bodyJson).to.equal(`{ "emailImportStats": {
              "added": 1,
              "existing": 0,
              "invalid": 0
            }
          }`.parseJsonString);
        });
    });

    describeCredentialsRule("import emails to other teams newsletter", "no", "newsletters", ["owner", "leader", "member", "guest"], (string type) {
      auto data = `{ "emails": ["email@giscollective.com"] }`.parseJsonString;

      router
        .request
        .post("/newsletters/" ~ publicNewsletterOtherTeamId ~ "/import-emails")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(data)
        .expectStatusCode(401)
        .end((Response response) => () {
          auto range = crates.newsletterEmail.get.where("email").equal("email@giscollective.com").and.exec;

          expect(range.empty).to.equal(true);

          expect(response.bodyJson).to.equal(`{
            "errors": [{
              "description": "You must be an owner or leader to do this.",
              "status": 401,
              "title": "Unauthorized"
            }]
          }`.parseJsonString);
        });
    });

    describeCredentialsRule("import emails to other teams newsletter", "no", "newsletters", "no rights", {
      auto data = `{ "emails": ["email@giscollective.com"] }`.parseJsonString;

      router
        .request
        .post("/newsletters/" ~ publicNewsletterOtherTeamId ~ "/import-emails")
        .send(data)
        .expectStatusCode(401)
        .end((Response response) => () {
          auto range = crates.newsletterEmail.get.where("email").equal("email@giscollective.com").and.exec;

          expect(range.empty).to.equal(true);

          expect(response.bodyJson).to.equal(`{
            "errors": [{
              "description": "You must authenticate to do this.",
              "status": 401,
              "title": "Unauthorized"
            }]
          }`.parseJsonString);
        });
    });

    it("creates two subscriber records for 2 new emails", {
      auto data = `{ "emails": ["email1@giscollective.com","email2@giscollective.com"] }`.parseJsonString;

      router
        .request
        .post("/newsletters/000000000000000000000001/import-emails")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          crates.newsletterEmail.get.where("email").equal("email1@giscollective.com").and.exec.empty.should.equal(false);
          crates.newsletterEmail.get.where("email").equal("email2@giscollective.com").and.exec.empty.should.equal(false);

          expect(response.bodyJson).to.equal(`{ "emailImportStats": {
              "added": 2,
              "existing": 0,
              "invalid": 0
            }
          }`.parseJsonString);
        });
    });

    it("creates two subscriber records for 2 new emails separated by ,", {
      auto data = `{ "emails": "email1@giscollective.com,email2@giscollective.com" }`.parseJsonString;

      router
        .request
        .post("/newsletters/000000000000000000000001/import-emails")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          crates.newsletterEmail.get.where("email").equal("email1@giscollective.com").and.exec.empty.should.equal(false);
          crates.newsletterEmail.get.where("email").equal("email2@giscollective.com").and.exec.empty.should.equal(false);

          expect(response.bodyJson).to.equal(`{ "emailImportStats": {
              "added": 2,
              "existing": 0,
              "invalid": 0
            }
          }`.parseJsonString);
        });
    });

    it("creates two subscriber records for 2 new quoted emails separated by tabs", {
      auto data = `{ "emails": "'email1@giscollective.com'\t\"email2@giscollective.com\"" }`.parseJsonString;

      router
        .request
        .post("/newsletters/000000000000000000000001/import-emails")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          crates.newsletterEmail.get.where("email").equal("email1@giscollective.com").and.exec.empty.should.equal(false);
          crates.newsletterEmail.get.where("email").equal("email2@giscollective.com").and.exec.empty.should.equal(false);

          expect(response.bodyJson).to.equal(`{ "emailImportStats": {
              "added": 2,
              "existing": 0,
              "invalid": 0
            }
          }`.parseJsonString);
        });
    });

    it("creates two subscriber records for 2 new emails separated by ;", {
      auto data = `{ "emails": "email1@giscollective.com;email2@giscollective.com" }`.parseJsonString;

      router
        .request
        .post("/newsletters/000000000000000000000001/import-emails")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          crates.newsletterEmail.get.where("email").equal("email1@giscollective.com").and.exec.empty.should.equal(false);
          crates.newsletterEmail.get.where("email").equal("email2@giscollective.com").and.exec.empty.should.equal(false);

          expect(response.bodyJson).to.equal(`{ "emailImportStats": {
              "added": 2,
              "existing": 0,
              "invalid": 0
            }
          }`.parseJsonString);
        });
    });

    it("creates two subscriber records for 2 new emails separated by spaces", {
      auto data = `{ "emails": "email1@giscollective.com  email2@giscollective.com" }`.parseJsonString;

      router
        .request
        .post("/newsletters/000000000000000000000001/import-emails")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          crates.newsletterEmail.get.where("email").equal("email1@giscollective.com").and.exec.empty.should.equal(false);
          crates.newsletterEmail.get.where("email").equal("email2@giscollective.com").and.exec.empty.should.equal(false);

          expect(response.bodyJson).to.equal(`{ "emailImportStats": {
              "added": 2,
              "existing": 0,
              "invalid": 0
            }
          }`.parseJsonString);
        });
    });

    it("creates two subscriber records for 2 new emails separated by new line", {
      auto data = `{ "emails": "email1@giscollective.com\nemail2@giscollective.com" }`.parseJsonString;

      router
        .request
        .post("/newsletters/000000000000000000000001/import-emails")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          crates.newsletterEmail.get.where("email").equal("email1@giscollective.com").and.exec.empty.should.equal(false);
          crates.newsletterEmail.get.where("email").equal("email2@giscollective.com").and.exec.empty.should.equal(false);

          expect(response.bodyJson).to.equal(`{ "emailImportStats": {
              "added": 2,
              "existing": 0,
              "invalid": 0
            }
          }`.parseJsonString);
        });
    });

    it("creates two subscriber records for 2 new emails separated by windows new line", {
      auto data = `{ "emails": "email1@giscollective.com\r\nemail2@giscollective.com" }`.parseJsonString;

      router
        .request
        .post("/newsletters/000000000000000000000001/import-emails")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          crates.newsletterEmail.get.where("email").equal("email1@giscollective.com").and.exec.empty.should.equal(false);
          crates.newsletterEmail.get.where("email").equal("email2@giscollective.com").and.exec.empty.should.equal(false);

          expect(response.bodyJson).to.equal(`{ "emailImportStats": {
              "added": 2,
              "existing": 0,
              "invalid": 0
            }
          }`.parseJsonString);
        });
    });

    it("ignores a value if it is not an email", {
      auto data = `{ "emails": ["invalid"] }`.parseJsonString;

      router
        .request
        .post("/newsletters/000000000000000000000001/import-emails")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          crates.newsletterEmail.get.where("email").equal("invalid").and.exec.empty.should.equal(true);

          expect(response.bodyJson).to.equal(`{ "emailImportStats": {
              "added": 0,
              "existing": 0,
              "invalid": 1
            }
          }`.parseJsonString);
        });
    });

    it("ignores an email if it is already added", {
      crates.newsletterEmail.addItem(`{
        "_id": "000000000000000000000001",
        "email": "exists@giscollective.com",
        "list": [{
          "newsletter": "000000000000000000000001",
          "joinedOn": "2023-01-12T16:20:11Z",
        }]
      }`.parseJsonString);

      auto data = `{ "emails": ["exists@giscollective.com"] }`.parseJsonString;

      crates.newsletterEmail.addItem(`{
        "_id": "000000000000000000000001",
        "email": "email@giscollective.com",
        "list": [{
          "newsletter": "000000000000000000000001",
          "joinedOn": "2023-01-12T16:20:11Z",
        }]
      }`.parseJsonString);

      router
        .request
        .post("/newsletters/000000000000000000000001/import-emails")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          crates.newsletterEmail.get.where("email").equal("exists@giscollective.com").and.exec.empty.should.equal(false);

          expect(response.bodyJson).to.equal(`{ "emailImportStats": {
              "added": 0,
              "existing": 1,
              "invalid": 0
            }
          }`.parseJsonString);
        });
    });

    it("removes existing subscribers when replace=true", {
      crates.newsletterEmail.addItem(`{
        "_id": "000000000000000000000001",
        "email": "exists@giscollective.com",
        "list": [{
          "newsletter": "000000000000000000000001",
          "joinedOn": "2023-01-12T16:20:11Z",
        }]
      }`.parseJsonString);

      auto data = `{ "replace": true, "emails": ["new@giscollective.com"] }`.parseJsonString;

      router
        .request
        .post("/newsletters/000000000000000000000001/import-emails")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          crates.newsletterEmail.get.where("email").equal("exists@giscollective.com").and.exec.front["list"].should.equal(Json.emptyArray);
          crates.newsletterEmail.get.where("email").equal("new@giscollective.com").and.exec.empty.should.equal(false);

          expect(response.bodyJson).to.equal(`{ "emailImportStats": {
              "added": 1,
              "existing": 0,
              "invalid": 0
            }
          }`.parseJsonString);
        });
    });
  });

  describe("the export-emails operation", {
    Json publicNewsletter;
    Json publicNewsletterOtherTeam;
    string publicNewsletterOtherTeamId;

    MemoryBroadcast broadcast;

    beforeEach({
      setupTestData();

      auto calendarMock = new CalendarMock("2023-01-12T16:20:11Z");
      SysCalendar.instance = calendarMock;

      router = new URLRouter;

      crates.setupDefaultTeams;
      crates.setupDefaultArticles;

      router.crateSetup.setupNewsletterApi(crates, broadcast);

      publicNewsletter = crates.newsletter.addItem(`{
        "name": "test",
        "description": "new description",
        "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" },
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);

      publicNewsletterOtherTeam = crates.newsletter.addItem(`{
        "name": "test",
        "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000004" },
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);

      publicNewsletterOtherTeamId = publicNewsletterOtherTeam["_id"].to!string;
    });

    describeCredentialsRule("export emails from own newsletter", "yes", "newsletters", ["administrator", "owner", "leader"], (string type) {
      router
        .request
        .get("/newsletters/000000000000000000000001/export-emails")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          expect(response.bodyString).to.equal("email,date added,read messages,links opened\n");
        });
    });

    describeCredentialsRule("export emails from own newsletter", "no", "newsletters", ["member", "guest"], (string type) {
      router
        .request
        .get("/newsletters/" ~ publicNewsletterOtherTeamId ~ "/export-emails")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(401)
        .end((Response response) => () {
          expect(response.bodyJson).to.equal(`{
            "errors":[
              {"description":"You must be an owner or leader to do this.","status":401,"title":"Unauthorized"}
            ]
          }`.parseJsonString);
        });
    });

    describeCredentialsRule("export emails from own newsletter", "no", "newsletters", "no rights", {
       router
        .request
        .get("/newsletters/" ~ publicNewsletterOtherTeamId ~ "/export-emails")
        .expectStatusCode(401)
        .end((Response response) => () {
          expect(response.bodyJson).to.equal(`{
            "errors":[
              {"description":"You must authenticate to do this.","status":401,"title":"Unauthorized"}
            ]
          }`.parseJsonString);
        });
    });

    ////
    describeCredentialsRule("export emails from other teams newsletter", "yes", "newsletters", "administrator", (string type) {
      auto data = `{ "emails": ["email@giscollective.com"] }`.parseJsonString;

      router
        .request
        .post("/newsletters/" ~ publicNewsletterOtherTeamId ~ "/import-emails")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          auto record = crates.newsletterEmail.get.where("email").equal("email@giscollective.com").and.exec.front;

          expect(record).to.equal(`{
            "_id": "000000000000000000000001",
            "email": "email@giscollective.com",
            "list": [{
              "newsletter": "000000000000000000000002",
              "joinedOn": "2023-01-12T16:20:11Z",
            }]
          }`.parseJsonString);

          expect(response.bodyJson).to.equal(`{ "emailImportStats": {
              "added": 1,
              "existing": 0,
              "invalid": 0
            }
          }`.parseJsonString);
        });
    });

    describeCredentialsRule("export emails from other teams newsletter", "no", "newsletters", ["owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/newsletters/" ~ publicNewsletterOtherTeamId ~ "/export-emails")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(401)
        .end((Response response) => () {
          expect(response.bodyJson).to.equal(`{
            "errors":[
              {"description":"You must be an owner or leader to do this.","status":401,"title":"Unauthorized"}
            ]
          }`.parseJsonString);
        });
    });

    describeCredentialsRule("export emails from other teams newsletter", "no", "newsletters", "no rights", {
      router
        .request
        .get("/newsletters/" ~ publicNewsletterOtherTeamId ~ "/export-emails")
        .expectStatusCode(401)
        .end((Response response) => () {
          expect(response.bodyJson).to.equal(`{
            "errors":[
              {"description":"You must authenticate to do this.","status":401,"title":"Unauthorized"}
            ]
          }`.parseJsonString);
        });
    });


    it("returns two emails when they belong to the newsletter", {
      crates.newsletterEmail.addItem(`{
        "_id": "000000000000000000000001",
        "email": "email1@giscollective.com",
        "list": [{
          "newsletter": "000000000000000000000001",
          "joinedOn": "2023-01-12T16:20:11Z",
        }]
      }`.parseJsonString);

      crates.newsletterEmail.addItem(`{
        "_id": "000000000000000000000002",
        "email": "email2@giscollective.com",
        "list": [{
          "newsletter": "000000000000000000000001",
          "joinedOn": "2024-01-12T16:20:11Z",
        }]
      }`.parseJsonString);


      crates.newsletterEmail.addItem(`{
        "_id": "000000000000000000000003",
        "email": "email3@giscollective.com",
        "list": [{
          "newsletter": "000000000000000000000002",
          "joinedOn": "2023-01-12T16:20:11Z",
        }]
      }`.parseJsonString);

      router
        .request
        .get("/newsletters/000000000000000000000001/export-emails")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(200)
        .end((Response response) => () {
          expect(response.bodyString).to.equal(
            "email,date added,read messages,links opened\n" ~
            "email1@giscollective.com,2023-01-12T16:20:11Z,0,0\n" ~
            "email2@giscollective.com,2024-01-12T16:20:11Z,0,0\n"
          );
        });
    });

    it("adds the emails with read messages stats", {
      crates.newsletterEmail.addItem(`{
        "_id": "000000000000000000000001",
        "email": "email1@giscollective.com",
        "list": [{
          "newsletter": "000000000000000000000001",
          "joinedOn": "2023-01-12T16:20:11Z",
        }]
      }`.parseJsonString);

      crates.metric.addItem(`{
        "type": "articleView",
        "name": "",
        "value": 1,
        "time": "2023-01-12T16:20:11Z",
        "labels": {
          "relatedId": "000000000000000000000001",
          "email": "email1@giscollective.com"
        }
      }`.parseJsonString);

       crates.metric.addItem(`{
        "type": "articleView",
        "name": "",
        "value": 1,
        "time": "2023-01-12T16:20:11Z",
        "labels": {
          "relatedId": "000000000000000000000001",
          "email": "email1@giscollective.com"
        }
      }`.parseJsonString);

      router
        .request
        .get("/newsletters/000000000000000000000001/export-emails")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(200)
        .end((Response response) => () {
          expect(response.bodyString).to.equal(
            "email,date added,read messages,links opened\n" ~
            "email1@giscollective.com,2023-01-12T16:20:11Z,2,0\n"
          );
        });
    });

    it("adds the emails with the link interactions stats", {
      crates.newsletterEmail.addItem(`{
        "_id": "000000000000000000000001",
        "email": "email1@giscollective.com",
        "list": [{
          "newsletter": "000000000000000000000001",
          "joinedOn": "2023-01-12T16:20:11Z",
        }]
      }`.parseJsonString);

      crates.metric.addItem(`{
        "type": "articleInteraction",
        "name": "",
        "value": 1,
        "time": "2023-01-12T16:20:11Z",
        "labels": {
          "relatedId": "000000000000000000000001",
          "email": "email1@giscollective.com"
        }
      }`.parseJsonString);

       crates.metric.addItem(`{
        "type": "articleInteraction",
        "name": "",
        "value": 1,
        "time": "2023-01-12T16:20:11Z",
        "labels": {
          "relatedId": "000000000000000000000001",
          "email": "email1@giscollective.com"
        }
      }`.parseJsonString);

      router
        .request
        .get("/newsletters/000000000000000000000001/export-emails")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(200)
        .end((Response response) => () {
          expect(response.bodyString).to.equal(
            "email,date added,read messages,links opened\n" ~
            "email1@giscollective.com,2023-01-12T16:20:11Z,0,2\n"
          );
        });
    });
  });
});
