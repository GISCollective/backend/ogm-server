/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.newsletters.get;

import tests.fixtures;
import vibe.http.common;
import fluent.asserts;
import gis_collective.hmq.broadcast.memory;
import std.algorithm;
import ogm.defaults.articles;
import ogm.defaults.teams;
import ogm.newsletters.api;

alias suite = Spec!({
  URLRouter router;
  Json publicNewsletter;
  string publicNewsletterId;

  Json privateNewsletter;
  string privateNewsletterId;

  Json privateNewsletterOtherTeam;
  string privateNewsletterOtherTeamId;

  describe("getting newsletters", {
    beforeEach({
      setupTestData();
      auto broadcast = new MemoryBroadcast;
      router = new URLRouter;
      router.crateSetup.setupNewsletterApi(crates, broadcast);

      crates.setupDefaultTeams;
      crates.setupDefaultArticles;

      publicNewsletter = crates.newsletter.addItem(`{
        "name": "test",
        "description": "some description",
        "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" },
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);

      publicNewsletterId = publicNewsletter["_id"].to!string;

      privateNewsletter = crates.newsletter.addItem(`{
        "name": "test",
        "visibility": { "isDefault": false, "isPublic": false, "team": "000000000000000000000001" },
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);

      privateNewsletterId = privateNewsletter["_id"].to!string;

      privateNewsletterOtherTeam = crates.newsletter.addItem(`{
        "name": "test",
        "visibility": { "isDefault": false, "isPublic": false, "team": "000000000000000000000004" },
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);

      privateNewsletterOtherTeamId = privateNewsletterOtherTeam["_id"].to!string;
    });

    describeCredentialsRule("get a public newsletter", "yes", "newsletters", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/newsletters/" ~ publicNewsletterId)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["newsletter"]["_id"].to!string.should.equal(publicNewsletterId);
        });
    });

    describeCredentialsRule("get a public newsletter", "yes", "newsletters", "no rights", {
      router
        .request
        .get("/newsletters/" ~ publicNewsletterId)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["newsletter"]["_id"].to!string.should.equal(publicNewsletterId);
        });
    });


    describeCredentialsRule("get a private newsletter from own team", "yes", "newsletters", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/newsletters/" ~ privateNewsletterId)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["newsletter"]["_id"].to!string.should.equal(privateNewsletterId);
        });
    });

    describeCredentialsRule("get a private newsletter from own team", "no", "newsletters", "no rights", {
      router
        .request
        .get("/newsletters/" ~ privateNewsletterId)
        .expectStatusCode(404)
        .end;
    });


    describeCredentialsRule("get a private newsletter from other teams", "yes", "newsletters", "administrator", (string type) {
      router
        .request
        .get("/newsletters/" ~ privateNewsletterOtherTeamId)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["newsletter"]["_id"].to!string.should.equal(privateNewsletterOtherTeamId);
        });
    });

    describeCredentialsRule("get a private newsletter from other teams", "no", "newsletters", ["owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/newsletters/" ~ privateNewsletterOtherTeamId)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(404)
        .end;
    });

    describeCredentialsRule("get a private newsletter from other teams", "no", "newsletters", "no rights", {
      router
        .request
        .get("/newsletters/" ~ privateNewsletterOtherTeamId)
        .expectStatusCode(404)
        .end;
    });

    describeCredentialsRule("filter newsletters by team", "yes", "newsletters", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/newsletters?team=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["newsletters"].byValue.map!(a => a["_id"].to!string).array.should.equal([publicNewsletterId, privateNewsletterId]);
        });
    });

    describeCredentialsRule("filter newsletters by team", "no", "newsletters", "no rights", {
      router
        .request
        .get("/newsletters?team=000000000000000000000001")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["newsletters"].byValue.map!(a => a["_id"].to!string).array.should.equal([publicNewsletterId]);
        });
    });
  });
});
