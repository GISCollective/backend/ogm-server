/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.newsletters.post;

import fluent.asserts;
import gis_collective.hmq.broadcast.memory;
import ogm.calendar;
import ogm.defaults.articles;
import ogm.defaults.teams;
import ogm.newsletters.api;
import std.algorithm;
import tests.fixtures;
import vibe.http.common;

alias suite = Spec!({
  URLRouter router;

  Json newsletter;

  describe("creating newsletters", {
    beforeEach({
      setupTestData();
      auto broadcast = new MemoryBroadcast;
      router = new URLRouter;
      router.crateSetup.setupNewsletterApi(crates, broadcast);

      crates.setupDefaultTeams;
      crates.setupDefaultArticles;

      newsletter = `{ "newsletter": {
        "name": "test",
        "description": "some description",
        "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" },
      }}`.parseJsonString;

      SysCalendar.instance = new CalendarMock("2023-12-11T12:22:22Z");
    });

    describe("when the team has allowNewsletters=true", {
      beforeEach({
        auto team = crates.team.getItem("000000000000000000000001").and.exec.front;
        team["allowNewsletters"] = true;

        crates.team.updateItem(team);
      });

      describeCredentialsRule("can create a newsletter when the team has allowNewsletters=true", "yes", "newsletters", ["administrator", "owner"], (string type) {
        router
          .request
          .post("/newsletters")
          .send(newsletter)
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["newsletter"]["_id"].to!string.should.equal("000000000000000000000001");
          });
      });

      describeCredentialsRule("can create a newsletter when the team has allowNewsletters=true", "no", "newsletters", ["leader", "member", "guest"], (string type) {
        router
          .request
          .post("/newsletters")
          .send(newsletter)
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"errors":[{"description":"You don't have enough rights to add an item.","status":403,"title":"Forbidden"}]}`.parseJsonString);
          });
      });

      describeCredentialsRule("can create a newsletter when the team has allowNewsletters=true", "no", "newsletters", "no rights", {
        router
          .request
          .post("/newsletters")
          .send(newsletter)
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
          });
      });

      it("creates a welcome message article for the newsletter", {
        newsletter["newsletter"]["visibility"] = `{
          "isDefault": false,
          "isPublic": true,
          "team": "000000000000000000000003"
        }`.parseJsonString;

        router
          .request
          .post("/newsletters")
          .send(newsletter)
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            auto article = crates.article.get
              .where("type").equal(ArticleType.newsletterWelcomeMessage).and
              .where("relatedId").equal("000000000000000000000001").and
              .exec
              .front;

            response.bodyJson["newsletter"]["welcomeMessage"].to!string.should.equal(article["_id"].to!string);
            response.bodyJson["newsletter"]["visibility"].should.equal(`{
              "isDefault": false,
              "isPublic": true,
              "team": "000000000000000000000003"
            }`.parseJsonString);

            article["visibility"].should.equal(`{
              "isDefault": false,
              "isPublic": true,
              "team": "000000000000000000000003"
            }`.parseJsonString);

            response.bodyJson["newsletter"]["info"].should.equal(`{
              "author": "000000000000000000000005",
              "changeIndex": 0,
              "createdOn": "2023-12-11T12:22:22Z",
              "lastChangeOn": "2023-12-11T12:22:22Z",
              "originalAuthor": "000000000000000000000005"
            }`.parseJsonString);

            article["info"].should.equal(`{
              "author": "000000000000000000000005",
              "changeIndex": 0,
              "createdOn": "2023-12-11T12:22:22Z",
              "lastChangeOn": "2023-12-11T12:22:22Z",
              "originalAuthor": "000000000000000000000005"
            }`.parseJsonString);
          });
      });
    });

    describe("when the team has allowNewsletters=false", {
      beforeEach({
        auto team = crates.team.getItem("000000000000000000000001").and.exec.front;
        team["allowNewsletters"] = false;

        crates.team.updateItem(team);
      });

      describeCredentialsRule("can create a newsletter when the team has allowNewsletters=false", "yes", "newsletters", "administrator", (string type) {
        router
          .request
          .post("/newsletters")
          .send(newsletter)
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["newsletter"]["_id"].to!string.should.equal("000000000000000000000001");
          });
      });

      describeCredentialsRule("can create a newsletter when the team has allowNewsletters=false", "no", "newsletters", ["owner", "leader", "member", "guest"], (string type) {
        router
          .request
          .post("/newsletters")
          .send(newsletter)
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"errors":[{"description":"You can't create this newsletter for this team.","status":403,"title":"Forbidden"}]}`.parseJsonString);
          });
      });

      describeCredentialsRule("can create a newsletter when the team has allowNewsletters=false", "no", "newsletters", "no rights", {
        router
          .request
          .post("/newsletters")
          .send(newsletter)
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
          });
      });
    });
  });
});
