/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.events.api;

import vibe.http.router;
import vibe.core.log;
import vibe.data.json;
import vibe.service.configuration.general;

import crate.http.router;
import crate.base;

import ogm.auth;
import ogm.crates.all;

import gis_collective.hmq.broadcast.base;
import ogm.events.middlewares.CalendarValidationMiddleware;
import ogm.events.middlewares.EventsFilterMiddleware;
import ogm.events.middlewares.EventsSortingMiddleware;
import ogm.filter.pagination;
import ogm.filter.visibility;
import ogm.middleware.adminrequest;
import ogm.middleware.creationRightsFlag;
import ogm.middleware.DefaultCover;
import ogm.middleware.event.EventOccurrencesMiddleware;
import ogm.middleware.IconFieldMapper;
import ogm.middleware.modelinfo;
import ogm.middleware.SortingMiddleware;
import ogm.middleware.userdata;
import ogm.operations.BulkDeleteOperation;
import ogm.operations.BulkPublishOperation;
import ogm.operations.BulkUnpublishOperation;

///
void setupEventApi(T)(CrateRouter!T crateRouter, OgmCrates crates, IBroadcast broadcast) {
  crateGetters["Team"] = &crates.team.getItem;
  crateGetters["Picture"] = &crates.picture.getItem;
  crateGetters["Calendar"] = &crates.calendar.getItem;
  crateGetters["Icon"] = &crates.icon.getItem;
  crateGetters["IconSet"] = &crates.iconSet.getItem;

  auto auth = Authentication.instance(crates.user);
  auto adminRequest = new AdminRequest(crates.user);

  auto userDataMiddleware = UserDataMiddleware.instance(crates);
  auto modelInfo = new ModelInfoMiddleware("event", crates.event);
  auto visibilityFilter = new VisibilityFilter!("events", false, true, true)(crates, crates.event);
  auto creationRightsFlag = new CreationRightsFlag!"event"(crates, "allowEvents");
  auto eventOccurrences = new EventOccurrencesMiddleware(crates);
  auto defaultCover = new DefaultCoverMiddleware(crates, "event");
  auto calendarValidation = new CalendarValidationMiddleware(crates);
  auto eventsFilter = new EventsFilterMiddleware(crates);
  auto paginationFilter = new PaginationFilter;
  auto sorting = new EventsSortingMiddleware(crates);
  auto iconFieldMapper = new IconFieldMapper(crates);

  auto bulkDelete = new BulkDeleteOperation!("event", "calendar")(crates, auth.privateDataMiddleware, []);
  auto bulkPublish = new BulkPublishOperation!("event", "calendar")(crates, auth.privateDataMiddleware, []);
  auto bulkUnpublish = new BulkUnpublishOperation!("event", "calendar")(crates, auth.privateDataMiddleware, []);

  auto preparedRoute = crateRouter
    .prepare(crates.event)
    .withCustomOperation(bulkDelete)
    .withCustomOperation(bulkPublish)
    .withCustomOperation(bulkUnpublish)
    .and(auth.publicDataMiddleware)
    .and(adminRequest)
    .and(userDataMiddleware)
    .and(creationRightsFlag)
    .and(calendarValidation)
    .and(modelInfo)
    .and(visibilityFilter)
    .and(eventOccurrences)
    .and(eventsFilter)
    .and(paginationFilter)
    .and(sorting)
    .and(iconFieldMapper)
    .and(defaultCover);
}