module ogm.events.middlewares.CalendarValidationMiddleware;

import crate.base;
import crate.error;
import std.algorithm;
import std.array;
import ogm.calendar;
import ogm.crates.all;
import ogm.http.request;
import vibe.data.json;
import vibe.http.router;

class CalendarValidationMiddleware {
  OgmCrates crates;

  this(OgmCrates crates) {
    this.crates = crates;
  }

  Json getCalendarId(HTTPServerRequest req) {
    auto calendarId = req.json["event"]["calendar"];
    enforce!CrateValidationException(calendarId.type == Json.Type.string, "The event has an invalid or missing `calendar` field.");

    return calendarId;
  }

  @put @patch
  void checkExistingCalendar(HTTPServerRequest req) {
    auto calendarId = this.getCalendarId(req);
    scope request = RequestUserData(req);

    auto event = crates.event.getItem(request.itemId).and.exec.front;
    enforce!ForbiddenException(event["calendar"] == calendarId, "You can't move the event to another calendar.");

    auto calendar = crates.calendar.getItem(calendarId.to!string).exec.front;
    req.json["event"]["visibility"] = calendar["visibility"];
  }

  ///
  @create
  void checkNewEvent(HTTPServerRequest req) {
    auto calendarId = this.getCalendarId(req);
    auto calendar = crates.calendar.getItem(calendarId.to!string).exec.front;

    enforce!ForbiddenException(calendar["visibility"]["team"] == req.json["event"]["visibility"]["team"], "The calendar does not belong to the event's team.");
    req.json["event"]["visibility"] = calendar["visibility"];
  }
}
