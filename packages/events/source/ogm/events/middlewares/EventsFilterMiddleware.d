module ogm.events.middlewares.EventsFilterMiddleware;

import crate.base;
import crate.error;
import std.algorithm;
import std.array;
import std.datetime;
import std.range;
import std.typecons;
import std.conv;
import ogm.calendar;
import ogm.crates.all;
import ogm.http.request;
import vibe.data.json;
import vibe.http.router;

class EventsFilterMiddleware {
  OgmCrates crates;

  struct Params {
    @describe("Filter events by calendar")
    string calendar;

    @describe("Filter events by a feature id")
    string location;

    @describe("Filter events by a icon ids")
    string icons;

    @describe("Filter events by the repetition type")
    @example("norepeat", "Get one off events.")
    @example("daily", "Get events that repeat daily.")
    @example("weekly", "Get events that repeat weekly.")
    @example("monthly", "Get events that repeat monthly.")
    @example("yearly", "Get events that repeat yearly.")
    string repetition;

    @describe("Get events that have occurences beginning after an ISO formatted date")
    string after;

    @describe("Get events that have occurences ending before an ISO formatted date")
    string before;

    @describe("Get events that have occurences ending on an ISO formatted date")
    string date;

    @describe("Filter events based on attrributes using the format: key1|value1|key2|value2 ...")
    string attributes;

    @describe("Filter events based on the day of week")
    @example("0", "Monday")
    @example("1", "Tuesday")
    @example("2", "Wednesday")
    @example("3", "Thursday")
    @example("4", "Friday")
    @example("5", "Saturday")
    @example("6", "Sunday")
    @example("7", "Weekday")
    @example("8", "Weekend")
    string dayOfWeek;

    @describe("When true it will return the events that start after the current date and time")
    bool onlyFuture;

    @describe("When true it will return the events that start before the current date and time")
    bool onlyPast;
  }

  this(OgmCrates crates) {
    this.crates = crates;
  }

  string[] getLocationsIds(string[string] pieces) {
    auto query = this.crates.feature.get;

    foreach (key, value; pieces) {
      query.where("attributes." ~ key).equal(value);
    }

    return query.and.withProjection(["_id"]).exec.map!(a => a["_id"].to!string).array;
  }

  @getList
  void paramsFilter(HTTPServerRequest req, IQuery selector, Params params) {
    auto request = RequestUserData(req);
    auto session = request.session(crates);
    auto ownCalendars = session.all.calendars;
    auto ownTeams = session.ownTeams;
    auto team = req.query.get("team", "");

    auto allowPrivate = request.isAdmin || ownTeams.canFind(team) || ownCalendars.canFind(params.calendar);

    if(params.calendar) {
      selector.where("calendar").equal(ObjectId(params.calendar));
    }

    if(params.location) {
      selector.where("location.value").equal(params.location);
    }

    if(params.repetition) {
      selector.where("entries.repetition").equal(params.repetition);
    }

    if(params.onlyFuture) {
      selector.where("nextOccurrence.begin").greaterThan(Clock.currTime);
    }

    if(params.onlyPast) {
      auto or = selector.or;
      or.where("nextOccurrence.end").lessThan(Clock.currTime).
      or.where("nextOccurrence").not.exists.
      or.where("nextOccurrence").isNull;
    }

    if(params.after) {
      selector.where("allOccurrences.begin").greaterThan(SysTime.fromISOExtString(params.after));
    }

    if(params.before) {
      selector.where("allOccurrences.end").lessThan(SysTime.fromISOExtString(params.before));
    }

    if(params.date) {
      auto date = SysTime.fromISOExtString(params.date);
      auto begin = SysTime(cast(Date) date, date.timezone) - 1.seconds;
      auto end = SysTime(cast(Date) date, date.timezone) + 1.days + 1.seconds;

      selector
        .where("allOccurrences.begin").greaterThan(begin).and
        .where("allOccurrences.end").lessThan(end);
    }

    if(params.dayOfWeek) {
      auto dayOfWeek = params.dayOfWeek.to!int;

      if(dayOfWeek >= 0 && dayOfWeek <=6) {
        selector
          .where("allOccurrences.dayOfWeek").equal(dayOfWeek);
      }

      if(dayOfWeek == 7) {
        auto or = selector.or;

        or
          .where("allOccurrences.dayOfWeek").equal(0).or
          .where("allOccurrences.dayOfWeek").equal(1).or
          .where("allOccurrences.dayOfWeek").equal(2).or
          .where("allOccurrences.dayOfWeek").equal(3).or
          .where("allOccurrences.dayOfWeek").equal(4);
      }

      if(dayOfWeek == 8) {
        auto or = selector.or;
        or.where("allOccurrences.dayOfWeek").equal(5).or
        .where("allOccurrences.dayOfWeek").equal(6);
      }
    }

    if(params.icons) {
      auto ids = params.icons.split(",").map!(a => ObjectId(a)).array;
      selector.where("icons").containsAll(ids);
    }

    if(params.attributes) {
      auto pieces = params.attributes.split("|")
        .chunks(2)
        .map!(a => (tuple(a[0], a[1])))
        .assocArray;

      auto attrQuery = selector.or;

      foreach (key, value; pieces) {
        attrQuery.where("attributes." ~ key).equal(value);
      }

      attrQuery.or.where("location.value").anyOf(this.getLocationsIds(pieces));
    }

    if(!allowPrivate) {
      selector.where("visibility.isPublic").equal(true);
    }
  }
}
