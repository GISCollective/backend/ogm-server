module ogm.events.middlewares.EventsSortingMiddleware;

import ogm.http.request;
import crate.base;
import vibe.http.server;
import ogm.crates.all;

class EventsSortingMiddleware {
  struct QueryParams {
    @describe("If is set the records will be sorted by the values in the field.")
    @example("name", "Sort by name.")
    string sortBy;

    @describe("If sortOrder is set, the records will be returned in the requested order.")
    @example("asc", "Return items in ascending order.")
    @example("desc", "Return items in descending order.")
    string sortOrder;
  }

  private {
    OgmCrates crates;
    string defaultField;
    int defaultOrder;
  }

  this(OgmCrates crates) {
    this.crates = crates;
    this.defaultField = "nextOccurrence.begin";
    this.defaultOrder = 1;
  }


  ///
  @getList
  void applySorting(HTTPServerRequest req, IQuery selector, QueryParams params) {
    if("onlyPast" in req.query && req.query["onlyPast"] == "true") {
      return;
    }

    selector.sort("nextOccurrence.begin", 1);
  }
}