/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.events.put;

import tests.fixtures;
import vibe.http.common;
import fluent.asserts;
import gis_collective.hmq.broadcast.memory;
import std.algorithm;
import ogm.defaults.teams;
import ogm.defaults.articles;
import ogm.events.api;
import ogm.calendar;

alias suite = Spec!({
  URLRouter router;
  Json publicEvent;
  string publicEventId;

  Json privateEvent;
  string privateEventId;

  Json eventOtherTeam;
  string eventOtherTeamId;

  describe("updating events", {
    beforeEach({
      setupTestData();
      auto broadcast = new MemoryBroadcast;
      router = new URLRouter;

      crates.setupDefaultTeams;
      crates.setupDefaultArticles;

      router.crateSetup.setupEventApi(crates, broadcast);

      publicEvent = crates.event.addItem(`{
        "name": "test",
        "article": "some description",
        "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" },
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 },
        "location": { "type": "Text", "value": "" },
        "entries": [],
        "calendar": "000000000000000000000001"
      }`.parseJsonString);

      publicEventId = publicEvent["_id"].to!string;

      eventOtherTeam = crates.event.addItem(`{
        "name": "test",
        "article": "some description",
        "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000004" },
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 },
        "location": { "type": "Text", "value": "" },
        "entries": [],
        "calendar": "000000000000000000000004"
      }`.parseJsonString);

      eventOtherTeamId = eventOtherTeam["_id"].to!string;

      SysCalendar.instance = new CalendarMock("2018-02-01T09:30:10Z");
    });

    describeCredentialsRule("update team event", "yes", "events", ["administrator", "owner", "leader"], (string type) {
      auto eventData = `{
        "event": {
          "name": "new name",
          "location": { "type": "Text", "value": "" },
          "article": "some description",
          "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" },
          "cover": "000000000000000000000001",
          "entries": [],
          "calendar": "000000000000000000000001"
        }
      }`.parseJsonString;

      router
        .request
        .put("/events/" ~ publicEventId)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(eventData)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["event"]["_id"].to!string.should.equal("000000000000000000000001");
        });
    });

    describeCredentialsRule("update team event", "no", "events", ["member", "guest"], (string type) {
      auto eventData = `{
        "event": {
          "name": "new name",
          "location": { "type": "Text", "value": "" },
          "article": "some description",
          "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" },
          "cover": "000000000000000000000001",
          "entries": [],
          "calendar": "000000000000000000000001"
        }
      }`.parseJsonString;

      router
        .request
        .put("/events/" ~ publicEventId)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(eventData)
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{
            "errors": [{
              "description": "You don't have enough rights to edit ` ~ "`000000000000000000000001`" ~ `.",
              "status": 403,
              "title": "Forbidden"
            }]
          }`).parseJsonString);
        });
    });

    describeCredentialsRule("update team event", "-", "events", "no rights", {
      auto eventData = `{
        "event": {
          "name": "new name",
          "location": { "type": "Text", "value": "" },
          "article": "some description",
          "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" },
          "cover": "000000000000000000000001",
          "entries": [],
          "calendar": "000000000000000000000001"
        }
      }`.parseJsonString;

      router
        .request
        .put("/events/" ~ publicEventId)
        .send(eventData)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
        });
    });


    describeCredentialsRule("update own team event", "yes", "events", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      auto event = crates.event.getItem(publicEventId).and.exec.front;
      event["info"]["originalAuthor"] = userId[type];
      crates.event.updateItem(event);

      auto eventData = `{
        "event": {
          "name": "new name",
          "location": { "type": "Text", "value": "" },
          "article": "some description",
          "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" },
          "cover": "000000000000000000000001",
          "entries": [],
          "calendar": "000000000000000000000001"
        }
      }`.parseJsonString;

      router
        .request
        .put("/events/" ~ publicEventId)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(eventData)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["event"]["_id"].to!string.should.equal("000000000000000000000001");
        });
    });

    describeCredentialsRule("update own team event", "-", "events", "no rights", {
      auto eventData = `{
        "event": {
          "name": "new name",
          "location": { "type": "Text", "value": "" },
          "article": "some description",
          "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" },
          "cover": "000000000000000000000001",
          "entries": [],
          "calendar": "000000000000000000000001"
        }
      }`.parseJsonString;

      router
        .request
        .put("/events/" ~ publicEventId)
        .send(eventData)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
        });
    });


    describeCredentialsRule("update other team event", "yes", "events", ["administrator"], (string type) {
      auto eventData = `{
        "event": {
          "name": "new name",
          "location": { "type": "Text", "value": "" },
          "article": "some description",
          "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000004" },
          "cover": "000000000000000000000001",
          "entries": [],
          "calendar": "000000000000000000000004"
        }
      }`.parseJsonString;

      router
        .request
        .put("/events/" ~ eventOtherTeamId)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(eventData)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["event"]["_id"].to!string.should.equal("000000000000000000000002");
        });
    });

    describeCredentialsRule("update other team event", "no", "events", ["owner", "leader", "member", "guest"], (string type) {
      auto eventData = `{
        "event": {
          "name": "new name",
          "location": { "type": "Text", "value": "" },
          "article": "some description",
          "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000004" },
          "cover": "000000000000000000000001",
          "entries": [],
          "calendar": "000000000000000000000004"
        }
      }`.parseJsonString;

      router
        .request
        .put("/events/" ~ eventOtherTeamId)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(eventData)
        .expectStatusCode(404)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{
            "errors": [{
              "description": "Item ` ~ "`000000000000000000000002`" ~ ` not found.",
              "status": 404,
              "title": "Crate not found"
            }]
          }`).parseJsonString);
        });
    });

    describeCredentialsRule("update other team event", "-", "events", "no rights", {
      auto eventData = `{
        "event": {
          "name": "new name",
          "location": { "type": "Text", "value": "" },
          "article": "some description",
          "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000004" },
          "cover": "000000000000000000000001",
          "entries": []
        }
      }`.parseJsonString;

      router
        .request
        .put("/events/" ~ eventOtherTeamId)
        .send(eventData)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
        });
    });


    it("can't change the calendar once it is set", {
      auto eventData = `{
        "event": {
          "name": "new name",
          "article": "some description",
          "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000004" },
          "cover": "000000000000000000000001",
          "entries": [],
          "location": { "type": "Text", "value": "" },
          "calendar": "000000000000000000000001"
        }
      }`.parseJsonString;

      router
        .request
        .put("/events/" ~ eventOtherTeamId)
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .send(eventData)
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "errors": [{
            "description": "You can't move the event to another calendar.",
            "status": 403,
            "title": "Forbidden"
          }]}`.parseJsonString);
        });
    });

    it("adds the occurrences on update", {
      auto eventData = `{
        "event": {
          "name": "new name",
          "article": "some description",
          "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000004" },
          "cover": "000000000000000000000001",
          "entries": [{
            "begin": "2018-01-01T12:30:10Z",
            "end": "2018-01-01T13:30:10Z",
            "intervalEnd": "2018-02-01T13:30:10Z",
            "repetition": "monthly",
            "timezone": "EET"
          }],
          "location": { "type": "Text", "value": "" },
          "calendar": "000000000000000000000004"
        }
      }`.parseJsonString;

      router
        .request
        .put("/events/" ~ eventOtherTeamId)
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .send(eventData)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["event"]["allOccurrences"].should.equal(`[{
            "begin": "2018-01-01T14:30:10+02:00",
            "end": "2018-01-01T15:30:10+02:00",
            "dayOfWeek": 0
          }, {
            "begin": "2018-02-01T14:30:10+02:00",
            "end": "2018-02-01T15:30:10+02:00",
            "dayOfWeek": 3
          }]`.parseJsonString);

          response.bodyJson["event"]["nextOccurrence"].should.equal(`{
            "begin": "2018-02-01T14:30:10+02:00",
            "end": "2018-02-01T15:30:10+02:00",
            "dayOfWeek": 3
          }`.parseJsonString);
        });
    });

    it("sets the visibility to the calendar visibility on update", {
      auto event = crates.event.addItem(`{
        "name": "test",
        "article": "some description",
        "visibility": { "isDefault": false, "isPublic": false, "team": "000000000000000000000002" },
        "entries": [],
        "location": { "type": "Text", "value": "" },
        "calendar": "000000000000000000000001"
      }`.parseJsonString);

      auto eventData = `{
        "event": {
          "name": "new name",
          "article": "some description",
          "visibility": { "isDefault": false, "isPublic": false, "team": "000000000000000000000002" },
          "cover": "000000000000000000000001",
          "entries": [],
          "location": { "type": "Text", "value": "" },
          "calendar": "000000000000000000000001"
        }
      }`.parseJsonString;

      router
        .request
        .put("/events/" ~ event["_id"].to!string)
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .send(eventData)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["event"]["visibility"].should.equal(`{
            "isDefault": false,
            "isPublic": true,
            "team": "000000000000000000000001"
          }`.parseJsonString);
        });
    });
  });
});
