/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.events.post;

import tests.fixtures;
import vibe.http.common;
import fluent.asserts;
import gis_collective.hmq.broadcast.memory;
import std.algorithm;
import ogm.defaults.teams;
import ogm.defaults.articles;
import ogm.events.api;
import ogm.calendar;

alias suite = Spec!({
  URLRouter router;

  Json event;

  describe("creating events", {
    beforeEach({
      setupTestData();
      auto broadcast = new MemoryBroadcast;
      router = new URLRouter;
      router.crateSetup.setupEventApi(crates, broadcast);

      crates.setupDefaultTeams;
      crates.setupDefaultArticles;

      event = `{ "event": {
        "name": "test",
        "location": { "type": "Text", "value": "" },
        "article": "some description",
        "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" },
        "entries": [],
        "calendar": "000000000000000000000001"
      }}`.parseJsonString;

      SysCalendar.instance = new CalendarMock("2018-02-01T09:30:10Z");
    });

    describe("when the team has allowEvents=true", {
      beforeEach({
        auto team = crates.team.getItem("000000000000000000000001").and.exec.front;
        team["allowEvents"] = true;

        crates.team.updateItem(team);
      });

      describeCredentialsRule("create an event when the team has allowEvents=true", "yes", "events", ["administrator", "owner", "leader", "member"], (string type) {
        router
          .request
          .post("/events")
          .send(event)
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["event"]["_id"].to!string.should.equal("000000000000000000000001");
          });
      });

      describeCredentialsRule("create an event when the team has allowEvents=true", "no", "events", ["guest"], (string type) {
        router
          .request
          .post("/events")
          .send(event)
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"errors":[{"description":"You don't have enough rights to add an item.","status":403,"title":"Forbidden"}]}`.parseJsonString);
          });
      });

      describeCredentialsRule("create an event when the team has allowEvents=true", "no", "events", "no rights", {
        router
          .request
          .post("/events")
          .send(event)
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
          });
      });
    });

    describe("when the team has allowEvents=false", {
      beforeEach({
        auto team = crates.team.getItem("000000000000000000000001").and.exec.front;
        team["allowEvents"] = false;

        crates.team.updateItem(team);
      });

      describeCredentialsRule("create an event when the team has allowEvents=false", "yes", "events", "administrator", (string type) {
        router
          .request
          .post("/events")
          .send(event)
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["event"]["_id"].to!string.should.equal("000000000000000000000001");
          });
      });

      describeCredentialsRule("create an event when the team has allowEvents=false", "no", "events", ["owner", "leader", "member", "guest"], (string type) {
        router
          .request
          .post("/events")
          .send(event)
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"errors":[{"description":"You can't create this event for this team.","status":403,"title":"Forbidden"}]}`.parseJsonString);
          });
      });

      describeCredentialsRule("create an event when the team has allowEvents=false", "no", "events", "no rights", {
        router
          .request
          .post("/events")
          .send(event)
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
          });
      });
    });

    it("adds the occurrences on creation", {
      event["event"]["entries"] = `[{
        "begin": "2018-01-01T12:30:10Z",
        "end": "2018-01-01T13:30:10Z",
        "intervalEnd": "2018-02-01T13:30:10Z",
        "repetition": "monthly",
        "timezone": "EET"
      }]`.parseJsonString;

      router
        .request
        .post("/events")
        .send(event)
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["event"]["allOccurrences"].should.equal(`[{
            "begin": "2018-01-01T14:30:10+02:00",
            "end": "2018-01-01T15:30:10+02:00",
            "dayOfWeek": 0
          }, {
            "begin": "2018-02-01T14:30:10+02:00",
            "end": "2018-02-01T15:30:10+02:00",
            "dayOfWeek": 3
          }]`.parseJsonString);

          response.bodyJson["event"]["nextOccurrence"].should.equal(`{
            "begin": "2018-02-01T14:30:10+02:00",
            "end": "2018-02-01T15:30:10+02:00",
            "dayOfWeek": 3
          }`.parseJsonString);
        });
    });

    it("can't set a calendar from another team", {
      event["event"]["calendar"] = `000000000000000000000004`;

      router
        .request
        .post("/events")
        .send(event)
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "errors": [{
            "description": "The calendar does not belong to the event's team.",
            "status": 403,
            "title": "Forbidden"
          }]}`.parseJsonString);
        });
    });
  });
});
