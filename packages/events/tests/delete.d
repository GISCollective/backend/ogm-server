/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.events.delete_;

import tests.fixtures;
import vibe.http.common;
import fluent.asserts;
import gis_collective.hmq.broadcast.memory;
import std.algorithm;
import ogm.defaults.articles;
import ogm.defaults.teams;
import ogm.events.api;

alias suite = Spec!({
  URLRouter router;
  Json publicEvent;
  string publicEventId;

  Json eventOtherTeam;
  string eventOtherTeamId;

  describe("deleting events", {
    beforeEach({
      setupTestData();
      auto broadcast = new MemoryBroadcast;
      router = new URLRouter;
      router.crateSetup.setupEventApi(crates, broadcast);

      crates.setupDefaultTeams;
      crates.setupDefaultArticles;

      publicEvent = crates.event.addItem(`{
        "name": "test",
        "description": "event details",
        "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" },
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);

      publicEventId = publicEvent["_id"].to!string;

      eventOtherTeam = crates.event.addItem(`{
        "name": "test",
        "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000004" },
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);

      eventOtherTeamId = eventOtherTeam["_id"].to!string;
    });

    describeCredentialsRule("delete from own team", "yes", "events", ["administrator", "owner", "leader"], (string type) {
      router
        .request
        .delete_("/events/" ~ publicEventId)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end;
    });

    describeCredentialsRule("delete from own team", "no", "events", ["member", "guest"], (string type) {
      router
        .request
        .delete_("/events/" ~ publicEventId)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end;
    });

    describeCredentialsRule("delete from own team", "-", "events", "no rights", {
      router
        .request
        .delete_("/events/" ~ publicEventId)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
        });
    });

    describeCredentialsRule("delete own events", "yes", "events", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      auto event = crates.event.getItem(publicEventId).and.exec.front;
      event["info"]["originalAuthor"] = userId[type];

      crates.event.updateItem(event);

      router
        .request
        .delete_("/events/" ~ publicEventId)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end;
    });

    describeCredentialsRule("delete own events", "-", "events", "no rights", {});

    describeCredentialsRule("delete from other teams", "yes", "events", "administrator", (string type) {
      router
        .request
        .delete_("/events/" ~ eventOtherTeamId)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204);
    });

    describeCredentialsRule("delete from other teams", "no", "events", ["owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .delete_("/events/" ~ eventOtherTeamId)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .end((Response response) => () {
          response.bodyJson.should.equal((`{
            "errors": [{
              "description": "Item ` ~ "`000000000000000000000002`" ~ ` not found.",
              "status": 404,
              "title": "Crate not found"
            }]
          }`).parseJsonString);
        });
    });

    describeCredentialsRule("delete from other teams", "-", "events", "no rights", {
      router
        .request
        .delete_("/events/" ~ eventOtherTeamId)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
        });
    });
  });
});
