/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.events.operations;

import tests.fixtures;
import vibe.http.common;
import fluent.asserts;
import gis_collective.hmq.broadcast.memory;
import std.algorithm;
import ogm.defaults.articles;
import ogm.defaults.teams;
import ogm.events.api;

alias suite = Spec!({
  URLRouter router;
  Json publicEvent;
  string publicEventId;

  Json privateEvent;
  string privateEventId;

  Json privateEventOtherTeam;
  string privateEventOtherTeamId;

  describe("operations", {
    beforeEach({
      setupTestData();
      auto broadcast = new MemoryBroadcast;
      router = new URLRouter;
      router.crateSetup.setupEventApi(crates, broadcast);

      crates.setupDefaultTeams;

      publicEvent = crates.event.addItem(`{
        "name": "test",
        "description": "some description",
        "calendar": "000000000000000000000001",
        "location": {
          "type": "Feature",
          "value": "000000000000000000000001"
        },
        "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" },
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);

      publicEventId = publicEvent["_id"].to!string;

      privateEvent = crates.event.addItem(`{
        "name": "test",
        "location": { "type": "Text", "value": "" },
        "calendar": "000000000000000000000001",
        "visibility": { "isDefault": false, "isPublic": false, "team": "000000000000000000000001" },
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);

      privateEventId = privateEvent["_id"].to!string;

      privateEventOtherTeam = crates.event.addItem(`{
        "name": "test",
        "location": { "type": "Text", "value": "" },
        "calendar": "000000000000000000000004",
        "visibility": { "isDefault": false, "isPublic": false, "team": "000000000000000000000004" },
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);

      privateEventOtherTeamId = privateEventOtherTeam["_id"].to!string;
    });

    describe("bulk delete", {
      MemoryBroadcast broadcast;

      describeCredentialsRule("delete events from own team", "yes", "events", ["administrator", "owner"], (string type) {
        router
          .request
          .delete_("/events?team=000000000000000000000001&calendar=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(204)
          .end;

        crates.event.get.exec.map!(a => a["visibility"]["team"]).array.sort.uniq.should.equal(["000000000000000000000004"]);
      });

      describeCredentialsRule("delete events from own team", "no", "events", ["leader", "member", "guest"], (string type) {
        router
          .request
          .delete_("/events?team=000000000000000000000001&calendar=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(204)
          .end;

        crates.event.get.exec.map!(a => a["visibility"]["team"]).array.sort.uniq.should.equal(["000000000000000000000001", "000000000000000000000004"]);
      });

      describeCredentialsRule("delete events from own team", "no", "events", "no rights", {
        router
          .request
          .delete_("/events?team=000000000000000000000001&calendar=000000000000000000000001")
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "error": "Authorization required"
            }`.parseJsonString);
          });
      });


      describeCredentialsRule("delete events from other team", "yes", "events", ["administrator"], (string type) {
        router
          .request
          .delete_("/events?team=000000000000000000000004&calendar=000000000000000000000004")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(204)
          .end;

        crates.event.get.exec.map!(a => a["visibility"]["team"]).array.sort.uniq.should.equal(["000000000000000000000001"]);
      });

      describeCredentialsRule("delete events from other team", "no", "events", ["owner", "leader", "member", "guest"], (string type) {
        router
          .request
          .delete_("/events?team=000000000000000000000004&calendar=000000000000000000000004")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(404)
          .end;

        crates.event.get.exec.map!(a => a["visibility"]["team"]).array.sort.uniq.should.equal(["000000000000000000000001", "000000000000000000000004"]);
      });

      describeCredentialsRule("delete events from other team", "no", "events", "no rights", {
        router
          .request
          .delete_("/events?team=000000000000000000000004&calendar=000000000000000000000004")
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "error": "Authorization required"
            }`.parseJsonString);
          });
      });


      describeCredentialsRule("delete own events", "yes", "events", ["administrator", "owner", "leader", "member"], (string type) {
        auto events = crates.event.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

        foreach(event; events) {
          event["info"]["originalAuthor"] = userId[type];
          crates.event.updateItem(event);
        }

        router
          .request
          .delete_("/events?team=000000000000000000000001&calendar=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(204)
          .end;

        crates.event.get.exec.map!(a => a["visibility"]["team"]).array.sort.uniq.should.equal(["000000000000000000000004"]);
      });

      describeCredentialsRule("delete own events", "no", "events", ["guest"], (string type) {
        auto events = crates.event.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

        foreach(event; events) {
          event["info"]["originalAuthor"] = userId[type];
          crates.event.updateItem(event);
        }

        router
          .request
          .delete_("/events?team=000000000000000000000001&calendar=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(204)
          .end;

        crates.event.get.exec.map!(a => a["visibility"]["team"]).array.sort.uniq.should.equal(["000000000000000000000001", "000000000000000000000004"]);
      });

      describeCredentialsRule("delete own events", "no", "events", "no rights", { });


      it("does not delete an event when it does not match the space", {
        auto events = crates.event.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

        router
          .request
          .delete_("/events?team=000000000000000000000001&calendar=000000000000000000000010")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(204)
          .end;

        crates.event.get.exec.map!(a => a["visibility"]["team"]).array.sort.uniq.should.equal(["000000000000000000000001", "000000000000000000000004"]);
      });

      it("deletes an event when it matches the space", {
        auto events = crates.event.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

        router
          .request
          .delete_("/events?team=000000000000000000000001&calendar=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(204)
          .end;

        crates.event.get.exec.map!(a => a["visibility"]["team"]).array.sort.uniq.should.equal(["000000000000000000000004"]);
      });
    });

    describe("bulk publish", {
      describeCredentialsRule("bulk publish events from own team", "yes", "events", ["administrator", "owner"], (string type) {
        router
          .request
          .post("/events/publish?team=000000000000000000000001&calendar=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(204)
          .end;

        auto event = crates.event.getItem(privateEventId).exec.front.clone;

        event["visibility"]["isPublic"].to!bool.should.equal(true);
      });

      describeCredentialsRule("bulk publish events from own team", "no", "events", ["leader", "member", "guest"], (string type) {
        router
          .request
          .post("/events/publish?team=000000000000000000000001&calendar=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(204)
          .end;

        auto event = crates.event.getItem(privateEventId).exec.front.clone;

        event["visibility"]["isPublic"].to!bool.should.equal(false);
      });

      describeCredentialsRule("bulk publish events from own team", "no", "events", "no rights", {
        router
          .request
          .post("/events/publish?team=000000000000000000000001&calendar=000000000000000000000001")
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "error": "Authorization required"
            }`.parseJsonString);
          });
      });


      describeCredentialsRule("bulk publish own events", "yes", "events", ["administrator", "owner", "leader", "member"], (string type) {
        auto events = crates.event.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

        foreach(event; events) {
          event["info"]["originalAuthor"] = userId[type];
          crates.event.updateItem(event);
        }

        router
          .request
          .post("/events/publish?team=000000000000000000000001&calendar=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(204)
          .end;

        auto event = crates.event.getItem("000000000000000000000001").exec.front.clone;
        event["visibility"]["isPublic"].to!bool.should.equal(true);
      });

      describeCredentialsRule("bulk publish own events", "no", "events", ["guest"], (string type) {
        auto events = crates.event.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

        foreach(event; events) {
          event["info"]["originalAuthor"] = userId[type];
          crates.event.updateItem(event);
        }

        router
          .request
          .post("/events/publish?team=000000000000000000000001&calendar=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(204)
          .end;

        auto event = crates.event.getItem(privateEventId).exec.front.clone;
        event["visibility"]["isPublic"].to!bool.should.equal(false);
      });

      describeCredentialsRule("bulk publish own events", "no", "events", "no rights", { });


      describeCredentialsRule("bulk publish events from other teams", "yes", "events", ["administrator"], (string type) {
        auto events = crates.event.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

        foreach(event; events) {
          event["info"]["originalAuthor"] = userId[type];
          event["visibility"]["team"] = "000000000000000000000004";
          crates.event.updateItem(event);
        }

        router
          .request
          .post("/events/publish?team=000000000000000000000004&calendar=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(204)
          .end;

        auto event = crates.event.getItem(privateEventId).exec.front.clone;
        event["visibility"]["isPublic"].to!bool.should.equal(true);
      });

      describeCredentialsRule("bulk publish events from other teams", "no", "events", ["owner", "leader", "member", "guest"], (string type) {
        auto events = crates.event.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

        foreach(event; events) {
          event["info"]["originalAuthor"] = userId[type];
          event["visibility"]["team"] = "000000000000000000000004";
          crates.event.updateItem(event);
        }

        router
          .request
          .post("/events/publish?team=000000000000000000000004&calendar=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(404)
          .end;

        auto event = crates.event.getItem(privateEventId).exec.front.clone;
        event["visibility"]["isPublic"].to!bool.should.equal(false);
      });

      describeCredentialsRule("bulk publish events from other teams", "no", "events", "no rights", { });
    });

    describe("bulk unpublish", {
      describeCredentialsRule("bulk unpublish events from own team", "yes", "events", ["administrator", "owner"], (string type) {
        router
          .request
          .post("/events/unpublish?team=000000000000000000000001&calendar=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(204)
          .end;

        auto event = crates.event.getItem(publicEventId).exec.front.clone;

        event["visibility"]["isPublic"].to!bool.should.equal(false);
      });

      describeCredentialsRule("bulk unpublish events from own team", "no", "events", ["leader", "member", "guest"], (string type) {
        router
          .request
          .post("/events/unpublish?team=000000000000000000000001&calendar=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(204)
          .end;

        auto event = crates.event.getItem(publicEventId).exec.front.clone;

        event["visibility"]["isPublic"].to!bool.should.equal(true);
      });

      describeCredentialsRule("bulk unpublish events from own team", "no", "events", "no rights", {
        router
          .request
          .post("/events/unpublish?team=000000000000000000000001&calendar=000000000000000000000001")
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "error": "Authorization required"
            }`.parseJsonString);
          });
      });


      describeCredentialsRule("bulk unpublish own events", "yes", "events", ["administrator", "owner", "leader", "member"], (string type) {
        auto events = crates.event.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

        foreach(event; events) {
          event["info"]["originalAuthor"] = userId[type];
          crates.event.updateItem(event);
        }

        router
          .request
          .post("/events/unpublish?team=000000000000000000000001&calendar=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(204)
          .end;

        auto event = crates.event.getItem(publicEventId).exec.front.clone;
        event["visibility"]["isPublic"].to!bool.should.equal(false);
      });

      describeCredentialsRule("bulk unpublish own events", "no", "events", ["guest"], (string type) {
        auto events = crates.event.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

        foreach(event; events) {
          event["info"]["originalAuthor"] = userId[type];
          crates.event.updateItem(event);
        }

        router
          .request
          .post("/events/unpublish?team=000000000000000000000001&calendar=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(204)
          .end;

        auto event = crates.event.getItem(publicEventId).exec.front.clone;
        event["visibility"]["isPublic"].to!bool.should.equal(true);
      });

      describeCredentialsRule("bulk unpublish own events", "no", "events", "no rights", { });


      describeCredentialsRule("bulk unpublish events from other teams", "yes", "events", ["administrator"], (string type) {
        auto events = crates.event.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

        foreach(event; events) {
          event["info"]["originalAuthor"] = userId[type];
          event["visibility"]["team"] = "000000000000000000000004";
          crates.event.updateItem(event);
        }

        router
          .request
          .post("/events/unpublish?team=000000000000000000000004&calendar=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(204)
          .end;

        auto event = crates.event.getItem(publicEventId).exec.front.clone;
        event["visibility"]["isPublic"].to!bool.should.equal(false);
      });

      describeCredentialsRule("bulk unpublish events from other teams", "no", "events", ["owner", "leader", "member", "guest"], (string type) {
        auto events = crates.event.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

        foreach(event; events) {
          event["info"]["originalAuthor"] = userId[type];
          event["visibility"]["team"] = "000000000000000000000004";
          crates.event.updateItem(event);
        }

        router
          .request
          .post("/events/unpublish?team=000000000000000000000004&calendar=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(404)
          .end;

        auto event = crates.event.getItem(publicEventId).exec.front.clone;
        event["visibility"]["isPublic"].to!bool.should.equal(true);
      });

      describeCredentialsRule("bulk unpublish events from other teams", "no", "events", "no rights", { });
    });
  });
});