/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module packages.events.tests.EventOccurrencesMiddleware;

import tests.fixtures;
import vibe.http.common;
import fluent.asserts;
import std.algorithm;
import ogm.defaults.teams;
import ogm.defaults.articles;
import ogm.events.api;
import ogm.middleware.event.EventOccurrencesMiddleware;

alias suite = Spec!({
  URLRouter router;
  Json eventData;
  EventOccurrencesMiddleware middleware;

  describe("Event Occurrences Middleware", {
    beforeEach({
      setupTestData();

      middleware = new EventOccurrencesMiddleware(crates);
      eventData = `{
        "name": "new name",
        "article": "some description",
        "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" },
        "cover": "000000000000000000000001",
        "entries": [],
        "calendar": "000000000000000000000001"
      }`.parseJsonString;
    });

    it("generates an event for a single occurrence", {
      eventData["entries"] = `[{
        "begin": "2018-01-01T12:30:10Z",
        "end": "2018-01-01T13:30:10Z",
        "intervalEnd": "2018-01-01T13:30:10Z",
        "repetition": "norepeat",
        "timezone": "EET"
      }]`.parseJsonString;
      middleware.addOccurrences(eventData);

      expect(eventData["allOccurrences"]).to.equal(parseJsonString(`[{
        "begin": "2018-01-01T14:30:10+02:00",
        "end": "2018-01-01T15:30:10+02:00",
        "dayOfWeek": 0
      }]`));
    });

    it("generates no occurrences when an event has no entries", {
      middleware.addOccurrences(eventData);

      expect(eventData["allOccurrences"]).to.equal(parseJsonString(`[]`));
    });

    it("generates 3 events for a single occurrence that repeats for 3 days", {
      eventData["entries"] = `[{
        "begin": "2018-01-01T12:30:10Z",
        "end": "2018-01-01T13:30:10Z",
        "intervalEnd": "2018-01-03T13:30:10Z",
        "repetition": "daily",
        "timezone": "EET"
      }]`.parseJsonString;
      middleware.addOccurrences(eventData);

      expect(eventData["allOccurrences"]).to.equal(parseJsonString(`[{
        "begin": "2018-01-01T14:30:10+02:00",
        "end": "2018-01-01T15:30:10+02:00",
        "dayOfWeek": 0
      }, {
        "begin": "2018-01-02T14:30:10+02:00",
        "end": "2018-01-02T15:30:10+02:00",
        "dayOfWeek": 1
      }, {
        "begin": "2018-01-03T14:30:10+02:00",
        "end": "2018-01-03T15:30:10+02:00",
        "dayOfWeek": 2
      }]`));
    });

    it("generates 5 events for a weekly occurrence that repeats for 1 month", {
      eventData["entries"] = `[{
        "begin": "2018-01-01T12:30:10Z",
        "end": "2018-01-01T13:30:10Z",
        "intervalEnd": "2018-02-01T13:30:10Z",
        "repetition": "weekly",
        "timezone": "EET"
      }]`.parseJsonString;
      middleware.addOccurrences(eventData);

      expect(eventData["allOccurrences"]).to.equal(parseJsonString(`[{
        "begin": "2018-01-01T14:30:10+02:00",
        "end": "2018-01-01T15:30:10+02:00",
        "dayOfWeek": 0
      }, {
        "begin": "2018-01-08T14:30:10+02:00",
        "end":   "2018-01-08T15:30:10+02:00",
        "dayOfWeek": 0
      }, {
        "begin": "2018-01-15T14:30:10+02:00",
        "end": "2018-01-15T15:30:10+02:00",
        "dayOfWeek": 0
      }, {
        "begin": "2018-01-22T14:30:10+02:00",
        "end": "2018-01-22T15:30:10+02:00",
        "dayOfWeek": 0
      }, {
        "begin": "2018-01-29T14:30:10+02:00",
        "end": "2018-01-29T15:30:10+02:00",
        "dayOfWeek": 0
      }]`));
    });

    it("generates 2 events for a monthly occurrence that repeats for 2 months starting Jan", {
      eventData["entries"] = `[{
        "begin": "2018-01-01T12:30:10Z",
        "end": "2018-01-01T13:30:10Z",
        "intervalEnd": "2018-02-01T13:30:10Z",
        "repetition": "monthly",
        "timezone": "EET"
      }]`.parseJsonString;
      middleware.addOccurrences(eventData);

      expect(eventData["allOccurrences"]).to.equal(parseJsonString(`[{
        "begin": "2018-01-01T14:30:10+02:00",
        "end":   "2018-01-01T15:30:10+02:00",
        "dayOfWeek": 0
      }, {
        "begin": "2018-02-01T14:30:10+02:00",
        "end": "2018-02-01T15:30:10+02:00",
        "dayOfWeek": 3
      }]`));
    });

    it("generates 2 events for a monthly occurrence that repeats for 2 months starting Dec", {
      eventData["entries"] = `[{
        "begin": "2018-12-01T12:30:10Z",
        "end": "2018-12-01T13:30:10Z",
        "intervalEnd": "2019-01-01T13:30:10Z",
        "repetition": "monthly",
        "timezone": "EET"
      }]`.parseJsonString;
      middleware.addOccurrences(eventData);

      expect(eventData["allOccurrences"]).to.equal(parseJsonString(`[{
        "begin": "2018-12-01T14:30:10+02:00",
        "end":   "2018-12-01T15:30:10+02:00",
        "dayOfWeek": 5
      }, {
        "begin": "2019-01-01T14:30:10+02:00",
        "end": "2019-01-01T15:30:10+02:00",
        "dayOfWeek": 1
      }]`));
    });

    it("generates 2 events for a yearly occurrence that repeats for 2 years", {
      eventData["entries"] = `[{
        "begin": "2018-01-01T12:30:10Z",
        "end": "2018-01-01T13:30:10Z",
        "intervalEnd": "2019-01-01T13:30:10Z",
        "repetition": "yearly",
        "timezone": "CET"
      }]`.parseJsonString;
      middleware.addOccurrences(eventData);

      expect(eventData["allOccurrences"]).to.equal(parseJsonString(`[{
        "begin": "2018-01-01T13:30:10+01:00",
        "end": "2018-01-01T14:30:10+01:00",
        "dayOfWeek": 0
      }, {
        "begin": "2019-01-01T13:30:10+01:00",
        "end": "2019-01-01T14:30:10+01:00",
        "dayOfWeek": 1
      }]`));
    });

    it("uses the DST timezone for a monthly event that last 1 year", {
      eventData["entries"] = `[{
        "begin": "2018-12-01T12:30:10Z",
        "end": "2018-12-01T13:30:10Z",
        "intervalEnd": "2019-12-01T13:30:10Z",
        "repetition": "monthly",
        "timezone": "EET"
      }]`.parseJsonString;
      middleware.addOccurrences(eventData);

      expect(eventData["allOccurrences"]).to.equal(parseJsonString(`[{
        "begin": "2018-12-01T14:30:10+02:00",
        "end": "2018-12-01T15:30:10+02:00",
        "dayOfWeek": 5
      }, {
        "begin": "2019-01-01T14:30:10+02:00",
        "end": "2019-01-01T15:30:10+02:00",
        "dayOfWeek": 1
      }, {
        "begin": "2019-02-01T14:30:10+02:00",
        "end": "2019-02-01T15:30:10+02:00",
        "dayOfWeek": 4
      }, {
        "begin": "2019-03-01T14:30:10+02:00",
        "end": "2019-03-01T15:30:10+02:00",
        "dayOfWeek": 4
      }, {
        "begin": "2019-04-01T15:30:10+03:00",
        "end": "2019-04-01T16:30:10+03:00",
        "dayOfWeek": 0
      }, {
        "begin": "2019-05-01T15:30:10+03:00",
        "end": "2019-05-01T16:30:10+03:00",
        "dayOfWeek": 2
      }, {
        "begin": "2019-06-01T15:30:10+03:00",
        "end": "2019-06-01T16:30:10+03:00",
        "dayOfWeek": 5
      }, {
        "begin": "2019-07-01T15:30:10+03:00",
        "end": "2019-07-01T16:30:10+03:00",
        "dayOfWeek": 0
      }, {
        "begin": "2019-08-01T15:30:10+03:00",
        "end": "2019-08-01T16:30:10+03:00",
        "dayOfWeek": 3
      }, {
        "begin": "2019-09-01T15:30:10+03:00",
        "end": "2019-09-01T16:30:10+03:00",
        "dayOfWeek": 6
      }, {
        "begin": "2019-10-01T15:30:10+03:00",
        "end": "2019-10-01T16:30:10+03:00",
        "dayOfWeek": 1
      }, {
        "begin": "2019-11-01T14:30:10+02:00",
        "end": "2019-11-01T15:30:10+02:00",
        "dayOfWeek": 4
      }, {
        "begin": "2019-12-01T14:30:10+02:00",
        "end": "2019-12-01T15:30:10+02:00",
        "dayOfWeek": 6
      }]`));
    });
  });
});
