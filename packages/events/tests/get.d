/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.events.get;

import tests.fixtures;
import vibe.http.common;
import fluent.asserts;
import gis_collective.hmq.broadcast.memory;
import std.algorithm;
import ogm.defaults.articles;
import ogm.defaults.teams;
import ogm.events.api;

alias suite = Spec!({
  URLRouter router;
  Json publicEvent;
  string publicEventId;

  Json privateEvent;
  string privateEventId;

  Json privateEventOtherTeam;
  string privateEventOtherTeamId;

  describe("getting events", {
    beforeEach({
      setupTestData();
      auto broadcast = new MemoryBroadcast;
      router = new URLRouter;
      router.crateSetup.setupEventApi(crates, broadcast);

      crates.setupDefaultTeams;

      publicEvent = crates.event.addItem(`{
        "name": "test",
        "description": "some description",
        "calendar": "000000000000000000000001",
        "location": {
          "type": "Feature",
          "value": "000000000000000000000001"
        },
        "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" },
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);

      publicEventId = publicEvent["_id"].to!string;

      privateEvent = crates.event.addItem(`{
        "name": "test",
        "location": { "type": "Text", "value": "" },
        "calendar": "000000000000000000000001",
        "visibility": { "isDefault": false, "isPublic": false, "team": "000000000000000000000001" },
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);

      privateEventId = privateEvent["_id"].to!string;

      privateEventOtherTeam = crates.event.addItem(`{
        "name": "test",
        "location": { "type": "Text", "value": "" },
        "calendar": "000000000000000000000004",
        "visibility": { "isDefault": false, "isPublic": false, "team": "000000000000000000000004" },
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);

      privateEventOtherTeamId = privateEventOtherTeam["_id"].to!string;
    });

    describeCredentialsRule("get a public event", "yes", "events", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/events/" ~ publicEventId)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["event"]["_id"].to!string.should.equal(publicEventId);
        });
    });

    describeCredentialsRule("get a public event", "yes", "events", "no rights", {
      router
        .request
        .get("/events/" ~ publicEventId)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["event"]["_id"].to!string.should.equal(publicEventId);
        });
    });


    describeCredentialsRule("get a private event from own team", "yes", "events", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/events/" ~ privateEventId)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["event"]["_id"].to!string.should.equal(privateEventId);
        });
    });

    describeCredentialsRule("get a private event from own team", "no", "events", "no rights", {
      router
        .request
        .get("/events/" ~ privateEventId)
        .expectStatusCode(404)
        .end;
    });


    describeCredentialsRule("get a private event from other teams", "yes", "events", "administrator", (string type) {
      router
        .request
        .get("/events/" ~ privateEventOtherTeamId)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["event"]["_id"].to!string.should.equal(privateEventOtherTeamId);
        });
    });

    describeCredentialsRule("get a private event from other teams", "no", "events", ["owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/events/" ~ privateEventOtherTeamId)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(404)
        .end;
    });

    describeCredentialsRule("get a private event from other teams", "no", "events", "no rights", {
      router
        .request
        .get("/events/" ~ privateEventOtherTeamId)
        .expectStatusCode(404)
        .end;
    });

    describeCredentialsRule("filter events by team", "yes", "events", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/events?team=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["events"].byValue.map!(a => a["_id"].to!string).array.should.equal([publicEventId, privateEventId]);
        });
    });

    describeCredentialsRule("filter events by team", "no", "events", "no rights", {
      router
        .request
        .get("/events?team=000000000000000000000001")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["events"].byValue.map!(a => a["_id"].to!string).array.should.equal([publicEventId]);
        });
    });


    describeCredentialsRule("filter events by a team calendar", "yes", "events", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/events?calendar=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["events"].byValue.map!(a => a["_id"].to!string).array.should.equal([publicEventId, privateEventId]);
        });
    });

    describeCredentialsRule("filter events by a team calendar", "yes", "events", "no rights", {
      router
        .request
        .get("/events?calendar=000000000000000000000001")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["events"].byValue.map!(a => a["_id"].to!string).array.should.equal([publicEventId]);
        });
    });


    describeCredentialsRule("filter events by other team calendar", "yes", "events", ["administrator"], (string type) {
      auto calendar = crates.calendar.getItem("000000000000000000000001").and.exec.front;
      calendar["visibility"]["team"] = "000000000000000000000003";
      crates.calendar.updateItem(calendar);

      router
        .request
        .get("/events?calendar=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["events"].byValue.map!(a => a["_id"].to!string).array.should.equal([publicEventId, privateEventId]);
        });
    });

    describeCredentialsRule("filter events by other team calendar", "yes", "events", ["owner", "leader", "member", "guest"], (string type) {
      auto calendar = crates.calendar.getItem("000000000000000000000001").and.exec.front;
      calendar["visibility"]["team"] = "000000000000000000000003";
      crates.calendar.updateItem(calendar);

      router
        .request
        .get("/events?calendar=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["events"].byValue.map!(a => a["_id"].to!string).array.should.equal([publicEventId]);
        });
    });

    describeCredentialsRule("filter events by other team calendar", "yes", "events", "no rights", {
      router
        .request
        .get("/events?calendar=000000000000000000000001")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["events"].byValue.map!(a => a["_id"].to!string).array.should.equal([publicEventId]);
        });
    });

    it("can filter an event by location", {
      router
        .request
        .get("/events?location=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["events"].byValue.map!(a => a["_id"].to!string).array.should.equal([publicEventId]);
        });
    });

    it("can filter an event by repetition", {
      auto event = crates.event.getItem(publicEventId).and.exec.front;
      event["entries"] = `[{
        "begin": "2018-01-01T12:30:10Z",
        "end": "2018-01-01T13:30:10Z",
        "intervalEnd": "2018-01-01T13:30:10Z",
        "repetition": "norepeat",
        "timezone": "EET"
      }]`.parseJsonString;

      crates.event.updateItem(event);

      router
        .request
        .get("/events?repetition=norepeat")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["events"].byValue.map!(a => a["_id"].to!string).array.should.equal([publicEventId]);
        });
    });

    it("can filter an event by icons when all icons match ", {
      auto event = crates.event.getItem(publicEventId).and.exec.front;
      event["icons"] = `["000000000000000000000001","000000000000000000000002"]`.parseJsonString;

      crates.event.updateItem(event);

      router
        .request
        .get("/events?icons=000000000000000000000002")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["events"].byValue.map!(a => a["_id"].to!string).array.should.equal([publicEventId]);
        });
    });

    it("returns nothing when filtering an event by icons when one icon does not match", {
      auto event = crates.event.getItem(publicEventId).and.exec.front;
      event["icons"] = `["000000000000000000000001","000000000000000000000002"]`.parseJsonString;

      crates.event.updateItem(event);

      router
        .request
        .get("/events?icons=000000000000000000000002,000000000000000000000003")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["events"].byValue.map!(a => a["_id"].to!string).array.should.equal([]);
        });
    });

    describe("the after parameter", {
      it("returns nothing when the next event is before the after value", {
        auto event = crates.event.getItem(publicEventId).and.exec.front;
        event["allOccurrences"] = `[{
          "begin": "2018-01-01T14:30:10+02:00",
          "end": "2018-01-01T15:30:10+02:00"
        }, {
          "begin": "2018-01-02T14:30:10+02:00",
          "end": "2018-01-02T15:30:10+02:00",
        }, {
          "begin": "2018-01-03T14:30:10+02:00",
          "end": "2018-01-03T15:30:10+02:00",
        }]`.parseJsonString;

        crates.event.updateItem(event);

        router
          .request
          .get("/events?after=2023-12-13T00%3A00%3A00.000%2B01%3A00")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["events"].byValue.map!(a => a["_id"].to!string).array.should.equal([]);
          });
      });

      it("returns an event when the next event is after the after value", {
        auto event = crates.event.getItem(publicEventId).and.exec.front;
        event["allOccurrences"] = `[{
          "begin": "2018-01-01T14:30:10+02:00",
          "end": "2018-01-01T15:30:10+02:00"
        }, {
          "begin": "2018-01-02T14:30:10+02:00",
          "end": "2018-01-02T15:30:10+02:00",
        }, {
          "begin": "2018-01-03T14:30:10+02:00",
          "end": "2018-01-03T15:30:10+02:00",
        }]`.parseJsonString;

        crates.event.updateItem(event);

        router
          .request
          .get("/events?after=2017-12-13T00%3A00%3A00.000%2B01%3A00")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["events"].byValue.map!(a => a["_id"].to!string).array.should.equal([publicEventId]);
          });
      });
    });

    describe("the before parameter", {
      it("returns nothing when the next event is before the before value", {
        auto event = crates.event.getItem(publicEventId).and.exec.front;
        event["allOccurrences"] = `[{
          "begin": "2018-01-01T14:30:10+02:00",
          "end": "2018-01-01T15:30:10+02:00"
        }, {
          "begin": "2018-01-02T14:30:10+02:00",
          "end": "2018-01-02T15:30:10+02:00",
        }, {
          "begin": "2018-01-03T14:30:10+02:00",
          "end": "2018-01-03T15:30:10+02:00",
        }]`.parseJsonString;

        crates.event.updateItem(event);

        router
          .request
          .get("/events?before=2017-12-13T00%3A00%3A00.000%2B01%3A00")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["events"].byValue.map!(a => a["_id"].to!string).array.should.equal([]);
          });
      });

      it("returns an event when the next event is before the before value", {
        auto event = crates.event.getItem(publicEventId).and.exec.front;
        event["allOccurrences"] = `[{
          "begin": "2018-01-01T14:30:10+02:00",
          "end": "2018-01-01T15:30:10+02:00"
        }, {
          "begin": "2018-01-02T14:30:10+02:00",
          "end": "2018-01-02T15:30:10+02:00",
        }, {
          "begin": "2018-01-03T14:30:10+02:00",
          "end": "2018-01-03T15:30:10+02:00",
        }]`.parseJsonString;

        crates.event.updateItem(event);

        router
          .request
          .get("/events?before=2022-12-13T00%3A00%3A00.000%2B01%3A00")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["events"].byValue.map!(a => a["_id"].to!string).array.should.equal([publicEventId]);
          });
      });
    });

    describe("the onlyFuture parameter", {
      it("returns nothing when the next event is in the past", {
        auto event = crates.event.getItem(publicEventId).and.exec.front;
        event["nextOccurrence"] = `{
          "begin": "2018-01-01T14:30:10+02:00",
          "end": "2018-01-01T15:30:10+02:00"
        }`.parseJsonString;

        crates.event.updateItem(event);

        router
          .request
          .get("/events?onlyFuture=true")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["events"].byValue.map!(a => a["_id"].to!string).array.should.equal([]);
          });
      });

      it("returns the event when the next occurence is in the future", {
        auto event = crates.event.getItem(publicEventId).and.exec.front;
        event["nextOccurrence"] = `{
          "begin": "2100-01-01T14:30:10+02:00",
          "end": "2100-01-01T15:30:10+02:00"
        }`.parseJsonString;

        crates.event.updateItem(event);

        router
          .request
          .get("/events?onlyFuture=true")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["events"].byValue.map!(a => a["_id"].to!string).array.should.equal([publicEventId]);
          });
      });
    });

    describe("the onlyPast parameter", {
      it("returns nothing when the next event is in the future", {
        auto event = crates.event.getItem(publicEventId).and.exec.front;
        event["nextOccurrence"] = `{
          "begin": "2118-01-01T14:30:10+02:00",
          "end": "2118-01-01T15:30:10+02:00"
        }`.parseJsonString;

        crates.event.updateItem(event);

        event = crates.event.getItem("000000000000000000000002").and.exec.front;
        event["nextOccurrence"] = `{
          "begin": "2050-01-01T14:30:10+02:00",
          "end": "2050-01-01T15:30:10+02:00"
        }`.parseJsonString;
        crates.event.updateItem(event);

        event = crates.event.getItem("000000000000000000000003").and.exec.front;
        event["nextOccurrence"] = `{
          "begin": "2050-01-01T14:30:10+02:00",
          "end": "2050-01-01T15:30:10+02:00"
        }`.parseJsonString;
        crates.event.updateItem(event);

        router
          .request
          .get("/events?onlyPast=true")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["events"].byValue.map!(a => a["_id"].to!string).array.should.equal([]);
          });
      });

      it("returns one event when the next occurence is in the past", {
        auto event = crates.event.getItem(publicEventId).and.exec.front;
        event["nextOccurrence"] = `{
          "begin": "2000-01-01T14:30:10+02:00",
          "end": "2000-01-01T15:30:10+02:00"
        }`.parseJsonString;

        crates.event.updateItem(event);

        event = crates.event.getItem("000000000000000000000002").and.exec.front;
        event["nextOccurrence"] = `{
          "begin": "2050-01-01T14:30:10+02:00",
          "end": "2050-01-01T15:30:10+02:00"
        }`.parseJsonString;
        crates.event.updateItem(event);

        event = crates.event.getItem("000000000000000000000003").and.exec.front;
        event["nextOccurrence"] = `{
          "begin": "2050-01-01T14:30:10+02:00",
          "end": "2050-01-01T15:30:10+02:00"
        }`.parseJsonString;
        crates.event.updateItem(event);

        router
          .request
          .get("/events?onlyPast=true")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["events"].byValue.map!(a => a["_id"].to!string).array.should.equal([publicEventId]);
          });
      });

      it("returns one event when the next occurence is null", {
        auto event = crates.event.getItem(publicEventId).and.exec.front;
        event["nextOccurrence"] = null;

        crates.event.updateItem(event);

        router
          .request
          .get("/events?onlyPast=true")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["events"].byValue.map!(a => a["_id"].to!string).array.should.equal(["000000000000000000000001", "000000000000000000000002", "000000000000000000000003"]);
          });
      });
    });

    describe("the date parameter", {
      it("returns nothing when there is no event happening on a day", {
        auto event = crates.event.getItem(publicEventId).and.exec.front;
        event["allOccurrences"] = `[{
          "begin": "2018-01-01T14:30:10+02:00",
          "end": "2018-01-01T15:30:10+02:00"
        }, {
          "begin": "2018-01-02T14:30:10+02:00",
          "end": "2018-01-02T15:30:10+02:00",
        }, {
          "begin": "2018-01-03T14:30:10+02:00",
          "end": "2018-01-03T15:30:10+02:00",
        }]`.parseJsonString;

        crates.event.updateItem(event);

        router
          .request
          .get("/events?date=2018-12-13T00%3A00%3A00.000%2B01%3A00")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["events"].byValue.map!(a => a["_id"].to!string).array.should.equal([]);
          });
      });

      it("returns an event when there is an event happening on a provided date", {
        auto event = crates.event.getItem(publicEventId).and.exec.front;
        event["allOccurrences"] = `[{
          "begin": "2018-01-01T14:30:10+02:00",
          "end": "2018-01-01T15:30:10+02:00"
        }, {
          "begin": "2018-01-02T14:30:10+02:00",
          "end": "2018-01-02T15:30:10+02:00",
        }, {
          "begin": "2018-01-03T14:30:10+02:00",
          "end": "2018-01-03T15:30:10+02:00",
        }]`.parseJsonString;

        event = crates.event.updateItem(event);

        router
          .request
          .get("/events?date=2018-01-02T00%3A00%3A00.000%2B01%3A00")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["events"].byValue.map!(a => a["_id"].to!string).array.should.equal([publicEventId]);
          });
      });
    });

    describe("the day of week parameter", {
      it("returns nothing when there is no event happening on a day", {
        auto event = crates.event.getItem(publicEventId).and.exec.front;
        event["allOccurrences"] = `[{
          "begin": "2018-01-01T14:30:10+02:00",
          "end": "2018-01-01T15:30:10+02:00",
          "dayOfWeek": 1,
        }, {
          "begin": "2018-01-02T14:30:10+02:00",
          "end": "2018-01-02T15:30:10+02:00",
          "dayOfWeek": 1,
        }, {
          "begin": "2018-01-03T14:30:10+02:00",
          "end": "2018-01-03T15:30:10+02:00",
          "dayOfWeek": 1,
        }]`.parseJsonString;

        crates.event.updateItem(event);

        router
          .request
          .get("/events?dayOfWeek=2")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["events"].byValue.map!(a => a["_id"].to!string).array.should.equal([]);
          });
      });

      it("returns an event when there is an event happening on a provided day", {
        auto event = crates.event.getItem(publicEventId).and.exec.front;
        event["allOccurrences"] = `[{
          "begin": "2018-01-01T14:30:10+02:00",
          "end": "2018-01-01T15:30:10+02:00",
          "dayOfWeek": 1,
        }, {
          "begin": "2018-01-02T14:30:10+02:00",
          "end": "2018-01-02T15:30:10+02:00",
          "dayOfWeek": 1,
        }, {
          "begin": "2018-01-03T14:30:10+02:00",
          "end": "2018-01-03T15:30:10+02:00",
          "dayOfWeek": 1,
        }]`.parseJsonString;

        event = crates.event.updateItem(event);

        router
          .request
          .get("/events?dayOfWeek=1")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["events"].byValue.map!(a => a["_id"].to!string).array.should.equal([publicEventId]);
          });
      });

      it("returns an event when there is an event happening on a workday", {
        auto event = crates.event.getItem(publicEventId).and.exec.front;
        event["allOccurrences"] = `[{
          "begin": "2018-01-01T14:30:10+02:00",
          "end": "2018-01-01T15:30:10+02:00",
          "dayOfWeek": 1,
        }, {
          "begin": "2018-01-02T14:30:10+02:00",
          "end": "2018-01-02T15:30:10+02:00",
          "dayOfWeek": 1,
        }, {
          "begin": "2018-01-03T14:30:10+02:00",
          "end": "2018-01-03T15:30:10+02:00",
          "dayOfWeek": 1,
        }]`.parseJsonString;

        event = crates.event.updateItem(event);

        router
          .request
          .get("/events?dayOfWeek=7")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["events"].byValue.map!(a => a["_id"].to!string).array.should.equal([publicEventId]);
          });
      });


      it("returns an event when there is an event happening on a weekend", {
        auto event = crates.event.getItem(publicEventId).and.exec.front;
        event["allOccurrences"] = `[{
          "begin": "2018-01-01T14:30:10+02:00",
          "end": "2018-01-01T15:30:10+02:00",
          "dayOfWeek": 6,
        }, {
          "begin": "2018-01-02T14:30:10+02:00",
          "end": "2018-01-02T15:30:10+02:00",
          "dayOfWeek": 6,
        }, {
          "begin": "2018-01-03T14:30:10+02:00",
          "end": "2018-01-03T15:30:10+02:00",
          "dayOfWeek": 6,
        }]`.parseJsonString;

        event = crates.event.updateItem(event);

        router
          .request
          .get("/events?dayOfWeek=8")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["events"].byValue.map!(a => a["_id"].to!string).array.should.equal([publicEventId]);
          });
      });
    });

    describe("the attributes parameter", {
      it("returns an empty list when the attributes does not match an event", {
        router
          .request
          .get("/events?attributes=some|value")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["events"].byValue.map!(a => a["_id"].to!string).array.should.equal([]);
          });
      });

      it("returns an event when it matches the attributes", {
        auto event = crates.event.getItem(publicEventId).and.exec.front;
        event["attributes"] = `{ "some": "value" }`.parseJsonString;
        event = crates.event.updateItem(event);

        router
          .request
          .get("/events?attributes=some|value")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["events"].byValue.map!(a => a["_id"].to!string).array.should.equal(["000000000000000000000001"]);
          });
      });

      it("returns an event when it matches the location attributes", {
        auto event = crates.event.getItem(publicEventId).and.exec.front;
        event["location"] = `{
          "type": "Feature",
          "value": "000000000000000000000001"
        }`.parseJsonString;
        event = crates.event.updateItem(event);

        auto feature = crates.event.getItem("000000000000000000000001").and.exec.front;
        feature["attributes"] = `{ "icon": { "some": "value" } }`.parseJsonString;
        crates.feature.updateItem(feature);

        router
          .request
          .get("/events?attributes=icon.some|value")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["events"].byValue.map!(a => a["_id"].to!string).array.should.equal(["000000000000000000000001"]);
          });
      });
    });
  });
});
