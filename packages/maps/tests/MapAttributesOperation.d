/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.maps.MapAttributesOperation;

import tests.fixtures;
import vibe.http.common;
import fluent.asserts;
import std.algorithm;
import ogm.defaults.teams;
import ogm.defaults.articles;
import ogm.maps.api;
import ogm.maps.operations.MapAttributesOperation;

alias suite = Spec!({
  URLRouter router;
  Json eventData;
  Json feature;
  MapAttributesOperation middleware;

  describe("Map Attributes Middleware", {
    beforeEach({
      setupTestData();

      feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
      middleware = new MapAttributesOperation(crates);
    });

    it("get a list of values for one attribute that belongs to a feature", {
      feature["attributes"] = `{ "icon": { "some": "value" } }`.parseJsonString;
      crates.feature.updateItem(feature);

      auto values = middleware.getAttributeList("000000000000000000000001", "icon.some", true, "", 0);

      expect(values).to.equal(["value"]);
    });

    it("get a list of values for two attributes that belongs to two features", {
      feature["attributes"] = `{ "icon": { "some": "value1" } }`.parseJsonString;
      crates.feature.updateItem(feature.clone);

      feature["attributes"] = `{ "icon": { "some": "value2" } }`.parseJsonString;
      feature.remove("_id");
      feature = crates.feature.addItem(feature);

      auto values = middleware.getAttributeList("000000000000000000000001", "icon.some", true, "", 0);

      expect(values).to.equal(["value1", "value2"]);
    });

    it("get a list of public values when withPrivateData is false", {
      feature["attributes"] = `{ "icon": { "some": "value1" } }`.parseJsonString;
      crates.feature.updateItem(feature.clone);

      feature["attributes"] = `{ "icon": { "some": "value2" } }`.parseJsonString;
      feature["computedVisibility"]["isPublic"] = false;
      feature.remove("_id");
      feature = crates.feature.addItem(feature);

      auto values = middleware.getAttributeList("000000000000000000000001", "icon.some", false, "", 0);

      expect(values).to.equal(["value1"]);
    });

    it("filters values by a term", {
      feature["attributes"] = `{ "icon": { "some": "abc" } }`.parseJsonString;
      crates.feature.updateItem(feature.clone);

      feature["attributes"] = `{ "icon": { "some": "def" } }`.parseJsonString;
      feature.remove("_id");
      feature = crates.feature.addItem(feature);

      auto values = middleware.getAttributeList("000000000000000000000001", "icon.some", true, "BC", 0);

      expect(values).to.equal(["abc"]);
    });

    it("can limit the result count", {
      feature["attributes"] = `{ "icon": { "some": "value1" } }`.parseJsonString;
      crates.feature.updateItem(feature.clone);

      feature["attributes"] = `{ "icon": { "some": "value2" } }`.parseJsonString;
      feature.remove("_id");
      feature = crates.feature.addItem(feature);

      auto values = middleware.getAttributeList("000000000000000000000001", "icon.some", true, "", 1);

      expect(values).to.equal(["value1"]);
    });
  });
});
