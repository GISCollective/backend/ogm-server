/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.maps.put;

import tests.fixtures;

import std.algorithm;
import std.array;
import ogm.maps.api;

import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({
  URLRouter router;
  Json data;

  describe("PUT", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupMapApi(crates, new MemoryBroadcast);

      data = `{ "map": {
        "iconSets":{
          "useCustomList": true,
          "list": ["000000000000000000000001"]
        },
        "name":"map1",
        "description":"",
        "area":{"coordinates":[[[-8,4],[-8,6],[-9,6],[-9,4],[-8,4]]],"type":"Polygon"},
        "visibility": {
          "team":"000000000000000000000001",
          "isPublic": true,
          "isDefault": false
        },
        "cover": "000000000000000000000001"
      }}`.parseJsonString;
    });


    describeCredentialsRule("update maps for your teams", "yes", "maps", ["owner", "administrator"], (string type) {
      auto initialSize = crates.map.get.size;

      router
        .request
        .put("/maps/000000000000000000000002")
        .send(data)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto item = crates.map.getItem("000000000000000000000002").exec.front.to!string.parseJsonString;
          item["canEdit"] = true;

          response.bodyJson["map"].should.equal(item);
        });
    });

    describeCredentialsRule("update maps for your teams", "no", "maps", ["leader", "member", "guest"], (string type) {
      auto initialSize = crates.map.get.size;

      router
        .request
        .put("/maps/000000000000000000000002")
        .send(data)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal("{\"errors\": [{
                \"description\": \"You don't have enough rights to edit `000000000000000000000002`.\",
                \"title\": \"Forbidden\",
                \"status\": 403
              }]}".parseJsonString);
        });
    });

    describeCredentialsRule("update maps for your teams", "-", "maps", "no rights", {
      it("should return an error", {
        router
          .request
          .put("/maps/000000000000000000000002")
          .send(data)
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "error": "Authorization required"
            }`.parseJsonString);
          });
      });
    });

    describeCredentialsRule("use iconsets from other teams", "no", "maps", ["owner", "administrator", "leader", "member", "guest"], (string type) {
      auto initialSize = crates.map.get.size;

      data["map"]["iconSets"] = `{
        "useCustomList": true,
        "list": ["000000000000000000000003"]
      }`.parseJsonString;

      router
        .request
        .put("/maps/000000000000000000000002")
        .send(data)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(400)
        .end((Response response) => () {
          response.bodyJson.should.equal("{\"errors\": [{
                \"description\": \"You can't use the icon set `set3` with id `000000000000000000000003` because it belongs to another team\",
                \"title\": \"Validation error\",
                \"status\": 400
              }]}".parseJsonString);
        });
    });

    describeCredentialsRule("use iconsets from other teams", "-", "maps", "no rights", {
      it("should return an error", {
        router
          .request
          .put("/maps/000000000000000000000002")
          .send(data)
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "error": "Authorization required"
            }`.parseJsonString);
          });
      });
    });


    describeCredentialsRule("update maps for other teams", "yes", "maps", ["administrator"], (string type) {
      router
        .request
        .put("/maps/000000000000000000000003")
        .send(data)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto item = crates.map.getItem("000000000000000000000003").exec.front.to!string.parseJsonString;
          item["canEdit"] = true;

          response.bodyJson["map"].should.equal(item);
        });
    });

    describeCredentialsRule("update maps for other teams", "no", "maps", ["owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .put("/maps/000000000000000000000003")
        .send(data)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(404)
        .end((Response response) => () {
          response.bodyJson.should.equal("{
            \"errors\": [
              {
                \"description\": \"Item `000000000000000000000003` not found.\",
                \"title\": \"Crate not found\",
                \"status\": 404
              }
            ]
          }".parseJsonString);
        });
    });

    describeCredentialsRule("update maps for other teams", "no", "maps", "no rights", {
      it("should return an error", {
        router
          .request
          .put("/maps/000000000000000000000003")
          .send(data)
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "error": "Authorization required"
            }`.parseJsonString);
          });
      });
    });


    describeCredentialsRule("change the `original author` field", "yes", "maps", ["administrator"], (string type) {
      auto map = data.clone;
      map["map"]["info"] = Json.emptyObject;
      map["map"]["info"]["originalAuthor"] = "@test";

      router
        .request
        .put("/maps/000000000000000000000002")
        .send(map)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto item = crates.map.getItem("000000000000000000000002").exec.front.to!string.parseJsonString;
          item["canEdit"] = true;

          response.bodyJson["map"].should.equal(item);
        });
    });

    describeCredentialsRule("change the `original author` field", "yes", "maps", "owner", (string type) {
      auto map = data.clone;
      map["map"]["info"] = Json.emptyObject;
      map["map"]["info"]["originalAuthor"] = "@test";

      router
        .request
        .put("/maps/000000000000000000000002")
        .send(map)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto item = crates.map.getItem("000000000000000000000002").exec.front.to!string.parseJsonString;
          item["canEdit"] = true;
          item["info"]["originalAuthor"] = "@test";

          response.bodyJson["map"].should.equal(item);
        });
    });

    describeCredentialsRule("change the `original author` field", "no", "maps", ["leader", "member", "guest"], (string type) {
      auto map = data.clone;
      map["map"]["info"] = Json.emptyObject;
      map["map"]["info"]["originalAuthor"] = "@test";

      router
        .request
        .put("/maps/000000000000000000000002")
        .send(map)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal("{
            \"errors\": [
              {
                \"description\": \"You don't have enough rights to edit `000000000000000000000002`.\",
                \"title\": \"Forbidden\",
                \"status\": 403
              }
            ]
          }".parseJsonString);
        });
    });

    describeCredentialsRule("change the `original author` field", "-", "maps", "no rights", {
      it("should return an error", {
        auto map = data.clone;
        map["map"]["info"] = Json.emptyObject;
        map["map"]["info"]["originalAuthor"] = "@test";

        router
          .request
          .put("/maps/000000000000000000000002")
          .send(map)
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "error": "Authorization required"
            }`.parseJsonString);
          });
      });
    });


    describeCredentialsRule("change the `created on` field", "yes", "maps", ["administrator"], (string type) {
      auto map = data.clone;
      map["map"]["info"] = Json.emptyObject;
      map["map"]["info"]["createdOn"] = "2000-01-01T01:01:01Z";

      router
        .request
        .put("/maps/000000000000000000000002")
        .send(map)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["map"]["info"]["createdOn"].to!string.should.equal("2000-01-01T01:01:01Z");
        });
    });

    describeCredentialsRule("change the `created on` field", "yes", "maps", "owner", (string type) {
      auto map = data.clone;
      map["map"]["info"] = Json.emptyObject;
      map["map"]["info"]["createdOn"] = "2000-01-01T01:01:01Z";


      router
        .request
        .put("/maps/000000000000000000000002")
        .send(map)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto item = crates.map.getItem("000000000000000000000002").exec.front.to!string.parseJsonString;
          item["info"]["createdOn"] = "2000-01-01T01:01:01Z";
          item["canEdit"] = true;

          response.bodyJson["map"].should.equal(item);
        });
    });

    describeCredentialsRule("change the `created on` field", "no", "maps", ["leader", "member", "guest"], (string type) {
      auto map = data.clone;
      map["map"]["info"] = Json.emptyObject;
      map["map"]["info"]["createdOn"] = "2000-01-01T01:01:01Z";

      router
        .request
        .put("/maps/000000000000000000000002")
        .send(map)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal("{
            \"errors\": [
              {
                \"description\": \"You don't have enough rights to edit `000000000000000000000002`.\",
                \"title\": \"Forbidden\",
                \"status\": 403
              }
            ]
          }".parseJsonString);
        });
    });

    describeCredentialsRule("change the `created on` field", "-", "maps", "no rights", {
      it("should return an error", {
        auto map = data.clone;
        map["map"]["info"] = Json.emptyObject;
        map["map"]["info"]["createdOn"] = "2000-01-01T01:01:01Z";

        router
          .request
          .put("/maps/000000000000000000000002")
          .send(map)
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "error": "Authorization required"
            }`.parseJsonString);
          });
      });
    });


    describeCredentialsRule("change the `hideOnMainMap` field", "yes", "maps", ["administrator"], (string type) {
      auto map = data.clone;
      map["map"]["hideOnMainMap"] = true;

      router
        .request
        .put("/maps/000000000000000000000002")
        .send(map)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["map"]["hideOnMainMap"].to!string.should.equal("true");
        });
    });

    describeCredentialsRule("change the `hideOnMainMap` field", "no", "maps", ["owner"], (string type) {
      auto map = data.clone;
      map["map"]["hideOnMainMap"] = true;

      router
        .request
        .put("/maps/000000000000000000000002")
        .send(map)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal("{
            \"errors\": [
              {
                \"description\": \"You need to be administrator to change or set 'hideOnMainMap'.\",
                \"title\": \"Forbidden\",
                \"status\": 403
              }
            ]
          }".parseJsonString);
        });
    });


    describeCredentialsRule("change the `hideOnMainMap` field", "no", "maps", ["leader", "member", "guest"], (string type) {
      auto map = data.clone;
      map["map"]["hideOnMainMap"] = true;

      router
        .request
        .put("/maps/000000000000000000000002")
        .send(map)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal("{
            \"errors\": [
              {
                \"description\": \"You don't have enough rights to edit `000000000000000000000002`.\",
                \"title\": \"Forbidden\",
                \"status\": 403
              }
            ]
          }".parseJsonString);
        });
    });

    describeCredentialsRule("change the `hideOnMainMap` field", "-", "maps", "no rights", {
      auto map = data.clone;
      map["map"]["hideOnMainMap"] = true;

      router
        .request
        .put("/maps/000000000000000000000002")
        .send(map)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "error": "Authorization required"
          }`.parseJsonString);
        });
    });


    describeCredentialsRule("change the `isIndex` field", "yes", "maps", ["administrator"], (string type) {
      auto map = data.clone;
      map["map"]["isIndex"] = true;

      router
        .request
        .put("/maps/000000000000000000000002")
        .send(map)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["map"]["isIndex"].to!string.should.equal("true");
        });
    });

    describeCredentialsRule("change the `isIndex` field", "no", "maps", ["owner"], (string type) {
      auto map = data.clone;
      map["map"]["isIndex"] = true;

      router
        .request
        .put("/maps/000000000000000000000002")
        .send(map)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal("{
            \"errors\": [
              {
                \"description\": \"You need to be administrator to change or set 'isIndex'.\",
                \"title\": \"Forbidden\",
                \"status\": 403
              }
            ]
          }".parseJsonString);
        });
    });

    describeCredentialsRule("change the `isIndex` field", "no", "maps", ["leader", "member", "guest"], (string type) {
      auto map = data.clone;
      map["map"]["isIndex"] = true;

      router
        .request
        .put("/maps/000000000000000000000002")
        .send(map)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal("{
            \"errors\": [
              {
                \"description\": \"You don't have enough rights to edit `000000000000000000000002`.\",
                \"title\": \"Forbidden\",
                \"status\": 403
              }
            ]
          }".parseJsonString);
        });
    });

    describeCredentialsRule("change the `isIndex` field", "-", "maps", "no rights", {
      auto map = data.clone;
      map["map"]["isIndex"] = true;

      router
        .request
        .put("/maps/000000000000000000000002")
        .send(map)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "error": "Authorization required"
          }`.parseJsonString);
        });
    });


    describe("a request with an administrator token", {
      it("should not delete the system cover on update", {
        data["map"]["cover"] = "000000000000000000000002";

        router
          .request
          .put("/maps/000000000000000000000003")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            crates.picture.get.exec
              .filter!(a => a["_id"] == "000000000000000000000001")
                .array.length.should.equal(1);
          });
      });

      describe("for a map with a custom cover", {
        Picture customCover;

        beforeEach({
          customCover.name = "custom cover";
          customCover.owner = "";
          customCover.picture = new PictureFile;
          auto result = crates.picture.addItem(customCover.serializeToJson);
          customCover._id = ObjectId.fromString(result["_id"].to!string);

          createMap(Map(ObjectId.fromString("5"), "custom map", Json(""),
            Visibility(team3, true), Polygon([[[-7, 3], [-7, 5], [-8, 5], [-8, 3], [-7,3]]]),
            customCover, IconSets(), BaseMaps(), "", Clock.currTime, Clock.currTime, ModelInfo()));
        });

        it("should delete the previous cover on update", {
          data["map"]["cover"] = "000000000000000000000001";

          router
            .request
            .put("/maps/000000000000000000000005")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .send(data)
            .expectStatusCode(200)
            .end((Response response) => () {
              crates.picture.get.exec
                .filter!(a => a["name"] == "custom cover")
                  .array.length.should.equal(0);
            });
        });

        it("should not delete the previous cover if the cover is not changed", {
          data["map"]["cover"] = customCover._id.toString;

          router
            .request
            .put("/maps/000000000000000000000005")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .send(data)
            .expectStatusCode(200)
            .end((Response response) => () {
              crates.picture.get.exec
                .filter!(a => a["name"] == "custom cover")
                  .array.length.should.equal(1);
            });
        });
      });
    });

    describe("a request with an owner token", {
      it("should not update the change index when the data is not changed", {
        data["map"] = crates.map.getItem("000000000000000000000002").exec.front;

        router
          .request
          .put("/maps/000000000000000000000002")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["map"]["info"].should.equal(`{
              "changeIndex": 0,
              "createdOn": "2015-01-01T00:00:00Z",
              "lastChangeOn": "2015-01-01T00:00:00Z",
              "originalAuthor": "",
              "author": ""
            }`.parseJsonString);
          });
      });
    });
  });
});
