/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.maps.post;

import tests.fixtures;

import std.algorithm;
import std.array;
import ogm.maps.api;

import gis_collective.hmq.broadcast.memory;
import ogm.middleware.GlobalAccess;

alias suite = Spec!({
  URLRouter router;
  Json data;
  Json dataOtherTeam;

  describe("POST", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupMapApi(crates, new MemoryBroadcast);

      data = `{ "map": {
        "iconSets":{
          "useCustomList": true,
          "list": ["000000000000000000000001"]
        },
        "name":"map1",
        "description":"",
        "area":{"coordinates":[[[-8,4],[-8,6],[-9,6],[-9,4],[-8,4]]],"type":"Polygon"},
        "visibility": {
          "team":"000000000000000000000001",
          "isPublic": true
        },
        "cover": "000000000000000000000001"
      }}`.parseJsonString;

      dataOtherTeam = `{ "map": {
        "iconSets":{
          "useCustomList": true,
          "list": ["000000000000000000000001"]
        },
        "name":"map1",
        "description":"",
        "area":{"coordinates":[[[-8,4],[-8,6],[-9,6],[-9,4],[-8,4]]],"type":"Polygon"},
        "visibility": {
          "team":"000000000000000000000004",
          "isPublic": true
        },
        "cover": "000000000000000000000001"
      }}`.parseJsonString;
    });

    describe("when the access.allowManageWithoutTeams preference is false", {
      Token token;

      beforeEach({
        GlobalAccessMiddleware.resetInstance;
        auto preference = Json.emptyObject;
        preference["name"] = "access.allowManageWithoutTeams";
        preference["value"] = "false";

        crates.preference.addItem(preference);

        UserModel user;
        user._id = ObjectId.fromString("6").toString;
        user.username = "test";
        user.email = "other@gmail.com";
        userCollection.createUser(user, "password");

        token = userCollection.createToken("other@gmail.com", Clock.currTime + 3600.seconds, ["doStuff"], "Bearer");
      });

      afterEach({
        GlobalAccessMiddleware.resetInstance;
      });

      it("does not allow creating maps", {
        auto initialSize = crates.map.get.size;

        router
          .request
          .post("/maps")
          .send(data)
          .header("Authorization", "Bearer " ~ token.name)
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal((`{"errors": [{
                "description": "You can't add an item for the team ` ~ "`000000000000000000000001`" ~ `.",
                "status": 403,
                "title": "Forbidden"
              }]}`).parseJsonString);
            crates.map.get.size.should.equal(initialSize);
          });
      });
    });

    describeCredentialsRule("create maps for your teams", "yes", "maps", ["owner", "administrator"], (string type) {
      auto initialSize = crates.map.get.size;

      router
        .request
        .post("/maps")
        .send(data)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto item = crates.map.getItem("000000000000000000000005").exec.front.to!string.parseJsonString;
          item["canEdit"] = true;

          response.bodyJson["map"].should.equal(item);
          crates.map.get.size.should.equal(initialSize + 1);
        });
    });

    describeCredentialsRule("create maps for your teams", "no", "maps", ["leader", "member", "guest"], (string type) {
      auto initialSize = crates.map.get.size;

      router
        .request
        .post("/maps")
        .send(data)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{"errors": [{
              "description": "You don't have enough rights to add an item.",
              "status": 403,
              "title": "Forbidden"
            }]}`).parseJsonString);
          crates.map.get.size.should.equal(initialSize);
        });
    });

    describeCredentialsRule("create maps for your teams", "-", "maps", "no rights", {
      router
        .request
        .post("/maps")
        .send(data)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "error": "Authorization required"
          }`.parseJsonString);
        });
    });


    describeCredentialsRule("create maps for other teams", "yes", "maps", "administrator", (string type) {
      auto initialSize = crates.map.get.size;

      router
        .request
        .post("/maps")
        .send(dataOtherTeam)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto item = crates.map.getItem("000000000000000000000005").exec.front.to!string.parseJsonString;
          item["canEdit"] = true;

          response.bodyJson["map"].should.equal(item);
          crates.map.get.size.should.equal(initialSize + 1);
        });
    });

    describeCredentialsRule("create maps for other teams", "no", "maps", ["owner", "leader", "member", "guest"], (string type) {
      auto initialSize = crates.map.get.size;

      router
        .request
        .post("/maps")
        .send(dataOtherTeam)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{"errors": [{
              "description": "You can't add an item for the team ` ~ "`000000000000000000000004`" ~ `.",
              "status": 403,
              "title": "Forbidden"
            }]}`).parseJsonString);
          crates.map.get.size.should.equal(initialSize);
        });
    });

    describeCredentialsRule("create maps for other teams", "no", "maps", "no rights", {
      router
        .request
        .post("/maps")
        .send(dataOtherTeam)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "error": "Authorization required"
          }`.parseJsonString);
        });
    });


    describeCredentialsRule("create maps that are hidden on the main map", "yes", "maps", "administrator", (string type) {
      auto initialSize = crates.map.get.size;

      auto map = data.clone;
      map["map"]["hideOnMainMap"] = true;

      router
        .request
        .post("/maps")
        .send(map)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto item = crates.map.getItem("000000000000000000000005").exec.front.to!string.parseJsonString;
          item["canEdit"] = true;

          response.bodyJson["map"].should.equal(item);
          crates.map.get.size.should.equal(initialSize + 1);
        });
    });

    describeCredentialsRule("create maps that are hidden on the main map", "no", "maps", "owner", (string type) {
      auto initialSize = crates.map.get.size;

      auto map = data.clone;
      map["map"]["hideOnMainMap"] = true;

      router
        .request
        .post("/maps")
        .send(map)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{"errors": [{
              "description": "You need to be administrator to change or set 'hideOnMainMap'.",
              "status": 403,
              "title": "Forbidden"
            }]}`).parseJsonString);
          crates.map.get.size.should.equal(initialSize);
        });
    });

    describeCredentialsRule("create maps that are hidden on the main map", "no", "maps", ["leader", "member", "guest"], (string type) {
      auto initialSize = crates.map.get.size;

      auto map = data.clone;
      map["map"]["hideOnMainMap"] = true;

      router
        .request
        .post("/maps")
        .send(map)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{"errors": [{
              "description": "You don't have enough rights to add an item.",
              "status": 403,
              "title": "Forbidden"
            }]}`).parseJsonString);
          crates.map.get.size.should.equal(initialSize);
        });
    });

    describeCredentialsRule("create maps that are hidden on the main map", "no", "maps", "no rights", {
      auto map = data.clone;
      map["map"]["hideOnMainMap"] = false;

      router
        .request
        .post("/maps")
        .send(map)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "error": "Authorization required"
          }`.parseJsonString);
        });
    });


    describeCredentialsRule("create indexable maps", "yes", "maps", "administrator", (string type) {
      auto initialSize = crates.map.get.size;

      auto map = data.clone;
      map["map"]["isIndex"] = true;

      router
        .request
        .post("/maps")
        .send(map)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto item = crates.map.getItem("000000000000000000000005").exec.front.to!string.parseJsonString;
          item["canEdit"] = true;

          response.bodyJson["map"].should.equal(item);
          crates.map.get.size.should.equal(initialSize + 1);
        });
    });

    describeCredentialsRule("create indexable maps", "no", "maps", "owner", (string type) {
      auto initialSize = crates.map.get.size;

      auto map = data.clone;
      map["map"]["isIndex"] = true;

      router
        .request
        .post("/maps")
        .send(map)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{"errors": [{
              "description": "You need to be administrator to change or set 'isIndex'.",
              "status": 403,
              "title": "Forbidden"
            }]}`).parseJsonString);
          crates.map.get.size.should.equal(initialSize);
        });
    });

    describeCredentialsRule("create indexable maps", "no", "maps", ["leader", "member", "guest"], (string type) {
      auto initialSize = crates.map.get.size;

      auto map = data.clone;
      map["map"]["isIndex"] = true;

      router
        .request
        .post("/maps")
        .send(map)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{"errors": [{
              "description": "You don't have enough rights to add an item.",
              "status": 403,
              "title": "Forbidden"
            }]}`).parseJsonString);
          crates.map.get.size.should.equal(initialSize);
        });
    });

    describeCredentialsRule("create indexable maps", "no", "maps", "no rights", {
      auto map = data.clone;
      map["map"]["isIndex"] = true;

      router
        .request
        .post("/maps")
        .send(map)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "error": "Authorization required"
          }`.parseJsonString);
        });
    });

    describe("a request with an administrator token", {
      it("should accept a map with a null cover", {
        auto dataWithoutCover = data.to!string.parseJsonString;
        dataWithoutCover["map"]["cover"] = Json(null);

        router
          .request
          .post("/maps")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .send(dataWithoutCover)
          .expectStatusCode(200)
          .end((Response response) => () {
            auto item = crates.map.getItem("000000000000000000000005").exec.front.to!string.parseJsonString;
            item["canEdit"] = true;

            response.bodyJson["map"].should.equal(item);
          });
      });

      it("should accept a map with a undefined cover", {
        auto dataWithoutCover = data.to!string.parseJsonString;
        dataWithoutCover["map"]["cover"] = Json.undefined;

        router
          .request
          .post("/maps")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .send(dataWithoutCover)
          .expectStatusCode(200)
          .end((Response response) => () {
            auto item = crates.map.getItem("000000000000000000000005").exec.front.to!string.parseJsonString;
            item["canEdit"] = true;

            response.bodyJson["map"].should.equal(item);
          });
      });

      it("should accept a map with a missing cover", {
        auto dataWithoutCover = data.to!string.parseJsonString;
        dataWithoutCover["map"].remove("cover");

        router
          .request
          .post("/maps")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .send(dataWithoutCover)
          .expectStatusCode(200)
          .end((Response response) => () {
            auto item = crates.map.getItem("000000000000000000000005").exec.front.to!string.parseJsonString;
            item["canEdit"] = true;

            response.bodyJson["map"].should.equal(item);
          });
      });

      it("should accept a map with a missing area", {
        auto dataWithoutArea= data.to!string.parseJsonString;
        dataWithoutArea["map"].remove("area");

        router
          .request
          .post("/maps")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .send(dataWithoutArea)
          .expectStatusCode(200)
          .end((Response response) => () {
            auto item = crates.map.getItem("000000000000000000000005").exec.front.to!string.parseJsonString;
            item["canEdit"] = true;

            response.bodyJson["map"].should.equal(item);
          });
      });

      it("should add a map without icon sets", {
        data["map"]["iconSets"] = Json.emptyObject;

        router
          .request
          .post("/maps")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            auto item = crates.map.getItem("000000000000000000000005").exec.front.to!string.parseJsonString;
            item["canEdit"] = true;
            item["iconSets"]["list"] = [ "000000000000000000000001" ].serializeToJson;

            response.bodyJson["map"].should.equal(item);
          });
      });

      it("should add a map with the default icon sets and ignore the list when useCustomList is true", {
        data["map"]["iconSets"] = `{
          "useCustomList": false,
          "list": [ "000000000000000000000001", "000000000000000000000002","000000000000000000000003", "000000000000000000000004" ]
        }`.parseJsonString;

        router
          .request
          .post("/maps")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            auto item = crates.map.getItem("000000000000000000000005").exec.front.to!string.parseJsonString;
            item["canEdit"] = true;
            item["iconSets"]["list"] = [ "000000000000000000000001" ].serializeToJson;

            response.bodyJson["map"].should.equal(item);
          });
      });
    });

    describe("a request with an owner token", {
      it("should add a map without icon sets", {
        data["map"]["iconSets"] = Json.emptyArray;

        router
          .request
          .post("/maps")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            auto item = crates.map.getItem("000000000000000000000005").exec.front.to!string.parseJsonString;
            item["canEdit"] = true;
            item["iconSets"]["list"] = [ "000000000000000000000001" ].serializeToJson;

            response.bodyJson["map"].should.equal(item);
          });
      });

      it("should add a map with the default icon sets and ignore the list when useCustomList is true", {
        data["map"]["iconSets"] = `{
          "useCustomList": false,
          "list": [ "000000000000000000000001", "000000000000000000000002","000000000000000000000003", "000000000000000000000004" ]
        }`.parseJsonString;

        router
          .request
          .post("/maps")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            auto item = crates.map.getItem("000000000000000000000005").exec.front.to!string.parseJsonString;
            item["canEdit"] = true;
            item["iconSets"]["list"] = [ "000000000000000000000001" ].serializeToJson;

            response.bodyJson["map"].should.equal(item);
          });
      });

      it("should not add a map with an icon set that the user does not has access to", {
        auto dataOtherSet = data.toString.parseJsonString;
        dataOtherSet["map"]["iconSets"] = `{
          "useCustomList": true,
          "list": [ "000000000000000000000003" ]
        }`.parseJsonString;

        router
          .request
          .post("/maps")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .send(dataOtherSet)
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal("{
              \"errors\":[
                {\"description\":\"You can't use the icon set `set3` with id `000000000000000000000003` because it belongs to another team\",\"status\":400,\"title\":\"Validation error\"}
            ]}".parseJsonString);
          });
      });

      it("should not add a map with an icon set that the user does not has access to, but it is public", {
        auto dataOtherSet = data.toString.parseJsonString;
        dataOtherSet["map"]["iconSets"] = `{
          "useCustomList": true,
          "list": [ "000000000000000000000003" ]
        }`.parseJsonString;

        router
          .request
          .post("/maps")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .send(dataOtherSet)
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal("{
              \"errors\":[
                {\"description\":\"You can't use the icon set `set3` with id `000000000000000000000003` because it belongs to another team\",
                  \"status\":400,\"title\":\"Validation error\"}
                ]
              }".parseJsonString);
          });
      });
    });
  });
});
