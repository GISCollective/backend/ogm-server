/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.maps.get.list.admin;

import tests.fixtures;

import std.algorithm;
import std.array;
import ogm.maps.api;

import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({
  URLRouter router;

  describe("with an admin token", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupMapApi(crates, new MemoryBroadcast);
    });

    it("should return all maps" , {
      router
        .request
        .get("/maps")
        .header("Authorization", "Bearer " ~ administratorToken.name)
        .expectStatusCode(200)
        .end((Response response) => () {
          auto item1 = crates.map.getItem("000000000000000000000001").exec.front.to!string.parseJsonString;
          item1["canEdit"] = true;

          auto item2 = crates.map.getItem("000000000000000000000002").exec.front.to!string.parseJsonString;
          item2["canEdit"] = true;

          auto item3 = crates.map.getItem("000000000000000000000003").exec.front.to!string.parseJsonString;
          item3["canEdit"] = true;

          auto item4 = crates.map.getItem("000000000000000000000003").exec.front.to!string.parseJsonString;
          item4["canEdit"] = true;

          auto maps = cast(Json[]) response.bodyJson["maps"];
          maps.length.should.equal(4);

          maps.should.contain(item1);
          maps.should.contain(item2);
          maps.should.contain(item3);
          maps.should.contain(item4);
        });
    });

    it("should get all user maps when the 'edit' query string is present", {
      router
        .request
        .get("/maps?edit=true")
        .header("Authorization", "Bearer " ~ administratorToken.name)
        .expectStatusCode(200)
        .end((Response response) => () {
          auto maps = cast(Json[]) response.bodyJson["maps"];
          maps.length.should.equal(0);
        });
    });

    it("should get all maps when the 'edit' query string is present and `all` is true", {
      router
        .request
        .get("/maps?edit=true&all=true")
        .header("Authorization", "Bearer " ~ administratorToken.name)
        .expectStatusCode(200)
        .end((Response response) => () {

          auto item1 = crates.map.getItem("000000000000000000000001").exec.front.to!string.parseJsonString;
          item1["canEdit"] = true;

          auto item2 = crates.map.getItem("000000000000000000000002").exec.front.to!string.parseJsonString;
          item2["canEdit"] = true;

          auto item3 = crates.map.getItem("000000000000000000000003").exec.front.to!string.parseJsonString;
          item3["canEdit"] = true;

          auto item4 = crates.map.getItem("000000000000000000000003").exec.front.to!string.parseJsonString;
          item4["canEdit"] = true;

          auto maps = cast(Json[]) response.bodyJson["maps"];
          maps.length.should.equal(4);

          maps.should.contain(item1);
          maps.should.contain(item2);
          maps.should.contain(item3);
          maps.should.contain(item4);
        });
    });

    it("should get all maps containing a point", {
      router
        .request
        .get("/maps?containing=-73.856077,40.848447")
        .header("Authorization", "Bearer " ~ administratorToken.name)
        .expectStatusCode(200)
        .end((Response response) => () {
          auto item = crates.map.getItem("000000000000000000000001").exec.front.to!string.parseJsonString;
          item["canEdit"] = true;

          auto maps = cast(Json[]) response.bodyJson["maps"];
          maps.length.should.equal(1);

          maps.should.contain(item);
        });
    });

    it("should get all maps containing a point specified with two containing query params", {
      router
        .request
        .get("/maps?containing=-73.856077&containing=40.848447")
        .header("Authorization", "Bearer " ~ administratorToken.name)
        .expectStatusCode(200)
        .end((Response response) => () {
          auto item = crates.map.getItem("000000000000000000000001").exec.front.to!string.parseJsonString;
          item["canEdit"] = true;

          auto maps = cast(Json[]) response.bodyJson["maps"];
          maps.length.should.equal(1);

          maps.should.contain(item);
        });
    });

    it("should get the maps of the teams that the user belongs when the 'canAdd' query param is true", {
      router
        .request
        .get("/maps?canAdd=true")
        .header("Authorization", "Bearer " ~ administratorToken.name)
        .expectStatusCode(200)
        .end((Response response) => () {
          auto maps = cast(Json[]) response.bodyJson["maps"];
          maps.length.should.equal(0);
          maps.map!`a["_id"]`.array.should.equal([]);
        });
    });

    it("should get all maps of the teams that the user does not belong when the 'canAdd' query param is false", {
      router
        .request
        .get("/maps?canAdd=false")
        .header("Authorization", "Bearer " ~ administratorToken.name)
        .expectStatusCode(200)
        .end((Response response) => () {
          auto maps = cast(Json[]) response.bodyJson["maps"];
          maps.length.should.equal(4);
          maps.map!(a => a["_id"].to!string).array.should.equal(["000000000000000000000001", "000000000000000000000002", "000000000000000000000003", "000000000000000000000004"]);
        });
    });

    describe("when there is an index map", {
      beforeEach({
        createMap(Map(
          ObjectId.fromString("5"), "indexed", Json(""),
          Visibility(team1, true), Polygon([[[-7, 3], [-7, 5], [-8, 5], [-8, 3], [-7,3]]]),
          cover, IconSets(true, [iconSet1]), BaseMaps(), "", SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-02-01T00:00:00Z"),
          ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z")),
          Picture.init, Mask(), true));
      });

      it("should be returned to the list of maps", {
        router
          .request
          .get("/maps")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            auto maps = cast(Json[]) response.bodyJson["maps"];
            maps.map!(a => a["_id"].to!string).should.contain("000000000000000000000005");
          });
      });
    });
  });
});
