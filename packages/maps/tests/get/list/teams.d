/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.maps.get.list.teams;

import tests.fixtures;

import std.algorithm;
import std.array;
import gis_collective.hmq.broadcast.memory;
import ogm.maps.api;

alias suite = Spec!({
  URLRouter router;

  describe("without a token", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupMapApi(crates , new MemoryBroadcast);
    });

    describe("given a team selector", {
      beforeEach({
        auto map = crates.map.getItem("000000000000000000000002").and.exec.front;

        map["team"] = "000000000000000000000001";
        crates.map.updateItem(map);
      });

      it("should return only the maps that match the team", {
        router
        .request
        .get("/maps?team=000000000000000000000003")
        .expectStatusCode(200)
        .end((Response response) => () {
          auto item = crates.map.getItem("000000000000000000000003").exec.front.to!string.parseJsonString;

          response.bodyJson["maps"].length.should.equal(1);
          response.bodyJson["maps"][0].should.equal(item);
        });
      });

      it("should not return private maps to users that have no access", {
        router
        .request
        .get("/maps?team=000000000000000000000001")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["maps"].length.should.equal(1);
        });
      });
    });
  });
});
