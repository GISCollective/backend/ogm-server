/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.maps.get.list.owner;

import tests.fixtures;

import std.algorithm;
import std.array;
import ogm.maps.api;

import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({
  URLRouter router;

  describe("with an owner token", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupMapApi(crates, new MemoryBroadcast);
    });

    it("should return both the public and owned maps" , {
      router
        .request
        .get("/maps")
        .header("Authorization", "Bearer " ~ bearerToken.name)
        .expectStatusCode(200)
        .end((Response response) => () {
          auto item1 = crates.map.getItem("000000000000000000000001").exec.front.clone;
          item1["canEdit"] = true;

          auto item2 = crates.map.getItem("000000000000000000000002").exec.front.clone;
          item2["canEdit"] = true;

          auto item3 = crates.map.getItem("000000000000000000000003").exec.front.clone;
          item3["canEdit"] = false;

          auto maps = cast(Json[]) response.bodyJson["maps"];
          maps.length.should.equal(3).because("there are 3 items in the db");

          maps[0].should.equal(item1);
          maps[1].should.equal(item2);
          maps[2].should.equal(item3);
        });
    });

    it("should get only the editable maps when the 'edit' query string is present", {
      router
        .request
        .get("/maps?edit=true")
        .header("Authorization", "Bearer " ~ bearerToken.name)
        .expectStatusCode(200)
        .end((Response response) => () {
          auto item1 = crates.map.getItem("000000000000000000000001").exec.front.clone;
          item1["canEdit"] = true;

          auto item2 = crates.map.getItem("000000000000000000000002").exec.front.clone;
          item2["canEdit"] = true;

          auto maps = cast(Json[]) response.bodyJson["maps"];
          maps.length.should.equal(2);

          maps[0].should.equal(item1);
          maps[1].should.equal(item2);
        });
    });
  });
});
