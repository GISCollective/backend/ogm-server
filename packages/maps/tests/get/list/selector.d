/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.maps.get.list.selector;

import tests.fixtures;

import std.algorithm;
import std.array;
import gis_collective.hmq.broadcast.memory;
import ogm.maps.api;

alias suite = Spec!({
  URLRouter router;
  describe("the list selectors", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupMapApi(crates, new MemoryBroadcast);
    });

    describe("with the ids query param", {
      it("returns only the items that have the requested ids", {
        router
        .request
        .get("/maps?ids=000000000000000000000001,000000000000000000000003,000000000000000000000002")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["maps"]
            .byValue
            .map!(a => a["_id"].to!string)
            .should
            .equal(["000000000000000000000001", "000000000000000000000003", "000000000000000000000002"]);
        });
      });

      it("returns only the items that the user can access", {
        router
        .request
        .get("/maps?ids=000000000000000000000001,000000000000000000000003,000000000000000000000002,000000000000000000000004")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["maps"]
            .byValue
            .map!(a => a["_id"].to!string)
            .should
            .equal(["000000000000000000000001", "000000000000000000000003"]);
        });
      });
    });

    describe("without a token", {
      describe("given a published selector", {
        it("should return an error when the query unpublished query parameter is set", {
          router
            .request
            .get("/maps?published=false")
            .expectStatusCode(403)
            .end((Response response) => () {
              response.bodyJson.should.equal("{\"errors\": [{
                \"description\": \"You can't use the `unpublished` query because you are not authenticated.\",
                \"title\": \"Forbidden\",
                \"status\": 403
              }]}".parseJsonString);
            });
        });
      });
    });

    describe("with a guest token", {

      describe("given a published selector", {
        it("should return only published items", {
          router
            .request
            .get("/maps?published=true")
            .header("Authorization", "Bearer " ~ bearerGuestToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              auto maps = cast(Json[]) response.bodyJson["maps"];

              maps.map!(a => a["_id"].to!string).should.containOnly(["000000000000000000000001", "000000000000000000000003"]);
            });
        });

        it("should return only unpublished items", {
          router
            .request
            .get("/maps?published=false")
            .header("Authorization", "Bearer " ~ bearerGuestToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              auto maps = cast(Json[]) response.bodyJson["maps"];

              maps.map!(a => a["_id"].to!string).should.containOnly(["000000000000000000000002"]);
            });
        });
      });
    });

    describe("with a member token", {
      describe("given a published selector", {
        it("should return only published items", {
          router
            .request
            .get("/maps?published=true")
            .header("Authorization", "Bearer " ~ bearerMemberToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              auto maps = cast(Json[]) response.bodyJson["maps"];

              maps.map!(a => a["_id"].to!string).should.containOnly(["000000000000000000000001", "000000000000000000000003"]);
            });
        });

        it("should return only unpublished items", {
          router
            .request
            .get("/maps?published=false")
            .header("Authorization", "Bearer " ~ bearerMemberToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              auto maps = cast(Json[]) response.bodyJson["maps"];

              maps.map!(a => a["_id"].to!string).should.containOnly(["000000000000000000000002"]);
            });
        });
      });
    });

    describe("with a leader token", {

      describe("given a published selector", {
        it("should return only published items", {
          router
            .request
            .get("/maps?published=true")
            .header("Authorization", "Bearer " ~ bearerLeaderToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              auto maps = cast(Json[]) response.bodyJson["maps"];

              maps.map!(a => a["_id"].to!string).should.containOnly(["000000000000000000000001", "000000000000000000000003"]);
            });
        });

        it("should return only unpublished items", {
          router
            .request
            .get("/maps?published=false")
            .header("Authorization", "Bearer " ~ bearerLeaderToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              auto maps = cast(Json[]) response.bodyJson["maps"];

              maps.map!(a => a["_id"].to!string).should.containOnly(["000000000000000000000002"]);
            });
        });
      });
    });

    describe("with an owner token", {

      describe("given a published selector", {
        it("should return only published items", {
          router
            .request
            .get("/maps?published=true")
            .header("Authorization", "Bearer " ~ bearerToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              auto maps = cast(Json[]) response.bodyJson["maps"];

              maps.map!(a => a["_id"].to!string).should.containOnly(["000000000000000000000001", "000000000000000000000003"]);
            });
        });

        it("should return only unpublished items", {
          router
            .request
            .get("/maps?published=false")
            .header("Authorization", "Bearer " ~ bearerToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              auto maps = cast(Json[]) response.bodyJson["maps"];

              maps.map!(a => a["_id"].to!string).should.containOnly(["000000000000000000000002"]);
            });
        });
      });
    });

    describe("with an admin token", {
      describe("given a published selector", {
        it("should return only published items", {
          router
            .request
            .get("/maps?published=true")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              auto maps = cast(Json[]) response.bodyJson["maps"];

              maps.map!(a => a["_id"].to!string).should.containOnly(["000000000000000000000001", "000000000000000000000003"]);
            });
        });

        it("should return only unpublished items", {
          router
            .request
            .get("/maps?published=false")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              auto maps = cast(Json[]) response.bodyJson["maps"];

              maps.map!(a => a["_id"].to!string).should.containOnly(["000000000000000000000002", "000000000000000000000004"]);
            });
        });
      });
    });

    describe("with an user without map", {
      describe("given a published selector", {
        it("should return only published items", {
          router
            .request
            .get("/maps?published=true")
            .header("Authorization", "Bearer " ~ otherToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              auto maps = cast(Json[]) response.bodyJson["maps"];

              maps.map!(a => a["_id"].to!string).should.containOnly(["000000000000000000000001", "000000000000000000000003"]);
            });
        });

        it("should return an empty list", {
          router
            .request
            .get("/maps?published=false")
            .header("Authorization", "Bearer " ~ otherToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              auto maps = cast(Json[]) response.bodyJson["maps"];

              maps.map!(a => a["_id"].to!string).should.containOnly([]);
            });
        });
      });
    });

    describe("the isIndex selector", {
      beforeEach({
        createMap(Map(
          ObjectId.fromString("5"), "indexed", Json(""),
          Visibility(team1, true), Polygon([[[-7, 3], [-7, 5], [-8, 5], [-8, 3], [-7,3]]]),
          cover, IconSets(true, [iconSet1]), BaseMaps(), "", SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-02-01T00:00:00Z"),
          ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z")),
          Picture.init, Mask(), true));
      });

      describeCredentialsRule("get the indexable maps using `isIndex` query", "yes", "maps", ["administrator", "owner", "leader", "member", "guest"], (string type) {
        router
          .request
          .get("/maps?isIndex=true")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .end((Response response) => () {
            auto maps = cast(Json[]) response.bodyJson["maps"];
            maps.map!(a => a["_id"].to!string).should.containOnly(["000000000000000000000005"]);
          });
      });

      describeCredentialsRule("get the indexable maps using `isIndex` query", "yes", "maps", "no rights", {
        router
          .request
          .get("/maps?isIndex=true")
          .expectStatusCode(200)
          .end((Response response) => () {
            auto maps = cast(Json[]) response.bodyJson["maps"];
            maps.map!(a => a["_id"].to!string).should.containOnly(["000000000000000000000005"]);
          });
      });
    });
  });
});
