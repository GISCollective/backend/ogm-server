/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.maps.get.list.notoken;

import tests.fixtures;

import std.algorithm;
import std.array;
import ogm.defaults.teams;
import ogm.maps.api;
import ogm.defaults.baseMaps;

import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({
  URLRouter router;

  describe("When there is no token provided", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupMapApi(crates, new MemoryBroadcast);
    });

    it("should return only the public maps", {
      router
        .request
        .get("/maps")
        .expectStatusCode(200)
        .end((Response response) => () {
          auto item1 = crates.map.getItem("000000000000000000000001").exec.front.to!string.parseJsonString;
          auto item2 = crates.map.getItem("000000000000000000000003").exec.front.to!string.parseJsonString;

          auto maps = cast(Json[]) response.bodyJson["maps"];
          maps.length.should.equal(2);

          maps.should.contain(item1);
          maps.should.contain(item2);
        });
    });

    it("should remove the non string iconset values", {
      auto originalItem1 = crates.map.getItem("000000000000000000000001").exec.front;
      auto map = crates.map.getItem("000000000000000000000001").exec.front;
      map["iconSets"]["list"].appendArrayElement(Json(null));
      map["iconSets"]["list"].appendArrayElement(Json.emptyObject);

      crates.map.updateItem(map);

      router
        .request
        .get("/maps")
        .expectStatusCode(200)
        .end((Response response) => () {
          auto item2 = crates.map.getItem("000000000000000000000003").exec.front.to!string.parseJsonString;

          auto maps = cast(Json[]) response.bodyJson["maps"];
          maps.length.should.equal(2);

          maps[0]["iconSets"].should.equal(originalItem1["iconSets"]);
        });
    });

    it("should return an error on getting maps when a token is not provided and the `edit` query string is present", {
      router
        .request
        .get("/maps?edit=true")
        .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "errors": [
                {
                  "description": "You can't edit items.",
                  "title": "Forbidden",
                  "status": 403
                }
              ]
            }`.parseJsonString);
          });
    });

    it("should return an error on getting a private map without a token", {
      router
        .request
        .get("/maps/000000000000000000000002")
        .expectStatusCode(404)
          .end((Response response) => () {
            response.bodyJson.should.equal("{
              \"errors\": [
                {
                  \"description\": \"There is no item with id `000000000000000000000002`\",
                  \"title\": \"Crate not found\",
                  \"status\": 404
                }
              ]
            }".parseJsonString);
          });
    });

    describe("when there is an index map", {
      beforeEach({
        createMap(Map(
          ObjectId.fromString("5"), "indexed", Json(""),
          Visibility(team1, true), Polygon([[[-7, 3], [-7, 5], [-8, 5], [-8, 3], [-7,3]]]),
          cover, IconSets(true, [iconSet1]), BaseMaps(), "", SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-02-01T00:00:00Z"),
          ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z")),
          Picture.init, Mask(), true));
      });

      it("should not be returned to the list of maps", {
        router
          .request
          .get("/maps")
          .expectStatusCode(200)
          .end((Response response) => () {
            auto maps = cast(Json[]) response.bodyJson["maps"];
            maps.map!(a => a["_id"].to!string).should.not.contain("000000000000000000000005");
          });
      });
    });

    describe("when there are default public basemaps", {
      beforeEach({
        router = new URLRouter;

        router.crateSetup.setupMapApi(crates, new MemoryBroadcast);
        setupDefaultTeams(crates);
        setupDefaultBaseMaps(crates);
      });

      it("should return the default base maps when useCustomList is false", {
        router
        .request
        .get("/maps/000000000000000000000001")
        .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "map": {
                "info": {
                  "changeIndex": 0,
                  "createdOn": "2015-01-01T00:00:00Z",
                  "lastChangeOn": "2015-01-01T00:00:00Z",
                  "originalAuthor": "",
                  "author": ""
                },
                "baseMaps": {
                  "useCustomList": false,
                  "list": [ "000000000000000000000001", "000000000000000000000002", "000000000000000000000003" ]
                },
                "visibility": {
                  "isDefault": false,
                  "isPublic": true,
                  "team": "000000000000000000000001"
                },
                "area": {
                  "type": "Polygon",
                  "coordinates": [[[ -70, 30 ], [ -70, 50 ], [ -80, 50 ], [ -80, 30 ], [ -70, 30 ]]]
                },
                "iconSets":{
                  "useCustomList": true,
                  "list": ["000000000000000000000001"]
                },
                "_id": "000000000000000000000001",
                "hideOnMainMap": false,
                "name": "map1",
                "endDate": "2015-02-01T00:00:00Z",
                "startDate": "2015-01-01T00:00:00Z",
                "cover": "000000000000000000000002",
                "tagLine": "",
                "sprites": {},
                "description": { "blocks": [] },
                "showPublicDownloadLinks": false,
                "addFeaturesAsPending": false,
                "isIndex": false,
                "mask": {
                  "map": "",
                  "isEnabled": false
                },
                "license": {
                  "url": "",
                  "name": ""
                },
                "cluster": {
                  "map": "",
                  "mode": 1
                }
            }}`.parseJsonString);
          });
      });

      it("should not return the default base maps when useCustomList is true", {
        auto map = crates.map.getItem("000000000000000000000001").exec.front;
        map["baseMaps"]["useCustomList"] = true;
        crates.map.updateItem(map);

        router
          .request
          .get("/maps/000000000000000000000001")
          .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal(`{
                "map": {
                  "info": {
                    "changeIndex": 0,
                    "createdOn": "2015-01-01T00:00:00Z",
                    "lastChangeOn": "2015-01-01T00:00:00Z",
                    "originalAuthor": "",
                    "author": ""
                  },
                  "baseMaps": {
                    "useCustomList": true,
                    "list": [ ]
                  },
                  "visibility": {
                    "isDefault": false,
                    "isPublic": true,
                    "team": "000000000000000000000001"
                  },
                  "area": {
                    "type": "Polygon",
                    "coordinates": [[[ -70, 30 ], [ -70, 50 ], [ -80, 50 ], [ -80, 30 ], [ -70, 30 ]]]
                  },
                  "iconSets":{
                    "useCustomList": true,
                    "list": ["000000000000000000000001"]
                  },
                  "_id": "000000000000000000000001",
                  "name": "map1",
                  "sprites": {},
                  "hideOnMainMap": false,
                  "endDate": "2015-02-01T00:00:00Z",
                  "startDate": "2015-01-01T00:00:00Z",
                  "cover": "000000000000000000000002",
                  "tagLine": "",
                  "description": { "blocks": [] },
                  "showPublicDownloadLinks": false,
                  "isIndex": false,
                  "addFeaturesAsPending": false,
                  "mask": {
                    "map": "",
                    "isEnabled": false
                  },
                  "license": {
                    "url": "",
                    "name": ""
                  },
                  "cluster": {
                    "map": "",
                    "mode": 1
                  }
              }}`.parseJsonString);
            });
      });
    });
  });
});
