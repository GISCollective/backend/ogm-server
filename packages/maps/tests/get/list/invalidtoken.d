/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.maps.get.list.invalidtoken;

import tests.fixtures;

import std.algorithm;
import std.array;
import ogm.maps.api;

import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({
  URLRouter router;

  describe("with an invalid token", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupMapApi(crates, new MemoryBroadcast);
    });

    it("should not return any maps", {
      router
        .request
        .get("/maps")
        .header("Authorization", "Bearer random")
        .expectStatusCode(400)
        .end((Response response) => () {
          response.bodyJson.should.equal("{\"error\": \"Invalid token.\"}".parseJsonString);
        });
    });
  });
});
