/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.maps.get.list.member;

import tests.fixtures;

import std.algorithm;
import std.array;
import ogm.maps.api;

import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({
  URLRouter router;

  describe("with a member token", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupMapApi(crates, new MemoryBroadcast);
    });

    it("should return both the public and joined maps" , {
      router
        .request
        .get("/maps")
        .header("Authorization", "Bearer " ~ bearerMemberToken.name)
        .expectStatusCode(200)
        .end((Response response) => () {
          auto item1 = crates.map.getItem("000000000000000000000001").exec.front.to!string.parseJsonString;
          item1["canEdit"] = false;

          auto item2 = crates.map.getItem("000000000000000000000002").exec.front.to!string.parseJsonString;
          item2["canEdit"] = false;

          auto item3 = crates.map.getItem("000000000000000000000003").exec.front.to!string.parseJsonString;
          item3["canEdit"] = false;

          auto maps = cast(Json[]) response.bodyJson["maps"];
          maps.length.should.equal(3).because("there are 3 items in the db");

          maps.should.contain(item1);
          maps.should.contain(item2);
          maps.should.contain(item3);
        });
    });

    it("should get no maps when the 'edit' query string is present", {
      router
        .request
        .get("/maps?edit=true")
        .header("Authorization", "Bearer " ~ bearerMemberToken.name)
        .expectStatusCode(200)
        .end((Response response) => () {
          auto maps = cast(Json[]) response.bodyJson["maps"];
          maps.length.should.equal(0);
        });
    });
  });
});
