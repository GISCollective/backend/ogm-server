/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.maps.get.geopackage;

import tests.fixtures;

import std.algorithm;
import std.array;

import ogm.maps.operations.GeopackageRemoteTask;
import gis_collective.hmq.broadcast.memory;
import ogm.maps.api;

alias suite = Spec!({
  MemoryBroadcast broadcast;
  URLRouter router;

  Json receivedMessage;
  int called;

  beforeEach({
    setupTestData();
    called = 0;

    broadcast = new MemoryBroadcast();
    router = new URLRouter;
    router.crateSetup.setupMapApi(crates, broadcast);
    GeopackageRemoteTask.interval = 50;
  });

  describe("when the geopackage task creates a file", {
    beforeEach({
      GeopackageRemoteTask.timeout = 2;
      called = 0;

      void testHandler(const Json value) @trusted {
        called++;
        receivedMessage = value.clone;

        Json mockFile = Json.emptyObject;
        mockFile["map"] = "000000000000000000000001";
        mockFile["file"] = "99";
        mockFile["options"] = Json.emptyObject;
        mockFile["options"]["uuid"] = value["uuid"];

        crates.mapFile.addItem(mockFile);
      }

      broadcast.register("mapFile.generateGeoPackage", &testHandler);
    });

    it("should return the file data", {
      router
        .request
        .get("/maps/000000000000000000000001/geopackage")
        .header("Authorization", "Bearer " ~ bearerToken.name)
        .expectStatusCode(200)
        .end((Response response) => () {
          called.should.equal(1);
          receivedMessage["id"].to!string.should.equal(`000000000000000000000001`);
          response.bodyString.should.equal("");
        });
    });
  });

  describe("when the geopackage task does not create the file in time", {
    beforeEach({
      GeopackageRemoteTask.timeout = 2;

      void testHandler(const Json value) @trusted {
        called++;
      }

      broadcast.register("mapFile.generateGeoPackage", &testHandler);
    });

    it("should return a timeout response", {
      router
        .request
        .get("/maps/000000000000000000000001/geopackage")
        .header("Authorization", "Bearer " ~ bearerToken.name)
        .expectStatusCode(408)
        .end((Response response) => () {
          called.should.equal(1);
          response.bodyString.should.equal(`{"errors":[{"description":"The file needs more time to be generated. Please try again later.","status":408,"title":"Request timed out"}]}`);
        });
    });
  });

  describeCredentialsRule("download as geopackage for own maps", "yes", "maps", ["administrator", "owner", "leader", "member", "guest"], (string type) {
    void testHandler(const Json value) @trusted {
      called++;
      receivedMessage = value.clone;

      Json mockFile = Json.emptyObject;
      mockFile["map"] = "000000000000000000000001";
      mockFile["file"] = "99";
      mockFile["options"] = Json.emptyObject;
      mockFile["options"]["uuid"] = value["uuid"];

      crates.mapFile.addItem(mockFile);
    }

    broadcast.register("mapFile.generateGeoPackage", &testHandler);

    router
      .request
      .get("/maps/000000000000000000000001/geopackage")
      .header("Authorization", "Bearer " ~ userTokenList[type])
      .expectStatusCode(200)
      .end((Response response) => () {
        called.should.equal(1);
        response.bodyString.should.equal(``);
      });
  });

  describeCredentialsRule("download as geopackage for own maps", "-", "maps", "no rights", {
    void testHandler(const Json value) @trusted {
      called++;
    }

    broadcast.register("mapFile.generateGeoPackage", &testHandler);

    router
      .request
      .get("/maps/000000000000000000000001/geopackage")
      .expectStatusCode(403)
      .end((Response response) => () {
        called.should.equal(0);

        response.bodyJson.should.equal(`{
          "errors": [{
            "description": "You must be authenticated to get the geopackage.",
            "status": 403,
            "title": "Forbidden"
          }]
        }`.parseJsonString);
      });
  });


  describeCredentialsRule("download as geopackage for other maps", "yes", "maps", "administrator", (string type) {
    void testHandler(const Json value) @trusted {
      called++;
      receivedMessage = value.clone;

      Json mockFile = Json.emptyObject;
      mockFile["map"] = "000000000000000000000003";
      mockFile["file"] = "99";
      mockFile["options"] = Json.emptyObject;
      mockFile["options"]["uuid"] = value["uuid"];

      crates.mapFile.addItem(mockFile);
    }

    broadcast.register("mapFile.generateGeoPackage", &testHandler);

    router
      .request
      .get("/maps/000000000000000000000003/geopackage")
      .header("Authorization", "Bearer " ~ userTokenList[type])
      .expectStatusCode(200)
      .end((Response response) => () {
        called.should.equal(1);
        response.bodyString.should.equal(``);
      });
  });

  describeCredentialsRule("download as geopackage for other maps", "no", "maps", ["owner", "leader", "member", "guest"], (string type) {
    void testHandler(const Json value) @trusted {
      called++;
      receivedMessage = value.clone;

      Json mockFile = Json.emptyObject;
      mockFile["map"] = "000000000000000000000003";
      mockFile["file"] = "99";
      mockFile["options"] = Json.emptyObject;
      mockFile["options"]["uuid"] = value["uuid"];

      crates.mapFile.addItem(mockFile);
    }

    broadcast.register("mapFile.generateGeoPackage", &testHandler);

    router
      .request
      .get("/maps/000000000000000000000003/geopackage")
      .header("Authorization", "Bearer " ~ userTokenList[type])
      .expectStatusCode(401)
      .end((Response response) => () {
        called.should.equal(0);
        response.bodyJson.should.equal(`{
          "errors": [{
              "description": "You get the geopackage for this map.",
              "status": 401,
              "title": "Unauthorized"
          }]
        }`.parseJsonString);
      });
  });

  describeCredentialsRule("download as geopackage for other maps", "no", "maps", "no rights", {
    void testHandler(const Json value) @trusted {
      called++;
    }

    broadcast.register("mapFile.generateGeoPackage", &testHandler);

    router
      .request
      .get("/maps/000000000000000000000003/geopackage")
      .expectStatusCode(403)
      .end((Response response) => () {
        called.should.equal(0);

        auto expected = `{
          "errors": [{
            "description": "You must be authenticated to get the geopackage.",
            "status": 403,
            "title": "Forbidden"
          }]
        }`.parseJsonString;

        response.bodyJson.should.equal(expected);
      });
  });
});
