/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.maps.get.metadata;

import tests.fixtures;

import std.algorithm;
import std.array;
import ogm.maps.api;

import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({
  URLRouter router;

  describe("without a token", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupMapApi(crates, new MemoryBroadcast);

      crates.meta.addItem(`{
        "type": "Map.info",
        "model": "Map",
        "itemId": "000000000000000000000003",
        "changeIndex": 0,
        "data": {
          "publicSites": 2,
          "totalSites": 2,
          "totalFeatures": 2,
          "totalContributors": 6
        }}`.parseJsonString);
    });

    it("should return the public map metadata", {
      router
        .request
        .get("/maps/000000000000000000000003/meta")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "publicSites": 2,
            "totalContributors": 6
          }`.parseJsonString);
        });
    });
  });


  describe("with an owner token", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupMapApi(crates, new MemoryBroadcast);

      crates.meta.addItem(`{
        "type": "Map.info",
        "model": "Map",
        "itemId": "000000000000000000000001",
        "changeIndex": 0,
        "data": {
          "publicSites": 2,
          "totalSites": 2,
          "totalContributors": 6
        }}`.parseJsonString);
    });

    it("should return the all map metadata", {
      router
        .request
        .get("/maps/000000000000000000000001/meta")
        .header("Authorization", "Bearer " ~ bearerToken.name)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "publicSites": 2,
            "totalSites": 2,
            "totalContributors": 6
          }`.parseJsonString);
        });
    });
  });

  describe("with an admin token", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupMapApi(crates, new MemoryBroadcast);

      crates.meta.addItem(`{
        "type": "Map.info",
        "model": "Map",
        "itemId": "000000000000000000000003",
        "changeIndex": 0,
        "data": {
          "publicSites": 2,
          "totalSites": 2,
          "totalContributors": 6
        }}`.parseJsonString);
    });

    it("should return the all map metadata", {
      router
        .request
        .get("/maps/000000000000000000000003/meta")
        .header("Authorization", "Bearer " ~ administratorToken.name)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "publicSites": 2,
            "totalSites": 2,
            "totalContributors": 6
          }`.parseJsonString);
        });
    });
  });
});
