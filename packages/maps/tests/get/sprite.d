/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.maps.get.sprite;

import tests.fixtures;

import std.algorithm;
import std.array;
import ogm.maps.api;

import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({
  URLRouter router;
  MemoryBroadcast broadcast;


  beforeEach({
    setupTestData();
    broadcast = new MemoryBroadcast();
    router = new URLRouter;
    router.crateSetup.setupMapApi(crates, broadcast);
  });

  it("returns 404 when the sprite picture does not exist", {
    router
      .request
      .get("/maps/000000000000000000000001/sprite@2x.png")
      .header("Authorization", "Bearer " ~ bearerToken.name)
      .expectStatusCode(404)
      .end;
  });

  it("returns 404 when the sprite index does not exist", {
    router
      .request
      .get("/maps/000000000000000000000001/sprite@2x.json")
      .header("Authorization", "Bearer " ~ bearerToken.name)
      .expectStatusCode(404)
      .end;
  });

  describe("when the all icons sprite exists", {
    beforeEach({
      auto picture = Picture();
      picture.picture = new PictureFile("");
      picture.meta.link = Json.emptyObject;
      picture.meta.link["model"] = "Map";
      picture.meta.link["modelId"] = "_";
      picture.meta.link["type"] = "sprite";
      picture.meta.link["size"] = 1;
      picture.meta.data = `{
        "size": { "height": 64, "width": 4096 },
        "index": {
          "_": { "height": 22, "width": 22, "pixelRatio": 1, "y": 21, "x": 21 }
        }
      }`.parseJsonString;

      crates.picture.addItem(picture.serializeToJson);

      picture.meta.link["size"] = 2;
      crates.picture.addItem(picture.serializeToJson);
    });

    it("can get all icons sprite picture", {
      router
        .request
        .get("/maps/_/sprite.png")
        .header("Authorization", "Bearer " ~ bearerToken.name)
        .expectStatusCode(200)
        .end;
    });

    it("can get all icons sprite json", {
      router
        .request
        .get("/maps/_/sprite.json")
        .header("Authorization", "Bearer " ~ bearerToken.name)
        .expectStatusCode(200)
        .end;
    });
  });

  describe("when the map has a sprite", {
    beforeEach({
      auto picture = Picture();
      picture.picture = new PictureFile("");
      picture.meta.link = Json.emptyObject;
      picture.meta.link["model"] = "Map";
      picture.meta.link["modelId"] = "000000000000000000000001";
      picture.meta.link["type"] = "sprite";
      picture.meta.link["size"] = 1;
      picture.meta.data = `{
        "size": { "height": 64, "width": 4096 },
        "index": {
          "000000000000000000000001": { "height": 22, "width": 22, "pixelRatio": 1, "y": 21, "x": 21 }
        }
      }`.parseJsonString;

      crates.picture.addItem(picture.serializeToJson);

      picture.meta.link["size"] = 2;
      crates.picture.addItem(picture.serializeToJson);
    });

    it("can get the sprite picture", {
      router
        .request
        .get("/maps/000000000000000000000001/sprite.png")
        .header("Authorization", "Bearer " ~ bearerToken.name)
        .expectStatusCode(200)
        .end;
    });

    it("can get the sprite index", {
      router
        .request
        .get("/maps/000000000000000000000001/sprite.json")
        .header("Authorization", "Bearer " ~ bearerToken.name)
        .expectStatusCode(200)
        .end;
    });

    it("can get the sprite 2x picture", {
      router
        .request
        .get("/maps/000000000000000000000001/sprite@2x.png")
        .header("Authorization", "Bearer " ~ bearerToken.name)
        .expectStatusCode(200)
        .end;
    });

    it("can get the sprite 2x json", {
      router
        .request
        .get("/maps/000000000000000000000001/sprite@2x.json")
        .header("Authorization", "Bearer " ~ bearerToken.name)
        .expectStatusCode(200)
        .end;
    });
  });

  describe("when a public map has the default sprite", {
    beforeEach({
      auto map = crates.map.getItem("000000000000000000000001").and.exec.front;
      map["iconSets"]["useCustomList"] = false;
      crates.map.updateItem(map);

      auto picture = Picture();
      picture.picture = new PictureFile("");
      picture.meta.link = Json.emptyObject;
      picture.meta.link["model"] = "Map";
      picture.meta.link["modelId"] = "default";
      picture.meta.link["type"] = "sprite";
      picture.meta.link["size"] = 1;
      picture.meta.data = `{
        "size": { "height": 64, "width": 4096 },
        "index": {
          "default": { "height": 22, "width": 22, "pixelRatio": 1, "y": 21, "x": 21 }
        }
      }`.parseJsonString;

      crates.picture.addItem(picture.serializeToJson);

      picture.meta.link["size"] = 2;
      crates.picture.addItem(picture.serializeToJson);
    });

    it("can get the sprite picture", {
      router
        .request
        .get("/maps/000000000000000000000001/sprite.png")
        .header("Authorization", "Bearer " ~ bearerToken.name)
        .expectStatusCode(200)
        .end;
    });

    it("can get the sprite index", {
      router
        .request
        .get("/maps/000000000000000000000001/sprite.json")
        .header("Authorization", "Bearer " ~ bearerToken.name)
        .expectStatusCode(200)
        .end;
    });

    it("can get the sprite 2x picture", {
      router
        .request
        .get("/maps/000000000000000000000001/sprite@2x.png")
        .header("Authorization", "Bearer " ~ bearerToken.name)
        .expectStatusCode(200)
        .end;
    });

    describeCredentialsRule("get the sprite when the map is public", "yes", "maps", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/maps/000000000000000000000001/sprite.png")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end();
    });

    describeCredentialsRule("get the sprite when the map is public", "yes", "maps", "no rights", {
      router
        .request
        .get("/maps/000000000000000000000001/sprite.png")
        .expectStatusCode(200)
        .end();
    });
  });

  describe("when a private map has the default sprite", {
    beforeEach({
      auto map = crates.map.getItem("000000000000000000000001").and.exec.front;
      map["visibility"]["isPublic"] = false;
      map["iconSets"]["useCustomList"] = false;
      crates.map.updateItem(map);

      auto picture = Picture();
      picture.picture = new PictureFile("");
      picture.meta.link = Json.emptyObject;
      picture.meta.link["model"] = "Map";
      picture.meta.link["modelId"] = "default";
      picture.meta.link["type"] = "sprite";
      picture.meta.link["size"] = 1;
      picture.meta.data = `{
        "size": { "height": 64, "width": 4096 },
        "index": {
          "default": { "height": 22, "width": 22, "pixelRatio": 1, "y": 21, "x": 21 }
        }
      }`.parseJsonString;

      crates.picture.addItem(picture.serializeToJson);

      picture.meta.link["size"] = 2;
      crates.picture.addItem(picture.serializeToJson);
    });

    describeCredentialsRule("get the sprite when the map is public", "yes", "maps", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/maps/000000000000000000000001/sprite.png")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end();
    });

    describeCredentialsRule("get the sprite when the map is public", "no", "maps", "no rights", {
      router
        .request
        .get("/maps/000000000000000000000001/sprite.png")
        .expectStatusCode(404)
        .end();
    });
  });

});
