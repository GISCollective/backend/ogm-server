/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.maps.delete_;

import tests.fixtures;

import std.algorithm;
import std.array;
import ogm.maps.api;

import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({
  URLRouter router;

  describe("DELETE", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupMapApi(crates, new MemoryBroadcast);
    });

    describeCredentialsRule("delete maps owned by your teams", "yes", "maps", ["owner", "administrator"], (string type) {
      auto initialSize = crates.map.get.size;

      router
        .request
        .delete_("/maps/000000000000000000000002")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end((Response response) => () {
          crates.map.get.size.should.equal(initialSize - 1);
        });
    });

    describeCredentialsRule("delete maps owned by your teams", "no", "maps", ["leader", "member", "guest"], (string type) {
      auto initialSize = crates.map.get.size;

      router
        .request
        .delete_("/maps/000000000000000000000002")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{"errors": [{
              "description": "You don't have enough rights to remove ` ~ "`000000000000000000000002`" ~ `.",
              "status": 403,
              "title": "Forbidden"
            }]}`).parseJsonString);
          crates.map.get.size.should.equal(initialSize);
        });
    });

    describeCredentialsRule("delete maps owned by your teams", "-", "maps", "no rights", {
      auto initialSize = crates.map.get.size;

      router
        .request
        .delete_("/maps/000000000000000000000002")
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
              "error": "Authorization required"
            }`.parseJsonString);
          crates.map.get.size.should.equal(initialSize);
        });
    });


    describeCredentialsRule("delete maps owned by other teams", "yes", "maps", "administrator", (string type) {
      auto initialSize = crates.map.get.size;

      router
        .request
        .delete_("/maps/000000000000000000000003")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end((Response response) => () {
          crates.map.get.size.should.equal(initialSize - 1);
        });
    });

    describeCredentialsRule("delete maps owned by other teams", "no", "maps", ["owner", "leader", "member", "guest"], (string type) {
      auto initialSize = crates.map.get.size;

      router
        .request
        .delete_("/maps/000000000000000000000003")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(404)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{"errors": [{
              "description": "Item ` ~ "`000000000000000000000003`" ~ ` not found.",
              "status": 404,
              "title": "Crate not found"
            }]}`).parseJsonString);
          crates.map.get.size.should.equal(initialSize);
        });
    });

    describeCredentialsRule("delete maps owned by other teams", "no", "maps", "no rights", {
      auto initialSize = crates.map.get.size;

      router
        .request
        .delete_("/maps/000000000000000000000003")
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
              "error": "Authorization required"
            }`.parseJsonString);
          crates.map.get.size.should.equal(initialSize);
        });
    });

    describe("with an administrator token", {
      beforeEach({
        Picture customCover;
        customCover.name = "custom cover";
        customCover.owner = "";
        customCover.picture = new PictureFile;
        auto result = crates.picture.addItem(customCover.serializeToJson);
        customCover._id = ObjectId.fromString(result["_id"].to!string);

        createMap(Map(ObjectId.fromString("3"), "map3", Json(""),
          Visibility(team3, true), Polygon([[[-7, 3], [-7, 5], [-8, 5], [-8, 3], [-7,3]]]),
          customCover, IconSets(), BaseMaps(), "", Clock.currTime, Clock.currTime, ModelInfo()));
      });

      it("should delete a map and it's cover", {
        router
          .request
          .delete_("/maps/000000000000000000000005")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(204)
          .end((Response response) => () {
            crates.picture.get.exec
              .filter!(a => a["name"] == "custom cover")
                .array.length.should.equal(0);
          });
      });

      it("should delete a map but not the default cover", {
        router
          .request
          .delete_("/maps/000000000000000000000003")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(204)
          .end((Response response) => () {
            crates.picture.get.exec
              .filter!(a => a["name"] == "cover")
                .array.length.should.equal(1);
          });
      });
    });
  });
});
