/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.maps.patch;

import tests.fixtures;

import std.algorithm;
import std.array;
import ogm.maps.api;

import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({
  URLRouter router;
  Json data;

  describe("PATCH", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupMapApi(crates, new MemoryBroadcast);

      data = `{ "map": {
        "visibility": {
          "team":"000000000000000000000001",
          "isPublic": true
        },
        "iconSets":{
          "useCustomList": true,
          "list": ["000000000000000000000001"]
        },
        "name":"map1",
        "description":"",
        "area":{"coordinates":[[[-8,4],[-8,6],[-9,6],[-9,4],[-8,4]]],"type":"Polygon"},
      }}`.parseJsonString;
    });

    describe("a request with an administrator token", {
      it("should upload a map belonging to a different team", {
        router
          .request
          .patch("/maps/000000000000000000000003")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            auto item = crates.map.getItem("000000000000000000000003").exec.front.to!string.parseJsonString;
            item["canEdit"] = true;

            response.bodyJson["map"].should.equal(item);
          });
      });
    });

    describe("a request with an owner token", {
      it("should not patch a map that the user does not have access", {
        router
          .request
          .patch("/maps/000000000000000000000003")
          .send(data)
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(404)
          .end((Response response) => () {
            response.bodyJson.should.equal("{
              \"errors\": [
                {
                  \"description\": \"Item `000000000000000000000003` not found.\",
                  \"title\": \"Crate not found\",
                  \"status\": 404
                }
              ]
            }".parseJsonString);
          });
      });

      it("should patch a map that the user have access", {
        router
          .request
          .patch("/maps/000000000000000000000002")
          .send(data)
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            auto item = crates.map.getItem("000000000000000000000002").exec.front.to!string.parseJsonString;
            item["canEdit"] = true;

            response.bodyJson["map"].should.equal(item);
          });
      });
    });

    describe("a request with a leader token", {
      it("should return an error for updating a public map belonging to a different team", {
        router
          .request
          .patch("/maps/000000000000000000000003")
          .header("Authorization", "Bearer " ~ bearerLeaderToken.name)
          .send(data)
          .expectStatusCode(404)
          .end((Response response) => () {
            response.bodyJson.should.equal("{
              \"errors\": [
                {
                  \"description\": \"Item `000000000000000000000003` not found.\",
                  \"title\": \"Crate not found\",
                  \"status\": 404
                }
              ]
            }".parseJsonString);
          });
      });

      it("should not be able to update a map that the user has access to", {
        router
          .request
          .patch("/maps/000000000000000000000002")
          .header("Authorization", "Bearer " ~ bearerLeaderToken.name)
          .send(data)
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"errors\": [{
              \"description\": \"You don't have enough rights to edit `000000000000000000000002`.\",
              \"title\": \"Forbidden\",
              \"status\": 403
            }]}".parseJsonString);
          });
      });
    });

    describe("a request with a member token", {
      it("should return an error for updating a public map belonging to a different team", {
        router
          .request
          .put("/maps/000000000000000000000003")
          .header("Authorization", "Bearer " ~ bearerMemberToken.name)
          .send(data)
          .expectStatusCode(404)
          .end((Response response) => () {
            response.bodyJson.should.equal("{ \"errors\": [
                {
                  \"description\": \"Item `000000000000000000000003` not found.\",
                  \"title\": \"Crate not found\",
                  \"status\": 404
                }]}".parseJsonString);
          });
      });

      it("should not be able to update a map that the user has access to", {
        router
          .request
          .patch("/maps/000000000000000000000002")
          .header("Authorization", "Bearer " ~ bearerMemberToken.name)
          .send(data)
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"errors\": [{
                  \"description\": \"You don't have enough rights to edit `000000000000000000000002`.\",
                  \"title\": \"Forbidden\",
                  \"status\": 403
                }]}".parseJsonString);
          });
      });
    });

    describe("a request with a guest token", {
      it("should return an error for updating a public map belonging to a different team", {
        router
          .request
          .patch("/maps/000000000000000000000003")
          .header("Authorization", "Bearer " ~ bearerGuestToken.name)
          .send(data)
          .expectStatusCode(404)
          .end((Response response) => () {
            response.bodyJson.should.equal("{ \"errors\": [
                {
                  \"description\": \"Item `000000000000000000000003` not found.\",
                  \"title\": \"Crate not found\",
                  \"status\": 404
                }]}".parseJsonString);
          });
      });

      it("should not be able to update a map that the user has access to", {
        router
          .request
          .patch("/maps/000000000000000000000002")
          .header("Authorization", "Bearer " ~ bearerGuestToken.name)
          .send(data)
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"errors\": [{
                  \"description\": \"You don't have enough rights to edit `000000000000000000000002`.\",
                  \"title\": \"Forbidden\",
                  \"status\": 403
                }]}".parseJsonString);
          });
      });
    });
  });
});
