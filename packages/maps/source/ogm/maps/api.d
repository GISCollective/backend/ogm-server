/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.maps.api;

import vibe.http.router;
import crate.http.router;
import crate.base;
import crate.error;

import ogm.auth;
import ogm.crates.all;

import ogm.middleware.adminrequest;
import ogm.middleware.userdata;

import ogm.filter.pagination;
import ogm.filter.searchterm;
import ogm.filter.visibility;
import ogm.filter.searchtag;

import ogm.maps.filter;
import ogm.middleware.modelinfo;
import ogm.maps.operations.meta;
import ogm.middleware.DefaultCover;
import gis_collective.hmq.broadcast.base;
import ogm.middleware.GlobalAccess;

import ogm.maps.operations.GeopackageRemoteTask;
import ogm.maps.operations.SpriteImageOperation;
import ogm.maps.operations.SpriteIndexOperation;
import ogm.maps.middlewares.MapMapper;
import ogm.middleware.IdsFilter;
import ogm.middleware.SortingMiddleware;
import ogm.maps.operations.MapAttributesOperation;
import ogm.middleware.map.mapListFieldValidationMiddleware;

///
void setupMapApi(T)(CrateRouter!T crateRouter, OgmCrates crates, IBroadcast broadcast) {
  PictureFileSettings.files = crates.pictureFiles;
  PictureFileSettings.chunks = crates.pictureChunks;

  auto auth = Authentication.instance(crates.user);
  auto adminRequest = new AdminRequest(crates.user);

  auto globalAccess = GlobalAccessMiddleware.instance(crates);
  auto userDataMiddleware = UserDataMiddleware.instance(crates);
  auto mapsFilter = new MapsFilter(crates);
  auto searchTermFilter = new SearchTermFilter;
  auto searchTagFilter = new SearchTagFilter;
  auto paginationFilter = new PaginationFilter;
  auto visibilityFilter = new VisibilityFilter!"maps"(crates, crates.map);
  auto defaultCover = new DefaultCoverMiddleware(crates, "map");
  auto geopackageRemoteTask = new GeopackageRemoteTask(crates, broadcast);
  auto spriteImageOperation = new SpriteImageOperation!1(crates);
  auto spriteImageOperationDouble = new SpriteImageOperation!2(crates);
  auto spriteIndexOperation = new SpriteIndexOperation!1(crates);
  auto spriteIndexOperationDouble = new SpriteIndexOperation!2(crates);
  auto mapMapper = new MapMapper();
  auto idsFilter = new IdsFilter(crates);
  auto iconSetsValidationMiddleware = new MapListFieldValidationMiddleware!("iconSets", "icon set")(crates, crates.iconSet);
  auto baseMapsValidationMiddleware = new MapListFieldValidationMiddleware!("baseMaps", "base map")(crates, crates.baseMap);
  auto sorting = new SortingMiddleware(crates, "info.lastChangeOn", -1);
  auto attributesOperation = new MapAttributesOperation(crates);

  auto modelInfo = new ModelInfoMiddleware("map", crates.map);

  crateGetters["Team"] = &crates.team.getItem;
  crateGetters["Picture"] = &crates.picture.getItem;
  crateGetters["IconSet"] = &crates.iconSet.getItem;

  metaCrate = crates.meta;
  batchJobCrate = crates.batchJob;

  auto privateAuth = auth.privateDataMiddleware;
  auto mandatoryAuth = &privateAuth.mandatory;

  auto preparedRoute = crateRouter.prepare(crates.map);

  preparedRoute
    .itemOperation!("meta")(&meta)
    .withCustomOperation(geopackageRemoteTask)
    .withCustomOperation(spriteImageOperation)
    .withCustomOperation(spriteImageOperationDouble)
    .withCustomOperation(spriteIndexOperation)
    .withCustomOperation(spriteIndexOperationDouble)
    .withCustomOperation(attributesOperation)
    .and(auth.publicDataMiddleware)
    .and(adminRequest)
    .and(userDataMiddleware)
    .and(globalAccess)
    .and(modelInfo)
    .and(iconSetsValidationMiddleware)
    .and(baseMapsValidationMiddleware)
    .and(visibilityFilter)
    .and(defaultCover)
    .and(mapsFilter)
    .and(sorting)
    .and(searchTermFilter)
    .and(searchTagFilter)
    .and(idsFilter)
    .and(paginationFilter)
    .and(mapMapper);
}
