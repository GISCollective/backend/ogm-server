/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.maps.operations.SpriteOperation;

import vibe.http.router;
import vibe.data.json;
import vibe.core.core;

import crate.http.operations.base;
import crate.base;
import crate.error;

import ogm.http.request;
import ogm.crates.all;
import ogm.models.mapFile;
import ogm.http.request;
import ogm.rights;

import std.exception;
import std.functional;
import std.datetime;
import std.algorithm;
import std.uuid;
import std.conv;

import gis_collective.hmq.broadcast.base;
import ogm.maps.operations.SpriteOperation;

class SpriteOperation(int size, string ext) : CrateOperation!DefaultStorage {
  protected {
    OgmCrates crates;
  }

  this(OgmCrates crates) {
    this.crates = crates;

    CrateRule rule;

    static if(size == 1) {
      string postfix = "";
    } else {
      string postfix = "@" ~ size.to!string ~ "x";
    }

    rule.request.path = "/maps/:id/sprite" ~ postfix ~ "." ~ ext;
    rule.request.method = HTTPMethod.GET;

    super(crates.map, rule);
  }

  ///
  override void handle(DefaultStorage storage) {
    auto itemId = getPathVar!"id"(storage.request.requestPath, rule.request.path);

    auto res = storage.response;
    auto req = storage.request;

    if(itemId != "_") {
      storage.item = this.prepareItemOperation!"getItem"(storage);

      if(storage.item["iconSets"]["useCustomList"] == false) {
        itemId = "default";
      }

      validateMapAccess(crates, storage.item, req);
    }

    if(res.headerWritten) {
      return;
    }

    auto pictures = crates.picture.get
      .where("meta.link.modelId").equal(itemId).and
      .where("meta.link.model").equal("Map").and
      .where("meta.link.type").equal("sprite").and
      .where("meta.link.size").equal(size).and
      .limit(1)
      .exec;

    enforce!CrateNotFoundException(!pictures.empty, "There is no sprite.");

    auto picture = LazyPicture(pictures.front).toType;

    this.handle(picture, storage);
  }

  abstract void handle(Picture picture, DefaultStorage storage);
}