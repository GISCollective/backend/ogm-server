/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.maps.operations.GeopackageRemoteTask;

import vibe.http.router;
import vibe.data.json;
import vibe.core.core;

import crate.http.operations.base;
import crate.base;
import crate.error;

import ogm.http.request;
import ogm.crates.all;
import ogm.models.mapFile;

import std.exception;
import std.functional;
import std.datetime;
import std.algorithm;
import std.uuid;

import gis_collective.hmq.broadcast.base;

class GeopackageRemoteTask : CrateOperation!DefaultStorage {
  static size_t timeout = 60;
  static size_t interval = 1000;

  private {
    OgmCrates crates;
    IBroadcast broadcast;
  }

  this(OgmCrates crates, IBroadcast broadcast) {
    this.crates = crates;
    this.broadcast = broadcast;

    CrateRule rule;

    rule.request.path = "/maps/:id/geopackage";
    rule.request.method = HTTPMethod.GET;

    super(crates.map, rule);
  }


  ///
  override void handle(DefaultStorage storage) {
    storage.item = this.prepareItemOperation!"getItem"(storage);

    auto res = storage.response;

    if(res.headerWritten) {
      return;
    }

    auto req = storage.request;

    scope request = RequestUserData(req);
    enforce!ForbiddenException(request.isAuthenticated, "You must be authenticated to get the geopackage.");

    auto session = request.session(crates);
    enforce!UnauthorizedException(request.isAdmin || session.all.maps.canFind(request.itemId), "You get the geopackage for this map.");

    string uuid = randomUUID.toString;

    auto message = Json.emptyObject;
    message["id"] = request.itemId;
    message["uuid"] = uuid;
    message["timestamp"] = Clock.currTime.toISOExtString;

    this.broadcast.push("mapFile.generateGeoPackage", message);

    MapFileResource file;
    size_t index;
    while(index <= timeout) {
      index++;
      sleep(interval.msecs);
      auto range = crates.mapFile.get.where("options.uuid").like(uuid).and.exec;

      if(range.empty && index >= timeout) {
        throw new CrateRequestTimeoutException("The file needs more time to be generated. Please try again later.");
      }

      if(!range.empty) {
        auto mapFile = LazyMapFile(range.front, (&itemResolver).toDelegate).toType;
        file = mapFile.file;
        break;
      }
    }

    res.statusCode = 200;

    res.headers["content-disposition"] = `attachment; filename="map.gpkg"`;
    res.headers["Content-Type"] = "application/geopackage+vnd.sqlite3";
    file.write(res.bodyWriter);
  }
}