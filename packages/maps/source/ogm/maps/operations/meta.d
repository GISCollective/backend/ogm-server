/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.maps.operations.meta;

import ogm.http.request;
import crate.http.request;
import vibe.data.json;
import vibe.http.server;
import ogm.models.meta;

void meta(HTTPServerRequest req, HTTPServerResponse res, Json selectedMap) {
  auto range = metaCrate.get
    .where("itemId").equal(selectedMap["_id"].to!string).and
    .where("type").equal("Map.info").and
    .exec;

  if(range.empty) {
    res.writeJsonBody(Json.emptyObject, 200);
    return;
  }

  auto item = range.front;

  if("data" !in item) {
    res.writeJsonBody(Json.emptyObject, 200);
    return;
  }

  auto result = Json.emptyObject;
  result["team"] = selectedMap["visibility"]["team"];

  auto request = RequestUserData(req);

  if(request.isAuthenticated) {
    result.editableByTeamOwners(request.session);

    if("canEdit" !in result || result["canEdit"] != true) {
      item["data"].remove("totalSites");
      item["data"].remove("totalFeatures");
    }
  } else {
    item["data"].remove("totalSites");
    item["data"].remove("totalFeatures");
  }

  res.writeJsonBody(item["data"], 200);
}