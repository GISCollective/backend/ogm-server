/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.maps.operations.SpriteIndexOperation;

import vibe.http.router;
import vibe.data.json;
import vibe.core.core;

import crate.http.operations.base;
import crate.base;
import crate.error;

import ogm.http.request;
import ogm.crates.all;
import ogm.models.mapFile;

import std.exception;
import std.functional;
import std.datetime;
import std.algorithm;
import std.conv;

import gis_collective.hmq.broadcast.base;
import ogm.maps.operations.SpriteOperation;

class SpriteIndexOperation(int size) : SpriteOperation!(size, "json") {

  this(OgmCrates crates) {
    super(crates);
  }

  ///
  override void handle(Picture picture, DefaultStorage storage) {
    storage.response.writeJsonBody(picture.meta.data["index"], 200);
  }
}