/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.maps.operations.SpriteImageOperation;

import vibe.http.router;
import vibe.data.json;
import vibe.core.core;

import crate.http.operations.base;
import crate.base;
import crate.error;

import ogm.http.request;
import ogm.crates.all;
import ogm.models.mapFile;

import std.exception;
import std.functional;
import std.datetime;
import std.algorithm;
import std.uuid;
import std.conv;

import gis_collective.hmq.broadcast.base;
import ogm.maps.operations.SpriteOperation;

class SpriteImageOperation(int size) : SpriteOperation!(size, "png") {

  this(OgmCrates crates) {
    super(crates);
  }

  ///
  override void handle(Picture picture, DefaultStorage storage) {
    storage.response.headers["Content-Type"] = picture.picture.contentType;

    if(picture.picture.hasSize) {
      storage.response.headers["Content-Length"] = picture.picture.size.to!string;
    }

    storage.response.statusCode = 200;
    picture.picture.write(storage.response.bodyWriter);
  }
}