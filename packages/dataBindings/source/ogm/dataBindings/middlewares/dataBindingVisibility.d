module ogm.dataBindings.middlewares.dataBindingVisibility;

import vibe.http.server;
import vibe.data.json;
import crate.base;

class DataBindingVisibility {

  @create @put @patch
  hideRecord(HTTPServerRequest req) {
    req.json["dataBinding"]["visibility"]["isPublic"] = false;
  }
}