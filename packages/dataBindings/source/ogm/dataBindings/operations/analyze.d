module ogm.dataBindings.operations.analyze;

import crate.http.operations.base;
import ogm.crates.all;
import crate.http.operations.getItem;
import gis_collective.hmq.broadcast.base;
import crate.base;
import vibe.http.router;
import vibe.data.json;
import vibe.core.core;
import std.datetime;

class DatabindingAnalyzeOperation : GetItemApiOperation!DefaultStorage {
  IBroadcast broadcast;

  static {
    size_t timeout = 120;
    size_t interval = 1000;
  }

  OgmCrates crates;

  this(OgmCrates crates, IBroadcast broadcast) {
    this.broadcast = broadcast;
    CrateRule rule;

    rule.request.path = "/databindings/:id/analyze";
    rule.request.method = HTTPMethod.GET;

    this.crates = crates;

    super(crates.dataBinding, rule);
  }

  override void handle(DefaultStorage storage) {
    auto item = this.prepareItemOperation!"getItem"(storage);
    auto res = storage.response;

    if(storage.response.headerWritten) {
      return;
    }
    this.applyRule(storage);

    auto message = Json.emptyObject;
    message["id"] = item["_id"];
    broadcast.push("databinding.analyze", message);

    string id = item["_id"].to!string;
    string initialAnalyzedAt = item["analyzedAt"].to!string;

    int index = 0;
    while(index <= timeout) {
      index++;
      sleep(interval.msecs);

      item = crates.dataBinding.getItem(id).and.exec.front;

      if(initialAnalyzedAt != item["analyzedAt"].to!string) {
        break;
      }
    }

    res.statusCode = 201;
    res.writeVoidBody();
  }
}