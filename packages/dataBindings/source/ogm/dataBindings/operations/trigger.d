module ogm.dataBindings.operations.trigger;

import crate.http.operations.base;
import ogm.crates.all;
import crate.http.operations.getItem;
import gis_collective.hmq.broadcast.base;
import crate.base;
import vibe.http.router;
import vibe.data.json;
import ogm.http.request;

class DatabindingTriggerOperation : GetItemApiOperation!DefaultStorage {
  IBroadcast broadcast;

  this(OgmCrates crates, IBroadcast broadcast) {
    this.broadcast = broadcast;
    CrateRule rule;

    rule.request.path = "/databindings/:id/trigger";
    rule.request.method = HTTPMethod.GET;

    super(crates.dataBinding, rule);
  }

  override void handle(DefaultStorage storage) {
    auto item = this.prepareItemOperation!"getItem"(storage);
    auto res = storage.response;

    if(storage.response.headerWritten) {
      return;
    }
    this.applyRule(storage);

    auto request = RequestUserData(storage.request);

    auto message = Json.emptyObject;
    message["id"] = item["_id"];

    if(request.isAuthenticated) {
      message["userId"] = request.userId;
    }

    broadcast.push("databinding.trigger", message);

    res.statusCode = 201;
    res.writeVoidBody();
  }
}