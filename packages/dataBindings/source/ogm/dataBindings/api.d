/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.dataBindings.api;

import vibe.http.router;
import vibe.core.log;
import vibe.data.json;

import crate.http.router;
import crate.base;

import ogm.auth;
import ogm.crates.all;

import gis_collective.hmq.broadcast.base;
import ogm.dataBindings.middlewares.dataBindingVisibility;
import ogm.dataBindings.operations.analyze;
import ogm.dataBindings.operations.trigger;
import ogm.filter.visibility;
import ogm.middleware.adminrequest;
import ogm.middleware.creationRightsFlag;
import ogm.middleware.modelinfo;
import ogm.middleware.PublisherTeamMiddleware;
import ogm.middleware.SlugMiddleware;
import ogm.middleware.userdata;
import ogm.operations.space.PagesMap;
import vibe.service.configuration.general;

///
void setupDataBindingApi(T)(CrateRouter!T crateRouter, OgmCrates crates, IBroadcast broadcast) {
  crateGetters["Team"] = &crates.team.getItem;
  crateGetters["Picture"] = &crates.picture.getItem;

  auto auth = Authentication.instance(crates.user);
  auto adminRequest = new AdminRequest(crates.user);

  auto userDataMiddleware = UserDataMiddleware.instance(crates);
  auto modelInfo = new ModelInfoMiddleware("dataBinding", crates.dataBinding);
  auto visibilityFilter = new VisibilityFilter!"dataBindings"(crates, crates.dataBinding);
  auto dataBindingVisibility = new DataBindingVisibility();
  auto trigger = new DatabindingTriggerOperation(crates, broadcast);
  auto analyze = new DatabindingAnalyzeOperation(crates, broadcast);
  auto creationRightsFlag = new CreationRightsFlag!"dataBinding"(crates, "allowCustomDataBindings");

  auto preparedRoute = crateRouter
    .prepare(crates.dataBinding)
    .withCustomOperation(trigger)
    .withCustomOperation(analyze)
    .and(auth.privateDataMiddleware)
    .and(adminRequest)
    .and(userDataMiddleware)
    .and(creationRightsFlag)
    .and(modelInfo)
    .and(dataBindingVisibility)
    .and(visibilityFilter);
}
