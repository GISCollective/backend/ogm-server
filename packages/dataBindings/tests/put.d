/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.dataBindings.put;

import tests.fixtures;
import vibe.http.common;
import ogm.defaults.teams;
import ogm.dataBindings.api;

alias suite = Spec!({
  URLRouter router;

  describe("Updating data bindings", {
    Json dataBinding;

    beforeEach({
      setupTestData();
      router = new URLRouter;

      crates.setupDefaultTeams;
      router.crateSetup.setupDataBindingApi(crates, new MemoryBroadcast);

      dataBinding = `{
        "name": "some name",
        "destination": {
          "type": "Map",
          "modelId": "000000000000000000000001",
          "deleteNonSyncedRecords": false
        },
        "extraIcons": [],
        "fields": [],
        "connection": {
          "type": "",
          "config": {}
        },
        "info": {},
        "updateBy": "",
        "crs": "",
        "descriptionTemplate": "",
        "visibility": {
          "isPublic": false,
          "team": "000000000000000000000001",
        }
      }`.parseJsonString;

      dataBinding = crates.dataBinding.addItem(dataBinding);

      dataBinding = `{ "dataBinding": {
        "name": "test",
        "destination": {
          "type": "Map",
          "modelId": "000000000000000000000001",
          "deleteNonSyncedRecords": false
        },
        "extraIcons": [],
        "fields": [],
        "updateBy": "",
        "crs": "",
        "descriptionTemplate": "",
        "connection": {
          "type": "",
          "config": {}
        },
        "visibility": {
          "isPublic": false,
          "team": "000000000000000000000001",
        }
      } }`.parseJsonString;
    });

    it("sets the visibility to false", {
      dataBinding["dataBinding"]["visibility"]["isPublic"] = true;

      router
        .request
        .put("/databindings/000000000000000000000001")
        .send(dataBinding)
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["dataBinding"]["visibility"]["isPublic"].to!bool.should.equal(false);
        });
    });

    it("can not change the team", {
      dataBinding["dataBinding"]["visibility"]["team"] = "000000000000000000000002";

      router
        .request
        .put("/databindings/000000000000000000000001")
        .send(dataBinding)
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "errors": [{
            "description": "You can't change the team.",
            "status": 403,
            "title": "Forbidden"
          }]}`.parseJsonString);
        });
    });

    describeCredentialsRule("update team data bindings", "yes", "data bindings", ["administrator", "owner"], (string type) {
      auto initialSize = crates.space.get.size;

      router
        .request
        .put("/databindings/000000000000000000000001")
        .send(dataBinding)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          crates.space.get.size.should.equal(initialSize);
          response.bodyJson["dataBinding"]["name"].to!string.should.equal("test");
        });
    });

    describeCredentialsRule("update team data bindings", "no", "data bindings", [ "leader", "member", "guest"], (string type) {
      auto initialSize = crates.space.get.size;

      router
        .request
        .put("/databindings/000000000000000000000001")
        .send(dataBinding)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{ "errors": [{
            "description": "You don't have enough rights to edit ` ~ "`000000000000000000000001`" ~ `.",
            "status": 403,
            "title": "Forbidden"
          }]}`).parseJsonString);
        });
    });

    describeCredentialsRule("update team data bindings", "-", "data bindings", "no rights", {
      router
        .request
        .put("/databindings/000000000000000000000001")
        .send(dataBinding)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
        });
    });

    describeCredentialsRule("update own data bindings", "yes", "data bindings", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      auto record = crates.dataBinding.getItem("000000000000000000000001").and.exec.front.clone;
      record["info"]["originalAuthor"] = userId[type];
      crates.dataBinding.updateItem(record);

      auto initialSize = crates.dataBinding.get.size;

      router
        .request
        .put("/databindings/000000000000000000000001")
        .send(dataBinding)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          crates.dataBinding.get.size.should.equal(initialSize);
          response.bodyJson["dataBinding"]["name"].to!string.should.equal("test");
        });
    });

    describeCredentialsRule("update own data bindings", "-", "data bindings", "no rights", {});
  });
});
