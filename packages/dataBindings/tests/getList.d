/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.dataBindings.getlist;

import tests.fixtures;
import vibe.http.common;
import ogm.defaults.spaces;
import std.algorithm;
import ogm.defaults.teams;
import ogm.dataBindings.api;

alias suite = Spec!({
  URLRouter router;

  describe("Get data binding list", {
    Json dataBinding1;

    beforeEach({
      setupTestData();
      router = new URLRouter;

      crates.setupDefaultTeams;
      router.crateSetup.setupDataBindingApi(crates, new MemoryBroadcast);

      dataBinding1 = `{
        "name": "some name",
        "destination": {
          "type": "Map",
          "id": "000000000000000000000001",
          "deleteNonSyncedRecords": false
        },
        "extraIcons": [],
        "fields": [],
        "info": {},
        "updateBy": "",
        "crs": "",
        "descriptionTemplate": "",
        "connection": {
          "type": "",
          "config": {}
        },
        "visibility": {
          "isPublic": false,
          "team": "000000000000000000000001",
        }
      }`.parseJsonString;

      dataBinding1 = crates.dataBinding.addItem(dataBinding1);
    });

    describeCredentialsRule("get the public data binding list", "yes", "data bindings", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      auto initialSize = crates.dataBinding.get.size;

      router
        .request
        .get("/databindings")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["dataBindings"].byValue.map!(a => a["_id"].to!string).should.contain("000000000000000000000001");
        });
    });

    describeCredentialsRule("get the public data binding list", "yes", "data bindings", "no rights", {
      router
        .request
        .get("/databindings")
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "error": "Authorization required"
          }`.parseJsonString);
        });
    });

    describe("when there is a private data binding", {
      beforeEach({
        auto dataBinding = crates.dataBinding.getItem("000000000000000000000001").and.exec.front;
        dataBinding["visibility"]["isPublic"] = false;
        dataBinding["visibility"]["team"] = "000000000000000000000001";

        crates.dataBinding.updateItem(dataBinding);
      });

      describeCredentialsRule("get the private data binding list", "yes", "data bindings", ["administrator", "owner", "leader", "member", "guest"], (string type) {
        auto initialSize = crates.dataBinding.get.size;

        router
          .request
          .get("/databindings")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["dataBindings"].byValue.map!(a => a["_id"].to!string).should.contain("000000000000000000000001");
          });
      });

      describeCredentialsRule("get the private data binding list", "no", "data bindings", "no rights", {
        router
          .request
          .get("/databindings")
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "error": "Authorization required"
            }`.parseJsonString);
          });
      });

      describeCredentialsRule("get the own data binding list", "yes", "data bindings", ["administrator", "owner", "leader", "member", "guest"], (string type) {
        auto editablespace = crates.dataBinding.getItem("000000000000000000000001").and.exec.front;
        editablespace["info"]["originalAuthor"] = userId[type];
        crates.dataBinding.updateItem(editablespace);

        router
          .request
          .get("/databindings")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["dataBindings"].byValue.map!(a => a["_id"].to!string).should.contain("000000000000000000000001");
          });
      });

      describeCredentialsRule("get the own data binding list", "-", "data bindings", "no rights", {});
    });
  });
});
