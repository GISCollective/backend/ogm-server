module tests.dataBindngs.trigger;

import tests.fixtures;
import vibe.http.common;
import std.datetime;
import ogm.defaults.teams;
import ogm.dataBindings.api;

alias suite = Spec!({
  URLRouter router;
  Json broadcastValue;

  describe("the trigger operation", {
    Json dataBinding;
    MemoryBroadcast broadcast;

    beforeEach({
      setupTestData();
      router = new URLRouter;

      crates.setupDefaultTeams;
      broadcast = new MemoryBroadcast;

      broadcastValue = Json.emptyObject;
      void testHandler(const Json value) @trusted {
        broadcastValue = value;
      }

      broadcast.register("databinding.trigger", &testHandler);

      router.crateSetup.setupDataBindingApi(crates, broadcast);

      dataBinding = `{
        "name": "some name",
        "destination": {
          "type": "Map",
          "id": "000000000000000000000001",
          "deleteNonSyncedRecords": false
        },
        "extraIcons": [],
        "fields": [],
        "updateBy": "",
        "crs": "",
        "descriptionTemplate": "",
        "connection": {
          "type": "",
          "config": {}
        },
        "visibility": {
          "isPublic": false,
          "team": "000000000000000000000001",
        }
      }`.parseJsonString;

      dataBinding = crates.dataBinding.addItem(dataBinding);
    });

    it("can trigger the job", {
      router
        .request
        .get("/databindings/000000000000000000000001/trigger")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(201)
        .end((Response response) => () {
          broadcastValue.should.equal(`{
            "id": "000000000000000000000001",
            "userId": "000000000000000000000005"
          }`.parseJsonString);
        });
    });
  });

  describe("the analyze operation", {
    Json dataBinding;
    MemoryBroadcast broadcast;

    beforeEach({
      setupTestData();
      router = new URLRouter;

      crates.setupDefaultTeams;
      broadcast = new MemoryBroadcast;

      broadcastValue = Json.emptyObject;
      void testHandler(const Json value) @trusted {
        broadcastValue = value;

        auto item = crates.dataBinding.getItem(value["id"].to!string).and.exec.front;
        item["analyzedAt"] = Clock.currTime.toISOExtString;

        crates.dataBinding.updateItem(item);
      }

      broadcast.register("databinding.analyze", &testHandler);

      router.crateSetup.setupDataBindingApi(crates, broadcast);

      dataBinding = `{
        "name": "some name",
        "destination": {
          "type": "Map",
          "id": "000000000000000000000001",
          "deleteNonSyncedRecords": false
        },
        "extraIcons": [],
        "fields": [],
        "updateBy": "",
        "crs": "",
        "descriptionTemplate": "",
        "connection": {
          "type": "",
          "config": {}
        },
        "visibility": {
          "isPublic": false,
          "team": "000000000000000000000001",
        }
      }`.parseJsonString;

      dataBinding = crates.dataBinding.addItem(dataBinding);
    });

    it("can analyze the job", {
      router
        .request
        .get("/databindings/000000000000000000000001/analyze")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(201)
        .end((Response response) => () {
          broadcastValue.should.equal(`{
            "id": "000000000000000000000001"
          }`.parseJsonString);
        });
    });
  });
});