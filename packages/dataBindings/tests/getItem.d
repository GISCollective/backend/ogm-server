/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.dataBindings.getItem;

import tests.fixtures;
import vibe.http.common;
import ogm.defaults.teams;
import ogm.dataBindings.api;

alias suite = Spec!({
  URLRouter router;
  string aboutId;

  describe("get data binding by id", {
    Json dataBinding;

    beforeEach({
      setupTestData();
      router = new URLRouter;

      crates.setupDefaultTeams;
      router.crateSetup.setupDataBindingApi(crates, new MemoryBroadcast);

      dataBinding = `{
        "name": "some name",
        "destination": {
          "type": "Map",
          "id": "000000000000000000000001",
          "deleteNonSyncedRecords": false
        },
        "extraIcons": [],
        "fields": [],
        "updateBy": "",
        "crs": "",
        "descriptionTemplate": "",
        "connection": {
          "type": "",
          "config": {}
        },
        "visibility": {
          "isPublic": false,
          "team": "000000000000000000000001",
        }
      }`.parseJsonString;

      dataBinding = crates.dataBinding.addItem(dataBinding);
    });

    describeCredentialsRule("get data binding by id", "yes", "data bindings", ["administrator", "owner"], (string type) {
      router
        .request
        .get("/databindings/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["dataBinding"]["_id"].should.equal("000000000000000000000001");
          response.bodyJson["dataBinding"]["canEdit"].to!bool.should.equal(true);
        });
    });

    describeCredentialsRule("get data binding by id", "yes", "data bindings", ["leader", "member", "guest"], (string type) {
      router
        .request
        .get("/databindings/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["dataBinding"]["_id"].should.equal("000000000000000000000001");
          response.bodyJson["dataBinding"]["canEdit"].to!bool.should.equal(false);
        });
    });

    describeCredentialsRule("get data binding by id", "yes", "data bindings", "no rights", {
      router
        .request
        .get("/databindings/000000000000000000000001")
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "error": "Authorization required"
          }`.parseJsonString);
        });
    });

    describe("when the data binding belongs to another team", {
      beforeEach({
        dataBinding["visibility"]["team"] = `000000000000000000000003`;

        crates.dataBinding.updateItem(dataBinding);
      });

      it("can be fetched by the admin", {
        router
          .request
          .get("/databindings/000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["dataBinding"]["_id"].should.equal("000000000000000000000001");
            response.bodyJson["dataBinding"]["canEdit"].to!bool.should.equal(true);
          });
      });

      it("can not be fetched by an owner of another team", {
        router
          .request
          .get("/databindings/000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList["owner"])
          .expectStatusCode(404)
          .end((Response response) => () {
            response.bodyJson.should.equal((`{
              "errors": [{
                "description": "There is no item with id ` ~ "`000000000000000000000001`" ~ `",
                "status": 404,
                "title": "Crate not found"
              }]
            }`).parseJsonString);
          });
      });

      it("can not be fetched by an annonymous users", {
        router
          .request
          .get("/databindings/000000000000000000000001")
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal((`{
              "error": "Authorization required"
            }`).parseJsonString);
          });
      });
    });
  });
});
