/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.dataBindings.post;

import tests.fixtures;
import vibe.http.common;
import ogm.defaults.teams;
import ogm.dataBindings.api;

alias suite = Spec!({
  URLRouter router;

  describe("Creating data bindings", {
    Json dataBinding;

    beforeEach({
      setupTestData();
      router = new URLRouter;

      crates.setupDefaultTeams;
      router.crateSetup.setupDataBindingApi(crates, new MemoryBroadcast);

      dataBinding = `{ "dataBinding": {
        "name": "some name",
        "type": "file",
        "destination": {
          "type": "Map",
          "modelId": "000000000000000000000001",
          "deleteNonSyncedRecords": false
        },
        "extraIcons": [],
        "fields": [],
        "updateBy": "",
        "crs": "",
        "descriptionTemplate": "",
        "connection": {
          "type": "",
          "config": {}
        },
        "visibility": {
          "isPublic": false,
          "team": "000000000000000000000001",
        }
      } }`.parseJsonString;

      auto team = crates.team.getItem("000000000000000000000001").and.exec.front;
      team["allowCustomDataBindings"] = true;

      crates.team.updateItem(team);
    });

    it("sets the visibility to false", {
      dataBinding["dataBinding"]["visibility"]["isPublic"] = true;

      router
        .request
        .post("/databindings")
        .send(dataBinding)
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["dataBinding"]["visibility"]["isPublic"].to!bool.should.equal(false);
        });
    });

    it("can not create a data binding with the team that has allowCustomDataBindings set to false", {
      auto team = crates.team.getItem("000000000000000000000001").and.exec.front;
      team["allowCustomDataBindings"] = false;
      crates.team.updateItem(team);

      router
        .request
        .post("/databindings")
        .send(dataBinding)
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "errors": [{
            "description": "You can't create this dataBinding for this team.",
            "status": 403,
            "title": "Forbidden"
          }]}`.parseJsonString);
        });
    });

    describeCredentialsRule("create for own team", "yes", "data bindings", ["administrator"], (string type) {
      auto initialSize = crates.dataBinding.get.size;

      router
        .request
        .post("/databindings")
        .send(dataBinding)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          crates.dataBinding.get.size.should.equal(initialSize + 1);
        });
    });

    describeCredentialsRule("create for own team", "yes", "data bindings", ["owner"], (string type) {
      auto initialSize = crates.dataBinding.get.size;

      router
        .request
        .post("/databindings")
        .send(dataBinding)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["dataBinding"]["_id"].should.equal(`000000000000000000000001`);
        });
    });


    describeCredentialsRule("create for own team", "no", "data bindings", ["leader", "member", "guest"], (string type) {
      auto initialSize = crates.dataBinding.get.size;

      router
        .request
        .post("/databindings")
        .send(dataBinding)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"errors":[{"description":"You don't have enough rights to add an item.","status":403,"title":"Forbidden"}]}`.parseJsonString);
        });
    });

    describeCredentialsRule("create for own team", "-", "data bindings", "no rights", {
      router
        .request
        .post("/databindings")
        .send(dataBinding)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
        });
    });
  });
});
