/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.dataBindings.delete_;

import tests.fixtures;
import vibe.http.common;
import ogm.defaults.teams;
import ogm.dataBindings.api;

alias suite = Spec!({
  URLRouter router;
  Json dataBinding;

  describe("Deleting data bindings", {
    beforeEach({
      setupTestData();
      router = new URLRouter;

      crates.setupDefaultTeams;
      router.crateSetup.setupDataBindingApi(crates, new MemoryBroadcast);

      dataBinding = `{
        "name": "some name",
        "destination": {
          "type": "Map",
          "id": "000000000000000000000001",
          "deleteNonSyncedRecords": false
        },
        "extraIcons": [],
        "fields": [],
        "updateBy": "",
        "crs": "",
        "descriptionTemplate": "",
        "connection": {
          "type": "",
          "config": {}
        },
        "visibility": {
          "isPublic": false,
          "team": "000000000000000000000001",
        },
        "info": {}
      }`.parseJsonString;

      dataBinding = crates.dataBinding.addItem(dataBinding);
    });

    describeCredentialsRule("delete any team data binding", "yes", "data bindings", ["administrator", "owner"], (string type) {
      auto initialSize = crates.dataBinding.get.size;

      router
        .request
        .delete_("/databindings/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end((Response response) => () {
          crates.dataBinding.get.where("_id").equal(ObjectId.fromString("000000000000000000000001")).and.exec.empty.should.equal(true);
          crates.dataBinding.get.size.should.equal(initialSize - 1);
        });
    });

    describeCredentialsRule("delete any team data binding", "no", "data bindings", ["leader", "member", "guest"], (string type) {
      auto initialSize = crates.dataBinding.get.size;

      router
        .request
        .delete_("/databindings/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          crates.dataBinding.get.size.should.equal(initialSize);
          crates.dataBinding.get.where("_id").equal(ObjectId.fromString("000000000000000000000001")).and.exec.empty.should.equal(false);
        });
    });

    describeCredentialsRule("delete any team data binding", "-", "data bindings", "no rights", {
      router
        .request
        .delete_("/databindings/000000000000000000000001")
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.toPrettyString.should.equal(`{"error": "Authorization required"}`.parseJsonString.toPrettyString);
        });
    });

    describeCredentialsRule("delete own data binding", "yes", "data bindings", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      auto dataBinding = crates.dataBinding.getItem("000000000000000000000001").and.exec.front;
      dataBinding["info"]["originalAuthor"] = userId[type];
      crates.dataBinding.updateItem(dataBinding);

      auto initialSize = crates.dataBinding.get.size;

      router
        .request
        .delete_("/databindings/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end((Response response) => () {
          crates.dataBinding.get.size.should.equal(initialSize - 1);
          crates.dataBinding.get.where("_id").equal(ObjectId.fromString("000000000000000000000001")).and.exec.empty.should.equal(true);
        });
    });

    describeCredentialsRule("delete own data binding", "-", "data bindings", "no rights", {});
  });
});
