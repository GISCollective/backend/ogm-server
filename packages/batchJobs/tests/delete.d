/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.batchjobs.delete_;

import tests.fixtures;
import ogm.batchjobs.api;

import std.algorithm;
import std.array;

alias suite = Spec!({
  URLRouter router;

  describe("DELETE", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupBatchJobApi(crates);

      crates.batchJob.addItem(`{
        "_id": "000000000000000000000001",
        "name": "test",
        "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" },
        "runHistory": [],
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);

      crates.batchJob.addItem(`{
        "_id": "000000000000000000000002",
        "name": "test",
        "visibility": { "isDefault": false, "isPublic": false, "team": "000000000000000000000002" },
        "runHistory": [],
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);

      crates.batchJob.addItem(`{
        "_id": "000000000000000000000003",
        "name": "test",
        "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000003" },
        "runHistory": [],
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);

      crates.batchJob.addItem(`{
        "_id": "000000000000000000000003",
        "name": "test",
        "visibility": { "isDefault": false, "isPublic": false, "team": "000000000000000000000004" },
        "runHistory": [],
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);
    });

    describe("without a token", {
      it("should respond with error" , {
        router
          .request
          .delete_("/batchjobs/000000000000000000000003")
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "error": "Authorization required"
            }`.parseJsonString);
          });
      });
    });

    describe("with an administrator token", {
      it("should delete a batch job that the user does not have access to" , {
        router
          .request
          .delete_("/batchjobs/000000000000000000000003")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(204)
          .end((Response response) => () {
            crates.batchJob.get.exec
              .filter!(a => a["_id"] == "000000000000000000000003")
                .array.length.should.equal(0);
          });
      });
    });

    describe("with an owner token", {
      it("should throw error on trying to delete a batch job that the user does not have access to" , {
        router
          .request
          .delete_("/batchjobs/000000000000000000000003")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(404)
          .end((Response response) => () {
            response.bodyJson.should.equal("{
              \"errors\": [
                {
                  \"description\": \"Item `000000000000000000000003` not found.\",
                  \"title\": \"Crate not found\",
                  \"status\": 404
                }
              ]
            }".parseJsonString);
          });
      });

      it("should delete a private batch job from a team that the user has access to" , {
        crates.batchJob.get.exec
          .filter!(a => a["_id"] == "000000000000000000000002")
            .array.length.should.equal(1);

        router
          .request
          .delete_("/batchjobs/000000000000000000000002")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(204)
          .end((Response response) => () {
            crates.batchJob.get.exec
              .filter!(a => a["_id"] == "000000000000000000000002")
                .array.length.should.equal(0);
          });
      });
    });

    describe("with a leader token", {
      it("should throw error on trying to delete a batch job that the user does not have access to" , {
        router
          .request
          .delete_("/batchjobs/000000000000000000000003")
          .header("Authorization", "Bearer " ~ bearerLeaderToken.name)
          .expectStatusCode(404)
          .end((Response response) => () {
            response.bodyJson.should.equal("{
              \"errors\":[{
                  \"description\": \"Item `000000000000000000000003` not found.\",
                  \"title\": \"Crate not found\",
                  \"status\": 404
                }]}".parseJsonString);
          });
      });

      it("should not delete a private batch job from a team that the user has access to" , {
        crates.batchJob.get.exec
          .filter!(a => a["_id"] == "000000000000000000000002")
            .array.length.should.equal(1);

        router
          .request
          .delete_("/batchjobs/000000000000000000000002")
          .header("Authorization", "Bearer " ~ bearerLeaderToken.name)
          .expectStatusCode(403)
          .end((Response response) => () {
            crates.batchJob.get.exec
              .filter!(a => a["_id"] == "000000000000000000000002")
                .array.length.should.equal(1);

            response.bodyJson.should.equal("{
              \"errors\": [{
                  \"description\": \"You don't have enough rights to remove `000000000000000000000002`.\",
                  \"title\": \"Forbidden\",
                  \"status\": 403
                }]}".parseJsonString);
          });
      });
    });

    describe("with a member token", {
      it("should throw error on trying to delete a batch job that the user does not have access to" , {
        router
          .request
          .delete_("/batchjobs/000000000000000000000003")
          .header("Authorization", "Bearer " ~ bearerMemberToken.name)
          .expectStatusCode(404)
          .end((Response response) => () {
            response.bodyJson.should.equal("{
              \"errors\":[{
                  \"description\": \"Item `000000000000000000000003` not found.\",
                  \"title\": \"Crate not found\",
                  \"status\": 404
                }]}".parseJsonString);
          });
      });

      it("should not delete a private batch job from a team that the user has access to" , {
        crates.batchJob.get.exec
          .filter!(a => a["_id"] == "000000000000000000000002")
            .array.length.should.equal(1);

        router
          .request
          .delete_("/batchjobs/000000000000000000000002")
          .header("Authorization", "Bearer " ~ bearerMemberToken.name)
          .expectStatusCode(403)
          .end((Response response) => () {
            crates.batchJob.get.exec
              .filter!(a => a["_id"] == "000000000000000000000002")
                .array.length.should.equal(1);

            response.bodyJson.should.equal("{
              \"errors\": [{
                  \"description\": \"You don't have enough rights to remove `000000000000000000000002`.\",
                  \"title\": \"Forbidden\",
                  \"status\": 403
                }]}".parseJsonString);
          });
      });
    });

    describe("with a guest token", {
      it("should throw error on trying to delete a batch job that the user does not have access to" , {
        router
          .request
          .delete_("/batchjobs/000000000000000000000003")
          .header("Authorization", "Bearer " ~ bearerGuestToken.name)
          .expectStatusCode(404)
          .end((Response response) => () {
            response.bodyJson.should.equal("{
              \"errors\":[{
                  \"description\": \"Item `000000000000000000000003` not found.\",
                  \"title\": \"Crate not found\",
                  \"status\": 404
                }]}".parseJsonString);
          });
      });

      it("should not delete a private batch job from a team that the user has access to" , {
        crates.batchJob.get.exec
          .filter!(a => a["_id"] == "000000000000000000000002")
            .array.length.should.equal(1);

        router
          .request
          .delete_("/batchjobs/000000000000000000000002")
          .header("Authorization", "Bearer " ~ bearerGuestToken.name)
          .expectStatusCode(403)
          .end((Response response) => () {
            crates.batchJob.get.exec
              .filter!(a => a["_id"] == "000000000000000000000002")
                .array.length.should.equal(1);

            response.bodyJson.should.equal("{
              \"errors\": [{
                  \"description\": \"You don't have enough rights to remove `000000000000000000000002`.\",
                  \"title\": \"Forbidden\",
                  \"status\": 403
                }]}".parseJsonString);
          });
      });
    });
  });
});
