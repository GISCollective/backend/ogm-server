/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.batchjobs.get.list.member;

import tests.fixtures;
import ogm.batchjobs.api;

import std.algorithm;
import std.array;

alias suite = Spec!({
  URLRouter router;

  describe("with a member token", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupBatchJobApi(crates);

      crates.batchJob.addItem(`{
        "_id": "000000000000000000000001",
        "name": "test",
        "visibility": { "isDefault": false, "isPublic": false, "team": "000000000000000000000001" },
        "runHistory": [],
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);

      crates.batchJob.addItem(`{
        "_id": "000000000000000000000002",
        "name": "test",
        "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000003" },
        "runHistory": [],
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);
    });

    it("should return both the public and joined items" , {
      router
        .request
        .get("/batchjobs")
        .header("Authorization", "Bearer " ~ bearerLeaderToken.name)
        .expectStatusCode(200)
        .end((Response response) => () {
          auto item1 = crates.batchJob.getItem("000000000000000000000001").exec.front.to!string.parseJsonString;
          item1["canEdit"] = false;
          auto item2 = crates.batchJob.getItem("000000000000000000000002").exec.front.to!string.parseJsonString;
          item2["canEdit"] = false;

          auto batchjobs = cast(Json[]) response.bodyJson["batchJobs"];
          batchjobs.length.should.equal(2);
          batchjobs.should.contain(item1);
          batchjobs.should.contain(item2);
        });
    });

    it("should get no items when the 'edit' query string is present", {
      router
        .request
        .get("/batchjobs?edit=true")
        .header("Authorization", "Bearer " ~ bearerMemberToken.name)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"batchJobs":[]}`.parseJsonString);
        });
    });
  });
});
