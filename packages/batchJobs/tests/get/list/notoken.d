/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.batchjobs.get.list.notoken;

import tests.fixtures;
import ogm.batchjobs.api;

import std.algorithm;
import std.array;

alias suite = Spec!({
  URLRouter router;

  describe("When there is no token provided", {
    before({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupBatchJobApi(crates);
    });

    it("should throw authorization error for lists", {
      router
        .request
        .get("/batchjobs")
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "error": "Authorization required" }`.parseJsonString);
        });
    });

    it("should throw authorization error for edit lists", {
      router
        .request
        .get("/batchjobs?edit=true")
        .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{ "error": "Authorization required" }`.parseJsonString);
          });
    });

    it("should throw authorization error for items", {
      router
        .request
        .get("/batchjobs/000000000000000000000001")
        .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{ "error": "Authorization required" }`.parseJsonString);
          });
    });
  });
});
