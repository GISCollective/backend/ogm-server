/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.batchjobs.get.list.invalidtoken;

import tests.fixtures;
import ogm.batchjobs.api;

import std.algorithm;
import std.array;

alias suite = Spec!({
  URLRouter router;

  describe("with an invalid token", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupBatchJobApi(crates);
    });

    it("should return an error", {
      router
        .request
        .get("/batchjobs")
        .header("Authorization", "Bearer random")
        .expectStatusCode(400)
        .end((Response response) => () {
          response.bodyJson.should.equal("{\"error\": \"Invalid token.\"}".parseJsonString);
        });
    });
  });
});
