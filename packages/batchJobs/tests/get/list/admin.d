/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.batchjobs.get.list.admin;

import tests.fixtures;
import ogm.batchjobs.api;

import std.algorithm;
import std.array;

alias suite = Spec!({
  URLRouter router;

  describe("with an admin token", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupBatchJobApi(crates);

      crates.batchJob.addItem(`{
        "_id": "000000000000000000000001",
        "name": "test",
        "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" },
        "runHistory": [],
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);
    });

    it("should return all batch jobs" , {
      router
        .request
        .get("/batchjobs")
        .header("Authorization", "Bearer " ~ administratorToken.name)
        .expectStatusCode(200)
        .end((Response response) => () {
          auto item = crates.batchJob.getItem("000000000000000000000001").exec.front.to!string.parseJsonString;
          item["canEdit"] = true;

          auto batchjobs = cast(Json[]) response.bodyJson["batchJobs"];
          batchjobs.length.should.equal(1);
          batchjobs.should.contain(item);
        });
    });

    it("should get all user batchjobs when the 'edit' query string is present", {
      router
        .request
        .get("/batchjobs?edit=true")
        .header("Authorization", "Bearer " ~ administratorToken.name)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"batchJobs": []}`.parseJsonString);
        });
    });


    it("should get all batchjobs when the 'edit' query string is present and `all` is true", {
      router
        .request
        .get("/batchjobs?edit=true&all=true")
        .header("Authorization", "Bearer " ~ administratorToken.name)
        .expectStatusCode(200)
        .end((Response response) => () {

          auto item = crates.batchJob.getItem("000000000000000000000001").exec.front.to!string.parseJsonString;
          item["canEdit"] = true;

          auto batchJobs = cast(Json[]) response.bodyJson["batchJobs"];
          batchJobs.length.should.equal(1);
          batchJobs.should.contain(item);
        });
    });

    it("should get all batchjobs containing a point", {
      router
        .request
        .get("/batchjobs?containing=-73.856077,40.848447")
        .header("Authorization", "Bearer " ~ administratorToken.name)
        .expectStatusCode(200)
        .end((Response response) => () {
          auto item = crates.batchJob.getItem("000000000000000000000001").exec.front.to!string.parseJsonString;
          item["canEdit"] = true;

          auto batchJobs = cast(Json[]) response.bodyJson["batchJobs"];
          batchJobs.length.should.equal(1);
          batchJobs.should.contain(item);
        });
    });
  });
});
