/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.batchjobs.patch;

import tests.fixtures;
import ogm.batchjobs.api;

import std.algorithm;
import std.array;

alias suite = Spec!({
  URLRouter router;
  Json data;

  describe("PATCH", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupBatchJobApi(crates);

      data = `{ "batchJob": {
        "visibility": {
          "team":"000000000000000000000001",
          "isPublic": true
        },
        "name":"batchJob23"
      }}`.parseJsonString;


      crates.batchJob.addItem(`{
        "_id": "000000000000000000000001",
        "name": "test",
        "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" },
        "runHistory": [],
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);

      crates.batchJob.addItem(`{
        "_id": "000000000000000000000002",
        "name": "test",
        "visibility": { "isDefault": false, "isPublic": false, "team": "000000000000000000000002" },
        "runHistory": [],
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);

      crates.batchJob.addItem(`{
        "_id": "000000000000000000000003",
        "name": "test",
        "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000003" },
        "runHistory": [],
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);

      crates.batchJob.addItem(`{
        "_id": "000000000000000000000004",
        "name": "test",
        "visibility": { "isDefault": false, "isPublic": false, "team": "000000000000000000000004" },
        "runHistory": [],
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);
    });

    describe("a request with an administrator token", {
      it("should upload a batch job belonging to a different team", {
        router
          .request
          .patch("/batchjobs/000000000000000000000003")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            auto item = crates.batchJob.getItem("000000000000000000000003").exec.front.to!string.parseJsonString;
            item["canEdit"] = true;

            response.bodyJson["batchJob"].should.equal(item);
          });
      });
    });

    describe("a request with an owner token", {
      it("should not patch a batch job that the user does not have access", {
        router
          .request
          .patch("/batchjobs/000000000000000000000003")
          .send(data)
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(404)
          .end((Response response) => () {
            response.bodyJson.should.equal("{
              \"errors\": [
                {
                  \"description\": \"Item `000000000000000000000003` not found.\",
                  \"title\": \"Crate not found\",
                  \"status\": 404
                }
              ]
            }".parseJsonString);
          });
      });

      it("should patch a batch job that the user have access", {
        router
          .request
          .patch("/batchjobs/000000000000000000000002")
          .send(data)
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            auto item = crates.batchJob.getItem("000000000000000000000002").exec.front.to!string.parseJsonString;
            item["canEdit"] = true;

            response.bodyJson["batchJob"].should.equal(item);
          });
      });
    });

    describe("a request with a leader token", {
      it("should return an error for updating a public batch job belonging to a different team", {
        router
          .request
          .patch("/batchjobs/000000000000000000000003")
          .header("Authorization", "Bearer " ~ bearerLeaderToken.name)
          .send(data)
          .expectStatusCode(404)
          .end((Response response) => () {
            response.bodyJson.should.equal("{
              \"errors\": [
                {
                  \"description\": \"Item `000000000000000000000003` not found.\",
                  \"title\": \"Crate not found\",
                  \"status\": 404
                }
              ]
            }".parseJsonString);
          });
      });

      it("should not be able to update a batch job that the user has access to", {
        router
          .request
          .patch("/batchjobs/000000000000000000000002")
          .header("Authorization", "Bearer " ~ bearerLeaderToken.name)
          .send(data)
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"errors\": [{
              \"description\": \"You don't have enough rights to edit `000000000000000000000002`.\",
              \"title\": \"Forbidden\",
              \"status\": 403
            }]}".parseJsonString);
          });
      });
    });

    describe("a request with a member token", {
      it("should return an error for updating a public batch job belonging to a different team", {
        router
          .request
          .put("/batchjobs/000000000000000000000003")
          .header("Authorization", "Bearer " ~ bearerMemberToken.name)
          .send(data)
          .expectStatusCode(404)
          .end((Response response) => () {
            response.bodyJson.should.equal("{ \"errors\": [
                {
                  \"description\": \"Item `000000000000000000000003` not found.\",
                  \"title\": \"Crate not found\",
                  \"status\": 404
                }]}".parseJsonString);
          });
      });

      it("should not be able to update a batch job that the user has access to", {
        router
          .request
          .patch("/batchjobs/000000000000000000000002")
          .header("Authorization", "Bearer " ~ bearerMemberToken.name)
          .send(data)
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"errors\": [{
                  \"description\": \"You don't have enough rights to edit `000000000000000000000002`.\",
                  \"title\": \"Forbidden\",
                  \"status\": 403
                }]}".parseJsonString);
          });
      });
    });

    describe("a request with a guest token", {
      it("should return an error for updating a public batch job belonging to a different team", {
        router
          .request
          .patch("/batchjobs/000000000000000000000003")
          .header("Authorization", "Bearer " ~ bearerGuestToken.name)
          .send(data)
          .expectStatusCode(404)
          .end((Response response) => () {
            response.bodyJson.should.equal("{ \"errors\": [
                {
                  \"description\": \"Item `000000000000000000000003` not found.\",
                  \"title\": \"Crate not found\",
                  \"status\": 404
                }]}".parseJsonString);
          });
      });

      it("should not be able to update a batch job that the user has access to", {
        router
          .request
          .patch("/batchjobs/000000000000000000000002")
          .header("Authorization", "Bearer " ~ bearerGuestToken.name)
          .send(data)
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"errors\": [{
                  \"description\": \"You don't have enough rights to edit `000000000000000000000002`.\",
                  \"title\": \"Forbidden\",
                  \"status\": 403
                }]}".parseJsonString);
          });
      });
    });
  });
});
