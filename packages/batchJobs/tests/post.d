/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.batchjobs.post;

import tests.fixtures;
import ogm.batchjobs.api;

import std.algorithm;
import std.array;

alias suite = Spec!({
  URLRouter router;
  Json data;
  Json dataOtherTeam;

  describe("POST", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupBatchJobApi(crates);

      data = `{ "batchJob": {
        "_id": "000000000000000000000001",
        "name": "test",
        "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" },
        "runHistory": []
      }}`.parseJsonString;

      dataOtherTeam = `{ "batchJob": {
        "_id": "000000000000000000000001",
        "name": "test",
        "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000004" },
        "runHistory": []
      }}`.parseJsonString;

    });

    describe("a request without token", {
      it("should return an error", {
        router
          .request
          .post("/batchjobs")
          .send(data)
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "error": "Authorization required"
            }`.parseJsonString);
          });
      });
    });

    describe("a request with an administrator token", {
      it("should add a batch job belonging to a different team", {
        router
          .request
          .post("/batchjobs")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            auto item = crates.batchJob.getItem("000000000000000000000001").exec.front.to!string.parseJsonString;
            item["canEdit"] = true;

            response.bodyJson["batchJob"].should.equal(item);
          });
      });
    });

    describe("a request with an owner token", {
      it("should return an error for adding a public batch job belonging to a different team", {
        router
          .request
          .post("/batchjobs")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .send(dataOtherTeam)
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal("{
              \"errors\": [
                {
                  \"description\": \"You can't add an item for the team `000000000000000000000004`.\",
                  \"title\": \"Forbidden\",
                  \"status\": 403
                }
              ]
            }".parseJsonString);
          });
      });

      it("should add a batch job with a team that the user has access to", {
        router
          .request
          .post("/batchjobs")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            auto item = crates.batchJob.getItem("000000000000000000000001").exec.front.to!string.parseJsonString;
            item["canEdit"] = true;

            response.bodyJson["batchJob"].should.equal(item);
          });
      });
    });

    describe("a request with a leader token", {
      it("should return an error for adding a public batch job belonging to a different team", {
        router
          .request
          .post("/batchjobs")
          .header("Authorization", "Bearer " ~ bearerLeaderToken.name)
          .send(data)
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal("{
              \"errors\": [
                {
                  \"description\": \"You don't have enough rights to add an item.\",
                  \"title\": \"Forbidden\",
                  \"status\": 403
                }
              ]
            }".parseJsonString);
          });
      });

      it("should not be able to add a batch job that the user has access to", {
        router
          .request
          .post("/batchjobs")
          .header("Authorization", "Bearer " ~ bearerLeaderToken.name)
          .send(data)
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"errors\": [{
              \"description\": \"You don't have enough rights to add an item.\",
              \"title\": \"Forbidden\",
              \"status\": 403
            }]}".parseJsonString);
          });
      });
    });

    describe("a request with a member token", {
      it("should return an error for updating a public batch job belonging to a different team", {
        router
          .request
          .post("/batchjobs")
          .header("Authorization", "Bearer " ~ bearerMemberToken.name)
          .send(dataOtherTeam)
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal("{ \"errors\": [
                {
                  \"description\": \"You can't add an item for the team `000000000000000000000004`.\",
                  \"title\": \"Forbidden\",
                  \"status\": 403
                }]}".parseJsonString);
          });
      });
    });

    describe("a request with a guest token", {
      it("should return an error for adding a batch job belonging to a different team", {
        router
          .request
          .post("/batchjobs")
          .header("Authorization", "Bearer " ~ bearerGuestToken.name)
          .send(dataOtherTeam)
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal("{ \"errors\": [
                {
                  \"description\": \"You can't add an item for the team `000000000000000000000004`.\",
                  \"title\": \"Forbidden\",
                  \"status\": 403
                }]}".parseJsonString);
          });
      });

      it("should not be able to add a batch job that the user has access to", {
        router
          .request
          .post("/batchjobs")
          .header("Authorization", "Bearer " ~ bearerGuestToken.name)
          .send(data)
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"errors\": [{
                  \"description\": \"You don't have enough rights to add an item.\",
                  \"title\": \"Forbidden\",
                  \"status\": 403
                }]}".parseJsonString);
          });
      });
    });
  });
});
