/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.batchjobs.api;

import vibe.http.router;
import crate.http.router;
import crate.base;

import ogm.auth;
import ogm.crates.all;

import ogm.middleware.adminrequest;
import ogm.middleware.userdata;

import ogm.filter.pagination;
import ogm.filter.searchterm;
import ogm.filter.visibility;
import ogm.filter.searchtag;

import ogm.maps.filter;
import ogm.middleware.modelinfo;

///
void setupBatchJobApi(T)(CrateRouter!T crateRouter, OgmCrates crates) {
  PictureFileSettings.files = crates.pictureFiles;
  PictureFileSettings.chunks = crates.pictureChunks;

  auto auth = Authentication.instance(crates.user);
  auto adminRequest = new AdminRequest(crates.user);

  auto userDataMiddleware = UserDataMiddleware.instance(crates);
  auto searchTermFilter = new SearchTermFilter;
  auto paginationFilter = new PaginationFilter;
  auto visibilityFilter = new VisibilityFilter!"batchJobs"(crates, crates.batchJob);
  auto modelInfo = new ModelInfoMiddleware("batchJob", crates.batchJob);

  crateGetters["Team"] = &crates.team.getItem;
  crateGetters["Picture"] = &crates.picture.getItem;

  crateRouter
    .add(crates.batchJob,
      auth.privateDataMiddleware,
      adminRequest,
      userDataMiddleware,
      modelInfo,
      visibilityFilter,
      searchTermFilter,
      paginationFilter);
}
