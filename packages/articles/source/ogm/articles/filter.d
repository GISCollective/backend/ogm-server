/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.articles.filter;

import ogm.http.request;
import ogm.crates.all;

import vibe.http.router;
import vibe.data.json;

import crate.base;
import crate.error;
import std.exception;
import std.algorithm;

///
class ArticlesFilter {
  OgmCrates crates;

  struct Parameters {
    @describe("Get only the visibility fields of the articles.")
    bool onlyVisibility;


    @describe("Get only articles related to a model id.")
    string relatedId;


    @describe("Get articles by type.")
    string type;
  }

  ///
  this(OgmCrates crates) {
    this.crates = crates;
  }

  Json getDefaultVisibility() {
    auto visibility = Json.emptyObject;

    visibility["team"] = crates.team.publishersId;
    visibility["isPublic"] = true;

    return visibility;
  }

  @get
  void get(IQuery selector, Parameters parameters) {
    if(parameters.onlyVisibility) {
      selector.withProjection(["_id", "visibility"]);
    }

    if(parameters.relatedId) {
      selector.where("relatedId").equal(parameters.relatedId);
    }

    if(parameters.type && parameters.type != "any") {
      selector.where("type").equal(parameters.type);
    }

    if(parameters.type == "any") {
      selector.where("type").not.containsAny(["newsletter-welcome-message", "newsletter-article", "template-newsletter-welcome-message"]);
    }
  }

  @patch @replace
  void checkSlug(HTTPServerRequest req, Json item) {
    if(req.json["article"].type != Json.Type.object) {
      return;
    }

    if(req.json["article"]["slug"].type != Json.Type.string) {
      return;
    }

    auto originalArticle = crates.article.getItem(req.params["id"]).and.exec.front;
    auto originalSlug = originalArticle["slug"].to!string;
    if(originalSlug.startsWith("notification-")) {
      req.json["article"]["slug"] = originalSlug;
    }

    auto slug = req.json["article"]["slug"].to!string;

    if(slug != "") {
      auto range = crates.article.get
        .where("slug").equal(slug).and
        .where("visibility.team").equal(ObjectId(req.json["article"]["visibility"]["team"])).and
        .exec;

      if(range.empty) {
        return;
      }

      if("id" in req.params && range.front["_id"] == req.params["id"]){
        return;
      }

      enforce!CrateValidationException(range.front["_id"] == req.json["article"]["_id"], "Another article with the same slug, already exists.");
    }
  }

  @create
  void checkNotificationSlug(HTTPServerRequest req, Json item) {
    if(req.json["article"].type != Json.Type.object) {
      return;
    }

    if(req.json["article"]["slug"].type != Json.Type.string) {
      return;
    }

    auto slug = req.json["article"]["slug"].to!string;
    enforce!CrateValidationException(!slug.canFind("notification-"), "You can't create new notifications");

    if(slug != "") {
      auto range = crates.article.get
        .where("slug").equal(slug).and
        .where("visibility.team").equal(ObjectId(req.json["article"]["visibility"]["team"])).and
        .exec;
      enforce!CrateValidationException(range.empty, "Another article with the same slug, already exists.");
    }
  }

  ///
  @patch @replace
  void replace(HTTPServerRequest req) {
    if(req.json["article"]["visibility"].type != Json.Type.object) {
      req.json["article"]["visibility"] = this.getDefaultVisibility;
    }
  }
}
