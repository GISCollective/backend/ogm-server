module ogm.articles.operations.unpublish;

import crate.base;
import crate.error;
import crate.http.operations.base;
import crate.http.operations.getItem;
import gis_collective.hmq.broadcast.base;
import ogm.crates.all;
import std.datetime;
import std.exception;
import std.algorithm;
import std.net.isemail;
import std.typecons : Yes, No;
import vibe.core.core;
import vibe.data.json;
import vibe.http.router;
import ogm.calendar;
import ogm.http.request;

class ArticleUnpublishOperation : CrateOperation!DefaultStorage {
  OgmCrates crates;

  this(OgmCrates crates) {
    CrateRule rule;

    rule.request.path = "/articles/:id/unpublish";
    rule.request.method = HTTPMethod.POST;

    this.crates = crates;

    super(crates.article, rule);
  }

  override void handle(DefaultStorage storage) {
    auto item = this.prepareItemOperation!"getItem"(storage);

    auto res = storage.response;

    if(storage.response.headerWritten) {
      return;
    }

    this.applyRule(storage);

    scope request = RequestUserData(storage.request);
    enforce!ForbiddenException(request.isAdmin || request.session(crates).owner.articles.canFind(item["_id"].to!string), "You can't unpublish this article.");

    if(item["type"] == "any") {
      item["visibility"]["isPublic"] = false;
      crates.article.updateItem(item);

      res.statusCode = 201;
      res.writeVoidBody;
      return;
    }

    enforce!CrateValidationException(item["type"] == "newsletter-article", "Only newsletter-articles can be unpublished.");
    enforce!CrateValidationException(item["status"] == "pending" || item["status"] == "publishing", "Only pending or publishing articles can be unpublished.");


    item["status"] = "draft";
    crates.article.updateItem(item);

    res.statusCode = 201;
    res.writeVoidBody;
  }
}