module ogm.articles.operations.test;

import crate.base;
import crate.error;
import crate.http.operations.base;
import crate.http.operations.getItem;
import gis_collective.hmq.broadcast.base;
import ogm.crates.all;
import std.datetime;
import std.exception;
import std.algorithm;
import std.net.isemail;
import std.typecons : Yes, No;
import vibe.core.core;
import vibe.data.json;
import vibe.http.router;
import ogm.calendar;
import ogm.http.request;

class ArticleTestOperation : CrateOperation!DefaultStorage {
  IBroadcast broadcast;

  OgmCrates crates;

  this(OgmCrates crates, IBroadcast broadcast) {
    this.broadcast = broadcast;
    CrateRule rule;

    rule.request.path = "/articles/:id/test";
    rule.request.method = HTTPMethod.POST;

    this.crates = crates;

    super(crates.article, rule);
  }

  override void handle(DefaultStorage storage) {
    auto item = this.prepareItemOperation!"getItem"(storage);

    auto res = storage.response;

    if(storage.response.headerWritten) {
      return;
    }

    this.applyRule(storage);

    scope request = RequestUserData(storage.request);
    enforce!ForbiddenException(request.isAdmin || request.session(crates).owner.articles.canFind(item["_id"].to!string), "You can't test this article.");

    enforce!CrateValidationException(item["type"].to!string.canFind("newsletter"), "Only newsletter articles can be tested.");

    auto message = Json.emptyObject;
    message["id"] = item["_id"];
    message["userId"] = request.userId;

    broadcast.push("article.test", message);

    res.statusCode = 201;
    res.writeVoidBody;
  }
}