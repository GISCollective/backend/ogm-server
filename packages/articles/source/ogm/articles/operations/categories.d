module ogm.articles.operations.categories;

import crate.base;
import crate.error;
import crate.http.operations.base;
import crate.http.operations.getItem;
import gis_collective.hmq.broadcast.base;
import ogm.crates.all;
import std.datetime;
import std.exception;
import std.array;
import std.algorithm;
import std.net.isemail;
import std.typecons : Yes, No;
import vibe.core.core;
import vibe.data.json;
import vibe.http.router;
import ogm.calendar;
import ogm.http.request;

class CategoriesOperation : CrateOperation!DefaultStorage {
  OgmCrates crates;

  this(OgmCrates crates) {
    CrateRule rule;

    rule.request.path = "/articles/_/categories";
    rule.request.method = HTTPMethod.GET;

    this.crates = crates;

    super(crates.article, rule);
  }

  override void handle(DefaultStorage storage) {
    this.prepareListOperation!"getItem"(storage);

    auto res = storage.response;

    if(storage.response.headerWritten) {
      return;
    }

    this.applyRule(storage);

    string[] categories;

    auto range = storage.query.withProjection(["categories"]).and.exec;
    string[] queryCategories;

    if("category" in storage.request.query) {
      queryCategories = storage.request.query["category"].split(",");
    }

    foreach(article; range) {
      if(article["categories"].type != Json.Type.array) {
        continue;
      }

      foreach (string category; article["categories"].deserializeJson!(string[])) {
        if(category == "" || queryCategories.canFind(category)) {
          continue;
        }

        if(!categories.canFind(category)) {
          categories ~= category;
        }
      }
    }

    res.writeJsonBody([ "categories": categories ], 200);
  }
}