module ogm.articles.operations.publish;

import crate.base;
import crate.error;
import crate.http.operations.base;
import crate.http.operations.getItem;
import gis_collective.hmq.broadcast.base;
import ogm.crates.all;
import std.datetime;
import std.exception;
import std.algorithm;
import std.net.isemail;
import std.typecons : Yes, No;
import vibe.core.core;
import vibe.data.json;
import vibe.http.router;
import ogm.calendar;
import ogm.http.request;

class ArticlePublishOperation : CrateOperation!DefaultStorage {
  IBroadcast broadcast;

  OgmCrates crates;

  this(OgmCrates crates, IBroadcast broadcast) {
    this.broadcast = broadcast;
    CrateRule rule;

    rule.request.path = "/articles/:id/publish";
    rule.request.method = HTTPMethod.POST;

    this.crates = crates;

    super(crates.article, rule);
  }

  override void handle(DefaultStorage storage) {
    auto item = this.prepareItemOperation!"getItem"(storage);

    auto res = storage.response;

    if(storage.response.headerWritten) {
      return;
    }

    this.applyRule(storage);

    scope request = RequestUserData(storage.request);
    enforce!ForbiddenException(request.isAdmin || request.session(crates).owner.articles.canFind(item["_id"].to!string), "You can't publish this article.");

    if(item["type"] == "any") {
      item["visibility"]["isPublic"] = true;
      crates.article.updateItem(item);

      res.statusCode = 201;
      res.writeVoidBody;
      return;
    }

    enforce!CrateValidationException(item["status"] == "draft" || item["status"] == "pending", "Only draft and pending articles can be published.");

    item["status"] = "pending";
    crates.article.updateItem(item);

    auto message = Json.emptyObject;
    message["id"] = item["_id"];

    broadcast.push("article.publish", message);

    res.statusCode = 201;
    res.writeVoidBody;
  }
}