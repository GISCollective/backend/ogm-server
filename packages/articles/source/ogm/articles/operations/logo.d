module ogm.articles.operations.logo;

import crate.base;
import crate.error;
import crate.http.operations.base;
import crate.http.operations.getItem;
import crate.http.operations.getResource;
import crate.lazydata.base;
import crate.resource.image;
import ogm.calendar;
import ogm.crates.all;
import ogm.http.request;
import ogm.models.metric;
import std.algorithm;
import std.array;
import std.datetime;
import std.exception;
import std.functional;
import vibe.core.path;
import vibe.data.json;
import vibe.http.router;
import gis_collective.hmq.log;

class ArticleLogoOperation : GetResourceApiOperation!(Picture, PictureFile, DefaultStorage, "picture") {
  OgmCrates crates;

  this(OgmCrates crates) {
    CrateRule rule;

    rule.request.path = "/articles/:id/logo";
    rule.request.method = HTTPMethod.GET;
    rule.response.statusCode = 200;

    this.crates = crates;

    super(crates.picture, rule);
  }

  void countVisit(DefaultStorage storage) {
    auto pieces = getPathVar!"id"(storage.request.requestPath, rule.request.path).split("-");
    auto article = crates.article.getItem(pieces[0]).and.exec.front;

    auto metric = Metric();
    metric.type = "articleView";
    metric.name = pieces[0];
    metric.value = 1;
    metric.time = SysCalendar.instance.now;
    metric.labels["relatedId"] = article["relatedId"].to!string;

    if(pieces.length == 2 && pieces[1] != "") {
      auto newsletterEmail = crates.newsletterEmail.getItem(pieces[1]).and.exec.front;
      metric.labels["email"] = newsletterEmail["email"].to!string;
    }

    crates.metric.addItem(metric.serializeToJson);
  }

  override Lazy!Picture getItem(DefaultStorage storage) {
    auto pieces = getPathVar!"id"(storage.request.requestPath, rule.request.path).split("-");
    auto article = crates.article.getItem(pieces[0]).and.exec.front;

    auto space = crates.space.get.where("visibility.isDefault").equal(true).and.exec.front;

    string logoId = space["logo"].to!string;

    auto item = crates.picture.getItem(logoId).and.exec.front;

    storage.properties.itemId = logoId;
    storage.request.params["id"] = logoId;

    handleMiddlewaresWithoutQueries!"getItem"(storage);

    if(storage.response.headerWritten) {
      return Lazy!Picture();
    }

    return Lazy!Picture(item, (&itemResolver).toDelegate);
  }

  override void handle(DefaultStorage storage) {
    countVisit(storage);
    auto value = getItem(storage);
    if(storage.response.headerWritten) {
      return;
    }

    auto nativeValue = value.toType;
    auto obj = nativeValue.picture;

    enforce!CrateNotFoundException(obj !is null, "The resource does not exist.");

    try {
      enum option = ImageSettings("png-512", "512", "512", "png");
      obj.crateHandler!option(storage.request, storage.response);
    } catch(CrateException err) {
      error(err);

      super.handle(storage);
    }
  }
}
