/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.articles.api;

import vibe.http.router;
import vibe.core.log;
import vibe.data.json;

import crate.http.router;
import crate.base;

import ogm.auth;
import ogm.crates.all;

import gis_collective.hmq.broadcast.base;
import ogm.defaults.articles;
import ogm.articles.filter;
import ogm.articles.operations.publish;
import ogm.articles.operations.unpublish;
import ogm.articles.operations.test;
import ogm.articles.operations.logo;
import ogm.filter.visibility;
import ogm.middleware.adminrequest;
import ogm.middleware.modelinfo;
import ogm.middleware.PublisherTeamMiddleware;
import ogm.middleware.SlugMiddleware;
import ogm.middleware.userdata;
import ogm.middleware.ResourceMiddleware;
import ogm.middleware.SortingMiddleware;
import ogm.articles.filters.category;
import ogm.articles.operations.categories;
import ogm.articles.middleware.releaseDate;
import ogm.filter.searchterm;
import ogm.filter.pagination;
import ogm.operations.BulkDeleteOperation;
import ogm.operations.BulkPublishOperation;
import ogm.operations.BulkUnpublishOperation;
import ogm.articles.middleware.cleanup;

///
void setupArticleApi(T)(CrateRouter!T crateRouter, OgmCrates crates, IBroadcast broadcast) {
  auto auth = Authentication.instance(crates.user);
  auto adminRequest = new AdminRequest(crates.user);

  auto userDataMiddleware = UserDataMiddleware.instance(crates);
  auto articlesFilter = new ArticlesFilter(crates);
  auto modelInfo = new ModelInfoMiddleware("article", crates.article);
  auto visibilityFilter = new VisibilityFilter!"articles"(crates, crates.article);
  auto slugMiddleware = new SlugMiddleware(crates.article);
  auto publishOperation = new ArticlePublishOperation(crates, broadcast);
  auto categoriesOperation = new CategoriesOperation(crates);
  auto testOperation = new ArticleTestOperation(crates, broadcast);
  auto unpublishOperation = new ArticleUnpublishOperation(crates);
  auto logoOperation = new ArticleLogoOperation(crates);
  auto picturesMiddleware = new ResourceMiddleware!("article", "pictures")(crates.article, crates.picture);
  auto sorting = new SortingMiddleware(crates);
  auto category = new CategoryFilter(crates);
  auto releaseDate = new ReleaseDateMiddleware(crates);
  auto searchTermFilter = new SearchTermFilter;
  auto paginationFilter = new PaginationFilter;
  auto cleanupMiddleware = new CleanupMiddleware(crates);

  auto bulkDelete = new BulkDeleteOperation!("article", "team")(crates, auth.privateDataMiddleware, [picturesMiddleware]);
  auto bulkPublish = new BulkPublishOperation!("article", "team")(crates, auth.privateDataMiddleware, [picturesMiddleware]);
  auto bulkUnpublish = new BulkUnpublishOperation!("article", "team")(crates, auth.privateDataMiddleware, [picturesMiddleware]);

  crateGetters["Team"] = &crates.team.getItem;
  crateGetters["Picture"] = &crates.picture.getItem;

  auto preparedRoute = crateRouter.prepare(crates.article)
    .withCustomOperation(categoriesOperation)
    .withCustomOperation(publishOperation)
    .withCustomOperation(unpublishOperation)
    .withCustomOperation(testOperation)
    .withCustomOperation(logoOperation)
    .withCustomOperation(bulkDelete)
    .withCustomOperation(bulkPublish)
    .withCustomOperation(bulkUnpublish)
    .and(auth.publicDataMiddleware)
    .and(adminRequest)
    .and(userDataMiddleware)
    .and(slugMiddleware)
    .and(visibilityFilter)
    .and(modelInfo)
    .and(articlesFilter)
    .and(releaseDate)
    .and(cleanupMiddleware)
    .and(picturesMiddleware)
    .and(category)
    .and(searchTermFilter)
    .and(sorting)
    .and(paginationFilter);
}
