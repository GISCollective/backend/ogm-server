module ogm.articles.middleware.releaseDate;


import ogm.http.request;
import crate.base;
import vibe.http.server;
import ogm.crates.all;
import vibe.data.json;
import std.datetime;
import ogm.calendar;
import ogm.http.request;

class ReleaseDateMiddleware {
  private {
    OgmCrates crates;
  }

  this(OgmCrates crates) {
    this.crates = crates;
  }

  ///
  @create
  void checkVisibility(HTTPServerRequest req) {
    SysTime releaseDate;

    try {
      releaseDate = SysTime.fromISOExtString(req.json["article"]["releaseDate"].to!string);
    } catch(Exception) {
      return;
    }

    if(releaseDate > Clock.currTime) {
      req.json["article"]["visibility"]["isPublic"] = false;
    }
  }

  ///
  @put
  void applyCategory(HTTPServerRequest req) {
    if("article" !in req.json) {
      return;
    }

    auto request = RequestUserData(req);

    auto storedArticle = crates.article.getItem(request.itemId).and.exec.front;

    if(storedArticle["visibility"]["isPublic"] != req.json["article"]["visibility"]["isPublic"]) {
      auto releaseDate = SysCalendar.instance.now;

      if(req.json["article"]["visibility"]["isPublic"] == false) {
        releaseDate += 3650.days;
      }


      req.json["article"]["releaseDate"] = releaseDate.toISOExtString;
    }

    this.checkVisibility(req);
  }
}