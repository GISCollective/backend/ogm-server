module ogm.articles.middleware.cleanup;


import ogm.http.request;
import crate.base;
import vibe.http.server;
import ogm.crates.all;
import vibe.data.json;
import std.datetime;
import std.string;
import ogm.calendar;
import ogm.http.request;

class CleanupMiddleware {
  private {
    OgmCrates crates;
  }

  this(OgmCrates crates) {
    this.crates = crates;
  }

  ///
  @put @create
  void cleanupCategory(HTTPServerRequest req) {
    if("article" !in req.json) {
      return;
    }

    auto request = RequestUserData(req);
    string[] categories;

    if(req.json["article"]["categories"].type == Json.Type.array) {
      foreach(category; req.json["article"]["categories"]) {
        auto strCategory = category.to!string.strip;

        if(strCategory != "") {
          categories ~= strCategory;
        }
      }
    }

    req.json["article"]["categories"] = categories.serializeToJson;
  }
}