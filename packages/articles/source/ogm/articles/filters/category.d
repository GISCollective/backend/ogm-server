module ogm.articles.filters.category;

import std.array;
import ogm.http.request;
import crate.base;
import vibe.http.server;
import ogm.crates.all;

class CategoryFilter {
  struct QueryParams {
    @describe("If is set the records that match teh category will be returned.")
    @example("Team", "Return articles that have the Team category.")
    string category;
  }

  private {
    OgmCrates crates;
  }

  this(OgmCrates crates) {
    this.crates = crates;
  }

  ///
  @getList @getItem
  void applyCategorry(IQuery selector, QueryParams params) {
    string[] queryCategories;

    if(params.category != "") {
      queryCategories = params.category.split(",");
    }

    if(queryCategories.length == 1) {
      selector.where("categories").arrayContains(queryCategories[0]);
    }

    if(queryCategories.length > 1) {
      selector.where("categories").containsAll(queryCategories);
    }
  }
}