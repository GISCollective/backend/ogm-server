/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.articles.delete_;

import tests.fixtures;
import vibe.http.common;
import ogm.defaults.teams;
import ogm.defaults.articles;
import ogm.articles.api;

alias suite = Spec!({
  URLRouter router;

  describe("Deleting articles", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupArticleApi(crates, new MemoryBroadcast);
      crates.setupDefaultTeams;
      crates.setupDefaultArticles;

      auto article = crates.article.getItem("000000000000000000000001").and.exec.front;
      article["visibility"]["isPublic"] = true;
      article["visibility"]["team"] = "000000000000000000000001";
      crates.article.updateItem(article);
    });

    describeCredentialsRule("delete any team articles", "yes", "articles", ["administrator", "owner"], (string type) {
      auto initialSize = crates.article.get.size;

      router
        .request
        .delete_("/articles/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end((Response response) => () {
          crates.article.get.size.should.equal(initialSize - 1);
          crates.article.get.where("_id").equal(ObjectId.fromString("000000000000000000000001")).and.exec.empty.should.equal(true);
        });
    });

    describeCredentialsRule("delete any team articles", "no", "articles", ["leader", "member", "guest"], (string type) {
      auto initialSize = crates.article.get.size;

      router
        .request
        .delete_("/articles/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          crates.article.get.size.should.equal(initialSize);
          crates.article.get.where("_id").equal(ObjectId.fromString("000000000000000000000001")).and.exec.empty.should.equal(false);
        });
    });

    describeCredentialsRule("delete any team articles", "-", "articles", "no rights", {
      router
        .request
        .delete_("/articles/000000000000000000000001")
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.toPrettyString.should.equal(`{"error": "Authorization required"}`.parseJsonString.toPrettyString);
        });
    });


    describeCredentialsRule("delete own articles", "yes", "articles", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      auto article = crates.article.getItem("000000000000000000000001").and.exec.front;
      article["info"]["originalAuthor"] = userId[type];
      crates.article.updateItem(article);

      auto initialSize = crates.article.get.size;

      router
        .request
        .delete_("/articles/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end((Response response) => () {
          crates.article.get.size.should.equal(initialSize - 1);
          crates.article.get.where("_id").equal(ObjectId.fromString("000000000000000000000001")).and.exec.empty.should.equal(true);
        });
    });

    describeCredentialsRule("delete own articles", "-", "articles", "no rights", {});
  });
});
