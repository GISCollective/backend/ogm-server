module tests.articles.logo;

import tests.fixtures;
import vibe.http.common;
import std.datetime;
import ogm.calendar;
import ogm.defaults.teams;
import ogm.defaults.articles;
import ogm.articles.api;
import ogm.defaults.spaces;

alias suite = Spec!({
  URLRouter router;
  Json broadcastValue;

  describe("when there is a newsletter article", {
    Json publicNewsletter;

    beforeEach({
      setupTestData();

      auto calendarMock = new CalendarMock("2023-01-12T16:20:11Z");
      SysCalendar.instance = calendarMock;

      router = new URLRouter;

      crates.setupDefaultTeams;
      crates.setupDefaultSpaces(GeneralConfig());
      auto broadcast = new MemoryBroadcast;

      crates.setupDefaultArticles;

      router.crateSetup.setupArticleApi(crates, broadcast);

      auto article = crates.article.getItem("000000000000000000000001").exec.front.clone;
      article["visibility"]["isPublic"] = true;
      article["visibility"]["team"] = "000000000000000000000001";
      article["status"] = "draft";
      article["type"] = "newsletter-article";
      article["relatedId"] = "00000000000000000000000a";

      crates.article.updateItem(article);

      auto picture = crates.picture.addItem(`{
        "name": "test",
        "picture": "http://localhost:9091/pictures/000000000000000000000004/picture"
      }`.parseJsonString);

      auto space = crates.space.get.where("visibility.isDefault").equal(true).and.exec.front;
      space["logo"] = picture["_id"];

      crates.space.updateItem(space);
    });

    describe("the logo operation", {
      it("can get the space logo", {
        router
          .request
          .get("/articles/000000000000000000000001/logo")
          .expectStatusCode(200)
          .end;
      });

      it("adds an articleView metric when only the article id is in the url", {
        router
          .request
          .get("/articles/000000000000000000000001/logo")
          .expectStatusCode(200)
          .end((Response response) => () {
            auto metrics = crates.metric.get.and.exec.array;

            metrics.length.should.equal(1);
            metrics[0].should.equal(`{
              "_id": "000000000000000000000001",
              "labels": {
                "relatedId": "00000000000000000000000a"
              },
              "name": "000000000000000000000001",
              "reporter": "",
              "time": "2023-01-12T16:20:11Z",
              "type": "articleView",
              "value": 1
            }`.parseJsonString);
          });
      });

      it("adds an articleView metric when the article id and email id are in the url", {
        crates.newsletterEmail.addItem(`{
          "email": "email1@giscollective.com",
          "list": [{
            "newsletter": "000000000000000000000004",
            "joinedOn": "2022-01-12T16:20:11Z",
          }]
        }`.parseJsonString);

        crates.newsletterEmail.addItem(`{
          "_id": "000000000000000000000001",
          "email": "email2@giscollective.com",
          "list": [{
            "newsletter": "000000000000000000000004",
            "joinedOn": "2022-01-12T16:20:11Z",
          }]
        }`.parseJsonString);

        router
          .request
          .get("/articles/000000000000000000000001-000000000000000000000002/logo")
          .expectStatusCode(200)
          .end((Response response) => () {
            auto metrics = crates.metric.get.and.exec.array;

            metrics.length.should.equal(1);
            metrics[0].should.equal(`{
              "_id": "000000000000000000000001",
              "labels": {
                "relatedId": "00000000000000000000000a",
                "email": "email2@giscollective.com"
              },
              "name": "000000000000000000000001",
              "reporter": "",
              "time": "2023-01-12T16:20:11Z",
              "type": "articleView",
              "value": 1
            }`.parseJsonString);
          });
      });
    });
  });
});
