/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.articles.post;

import tests.fixtures;
import vibe.http.common;
import ogm.defaults.teams;
import ogm.defaults.articles;
import ogm.articles.api;

alias suite = Spec!({
  URLRouter router;

  describe("Creating articles", {
    Json article;

    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupArticleApi(crates, new MemoryBroadcast);
      crates.setupDefaultTeams;
      crates.setupDefaultArticles;
      article = `{ "article": {
        "title": "test",
        "slug": "test",
        "content": {},
        "visibility": {
          "team":"000000000000000000000001",
          "isPublic": true,
          "isDefault": false
        }
      }}`.parseJsonString;

      auto team = crates.team.getItem("000000000000000000000001").and.exec.front;
      team["isPublisher"] = true;
      crates.team.updateItem(team);
    });

    it("does not allow creating a new notification", {
      auto initialSize = crates.article.get.size;
      article["article"]["slug"] = "notification-new";

      router
        .request
        .post("/articles")
        .send(article)
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(400)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"errors": [{
            "description": "You can't create new notifications",
            "status": 400,
            "title": "Validation error"
          }]}`.parseJsonString);
          crates.article.get.size.should.equal(initialSize);
        });
    });

    it("does not allow creating an article with an existing slug in the same team", {
      crates.article.addItem(article["article"]);

      auto initialSize = crates.article.get.size;

      article["article"]["slug"] = "test";

      router
        .request
        .post("/articles")
        .send(article)
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(400)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"errors": [{
            "description": "Another article with the same slug, already exists.",
            "status": 400,
            "title": "Validation error"
          }]}`.parseJsonString);
          crates.article.get.size.should.equal(initialSize);
        });
    });

    it("allows creating an article with an existing slug on another team", {
      crates.article.addItem(article["article"]);
      auto initialSize = crates.article.get.size;

      article["article"]["slug"] = "test";
      article["article"]["visibility"]["team"] = "000000000000000000000002";

      router
        .request
        .post("/articles")
        .send(article)
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(200)
        .end((Response response) => () {
          crates.article.get.size.should.equal(initialSize + 1);
        });
    });

    describeCredentialsRule("create articles for publisher teams", "yes", "articles", ["administrator", "owner"], (string type) {
      auto initialSize = crates.article.get.size;

      router
        .request
        .post("/articles")
        .send(article)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          crates.article.get.size.should.equal(initialSize + 1);
        });
    });

    describeCredentialsRule("create articles for publisher teams", "no", "articles", ["leader", "member", "guest"], (string type) {
      auto initialSize = crates.article.get.size;

      router
        .request
        .post("/articles")
        .send(article)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "errors": [{
            "description": "You don't have enough rights to add an item.",
            "status": 403,
            "title": "Forbidden"
          }]}`.parseJsonString);
        });
    });

    describeCredentialsRule("create articles for publisher teams", "-", "articles", "no rights", {
      router
        .request
        .post("/articles")
        .send(article)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
        });
    });

    describe("releaseDate", {
      it("sets an article to private when it has a release date in the future", {
        auto initialSize = crates.article.get.size;

        article["article"]["releaseDate"] = "2050-03-02T00:00:00Z";
        article["article"]["visibility"]["isPublic"] = true;

        router
          .request
          .post("/articles")
          .send(article)
          .header("Authorization", "Bearer " ~ userTokenList["owner"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["article"]["visibility"]["isPublic"].should.equal(false);
          });
      });

      it("sets an article to public when it has a release date in the past", {
        auto initialSize = crates.article.get.size;

        article["article"]["releaseDate"] = "2000-03-02T00:00:00Z";
        article["article"]["visibility"]["isPublic"] = true;

        router
          .request
          .post("/articles")
          .send(article)
          .header("Authorization", "Bearer " ~ userTokenList["owner"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["article"]["visibility"]["isPublic"].should.equal(true);
          });
      });
    });

    it("can create an article without a slug", {
      auto initialSize = crates.article.get.size;

      article["article"]["slug"] = "";

      router
        .request
        .post("/articles")
        .send(article)
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .expectStatusCode(200)
        .end((Response response) => () {
          crates.article.get.size.should.equal(initialSize + 1);
        });
    });

    it("removes white spaces from categories", {
      auto initialSize = crates.article.get.size;

      article["article"]["categories"] = ["some-category ", " team-category", " "].serializeToJson;

      router
        .request
        .post("/articles")
        .send(article)
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["article"]["categories"].should.equal(["some-category", "team-category"].serializeToJson);
        });
    });

    it("can create two articles without a slug", {
      auto initialSize = crates.article.get.size;

      article["article"]["slug"] = "";

      router
        .request
        .post("/articles")
        .send(article)
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .expectStatusCode(200)
        .end;

      router
        .request
        .post("/articles")
        .send(article)
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .expectStatusCode(200)
        .end((Response response) => () {
          crates.article.get.size.should.equal(initialSize + 2);
        });
    });
  });
});
