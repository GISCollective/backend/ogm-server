module tests.articles.operations;

import tests.fixtures;
import vibe.http.common;
import std.datetime;
import ogm.calendar;
import ogm.defaults.teams;
import ogm.defaults.articles;
import ogm.articles.api;

alias suite = Spec!({
  URLRouter router;
  Json broadcastValue;

  describe("when there is a newsletter article", {
    Json publicNewsletter;
    Json privateNewsletter;

    MemoryBroadcast broadcast;

    beforeEach({
      setupTestData();

      auto calendarMock = new CalendarMock("2023-01-12T16:20:11Z");
      SysCalendar.instance = calendarMock;

      router = new URLRouter;

      broadcast = new MemoryBroadcast;

      crates.setupDefaultTeams;
      crates.setupDefaultArticles;

      router.crateSetup.setupArticleApi(crates, broadcast);

      auto article = crates.article.getItem("000000000000000000000001").exec.front.clone;
      article["visibility"]["isPublic"] = true;
      article["visibility"]["team"] = "000000000000000000000001";
      article["status"] = "draft";
      article["type"] = "newsletter-article";

      crates.article.updateItem(article);
    });

    describe("the publish operation", {
      beforeEach({
        broadcastValue = Json.emptyObject;
        void testHandler(const Json value) @trusted {
          broadcastValue = value;
        }
        broadcast.register("article.publish", &testHandler);
      });

      it("can trigger the job", {
        router
          .request
          .post("/articles/000000000000000000000001/publish")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(201)
          .end((Response response) => () {
            broadcastValue.should.equal(`{
              "id": "000000000000000000000001",
            }`.parseJsonString);
          });
      });

      it("sets the article to pending", {
        router
          .request
          .post("/articles/000000000000000000000001/publish")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(201)
          .end((Response response) => () {
            auto article = crates.article.getItem("000000000000000000000001").exec.front.clone;

            article["status"].to!string.should.equal("pending");
          });
      });

      it("returns an error when the article has the pending status", {
        auto article = crates.article.getItem("000000000000000000000001").exec.front.clone;
        article["status"] = "publishing";
        crates.article.updateItem(article);

        router
          .request
          .post("/articles/000000000000000000000001/publish")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "errors": [{
                "description": "Only draft and pending articles can be published.",
                "status": 400,
                "title": "Validation error"
              }]
            }`.parseJsonString);

            auto article = crates.article.getItem("000000000000000000000001").exec.front.clone;
            article["status"].to!string.should.equal("publishing");
          });
      });

      it("publishes a regular article when is not pending", {
        auto article = crates.article.getItem("000000000000000000000001").exec.front.clone;
        article["visibility"]["isPublic"] = false;
        article["type"] = "any";

        crates.article.updateItem(article);

        router
          .request
          .post("/articles/000000000000000000000001/publish")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(201)
          .end((Response response) => () {
            auto article = crates.article.getItem("000000000000000000000001").exec.front;

            article["visibility"].should.equal(`{
                "isDefault": false,
                "isPublic": true,
                "team": "000000000000000000000001"
            }`.parseJsonString);
          });
      });

      describeCredentialsRule("publish newsletter article", "yes", "articles", ["administrator", "owner"], (string type) {
        router
          .request
          .post("/articles/000000000000000000000001/publish")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(201)
          .end;
      });

      describeCredentialsRule("publish newsletter article", "no", "articles", ["leader", "member", "guest"], (string type) {
        router
          .request
          .post("/articles/000000000000000000000001/publish")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "errors": [{
                "description": "You can't publish this article.",
                "status": 403,
                "title": "Forbidden"
              }]
            }`.parseJsonString);
          });
      });

      describeCredentialsRule("publish newsletter article", "no", "articles", "no rights", {
        router
          .request
          .post("/articles/000000000000000000000001/publish")
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "errors": [{
                "description": "You can't publish this article.",
                "status": 403,
                "title": "Forbidden"
              }]
            }`.parseJsonString);
          });
      });
    });

    describe("the test operation", {
      beforeEach({
        broadcastValue = Json.emptyObject;
        void testHandler(const Json value) @trusted {
          broadcastValue = value;
        }
        broadcast.register("article.test", &testHandler);
      });

      it("can trigger the job", {
        router
          .request
          .post("/articles/000000000000000000000001/test")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(201)
          .end((Response response) => () {
            broadcastValue.should.equal(`{
              "id": "000000000000000000000001",
              "userId": "000000000000000000000005"
            }`.parseJsonString);
          });
      });

      describeCredentialsRule("test newsletter article", "yes", "articles", ["administrator", "owner"], (string type) {
        router
          .request
          .post("/articles/000000000000000000000001/test")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(201)
          .end;
      });

      describeCredentialsRule("test newsletter article", "no", "articles", ["leader", "member", "guest"], (string type) {
        router
          .request
          .post("/articles/000000000000000000000001/test")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "errors": [{
                "description": "You can't test this article.",
                "status": 403,
                "title": "Forbidden"
              }]
            }`.parseJsonString);
          });
      });

      describeCredentialsRule("test newsletter article", "no", "articles", "no rights", {
        router
          .request
          .post("/articles/000000000000000000000001/test")
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "errors": [{
                "description": "You can't test this article.",
                "status": 403,
                "title": "Forbidden"
              }]
            }`.parseJsonString);
          });
      });
    });
  });

  describe("the unpublish operation", {
    Json publicNewsletter;
    Json privateNewsletter;

    MemoryBroadcast broadcast;

    beforeEach({
      setupTestData();

      auto calendarMock = new CalendarMock("2023-01-12T16:20:11Z");
      SysCalendar.instance = calendarMock;

      router = new URLRouter;

      crates.setupDefaultTeams;
      broadcast = new MemoryBroadcast;

      broadcastValue = Json.emptyObject;
      void testHandler(const Json value) @trusted {
        broadcastValue = value;
      }

      broadcast.register("article.publish", &testHandler);

      crates.setupDefaultTeams;
      crates.setupDefaultArticles;

      router.crateSetup.setupArticleApi(crates, broadcast);

      auto article = crates.article.getItem("000000000000000000000001").exec.front.clone;
      article["visibility"]["isPublic"] = true;
      article["visibility"]["team"] = "000000000000000000000001";
      article["status"] = "pending";
      article["type"] = "newsletter-article";

      crates.article.updateItem(article);
    });

    it("sets the article to draft", {
      router
        .request
        .post("/articles/000000000000000000000001/unpublish")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(201)
        .end((Response response) => () {
          auto article = crates.article.getItem("000000000000000000000001").exec.front.clone;

          article["status"].to!string.should.equal("draft");
        });
    });

    it("returns an error when the article has the pending status", {
      auto article = crates.article.getItem("000000000000000000000001").exec.front.clone;
      article["status"] = "draft";
      crates.article.updateItem(article);

      router
        .request
        .post("/articles/000000000000000000000001/unpublish")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(400)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "errors": [{
              "description": "Only pending or publishing articles can be unpublished.",
              "status": 400,
              "title": "Validation error"
            }]
          }`.parseJsonString);

          auto article = crates.article.getItem("000000000000000000000001").exec.front.clone;
          article["status"].to!string.should.equal("draft");
        });
    });

    it("returns an error when the article is not a newsletter-article", {
      auto article = crates.article.getItem("000000000000000000000001").exec.front.clone;
      article["type"] = "newsletter-welcome-message";
      crates.article.updateItem(article);

      router
        .request
        .post("/articles/000000000000000000000001/unpublish")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(400)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "errors": [{
              "description": "Only newsletter-articles can be unpublished.",
              "status": 400,
              "title": "Validation error"
            }]
          }`.parseJsonString);

          auto article = crates.article.getItem("000000000000000000000001").exec.front.clone;
          article["status"].to!string.should.equal("pending");
        });
    });

    it("unpublishes a regular article", {
      auto article = crates.article.getItem("000000000000000000000001").exec.front.clone;
      article["visibility"]["isPublic"] = true;
      article["type"] = "any";

      crates.article.updateItem(article);

      router
        .request
        .post("/articles/000000000000000000000001/unpublish")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(201)
        .end((Response response) => () {
           auto article = crates.article.getItem("000000000000000000000001").exec.front;

            article["visibility"].should.equal(`{
                "isDefault": false,
                "isPublic": false,
                "team": "000000000000000000000001"
            }`.parseJsonString);
        });
    });

    describeCredentialsRule("unpublish newsletter article", "yes", "articles", ["administrator", "owner"], (string type) {
      router
        .request
        .post("/articles/000000000000000000000001/unpublish")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(201)
        .end;
    });

    describeCredentialsRule("unpublish newsletter article", "no", "articles", ["leader", "member", "guest"], (string type) {
      router
        .request
        .post("/articles/000000000000000000000001/unpublish")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "errors": [{
              "description": "You can't unpublish this article.",
              "status": 403,
              "title": "Forbidden"
            }]
          }`.parseJsonString);
        });
    });

    describeCredentialsRule("unpublish newsletter article", "no", "articles", "no rights", {
      router
        .request
        .post("/articles/000000000000000000000001/unpublish")
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "errors": [{
              "description": "You can't unpublish this article.",
              "status": 403,
              "title": "Forbidden"
            }]
          }`.parseJsonString);
        });
    });
  });

  describe("categories", {
    beforeEach(() => {
      auto article = crates.article.getItem("000000000000000000000001").exec.front.clone;
      article["categories"] = Json.emptyArray;
      crates.article.updateItem(article);
    });

    it("returns an empty list when there are no categories", {
      router
        .request
        .get("/articles/_/categories")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "categories": [] }`.parseJsonString);
        });
    });

    it("returns a category when there is an article with a category", {
      auto article = crates.article.getItem("000000000000000000000001").exec.front.clone;
      article["categories"] = [`some category`].serializeToJson;
      crates.article.updateItem(article);

      router
        .request
        .get("/articles/_/categories")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "categories": ["some category"] }`.parseJsonString);
        });
    });

    it("returns one category when there are 2 articles with the same category", {
      auto article = crates.article.getItem("000000000000000000000001").exec.front.clone;
      article["categories"] = [`some category`].serializeToJson;
      crates.article.updateItem(article);

      article = crates.article.getItem("000000000000000000000002").exec.front.clone;
      article["categories"] = [`some category`].serializeToJson;
      crates.article.updateItem(article);

      router
        .request
        .get("/articles/_/categories")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "categories": ["some category"] }`.parseJsonString);
        });
    });

    it("ignores empty categories", {
      auto article = crates.article.getItem("000000000000000000000001").exec.front.clone;
      article["categories"] = [``].serializeToJson;
      article["visibility"] = `{"isPublic": false,"team":"000000000000000000000004","isDefault": true}`.parseJsonString;
      crates.article.updateItem(article);

      article = crates.article.getItem("000000000000000000000002").exec.front.clone;
      article["categories"] = [`other category`].serializeToJson;
      crates.article.updateItem(article);

      router
        .request
        .get("/articles/_/categories?team=000000000000000000000004")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "categories": [] }`.parseJsonString);
        });
    });

    it("can search by category", {
      auto article = crates.article.getItem("000000000000000000000001").exec.front.clone;
      article["categories"] = [`main`, `cat1`].serializeToJson;
      article["visibility"] = `{"isPublic": false,"team":"000000000000000000000004","isDefault": true}`.parseJsonString;
      crates.article.updateItem(article);

      article = crates.article.getItem("000000000000000000000002").exec.front.clone;
      article["categories"] = [`main`, `cat2`].serializeToJson;
      article["visibility"] = `{"isPublic": false,"team":"000000000000000000000004","isDefault": true}`.parseJsonString;
      crates.article.updateItem(article);

      router
        .request
        .get("/articles/_/categories?team=000000000000000000000004&category=main")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "categories": ["cat1", "cat2"] }`.parseJsonString);
        });
    });
  });

  describe("bulk delete", {
    MemoryBroadcast broadcast;

    beforeEach({
      router = new URLRouter;
      broadcast = new MemoryBroadcast;

      setupTestData();
      crates.setupDefaultTeams;
      crates.setupDefaultArticles;

      router.crateSetup.setupArticleApi(crates, broadcast);

      auto article = crates.article.getItem("000000000000000000000001").exec.front.clone;
      article["visibility"]["isPublic"] = true;
      article["visibility"]["team"] = "000000000000000000000001";
      article["status"] = "draft";
      article["type"] = "newsletter-article";

      crates.article.updateItem(article);
    });

    describeCredentialsRule("delete articles from own team", "yes", "articles", ["administrator", "owner"], (string type) {
      router
        .request
        .delete_("/articles?team=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end;

      crates.article.get.exec.map!(a => a["visibility"]["team"]).array.sort.uniq.should.equal(["000000000000000000000006"]);
    });

    describeCredentialsRule("delete articles from own team", "no", "articles", ["leader", "member", "guest"], (string type) {
      router
        .request
        .delete_("/articles?team=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end;

      crates.article.get.exec.map!(a => a["visibility"]["team"]).array.sort.uniq.should.equal(["000000000000000000000001", "000000000000000000000006"]);
    });

    describeCredentialsRule("delete articles from own team", "no", "articles", "no rights", {
      router
        .request
        .delete_("/articles?team=000000000000000000000001")
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "error": "Authorization required"
          }`.parseJsonString);
        });
    });


    describeCredentialsRule("delete own articles", "yes", "articles", ["administrator", "owner", "leader", "member"], (string type) {
      auto articles = crates.article.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

      foreach(article; articles) {
        article["info"]["originalAuthor"] = userId[type];
        crates.article.updateItem(article);
      }

      router
        .request
        .delete_("/articles?team=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end;

      crates.article.get.exec.map!(a => a["visibility"]["team"]).array.sort.uniq.should.equal(["000000000000000000000006"]);
    });

    describeCredentialsRule("delete own articles", "no", "articles", ["guest"], (string type) {
      auto articles = crates.article.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

      foreach(article; articles) {
        article["info"]["originalAuthor"] = userId[type];
        crates.article.updateItem(article);
      }

      router
        .request
        .delete_("/articles?team=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end;

      crates.article.get.exec.map!(a => a["visibility"]["team"]).array.sort.uniq.should.equal(["000000000000000000000001", "000000000000000000000006"]);
    });

    describeCredentialsRule("delete own articles", "no", "articles", "no rights", { });


    it("does not delete an article when it does not match the category", {
      auto articles = crates.article.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

      foreach(article; articles) {
        article["categories"] = ["some"].serializeToJson;
        crates.article.updateItem(article);
      }

      router
        .request
        .delete_("/articles?team=000000000000000000000001&category=other")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(204)
        .end;

      crates.article.get.exec.map!(a => a["visibility"]["team"]).array.sort.uniq.should.equal(["000000000000000000000001", "000000000000000000000006"]);
    });

    it("deletes an article when it matches the category", {
      auto articles = crates.article.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

      foreach(article; articles) {
        article["categories"] = ["some"].serializeToJson;
        crates.article.updateItem(article);
      }

      router
        .request
        .delete_("/articles?team=000000000000000000000001&category=some")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(204)
        .end;

      crates.article.get.exec.map!(a => a["visibility"]["team"]).array.sort.uniq.should.equal(["000000000000000000000006"]);
    });
  });

  describe("bulk publish", {
    MemoryBroadcast broadcast;

    beforeEach({
      router = new URLRouter;
      broadcast = new MemoryBroadcast;

      setupTestData();
      crates.setupDefaultTeams;
      crates.setupDefaultArticles;

      router.crateSetup.setupArticleApi(crates, broadcast);

      auto article = crates.article.getItem("000000000000000000000001").exec.front.clone;
      article["visibility"]["isPublic"] = false;
      article["visibility"]["team"] = "000000000000000000000001";

      crates.article.updateItem(article);
    });

    describeCredentialsRule("bulk publish articles from own team", "yes", "articles", ["administrator", "owner"], (string type) {
      router
        .request
        .post("/articles/publish?team=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end;

      auto article = crates.article.getItem("000000000000000000000001").exec.front.clone;

      article["visibility"]["isPublic"].to!bool.should.equal(true);
    });

    describeCredentialsRule("bulk publish articles from own team", "no", "articles", ["leader", "member", "guest"], (string type) {
      router
        .request
        .post("/articles/publish?team=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end;

      auto article = crates.article.getItem("000000000000000000000001").exec.front.clone;

      article["visibility"]["isPublic"].to!bool.should.equal(false);
    });

    describeCredentialsRule("bulk publish articles from own team", "no", "articles", "no rights", {
      router
        .request
        .post("/articles/publish?team=000000000000000000000001")
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "error": "Authorization required"
          }`.parseJsonString);
        });
    });


    describeCredentialsRule("bulk publish own articles", "yes", "articles", ["administrator", "owner", "leader", "member"], (string type) {
      auto articles = crates.article.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

      foreach(article; articles) {
        article["info"]["originalAuthor"] = userId[type];
        crates.article.updateItem(article);
      }

      router
        .request
        .post("/articles/publish?team=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end;

      auto article = crates.article.getItem("000000000000000000000001").exec.front.clone;
      article["visibility"]["isPublic"].to!bool.should.equal(true);
    });

    describeCredentialsRule("bulk publish own articles", "no", "articles", ["guest"], (string type) {
      auto articles = crates.article.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

      foreach(article; articles) {
        article["info"]["originalAuthor"] = userId[type];
        crates.article.updateItem(article);
      }

      router
        .request
        .post("/articles/publish?team=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end;

      auto article = crates.article.getItem("000000000000000000000001").exec.front.clone;
      article["visibility"]["isPublic"].to!bool.should.equal(false);
    });

    describeCredentialsRule("bulk publish own articles", "no", "articles", "no rights", { });


    describeCredentialsRule("bulk publish articles from other teams", "yes", "articles", ["administrator"], (string type) {
      auto articles = crates.article.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

      foreach(article; articles) {
        article["info"]["originalAuthor"] = userId[type];
        article["visibility"]["team"] = "000000000000000000000004";
        crates.article.updateItem(article);
      }

      router
        .request
        .post("/articles/publish?team=000000000000000000000004")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end;

      auto article = crates.article.getItem("000000000000000000000001").exec.front.clone;
      article["visibility"]["isPublic"].to!bool.should.equal(true);
    });

    describeCredentialsRule("bulk publish articles from other teams", "no", "articles", ["owner", "leader", "member", "guest"], (string type) {
      auto articles = crates.article.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

      foreach(article; articles) {
        article["info"]["originalAuthor"] = userId[type];
        article["visibility"]["team"] = "000000000000000000000004";
        crates.article.updateItem(article);
      }

      router
        .request
        .post("/articles/publish?team=000000000000000000000004")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(404)
        .end;

      auto article = crates.article.getItem("000000000000000000000001").exec.front.clone;
      article["visibility"]["isPublic"].to!bool.should.equal(false);
    });

    describeCredentialsRule("bulk publish articles from other teams", "no", "articles", "no rights", { });
  });

  describe("bulk unpublish", {
    MemoryBroadcast broadcast;

    beforeEach({
      router = new URLRouter;
      broadcast = new MemoryBroadcast;

      setupTestData();
      crates.setupDefaultTeams;
      crates.setupDefaultArticles;

      router.crateSetup.setupArticleApi(crates, broadcast);

      auto article = crates.article.getItem("000000000000000000000001").exec.front.clone;
      article["visibility"]["isPublic"] = true;
      article["visibility"]["team"] = "000000000000000000000001";

      crates.article.updateItem(article);
    });

    describeCredentialsRule("bulk unpublish articles from own team", "yes", "articles", ["administrator", "owner"], (string type) {
      router
        .request
        .post("/articles/unpublish?team=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end;

      auto article = crates.article.getItem("000000000000000000000001").exec.front.clone;

      article["visibility"]["isPublic"].to!bool.should.equal(false);
    });

    describeCredentialsRule("bulk unpublish articles from own team", "no", "articles", ["leader", "member", "guest"], (string type) {
      router
        .request
        .post("/articles/unpublish?team=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end;

      auto article = crates.article.getItem("000000000000000000000001").exec.front.clone;

      article["visibility"]["isPublic"].to!bool.should.equal(true);
    });

    describeCredentialsRule("bulk unpublish articles from own team", "no", "articles", "no rights", {
      router
        .request
        .post("/articles/unpublish?team=000000000000000000000001")
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "error": "Authorization required"
          }`.parseJsonString);
        });
    });


    describeCredentialsRule("bulk unpublish own articles", "yes", "articles", ["administrator", "owner", "leader", "member"], (string type) {
      auto articles = crates.article.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

      foreach(article; articles) {
        article["info"]["originalAuthor"] = userId[type];
        crates.article.updateItem(article);
      }

      router
        .request
        .post("/articles/unpublish?team=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end;

      auto article = crates.article.getItem("000000000000000000000001").exec.front.clone;
      article["visibility"]["isPublic"].to!bool.should.equal(false);
    });

    describeCredentialsRule("bulk unpublish own articles", "no", "articles", ["guest"], (string type) {
      auto articles = crates.article.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

      foreach(article; articles) {
        article["info"]["originalAuthor"] = userId[type];
        crates.article.updateItem(article);
      }

      router
        .request
        .post("/articles/unpublish?team=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end;

      auto article = crates.article.getItem("000000000000000000000001").exec.front.clone;
      article["visibility"]["isPublic"].to!bool.should.equal(true);
    });

    describeCredentialsRule("bulk unpublish own articles", "no", "articles", "no rights", { });


    describeCredentialsRule("bulk unpublish articles from other teams", "yes", "articles", ["administrator"], (string type) {
      auto articles = crates.article.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

      foreach(article; articles) {
        article["info"]["originalAuthor"] = userId[type];
        article["visibility"]["team"] = "000000000000000000000004";
        crates.article.updateItem(article);
      }

      router
        .request
        .post("/articles/unpublish?team=000000000000000000000004")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end;

      auto article = crates.article.getItem("000000000000000000000001").exec.front.clone;
      article["visibility"]["isPublic"].to!bool.should.equal(false);
    });

    describeCredentialsRule("bulk unpublish articles from other teams", "no", "articles", ["owner", "leader", "member", "guest"], (string type) {
      auto articles = crates.article.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

      foreach(article; articles) {
        article["info"]["originalAuthor"] = userId[type];
        article["visibility"]["team"] = "000000000000000000000004";
        crates.article.updateItem(article);
      }

      router
        .request
        .post("/articles/unpublish?team=000000000000000000000004")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(404)
        .end;

      auto article = crates.article.getItem("000000000000000000000001").exec.front.clone;
      article["visibility"]["isPublic"].to!bool.should.equal(true);
    });

    describeCredentialsRule("bulk unpublish articles from other teams", "no", "articles", "no rights", { });
  });
});
