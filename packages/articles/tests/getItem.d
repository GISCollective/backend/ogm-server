/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.articles.getitem;

import tests.fixtures;
import vibe.http.common;
import ogm.defaults.teams;
import ogm.defaults.articles;
import ogm.articles.api;

alias suite = Spec!({
  URLRouter router;

  describe("Getting articles by id", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupArticleApi(crates, new MemoryBroadcast);
      crates.setupDefaultTeams;
      crates.setupDefaultArticles;

      auto article = crates.article.getItem("000000000000000000000001").exec.front.clone;
      article["visibility"]["isPublic"] = true;
      article["visibility"]["team"] = "000000000000000000000001";
      article["slug"] = "about--test";

      crates.article.updateItem(article);
    });

    it("can get only the article visibility", {
      router
        .request
        .get("/articles/000000000000000000000001?onlyVisibility=true")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["article"].should.equal(`{
            "_id": "000000000000000000000001",
            "visibility": {
              "isDefault": false,
              "isPublic": true,
              "team": "000000000000000000000001"
            },
            "pictures": [],
            "canEdit": true
          }`.parseJsonString);
        });
    });


    describeCredentialsRule("get public article by id", "yes", "articles", ["administrator", "owner"], (string type) {
      router
        .request
        .get("/articles/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["article"]["_id"].should.equal("000000000000000000000001");
          response.bodyJson["article"]["canEdit"].to!bool.should.equal(true);
        });
    });

    describeCredentialsRule("get public article by id", "yes", "articles", ["leader", "member", "guest"], (string type) {
      router
        .request
        .get("/articles/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["article"]["_id"].should.equal("000000000000000000000001");
          response.bodyJson["article"]["canEdit"].to!bool.should.equal(false);
        });
    });

    describeCredentialsRule("get public article by id", "yes", "articles", "no rights", {
      router
        .request
        .get("/articles/000000000000000000000001")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["article"]["_id"].should.equal("000000000000000000000001");
          response.bodyJson["article"]["canEdit"].to!bool.should.equal(false);
        });
    });

    describeCredentialsRule("get public article by slug", "yes", "articles", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/articles/about--test?team=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["article"]["_id"].should.equal("000000000000000000000001");
        });
    });

    describeCredentialsRule("get public article by slug", "yes", "articles", "no rights", {
      router
        .request
        .get("/articles/about--test?team=000000000000000000000001")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["article"]["_id"].should.equal("000000000000000000000001");
        });
    });


    describe("when there is a private article", {
      beforeEach({
        auto article = crates.article.getItem("000000000000000000000001").and.exec.front;
        article["visibility"]["isPublic"] = false;
        article["visibility"]["team"] = "000000000000000000000001";

        crates.article.updateItem(article);
      });

      describeCredentialsRule("get private articles", "yes", "articles", ["administrator", "owner", "leader", "member", "guest"], (string type) {
        router
          .request
          .get("/articles/000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["article"]["_id"].should.equal("000000000000000000000001");
          });
      });

      describeCredentialsRule("get private articles", "no", "articles", "no rights", {
        router
          .request
          .get("/articles/000000000000000000000001")
          .expectStatusCode(404);
      });
    });
  });
});
