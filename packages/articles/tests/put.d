/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.articles.put;

import tests.fixtures;
import vibe.http.common;
import ogm.defaults.teams;
import ogm.defaults.articles;
import ogm.articles.api;
import ogm.calendar;

alias suite = Spec!({
  URLRouter router;

  describe("Updating articles", {
    Json article;

    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupArticleApi(crates, new MemoryBroadcast);
      crates.setupDefaultTeams;
      crates.setupDefaultArticles;
      article = Json.emptyObject;
      article["article"] = crates.article.getItem("000000000000000000000001").exec.front.clone;
      article["article"]["slug"] = "";
      article["article"]["title"] = "test";
      article["article"]["content"] = "";

      auto storedArticle = crates.article.getItem("000000000000000000000001").exec.front;
      storedArticle["visibility"]["isPublic"] = true;
      storedArticle["visibility"]["team"] = "000000000000000000000001";
      crates.article.updateItem(storedArticle);
    });

    describeCredentialsRule("update team articles", "yes", "articles", ["administrator", "owner"], (string type) {
      auto initialSize = crates.page.get.size;

      router
        .request
        .put("/articles/000000000000000000000001")
        .send(article)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto article = crates.article.getItem("000000000000000000000001").exec.front;
          article["canEdit"] = true;
          response.bodyJson["article"].should.equal(article);
        });
    });

    describeCredentialsRule("update team articles", "no", "articles", [ "leader", "member", "guest"], (string type) {
      auto initialSize = crates.page.get.size;

      router
        .request
        .put("/articles/000000000000000000000001")
        .send(article)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{ "errors": [{
            "description": "You don't have enough rights to edit ` ~ "`000000000000000000000001`" ~ `.",
            "status": 403,
            "title": "Forbidden"
          }]}`).parseJsonString);
        });
    });

    describeCredentialsRule("update team articles", "-", "articles", "no rights", {
      router
        .request
        .put("/articles/000000000000000000000001")
        .send(article)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
        });
    });


    describeCredentialsRule("update own articles", "yes", "articles", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      auto editableArticle = crates.article.getItem("000000000000000000000001").and.exec.front;
      editableArticle["info"]["originalAuthor"] = userId[type];
      crates.article.updateItem(editableArticle);

      router
        .request
        .put("/articles/000000000000000000000001")
        .send(article)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["article"]["title"].to!string.should.equal("test");
        });
    });

    describeCredentialsRule("update own articles", "-", "articles", "no rights", {});


    describe("with an admin token", {
      it("adds the visibility field when it is not set", {
        crates.setupDefaultTeams;
        auto article = crates.article.addItem(`{
          "_id": "10000000000000000000000",
          "slug": "test",
          "title": "title",
          "content": "content",
          "visibility": {
            "team": "00000000000000000000001",
            "isPublic": false,
            "isDefault": false
          },
          "info": {
            "changeIndex": 0,
            "author": "@unknown",
            "createdOn": "2015-01-01T00:00:00Z",
            "lastChangeOn": "2015-01-01T00:00:00Z",
          }
        }`.parseJsonString);

        Json data = Json.emptyObject;
        data["article"] = article.clone;
        data["article"]["visibility"] = null;

        router
          .request
          .put("/articles/" ~ article["_id"].to!string)
          .send(data)
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            auto article = crates.article.getItem(article["_id"].to!string).exec.front;
            article["canEdit"] = true;
            response.bodyJson["article"].should.equal(article);
          });
      });

      describe("releaseDate", {
        Json article;
        Json data;

        beforeEach({
          crates.setupDefaultTeams;
          article = crates.article.addItem(`{
            "slug": "test",
            "title": "title",
            "content": "content",
            "info": {
              "changeIndex": 0,
              "author": "@unknown",
              "createdOn": "2015-01-01T00:00:00Z",
              "lastChangeOn": "2015-01-01T00:00:00Z",
            },
            "visibility": {
              "team":"000000000000000000000001",
              "isPublic": true,
              "isDefault": false
            }
          }`.parseJsonString);

          data = Json.emptyObject;
          data["article"] = article.clone;

          auto calendarMock = new CalendarMock("2021-01-12T16:20:11Z");
          SysCalendar.instance = calendarMock;
        });

        it("sets an article to private when it has a release date in the future", {
          auto initialSize = crates.article.get.size;

          data["article"]["releaseDate"] = "2050-03-02T00:00:00Z";
          data["article"]["visibility"]["isPublic"] = true;

          router
            .request
            .put("/articles/" ~ article["_id"].to!string)
            .send(data)
            .header("Authorization", "Bearer " ~ userTokenList["owner"])
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["article"]["visibility"]["isPublic"].should.equal(false);
            });
        });

        it("sets an article to public when it has a release date in the past", {
          auto initialSize = crates.article.get.size;

          data["article"]["releaseDate"] = "2000-03-02T00:00:00Z";
          data["article"]["visibility"]["isPublic"] = true;

          router
            .request
            .put("/articles/" ~ article["_id"].to!string)
            .send(data)
            .header("Authorization", "Bearer " ~ userTokenList["owner"])
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["article"]["visibility"]["isPublic"].should.equal(true);
            });
        });

        it("updates the release date when the article is published", {
          auto initialSize = crates.article.get.size;
          article["visibility"]["isPublic"] = false;

          crates.article.updateItem(article);

          data["article"]["releaseDate"] = "2050-03-02T00:00:00Z";
          data["article"]["visibility"]["isPublic"] = true;

          router
            .request
            .put("/articles/" ~ article["_id"].to!string)
            .send(data)
            .header("Authorization", "Bearer " ~ userTokenList["owner"])
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["article"]["visibility"]["isPublic"].should.equal(true);
              response.bodyJson["article"]["releaseDate"].should.equal("2021-01-12T16:20:11Z");
            });
        });

        it("sets the release date in 10 years when an article is unpublished", {
          auto initialSize = crates.article.get.size;
          article["visibility"]["isPublic"] = true;

          crates.article.updateItem(article);

          data["article"]["releaseDate"] = "2050-03-02T00:00:00Z";
          data["article"]["visibility"]["isPublic"] = false;

          router
            .request
            .put("/articles/" ~ article["_id"].to!string)
            .send(data)
            .header("Authorization", "Bearer " ~ userTokenList["owner"])
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["article"]["visibility"]["isPublic"].should.equal(false);
              response.bodyJson["article"]["releaseDate"].should.equal("2031-01-10T16:20:11Z");
            });
        });
      });
    });
  });
});
