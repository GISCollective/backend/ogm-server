/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.articles.getlist;

import tests.fixtures;
import vibe.http.common;
import ogm.defaults.teams;
import ogm.defaults.articles;
import ogm.articles.api;

alias suite = Spec!({
  URLRouter router;

  describe("Get article list", {
    before({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupArticleApi(crates, new MemoryBroadcast);
      crates.setupDefaultTeams;
      crates.setupDefaultArticles(true);

      auto article = crates.article.getItem("000000000000000000000001").and.exec.front;
      article["visibility"]["isPublic"] = true;

      crates.article.updateItem(article);
    });


    describeCredentialsRule("get the public article list", "yes", "articles", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/articles")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["articles"].byValue.map!(a => a["_id"].to!string).should.contain("000000000000000000000001");
        });
    });

    describeCredentialsRule("get the public article list", "yes", "articles", "no rights", {
      router
        .request
        .get("/articles")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["articles"].byValue.map!(a => a["_id"].to!string).should.contain("000000000000000000000001");
        });
    });

    describe("when there is a private article", {
      before({
        auto article = crates.article.getItem("000000000000000000000001").and.exec.front;
        article["visibility"]["isPublic"] = false;
        article["visibility"]["team"] = "000000000000000000000001";

        crates.article.updateItem(article);
      });

      describeCredentialsRule("get the private article list", "yes", "articles", ["administrator", "owner", "leader", "member", "guest"], (string type) {
        auto initialSize = crates.article.get.size;

        router
          .request
          .get("/articles")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["articles"].byValue.map!(a => a["_id"].to!string).should.contain("000000000000000000000001");
          });
      });

      describeCredentialsRule("get the private article list", "no", "articles", "no rights", {
        router
          .request
          .get("/articles")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["articles"].byValue.map!(a => a["_id"].to!string).should.not.contain("000000000000000000000001");
          });
      });
    });

    describe("the category query param", {
      before({
        auto article = crates.article.getItem("000000000000000000000001").and.exec.front;
        article["categories"] = ["some-category", "team-category"].serializeToJson;

        crates.article.updateItem(article);
      });

      it("returns an article when it matches the category", {
        router
          .request
          .get("/articles?category=some-category")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["articles"].byValue.map!(a => a["_id"].to!string).should.equal(["000000000000000000000001"]);
          });
      });

      it("returns an article when it matches 2 categories", {
        router
          .request
          .get("/articles?category=some-category,team-category")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["articles"].byValue.map!(a => a["_id"].to!string).should.equal(["000000000000000000000001"]);
          });
      });

      it("returns no article when it does not match a category", {
        router
          .request
          .get("/articles?category=other-category")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["articles"].length.should.equal(0);
          });
      });
    });
  });
});
