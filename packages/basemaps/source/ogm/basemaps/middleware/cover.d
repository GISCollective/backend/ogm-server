/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.basemaps.middleware.cover;

import ogm.crates.all;
import ogm.http.request;

import vibe.http.router;
import vibe.data.json;

import crate.base;

class CoverMiddleware {
  private {
    OgmCrates crates;
  }

  ///
  this(OgmCrates crates) {
    this.crates = crates;
  }

  /// It denies adding/editing/removing maps if the items are not owned
  @get
  IQuery get(IQuery selector, HTTPServerRequest req) {
    selector.sort("defaultOrder", 1);

    return selector;
  }

  ///
  @replace @patch
  void replace(HTTPServerRequest req, HTTPServerResponse) {
    if("cover" !in req.json["baseMap"]) {
      return;
    }

    string newCover = req.json["baseMap"]["cover"].to!string;

    auto baseMap = crates.baseMap.getItem(req.params["id"]).exec.front;
    string coverId = baseMap["cover"].to!string;

    if(coverId != newCover) {
      crates.picture.removePicture(coverId);
    }
  }

  ///
  @delete_
  void delete_(HTTPServerRequest req, HTTPServerResponse res) {
    auto baseMap = crates.baseMap.getItem(req.params["id"]).exec.front;
    auto coverId = baseMap["cover"].to!string;

    crates.picture.removePicture(coverId);
  }

  @mapper
  Json mapper(const Json item) @safe {
    Json result = item;

    if("cover" in item && item["cover"] == "") {
      result.remove("cover");
    }

    return result;
  }
}
