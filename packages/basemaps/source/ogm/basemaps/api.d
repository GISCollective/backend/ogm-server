/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.basemaps.api;

import vibe.http.router;
import vibe.data.json;
import crate.http.router;
import crate.base;

import ogm.auth;
import ogm.crates.all;

import ogm.models.basemap;
import ogm.models.attribution;

import ogm.middleware.adminrequest;
import ogm.middleware.userdata;
import ogm.middleware.sort;
import ogm.basemaps.middleware.cover;

import ogm.filter.pagination;
import ogm.filter.searchterm;
import ogm.filter.visibility;
import ogm.middleware.GlobalAccess;

import ogm.crates.defaults;

///
void setupBaseMapApi(T)(CrateRouter!T crateRouter, OgmCrates crates) {
  auto auth = Authentication.instance(crates.user);
  auto adminRequest = new AdminRequest(crates.user);

  auto sort = new SortMiddleware(crates, "defaultOrder", 1);
  auto cover = new CoverMiddleware(crates);
  auto userDataMiddleware = UserDataMiddleware.instance(crates);
  auto searchTermFilter = new SearchTermFilter;
  auto paginationFilter = new PaginationFilter;
  auto visibilityFilter = new VisibilityFilter!"baseMaps"(crates, crates.baseMap);
  auto globalAccess = GlobalAccessMiddleware.instance(crates);

  crateGetters["Team"] = &crates.team.getItem;
  crateGetters["Picture"] = &crates.picture.getItem;

  crateRouter
    .add(crates.baseMap,
      auth.publicDataMiddleware,
      adminRequest,
      userDataMiddleware,
      globalAccess,
      visibilityFilter,
      cover,
      sort,
      searchTermFilter);
}
