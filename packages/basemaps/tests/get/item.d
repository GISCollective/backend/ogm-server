/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.basemaps.get.item;

import tests.fixtures;
import vibe.http.common;
import ogm.crates.defaults;
import ogm.defaults.baseMaps;
import ogm.basemaps.api;

alias suite = Spec!({
  URLRouter router;

  describe("Get a default base map", {
    beforeEach({
      setupTestData();
      router = new URLRouter;

      crates.picture.addDefault("image/jpeg", "cover", "./deploy/cover.jpg");
      crates.addDefaultTeam("General");
      router.crateSetup.setupBaseMapApi(crates);
      setupDefaultBaseMaps(crates);

      auto newBaseMap = `{ "name": "test 1", "icon": "icon", "layers": [], "visibility": { "isDefault": false, "isPublic": true } }`.parseJsonString;
      crates.baseMap.addItem(newBaseMap);
    });

    describe("without a token", {
      it("should return the base map", {
        router
          .request
          .get("/basemaps/000000000000000000000004")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{ "baseMap":
              { "_id": "000000000000000000000004", "name": "test 1", "icon": "icon", "layers": [], "visibility": { "isDefault": false, "isPublic": true }}
            }`.parseJsonString);
          });
      });
    });
  });

  describe("When there is an item with `isDefault` false", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      crates.picture.addDefault("image/jpeg", "cover", "./deploy/cover.jpg");
      crates.addDefaultTeam("General");
      router.crateSetup.setupBaseMapApi(crates);
      setupDefaultBaseMaps(crates);

      auto newBaseMap = `{ "name": "test 1", "icon": "icon", "layers": [], "visibility": { "isDefault": false, "isPublic": true }}`.parseJsonString;
      crates.baseMap.addItem(newBaseMap);
    });

    describe("without a token", {
      it("should return the base map", {
        router
          .request
          .get("/basemaps/000000000000000000000004")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{ "baseMap":
              { "icon": "icon", "layers": [], "visibility": { "isDefault": false, "isPublic": true }, "_id": "000000000000000000000004",
                "name": "test 1",  }
            }`.parseJsonString);
          });
      });
    });
  });
});
