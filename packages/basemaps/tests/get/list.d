/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.basemaps.get.list;

import tests.fixtures;
import vibe.http.common;
import ogm.crates.defaults;
import std.algorithm;
import ogm.defaults.baseMaps;
import ogm.basemaps.api;

alias suite = Spec!({
  URLRouter router;

  describe("Get public base maps", {
    beforeEach({
      setupTestData();
      router = new URLRouter;

      crates.picture.addDefault("image/jpeg", "cover", "./deploy/cover.jpg");
      crates.addDefaultTeam("General");

      router.crateSetup.setupBaseMapApi(crates);
      setupDefaultBaseMaps(crates);

      auto newBaseMap = `{ "name": "test 3", "icon": "icon", "layers": [], "defaultOrder": 3, "visibility": { "isDefault": true, "isPublic": true }}`.parseJsonString;
      crates.baseMap.addItem(newBaseMap);

      newBaseMap = `{ "name": "test 0", "icon": "icon", "layers": [], "defaultOrder": 0, "visibility": { "isDefault": true, "isPublic": true }}`.parseJsonString;
      crates.baseMap.addItem(newBaseMap);

      newBaseMap = `{ "name": "test 2", "icon": "icon", "layers": [], "defaultOrder": 2, "visibility": { "isDefault": true, "isPublic": true }}`.parseJsonString;
      crates.baseMap.addItem(newBaseMap);
    });

    describeCredentialsRule("get the list of default base maps", "yes", "base maps", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/basemaps?default=true")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {

          response.bodyJson["baseMaps"].byValue
            .map!(a => a["_id"].to!string)
            .array
            .should.equal([ "000000000000000000000001", "000000000000000000000005", "000000000000000000000002",
              "000000000000000000000003", "000000000000000000000006", "000000000000000000000004" ]);
        });
    });

    describeCredentialsRule("get the list of default base maps", "yes", "base maps", "no rights", {
      router
        .request
        .get("/basemaps?default=true")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["baseMaps"].byValue
            .map!(a => a["_id"].to!string)
            .array
            .should.equal([ "000000000000000000000001", "000000000000000000000005", "000000000000000000000002",
              "000000000000000000000003", "000000000000000000000006", "000000000000000000000004" ]);
        });
    });

    describeCredentialsRule("search base maps by name", "yes", "base maps", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/basemaps?term=test")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {

          response.bodyJson["baseMaps"].byValue
            .map!(a => a["_id"].to!string)
            .array
            .should.contain([ "000000000000000000000004", "000000000000000000000005", "000000000000000000000006" ]);
        });
    });

    describeCredentialsRule("search base maps by name", "yes", "base maps", "no rights", {
      router
        .request
        .get("/basemaps?term=test")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["baseMaps"].byValue
            .map!(a => a["_id"].to!string)
            .array
            .should.contain([ "000000000000000000000004", "000000000000000000000005", "000000000000000000000006" ]);
        });
    });
  });

  describe("Get private base maps", {
    beforeEach({
      setupTestData();
      router = new URLRouter;

      crates.picture.addDefault("image/jpeg", "cover", "./deploy/cover.jpg");
      crates.addDefaultTeam("General");

      router.crateSetup.setupBaseMapApi(crates);
      setupDefaultBaseMaps(crates);

      auto newBaseMap = `{ "name": "test 3", "icon": "icon", "layers": [], "defaultOrder": 3, "visibility": { "isDefault": true, "isPublic": false, "team": "000000000000000000000001" }}`.parseJsonString;
      crates.baseMap.addItem(newBaseMap);

      newBaseMap = `{ "name": "test 0", "icon": "icon", "layers": [], "defaultOrder": 0, "visibility": { "isDefault": true, "isPublic": false, "team": "000000000000000000000002" }}`.parseJsonString;
      crates.baseMap.addItem(newBaseMap);

      newBaseMap = `{ "name": "test 2", "icon": "icon", "layers": [], "defaultOrder": 2, "visibility": { "isDefault": true, "isPublic": false, "team": "000000000000000000000003" }}`.parseJsonString;
      crates.baseMap.addItem(newBaseMap);
    });

    describeCredentialsRule("get the list of default private base maps owned by your teams", "yes", "base maps", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/basemaps?default=true")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto ids = response.bodyJson["baseMaps"].byValue
            .map!(a => a["_id"].to!string)
            .array;

          ids.should.contain([ "000000000000000000000004", "000000000000000000000005" ]);
        });
    });

    describeCredentialsRule("get the list of default private base maps owned by your teams", "-", "base maps", "no rights", {
      router
        .request
        .get("/basemaps?default=true")
        .expectStatusCode(200)
        .end((Response response) => () {
          auto ids = response.bodyJson["baseMaps"].byValue
            .map!(a => a["_id"].to!string)
            .array;

          ids.should.not.contain([ "000000000000000000000004", "000000000000000000000005" ]);
        });
    });

    describeCredentialsRule("get the list of default private base maps owned by other teams", "yes", "base maps", "administrator", (string type) {
      router
        .request
        .get("/basemaps?default=true")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto ids = response.bodyJson["baseMaps"].byValue
            .map!(a => a["_id"].to!string)
            .array;

          ids.should.contain([ "000000000000000000000006" ]);
        });
    });

    describeCredentialsRule("get the list of default private base maps owned by other teams", "no", "base maps", ["owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/basemaps?default=true")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto ids = response.bodyJson["baseMaps"].byValue
            .map!(a => a["_id"].to!string)
            .array;

          ids.should.not.contain([ "000000000000000000000006" ]);
        });
    });

    describeCredentialsRule("get the list of default private base maps owned by other teams", "-", "base maps", "no rights", {
      router
        .request
        .get("/basemaps?default=true")
        .expectStatusCode(200)
        .end((Response response) => () {
          auto ids = response.bodyJson["baseMaps"].byValue
            .map!(a => a["_id"].to!string)
            .array;

          ids.should.not.contain([ "000000000000000000000006" ]);
        });
    });


    describeCredentialsRule("get the list of editable base maps owned by your teams", "yes", "base maps", "administrator", (string type) {
      router
        .request
        .get("/basemaps?edit=true&all=true")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto ids = response.bodyJson["baseMaps"].byValue
            .map!(a => a["_id"].to!string)
            .array;

          ids.should.contain([ "000000000000000000000004", "000000000000000000000005" ]);
        });
    });

    describeCredentialsRule("get the list of editable base maps owned by your teams", "yes", "base maps", "owner", (string type) {
      router
        .request
        .get("/basemaps?edit=true")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto ids = response.bodyJson["baseMaps"].byValue
            .map!(a => a["_id"].to!string)
            .array;

          ids.should.contain([ "000000000000000000000004", "000000000000000000000005" ]);
        });
    });

    describeCredentialsRule("get the list of editable base maps owned by your teams", "no", "base maps", ["leader", "member", "guest"], (string type) {
      router
        .request
        .get("/basemaps?edit=true")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto ids = response.bodyJson["baseMaps"].byValue
            .map!(a => a["_id"].to!string)
            .array;

          ids.should.not.contain([ "000000000000000000000004", "000000000000000000000005" ]);
        });
    });

    describeCredentialsRule("get the list of editable base maps owned by your teams", "-", "base maps", "no rights", {
      router
        .request
        .get("/basemaps?edit=true")
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "errors":[
              {"description":"You can't edit items.","status":403,"title":"Forbidden"}
            ]
          }`.parseJsonString);
        });
    });


    describeCredentialsRule("get the list of editable base maps owned by other teams", "yes", "base maps", "administrator", (string type) {
      router
        .request
        .get("/basemaps?edit=true&all=true")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto ids = response.bodyJson["baseMaps"].byValue
            .map!(a => a["_id"].to!string)
            .array;

          ids.should.contain([ "000000000000000000000006" ]);
        });
    });

    describeCredentialsRule("get the list of editable base maps owned by other teams", "no", "base maps", ["owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/basemaps?edit=true&all=true")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto ids = response.bodyJson["baseMaps"].byValue
            .map!(a => a["_id"].to!string)
            .array;

          ids.should.not.contain([ "000000000000000000000006" ]);
        });
    });

    describeCredentialsRule("get the list of editable base maps owned by other teams", "-", "base maps", "no rights", {
      router
        .request
        .get("/basemaps?edit=true&all=true")
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "errors":[
              {"description":"You can't edit items.","status":403,"title":"Forbidden"}
            ]
          }`.parseJsonString);
        });
    });
  });

  describe("Get base map list when there is an item with `isDefault` is false", {
    beforeEach({
      setupTestData();

      crates.picture.addDefault("image/jpeg", "cover", "./deploy/cover.jpg");
      crates.addDefaultTeam("General");

      router = new URLRouter;
      router.crateSetup.setupBaseMapApi(crates);
      setupDefaultBaseMaps(crates);

      auto newBaseMap = `{ "name": "test 1", "icon": "icon", "layers": [], "visibility": {"isDefault": false } }`.parseJsonString;
      crates.baseMap.addItem(newBaseMap);
    });

    describe("without a token", {
      it("should return the base map list with all the default items", {
        router
          .request
          .get("/basemaps?default=true")
          .expectStatusCode(200)
          .end((Response response) => () {
            auto items = cast(Json[]) response.bodyJson["baseMaps"];
            items.map!(a => a["visibility"]["isDefault"].to!bool).should.equal([true, true, true]);
          });
      });


      it("should return an error when edit param is present", {
        router
          .request
          .get("/basemaps?edit=true")
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal((`{"errors": [{
              "description": "You can't edit items.",
              "status": 403,
              "title": "Forbidden" }]}`).parseJsonString);
          });
      });
    });

    describe("with a regular token", {
      it("should return the base map list with all the default items", {
        router
          .request
          .get("/basemaps?default=true")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            auto items = cast(Json[]) response.bodyJson["baseMaps"];
            items.map!(a => a["visibility"]["isDefault"].to!bool).should.equal([true, true, true]);
          });
      });
    });

    describe("with an admin token", {
      it("should return the base map list with all the default items", {
        router
          .request
          .get("/basemaps?default=true")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            auto items = cast(Json[]) response.bodyJson["baseMaps"];
            items.map!(a => a["visibility"]["isDefault"].to!bool).should.equal([true, true, true]);
          });
      });

      it("should return all base maps when edit param is present", {
        router
          .request
          .get("/basemaps?edit=true&all=true")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            auto items = cast(Json[]) response.bodyJson["baseMaps"];
            items.map!(a => a["visibility"]["isDefault"].to!bool).should.equal([true, true, true, false]);
          });
      });
    });
  });
});
