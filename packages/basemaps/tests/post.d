/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.basemaps.post;

import tests.fixtures;
import vibe.http.common;
import ogm.crates.defaults;
import ogm.defaults.baseMaps;
import ogm.basemaps.api;

alias suite = Spec!({
  URLRouter router;

  describe("creating base maps", {
    Json newBaseMap;
    Json newDefaultBaseMap;
    Json newBaseMapForOtherTeam;

    beforeEach({
      setupTestData();
      router = new URLRouter;

      crates.picture.addDefault("image/jpeg", "cover", "./deploy/cover.jpg");
      crates.addDefaultTeam("General");
      router.crateSetup.setupBaseMapApi(crates);
      setupDefaultBaseMaps(crates);

      newBaseMap = `{
        "baseMap": {
          "_id": "000000000000000000000001",
          "name": "test 1",
          "icon": "icon",
          "layers": [],
          "visibility": {"isPublic": true,"team":"000000000000000000000001", "isDefault": false}
        }}`.parseJsonString;

      newDefaultBaseMap = newBaseMap.clone;
      newDefaultBaseMap["baseMap"]["visibility"]["isDefault"] = true;

      newBaseMapForOtherTeam = newBaseMap.clone;
      newBaseMapForOtherTeam["baseMap"]["visibility"]["team"] = "000000000000000000000003";
    });

    describeCredentialsRule("create base maps for your teams", "yes", "base maps", ["owner", "administrator"], (string type) {
      router
        .request
        .post("/basemaps")
        .send(newBaseMap)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "baseMap":
            { "_id": "000000000000000000000004",
              "defaultOrder": 0,
              "attributions": [], "name": "test 1", "icon": "icon", "layers": [],
              "canEdit": true, "visibility": {"isPublic": true,"team":"000000000000000000000001", "isDefault": false} }
          }`.parseJsonString);
        });
    });

    describeCredentialsRule("create base maps for your teams", "no", "base maps", ["leader", "member", "guest"], (string type) {
      router
        .request
        .post("/basemaps")
        .send(newBaseMap)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "errors": [{
            "description": "You don't have enough rights to add an item.",
            "status": 403,
            "title": "Forbidden"
          }]}`.parseJsonString);
        });
    });

    describeCredentialsRule("create base maps for your teams", "-", "base maps", "no rights", {
      router
        .request
        .post("/basemaps")
        .send(newBaseMap)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
              "error": "Authorization required"
            }`.parseJsonString);
        });
    });


    describeCredentialsRule("create base maps for other teams", "yes", "base maps", "administrator", (string type) {
      router
        .request
        .post("/basemaps")
        .send(newBaseMapForOtherTeam)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "baseMap":
            { "_id": "000000000000000000000004",
              "defaultOrder": 0,
              "attributions": [], "name": "test 1", "icon": "icon", "layers": [],
              "canEdit": true, "visibility": {"isPublic": true,"team":"000000000000000000000003", "isDefault": false} }
          }`.parseJsonString);
        });
    });

    describeCredentialsRule("create base maps for other teams", "no", "base maps", ["owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .post("/basemaps")
        .send(newBaseMapForOtherTeam)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{ "errors": [{
            "description": "You can't add an item for the team ` ~ "`000000000000000000000003`"~ `.",
            "status": 403,
            "title": "Forbidden"
          }]}`).parseJsonString);
        });
    });

    describeCredentialsRule("create base maps for other teams", "-", "base maps", "no rights", {
      router
        .request
        .post("/basemaps")
        .send(newBaseMapForOtherTeam)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
              "error": "Authorization required"
            }`.parseJsonString);
        });
    });


    describeCredentialsRule("create default base maps", "yes", "base maps", "administrator", (string type) {
      router
        .request
        .post("/basemaps")
        .send(newDefaultBaseMap)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "baseMap":
            { "_id": "000000000000000000000004",
              "defaultOrder": 0,
              "attributions": [], "name": "test 1", "icon": "icon", "layers": [],
              "canEdit": true, "visibility": {"isPublic": true,"team":"000000000000000000000001", "isDefault": true} }
          }`.parseJsonString);
        });
    });

    describeCredentialsRule("create default base maps", "no", "base maps", ["owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .post("/basemaps")
        .send(newDefaultBaseMap)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "errors": [{
            "description": "You don't have enough rights to add a default item.",
            "status": 403,
            "title": "Forbidden"
          }]}`.parseJsonString);
        });
    });

    describeCredentialsRule("create default base maps", "-", "base maps", "no rights", {
      router
        .request
        .post("/basemaps")
        .send(newDefaultBaseMap)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
              "error": "Authorization required"
            }`.parseJsonString);
        });
    });
  });
});
