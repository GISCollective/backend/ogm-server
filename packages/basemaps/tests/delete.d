/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.basemaps.delete_;

import tests.fixtures;
import vibe.http.common;
import ogm.crates.defaults;
import ogm.defaults.baseMaps;
import ogm.basemaps.api;

alias suite = Spec!({
  URLRouter router;

  describe("delete base maps", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupBaseMapApi(crates);

      crates.picture.addDefault("image/jpeg", "cover", "./deploy/cover.jpg");
      crates.addDefaultTeam("General");
      setupDefaultBaseMaps(crates);

      auto newBaseMap = `{ "name": "test 3", "icon": "icon", "layers": [], "defaultOrder": 3, "visibility": { "isDefault": true, "isPublic": true,"team": "000000000000000000000001" }}`.parseJsonString;
      crates.baseMap.addItem(newBaseMap);
    });

    describeCredentialsRule("delete base maps owned by other teams", "yes", "base maps", "administrator", (string type) {
      auto initialSize = crates.baseMap.get.size;
      crates.baseMap.get.size.should.not.equal(0);

      router
        .request
        .delete_("/basemaps/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end((Response response) => () {
          crates.baseMap.get.size.should.equal(initialSize - 1);
        });
    });

    describeCredentialsRule("delete base maps owned by other teams", "no", "base maps", ["owner", "leader", "member", "guest"], (string type) {
      auto initialSize = crates.baseMap.get.size;

      router
        .request
        .delete_("/basemaps/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(404)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{"errors": [{
              "description": "Item ` ~ "`000000000000000000000001`" ~ ` not found.",
              "status": 404,
              "title": "Crate not found"
            }]}`).parseJsonString);
          crates.baseMap.get.size.should.equal(initialSize);
        });
    });

    describeCredentialsRule("delete base maps owned by other teams", "no", "base maps", "no rights", {
      auto initialSize = crates.baseMap.get.size;

      router
        .request
        .delete_("/basemaps/000000000000000000000001")
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
              "error": "Authorization required"
            }`.parseJsonString);
          crates.baseMap.get.size.should.equal(initialSize);
        });
    });


    describeCredentialsRule("delete base maps owned by your teams", "yes", "base maps", ["administrator", "owner"], (string type) {
      auto initialSize = crates.baseMap.get.size;
      crates.baseMap.get.size.should.not.equal(0);

      router
        .request
        .delete_("/basemaps/000000000000000000000004")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end((Response response) => () {
          crates.baseMap.get.size.should.equal(initialSize - 1);
        });
    });

    describeCredentialsRule("delete base maps owned by your teams", "no", "base maps", ["leader", "member", "guest"], (string type) {
      auto initialSize = crates.baseMap.get.size;

      router
        .request
        .delete_("/basemaps/000000000000000000000004")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{"errors": [{
              "description": "You don't have enough rights to remove ` ~ "`000000000000000000000004`" ~ `.",
              "status": 403,
              "title": "Forbidden"
            }]}`).parseJsonString);
          crates.baseMap.get.size.should.equal(initialSize);
        });
    });

    describeCredentialsRule("delete base maps owned by your teams", "-", "base maps", "no rights", {
      auto initialSize = crates.baseMap.get.size;

      router
        .request
        .delete_("/basemaps/000000000000000000000001")
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
              "error": "Authorization required"
            }`.parseJsonString);
          crates.baseMap.get.size.should.equal(initialSize);
        });
    });


    describe("with an admin token", {
      describe("for a base map with a custom cover", {
        beforeEach({
          Picture customCover;
          customCover.name = "custom cover";
          customCover.owner = "";
          customCover.picture = new PictureFile;
          auto result = crates.picture.addItem(customCover.serializeToJson);
          customCover._id = ObjectId.fromString(result["_id"].to!string);

          auto baseMap = crates.baseMap.getItem("000000000000000000000001").exec.front;
          baseMap["cover"] = result["_id"];
          crates.baseMap.updateItem(baseMap);
        });

        it("should delete the cover map", {
          router
            .request
            .delete_("/basemaps/000000000000000000000001")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(204)
            .end((Response response) => () {
              crates.picture.get.exec
                .filter!(a => a["name"] == "custom cover")
                  .array.length.should.equal(0);
            });
        });
      });

      describe("for a base map with a default cover", {
        beforeEach({
          Picture customCover;
          customCover.name = "custom cover";
          customCover.owner = "@system";
          customCover.picture = new PictureFile;
          auto result = crates.picture.addItem(customCover.serializeToJson);
          customCover._id = ObjectId.fromString(result["_id"].to!string);

          auto baseMap = crates.baseMap.getItem("000000000000000000000001").exec.front;

          baseMap["cover"] = result["_id"];
          crates.baseMap.updateItem(baseMap);
        });

        it("should not delete the cover map", {
          router
            .request
            .delete_("/basemaps/000000000000000000000001")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(204)
            .end((Response response) => () {
              crates.picture.get.exec
                .filter!(a => a["_id"] == "000000000000000000000001")
                  .array.length.should.equal(1);
            });
        });
      });
    });
  });
});
