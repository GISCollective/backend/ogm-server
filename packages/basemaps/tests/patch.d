/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.basemaps.patch;

import tests.fixtures;
import vibe.http.common;
import ogm.crates.defaults;
import ogm.defaults.baseMaps;
import ogm.basemaps.api;

alias suite = Spec!({
  URLRouter router;

  describe("patching base maps", {
    Json basemap;

    beforeEach({
      setupTestData();
      router = new URLRouter;

      crates.picture.addDefault("image/jpeg", "cover", "./deploy/cover.jpg");
      crates.addDefaultTeam("General");
      router.crateSetup.setupBaseMapApi(crates);
      setupDefaultBaseMaps(crates);

      auto newBaseMap = `{ "_id": "000000000000000000000001", "name": "test 1", "icon": "icon",
          "visibility": {"isPublic": true,"team":"000000000000000000000001"},"layers": [] }`.parseJsonString;
      basemap = `{ "baseMap": { "name": "test 1", "icon": "icon", "layers": [],
          "visibility": {"isPublic": true,"team":"000000000000000000000001", "isDefault": true },
          }}`.parseJsonString;
      crates.baseMap.addItem(newBaseMap);
    });

    describe("without a token", {
      it("should not update an base map", {
        router
          .request
          .patch("/basemaps/000000000000000000000001")
          .send(basemap)
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
                "error": "Authorization required"
              }`.parseJsonString);
          });
      });
    });

    describe("with a regular token", {
      it("should not update a base map", {
        router
          .request
          .patch("/basemaps/000000000000000000000001")
          .send(basemap)
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(404)
          .end((Response response) => () {
            response.bodyJson.should.equal((`{"errors": [{
                "description": "Item ` ~ "`000000000000000000000001`" ~ ` not found.",
                "status": 404,
                "title": "Crate not found"
              }]}`).parseJsonString);
          });
      });
    });

    describe("with an admin token", {
      it("should update a base map", {
        router
          .request
          .patch("/basemaps/000000000000000000000001")
          .send(basemap)
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
              response.bodyJson.should.equal(`{ "baseMap": {
                "_id": "000000000000000000000001",
                "name": "test 1",
                "icon": "icon",
                "defaultOrder": 0,
                "cover": "000000000000000000000003",
                "layers": [],
                "canEdit": true,
                "attributions": [{
                  "name": "© OpenStreetMap contributors",
                  "url": "https://www.openstreetmap.org/copyright"
                }],
                "visibility": {"isPublic": true,"team":"000000000000000000000001","isDefault": true} }
            }`.parseJsonString);
          });
      });

      describe("for a base map with a custom cover", {
        beforeEach({
          Picture customCover;
          customCover.name = "custom cover";
          customCover.owner = "";
          customCover.picture = new PictureFile;
          auto result = crates.picture.addItem(customCover.serializeToJson);
          customCover._id = ObjectId.fromString(result["_id"].to!string);

          auto baseMap = crates.baseMap.getItem("000000000000000000000001").exec.front;
          baseMap["cover"] = result["_id"];
          crates.baseMap.updateItem(baseMap);
        });

        it("should delete the cover map", {
          basemap["baseMap"]["cover"] = "000000000000000000000001";

          router
            .request
            .patch("/basemaps/000000000000000000000001")
            .send(basemap)
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              crates.picture.get.exec
                .filter!(a => a["name"] == "custom cover")
                  .array.length.should.equal(0);
            });
        });
      });

      describe("for a base map with a default cover", {
        Json result;

        beforeEach({
          Picture customCover;
          customCover.name = "system cover";
          customCover.owner = "@system";
          customCover.picture = new PictureFile;
          result = crates.picture.addItem(customCover.serializeToJson);
          customCover._id = ObjectId.fromString(result["_id"].to!string);

          auto baseMap = crates.baseMap.getItem("000000000000000000000001").exec.front;

          baseMap["cover"] = result["_id"];
          crates.baseMap.updateItem(baseMap);
        });

        it("should not delete the cover map", {
          basemap["baseMap"]["cover"] = "000000000000000000000001";

          router
            .request
            .patch("/basemaps/000000000000000000000001")
            .send(basemap)
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              crates.picture.get.exec
                .filter!(a => a["_id"] == result["_id"])
                  .array.length.should.equal(1);
            });
        });
      });
    });
  });
});
