/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.basemaps.put;

import tests.fixtures;
import vibe.http.common;
import ogm.crates.defaults;
import ogm.defaults.baseMaps;
import ogm.basemaps.api;

alias suite = Spec!({
  URLRouter router;

  describe("replacing a base maps", {
    Json basemap;
    Json basemapForOtherTeam;
    Json defaultBasemap;

    beforeEach({
      setupTestData();
      router = new URLRouter;

      crates.picture.addDefault("image/jpeg", "cover", "./deploy/cover.jpg");
      crates.addDefaultTeam("General");
      router.crateSetup.setupBaseMapApi(crates);
      setupDefaultBaseMaps(crates);

      auto newBaseMap = `{
        "_id": "000000000000000000000001",
        "name": "test 1",
        "icon": "icon",
        "layers": [],
        "visibility": {"isPublic": false,"team":"000000000000000000000001","isDefault": false}
      }`.parseJsonString;

      basemap = `{ "baseMap": {
        "name": "test 2",
        "icon": "icon",
        "layers": [],
        "visibility": {"isPublic": false,"team":"000000000000000000000001","isDefault": false}
      }}`.parseJsonString;

      basemapForOtherTeam = `{ "baseMap": {
        "name": "test 2",
        "icon": "icon",
        "layers": [],
        "visibility": {"isPublic": false,"team":"000000000000000000000001","isDefault": true}
      }}`.parseJsonString;

      defaultBasemap = `{ "baseMap": {
        "name": "test 1",
        "icon": "icon",
        "layers": [],
        "visibility": {"isPublic": false,"team":"000000000000000000000001","isDefault": true}
      }}`.parseJsonString;

      crates.baseMap.addItem(newBaseMap);
    });


    describeCredentialsRule("update base maps for your teams", "yes", "base maps", ["owner", "administrator"], (string type) {
      router
        .request
        .put("/basemaps/000000000000000000000004")
        .send(basemap)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "baseMap": {
              "_id": "000000000000000000000004",
              "name": "test 2",
              "defaultOrder": 0,
              "icon": "icon",
              "layers": [],
              "canEdit": true,
              "attributions": [],
              "visibility": {
                "isPublic": false,
                "isDefault": false,
                "team": "000000000000000000000001"
          }}}`.parseJsonString);
      });
    });

    describeCredentialsRule("update base maps for your teams", "no", "base maps", ["leader", "member", "guest"], (string type) {
      router
        .request
        .put("/basemaps/000000000000000000000004")
        .send(basemap)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{"errors": [{
              "description": "You don't have enough rights to edit ` ~ "`000000000000000000000004`" ~`.",
              "status": 403,
              "title": "Forbidden"
            }]}`).parseJsonString);
        });
    });

    describeCredentialsRule("update base maps for your teams", "-", "base maps", "no rights", {
      router
        .request
        .put("/basemaps/000000000000000000000004")
        .send(basemap)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
              "error": "Authorization required"
            }`.parseJsonString);
        });
    });


    describeCredentialsRule("update base maps for other teams", "yes", "base maps", "administrator", (string type) {
      router
        .request
        .put("/basemaps/000000000000000000000001")
        .send(basemapForOtherTeam)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "baseMap": {
              "_id": "000000000000000000000001",
              "name": "test 2",
              "defaultOrder": 0,
              "icon": "icon",
              "layers": [],
              "canEdit": true,
              "attributions": [],
              "visibility": {
                "isPublic": false,
                "isDefault": true,
                "team": "000000000000000000000001"
          }}}`.parseJsonString);
      });
    });

    describeCredentialsRule("update base maps for other teams", "no", "base maps", ["owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .put("/basemaps/000000000000000000000001")
        .send(basemapForOtherTeam)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(404)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{"errors": [{
              "description": "Item ` ~ "`000000000000000000000001`" ~` not found.",
              "status": 404,
              "title": "Crate not found"
            }]}`).parseJsonString);
        });
    });

    describeCredentialsRule("update base maps for other teams", "no", "base maps", "no rights", {
      router
        .request
        .put("/basemaps/000000000000000000000001")
        .send(basemapForOtherTeam)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
              "error": "Authorization required"
            }`.parseJsonString);
        });
    });


    describeCredentialsRule("set base maps as default", "yes", "base maps", "administrator", (string type) {
      router
        .request
        .put("/basemaps/000000000000000000000004")
        .send(defaultBasemap)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "baseMap": {
              "_id": "000000000000000000000004",
              "name": "test 1",
              "defaultOrder": 0,
              "icon": "icon",
              "layers": [],
              "canEdit": true,
              "attributions": [],
              "visibility": {
                "isPublic": false,
                "isDefault": true,
                "team": "000000000000000000000001"
          }}}`.parseJsonString);
      });
    });

    describeCredentialsRule("set base maps as default", "no", "base maps", [ "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .put("/basemaps/000000000000000000000004")
        .send(defaultBasemap)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{"errors": [{
              "description": "You don't have enough rights to change the ` ~ "`isDefault`" ~ ` flag.",
              "status": 403,
              "title": "Forbidden"
            }]}`).parseJsonString);
        });
    });

    describeCredentialsRule("set base maps as default", "no", "base maps", "no rights", {
      router
        .request
        .put("/basemaps/000000000000000000000004")
        .send(defaultBasemap)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
              "error": "Authorization required"
            }`.parseJsonString);
        });
    });
  });
});
