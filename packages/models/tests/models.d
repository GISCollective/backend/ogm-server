/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.models;

import tests.fixtures;
import ogm.models.api;

alias suite = Spec!({
  describe("GET", {
    URLRouter router;
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupModelApi(crates);
    });

    it("should get the number of public maps", {
      router
        .request
        .get("/models/000000000000000000000001")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"model":{
            "_id": "000000000000000000000001",
            "itemCount": 2,
            "name": "Map"
          }}`.parseJsonString);
        });
    });

    it("should get the number of public sites", {
      router
        .request
        .get("/models/000000000000000000000002")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"model":{
            "_id": "000000000000000000000002",
            "itemCount": 2,
            "name": "Feature"
          }}`.parseJsonString);
        });
    });

    it("should get the number of public teams", {
      router
        .request
        .get("/models/000000000000000000000003")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"model":{
            "_id": "000000000000000000000003",
            "itemCount": 4,
            "name": "Team"
          }}`.parseJsonString);
        });
    });

    describe("when there an auth token is present", {
      it("should get the number of accessible maps", {
        router
          .request
          .get("/models/000000000000000000000001")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"model":{
              "_id": "000000000000000000000001",
              "itemCount": 3,
              "name": "Map"
            }}`.parseJsonString);
          });
      });

      it("should get the number of accessible sites", {
        router
          .request
          .get("/models/000000000000000000000002")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"model":{
              "_id": "000000000000000000000002",
              "itemCount": 3,
              "name": "Feature"
            }}`.parseJsonString);
          });
      });

      it("should get the number of accessible teams", {
        router
          .request
          .get("/models/000000000000000000000003")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"model":{
              "_id": "000000000000000000000003",
              "itemCount": 4,
              "name": "Team"
            }}`.parseJsonString);
          });
      });
    });
  });
});
