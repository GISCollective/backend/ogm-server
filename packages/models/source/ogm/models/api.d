/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.models.api;

import vibe.http.router;
import crate.http.router;
import crate.base;

import ogm.auth;
import ogm.crates.all;

import ogm.middleware.adminrequest;
import ogm.middleware.userdata;

import ogm.filter.indirectvisibility;
import ogm.filter.visibility;
import ogm.filter.searchterm;
import ogm.filter.searchtag;

import ogm.maps.middlewares.get;
import ogm.middleware.features.GetFeaturesFilter;
import ogm.icons.TaxonomyTree;
import ogm.icons.IconCache;

///
void setupModelApi(T)(CrateRouter!T crateRouter, OgmCrates crates) {
  auto auth = Authentication.instance(crates.user);
  auto adminRequest = new AdminRequest(crates.user);
  auto userDataMiddleware = UserDataMiddleware.instance(crates);

  auto cache = new IconCache(&crates.icon.get, &crates.iconSet.getItem);
  auto iconTree = new TaxonomyTree(&crates.icon.get, &crates.iconSet.getItem, cache);

  auto featuresFilter = new GetFeaturesFilter(auth.getCollection, crates, iconTree);
  auto mapsVisibilityFilter = new VisibilityFilter!"maps"(crates, crates.map);
  auto mapsFilter = new GetMapsFilter(crates);

  auto campaignVisibilityFilter = new VisibilityFilter!"campaigns"(crates, crates.campaign);

  auto iconsVisibilityFilter = new VisibilityFilter!"iconSets"(crates, crates.iconSet);

  auto searchTermFilter = new SearchTermFilter;
  auto searchTagFilter = new SearchTagFilter;
  auto indirectVisibility = new IndirectVisibility(crates, crates.feature);

  auto modelPageFilter = new ModelPageFilter();
  modelPageFilter.add(crates.map, mapsVisibilityFilter, mapsFilter, searchTermFilter, searchTagFilter);
  modelPageFilter.add(crates.feature, indirectVisibility, featuresFilter, searchTermFilter, searchTagFilter);
  modelPageFilter.add(crates.team);
  modelPageFilter.add(crates.iconSet, iconsVisibilityFilter, searchTermFilter);
  modelPageFilter.add(crates.campaign, campaignVisibilityFilter, searchTermFilter, searchTagFilter);

  crateRouter
    .add(crates.model,
      auth.publicDataMiddleware,
      adminRequest,
      userDataMiddleware,
      modelPageFilter);
}
