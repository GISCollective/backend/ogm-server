/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.features.listItems;

import tests.fixtures;
import tests.data.features;
import vibe.http.common;
import geo.json;
import ogm.features.api;

alias suite = Spec!({
  URLRouter router;

  describe("GET list", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupFeaturesApi(crates);
    });

    describeCredentialsRule("get features from own teams", "yes", "features", ["administrator"], (string type) {
      router
        .request
        .get("/features")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["features"].byValue.map!(a => a["_id"].to!string).should.equal(["000000000000000000000001", "000000000000000000000002", "000000000000000000000003", "000000000000000000000004"]);
          response.bodyJson["features"].byValue.map!(a => a["canEdit"].to!string).should.equal(["true", "true", "true", "true"]);
        });
    });

    describeCredentialsRule("get features from own teams", "yes", "features", [ "owner", "leader"], (string type) {
      router
        .request
        .get("/features")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["features"].byValue.map!(a => a["_id"].to!string).should.equal(["000000000000000000000001", "000000000000000000000002", "000000000000000000000003"]);
          response.bodyJson["features"].byValue.map!(a => a["canEdit"].to!string).should.equal(["true", "true", "false"]);
        });
    });

    describeCredentialsRule("get features from own teams", "yes", "features", [ "member", "guest"], (string type) {
      router
        .request
        .get("/features")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["features"].byValue.map!(a => a["_id"].to!string).should.equal(["000000000000000000000001", "000000000000000000000002", "000000000000000000000003"]);
          response.bodyJson["features"].byValue.map!(a => a["canEdit"].to!string).should.equal(["false", "false", "false"]);
        });
    });

    describeCredentialsRule("get features from own teams", "no", "features", "no rights", {
      router
        .request
        .get("/features")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["features"].byValue.map!(a => a["_id"].to!string).should.equal(["000000000000000000000001", "000000000000000000000003"]);
          response.bodyJson["features"].byValue.map!(a => a["canEdit"].to!string).should.equal(["null", "null"]);
        });
    });

    ///

    describeCredentialsRule("get features from other teams private maps", "yes", "features", ["administrator"], (string type) {
      router
        .request
        .get("/features?map=000000000000000000000004")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["features"].byValue.map!(a => a["_id"].to!string).should.equal(["000000000000000000000004"]);
          response.bodyJson["features"].byValue.map!(a => a["canEdit"].to!string).should.equal([ "true"]);
        });
    });

    describeCredentialsRule("get features from other teams private maps", "no", "features", [ "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/features?map=000000000000000000000004")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["features"].byValue.map!(a => a["_id"].to!string).should.equal([]);
          response.bodyJson["features"].byValue.map!(a => a["canEdit"].to!string).should.equal([]);
        });
    });

    describeCredentialsRule("get features from other teams private maps", "no", "features", ["no rights"], {
      router
        .request
        .get("/features?map=000000000000000000000004")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["features"].byValue.map!(a => a["_id"].to!string).should.equal([]);
          response.bodyJson["features"].byValue.map!(a => a["canEdit"].to!string).should.equal([]);
        });
    });


    ///

    describeCredentialsRule("get features from other teams public maps", "yes", "features", ["administrator"], (string type) {
      router
        .request
        .get("/features?map=000000000000000000000003")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["features"].byValue.map!(a => a["_id"].to!string).should.equal(["000000000000000000000003"]);
          response.bodyJson["features"].byValue.map!(a => a["canEdit"].to!string).should.equal([ "true" ]);
        });
    });

    describeCredentialsRule("get features from other teams public maps", "yes", "features", [ "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/features?map=000000000000000000000003")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["features"].byValue.map!(a => a["_id"].to!string).should.equal(["000000000000000000000003"]);
          response.bodyJson["features"].byValue.map!(a => a["canEdit"].to!string).should.equal([ "false" ]);
        });
    });

    describeCredentialsRule("get features from other teams public maps", "yes", "features", ["no rights"], {
      router
        .request
        .get("/features?map=000000000000000000000003")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["features"].byValue.map!(a => a["_id"].to!string).should.equal(["000000000000000000000003"]);
          response.bodyJson["features"].byValue.map!(a => a["canEdit"].to!string).should.equal([ "null" ]);
        });
    });

    ///
    describeCredentialsRule("get features from own private maps", "yes", "features", ["administrator"], (string type) {
      router
        .request
        .get("/features?map=000000000000000000000002")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["features"].byValue.map!(a => a["_id"].to!string).should.equal(["000000000000000000000002"]);
          response.bodyJson["features"].byValue.map!(a => a["canEdit"].to!string).should.equal([ "true" ]);
        });
    });

    describeCredentialsRule("get features from own private maps", "yes", "features", [ "owner", "leader" ], (string type) {
      router
        .request
        .get("/features?map=000000000000000000000002")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["features"].byValue.map!(a => a["_id"].to!string).should.equal(["000000000000000000000002"]);
          response.bodyJson["features"].byValue.map!(a => a["canEdit"].to!string).should.equal([ "true" ]);
        });
    });

    describeCredentialsRule("get features from own private maps", "yes", "features", [ "member", "guest"], (string type) {
      router
        .request
        .get("/features?map=000000000000000000000002")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["features"].byValue.map!(a => a["_id"].to!string).should.equal(["000000000000000000000002"]);
          response.bodyJson["features"].byValue.map!(a => a["canEdit"].to!string).should.equal([ "false" ]);
        });
    });

    describeCredentialsRule("get features from own private maps", "no", "features", ["no rights"], {
      router
        .request
        .get("/features?map=000000000000000000000002")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["features"].byValue.map!(a => a["_id"].to!string).should.equal([]);
          response.bodyJson["features"].byValue.map!(a => a["canEdit"].to!string).should.equal([]);
        });
    });


    ///
    describeCredentialsRule("get features from own public maps", "yes", "features", ["administrator"], (string type) {
      router
        .request
        .get("/features?map=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["features"].byValue.map!(a => a["_id"].to!string).should.equal(["000000000000000000000001"]);
          response.bodyJson["features"].byValue.map!(a => a["canEdit"].to!string).should.equal([ "true" ]);
        });
    });

    describeCredentialsRule("get features from own public maps", "yes", "features", [ "owner", "leader" ], (string type) {
      router
        .request
        .get("/features?map=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["features"].byValue.map!(a => a["_id"].to!string).should.equal(["000000000000000000000001"]);
          response.bodyJson["features"].byValue.map!(a => a["canEdit"].to!string).should.equal([ "true" ]);
        });
    });

    describeCredentialsRule("get features from own public maps", "yes", "features", [ "member", "guest"], (string type) {
      router
        .request
        .get("/features?map=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["features"].byValue.map!(a => a["_id"].to!string).should.equal(["000000000000000000000001"]);
          response.bodyJson["features"].byValue.map!(a => a["canEdit"].to!string).should.equal([ "false" ]);
        });
    });

    describeCredentialsRule("get features from own public maps", "no", "features", ["no rights"], {
      router
        .request
        .get("/features?map=000000000000000000000001")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["features"].byValue.map!(a => a["_id"].to!string).should.equal(["000000000000000000000001"]);
          response.bodyJson["features"].byValue.map!(a => a["canEdit"].to!string).should.equal([ "null" ]);
        });
    });
  });
});
