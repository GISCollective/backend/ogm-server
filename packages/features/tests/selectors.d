/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.features.selectors;

import tests.fixtures;
import tests.data.features;
import vibe.http.common;
import geo.json : GeoJsonGeometry;
import ogm.features.api;

alias suite = Spec!({
  URLRouter router;
  string publicFeatureId = "000000000000000000000001";

  describe("The features list", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupFeaturesApi(crates);
    });

    describeCredentialsRule("filter by map", "yes", "features", ["administrator", "owner", "leader"], (string type) {
      router
        .request
        .get("/features?map=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["features"].length.should.equal(1);
          response.bodyJson["features"][0].should.equal(site1Editable);
        });
    });

    describeCredentialsRule("filter by map", "yes", "features", "member", (string type) {
      auto site = site1.clone;
      site["canEdit"] = false;

      router
        .request
        .get("/features?map=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["features"].length.should.equal(1);
          response.bodyJson["features"][0].should.equal(site);
        });
    });

    describeCredentialsRule("filter by map", "yes", "features", ["guest"], (string type) {
      router
        .request
        .get("/features?map=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["features"].length.should.equal(1);

          auto expected = site1.clone;
          expected["canEdit"] = false;
          response.bodyJson["features"][0].should.equal(expected);
        });
    });

    describeCredentialsRule("filter by map", "yes", "features", "no rights", (string type) {
      router
        .request
        .get("/features?map=000000000000000000000001")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["features"].length.should.equal(1);
          response.bodyJson["features"][0].should.equal(site1);
        });
    });

    ///
    describeCredentialsRule("filter by author", "yes", "features", ["administrator", "owner", "leader"], (string type) {
      router
        .request
        .get("/features?author=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["features"].length.should.equal(1);
          response.bodyJson["features"][0].should.equal(site1Editable);
        });
    });

    describeCredentialsRule("filter by author", "yes", "features", "member", (string type) {
      auto _site1 = site1.clone;
      _site1["canEdit"] = false;

      router
        .request
        .get("/features?author=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["features"].length.should.equal(1);
          response.bodyJson["features"][0].should.equal(_site1);
        });
    });

    describeCredentialsRule("filter by author", "yes", "features", "guest", (string type) {
      router
        .request
        .get("/features?author=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["features"].length.should.equal(1);

          auto expected = site1.clone;
          expected["canEdit"] = false;
          response.bodyJson["features"][0].should.equal(expected);
        });
    });

    describeCredentialsRule("filter by author", "yes", "features", "no rights", (string type) {
      router
        .request
        .get("/features?author=000000000000000000000001")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["features"].length.should.equal(1);
          response.bodyJson["features"][0].should.equal(site1);
        });
    });

    describe("when there is a feature assigned to an index map", {
      beforeEach({
        createMap(Map(
          ObjectId.fromString("5"), "indexed", Json(""),
          Visibility(team1, true), Polygon([[[-7, 3], [-7, 5], [-8, 5], [-8, 3], [-7,3]]]),
          cover, IconSets(true, [iconSet1]), BaseMaps(), "", SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-02-01T00:00:00Z"),
          ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z")),
          Picture.init, Mask(), true));

        auto feature = Feature(ObjectId.fromString("5"),
          [],
          "site5",
          `{
            "blocks": [{
              "type": "paragraph",
              "data": {
                "text": "description of site5"
              }
            }]
          }`.parseJsonString,
          GeoJsonGeometry.fromJson(`{ "type": "Point", "coordinates": [1.8, 1.8] }`.parseJsonString),
          ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z")),
          [],
          [],
          [],
          VisibilityEnum.Public);
        feature.maps = [ Map(ObjectId.fromString("5")) ];
        createFeature(feature);
      });

      it("should not get the indexed feature in the list for unauthenticated users", {
        router
          .request
          .get("/features")
          .expectStatusCode(200)
          .end((Response response) => () {
            auto features = response.bodyJson["features"].byValue;
            features.map!(a => a["_id"].to!string).should.not.contain("000000000000000000000005");
          });
      });

      it("should get only the indexed features in the list when onlyIndexes = true for unauthenticated users", {
        router
          .request
          .get("/features?onlyIndexes=true")
          .expectStatusCode(200)
          .end((Response response) => () {
            auto features = response.bodyJson["features"].byValue;
            features.map!(a => a["_id"].to!string).should.containOnly(["000000000000000000000005"]);
          });
      });

      it("should get the indexed feature by id for unauthenticated users", {
        router
          .request
          .get("/features/000000000000000000000005")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["feature"]["_id"].to!string.should.equal("000000000000000000000005");
          });
      });

      it("should not get the indexed feature in the list for admin users", {
        router
          .request
          .get("/features")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            auto features = response.bodyJson["features"].byValue;
            features.map!(a => a["_id"].to!string).should.not.contain("000000000000000000000005");
          });
      });

      it("should get only the indexed features in the list when onlyIndexes = true for admin users", {
        router
          .request
          .get("/features?onlyIndexes=true")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            auto features = response.bodyJson["features"].byValue;
            features.map!(a => a["_id"].to!string).should.containOnly(["000000000000000000000005"]);
          });
      });

      it("should not return index features when onMainMap = true for admin users", {
        router
          .request
          .get("/features?onMainMap=true")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            auto features = response.bodyJson["features"].byValue;
            features.map!(a => a["_id"].to!string).should.not.contain("000000000000000000000005");
          });
      });
    });

    describe("with the term parameter", {
      it("should find the term in the description", {
        router
          .request
          .get("/features?term=description of site1")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["features"].type.should.equal(Json.Type.array);
            response.bodyJson["features"].should.equal([ site1 ]);
          });
      });
    });

    describe("with range selectors", {
      it("should return features with a limit", {
        router
          .request
          .get("/features?limit=1")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["features"].type.should.equal(Json.Type.array);
            response.bodyJson["features"].should.equal([ site1 ]);
          });
      });

      it("should skip features", {
        router
          .request
          .get("/features?skip=1")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["features"].type.should.equal(Json.Type.array);
            response.bodyJson["features"].should.equal([ site3 ]);
          });
      });
    });

    describe("with a map selector", {
      it("should return only the features of public maps", {
        router
          .request
          .get("/features")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["features"].length.should.equal(2);
            response.bodyJson.should.equal(parseJsonString(`{"features": [ ` ~ site1.to!string ~ `, ` ~ site3.to!string ~ ` ]}`));
          });
      });

      it("should return features of a specific public map mentioned in the request" , {
        router
          .request
          .get("/features?map=000000000000000000000001")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(parseJsonString(`{"features":[ ` ~ site1.to!string ~ ` ]}`));
          });
      });

      it("should not return features for a specific private map" , {
        router
          .request
          .get("/features?map=000000000000000000000002")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"features":[]}`.parseJsonString);
          });
      });
    });

    describe("with the published selector", {
      describe("with admin token", {
        beforeEach({
          auto feature = Feature(ObjectId.fromString("5"),
                  [],
                  "site5",
                  `{
                    "blocks": [{
                      "type": "paragraph",
                      "data": {
                        "text": "description of site5"
                      }
                    }]
                  }`.parseJsonString,
                  GeoJsonGeometry.fromJson(`{ "type": "Point", "coordinates": [1.8, 1.8] }`.parseJsonString),
                  ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z")),
                  [],
                  [],
                  [],
                  VisibilityEnum.Private);
          feature.maps = [ Map(ObjectId.fromString("1")) ];
          createFeature(feature);
        });

        it("should return an error when visibility is not true or false", {
          router
            .request
            .get("/features?visibility=invalid")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(403)
            .end((Response response) => () {
              response.bodyJson.should.equal((`{"errors": [{
                  "description": "You provided an invalid value for the ` ~ "`visibility`" ~ ` parameter.",
                  "status": 403,
                  "title": "Forbidden"
                }]}`).parseJsonString);
            });
        });
      });

      describe("with a leader token", {
        beforeEach({
          auto feature = Feature(ObjectId.fromString("5"),
                  [],
                  "site5",
                  `{
                    "blocks": [{
                      "type": "paragraph",
                      "data": {
                        "text": "description of site5"
                      }
                    }]
                  }`.parseJsonString,
                  GeoJsonGeometry.fromJson(`{ "type": "Point", "coordinates": [1.8, 1.8] }`.parseJsonString),
                  ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z")),
                  [],
                  [],
                  [],
                  VisibilityEnum.Private);
          feature.maps = [ Map(ObjectId.fromString("1")) ];
          createFeature(feature);
        });

        it("should return only the published features when visibility=1", {
          router
            .request
            .get("/features?visibility=1")
            .header("Authorization", "Bearer " ~ bearerLeaderToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              Json[] features = cast(Json[]) response.bodyJson["features"];
              features.map!(a => a["_id"].to!string).array.should.equal(["000000000000000000000001", "000000000000000000000003"]);
            });
        });

        it("should return only the unpublished features when visibility=0", {
          router
            .request
            .get("/features?visibility=0")
            .header("Authorization", "Bearer " ~ bearerLeaderToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              Json[] features = cast(Json[]) response.bodyJson["features"];
              features.map!(a => a["_id"].to!string).array.should.equal([ "000000000000000000000002", "000000000000000000000005" ]);
            });
        });
      });

      describe("without a token", {
        it("should return an error when visibility=1", {
          router
            .request
            .get("/features?visibility=1")
            .expectStatusCode(403)
            .end((Response response) => () {
              response.bodyJson.should.equal((`{"errors": [{
                  "description": "You can't use the ` ~ "`visibility`" ~ ` parameter, without an auth token",
                  "status": 403,
                  "title": "Forbidden"
                }]}`).parseJsonString);
            });
        });
      });
    });

    describe("with an author selector", {
      describe("with no token", {
        it("should return only the sites of public maps of a the provided author", {
          router
            .request
            .get("/features?author=000000000000000000000001")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["features"].length.should.equal(1);
              response.bodyJson.should.equal(parseJsonString(`{"features":[ ` ~ site1.to!string ~ ` ]}`));
            });
        });

        it("should return no features when edit is true", {
          router
            .request
            .get("/features?author=000000000000000000000001&edit=true")
            .expectStatusCode(403)
            .end((Response response) => () {
              response.bodyJson.should.equal("{
                \"errors\": [{
                  \"description\": \"You can't edit items.\",
                  \"status\": 403,
                  \"title\": \"Forbidden\"
                }]
              }".parseJsonString);
            });
        });

        it("should return no features when the user has no features", {
          router
            .request
            .get("/features?author=000000000000000000000002")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["features"].length.should.equal(0);
            });
        });
      });

      describe("with a leader token", {
        it("should return only the features of the provided author which the current user can edit", {
          router
            .request
            .get("/features?author=000000000000000000000001&edit=true")
            .header("Authorization", "Bearer " ~ bearerLeaderToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["features"].length.should.equal(1);
              response.bodyJson.should.equal(parseJsonString(`{"features":[ ` ~ site1Editable.to!string ~ ` ]}`));
            });
        });
      });

      describe("with an admin token", {
        it("should return only the features of the provided author which the current user can edit, based on the teams", {
          router
            .request
            .get("/features?author=000000000000000000000001&edit=true")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["features"].length.should.equal(0);
            });
        });

        it("should return only the features of the provided author which the current user can edit", {
          router
            .request
            .get("/features?author=000000000000000000000001&edit=true&all=true")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["features"].length.should.equal(1);
              response.bodyJson.should.equal(parseJsonString(`{"features":[ ` ~ site1Editable.to!string ~ ` ]}`));
            });
        });
      });
    });

    describe("with unpublished features", {
      beforeEach({
        createFeature(
          Feature(ObjectId.fromString("5"),
              [ Map(ObjectId.fromString("1")) ],
              "site5",
              `{
                "blocks": [{
                  "type": "paragraph",
                  "data": {
                    "text": "description of site5"
                  }
                }]
              }`.parseJsonString,
              GeoJsonGeometry.fromJson(`{ "type": "Point", "coordinates": [1.5, 1.5] }`.parseJsonString),
              ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z")),
              [],
              [],
              [],
              VisibilityEnum.Private));
      });

      it("should not return unpublished features", {
        router
        .request
        .get("/features")
        .expectStatusCode(200)
        .end((Response response) => () {
          (cast(Json[]) response.bodyJson["features"]).map!(a => a["_id"].to!string)
            .should
            .not
            .contain("000000000000000000000005");
        });
      });
    });

    describe("when there is an open issue for a feature", {
      beforeEach(function() {
        createIssue(Issue(ObjectId.fromString("000000000000000000000001"), "000000000000000000000001"));
      });

      it("should set the issue count to 1", {
        router
          .request
          .get("/features?issues=true")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["features"].length.should.equal(1);
            response.bodyJson["features"][0]["_id"].should.equal("000000000000000000000001");
          });
      });
    });

    it("should find items by a term", {
      router
        .request
        .get("/features?term=site1")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["features"].type.should.equal(Json.Type.array);
          response.bodyJson["features"].should.equal([ site1 ]);
        });
    });

    it("should filter features by icon category", {
      router
        .request
        .get("/features?iconCategory=category2")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["features"].length.should.equal(1);
          response.bodyJson.should.equal(parseJsonString(`{"features":[ ` ~ site3.to!string ~ ` ]}`));
        });
    });

    it("should return no item if there is no icon category match", {
      router
        .request
        .get("/features?iconCategory=other")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["features"].length.should.equal(0);
        });
    });

    it("should filter features by icon subcategory", {
      router
        .request
        .get("/features?iconSubcategory=subcategory2")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["features"].length.should.equal(1);
          response.bodyJson.should.equal(parseJsonString(`{"features":[ ` ~ site3.to!string ~ ` ]}`));
        });
    });

    it("should return no item if there is no icon category match", {
      router
        .request
        .get("/features?iconSubcategory=other")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["features"].length.should.equal(0);
        });
    });

    describe("the icons filter", {
      it("should ignore empty icons parameter", {
        router
          .request
          .get("/features?icons=")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["features"].length.should.equal(2);
            response.bodyJson.should.equal(parseJsonString(`{"features":[ ` ~ site1.to!string ~ `, ` ~ site3.to!string ~ ` ]}`));
          });
      });

      it("finds features when at least one icon is matched", {
        router
          .request
          .get("/features?icons=000000000000000000000001,000000000000000000000011")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["features"].length.should.equal(2);
            response.bodyJson.should.equal(parseJsonString(`{"features":[ ` ~ site1.to!string ~ `, ` ~ site3.to!string ~ ` ]}`));
          });
      });

      it("finds no features when the icon is not matched", {
        router
          .request
          .get("/features?icons=000000000000000000000011")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["features"].length.should.equal(0);
          });
      });

      it("finds no features when there is no icon having all icons with iop=and", {
        router
          .request
          .get("/features?icons=000000000000000000000001,000000000000000000000003&iop=and")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["features"].length.should.equal(0);
          });
      });

      it("finds a feature that has both queried icons when iop=and", {
        auto feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
        feature["icons"] = `["000000000000000000000001","000000000000000000000003","000000000000000000000002"]`.parseJsonString;

        crates.feature.updateItem(feature);

        router
          .request
          .get("/features?icons=000000000000000000000001,000000000000000000000003&iop=and")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["features"].length.should.equal(1);
          });
      });
    });

    it("can sort by name", {
      router
        .request
        .get("/features?sortBy=name&sortOrder=desc")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["features"].length.should.equal(2);
          response.bodyJson.should.equal(parseJsonString(`{"features":[ ` ~ site3.to!string ~ `, ` ~ site1.to!string ~ ` ]}`));
        });
    });

    describe("the attributes parameter", {
      it("returns an empty list when the attributes does not match an feature", {
        router
          .request
          .get("/features?attributes=some|value")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["features"].byValue.map!(a => a["_id"].to!string).array.should.equal([]);
          });
      });

      it("returns an feature when it matches the attributes", {
        auto feature = crates.feature.getItem(publicFeatureId).and.exec.front;
        feature["attributes"] = `{ "some": "value" }`.parseJsonString;
        feature = crates.feature.updateItem(feature);

        router
          .request
          .get("/features?attributes=some|value")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["features"].byValue.map!(a => a["_id"].to!string).array.should.equal(["000000000000000000000001"]);
          });
      });
    });
  });
});
