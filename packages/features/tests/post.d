/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.features.post;

import tests.fixtures;
import ogm.calendar;
import ogm.features.api;

alias suite = Spec!({
  URLRouter router;

  describe("add new features", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupFeaturesApi(crates);

      auto date = Clock.currTime.toUTC;
      date.fracSecs = Duration.zero;

      auto calendarMock = new CalendarMock("2021-01-12T16:20:11Z");
      SysCalendar.instance = calendarMock;
    });

    describe("on maps with addFeaturesAsPending = true", {
      Json data;

      beforeEach({
        auto map = crates.map.getItem("000000000000000000000001").exec.front;
        map["addFeaturesAsPending"] = true;
        crates.map.updateItem(map);

        data = `{"feature": {
          "name": "new site",
          "position":{"coordinates":[1.5,1.5],"type":"Point"},
          "icons":["000000000000000000000001"],
          "description":{
            "blocks": [{
              "type": "paragraph",
              "data": {
                "text": "description of site1"
              }
            }]
          },
          "pictures":[],
          "sounds": [],
          "maps":["000000000000000000000001"]}}`.parseJsonString;
      });

      it("adds new features with pending visibility", {
        router
          .request
          .post("/features")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            Json expected = parseJsonString(`{ "feature": {
              "_id": "000000000000000000000005",
              "info": { "createdOn": "` ~ response.bodyJson["feature"]["info"]["createdOn"].to!string ~ `",
                "lastChangeOn": "` ~ response.bodyJson["feature"]["info"]["lastChangeOn"].to!string ~ `",
                "changeIndex": 0, "originalAuthor": "` ~ userId["administrator"] ~ `", "author": "` ~ userId["administrator"] ~ `" },
              "contributors": [ ],
              "maps": [ "000000000000000000000001" ],
              "name": "new site",
              "computedVisibility": {
                "isDefault": false,
                "isPublic": true,
                "team": "000000000000000000000001"
              },
              "visibility": -1,
              "attributes": {},
              "icons": [ "000000000000000000000001" ],
              "decorators": {
                "center": [],
                  "changeIndex": 0,
                "showAsLineAfterZoom": 99,
                "showLineMarkers": false,
                "useDefault": true,
                "keepWhenSmall": true,
                "changeIndex": 0,
                "minZoom": 0,
                "maxZoom": 20
              },
              "description": {
                "blocks": [{
                  "type": "paragraph",
                  "data": {
                    "text": "description of site1"
                  }
                }]
              },
              "isLarge": false,
              "source": {
                "type": "",
                "remoteId": "",
                "modelId": "",
                "syncAt": "0001-01-01T00:00:00+00:00"
              },
              "position": {
                "type": "Point",
                "coordinates": [ 1.5, 1.5 ]
              },
              "canEdit": true,
              "pictures": [],
              "sounds": []
            }}`);

            response.bodyJson.should.equal(expected);
          });
      });
    });

    describe("on public maps", {
      Json data;

      beforeEach({
        data = `{"feature": {
          "name": "new site",
          "position":{"coordinates":[1.5,1.5],"type":"Point"},
          "icons":["000000000000000000000001"],
          "description":"description of site1",
          "pictures":[],
          "sounds": [],
          "maps":["000000000000000000000001"]}}`.parseJsonString;
      });

      describeCredentialsRule("add new features on public maps", "yes", "features", (string type) {
        router
          .request
          .post("/features")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            Json expected = parseJsonString(`{ "feature": {
              "_id": "000000000000000000000005",
              "info": { "createdOn": "` ~ response.bodyJson["feature"]["info"]["createdOn"].to!string ~ `",
                "lastChangeOn": "` ~ response.bodyJson["feature"]["info"]["lastChangeOn"].to!string ~ `",
                "changeIndex": 0, "originalAuthor": "` ~ userId[type] ~ `", "author": "` ~ userId[type] ~ `" },
              "contributors": [ ],
              "maps": [ "000000000000000000000001" ],
              "name": "new site",
              "computedVisibility": {
                "isDefault": false,
                "isPublic": false,
                "team": "000000000000000000000001"
              },
              "visibility": 0,
              "attributes": {},
              "icons": [ "000000000000000000000001" ],
              "decorators": {
                "center": [],
                  "changeIndex": 0,
                "showAsLineAfterZoom": 99,
                "showLineMarkers": false,
                "useDefault": true,
                "keepWhenSmall": true,
                "changeIndex": 0,
                "minZoom": 0,
                "maxZoom": 20
              },
              "source": {
                "type": "",
                "remoteId": "",
                "modelId": "",
                "syncAt": "0001-01-01T00:00:00+00:00"
              },
              "description": {
                "blocks": [{
                  "type": "paragraph",
                  "data": {
                    "text": "description of site1"
                  }
                }]
              },
              "isLarge": false,
              "position": {
                "type": "Point",
                "coordinates": [ 1.5, 1.5 ]
              },
              "pictures": [],
              "sounds": []
            }}`);

            expected["feature"]["canEdit"] = true;

            response.bodyJson.should.equal(expected);
          });
      });

      describeCredentialsRule("add new features on public maps", "yes", "features", "administrator", {
        router
          .request
          .post("/features")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            Json expected = parseJsonString(`{ "feature": {
              "_id": "000000000000000000000005",
              "info": { "createdOn": "` ~ response.bodyJson["feature"]["info"]["createdOn"].to!string ~ `",
                "lastChangeOn": "` ~ response.bodyJson["feature"]["info"]["lastChangeOn"].to!string ~ `",
                "changeIndex": 0, "originalAuthor": "` ~ userId["administrator"] ~ `", "author": "` ~ userId["administrator"] ~ `" },
              "contributors": [ ],
              "maps": [ "000000000000000000000001" ],
              "name": "new site",
              "computedVisibility": {
                "isDefault": false,
                "isPublic": false,
                "team": "000000000000000000000001"
              },
              "visibility": 0,
              "attributes": {},
              "icons": [ "000000000000000000000001" ],
              "decorators": {
                "center": [],
                  "changeIndex": 0,
                "showAsLineAfterZoom": 99,
                "showLineMarkers": false,
                "useDefault": true,
                "keepWhenSmall": true,
                "changeIndex": 0,
                "minZoom": 0,
                "maxZoom": 20
              },
              "source": {
                "type": "",
                "remoteId": "",
                "modelId": "",
                "syncAt": "0001-01-01T00:00:00+00:00"
              },
              "description": {
                "blocks": [{
                  "type": "paragraph",
                  "data": {
                    "text": "description of site1"
                  }
                }]
              },
              "isLarge": false,
              "position": {
                "type": "Point",
                "coordinates": [ 1.5, 1.5 ]
              },
              "canEdit": true,
              "pictures": [],
              "sounds": []
            }}`);

            response.bodyJson.should.equal(expected);
          });
      });

      describeCredentialsRule("add new features on public maps", "yes", "features", "no rights", {
        router
          .request
          .post("/features")
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            Json expected = parseJsonString(`{ "feature": {
              "_id": "000000000000000000000005",
              "info": { "createdOn": "` ~ response.bodyJson["feature"]["info"]["createdOn"].to!string ~ `",
                "lastChangeOn": "` ~ response.bodyJson["feature"]["info"]["lastChangeOn"].to!string ~ `",
                "changeIndex": 0, "originalAuthor": "@anonymous", "author": "@anonymous" },
              "contributors": [ ],
              "maps": [ "000000000000000000000001" ],
              "name": "new site",
              "computedVisibility": {
                "isDefault": false,
                "isPublic": false,
                "team": "000000000000000000000001"
              },
              "visibility": 0,
              "attributes": {"name1": {}},
              "icons": [ "000000000000000000000001" ],
              "decorators": {
                "center": [],
                  "changeIndex": 0,
                "showAsLineAfterZoom": 99,
                "showLineMarkers": false,
                "useDefault": true,
                "keepWhenSmall": true,
                "changeIndex": 0,
                "minZoom": 0,
                "maxZoom": 20
              },
              "source": {
                "type": "",
                "remoteId": "",
                "modelId": "",
                "syncAt": "0001-01-01T00:00:00+00:00"
              },
              "description": {
                "blocks": [{
                  "type": "paragraph",
                  "data": {
                    "text": "description of site1"
                  }
                }]
              },
              "isLarge": false,
              "position": {
                "type": "Point",
                "coordinates": [ 1.5, 1.5 ]
              },
              "pictures": [],
              "sounds": []
            }}`);

            response.bodyJson.should.equal(expected);
          });
      });
    });

    describe("on private maps", {
      Json data;

      beforeEach({
        data = `{"feature": {
          "name": "new site",
          "position":{"coordinates":[1.5,1.5],"type":"Point"},
          "icons":["000000000000000000000001"],
          "description":{
            "blocks": [{
              "type": "paragraph",
              "data": {
                "text": "description of site1"
              }
            }]
          },
          "pictures":[],
          "sounds": [],
          "maps":["000000000000000000000002"]}}`.parseJsonString;
      });

      describeCredentialsRule("add new features on private maps", "yes", "features", (string type) {
        router
          .request
          .post("/features")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            Json expected = parseJsonString(`{ "feature": {
              "_id": "000000000000000000000005",
              "info": { "createdOn": "` ~ response.bodyJson["feature"]["info"]["createdOn"].to!string ~ `",
                "lastChangeOn": "` ~ response.bodyJson["feature"]["info"]["lastChangeOn"].to!string ~ `",
                "changeIndex": 0, "originalAuthor": "` ~ userId[type] ~ `", "author": "` ~ userId[type] ~ `" },
              "contributors": [ ],
              "maps": [ "000000000000000000000002" ],
              "name": "new site",
              "computedVisibility": {
                "isDefault": false,
                "isPublic": false,
                "team": "000000000000000000000002"
              },
              "visibility": 0,
              "attributes": {},
              "icons": [ "000000000000000000000001" ],
              "decorators": {
                "center": [],
                  "changeIndex": 0,
                "showAsLineAfterZoom": 99,
                "showLineMarkers": false,
                "useDefault": true,
                "keepWhenSmall": true,
                "changeIndex": 0,
                "minZoom": 0,
                "maxZoom": 20
              },
              "source": {
                "type": "",
                "remoteId": "",
                "modelId": "",
                "syncAt": "0001-01-01T00:00:00+00:00"
              },
              "description": {
                "blocks": [{
                  "type": "paragraph",
                  "data": {
                    "text": "description of site1"
                  }
                }]
              },
              "isLarge": false,
              "position": {
                "type": "Point",
                "coordinates": [ 1.5, 1.5 ]
              },
              "pictures": [],
              "sounds": []
            }}`);

            expected["feature"]["canEdit"] = true;

            response.bodyJson.should.equal(expected);
          });
      });

      describeCredentialsRule("add new features on private maps", "yes", "features", "administrator", {
        router
          .request
          .post("/features")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            Json expected = parseJsonString(`{ "feature": {
              "_id": "000000000000000000000005",
              "info": { "createdOn": "` ~ response.bodyJson["feature"]["info"]["createdOn"].to!string ~ `",
                "lastChangeOn": "` ~ response.bodyJson["feature"]["info"]["lastChangeOn"].to!string ~ `",
                "changeIndex": 0, "originalAuthor": "` ~ userId["administrator"] ~ `", "author": "` ~ userId["administrator"] ~ `" },
              "contributors": [ ],
              "maps": [ "000000000000000000000002" ],
              "name": "new site",
              "computedVisibility": {
                "isDefault": false,
                "isPublic": false,
                "team": "000000000000000000000002"
              },
              "visibility": 0,
              "attributes": {},
              "icons": [ "000000000000000000000001" ],
              "decorators": {
                "center": [],
                  "changeIndex": 0,
                "showAsLineAfterZoom": 99,
                "showLineMarkers": false,
                "useDefault": true,
                "keepWhenSmall": true,
                "changeIndex": 0,
                "minZoom": 0,
                "maxZoom": 20
              },
              "description": {
                "blocks": [{
                  "type": "paragraph",
                  "data": {
                    "text": "description of site1"
                  }
                }]
              },
              "source": {
                "type": "",
                "remoteId": "",
                "modelId": "",
                "syncAt": "0001-01-01T00:00:00+00:00"
              },
              "isLarge": false,
              "position": {
                "type": "Point",
                "coordinates": [ 1.5, 1.5 ]
              },
              "canEdit": true,
              "pictures": [],
              "sounds": []
            }}`);

            response.bodyJson.should.equal(expected);
          });
      });

      describeCredentialsRule("add new features on private maps", "no", "features", "no rights", {
        router
          .request
          .post("/features")
          .send(data)
          .expectStatusCode(404)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"errors": [{
              "description": "The map does not exist.",
              "status": 404,
              "title": "Crate not found"
            }]}`.parseJsonString);
        });
      });
    });

    describe("on maps that the user does not have access", {
      Json data;

      beforeEach({
        data = `{"feature": {
          "name": "new site",
          "position":{"coordinates":[ 1.5,1.5 ],"type":"Point"},
          "icons":["000000000000000000000001"],
          "description":{
            "blocks": [{
              "type": "paragraph",
              "data": {
                "text": "description of site1"
              }
            }]
          },
          "pictures":[],
          "sounds": [],
          "maps":["000000000000000000000004"]}}`.parseJsonString;
      });

      describeCredentialsRule("add new features on maps that the user does not have access", "no", "features", (string type) {
        router
            .request
            .post("/features")
            .header("Authorization", "Bearer " ~ bearerLeaderToken.name)
            .send(data)
            .expectStatusCode(404)
            .end((Response response) => () {
              response.bodyJson.should.equal(`{"errors": [{
                "description": "The map does not exist.",
                "status": 404,
                "title": "Crate not found"
              }]}`.parseJsonString);
            });
      });

      describeCredentialsRule("add new features on maps that the user does not have access", "yes", "features", "administrator", {
        router
          .request
          .post("/features")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            Json expected = parseJsonString(`{ "feature": {
              "_id": "000000000000000000000005",
              "info": { "createdOn": "` ~ response.bodyJson["feature"]["info"]["createdOn"].to!string ~ `",
                "lastChangeOn": "` ~ response.bodyJson["feature"]["info"]["lastChangeOn"].to!string ~ `",
                "changeIndex": 0, "originalAuthor": "` ~ userId["administrator"] ~ `", "author": "` ~ userId["administrator"] ~ `" },
              "contributors": [ ],
              "maps": [ "000000000000000000000004" ],
              "name": "new site",
              "computedVisibility": {
                "isDefault": false,
                "isPublic": false,
                "team": "000000000000000000000004"
              },
              "visibility": 0,
              "attributes": {},
              "icons": [ "000000000000000000000001" ],
              "decorators": {
                "center": [],
                  "changeIndex": 0,
                "showAsLineAfterZoom": 99,
                "showLineMarkers": false,
                "useDefault": true,
                "keepWhenSmall": true,
                "changeIndex": 0,
                "minZoom": 0,
                "maxZoom": 20
              },
              "description": {
                "blocks": [{
                  "type": "paragraph",
                  "data": {
                    "text": "description of site1"
                  }
                }]
              },
              "source": {
                "type": "",
                "remoteId": "",
                "modelId": "",
                "syncAt": "0001-01-01T00:00:00+00:00"
              },
              "isLarge": false,
              "position": {
                "type": "Point",
                "coordinates": [ 1.5, 1.5 ]
              },
              "canEdit": true,
              "pictures": [],
              "sounds": []
            }}`);

            response.bodyJson.should.equal(expected);
          });
      });

      describeCredentialsRule("add new features on maps that the user does not have access", "no", "features", "no rights", (string type) {
        router
            .request
            .post("/features")
            .send(data)
            .expectStatusCode(404)
            .end((Response response) => () {
              response.bodyJson.should.equal(`{"errors": [{
                "description": "The map does not exist.",
                "status": 404,
                "title": "Crate not found"
              }]}`.parseJsonString);
            });
      });
    });

    describe("using the gpx track format", {
      Json data;

      beforeEach({
        data = `{"feature": {
          "name": "new site",
          "position":{"type":"gpx.track", "value": ""},
          "icons":["000000000000000000000001"],
          "description":"description of site1",
          "pictures":[],
          "maps":["000000000000000000000001"]}}`.parseJsonString;

        data["feature"]["position"]["value"] = `<?xml version="1.0" encoding="UTF-8" standalone="no" ?>` ~
          `<gpx xmlns="http://www.topografix.com/GPX/1/1" version="1.1" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">` ~
          `<trk>` ~
          `<trkseg>`~
          `<trkpt lat="12.0000000000000" lon="11.0000000000000"/>` ~
          `<trkpt lat="14.0000000000000" lon="13.0000000000000"/>` ~
          `</trkseg>`~
          `<trkseg>`~
          `<trkpt lat="16.0000000000000" lon="15.0000000000000"/>` ~
          `<trkpt lat="18.0000000000000" lon="17.0000000000000"/>` ~
          `</trkseg>`~
          `</trk>` ~
          `</gpx>`;
      });

      describeCredentialsRule("add new features using the gpx track format", "yes", "features", (string type) {
        router
          .request
          .post("/features")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            Json expected = parseJsonString(`{ "feature": {
              "_id": "000000000000000000000005",
              "info": { "createdOn": "` ~ response.bodyJson["feature"]["info"]["createdOn"].to!string ~ `",
                "lastChangeOn": "` ~ response.bodyJson["feature"]["info"]["lastChangeOn"].to!string ~ `",
                "changeIndex": 0, "originalAuthor": "` ~ userId[type] ~ `", "author": "` ~ userId[type] ~ `" },
              "contributors": [ ],
              "maps": [ "000000000000000000000001" ],
              "name": "new site",
              "computedVisibility": {
                "isDefault": false,
                "isPublic": false,
                "team": "000000000000000000000001"
              },
              "visibility": 0,
              "attributes": {},
              "icons": [ "000000000000000000000001" ],
              "decorators": {
                "center": [],
                  "changeIndex": 0,
                "showAsLineAfterZoom": 99,
                "showLineMarkers": false,
                "useDefault": true,
                "keepWhenSmall": true,
                "changeIndex": 0,
                "minZoom": 0,
                "maxZoom": 20
              },
              "description": {
                "blocks": [{
                  "type": "paragraph",
                  "data": {
                    "text": "description of site1"
                  }
                }]
              },
              "source": {
                "type": "",
                "remoteId": "",
                "modelId": "",
                "syncAt": "0001-01-01T00:00:00+00:00"
              },
              "isLarge": false,
              "position": {
                "type": "MultiLineString",
                "coordinates": [[[ 11, 12 ], [ 13, 14 ]], [[15, 16], [17, 18]]]
              },
              "pictures": [],
              "sounds": []
            }}`);

            expected["feature"]["canEdit"] = true;

            response.bodyJson.should.equal(expected);
          });
      });

      describeCredentialsRule("add new features using the gpx track format", "yes", "features", "administrator", {
        router
          .request
          .post("/features")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            Json expected = parseJsonString(`{ "feature": {
              "_id": "000000000000000000000005",
              "info": { "createdOn": "` ~ response.bodyJson["feature"]["info"]["createdOn"].to!string ~ `",
                "lastChangeOn": "` ~ response.bodyJson["feature"]["info"]["lastChangeOn"].to!string ~ `",
                "changeIndex": 0, "originalAuthor": "` ~ userId["administrator"] ~ `", "author": "` ~ userId["administrator"] ~ `" },
              "contributors": [ ],
              "maps": [ "000000000000000000000001" ],
              "name": "new site",
              "computedVisibility": {
                "isDefault": false,
                "isPublic": false,
                "team": "000000000000000000000001"
              },
              "visibility": 0,
              "attributes": {},
              "icons": [ "000000000000000000000001" ],
              "decorators": {
                "center": [],
                  "changeIndex": 0,
                "showAsLineAfterZoom": 99,
                "showLineMarkers": false,
                "useDefault": true,
                "keepWhenSmall": true,
                "changeIndex": 0,
                "minZoom": 0,
                "maxZoom": 20
              },
              "description": {
                "blocks": [{
                  "type": "paragraph",
                  "data": {
                    "text": "description of site1"
                  }
                }]
              },
              "source": {
                "type": "",
                "remoteId": "",
                "modelId": "",
                "syncAt": "0001-01-01T00:00:00+00:00"
              },
              "isLarge": false,
              "position": {
                "type": "MultiLineString",
                "coordinates": [[[ 11, 12 ], [ 13, 14 ]], [[15, 16], [17, 18]]]
              },
              "canEdit": true,
              "pictures": [],
              "sounds": []
            }}`);

            response.bodyJson.should.equal(expected);
          });
      });

      describeCredentialsRule("add new features using the gpx track format", "yes", "features", "no rights", {
        router
          .request
          .post("/features")
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            Json expected = parseJsonString(`{ "feature": {
              "_id": "000000000000000000000005",
              "info": { "createdOn": "` ~ response.bodyJson["feature"]["info"]["createdOn"].to!string ~ `",
                "lastChangeOn": "` ~ response.bodyJson["feature"]["info"]["lastChangeOn"].to!string ~ `",
                "changeIndex": 0, "originalAuthor": "@anonymous", "author": "@anonymous" },
              "contributors": [ ],
              "maps": [ "000000000000000000000001" ],
              "name": "new site",
              "computedVisibility": {
                "isDefault": false,
                "isPublic": false,
                "team": "000000000000000000000001"
              },
              "visibility": 0,
              "attributes": {"name1": {}},
              "icons": [ "000000000000000000000001" ],
              "decorators": {
                "center": [],
                  "changeIndex": 0,
                "showAsLineAfterZoom": 99,
                "showLineMarkers": false,
                "useDefault": true,
                "keepWhenSmall": true,
                "changeIndex": 0,
                "minZoom": 0,
                "maxZoom": 20
              },
              "description": {
                "blocks": [{
                  "type": "paragraph",
                  "data": {
                    "text": "description of site1"
                  }
                }]
              },
              "source": {
                "type": "",
                "remoteId": "",
                "modelId": "",
                "syncAt": "0001-01-01T00:00:00+00:00"
              },
              "isLarge": false,
              "position": {
                "type": "MultiLineString",
                "coordinates": [[[ 11, 12 ], [ 13, 14 ]], [[15, 16], [17, 18]]]
              },
              "pictures": [],
              "sounds": []
            }}`);

            response.bodyJson.should.equal(expected);
          });
      });
    });

    describe("using the gpx route format", {
      Json data;

      beforeEach({
        data = `{"feature": {
          "name": "new site",
          "position":{"type":"gpx.route", "value": ""},
          "icons":["000000000000000000000001"],
          "description":"description of site1",
          "pictures":[],
          "maps":["000000000000000000000001"]}}`.parseJsonString;

        data["feature"]["position"]["value"] = `<?xml version="1.0" encoding="UTF-8" standalone="no" ?>` ~
          `<gpx xmlns="http://www.topografix.com/GPX/1/1" version="1.1" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">` ~
          `<rte>` ~
          `<rtept lat="12.0000000000000" lon="11.0000000000000"/>` ~
          `<rtept lat="14.0000000000000" lon="13.0000000000000"/>` ~
          `<rtept lat="16.0000000000000" lon="15.0000000000000"/>` ~
          `<rtept lat="18.0000000000000" lon="17.0000000000000"/>` ~
          `</rte>` ~
          `</gpx>`;
      });

      describeCredentialsRule("add new features using the gpx route format", "yes", "features", (string type) {
        router
          .request
          .post("/features")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            Json expected = parseJsonString(`{ "feature": {
              "_id": "000000000000000000000005",
              "canEdit": true,
              "info": { "createdOn": "` ~ response.bodyJson["feature"]["info"]["createdOn"].to!string ~ `",
                "lastChangeOn": "` ~ response.bodyJson["feature"]["info"]["lastChangeOn"].to!string ~ `",
                "changeIndex": 0, "originalAuthor": "` ~ userId[type] ~ `", "author": "` ~ userId[type] ~ `" },
              "contributors": [ ],
              "maps": [ "000000000000000000000001" ],
              "name": "new site",
              "computedVisibility": {
                "isDefault": false,
                "isPublic": false,
                "team": "000000000000000000000001"
              },
              "visibility": 0,
              "attributes": {},
              "icons": [ "000000000000000000000001" ],
              "decorators": {
                "center": [],
                  "changeIndex": 0,
                "showAsLineAfterZoom": 99,
                "showLineMarkers": false,
                "useDefault": true,
                "keepWhenSmall": true,
                "changeIndex": 0,
                "minZoom": 0,
                "maxZoom": 20
              },
              "description": {
                "blocks": [{
                  "type": "paragraph",
                  "data": {
                    "text": "description of site1"
                  }
                }]
              },
              "source": {
                "type": "",
                "remoteId": "",
                "modelId": "",
                "syncAt": "0001-01-01T00:00:00+00:00"
              },
              "isLarge": false,
              "position": {
                "type": "MultiLineString",
                "coordinates": [[[ 11, 12 ], [ 13, 14 ], [15, 16], [17, 18]]]
              },
              "pictures": [],
              "sounds": []
            }}`);

            if(type != "guest") {
              expected["feature"]["canEdit"] = true;
            }

            response.bodyJson.should.equal(expected);
          });
      });

      describeCredentialsRule("add new features using the gpx route format", "yes", "features", "administrator", {
        router
          .request
          .post("/features")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            Json expected = parseJsonString(`{ "feature": {
              "_id": "000000000000000000000005",
              "info": { "createdOn": "` ~ response.bodyJson["feature"]["info"]["createdOn"].to!string ~ `",
                "lastChangeOn": "` ~ response.bodyJson["feature"]["info"]["lastChangeOn"].to!string ~ `",
                "changeIndex": 0, "originalAuthor": "` ~ userId["administrator"] ~ `", "author": "` ~ userId["administrator"] ~ `" },
              "contributors": [ ],
              "maps": [ "000000000000000000000001" ],
              "name": "new site",
              "computedVisibility": {
                "isDefault": false,
                "isPublic": false,
                "team": "000000000000000000000001"
              },
              "visibility": 0,
              "attributes": {},
              "icons": [ "000000000000000000000001" ],
              "decorators": {
                "center": [],
                  "changeIndex": 0,
                "showAsLineAfterZoom": 99,
                "showLineMarkers": false,
                "useDefault": true,
                "keepWhenSmall": true,
                "changeIndex": 0,
                "minZoom": 0,
                "maxZoom": 20
              },
              "description": {
                "blocks": [{
                  "type": "paragraph",
                  "data": {
                    "text": "description of site1"
                  }
                }]
              },
              "source": {
                "type": "",
                "remoteId": "",
                "modelId": "",
                "syncAt": "0001-01-01T00:00:00+00:00"
              },
              "isLarge": false,
              "position": {
                "type": "MultiLineString",
                "coordinates": [[[ 11, 12 ], [ 13, 14 ], [15, 16], [17, 18]]]
              },
              "canEdit": true,
              "pictures": [],
              "sounds": []
            }}`);

            response.bodyJson.should.equal(expected);
          });
      });

      describeCredentialsRule("add new features using the gpx route format", "yes", "features", "no rights", {
        router
          .request
          .post("/features")
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            Json expected = parseJsonString(`{ "feature": {
              "_id": "000000000000000000000005",
              "info": { "createdOn": "` ~ response.bodyJson["feature"]["info"]["createdOn"].to!string ~ `",
                "lastChangeOn": "` ~ response.bodyJson["feature"]["info"]["lastChangeOn"].to!string ~ `",
                "changeIndex": 0, "originalAuthor": "@anonymous", "author": "@anonymous" },
              "contributors": [ ],
              "maps": [ "000000000000000000000001" ],
              "name": "new site",
              "computedVisibility": {
                "isDefault": false,
                "isPublic": false,
                "team": "000000000000000000000001"
              },
              "visibility": 0,
              "attributes": {"name1": {}},
              "icons": [ "000000000000000000000001" ],
              "decorators": {
                "center": [],
                  "changeIndex": 0,
                "showAsLineAfterZoom": 99,
                "showLineMarkers": false,
                "useDefault": true,
                "keepWhenSmall": true,
                "changeIndex": 0,
                "minZoom": 0,
                "maxZoom": 20
              },
              "source": {
                "type": "",
                "remoteId": "",
                "modelId": "",
                "syncAt": "0001-01-01T00:00:00+00:00"
              },
              "description": {
                "blocks": [{
                  "type": "paragraph",
                  "data": {
                    "text": "description of site1"
                  }
                }]
              },
              "isLarge": false,
              "position": {
                "type": "MultiLineString",
                "coordinates": [[[ 11, 12 ], [ 13, 14 ], [15, 16], [17, 18]]]
              },
              "pictures": [],
              "sounds": []
            }}`);

            response.bodyJson.should.equal(expected);
          });
      });
    });

    describe("using an icon with required attributes", {
      beforeEach({
        auto icon = crates.icon.getItem("000000000000000000000001").exec.front;
        icon["attributes"][0]["isRequired"] = true;
        icon["attributes"][1]["isRequired"] = true;
        crates.icon.updateItem(icon);
      });

      it("should not allow sites with missing required attributes", {
        auto data = `{"feature":{
        "name": "new line",
        "position":{"coordinates":[[1.5,1.5],[2,2],[3,3]],"type":"LineString"},
        "icons":["000000000000000000000001"],
        "description":"description of line1",
        "pictures":[],
        "attributes": {},
        "maps":["000000000000000000000001"]}}`.parseJsonString;

        router
          .request
          .post("/features")
          .send(data)
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"errors": [{
                "description": "The item can not be added. Required icon attribute phone is not set for name1.",
                "status": 400,
                "title": "Validation error"
              }]}`.parseJsonString);
          });
      });

      it("should not accept adding sites with empty required attributes", {
        auto data = `{"feature":{
        "name": "new line",
        "position":{"coordinates":[[1.5,1.5],[2,2],[3,3]],"type":"LineString"},
        "icons":["000000000000000000000001"],
        "description":"description of line1",
        "pictures":[],
        "attributes": { "name1": { "phone": "", "program": ""}},
        "maps":["000000000000000000000001"]}}`.parseJsonString;

        router
          .request
          .post("/features")
          .send(data)
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"errors": [{
                "description": "The item can not be added. Required icon attribute phone is not set for name1.",
                "status": 400,
                "title": "Validation error"
              }]}`.parseJsonString);
          });
      });

      it("should not accept adding sites with required attributes that have values spaces", {
        auto data = `{"feature":{
        "name": "new line",
        "position":{"coordinates":[[1.5,1.5],[2,2],[3,3]],"type":"LineString"},
        "icons":["000000000000000000000001"],
        "description":"description of line1",
        "pictures":[],
        "attributes": { "name1": { "phone": " ", "program": " "}},
        "maps":["000000000000000000000001"]}}`.parseJsonString;

        router
          .request
          .post("/features")
          .send(data)
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"errors": [{
                "description": "The item can not be added. Required icon attribute phone is not set for name1.",
                "status": 400,
                "title": "Validation error"
              }]}`.parseJsonString);
          });
      });

      it("should not accepts adding sites with one required attribute empty", {
        auto data = `{"feature":{
        "name": "new line",
        "position":{"coordinates":[[1.5,1.5],[2,2],[3,3]],"type":"LineString"},
        "icons":["000000000000000000000001"],
        "description":"description of line1",
        "pictures":[],
        "attributes": { "name1": { "phone": "12345", "program": ""}},
        "maps":["000000000000000000000001"]}}`.parseJsonString;

        router
          .request
          .post("/features")
          .send(data)
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"errors": [{
                "description": "The item can not be added. Required icon attribute program is not set for name1.",
                "status": 400,
                "title": "Validation error"
              }]}`.parseJsonString);
          });
      });

      it("should accept adding sites with provided required attributes", {
        auto data = `{"feature":{
        "name": "new line",
        "position":{"coordinates":[[1.5,1.5],[2,2],[3,3]],"type":"LineString"},
        "icons":["000000000000000000000001"],
        "description":"description of line1",
        "pictures":[],
        "attributes": { "name1": { "phone": "1234", "program": "Luni-Vineri:9-18"}},
        "maps":["000000000000000000000001"]}}`.parseJsonString;

        router
          .request
          .post("/features")
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal((`{ "feature": {
                "_id": "000000000000000000000005",
                "info": { "createdOn": "2021-01-12T16:20:11Z", "lastChangeOn": "2021-01-12T16:20:11Z",
                  "author": "@anonymous", "originalAuthor": "@anonymous",
                  "changeIndex": 0 },
                "contributors": [ ],
                "maps": [ "000000000000000000000001" ],
                "name": "new line",
                "computedVisibility": {
                  "isDefault": false,
                  "isPublic": false,
                  "team": "000000000000000000000001"
                },
                "visibility": 0,
                "icons": [ "000000000000000000000001" ],
                "decorators": {
                  "center": [],
                  "changeIndex": 0,
                  "showAsLineAfterZoom": 99,
                  "showLineMarkers": false,
                  "useDefault": true,
                  "keepWhenSmall": true,
                  "changeIndex": 0,
                  "minZoom": 0,
                  "maxZoom": 20
                },
                "description": {
                  "blocks": [{
                    "type": "paragraph",
                    "data": {
                      "text": "description of line1"
                    }
                  }]
                },
                "source": {
                  "type": "",
                  "remoteId": "",
                  "modelId": "",
                  "syncAt": "0001-01-01T00:00:00+00:00"
                },
                "attributes": {"name1": { "phone": "1234", "program": "Luni-Vineri:9-18"}},
                "isLarge": false,
                "position":{"coordinates":[[1.5,1.5],[2,2],[3,3]],"type":"LineString"},
                "pictures": [],
                "sounds": []
              }}`).parseJsonString);
          });
      });

      describe("in the allowMany case", {
        beforeEach({
          auto icon = crates.icon.getItem("000000000000000000000001").exec.front;
          icon["allowMany"] = true;
          crates.icon.updateItem(icon);
        });

        it("should not accept adding sites with empty required attributes", {
          auto data = `{"feature":{
          "name": "new line",
          "position":{"coordinates":[[1.5,1.5],[2,2],[3,3]],"type":"LineString"},
          "icons":["000000000000000000000001"],
          "description":"description of line1",
          "pictures":[],
          "attributes": { "name1": [{ "phone": "", "program": ""}, {}]},
          "maps":["000000000000000000000001"]}}`.parseJsonString;

          router
            .request
            .post("/features")
            .send(data)
            .expectStatusCode(400)
            .end((Response response) => () {
              response.bodyJson.should.equal(`{"errors": [{
                  "description": "The item can not be added. Required icon attribute phone is not set for name1.",
                  "status": 400,
                  "title": "Validation error"
                }]}`.parseJsonString);
            });
        });

        it("should accept adding sites with non-empty required attributes", {
          auto data = `{"feature":{
          "name": "new line",
          "position":{"coordinates":[[1.5,1.5],[2,2],[3,3]],"type":"LineString"},
          "icons":["000000000000000000000001"],
          "description":"description of line1",
          "pictures":[],
          "attributes": { "name1": [{ "phone": "1234", "program": "test"}, {"phone": "4324", "program": "test program"}]},
          "maps":["000000000000000000000001"]}}`.parseJsonString;

          router
            .request
            .post("/features")
            .send(data)
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal((`{ "feature": {
                  "_id": "000000000000000000000005",
                  "info": { "createdOn": "2021-01-12T16:20:11Z", "lastChangeOn": "2021-01-12T16:20:11Z",
                    "author": "@anonymous", "originalAuthor": "@anonymous",
                    "changeIndex": 0 },
                  "contributors": [ ],
                  "maps": [ "000000000000000000000001" ],
                  "name": "new line",
                  "computedVisibility": {
                    "isDefault": false,
                    "isPublic": false,
                    "team": "000000000000000000000001"
                  },
                  "visibility": 0,
                  "icons": [ "000000000000000000000001" ],
                  "decorators": {
                    "center": [],
                  "changeIndex": 0,
                    "showAsLineAfterZoom": 99,
                    "showLineMarkers": false,
                    "useDefault": true,
                    "keepWhenSmall": true,
                    "changeIndex": 0,
                    "minZoom": 0,
                    "maxZoom": 20
                  },
                  "source": {
                    "type": "",
                    "remoteId": "",
                    "modelId": "",
                    "syncAt": "0001-01-01T00:00:00+00:00"
                  },
                  "description": {
                  "blocks": [{
                    "type": "paragraph",
                    "data": {
                      "text": "description of line1"
                    }
                  }]
                },
                "attributes": { "name1": [{ "phone": "1234", "program": "test"}, {"phone": "4324", "program": "test program"}]},
                "isLarge": false,
                "position":{"coordinates":[[1.5,1.5],[2,2],[3,3]],"type":"LineString"},
                "pictures": [],
                "sounds": []
              }}`).parseJsonString);
            });
        });
      });
    });

    describe("without a token", {
      it("should allow adding anonymous lines", {
        auto data = `{"feature":{
          "name": "new line",
          "position":{"coordinates":[[1.5,1.5],[2,2],[3,3]],"type":"LineString"},
          "icons":["000000000000000000000001"],
          "description":"description of line1",
          "pictures":[],
          "maps":["000000000000000000000001"]}}`.parseJsonString;

        router
          .request
          .post("/features")
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            auto expected = parseJsonString(`{ "feature": {
              "_id": "000000000000000000000005",
              "contributors": [ ],
              "maps": [ "000000000000000000000001" ],
              "name": "new line",
              "computedVisibility": {
                "isDefault": false,
                "isPublic": false,
                "team": "000000000000000000000001"
              },
              "visibility": 0,
              "icons": [ "000000000000000000000001" ],
              "decorators": {
                "center": [],
                "changeIndex": 0,
                "showAsLineAfterZoom": 99,
                "showLineMarkers": false,
                "useDefault": true,
                "keepWhenSmall": true,
                "changeIndex": 0,
                "minZoom": 0,
                "maxZoom": 20
              },
              "source": {
                "type": "",
                "remoteId": "",
                "modelId": "",
                "syncAt": "0001-01-01T00:00:00+00:00"
              },
              "description": {
                "blocks": [{
                  "type": "paragraph",
                  "data": {
                    "text": "description of line1"
                  }
                }]
              },
              "attributes": {"name1": {}},
              "isLarge": false,
              "position":{"coordinates":[[1.5,1.5],[2,2],[3,3]],"type":"LineString"},
              "pictures": [],
              "sounds": []
            }}`);

            expected["feature"]["info"] = response.bodyJson["feature"]["info"];

            response.bodyJson.should.equal(expected);
          });
      });

      it("returns an error when there is no map", {
        auto data = `{"feature":{
          "name": "new site",
          "position":{"coordinates":[1.5,1.5],"type":"Point"},
          "icons":["000000000000000000000001"],
          "description":"description of site1",
          "pictures":[],
          "maps":[]}}`.parseJsonString;

        router
            .request
            .post("/features")
            .send(data)
            .expectStatusCode(400)
            .end((Response response) => () {
              response.bodyJson.should.equal((`{
                "errors": [{
                  "description": "You must set a map to the feature.",
                  "status": 400,
                  "title": "Validation error"
                }]
              }`).parseJsonString);
            });
      });

      it("should not accept visibility field with `1` value", {
        auto data = `{"feature":{
          "visibility": 1,
          "name": "new site",
          "position":{"coordinates":[1.5,1.5],"type":"Point"},
          "icons":["000000000000000000000001"],
          "description":"description of site1",
          "pictures":[],
          "maps":["000000000000000000000001"]}}`.parseJsonString;

        router
            .request
            .post("/features")
            .send(data)
            .expectStatusCode(400)
            .end((Response response) => () {
              response.bodyJson.should.equal(`{"errors": [{
                "description": "The item can not be unpublished by you.",
                "status": 400,
                "title": "Validation error"
              }]}`.parseJsonString);
            });
      });

      it("should not accept contributors field", {
        auto data = `{"feature":{
          "contributors": [],
          "name": "new site",
          "position":{"coordinates":[1.5,1.5],"type":"Point"},
          "icons":["000000000000000000000001"],
          "description":{
            "blocks": [{
              "type": "paragraph",
              "data": {
                "text": "description of site1"
              }
            }]
          },
          "pictures":[],
          "maps":["000000000000000000000001"]}}`.parseJsonString;

        router
            .request
            .post("/features")
            .send(data)
            .expectStatusCode(400)
            .end((Response response) => () {
              response.bodyJson.should.equal("{\"errors\": [{
                \"description\": \"You must not send the `feature.contributors` field.\",
                \"status\": 400,
                \"title\": \"Validation error\"
              }]}".parseJsonString);
            });
      });

      it("should not accept canEdit field", {
        auto data = `{"feature":{
          "canEdit": true,
          "name": "new site",
          "position":{"coordinates":[1.5,1.5],"type":"Point"},
          "icons":["000000000000000000000001"],
          "description": {
            "blocks": [{
              "type": "paragraph",
              "data": {
                "text": "description of site1"
              }
            }]
          },
          "pictures":[],
          "maps":["000000000000000000000001"]}}`.parseJsonString;

        router
            .request
            .post("/features")
            .send(data)
            .expectStatusCode(400)
            .end((Response response) => () {
              response.bodyJson.should.equal("{\"errors\": [{
                \"description\": \"You must not send the `feature.canEdit` field.\",
                \"status\": 400,
                \"title\": \"Validation error\"
              }]}".parseJsonString);
            });
      });

      it("should not accept issueCount field", {
        auto data = `{"feature":{
          "issueCount": 3,
          "name": "new site",
          "position":{"coordinates":[1.5,1.5],"type":"Point"},
          "icons":["000000000000000000000001"],
          "description":"description of site1",
          "pictures":[],
          "maps":["000000000000000000000001"]}}`.parseJsonString;

        router
            .request
            .post("/features")
            .send(data)
            .expectStatusCode(400)
            .end((Response response) => () {
              response.bodyJson.should.equal("{\"errors\": [{
                \"description\": \"You must not send the `feature.issueCount` field.\",
                \"status\": 400,
                \"title\": \"Validation error\"
              }]}".parseJsonString);
            });
      });
    });

    describe("with an admin token", {
      it("should accept visibility field", {
        auto data = `{"feature": {
          "name": "new site",
          "visibility": 1,
          "position":{"coordinates":[1.5,1.5],"type":"Point"},
          "icons":["000000000000000000000001"],
          "description":"description of site1",
          "pictures":[],
          "maps":["000000000000000000000001"]}}`.parseJsonString;

        router
            .request
            .post("/features")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .send(data)
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal((`{ "feature": {
                "info": { "createdOn": "` ~ response.bodyJson["feature"]["info"]["createdOn"].to!string ~ `",
                  "lastChangeOn": "` ~ response.bodyJson["feature"]["info"]["lastChangeOn"].to!string ~ `",
                  "changeIndex": 0, "originalAuthor": "000000000000000000000005", "author": "000000000000000000000005" },
                "contributors": [ ],
                "maps": [ "000000000000000000000001" ],
                "name": "new site",
                "_id": "000000000000000000000005",
                "computedVisibility": {
                  "isDefault": false,
                  "isPublic": true,
                  "team": "000000000000000000000001"
                },
                "visibility": 1,
                "canEdit": true,
                "attributes": {},
                "icons": [ "000000000000000000000001" ],
                "decorators": {
                  "center": [],
                  "changeIndex": 0,
                  "showAsLineAfterZoom": 99,
                  "showLineMarkers": false,
                  "useDefault": true,
                  "keepWhenSmall": true,
                  "changeIndex": 0,
                  "minZoom": 0,
                  "maxZoom": 20
                },
                "description": {
                  "blocks": [{
                    "type": "paragraph",
                    "data": {
                      "text": "description of site1"
                    }
                  }]
                },
                "source": {
                  "type": "",
                  "remoteId": "",
                  "modelId": "",
                  "syncAt": "0001-01-01T00:00:00+00:00"
                },
                "isLarge": false,
                "position": {
                  "type": "Point",
                  "coordinates": [ 1.5, 1.5 ]
                },
                "pictures": [],
                "sounds": []
              }}`).parseJsonString);
            });
      });

      it("should not accept contributors", {
        auto data = `{"feature": {
          "name": "new site",
          "contributors": [],
          "position":{"coordinates":[1.5,1.5],"type":"Point"},
          "icons":["000000000000000000000001"],
          "description":"description of site1",
          "pictures":[],
          "maps":["000000000000000000000001"]}}`.parseJsonString;

        router
            .request
            .post("/features")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .send(data)
            .expectStatusCode(400)
            .end((Response response) => () {
              response.bodyJson.should.equal("{\"errors\": [{
                \"description\": \"You must not send the `feature.contributors` field.\",
                \"status\": 400,
                \"title\": \"Validation error\"
              }]}".parseJsonString);
            });
      });

      it("should not accept canEdit", {
        auto data = `{"feature": {
          "name": "new site",
          "canEdit": true,
          "position":{"coordinates":[1.5,1.5],"type":"Point"},
          "icons":["000000000000000000000001"],
          "description":"description of site1",
          "pictures":[],
          "maps":["000000000000000000000001"]}}`.parseJsonString;

        router
            .request
            .post("/features")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .send(data)
            .expectStatusCode(400)
            .end((Response response) => () {
              response.bodyJson.should.equal("{\"errors\": [{
                \"description\": \"You must not send the `feature.canEdit` field.\",
                \"status\": 400,
                \"title\": \"Validation error\"
              }]}".parseJsonString);
            });
      });

      it("should not accept unmasked", {
        auto data = `{"feature": {
          "name": "new site",
          "position":{"coordinates":[1.5,1.5],"type":"Point"},
          "unmasked":{"coordinates":[1.5,1.5],"type":"Point"},
          "icons":["000000000000000000000001"],
          "description":"description of site1",
          "pictures":[],
          "maps":["000000000000000000000001"]}}`.parseJsonString;

        router
            .request
            .post("/features")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .send(data)
            .expectStatusCode(400)
            .end((Response response) => () {
              response.bodyJson.should.equal("{\"errors\": [{
                \"description\": \"You must not send the `feature.unmasked` field.\",
                \"status\": 400,
                \"title\": \"Validation error\"
              }]}".parseJsonString);
            });
      });

      it("should not accept issueCount", {
        auto data = `{"feature": {
          "name": "new site",
          "issueCount": 99,
          "position":{"coordinates":[1.5,1.5],"type":"Point"},
          "icons":["000000000000000000000001"],
          "description":"description of site1",
          "pictures":[],
          "maps":["000000000000000000000001"]}}`.parseJsonString;

        router
            .request
            .post("/features")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .send(data)
            .expectStatusCode(400)
            .end((Response response) => () {
              response.bodyJson.should.equal("{\"errors\": [{
                \"description\": \"You must not send the `feature.issueCount` field.\",
                \"status\": 400,
                \"title\": \"Validation error\"
              }]}".parseJsonString);
            });
      });
    });

    describe("with an owner token", {
      it("should not allow adding features to maps that the user does not have access", {
        auto data = `{"feature": {
          "name": "new site",
          "position":{"coordinates":[ 1.5,1.5 ],"type":"Point"},
          "icons":["000000000000000000000001"],
          "description":"description of site1",
          "pictures":[],
          "maps":["000000000000000000000004"]}}`.parseJsonString;

        router
            .request
            .post("/features")
            .header("Authorization", "Bearer " ~ bearerToken.name)
            .send(data)
            .expectStatusCode(404)
            .end((Response response) => () {
              response.bodyJson.should.equal(`{"errors": [{
                "description": "The map does not exist.",
                "status": 404,
                "title": "Crate not found"
              }]}`.parseJsonString);
            });
      });

      it("should accept visibility field", {
        auto data = `{"feature": {
          "name": "new site",
          "visibility": 1,
          "position":{"coordinates":[1.5,1.5],"type":"Point"},
          "icons":["000000000000000000000001"],
          "description":"description of site1",
          "pictures":[],
          "maps":["000000000000000000000001"]}}`.parseJsonString;

        router
            .request
            .post("/features")
            .header("Authorization", "Bearer " ~ bearerToken.name)
            .send(data)
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal((`{ "feature": {
                "info": {
                  "createdOn": "` ~ response.bodyJson["feature"]["info"]["createdOn"].to!string ~ `",
                  "lastChangeOn": "` ~ response.bodyJson["feature"]["info"]["lastChangeOn"].to!string ~ `",
                  "changeIndex": 0, "originalAuthor": "000000000000000000000004", "author": "000000000000000000000004" },
                "contributors": [ ],
                "maps": [ "000000000000000000000001" ],
                "name": "new site",
                "_id": "000000000000000000000005",
                "computedVisibility": {
                  "isDefault": false,
                  "isPublic": true,
                  "team": "000000000000000000000001"
                },
                "visibility": 1,
                "canEdit": true,
                "attributes": {},
                "icons": [ "000000000000000000000001" ],
                "decorators": {
                  "center": [],
                  "changeIndex": 0,
                  "showAsLineAfterZoom": 99,
                  "showLineMarkers": false,
                  "useDefault": true,
                  "keepWhenSmall": true,
                  "changeIndex": 0,
                  "minZoom": 0,
                  "maxZoom": 20
                },
                "source": {
                  "type": "",
                  "remoteId": "",
                  "modelId": "",
                  "syncAt": "0001-01-01T00:00:00+00:00"
                },
                "description": {
                  "blocks": [{
                    "type": "paragraph",
                    "data": {
                      "text": "description of site1"
                    }
                  }]
                },
                "source": {
                  "type": "",
                  "remoteId": "",
                  "modelId": "",
                  "syncAt": "0001-01-01T00:00:00+00:00"
                },
                "isLarge": false,
                "position": {
                  "type": "Point",
                  "coordinates": [ 1.5, 1.5 ]
                },
                "pictures": [],
                "sounds": []
              }}`).parseJsonString);
            });
      });

      it("should not accept canEdit", {
        auto data = `{"feature": {
          "name": "new site",
          "canEdit": true,
          "position":{"coordinates":[1.5,1.5],"type":"Point"},
          "icons":["000000000000000000000001"],
          "description":"description of site1",
          "pictures":[],
          "maps":["000000000000000000000001"]}}`.parseJsonString;

        router
            .request
            .post("/features")
            .header("Authorization", "Bearer " ~ bearerToken.name)
            .send(data)
            .expectStatusCode(400)
            .end((Response response) => () {
              response.bodyJson.should.equal("{\"errors\": [{
                \"description\": \"You must not send the `feature.canEdit` field.\",
                \"status\": 400,
                \"title\": \"Validation error\"
              }]}".parseJsonString);
            });
      });

      it("should not accept issueCount", {
        auto data = `{"feature": {
          "name": "new site",
          "issueCount": 99,
          "position":{"coordinates":[1.5,1.5],"type":"Point"},
          "icons":["000000000000000000000001"],
          "description":"description of site1",
          "pictures":[],
          "maps":["000000000000000000000001"]}}`.parseJsonString;

        router
            .request
            .post("/features")
            .header("Authorization", "Bearer " ~ bearerToken.name)
            .send(data)
            .expectStatusCode(400)
            .end((Response response) => () {
              response.bodyJson.should.equal("{\"errors\": [{
                \"description\": \"You must not send the `feature.issueCount` field.\",
                \"status\": 400,
                \"title\": \"Validation error\"
              }]}".parseJsonString);
            });
      });
    });

    describe("with a leader token", {
      it("should not allow adding features to maps that the user does not have access", {
        auto data = `{"feature": {
          "name": "new site",
          "position":{"coordinates":[ 1.5,1.5 ],"type":"Point"},
          "icons":["000000000000000000000001"],
          "description": {
            "blocks": [{
              "type": "paragraph",
              "data": {
                "text": "description of site5"
              }
            }]
          },
          "pictures":[],
          "maps":["000000000000000000000004"]}}`.parseJsonString;

        router
            .request
            .post("/features")
            .header("Authorization", "Bearer " ~ bearerLeaderToken.name)
            .send(data)
            .expectStatusCode(404)
            .end((Response response) => () {
              response.bodyJson.should.equal(`{"errors": [{
                "description": "The map does not exist.",
                "status": 404,
                "title": "Crate not found"
              }]}`.parseJsonString);
            });
      });

      it("should accept visibility field", {
        auto data = `{"feature": {
          "name": "new site",
          "visibility": 1,
          "position":{"coordinates":[1.5,1.5],"type":"Point"},
          "icons":["000000000000000000000001"],
          "description":{
            "blocks": [{
              "type": "paragraph",
              "data": {
                "text": "description of site5"
              }
            }]
          },
          "pictures":[],
          "maps":["000000000000000000000001"]}}`.parseJsonString;

        router
            .request
            .post("/features")
            .header("Authorization", "Bearer " ~ bearerLeaderToken.name)
            .send(data)
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal((`{ "feature": {
                "info": { "createdOn": "` ~ response.bodyJson["feature"]["info"]["createdOn"].to!string ~ `",
                  "lastChangeOn": "` ~ response.bodyJson["feature"]["info"]["lastChangeOn"].to!string ~ `",
                  "changeIndex": 0, "originalAuthor": "000000000000000000000001", "author": "000000000000000000000001" },
                "contributors": [ ],
                "maps": [ "000000000000000000000001" ],
                "name": "new site",
                "_id": "000000000000000000000005",
                "computedVisibility": {
                  "isDefault": false,
                  "isPublic": true,
                  "team": "000000000000000000000001"
                },
                "visibility": 1,
                "attributes": {},
                "canEdit": true,
                "icons": [ "000000000000000000000001" ],
                "decorators": {
                  "center": [],
                  "changeIndex": 0,
                  "showAsLineAfterZoom": 99,
                  "showLineMarkers": false,
                  "useDefault": true,
                  "keepWhenSmall": true,
                  "changeIndex": 0,
                  "minZoom": 0,
                  "maxZoom": 20
                },
                "description": {
                  "blocks": [{
                    "type": "paragraph",
                    "data": {
                      "text": "description of site5"
                    }
                  }]
                },
                "source": {
                  "type": "",
                  "remoteId": "",
                  "modelId": "",
                  "syncAt": "0001-01-01T00:00:00+00:00"
                },
                "isLarge": false,
                "position": {
                  "type": "Point",
                  "coordinates": [ 1.5, 1.5 ]
                },
                "pictures": [],
                "sounds": []
              }}`).parseJsonString);
            });
      });
    });

    describe("with a member token", {
      it("should allow adding features to a public map", {
        auto data = `{"feature": {
          "name": "new site",
          "position":{"coordinates":[1.5,1.5],"type":"Point"},
          "icons":["000000000000000000000001"],
          "description":"description of site1",
          "pictures":[],
          "maps":["000000000000000000000001"]}}`.parseJsonString;

        router
            .request
            .post("/features")
            .header("Authorization", "Bearer " ~ bearerMemberToken.name)
            .send(data)
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal((`{ "feature": {
                "info": { "createdOn": "` ~ response.bodyJson["feature"]["info"]["createdOn"].to!string ~ `",
                  "lastChangeOn": "` ~ response.bodyJson["feature"]["info"]["createdOn"].to!string ~ `",
                  "changeIndex": 0, "originalAuthor": "000000000000000000000002", "author": "000000000000000000000002" },
                "contributors": [ ],
                "maps": [ "000000000000000000000001" ],
                "name": "new site",
                "_id": "000000000000000000000005",
                "computedVisibility": {
                  "isDefault": false,
                  "isPublic": false,
                  "team": "000000000000000000000001"
                },
                "visibility": 0,
                "attributes": {},
                "canEdit": true,
                "icons": [ "000000000000000000000001" ],
                "decorators": {
                  "center": [],
                  "changeIndex": 0,
                  "showAsLineAfterZoom": 99,
                  "showLineMarkers": false,
                  "useDefault": true,
                  "keepWhenSmall": true,
                  "changeIndex": 0,
                  "minZoom": 0,
                  "maxZoom": 20
                },
                "description": {
                  "blocks": [{
                    "type": "paragraph",
                    "data": {
                      "text": "description of site1"
                    }
                  }]
                },
                "isLarge": false,
                "position": {
                  "type": "Point",
                  "coordinates": [ 1.5, 1.5 ]
                },
                "source": {
                  "type": "",
                  "remoteId": "",
                  "modelId": "",
                  "syncAt": "0001-01-01T00:00:00+00:00"
                },
                "pictures": [],
                "sounds": []
              }}`).parseJsonString);
            });
      });

      it("should allow adding features to a private map", {
        auto data = `{"feature": {
          "name": "new site",
          "position":{"coordinates":[1.5,1.5],"type":"Point"},
          "icons":["000000000000000000000001"],
          "description":"description of site1",
          "pictures":[],
          "maps":["000000000000000000000002"]}}`.parseJsonString;

        router
            .request
            .post("/features")
            .header("Authorization", "Bearer " ~ bearerMemberToken.name)
            .send(data)
            .expectStatusCode(200)
            .end((Response response) => () {
              auto expected = parseJsonString(`{ "feature": {
                "contributors": [ ],
                "maps": [ "000000000000000000000002" ],
                "name": "new site",
                "attributes": {},
                "_id": "000000000000000000000005",
                "computedVisibility": {
                  "isDefault": false,
                  "isPublic": false,
                  "team": "000000000000000000000002"
                },
                "visibility": 0,
                "canEdit": true,
                "icons": [ "000000000000000000000001" ],
                "decorators": {
                  "center": [],
                  "changeIndex": 0,
                  "showAsLineAfterZoom": 99,
                  "showLineMarkers": false,
                  "useDefault": true,
                  "keepWhenSmall": true,
                  "changeIndex": 0,
                  "minZoom": 0,
                  "maxZoom": 20
                },
                "description": {
                  "blocks": [{
                    "type": "paragraph",
                    "data": {
                      "text": "description of site1"
                    }
                  }]
                },
                "source": {
                  "type": "",
                  "remoteId": "",
                  "modelId": "",
                  "syncAt": "0001-01-01T00:00:00+00:00"
                },
                "isLarge": false,
                "position": {
                  "type": "Point",
                  "coordinates": [ 1.5, 1.5 ]
                },
                "pictures": [],
                "sounds": []
              }}`);

              expected["feature"]["info"] = response.bodyJson["feature"]["info"];
              response.bodyJson.should.equal(expected);
            });
      });

      it("should not allow adding features to maps that the user does not have access", {
        auto data = `{"feature": {
          "name": "new site",
          "position":{"coordinates":[ 1.5,1.5 ],"type":"Point"},
          "icons":["000000000000000000000001"],
          "description":"description of site1",
          "pictures":[],
          "maps":["000000000000000000000004"]}}`.parseJsonString;

        router
            .request
            .post("/features")
            .header("Authorization", "Bearer " ~ bearerMemberToken.name)
            .send(data)
            .expectStatusCode(404)
            .end((Response response) => () {
              response.bodyJson.should.equal(`{"errors": [{
                "description": "The map does not exist.",
                "status": 404,
                "title": "Crate not found"
              }]}`.parseJsonString);
            });
      });

      it("should accept visibility field", {
        auto data = `{"feature": {
          "name": "new site",
          "visibility": 1,
          "position":{"coordinates":[1.5, 1.5],"type":"Point"},
          "icons":["000000000000000000000001"],
          "description":{
            "blocks": [{
              "type": "paragraph",
              "data": {
                "text": "description of site5"
              }
            }]
          },
          "pictures":[],
          "maps":["000000000000000000000001"]}}`.parseJsonString;

        router
            .request
            .post("/features")
            .header("Authorization", "Bearer " ~ bearerMemberToken.name)
            .send(data)
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal((`{ "feature": {
                "info": { "createdOn": "` ~ response.bodyJson["feature"]["info"]["createdOn"].to!string ~ `",
                  "lastChangeOn": "` ~ response.bodyJson["feature"]["info"]["lastChangeOn"].to!string ~ `",
                  "changeIndex": 0, "originalAuthor": "000000000000000000000002", "author": "000000000000000000000002" },
                "contributors": [ ],
                "maps": [ "000000000000000000000001" ],
                "name": "new site",
                "_id": "000000000000000000000005",
                "computedVisibility": {
                  "isDefault": false,
                  "isPublic": true,
                  "team": "000000000000000000000001"
                },
                "visibility": 1,
                "attributes": {},
                "canEdit": true,
                "icons": [ "000000000000000000000001" ],
                "decorators": {
                  "center": [],
                  "changeIndex": 0,
                  "showAsLineAfterZoom": 99,
                  "showLineMarkers": false,
                  "useDefault": true,
                  "keepWhenSmall": true,
                  "minZoom": 0,
                  "maxZoom": 20
                },
                "description": {
                "blocks": [{
                    "type": "paragraph",
                    "data": {
                      "text": "description of site5"
                    }
                  }]
                },
                "source": {
                  "type": "",
                  "remoteId": "",
                  "modelId": "",
                  "syncAt": "0001-01-01T00:00:00+00:00"
                },
                "isLarge": false,
                "position": {
                  "type": "Point",
                  "coordinates": [ 1.5, 1.5 ]
                },
                "pictures": [],
                "sounds": []
              }}`).parseJsonString);
            });
      });
    });

    describe("with a guest token", {
      it("should not allow adding features to maps that the user does not have access", {
        auto data = `{"feature": {
          "name": "new site",
          "position":{"coordinates":[ 1.5,1.5 ],"type":"Point"},
          "icons":["000000000000000000000001"],
          "description":"description of site1",
          "pictures":[],
          "maps":["000000000000000000000004"]}}`.parseJsonString;

        router
            .request
            .post("/features")
            .header("Authorization", "Bearer " ~ bearerGuestToken.name)
            .send(data)
            .expectStatusCode(404)
            .end((Response response) => () {
              response.bodyJson.should.equal(`{"errors": [{
                "description": "The map does not exist.",
                "status": 404,
                "title": "Crate not found"
              }]}`.parseJsonString);
            });
      });

      it("should not accept visibility field", {
        auto data = `{"feature": {
          "name": "new site",
          "visibility": 1,
          "position":{"coordinates":[1.5,1.5],"type":"Point"},
          "icons":["000000000000000000000001"],
          "description":"description of site1",
          "pictures":[],
          "maps":["000000000000000000000001"]}}`.parseJsonString;

        router
            .request
            .post("/features")
            .header("Authorization", "Bearer " ~ bearerGuestToken.name)
            .send(data)
            .expectStatusCode(400)
            .end((Response response) => () {
              response.bodyJson.should.equal(`{"errors": [{
                "description": "The item can not be unpublished by you.",
                "status": 400,
                "title": "Validation error"
              }]}`.parseJsonString);
            });
      });

      it("should not accept contributors", {
        auto data = `{"feature": {
          "name": "new site",
          "contributors": [],
          "position":{"coordinates":[1.5,1.5],"type":"Point"},
          "icons":["000000000000000000000001"],
          "description":"description of site1",
          "pictures":[],
          "maps":["000000000000000000000001"]}}`.parseJsonString;

        router
            .request
            .post("/features")
            .header("Authorization", "Bearer " ~ bearerGuestToken.name)
            .send(data)
            .expectStatusCode(400)
            .end((Response response) => () {
              response.bodyJson.should.equal("{\"errors\": [{
                \"description\": \"You must not send the `feature.contributors` field.\",
                \"status\": 400,
                \"title\": \"Validation error\"
              }]}".parseJsonString);
            });
      });
    });

    describe("adding new sounds", {
      Json invalidSound;
      Json validSound;

      beforeEach({
        auto info = ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z"));
        invalidSound = createSound(Sound(ObjectId.fromString("1"), "name1", VisibilityOptional(Team(ObjectId.fromString("3")), true), info, new SoundFile));
        validSound = createSound(Sound(ObjectId.fromString("2"), "name2", VisibilityOptional(Team(ObjectId.fromString("1")), true), info, new SoundFile));
      });

      it("should not allow adding sounds from other teams to features that don't have access to any of the map", {
        auto data = `{"feature": {
          "name": "new site",
          "position":{"coordinates":[ 1.5,1.5 ],"type":"Point"},
          "icons":["000000000000000000000001"],
          "description":"description of site1",
          "pictures":[],
          "sounds": ["000000000000000000000002"],
          "maps":["000000000000000000000001"]}}`.parseJsonString;
        data["feature"]["sounds"] ~= invalidSound["_id"];

        router
          .request
          .post("/features")
          .header("Authorization", "Bearer " ~ bearerGuestToken.name)
          .send(data)
          .expectStatusCode(400)
          .end((Response response) => () {

            response.bodyJson.should.equal((`{"errors": [{
              "description": "The sound with id ` ~ "`" ~ invalidSound["_id"].to!string ~ "`" ~ ` can not be added to this feature.",
              "status": 400,
              "title": "Validation error"
            }]}`).parseJsonString);
          });
      });

      it("should allow adding sounds to features that have the first map ", {
        auto data = `{"feature": {
          "name": "new site",
          "position":{"coordinates":[ 1.5,1.5 ],"type":"Point"},
          "icons":["000000000000000000000001"],
          "description":"description of site1",
          "pictures":[],
          "sounds": ["000000000000000000000002"],
          "maps":["000000000000000000000001"]}}`.parseJsonString;

        router
          .request
          .post("/features")
          .header("Authorization", "Bearer " ~ bearerGuestToken.name)
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["feature"]["sounds"].should.equal(`["000000000000000000000002"]`.parseJsonString);
          });
      });
    });
  });

  describe("bulk publish", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupFeaturesApi(crates);

      auto feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
      feature["visibility"] = 0;
      crates.feature.updateItem(feature);
    });

    describeCredentialsRule("bulk publish features from owned maps", "yes", "features", ["administrator", "owner", "leader"], (string type) {
      router
        .request
        .post("/features/publish?map=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end((Response response) => () {
          auto feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
          feature["visibility"].to!int.should.equal(1);
        });
    });

    describeCredentialsRule("bulk publish features from owned maps", "no", "features", ["member", "guest"], (string type) {
      router
        .request
        .post("/features/publish?map=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end((Response response) => () {
          auto feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
          feature["visibility"].to!int.should.equal(0);
        });
    });

    describeCredentialsRule("bulk publish features from owned maps", "-", "features", "no rights", {
      router
        .request
        .post("/features/publish?map=000000000000000000000001")
        .expectStatusCode(401)
        .end((Response response) => () {
          auto feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
          feature["visibility"].to!int.should.equal(0);

          response.bodyJson.should.equal(`{
            "error": "Authorization required"
          }`.parseJsonString);
        });
    });

    it("returns an error when the the map query is not present", {
      router
        .request
        .post("/features/publish?map=")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(400)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{"errors": [{
            "description": "You must provide the ` ~ "`map`" ~ ` query parameter",
            "status": 400,
            "title": "Validation error"
          }]}`).parseJsonString);
        });
    });
  });

  describe("bulk pending", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupFeaturesApi(crates);

      auto feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
      feature["visibility"] = 0;
      crates.feature.updateItem(feature);
    });

    describeCredentialsRule("bulk pending features from owned maps", "yes", "features", ["administrator", "owner", "leader"], (string type) {
      router
        .request
        .post("/features/pending?map=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end((Response response) => () {
          auto feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
          feature["visibility"].to!int.should.equal(-1);
        });
    });

    describeCredentialsRule("bulk pending features from owned maps", "no", "features", ["member", "guest"], (string type) {
      router
        .request
        .post("/features/pending?map=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end((Response response) => () {
          auto feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
          feature["visibility"].to!int.should.equal(0);
        });
    });

    describeCredentialsRule("bulk pending features from owned maps", "-", "features", "no rights", {
      router
        .request
        .post("/features/pending?map=000000000000000000000001")
        .expectStatusCode(401)
        .end((Response response) => () {
          auto feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
          feature["visibility"].to!int.should.equal(0);

          response.bodyJson.should.equal(`{
            "error": "Authorization required"
          }`.parseJsonString);
        });
    });

    it("returns an error when the the map query is not present", {
      router
        .request
        .post("/features/pending?map=")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(400)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{"errors": [{
            "description": "You must provide the ` ~ "`map`" ~ ` query parameter",
            "status": 400,
            "title": "Validation error"
          }]}`).parseJsonString);
        });
    });
  });

  describe("bulk unpublish", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupFeaturesApi(crates);
    });

    it("returns a response for the OPTIONS method", {
        router
          .request
          .customMethod!(HTTPMethod.OPTIONS)("/features/unpublish?map=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(204)
            .end((Response response) => () {
              response.headers.serializeToJson.should.equal(`{
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, DELETE, PUT, PATCH, POST, OPTIONS",
                "Access-Control-Allow-Headers": "Content-Type, Authorization, If-None-Match"
              }`.parseJsonString);
              response.bodyString.should.equal(``);
            });
      });

    describeCredentialsRule("bulk unpublish features from owned maps", "yes", "features", ["administrator", "owner", "leader"], (string type) {
      router
        .request
        .post("/features/unpublish?map=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end((Response response) => () {
          auto feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
          feature["visibility"].to!int.should.equal(0);
        });
    });

    describeCredentialsRule("bulk unpublish features from owned maps", "no", "features", ["member", "guest"], (string type) {
      router
        .request
        .post("/features/unpublish?map=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end((Response response) => () {
          import std.stdio;
          writeln(response.bodyString);

          auto feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
          feature["visibility"].should.equal(1);
        });
    });

    describeCredentialsRule("bulk unpublish features from owned maps", "-", "features", "no rights", {
      router
        .request
        .post("/features/unpublish?map=000000000000000000000001")
        .expectStatusCode(401)
        .end((Response response) => () {
          auto feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
          feature["visibility"].to!int.should.equal(1);

          response.bodyJson.should.equal(`{
            "error": "Authorization required"
          }`.parseJsonString);
        });
    });
  });
});
