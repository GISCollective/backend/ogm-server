/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.features.get.list.admin;

import tests.fixtures;
import tests.data.features;
import vibe.http.common;
import geo.json;
import ogm.features.api;

alias suite = Spec!({
  URLRouter router;

  describe("when there is an admin token", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupFeaturesApi(crates);
    });

    it("should return features from all maps", {
      router
        .request
        .get("/features")
        .header("Authorization", "Bearer " ~ administratorToken.name)
        .expectStatusCode(200)
        .end((Response response) => () {
          auto features = (cast(Json[]) response.bodyJson["features"]);
          features.map!(a => a["_id"].to!string).array.should.containOnly([
            "000000000000000000000001",
            "000000000000000000000002",
            "000000000000000000000003",
            "000000000000000000000004" ]);

          features.map!(a => a["canEdit"].to!string).array.should.containOnly(["true", "true", "true", "true"]);
        });
    });

    it("should return sites from all maps when the 'edit' query string is present and 'all' is true", {
      router
        .request
        .get("/features?edit=true&all=true")
        .header("Authorization", "Bearer " ~ administratorToken.name)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["features"].length.should.equal(4);

          auto features = (cast(Json[]) response.bodyJson["features"]);
          features.map!(a => a["_id"].to!string).array.should.containOnly([
            "000000000000000000000001",
            "000000000000000000000002",
            "000000000000000000000003",
            "000000000000000000000004" ]);
          features.map!(a => a["canEdit"].to!string).array.should.containOnly(["true", "true", "true", "true"]);
        });
    });

    it("should return the user features when the 'edit' query string is present", {
      router
        .request
        .get("/features?edit=true")
        .header("Authorization", "Bearer " ~ administratorToken.name)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["features"].length.should.equal(0);

          auto features = (cast(Json[]) response.bodyJson["features"]);
          features.should.containOnly([ ]);
        });
    });

    it("should return editable features with a specific author" , {
      router
        .request
        .get("/features?author=000000000000000000000001&edit=true")
        .header("Authorization", "Bearer " ~ administratorToken.name)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["features"].length.should.equal(0);
        });
    });

    describe("with unpublished features", {
      beforeEach({
        createFeature(
          Feature(ObjectId.fromString("5"),
            [ Map(ObjectId.fromString("1")) ],
            "site5",
            `{
              "blocks": [{
                "type": "paragraph",
                "data": {
                  "text": "description of site5"
                }
              }]
            }`.parseJsonString,
            GeoJsonGeometry.fromJson(`{ "type": "Point", "coordinates": [1.5, 1.5] }`.parseJsonString),
            ModelInfo(SysTime.fromISOExtString("2010-01-01T00:00:00Z")),
            [],
            [],
            [],
            VisibilityEnum.Private));

          createFeature(
            Feature(ObjectId.fromString("6"),
              [ Map(ObjectId.fromString("3")) ],
              "site6",
              `{
                "blocks": [{
                  "type": "paragraph",
                  "data": {
                    "text": "description of site6"
                  }
                }]
              }`.parseJsonString,
              GeoJsonGeometry.fromJson(`{ "type": "Point", "coordinates": [1.5, 1.5] }`.parseJsonString),
              ModelInfo(SysTime.fromISOExtString("2010-01-01T00:00:00Z")),
              [],
              [],
              [],
              VisibilityEnum.Private));
      });

      it("should return the unpublished features", {
        router
          .request
          .get("/features")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["features"].type.should.equal(Json.Type.array);
            (cast(Json[]) response.bodyJson["features"])
              .map!(a => a["_id"].to!string)
              .should
              .contain("000000000000000000000005");
          });
      });
    });

    describe("when there is an open issue for a feature", {
      beforeEach(function() {
        createIssue(Issue(ObjectId.fromString("000000000000000000000001"), "000000000000000000000001"));
      });

      it("should set the issue count to 1", {
        router
          .request
          .get("/features/000000000000000000000001")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["feature"]["issueCount"].should.equal(1);
          });
      });
    });

    describe("when there is an resolved issue for a site", {
      beforeEach(function() {
        auto issue = Issue(ObjectId.fromString("000000000000000000000001"), "000000000000000000000001");
        issue.status = Issue.Status.resolved;
        createIssue(issue);
      });

      it("should not set the issue count", {
        router
          .request
          .get("/features/000000000000000000000001")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["feature"]["issueCount"].should.not.equal(1);
          });
      });
    });
  });
});
