/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.features.cors;

import tests.fixtures;
import vibe.http.common;
import ogm.features.api;

alias suite = Spec!({
  URLRouter router;

  describe("For the features list", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupFeaturesApi(crates);
    });

    it("should return the right CORS", {
      router
        .request
        .customMethod!(HTTPMethod.OPTIONS)("/features")
        .expectHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, OPTIONS")
        .expectStatusCode(204)
        .end;
    });
  });

  describe("For features", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupFeaturesApi(crates);
    });

    it("should return the right CORS", {
      router
        .request
        .customMethod!(HTTPMethod.OPTIONS)("/features/000000000000000000000001")
        .expectHeader("Access-Control-Allow-Methods", "GET, DELETE, PUT, PATCH, OPTIONS")
        .expectStatusCode(204)
        .end;
    });
  });
});
