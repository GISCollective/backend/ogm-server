/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.features.delete_;

import tests.fixtures;
import geo.json;
import ogm.features.api;

alias suite = Spec!({
  URLRouter router;
  Json site5;

  describe("delete features", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupFeaturesApi(crates);

      site5 = Json.emptyObject;
      site5["feature"] = createFeature(
        Feature(ObjectId.fromString("5"),
            [ Map(ObjectId.fromString("1")) ],
            "site5",
            Json("description of site5"),
            GeoJsonGeometry.fromJson(`{ "type": "Point", "coordinates": [1.5, 1.5] }`.parseJsonString),
            ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z")),
            [], [], [], VisibilityEnum.Private, ["000000000000000000000002"]));

      site5["feature"].remove("issueCount");
    });

    describeCredentialsRule("delete features from owned maps", "yes", "features", ["administrator", "owner", "leader"], (string type) {
      router
        .request
        .delete_("/features/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end((Response response) => () {
          crates.feature.get.size.should.equal(4);
        });
    });

    describeCredentialsRule("delete features from owned maps", "no", "features", ["member", "guest"], (string type) {
      router
        .request
        .delete_("/features/000000000000000000000005")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          crates.feature.get.size.should.equal(5);
          response.bodyJson.should.equal("{\"errors\": [{
              \"description\": \"You don't have enough rights to remove `000000000000000000000005`.\",
              \"status\": 403,
              \"title\": \"Forbidden\"
            }]}".parseJsonString);
        });
    });

    describeCredentialsRule("delete features from owned maps", "no", "features", "no rights", {
      router
        .request
        .delete_("/features/000000000000000000000001")
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "error": "Authorization required"
          }`.parseJsonString);
        });
    });

    describeCredentialsRule("delete features from other maps", "yes", "features", ["administrator"], (string type) {
      router
        .request
        .delete_("/features/000000000000000000000004")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end((Response response) => () {
          crates.feature.get.size.should.equal(4);
        });
    });

    describeCredentialsRule("delete features from other maps", "no", "features", ["owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .delete_("/features/000000000000000000000004")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(404)
        .end((Response response) => () {
          crates.feature.get.size.should.equal(5);
          response.bodyJson.should.equal("{\"errors\": [{
              \"description\": \"Item `000000000000000000000004` not found.\",
              \"status\": 404,
              \"title\": \"Crate not found\"
            }]}".parseJsonString);
        });
    });

    describeCredentialsRule("delete features from other maps", "-", "features", "no rights", {
      router
        .request
        .delete_("/features/000000000000000000000004")
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "error": "Authorization required"
          }`.parseJsonString);
        });
    });

    describe("deleting features with pictures", {
      Json validPicture;
      Picture picture;

      beforeEach({
        auto info = ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z"));
        picture = Picture(ObjectId.fromString("99"), "name1", "", new PictureFile());
        validPicture = createPicture(picture);

        createFeature(
          Feature(ObjectId.fromString("6"),
          [ Map(ObjectId.fromString("1")) ],
          "site5",
          Json("description of site5"),
          GeoJsonGeometry.fromJson(`{ "type": "Point", "coordinates": [1.5, 1.5] }`.parseJsonString),
          ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z")),
          [picture], [], [], VisibilityEnum.Private, ["000000000000000000000002"]));
      });

      it("should delete the related picture with the feature", {
        router
          .request
          .delete_("/features/000000000000000000000006")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(204)
          .end((Response response) => () {
            crates.feature.get.where("_id").equal("000000000000000000000006").and.size.should.equal(0);
            crates.picture.get.size.should.equal(2);
            crates.picture.get.where("_id").equal("000000000000000000000099").and.size.should.equal(0);
          });
      });

      it("should not delete the related picture with the feature when is attached to 2 features", {
        createFeature(
          Feature(ObjectId.fromString("7"),
          [ Map(ObjectId.fromString("1")) ],
          "site5",
          Json("description of site5"),
          GeoJsonGeometry.fromJson(`{ "type": "Point", "coordinates": [1.5, 1.5] }`.parseJsonString),
          ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z")),
          [picture], [], [], VisibilityEnum.Private, ["000000000000000000000002"]));

        router
          .request
          .delete_("/features/000000000000000000000006")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(204)
          .end((Response response) => () {
            crates.feature.get.where("_id").equal("000000000000000000000006").and.size.should.equal(0);
            crates.picture.get.size.should.equal(3);
            crates.picture.get.where("_id").equal("000000000000000000000099").and.size.should.equal(1);
          });
      });
    });

    describe("deleting features with sounds", {
      Json validSound;
      Sound sound;

      beforeEach({
        auto info = ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z"));
        sound = Sound(ObjectId.fromString("1"), "name1", VisibilityOptional(Team(ObjectId.fromString("1")), true), info, new SoundFile);
        validSound = createSound(sound);

        createSound(Sound(ObjectId.fromString("2"), "name", VisibilityOptional(Team(ObjectId.fromString("1")), true), info, new SoundFile));

        createFeature(
          Feature(ObjectId.fromString("6"),
          [ Map(ObjectId.fromString("1")) ],
          "site5",
          Json("description of site5"),
          GeoJsonGeometry.fromJson(`{ "type": "Point", "coordinates": [1.5, 1.5] }`.parseJsonString),
          ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z")),
          [], [sound], [], VisibilityEnum.Private, ["000000000000000000000002"]));
      });

      it("should delete the related sounds with the feature", {
        router
          .request
          .delete_("/features/000000000000000000000006")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(204)
          .end((Response response) => () {
            crates.feature.get.where("_id").equal("000000000000000000000006").and.size.should.equal(0);
            crates.sound.get.size.should.equal(1);
            crates.sound.get.where("_id").equal("000000000000000000000001").and.size.should.equal(0);
          });
      });

      it("should not delete the related sounds with the feature when they are attached to two features", {
         createFeature(
          Feature(ObjectId.fromString("7"),
          [ Map(ObjectId.fromString("1")) ],
          "site5",
          Json("description of site5"),
          GeoJsonGeometry.fromJson(`{ "type": "Point", "coordinates": [1.5, 1.5] }`.parseJsonString),
          ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z")),
          [], [sound], [], VisibilityEnum.Private, ["000000000000000000000002"]));

        router
          .request
          .delete_("/features/000000000000000000000006")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(204)
          .end((Response response) => () {
            crates.feature.get.where("_id").equal("000000000000000000000006").and.size.should.equal(0);
            crates.sound.get.size.should.equal(2);
            crates.sound.get.where("_id").equal("000000000000000000000001").and.size.should.equal(1);
          });
      });
    });

    describe("deleting features without sounds", {
      beforeEach({
        auto feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
        feature.remove("sounds");
        crates.feature.updateItem(feature);
      });

      it("should allow deleting the feature", {
        router
          .request
          .delete_("/features/000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(204)
          .end((Response response) => () {
            crates.feature.get.where("_id").equal("000000000000000000000001").and.size.should.equal(0);
          });
      });
    });

    describe("bulk delete", {
      beforeEach({
        auto info = ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z"));
        auto sound = Sound(ObjectId.fromString("1"), "name1", VisibilityOptional(Team(ObjectId.fromString("1")), true), info, new SoundFile);
        auto validSound = createSound(sound);

        createSound(Sound(ObjectId.fromString("2"), "name", VisibilityOptional(Team(ObjectId.fromString("1")), true), info, new SoundFile));

        createFeature(
          Feature(ObjectId.fromString("6"),
          [ Map(ObjectId.fromString("1")) ],
          "site6",
          Json("description of site6"),
          GeoJsonGeometry.fromJson(`{ "type": "Point", "coordinates": [1.5, 1.5] }`.parseJsonString),
          ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z")),
          [], [sound], [], VisibilityEnum.Private, ["000000000000000000000002"]));
      });

      it("returns a response for the OPTIONS method", {
        router
          .request
          .customMethod!(HTTPMethod.OPTIONS)("/features")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(204)
            .end((Response response) => () {
              response.headers.serializeToJson.should.equal(`{
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, DELETE, OPTIONS",
                "Access-Control-Allow-Headers": "Content-Type, Authorization, If-None-Match"
              }`.parseJsonString);
              response.bodyString.should.equal(``);
            });
      });

      it("returns an error when the the map query is not present", {
        router
          .request
          .delete_("/features")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(400)
          .end((Response response) => () {
            crates.feature.get.size.should.equal(6);

            response.bodyJson.should.equal((`{"errors": [{
              "description": "You must provide the ` ~ "`map`" ~ ` query parameter",
              "status": 400,
              "title": "Validation error"
            }]}`).parseJsonString);
          });
      });

      it("deletes the sounds related to features", {
        router
          .request
          .delete_("/features?map=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(204)
          .end((Response response) => () {
            crates.feature.get.where("_id").equal("000000000000000000000006").and.size.should.equal(0);
            crates.sound.get.size.should.equal(1);
            crates.sound.get.where("_id").equal("000000000000000000000001").and.size.should.equal(0);
            crates.sound.get.where("_id").equal("000000000000000000000002").and.size.should.equal(1);
          });
      });

      it("does not delete sounds when the features has none attached", {
        router
          .request
          .delete_("/features?map=000000000000000000000003")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(204)
          .end((Response response) => () {
            crates.feature.get.where("_id").equal("000000000000000000000003").and.size.should.equal(0);
            crates.sound.get.size.should.equal(2);
            crates.sound.get.where("_id").equal("000000000000000000000001").and.size.should.equal(1);
            crates.sound.get.where("_id").equal("000000000000000000000002").and.size.should.equal(1);
          });
      });

      describeCredentialsRule("bulk delete features from owned maps", "yes", "features", ["administrator", "owner", "leader"], (string type) {
        router
          .request
          .delete_("/features?map=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(204)
          .end((Response response) => () {
            crates.feature.get.size.should.equal(3);
          });
      });

      describeCredentialsRule("bulk delete features from owned maps", "no", "features", ["member", "guest"], (string type) {
        router
          .request
          .delete_("/features?map=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(204)
          .end((Response response) => () {
            crates.feature.get.size.should.equal(6);
          });
      });

      describeCredentialsRule("bulk delete features from owned maps", "-", "features", "no rights", {
        router
          .request
          .delete_("/features?map=000000000000000000000001")
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "error": "Authorization required"
            }`.parseJsonString);
          });
      });
    });
  });
});
