/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.features.formats;

import tests.fixtures;
import tests.data.features;
import vibe.http.common;
import ogm.features.api;

alias suite = Spec!({
  URLRouter router;

  describe("using the format query parameter", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupFeaturesApi(crates);
    });

    describe("when there is feature with a source with author data", {
      beforeEach({
        Campaign survey;
        survey.contributorQuestions = [
          Question(),
          Question(),
          Question(),
          Question()
        ];

        survey.map.isEnabled = true;
        survey.map.map = "000000000000000000000001";
        survey.contributorQuestions[0].name = "field1";
        survey.contributorQuestions[1].name = "field2";
        survey.contributorQuestions[2].name = "field3";
        survey.contributorQuestions[3].name = "field4";

        crates.campaign.addItem(survey.serializeToJson);

        CampaignAnswer answer1;
        answer1.contributor["field1"] = "value 1";
        answer1.contributor["field2"] = "value 2";

        crates.campaignAnswer.addItem(answer1.serializeToJson);

        CampaignAnswer answer2;
        answer2.contributor["field1"] = "value 1";
        answer2.contributor["field3"] = "value 3";

        crates.campaignAnswer.addItem(answer2.serializeToJson);

        auto feature1 = crates.feature.getItem("000000000000000000000001").exec.front.clone;
        feature1["source"] = `{
          "type": "Campaign",
          "remoteId": "000000000000000000000001",
          "modelId": "000000000000000000000001",
          "syncAt": "2024-12-20T19:00:10Z"
        }`.parseJsonString;

        crates.feature.updateItem(feature1);

        auto feature2 = crates.feature.getItem("000000000000000000000002").exec.front.clone;
        feature2["maps"] = ["000000000000000000000001"].serializeToJson;
        feature2["source"] = `{
          "type": "Campaign",
          "remoteId": "000000000000000000000002",
          "modelId": "000000000000000000000001",
          "syncAt": "2024-12-20T19:00:10Z"
        }`.parseJsonString;

        crates.feature.updateItem(feature2);
      });

      it("adds the author fields to the csv to all team members", {
        router
          .request
          .get("/features?format=csv&map=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList["owner"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyString.should.equal(`_id,maps,maps.name,name,description,position.lon,position.lat,position.wkt,position.geoJson,pictures,pictures.name,pictures.url,sounds,sounds.name,sounds.url,icons,icons.name,visibility,contributors,source.type,source.modelId,source.remoteId,source.syncAt,computedVisibility.isPublic,computedVisibility.isDefault,attributes.position details.type,attributes.position details.altitudeAccuracy,attributes.position details.altitude,attributes.position details.accuracy,attributes.source.answer,attributes.source.type,attributes.source.campaign,createdOn,lastChangeOn,changeIndex,author.email,originalAuthor.email,attributes.contributor.field1,attributes.contributor.field2,attributes.contributor.field3,attributes.contributor.field4` ~ "\n" ~
              `000000000000000000000001,000000000000000000000001,map1,site1,description of site1,1.5,1.5,POINT(1.5 1.5),"{""type"":""Point"",""coordinates"":[1.5,1.5]}",,,,,,,000000000000000000000001,name1,1,,Campaign,000000000000000000000001,000000000000000000000001,2024-12-20T19:00:10Z,true,false,,,,,,,,2015-01-01T00:00:00Z,2015-01-01T00:00:00Z,0,,000000000000000000000001,value 1,value 2,,` ~ "\n" ~
              `000000000000000000000002,000000000000000000000001,map1,site2,description of site2,1.6,1.6,POINT(1.6 1.6),"{""type"":""Point"",""coordinates"":[1.6,1.6]}",,,,,,,000000000000000000000001;000000000000000000000002,name1;name2,0,,Campaign,000000000000000000000001,000000000000000000000002,2024-12-20T19:00:10Z,false,false,,,,,,,,2015-01-01T00:00:00Z,2015-01-01T00:00:00Z,0,,,value 1,,value 3,` ~ "\n");
          });
      });

      it("does not add the author fields to other team members", {
        router
          .request
          .get("/features?format=csv&map=000000000000000000000001")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyString.should.equal(`_id,maps,maps.name,name,description,position.lon,position.lat,position.wkt,position.geoJson,pictures,pictures.name,pictures.url,sounds,sounds.name,sounds.url,icons,icons.name,visibility,contributors,source.type,source.modelId,source.remoteId,source.syncAt,computedVisibility.isPublic,computedVisibility.isDefault,attributes.position details.type,attributes.position details.altitudeAccuracy,attributes.position details.altitude,attributes.position details.accuracy,attributes.source.answer,attributes.source.type,attributes.source.campaign,createdOn,lastChangeOn,changeIndex,author.email,originalAuthor.email` ~ "\n" ~
              `000000000000000000000001,000000000000000000000001,map1,site1,description of site1,1.5,1.5,POINT(1.5 1.5),"{""type"":""Point"",""coordinates"":[1.5,1.5]}",,,,,,,000000000000000000000001,name1,1,,Campaign,000000000000000000000001,000000000000000000000001,2024-12-20T19:00:10Z,true,false,,,,,,,,2015-01-01T00:00:00Z,2015-01-01T00:00:00Z,0,,000000000000000000000001` ~ "\n");
          });
      });
    });

    describe("to get a csv file", {
      describeCredentialsRule("download list as csv", "yes", "features", (string type) {
        router
          .request
          .get("/features?format=csv")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyString.should.equal(`_id,maps,maps.name,name,description,position.lon,position.lat,position.wkt,position.geoJson,pictures,pictures.name,pictures.url,sounds,sounds.name,sounds.url,icons,icons.name,visibility,contributors,source.type,source.modelId,source.remoteId,source.syncAt,computedVisibility.isPublic,computedVisibility.isDefault,attributes.position details.type,attributes.position details.altitudeAccuracy,attributes.position details.altitude,attributes.position details.accuracy,attributes.source.answer,attributes.source.type,attributes.source.campaign,createdOn,lastChangeOn,changeIndex,author.email,originalAuthor.email` ~ "\n" ~
              `000000000000000000000001,000000000000000000000001,map1,site1,description of site1,1.5,1.5,POINT(1.5 1.5),"{""type"":""Point"",""coordinates"":[1.5,1.5]}",,,,,,,000000000000000000000001,name1,1,,,,,0001-01-01T00:00:00+00:00,true,false,,,,,,,,2015-01-01T00:00:00Z,2015-01-01T00:00:00Z,0,,000000000000000000000001` ~ "\n" ~
              `000000000000000000000002,000000000000000000000002,map2,site2,description of site2,1.6,1.6,POINT(1.6 1.6),"{""type"":""Point"",""coordinates"":[1.6,1.6]}",,,,,,,000000000000000000000001;000000000000000000000002,name1;name2,0,,,,,0001-01-01T00:00:00+00:00,false,false,,,,,,,,2015-01-01T00:00:00Z,2015-01-01T00:00:00Z,0,,` ~ "\n" ~
              `000000000000000000000003,000000000000000000000003,map3,site3,description of site3,1.7,1.7,POINT(1.7 1.7),"{""type"":""Point"",""coordinates"":[1.7,1.7]}",,,,,,,000000000000000000000001;000000000000000000000002,name1;name2,1,,,,,0001-01-01T00:00:00+00:00,true,false,,,,,,,,2015-01-01T00:00:00Z,2015-01-01T00:00:00Z,0,,` ~ "\n");
          });
      });

      describeCredentialsRule("download list as csv", "yes", "features", "administrator", {
        router
          .request
          .get("/features?format=csv")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyString.should.equal(`_id,maps,maps.name,name,description,position.lon,position.lat,position.wkt,position.geoJson,pictures,pictures.name,pictures.url,sounds,sounds.name,sounds.url,icons,icons.name,visibility,contributors,source.type,source.modelId,source.remoteId,source.syncAt,computedVisibility.isPublic,computedVisibility.isDefault,attributes.position details.type,attributes.position details.altitudeAccuracy,attributes.position details.altitude,attributes.position details.accuracy,attributes.source.answer,attributes.source.type,attributes.source.campaign,createdOn,lastChangeOn,changeIndex,author.email,originalAuthor.email` ~ "\n" ~
              `000000000000000000000001,000000000000000000000001,map1,site1,description of site1,1.5,1.5,POINT(1.5 1.5),"{""type"":""Point"",""coordinates"":[1.5,1.5]}",,,,,,,000000000000000000000001,name1,1,,,,,0001-01-01T00:00:00+00:00,true,false,,,,,,,,2015-01-01T00:00:00Z,2015-01-01T00:00:00Z,0,,000000000000000000000001` ~ "\n" ~
              `000000000000000000000002,000000000000000000000002,map2,site2,description of site2,1.6,1.6,POINT(1.6 1.6),"{""type"":""Point"",""coordinates"":[1.6,1.6]}",,,,,,,000000000000000000000001;000000000000000000000002,name1;name2,0,,,,,0001-01-01T00:00:00+00:00,false,false,,,,,,,,2015-01-01T00:00:00Z,2015-01-01T00:00:00Z,0,,` ~ "\n" ~
              `000000000000000000000003,000000000000000000000003,map3,site3,description of site3,1.7,1.7,POINT(1.7 1.7),"{""type"":""Point"",""coordinates"":[1.7,1.7]}",,,,,,,000000000000000000000001;000000000000000000000002,name1;name2,1,,,,,0001-01-01T00:00:00+00:00,true,false,,,,,,,,2015-01-01T00:00:00Z,2015-01-01T00:00:00Z,0,,` ~ "\n" ~
              `000000000000000000000004,000000000000000000000004,map4,site4,description of site4,1.8,1.8,POINT(1.8 1.8),"{""type"":""Point"",""coordinates"":[1.8,1.8]}",,,,,,,000000000000000000000001;000000000000000000000002,name1;name2,0,,,,,0001-01-01T00:00:00+00:00,false,false,,,,,,,,2015-01-01T00:00:00Z,2015-01-01T00:00:00Z,0,,` ~ "\n");
          });
      });

      describeCredentialsRule("download list as csv", "yes", "features", "no rights", {
        router
          .request
          .get("/features?format=csv")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyString.should.equal(
              `_id,maps,maps.name,name,description,position.lon,position.lat,position.wkt,position.geoJson,pictures,pictures.name,pictures.url,sounds,sounds.name,sounds.url,icons,icons.name,visibility,contributors,source.type,source.modelId,source.remoteId,source.syncAt,computedVisibility.isPublic,computedVisibility.isDefault,attributes.position details.type,attributes.position details.altitudeAccuracy,attributes.position details.altitude,attributes.position details.accuracy,attributes.source.answer,attributes.source.type,attributes.source.campaign,createdOn,lastChangeOn,changeIndex,author.email,originalAuthor.email` ~ "\n" ~
              `000000000000000000000001,000000000000000000000001,map1,site1,description of site1,1.5,1.5,POINT(1.5 1.5),"{""type"":""Point"",""coordinates"":[1.5,1.5]}",,,,,,,000000000000000000000001,name1,1,,,,,0001-01-01T00:00:00+00:00,true,false,,,,,,,,2015-01-01T00:00:00Z,2015-01-01T00:00:00Z,0,,000000000000000000000001` ~ "\n" ~
              `000000000000000000000003,000000000000000000000003,map3,site3,description of site3,1.7,1.7,POINT(1.7 1.7),"{""type"":""Point"",""coordinates"":[1.7,1.7]}",,,,,,,000000000000000000000001;000000000000000000000002,name1;name2,1,,,,,0001-01-01T00:00:00+00:00,true,false,,,,,,,,2015-01-01T00:00:00Z,2015-01-01T00:00:00Z,0,,` ~"\n");
          });
      });
    });

    describe("to get a csv file with masked features", {
      beforeEach({
        auto feature1 = crates.feature.getItem("000000000000000000000001").and.exec.front;
        feature1["unmasked"] = `{
          "type": "Point",
          "coordinates": [2,2]
        }`.parseJsonString;

        crates.feature.updateItem(feature1);
      });

      describeCredentialsRule("download masked geometries as csv", "yes", "features", "administrator", {
        router
          .request
          .get("/features?format=csv")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            auto lines = response.bodyString.split("\n");

            lines[0].should.equal(`_id,maps,maps.name,name,description,position.lon,position.lat,position.wkt,position.geoJson,pictures,pictures.name,pictures.url,sounds,sounds.name,sounds.url,icons,icons.name,visibility,contributors,source.type,source.modelId,source.remoteId,source.syncAt,computedVisibility.isPublic,computedVisibility.isDefault,attributes.position details.type,attributes.position details.altitudeAccuracy,attributes.position details.altitude,attributes.position details.accuracy,attributes.source.answer,attributes.source.type,attributes.source.campaign,createdOn,lastChangeOn,changeIndex,author.email,originalAuthor.email`);
            lines[1].should.equal(`000000000000000000000001,000000000000000000000001,map1,site1,description of site1,2,2,POINT(2 2),"{""type"":""Point"",""coordinates"":[2,2]}",,,,,,,000000000000000000000001,name1,1,,,,,0001-01-01T00:00:00+00:00,true,false,,,,,,,,2015-01-01T00:00:00Z,2015-01-01T00:00:00Z,0,,000000000000000000000001`);
          });
      });

      describeCredentialsRule("download masked geometries as csv", "yes", "features", ["owner", "leader", "member", "guest"], (string type) {
        router
          .request
          .get("/features?format=csv")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .end((Response response) => () {
            auto lines = response.bodyString.split("\n");

            lines[0].should.equal(`_id,maps,maps.name,name,description,position.lon,position.lat,position.wkt,position.geoJson,pictures,pictures.name,pictures.url,sounds,sounds.name,sounds.url,icons,icons.name,visibility,contributors,source.type,source.modelId,source.remoteId,source.syncAt,computedVisibility.isPublic,computedVisibility.isDefault,attributes.position details.type,attributes.position details.altitudeAccuracy,attributes.position details.altitude,attributes.position details.accuracy,attributes.source.answer,attributes.source.type,attributes.source.campaign,createdOn,lastChangeOn,changeIndex,author.email,originalAuthor.email`);
            lines[1].should.equal(`000000000000000000000001,000000000000000000000001,map1,site1,description of site1,2,2,POINT(2 2),"{""type"":""Point"",""coordinates"":[2,2]}",,,,,,,000000000000000000000001,name1,1,,,,,0001-01-01T00:00:00+00:00,true,false,,,,,,,,2015-01-01T00:00:00Z,2015-01-01T00:00:00Z,0,,000000000000000000000001`);
          });
      });

      describeCredentialsRule("download masked geometries as csv", "yes", "features", "no rights", {
        router
          .request
          .get("/features?format=csv")
          .expectStatusCode(200)
          .end((Response response) => () {
            auto lines = response.bodyString.split("\n");

            lines[0].should.equal(`_id,maps,maps.name,name,description,position.lon,position.lat,position.wkt,position.geoJson,pictures,pictures.name,pictures.url,sounds,sounds.name,sounds.url,icons,icons.name,visibility,contributors,source.type,source.modelId,source.remoteId,source.syncAt,computedVisibility.isPublic,computedVisibility.isDefault,attributes.position details.type,attributes.position details.altitudeAccuracy,attributes.position details.altitude,attributes.position details.accuracy,attributes.source.answer,attributes.source.type,attributes.source.campaign,createdOn,lastChangeOn,changeIndex,author.email,originalAuthor.email`);
            lines[1].should.equal(`000000000000000000000001,000000000000000000000001,map1,site1,description of site1,1.5,1.5,POINT(1.5 1.5),"{""type"":""Point"",""coordinates"":[1.5,1.5]}",,,,,,,000000000000000000000001,name1,1,,,,,0001-01-01T00:00:00+00:00,true,false,,,,,,,,2015-01-01T00:00:00Z,2015-01-01T00:00:00Z,0,,000000000000000000000001`);
          });
      });
    });

    describe("to get a geoJson file", {
      describeCredentialsRule("download list as geojson", "yes", "features", (string type) {
        router
          .request
          .get("/features?format=geojson")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "type": "FeatureCollection",
              "features": [{
                "type": "Feature",
                "geometry": {"type": "Point","coordinates": [1.5,1.5]},
                "properties": {
                  "info": {
                    "changeIndex": 0,
                    "createdOn": "2015-01-01T00:00:00Z",
                    "lastChangeOn": "2015-01-01T00:00:00Z",
                    "originalAuthor": "000000000000000000000001",
                    "author": ""
                  },
                  "computedVisibility": {
                    "isDefault": false,
                    "isPublic": true,
                    "team": "000000000000000000000001"
                  },
                  "contributors": [],
                  "maps": [ "000000000000000000000001" ],
                  "name": "site1",
                  "_id": "000000000000000000000001",
                  "isLarge": false,
                  "visibility": 1,
                  "icons": [ "000000000000000000000001" ],
                  "decorators": {
                    "center": [],
                    "showAsLineAfterZoom": 99,
                    "showLineMarkers": false,
                    "useDefault": true,
                    "keepWhenSmall": true,
                    "changeIndex": 0,
                    "minZoom": 0,
                    "maxZoom": 20
                  },
                  "description": {
                    "blocks": [{
                      "type": "paragraph",
                      "data": {
                        "text": "description of site1"
                      }
                    }]
                  },
                  "source": {
                    "type": "",
                    "remoteId": "",
                    "modelId": "",
                    "syncAt": "0001-01-01T00:00:00+00:00"
                  },
                  "attributes": {
                    "name1": {
                      "program": "Luni-Vineri:9-18",
                      "phone": "123456",
                      "kids-friendly": "true",
                      "type of food": "international",
                      "price": "11.4",
                      "max number of people": "23"
                    }
                  },
                  "pictures": [],
                  "sounds": []
                }
              },{
                "type": "Feature",
                "geometry": {"type": "Point","coordinates":[1.6,1.6]},
                "properties": {
                  "info": {
                    "changeIndex": 0,
                    "createdOn": "2015-01-01T00:00:00Z",
                    "lastChangeOn": "2015-01-01T00:00:00Z",
                    "originalAuthor": "",
                    "author": ""
                  },
                  "computedVisibility": {
                    "isDefault": false,
                    "isPublic": false,
                    "team": "000000000000000000000002"
                  },
                  "contributors": [],
                  "maps": ["000000000000000000000002" ],
                  "name": "site2",
                  "_id": "000000000000000000000002",
                  "isLarge": false,
                  "visibility": 0,
                  "icons": ["000000000000000000000001","000000000000000000000002"],
                  "source": {
                    "type": "",
                    "remoteId": "",
                    "modelId": "",
                    "syncAt": "0001-01-01T00:00:00+00:00"
                  },
                  "decorators": {
                    "center": [],
                    "showAsLineAfterZoom": 99,
                    "showLineMarkers": false,
                    "useDefault": true,
                    "keepWhenSmall": true,
                    "changeIndex": 0,
                    "minZoom": 0,
                    "maxZoom": 20
                  },
                  "description": {
                    "blocks": [{
                      "type": "paragraph",
                      "data": {
                        "text": "description of site2"
                      }
                    }]
                  },
                  "attributes": {},
                  "pictures": [],
                  "sounds": []
                }
              },{
                "type": "Feature",
                "geometry": { "type": "Point", "coordinates": [1.7,1.7] },
                "properties": {
                  "info": {
                    "changeIndex": 0,
                    "createdOn": "2015-01-01T00:00:00Z",
                    "lastChangeOn": "2015-01-01T00:00:00Z",
                    "originalAuthor": "",
                    "author": ""
                  },
                  "source": {
                    "type": "",
                    "remoteId": "",
                    "modelId": "",
                    "syncAt": "0001-01-01T00:00:00+00:00"
                  },
                  "computedVisibility": {
                    "isDefault": false,
                    "isPublic": true,
                    "team": "000000000000000000000003"
                  },
                  "contributors": [],
                  "maps": ["000000000000000000000003"],
                  "name": "site3",
                  "_id": "000000000000000000000003",
                  "isLarge": false,
                  "visibility": 1,
                  "icons": ["000000000000000000000001","000000000000000000000002"],
                  "decorators": {
                    "center": [],
                    "showAsLineAfterZoom": 99,
                    "showLineMarkers": false,
                    "useDefault": true,
                    "keepWhenSmall": true,
                    "changeIndex": 0,
                    "minZoom": 0,
                    "maxZoom": 20
                  },
                  "description": {
                    "blocks": [{
                      "type": "paragraph",
                      "data": {
                        "text": "description of site3"
                      }
                    }]
                  },
                  "attributes": {},
                  "pictures": [],
                  "sounds": []
                }
              }]}`.parseJsonString);
          });
      });

      describeCredentialsRule("download list as geojson", "yes", "features", "administrator", {
        router
          .request
          .get("/features?format=geojson")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            Json[] features = cast(Json[]) response.bodyJson["features"];

            features.map!(a => a["properties"]["_id"].to!string).array.should
              .equal(["000000000000000000000001", "000000000000000000000002", "000000000000000000000003", "000000000000000000000004"]);
          });
      });

      describeCredentialsRule("download list as geojson", "yes", "features", "no rights", {
        router
            .request
            .get("/features?format=geojson")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal(`{
                "type": "FeatureCollection",
                "features": [{
                  "type": "Feature",
                  "geometry": {"type": "Point","coordinates": [1.5,1.5]},
                  "properties": {
                    "info": {
                      "changeIndex": 0,
                      "createdOn": "2015-01-01T00:00:00Z",
                      "lastChangeOn": "2015-01-01T00:00:00Z",
                      "originalAuthor": "000000000000000000000001",
                      "author": ""
                    },
                    "computedVisibility": {
                      "isDefault": false,
                      "isPublic": true,
                      "team": "000000000000000000000001"
                    },
                    "source": {
                      "type": "",
                      "remoteId": "",
                      "modelId": "",
                      "syncAt": "0001-01-01T00:00:00+00:00"
                    },
                    "contributors": [],
                    "maps": [ "000000000000000000000001" ],
                    "name": "site1",
                    "_id": "000000000000000000000001",
                    "visibility": 1,
                    "isLarge": false,
                    "icons": [ "000000000000000000000001" ],
                    "description": {
                      "blocks": [{
                        "type": "paragraph",
                        "data": {
                          "text": "description of site1"
                        }
                      }]
                    },
                    "attributes": {
                      "name1": {
                        "program": "Luni-Vineri:9-18",
                        "phone": "123456",
                        "kids-friendly": "true",
                        "type of food": "international",
                        "price": "11.4",
                        "max number of people": "23"
                      }
                    },
                    "decorators": {
                      "center": [],
                      "showAsLineAfterZoom": 99,
                      "showLineMarkers": false,
                      "useDefault": true,
                      "keepWhenSmall": true,
                      "changeIndex": 0,
                      "minZoom": 0,
                      "maxZoom": 20
                    },
                    "pictures": [],
                    "sounds": []
                  }
                },{
                  "type": "Feature",
                  "geometry": { "type": "Point", "coordinates": [1.7,1.7] },
                  "properties": {
                    "info": {
                      "changeIndex": 0,
                      "createdOn": "2015-01-01T00:00:00Z",
                      "lastChangeOn": "2015-01-01T00:00:00Z",
                      "originalAuthor": "",
                      "author": ""
                    },
                    "source": {
                      "type": "",
                      "remoteId": "",
                      "modelId": "",
                      "syncAt": "0001-01-01T00:00:00+00:00"
                    },
                    "computedVisibility": {
                      "isDefault": false,
                      "isPublic": true,
                      "team": "000000000000000000000003"
                    },
                    "contributors": [],
                    "maps": ["000000000000000000000003"],
                    "name": "site3",
                    "_id": "000000000000000000000003",
                    "visibility": 1,
                    "isLarge": false,
                    "icons": ["000000000000000000000001","000000000000000000000002"],
                    "decorators": {
                      "center": [],
                      "showAsLineAfterZoom": 99,
                      "showLineMarkers": false,
                      "useDefault": true,
                      "keepWhenSmall": true,
                      "changeIndex": 0,
                      "minZoom": 0,
                      "maxZoom": 20
                    },
                    "description": {
                      "blocks": [{
                        "type": "paragraph",
                        "data": {
                          "text": "description of site3"
                        }
                      }]
                    },
                    "attributes": {},
                    "pictures": [],
                    "sounds": []
                  }
                }]}`.parseJsonString);
            });
      });
    });

    describe("to get a geoJson file with masked features", {
      Json feature1;

      beforeEach({
        feature1 = crates.feature.getItem("000000000000000000000001").and.exec.front;
        feature1["unmasked"] = `{
          "type": "Point",
          "coordinates": [2,2]
        }`.parseJsonString;

        crates.feature.updateItem(feature1);
      });

      describeCredentialsRule("download unmasked geometries as geojson", "yes", "features", ["owner", "leader", "member", "guest"], (string type) {
        router
          .request
          .get("/features?format=geojson")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .end((Response response) => () {
            Json expected = `{
              "type": "Feature",
              "geometry": {"type": "Point","coordinates": [2, 2]},
              "properties": {
                "info": {
                  "changeIndex": 0,
                  "createdOn": "2015-01-01T00:00:00Z",
                  "lastChangeOn": "2015-01-01T00:00:00Z",
                  "originalAuthor": "000000000000000000000001",
                  "author": ""
                },
                "computedVisibility": {
                  "isDefault": false,
                  "isPublic": true,
                  "team": "000000000000000000000001"
                },
                "contributors": [],
                "maps": [ "000000000000000000000001" ],
                "name": "site1",
                "_id": "000000000000000000000001",
                "visibility": 1,
                "isLarge": false,
                "icons": [ "000000000000000000000001" ],
                "description": {
                  "blocks": [{
                    "type": "paragraph",
                    "data": {
                      "text": "description of site1"
                    }
                  }]
                },
                "attributes": {
                  "name1": {
                    "program": "Luni-Vineri:9-18",
                    "phone": "123456",
                    "kids-friendly": "true",
                    "type of food": "international",
                    "price": "11.4",
                    "max number of people": "23"
                  }
                },
                "decorators": {
                  "center": [],
                  "showAsLineAfterZoom": 99,
                  "showLineMarkers": false,
                  "useDefault": true,
                  "keepWhenSmall": true,
                  "changeIndex": 0,
                  "minZoom": 0,
                  "maxZoom": 20
                },
                "source": {
                  "type": "",
                  "remoteId": "",
                  "modelId": "",
                  "syncAt": "0001-01-01T00:00:00+00:00"
                },
                "pictures": [],
                "sounds": []
              }
            }`.parseJsonString;

            response.bodyJson["features"][0].should.equal(expected);
          });
      });

      describeCredentialsRule("download unmasked geometries as geojson", "yes", "features", "administrator", {
        router
          .request
          .get("/features?format=geojson")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["features"][0].should.equal(`{
              "type": "Feature",
              "geometry": {"type": "Point","coordinates": [2, 2]},
              "properties": {
                "info": {
                  "changeIndex": 0,
                  "createdOn": "2015-01-01T00:00:00Z",
                  "lastChangeOn": "2015-01-01T00:00:00Z",
                  "originalAuthor": "000000000000000000000001",
                  "author": ""
                },
                "computedVisibility": {
                  "isDefault": false,
                  "isPublic": true,
                  "team": "000000000000000000000001"
                },
                "contributors": [],
                "maps": [ "000000000000000000000001" ],
                "name": "site1",
                "_id": "000000000000000000000001",
                "visibility": 1,
                "isLarge": false,
                "icons": [ "000000000000000000000001" ],
                "description": {
                  "blocks": [{
                    "type": "paragraph",
                    "data": {
                      "text": "description of site1"
                    }
                  }]
                },
                "source": {
                  "type": "",
                  "remoteId": "",
                  "modelId": "",
                  "syncAt": "0001-01-01T00:00:00+00:00"
                },
                "attributes": {
                  "name1": {
                    "program": "Luni-Vineri:9-18",
                    "phone": "123456",
                    "kids-friendly": "true",
                    "type of food": "international",
                    "price": "11.4",
                    "max number of people": "23"
                  }
                },
                "decorators": {
                  "center": [],
                  "showAsLineAfterZoom": 99,
                  "showLineMarkers": false,
                  "useDefault": true,
                  "keepWhenSmall": true,
                  "changeIndex": 0,
                  "minZoom": 0,
                  "maxZoom": 20
                },
                "pictures": [],
                "sounds": []
              }
            }`.parseJsonString);
          });
      });

      describeCredentialsRule("download unmasked geometries as geojson", "no", "features", "no rights", {
        router
            .request
            .get("/features?format=geojson")
            .expectStatusCode(200)
            .end((Response response) => () {
            response.bodyJson["features"][0].should.equal(`{
              "type": "Feature",
              "geometry": {"type": "Point","coordinates": [1.5,1.5]},
              "properties": {
                "info": {
                  "changeIndex": 0,
                  "createdOn": "2015-01-01T00:00:00Z",
                  "lastChangeOn": "2015-01-01T00:00:00Z",
                  "originalAuthor": "000000000000000000000001",
                  "author": ""
                },
                "computedVisibility": {
                  "isDefault": false,
                  "isPublic": true,
                  "team": "000000000000000000000001"
                },
                "contributors": [],
                "maps": [ "000000000000000000000001" ],
                "name": "site1",
                "_id": "000000000000000000000001",
                "visibility": 1,
                "isLarge": false,
                "isMasked": true,
                "icons": [ "000000000000000000000001" ],
                "description": {
                  "blocks": [{
                    "type": "paragraph",
                    "data": {
                      "text": "description of site1"
                    }
                  }]
                },
                "source": {
                  "type": "",
                  "remoteId": "",
                  "modelId": "",
                  "syncAt": "0001-01-01T00:00:00+00:00"
                },
                "attributes": {
                  "name1": {
                    "program": "Luni-Vineri:9-18",
                    "phone": "123456",
                    "kids-friendly": "true",
                    "type of food": "international",
                    "price": "11.4",
                    "max number of people": "23"
                  }
                },
                "decorators": {
                  "center": [],
                  "showAsLineAfterZoom": 99,
                  "showLineMarkers": false,
                  "useDefault": true,
                  "keepWhenSmall": true,
                  "changeIndex": 0,
                  "minZoom": 0,
                  "maxZoom": 20
                },
                "pictures": [],
                "sounds": []
              }
            }`.parseJsonString);
          });
      });
    });

    describe("to get a gpx file", {
      Json feature1;

      beforeEach({
        feature1 = crates.feature.getItem("000000000000000000000001").and.exec.front;
      });

      it("can download the list as gpx as guest", {
        router
          .request
          .get("/features?format=gpx")
          .header("Authorization", "Bearer " ~ userTokenList["guest"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyString.should.equal(`<?xml version="1.0" encoding="UTF-8" standalone="no" ?>` ~
            `<gpx xmlns="http://www.topografix.com/GPX/1/1" creator="GISCollective" version="1.1" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">` ~
            `<wpt lat="1.5000000000000" lon="1.5000000000000"><name>site1</name><desc>description of site1</desc><type>name1</type></wpt>` ~ "\n" ~
            `<wpt lat="1.6000000000000" lon="1.6000000000000"><name>site2</name><desc>description of site2</desc><type>name1</type></wpt>` ~ "\n" ~
            `<wpt lat="1.7000000000000" lon="1.7000000000000"><name>site3</name><desc>description of site3</desc><type>name1</type></wpt></gpx>`);
          });
      });

      it("can download the list as gpx as admin", {
        router
          .request
          .get("/features?format=gpx")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyString.should.equal(`<?xml version="1.0" encoding="UTF-8" standalone="no" ?>` ~
            `<gpx xmlns="http://www.topografix.com/GPX/1/1" creator="GISCollective" version="1.1" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">` ~
            `<wpt lat="1.5000000000000" lon="1.5000000000000"><name>site1</name><desc>description of site1</desc><type>name1</type></wpt>` ~ "\n" ~
            `<wpt lat="1.6000000000000" lon="1.6000000000000"><name>site2</name><desc>description of site2</desc><type>name1</type></wpt>` ~ "\n" ~
            `<wpt lat="1.7000000000000" lon="1.7000000000000"><name>site3</name><desc>description of site3</desc><type>name1</type></wpt>` ~ "\n" ~
            `<wpt lat="1.8000000000000" lon="1.8000000000000"><name>site4</name><desc>description of site4</desc><type>name1</type></wpt></gpx>`);
          });
      });

      it("can download the list as gpx as without token", {
        router
            .request
            .get("/features?format=gpx")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyString.should.equal(`<?xml version="1.0" encoding="UTF-8" standalone="no" ?>` ~
              `<gpx xmlns="http://www.topografix.com/GPX/1/1" creator="GISCollective" version="1.1" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">` ~
              `<wpt lat="1.5000000000000" lon="1.5000000000000"><name>site1</name><desc>description of site1</desc><type>name1</type></wpt>` ~ "\n" ~
              `<wpt lat="1.7000000000000" lon="1.7000000000000"><name>site3</name><desc>description of site3</desc><type>name1</type></wpt></gpx>`);
            });
      });

      it("can get the list as gpx when there is a multi point feature", {
        feature1["position"] = `{
          "type": "MultiPoint",
          "coordinates": [
            [10.0, 40.0], [40.0, 30.0], [20.0, 20.0], [30.0, 10.0]
          ]
        }`.parseJsonString;
        crates.feature.updateItem(feature1);


        router
            .request
            .get("/features?format=gpx")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyString.should.equal(`<?xml version="1.0" encoding="UTF-8" standalone="no" ?>` ~
              `<gpx xmlns="http://www.topografix.com/GPX/1/1" creator="GISCollective" version="1.1" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">` ~
              `<wpt lat="40.0000000000000" lon="10.0000000000000"><name>site1</name><desc>description of site1</desc><type>name1</type></wpt>` ~ "\n" ~
              `<wpt lat="30.0000000000000" lon="40.0000000000000"><name>site1</name><desc>description of site1</desc><type>name1</type></wpt>` ~ "\n" ~
              `<wpt lat="20.0000000000000" lon="20.0000000000000"><name>site1</name><desc>description of site1</desc><type>name1</type></wpt>` ~ "\n" ~
              `<wpt lat="10.0000000000000" lon="30.0000000000000"><name>site1</name><desc>description of site1</desc><type>name1</type></wpt>` ~ "\n" ~
              `<wpt lat="1.7000000000000" lon="1.7000000000000"><name>site3</name><desc>description of site3</desc><type>name1</type></wpt></gpx>`);
            });
      });

      it("can get the list as gpx when there is a line feature", {
        feature1["position"] = `{
          "type": "LineString",
          "coordinates": [
            [30.0, 10.0], [10.0, 30.0], [40.0, 40.0]
          ]
        }`.parseJsonString;
        crates.feature.updateItem(feature1);


        router
            .request
            .get("/features?format=gpx")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyString.should.equal(`<?xml version="1.0" encoding="UTF-8" standalone="no" ?>` ~
              `<gpx xmlns="http://www.topografix.com/GPX/1/1" creator="GISCollective" version="1.1" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">` ~
              `<rte><name>site1</name><desc>description of site1</desc><type>name1</type><rtept lat="10.0000000000000" lon="30.0000000000000"/><rtept lat="30.0000000000000" lon="10.0000000000000"/><rtept lat="40.0000000000000" lon="40.0000000000000"/></rte>` ~ "\n" ~
              `<wpt lat="1.7000000000000" lon="1.7000000000000"><name>site3</name><desc>description of site3</desc><type>name1</type></wpt></gpx>`);
            });
      });

      it("can get the list as gpx when there is a multi line feature", {
        feature1["position"] = `{
          "type": "MultiLineString",
          "coordinates": [
            [[10.0, 10.0], [20.0, 20.0], [10.0, 40.0]],
            [[40.0, 40.0], [30.0, 30.0], [40.0, 20.0], [30.0, 10.0]]
          ]
        }`.parseJsonString;
        crates.feature.updateItem(feature1);


        router
            .request
            .get("/features?format=gpx")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyString.should.equal(`<?xml version="1.0" encoding="UTF-8" standalone="no" ?>` ~
              `<gpx xmlns="http://www.topografix.com/GPX/1/1" creator="GISCollective" version="1.1" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">` ~
              `<rte><name>site1</name><desc>description of site1</desc><type>name1</type><rtept lat="10.0000000000000" lon="10.0000000000000"/><rtept lat="20.0000000000000" lon="20.0000000000000"/><rtept lat="40.0000000000000" lon="10.0000000000000"/></rte>` ~ "\n" ~
              `<rte><name>site1</name><desc>description of site1</desc><type>name1</type><rtept lat="40.0000000000000" lon="40.0000000000000"/><rtept lat="30.0000000000000" lon="30.0000000000000"/><rtept lat="20.0000000000000" lon="40.0000000000000"/><rtept lat="10.0000000000000" lon="30.0000000000000"/></rte>` ~ "\n" ~
              `<wpt lat="1.7000000000000" lon="1.7000000000000"><name>site3</name><desc>description of site3</desc><type>name1</type></wpt></gpx>`);
            });
      });

      it("can get the list as gpx when there is a Polygon feature", {
        feature1["position"] = `{
          "type": "Polygon",
          "coordinates": [
            [[35.0, 10.0], [45.0, 45.0], [15.0, 40.0], [10.0, 20.0], [35.0, 10.0]],
            [[20.0, 30.0], [35.0, 35.0], [30.0, 20.0], [20.0, 30.0]]
          ]
        }`.parseJsonString;
        crates.feature.updateItem(feature1);


        router
            .request
            .get("/features?format=gpx")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyString.should.equal(`<?xml version="1.0" encoding="UTF-8" standalone="no" ?>` ~
              `<gpx xmlns="http://www.topografix.com/GPX/1/1" creator="GISCollective" version="1.1" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">` ~
              " \n" ~
              `<wpt lat="1.7000000000000" lon="1.7000000000000"><name>site3</name><desc>description of site3</desc><type>name1</type></wpt></gpx>`);
            });
      });

      it("can get the list as gpx when there is a MultiPolygon feature", {
          feature1["position"] = `{
            "type": "MultiPolygon",
            "coordinates": [[
              [[40.0, 40.0], [20.0, 45.0], [45.0, 30.0], [40.0, 40.0]]
            ],[
              [[20.0, 35.0], [10.0, 30.0], [10.0, 10.0], [30.0, 5.0], [45.0, 20.0], [20.0, 35.0]],
              [[30.0, 20.0], [20.0, 15.0], [20.0, 25.0], [30.0, 20.0]]
            ]]
          }`.parseJsonString;
          crates.feature.updateItem(feature1);


          router
              .request
              .get("/features?format=gpx")
              .expectStatusCode(200)
              .end((Response response) => () {
                response.bodyString.should.equal(`<?xml version="1.0" encoding="UTF-8" standalone="no" ?>` ~
                `<gpx xmlns="http://www.topografix.com/GPX/1/1" creator="GISCollective" version="1.1" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">` ~
                " \n" ~
                `<wpt lat="1.7000000000000" lon="1.7000000000000"><name>site3</name><desc>description of site3</desc><type>name1</type></wpt></gpx>`);
              });
      });
    });

    describe("to get a gpx file with masked features", {
      beforeEach({
        auto feature1 = crates.feature.getItem("000000000000000000000001").and.exec.front;
        feature1["unmasked"] = `{
          "type": "Point",
          "coordinates": [2,2]
        }`.parseJsonString;

        crates.feature.updateItem(feature1);
      });

      describeCredentialsRule("download masked geometries as gpx", "yes", "features", ["owner", "leader", "member", "guest"], (string type) {
        router
          .request
          .get("/features?format=gpx")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyString.should.equal(`<?xml version="1.0" encoding="UTF-8" standalone="no" ?>` ~
            `<gpx xmlns="http://www.topografix.com/GPX/1/1" creator="GISCollective" version="1.1" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">` ~
            `<wpt lat="2.0000000000000" lon="2.0000000000000"><name>site1</name><desc>description of site1</desc><type>name1</type></wpt>` ~ "\n" ~
            `<wpt lat="1.6000000000000" lon="1.6000000000000"><name>site2</name><desc>description of site2</desc><type>name1</type></wpt>` ~ "\n" ~
            `<wpt lat="1.7000000000000" lon="1.7000000000000"><name>site3</name><desc>description of site3</desc><type>name1</type></wpt></gpx>`);
          });
      });

      describeCredentialsRule("download masked geometries as gpx", "yes", "features", "administrator", {
        router
          .request
          .get("/features?format=gpx")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyString.should.equal(`<?xml version="1.0" encoding="UTF-8" standalone="no" ?>` ~
            `<gpx xmlns="http://www.topografix.com/GPX/1/1" creator="GISCollective" version="1.1" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">` ~
            `<wpt lat="2.0000000000000" lon="2.0000000000000"><name>site1</name><desc>description of site1</desc><type>name1</type></wpt>` ~ "\n" ~
            `<wpt lat="1.6000000000000" lon="1.6000000000000"><name>site2</name><desc>description of site2</desc><type>name1</type></wpt>` ~ "\n" ~
            `<wpt lat="1.7000000000000" lon="1.7000000000000"><name>site3</name><desc>description of site3</desc><type>name1</type></wpt>` ~ "\n" ~
            `<wpt lat="1.8000000000000" lon="1.8000000000000"><name>site4</name><desc>description of site4</desc><type>name1</type></wpt></gpx>`);
          });
      });

      describeCredentialsRule("download masked geometries as gpx", "yes", "features", "no rights", {
        router
            .request
            .get("/features?format=gpx")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyString.should.equal(`<?xml version="1.0" encoding="UTF-8" standalone="no" ?>` ~
              `<gpx xmlns="http://www.topografix.com/GPX/1/1" creator="GISCollective" version="1.1" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">` ~
              `<wpt lat="1.5000000000000" lon="1.5000000000000"><name>site1</name><desc>description of site1</desc><type>name1</type></wpt>` ~ "\n" ~
              `<wpt lat="1.7000000000000" lon="1.7000000000000"><name>site3</name><desc>description of site3</desc><type>name1</type></wpt></gpx>`);
            });
      });
    });
  });
});
