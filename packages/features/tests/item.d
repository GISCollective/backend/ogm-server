/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.features.item;

import tests.fixtures;
import tests.data.features;
import vibe.http.common;
import geo.json;
import ogm.features.api;

alias suite = Spec!({
  URLRouter router;

  describe("when there is an admin token", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupFeaturesApi(crates);
    });

    describe("with features with pictures of invalid type", {
      beforeEach({
        auto feature = crates.feature.getItem("000000000000000000000001").exec.front;
        feature["pictures"] ~= Json.emptyObject;

        crates.feature.updateItem(feature);
      });

      it("removes it from the list", {
        router
          .request
          .get("/features/000000000000000000000001")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["feature"]["pictures"].length.should.equal(0);
          });
      });
    });

    describe("with features with pictures that were deleted", {
      beforeEach({
        auto feature = crates.feature.getItem("000000000000000000000001").exec.front;
        feature["pictures"] ~= "000000000000000000000090";

        crates.feature.updateItem(feature);
      });

      it("removes it from the list", {
        router
          .request
          .get("/features/000000000000000000000001")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["feature"]["pictures"].length.should.equal(0);
          });
      });
    });

    describe("with features with sounds of invalid type", {
      beforeEach({
        auto feature = crates.feature.getItem("000000000000000000000001").exec.front;
        feature["sounds"] ~= Json.emptyObject;

        crates.feature.updateItem(feature);
      });

      it("removes it from the list", {
        router
          .request
          .get("/features/000000000000000000000001")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["feature"]["sounds"].length.should.equal(0);
          });
      });
    });

    describe("with features with sounds that were deleted", {
      beforeEach({
        auto feature = crates.feature.getItem("000000000000000000000001").exec.front;
        feature["sounds"] ~= "000000000000000000000090";

        crates.feature.updateItem(feature);
      });

      it("removes it from the list", {
        router
          .request
          .get("/features/000000000000000000000001")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["feature"]["sounds"].length.should.equal(0);
          });
      });
    });

    describe("with masked features from own team", {
      beforeEach({
        auto feature1 = crates.feature.getItem("000000000000000000000001").and.exec.front;
        feature1["unmasked"] = `{
          "type": "Point",
          "coordinates": [2,2]
        }`.parseJsonString;

        crates.feature.updateItem(feature1);
      });

      describeCredentialsRule("view unmasked geometry from own team", "yes", "features", ["administrator", "owner", "leader", "member", "guest"], (string type) {
        router
          .request
          .get("/features/000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["feature"]["position"].should.equal(`{
              "type": "Point",
              "coordinates": [2,2]
            }`.parseJsonString);
          });
      });

      describeCredentialsRule("view unmasked geometry from own team", "-", "features", "no rights", {
        router
          .request
          .get("/features/000000000000000000000001")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["feature"]["position"].should.equal(`{
              "type": "Point",
              "coordinates": [1.5,1.5]
            }`.parseJsonString);
          });
      });
    });

    describe("with masked features from other teams", {
      beforeEach({
        auto feature1 = crates.feature.getItem("000000000000000000000003").and.exec.front;
        feature1["unmasked"] = `{
          "type": "Point",
          "coordinates": [2,2]
        }`.parseJsonString;

        crates.feature.updateItem(feature1);
      });

      describeCredentialsRule("view unmasked geometry from other teams", "yes", "features", "administrator", (string type) {
        router
          .request
          .get("/features/000000000000000000000003")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["feature"]["isMasked"].type.should.equal(Json.Type.undefined);
            response.bodyJson["feature"]["position"].should.equal(`{
              "type": "Point",
              "coordinates": [2, 2]
            }`.parseJsonString);
          });
      });

      describeCredentialsRule("view unmasked geometry from other teams", "no", "features", ["owner", "leader", "member", "guest"], (string type) {
        router
          .request
          .get("/features/000000000000000000000003")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["feature"]["isMasked"].to!bool.should.equal(true);
            response.bodyJson["feature"]["position"].should.equal(`{
              "type": "Point",
              "coordinates": [1.7, 1.7]
            }`.parseJsonString);
          });
      });

      describeCredentialsRule("view unmasked geometry from other teams", "no", "features", "no rights", {
        router
          .request
          .get("/features/000000000000000000000003")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["feature"]["isMasked"].to!bool.should.equal(true);
            response.bodyJson["feature"]["position"].should.equal(`{
              "type": "Point",
              "coordinates": [1.7,1.7]
            }`.parseJsonString);
          });
      });
    });


    describe("with masked features from other teams", {
      beforeEach({
        auto feature1 = crates.feature.getItem("000000000000000000000003").and.exec.front;
        feature1["icons"] = `[null]`.parseJsonString;

        crates.feature.updateItem(feature1);
      });

      it("returns an empty icon list", {
        router
          .request
          .get("/features/000000000000000000000003")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["feature"]["icons"].length.should.equal(0);
          });
      });
    });

    describe("the feature attributes object", {
      it("is not changed with an admin token", {
        router
          .request
          .get("/features/000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["feature"]["attributes"].should.equal(`{"name1": {
              "program": "Luni-Vineri:9-18",
              "phone": "123456",
              "type of food": "international",
              "kids-friendly": "true",
              "price": "11.4",
              "max number of people": "23"
            }}`.parseJsonString);
          });
      });

      describe("when the attribute is not defined in an icon", {
        beforeEach({
          auto feature = crates.feature.getItem("000000000000000000000001").exec.front;
          feature["attributes"]["other"] = feature["attributes"]["name1"];
          feature["attributes"].remove("name1");

          crates.feature.updateItem(feature);
        });

        it("is returned for an admin", {
          router
            .request
            .get("/features/000000000000000000000001")
            .header("Authorization", "Bearer " ~ userTokenList["administrator"])
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["feature"]["attributes"].should.equal(`{"other": {
                "program": "Luni-Vineri:9-18",
                "phone": "123456",
                "max number of people": "23",
                "type of food": "international",
                "kids-friendly": "true",
                "price": "11.4"
              }}`.parseJsonString);
            });
        });

        it("is returned for a team member", {
          router
            .request
            .get("/features/000000000000000000000001")
            .header("Authorization", "Bearer " ~ userTokenList["owner"])
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["feature"]["attributes"].should.equal(`{"other": {
                "program": "Luni-Vineri:9-18",
                "phone": "123456",
                "max number of people": "23",
                "type of food": "international",
                "kids-friendly": "true",
                "price": "11.4"
              }}`.parseJsonString);
            });
        });

        it("is not returned for an user that is not part of the team", {
          router
            .request
            .get("/features/000000000000000000000001")
            .header("Authorization", "Bearer " ~ otherToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["feature"]["attributes"].should.equal(`{"name1": {}}`.parseJsonString);
            });
        });

        it("is not returned when there is no token", {
          router
            .request
            .get("/features/000000000000000000000001")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["feature"]["attributes"].should.equal(`{"name1": {}}`.parseJsonString);
            });
        });
      });


      describe("when the icon has a private attributes", {
        beforeEach({
          auto icon = crates.icon.getItem("000000000000000000000001").exec.front;
          icon["attributes"][0]["isPrivate"] = true;
          icon["attributes"][1]["isPrivate"] = true;
          icon["attributes"][2]["isPrivate"] = true;
          icon["attributes"][3]["isPrivate"] = true;
          icon["attributes"][4]["isPrivate"] = false;
          crates.icon.updateItem(icon);
        });

        it("is returned for an admin", {
          router
            .request
            .get("/features/000000000000000000000001")
            .header("Authorization", "Bearer " ~ userTokenList["administrator"])
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["feature"]["attributes"].should.equal(`{"name1": {
                "program": "Luni-Vineri:9-18",
                "phone": "123456",
                "max number of people": "23",
                "type of food": "international",
                "kids-friendly": "true",
                "price": "11.4"
              }}`.parseJsonString);
            });
        });

        it("is returned for a team member", {
          router
            .request
            .get("/features/000000000000000000000001")
            .header("Authorization", "Bearer " ~ userTokenList["owner"])
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["feature"]["attributes"].should.equal(`{"name1": {
                "program": "Luni-Vineri:9-18",
                "phone": "123456",
                "max number of people": "23",
                "type of food": "international",
                "kids-friendly": "true",
                "price": "11.4"
              }}`.parseJsonString);
            });
        });

        it("is not returned for an user that is not part of the team", {
          router
            .request
            .get("/features/000000000000000000000001")
            .header("Authorization", "Bearer " ~ otherToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["feature"]["attributes"].should.equal(`{"name1": {
                "kids-friendly": "true",
                "type of food": "international"
              }}`.parseJsonString);
            });
        });

        it("is not returned when there is no token", {
          router
            .request
            .get("/features/000000000000000000000001")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["feature"]["attributes"].should.equal(`{"name1": {
                "kids-friendly": "true",
                "type of food": "international"
              }}`.parseJsonString);
            });
        });
      });

      describe("when the attribute has a list", {
        beforeEach({
          auto icon = crates.icon.getItem("000000000000000000000001").exec.front;
          icon["allowMany"] = true;
          crates.icon.updateItem(icon);

          auto feature = crates.feature.getItem("000000000000000000000001").exec.front;
          auto attr = feature["attributes"]["name1"];
          feature["attributes"]["name1"] = Json.emptyArray;
          feature["attributes"]["name1"] ~= attr;

          crates.feature.updateItem(feature);
        });

        it("is returned for an admin", {
          router
            .request
            .get("/features/000000000000000000000001")
            .header("Authorization", "Bearer " ~ userTokenList["administrator"])
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["feature"]["attributes"].should.equal(`{"name1": [{
                "program": "Luni-Vineri:9-18",
                "phone": "123456",
                "max number of people": "23",
                "type of food": "international",
                "kids-friendly": "true",
                "price": "11.4"
              }]}`.parseJsonString);
            });
        });

        it("is returned for a team member", {
          router
            .request
            .get("/features/000000000000000000000001")
            .header("Authorization", "Bearer " ~ userTokenList["owner"])
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["feature"]["attributes"].should.equal(`{"name1": [{
                "program": "Luni-Vineri:9-18",
                "phone": "123456",
                "max number of people": "23",
                "type of food": "international",
                "kids-friendly": "true",
                "price": "11.4"
              }]}`.parseJsonString);
            });
        });

        it("is returned for an user that is not part of the team", {
          router
            .request
            .get("/features/000000000000000000000001")
            .header("Authorization", "Bearer " ~ otherToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["feature"]["attributes"].should.equal(`{"name1": [{
                "program": "Luni-Vineri:9-18",
                "phone": "123456",
                "max number of people": "23",
                "type of food": "international",
                "kids-friendly": "true",
                "price": "11.4"
              }]}`.parseJsonString);
            });
        });

        it("is returned when there no token", {
          router
            .request
            .get("/features/000000000000000000000001")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["feature"]["attributes"].should.equal(`{"name1": [{
                "program": "Luni-Vineri:9-18",
                "phone": "123456",
                "max number of people": "23",
                "type of food": "international",
                "kids-friendly": "true",
                "price": "11.4"
              }]}`.parseJsonString);
            });
        });
      });
    });
  });
});
