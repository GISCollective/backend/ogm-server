/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.features.put;

import tests.fixtures;
import tests.data.features;
import ogm.features.api;

import ogm.calendar;
import geo.json : GeoJsonGeometry;

alias suite = Spec!({
  URLRouter router;
  Json site1;
  Json site5;

  describe("edit features", {
    beforeEach({
      auto calendarMock = new CalendarMock("2023-01-12T16:20:11Z");
      SysCalendar.instance = calendarMock;

      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupFeaturesApi(crates);

      site1 = `{"feature": {
            "_id":"000000000000000000000001",
            "info": {
              "createdOn": "2015-01-01T00:00:00Z"
            },
            "name": "new site",
            "isLarge": false,
            "position":{"coordinates":[1.5,1.5],"type":"Point"},
            "icons":["000000000000000000000001"],
            "description": {
              "blocks": [{
                "type": "paragraph",
                "data": {
                  "text": "description of site1"
                }
              }
            ]},
            "pictures": [],
            "attributes": {},
            "maps":["000000000000000000000001"]}}`.parseJsonString;

      site5 = Json.emptyObject;
      site5["feature"] = createFeature(
        Feature(ObjectId.fromString("5"),
            [ Map(ObjectId.fromString("1")) ],
            "site5",
            Json("description of site5"),
            GeoJsonGeometry.fromJson(`{ "type": "Point", "coordinates": [1.5, 1.5] }`.parseJsonString),
            ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z")),
            [],
            [],
            [],
            VisibilityEnum.Private, ["000000000000000000000002"]));

      site5["feature"].remove("issueCount");

      crates.feature.addItem(site5.clone);

      site5["feature"].remove("computedVisibility");
      site5["feature"].remove("contributors");
    });

    describeCredentialsRule("edit other features from their own team", "yes", "features", ["administrator", "owner", "leader"], (string type) {
      auto feature = crates.feature.getItem("000000000000000000000001").and.exec.front.clone;
      feature["info"]["originalAuthor"] = "00000000000000000000000a";
      crates.feature.updateItem(feature);

      router
        .request
        .put("/features/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(site1)
        .expectStatusCode(200)
        .end((Response response) => () {
          auto expected = parseJsonString(`{ "feature": {
            "_id": "000000000000000000000001",
            "info": {
              "createdOn": "2015-01-01T00:00:00Z",
              "lastChangeOn": "2023-01-12T16:20:11Z",
              "changeIndex": 1,
              "originalAuthor": "00000000000000000000000a",
              "author": "` ~ userId[type] ~ `"
            },
            "contributors": [],
            "maps": [ "000000000000000000000001" ],
            "name": "new site",
            "visibility": 0,
            "attributes": {},
            "icons": [ "000000000000000000000001" ],
            "computedVisibility": {
              "isDefault": false,
              "isPublic": false,
              "team": "000000000000000000000001"
            },
            "decorators": {
              "center": [],
              "showAsLineAfterZoom": 99,
              "showLineMarkers": false,
              "useDefault": true,
              "keepWhenSmall": true,
              "changeIndex": 0,
              "minZoom": 0,
              "maxZoom": 20
            },
            "description": {
                "blocks": [{
                  "type": "paragraph",
                  "data": {
                    "text": "description of site1"
                  }
                }
            ]},
            "source": {
              "type": "",
              "remoteId": "",
              "modelId": "",
              "syncAt": "0001-01-01T00:00:00+00:00"
            },
            "isLarge": false,
            "position": { "type": "Point", "coordinates": [1.5, 1.5] },
            "canEdit": true,
            "pictures": [],
            "sounds": []
          }}`);

          response.bodyJson.should.equal(expected);
        });
    });

    describeCredentialsRule("edit other features from their own team", "no", "features", [ "member", "guest" ], (string type) {
      router
        .request
        .put("/features/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(site1)
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal("{ \"errors\": [{
            \"description\": \"You don't have enough rights to edit `000000000000000000000001`.\",
            \"status\": 403,
            \"title\": \"Forbidden\"
          }]}".parseJsonString);
        });
    });

    describeCredentialsRule("edit other features from their own team", "no", "features", "no rights", {
      router
        .request
        .put("/features/000000000000000000000001")
        .send(site1)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "error": "Authorization required"
          }`.parseJsonString);
        });
    });

    ///
    describeCredentialsRule("edit own features from their own team", "yes", "features", ["administrator", "owner", "leader", "member", "guest" ], (string type) {
      auto feature = crates.feature.getItem("000000000000000000000001").and.exec.front.clone;
      feature["info"]["originalAuthor"] = userId[type];
      crates.feature.updateItem(feature);

      router
        .request
        .put("/features/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(site1)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["feature"]["_id"].should.equal("000000000000000000000001");
        });
    });

    describeCredentialsRule("edit own features from their own team", "no", "features", "no rights", {
      router
        .request
        .put("/features/000000000000000000000001")
        .send(site1)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "error": "Authorization required"
          }`.parseJsonString);
        });
    });

    ///
    describeCredentialsRule("edit features from other teams", "yes", "features", ["administrator"], (string type) {
      auto feature = crates.feature.getItem("000000000000000000000001").and.exec.front.clone;
      feature["info"]["originalAuthor"] = "00000000000000000000000a";
      feature["maps"] = ["000000000000000000000004"].serializeToJson;
      feature["computedVisibility"]["team"] = "000000000000000000000004";
      crates.feature.updateItem(feature);

      router
        .request
        .put("/features/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(site1)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["feature"]["_id"].should.equal("000000000000000000000001");
        });
    });

    describeCredentialsRule("edit features from other teams", "no", "features", ["owner", "leader", "member", "guest" ], (string type) {
      auto feature = crates.feature.getItem("000000000000000000000001").and.exec.front.clone;
      feature["info"]["originalAuthor"] = "00000000000000000000000a";
      feature["maps"] = ["000000000000000000000004"].serializeToJson;
      feature["computedVisibility"]["team"] = "000000000000000000000004";
      crates.feature.updateItem(feature);

       router
        .request
        .put("/features/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(site1)
        .expectStatusCode(404)
        .end((Response response) => () {
          response.bodyJson.should.equal("{
            \"errors\": [{
              \"description\": \"Item `000000000000000000000001` not found.\",
              \"status\": 404,
              \"title\": \"Crate not found\"
            }]
          }".parseJsonString);
        });
    });

    describeCredentialsRule("edit features from other teams", "no", "features", "no rights", {
      router
        .request
        .put("/features/000000000000000000000001")
        .send(site1)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "error": "Authorization required"
          }`.parseJsonString);
        });
    });

    describe("using the gpx format", {
      beforeEach({
        site1["feature"].remove("_id");

        site1["feature"]["position"]["type"] = `gpx.track`;
        site1["feature"]["position"]["value"] = `<?xml version="1.0" encoding="UTF-8" standalone="no" ?>` ~
          `<gpx xmlns="http://www.topografix.com/GPX/1/1" version="1.1" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">` ~
          `<trk>` ~
          `<trkseg>`~
          `<trkpt lat="12.0000000000000" lon="11.0000000000000"/>` ~
          `<trkpt lat="14.0000000000000" lon="13.0000000000000"/>` ~
          `</trkseg>`~
          `<trkseg>`~
          `<trkpt lat="16.0000000000000" lon="15.0000000000000"/>` ~
          `<trkpt lat="18.0000000000000" lon="17.0000000000000"/>` ~
          `</trkseg>`~
          `</trk>` ~
          `</gpx>`;
      });

      describeCredentialsRule("edit features using the gpx format", "yes", "features", ["administrator", "owner", "leader"], (string type) {
        router
          .request
          .put("/features/000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .send(site1)
          .expectStatusCode(200)
          .end((Response response) => () {
            Json expected = parseJsonString(`{ "feature": {
              "_id": "000000000000000000000001",
              "info": { "createdOn": "` ~ response.bodyJson["feature"]["info"]["createdOn"].to!string ~ `",
                "lastChangeOn": "` ~ response.bodyJson["feature"]["info"]["lastChangeOn"].to!string ~ `",
                "changeIndex": 1, "originalAuthor": "000000000000000000000001", "author": "` ~ userId[type] ~ `" },
              "contributors": [ ],
              "maps": [ "000000000000000000000001" ],
              "name": "new site",
              "visibility":0,
              "attributes": {},
              "icons": [ "000000000000000000000001" ],
              "computedVisibility": {
                "isDefault": false,
                "isPublic": false,
                "team": "000000000000000000000001"
              },
              "decorators": {
                "center": [],
                "showAsLineAfterZoom": 99,
                "showLineMarkers": false,
                "useDefault": true,
                "keepWhenSmall": true,
                "changeIndex": 0,
                "minZoom": 0,
                "maxZoom": 20
              },
              "source": {
                "type": "",
                "remoteId": "",
                "modelId": "",
                "syncAt": "0001-01-01T00:00:00+00:00"
              },
              "description": {
                  "blocks": [{
                    "type": "paragraph",
                    "data": {
                      "text": "description of site1"
                    }
                  }
              ]},
              "isLarge": false,
              "position": {
                "type": "MultiLineString",
                "coordinates": [[[ 11, 12 ], [ 13, 14 ]], [[15, 16], [17, 18]]]
              },
              "canEdit": true,
              "pictures": [],
              "sounds": []
            }}`);

            response.bodyJson.should.equal(expected);
          });
      });

      describeCredentialsRule("edit features using the gpx format", "yes", "features", ["member"], (string type) {
        auto feature = crates.feature.getItem("000000000000000000000005").and.exec.front.clone;
        feature["info"]["originalAuthor"] = userId[type];
        crates.feature.updateItem(feature);

        router
          .request
          .put("/features/000000000000000000000005")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .send(site1)
          .expectStatusCode(200)
          .end((Response response) => () {
            Json expected = parseJsonString(`{ "feature": {
              "_id": "000000000000000000000005",
              "info": {
                "createdOn": "2015-01-01T00:00:00Z",
                "lastChangeOn": "2023-01-12T16:20:11Z",
                "changeIndex": 1,
                "originalAuthor": "` ~ userId[type] ~ `",
                "author": "` ~ userId[type] ~ `"
              },
              "contributors": [],
              "maps": [ "000000000000000000000001" ],
              "name": "new site",
              "visibility": 0,
              "attributes": {},
              "icons": [ "000000000000000000000001" ],
              "computedVisibility": {
                "isDefault": false,
                "isPublic": false,
                "team": "000000000000000000000001"
              },
              "decorators": {
                "center": [],
                "showAsLineAfterZoom": 99,
                "showLineMarkers": false,
                "useDefault": true,
                "keepWhenSmall": true,
                "changeIndex": 0,
                "minZoom": 0,
                "maxZoom": 20
              },
              "source": {
                "type": "",
                "remoteId": "",
                "modelId": "",
                "syncAt": "0001-01-01T00:00:00+00:00"
              },
              "description": {
                  "blocks": [{
                    "type": "paragraph",
                    "data": {
                      "text": "description of site1"
                    }
                  }
              ]},
              "isLarge": false,
              "position": {
                "type": "MultiLineString",
                "coordinates": [[[ 11, 12 ], [ 13, 14 ]], [[15, 16], [17, 18]]]
              },
              "canEdit": true,
              "pictures": [],
              "sounds": []
            }}`);

            response.bodyJson.should.equal(expected);
          });
      });

      describeCredentialsRule("edit features using the gpx format", "no", "features", "guest", (string type) {
        router
          .request
          .put("/features/000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .send(site1)
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"errors\": [{
              \"description\":\"You don't have enough rights to edit `000000000000000000000001`.\",
              \"status\": 403,
              \"title\":\"Forbidden\"
            }]}".parseJsonString);
          });
      });

      describeCredentialsRule("edit features using the gpx format", "no", "features", "no rights", {
        router
          .request
          .put("/features/000000000000000000000001")
          .send(site1)
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "error": "Authorization required"
            }`.parseJsonString);
          });
      });
    });

    describe("with an admin token", {
      it("should not accept issueCount field", {
        auto data = site1.clone;
        data["feature"]["issueCount"] = 1;

        router
          .request
          .put("/features/000000000000000000000001")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .send(data)
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"errors\": [{
              \"description\": \"You must not send the `feature.issueCount` field.\",
              \"status\": 400,
              \"title\": \"Validation error\"
            }]}".parseJsonString);
          });
      });

      it("should not accept canEdit field", {
        auto data = site1.clone;
        data["feature"]["canEdit"] = true;

        router
          .request
          .put("/features/000000000000000000000001")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .send(data)
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"errors\": [{
              \"description\": \"You must not send the `feature.canEdit` field.\",
              \"status\": 400,
              \"title\": \"Validation error\"
            }]}".parseJsonString);
          });
      });

      it("should not accept unmasked field", {
        auto data = site1.clone;
        data["feature"]["unmasked"] = data["feature"]["position"];

        router
          .request
          .put("/features/000000000000000000000001")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .send(data)
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"errors\": [{
              \"description\": \"You must not send the `feature.unmasked` field.\",
              \"status\": 400,
              \"title\": \"Validation error\"
            }]}".parseJsonString);
          });
      });

      it("should require the `feature` object", {
        auto data = `{}`.parseJsonString;

        router
          .request
          .put("/features/000000000000000000000001")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .send(data)
          .expectStatusCode(400)
          .end((Response response) => () {

            response.bodyJson.should.equal("{\"errors\": [{
              \"description\": \"object type expected to be `feature`\",
              \"status\": 400,
              \"title\": \"Validation error\"
            }]}".parseJsonString);
          });
      });

      it("should require the `feature` object fields", {
        auto data = `{"feature": {}}`.parseJsonString;

        router
          .request
          .put("/features/000000000000000000000001")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .send(data)
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson["errors"].length.should.equal(1);
            response.bodyJson["errors"][0]["status"].should.equal(400);
            response.bodyJson["errors"][0]["title"].should.equal("Validation error");
          });
      });

      it("should not increment the change index when the data is not changed", {
        auto data = `{"feature": {}}`.parseJsonString;
        data["feature"] = crates.feature.getItem("000000000000000000000001").exec.front;
        data["feature"]["info"] = Json.emptyObject;
        data["feature"].remove("computedVisibility");
        data["feature"].remove("contributors");

        router
          .request
          .put("/features/000000000000000000000001")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["feature"]["info"]["changeIndex"].to!int.should.equal(0);
          });
      });

      it("should change the original author", {
        auto data = site1.clone;

        data["feature"]["info"] = `{
          "createdOn": "1999-01-01T00:00:00Z",
          "author": "@someone",
          "originalAuthor": "@didibao",
          "lastChangeOn": "1998-01-01T00:00:00Z"
        }`.parseJsonString;

        router
          .request
          .put("/features/000000000000000000000001")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["feature"]["info"].should.equal(parseJsonString(`{
              "createdOn": "1999-01-01T00:00:00Z",
              "originalAuthor": "@didibao",
              "author": "000000000000000000000005",
              "lastChangeOn": "` ~ response.bodyJson["feature"]["info"]["lastChangeOn"].to!string ~ `",
              "changeIndex": 1 }`));
          });
      });

      it("should not change the feature originalAuthor", {
        auto data = site1.clone;

        data["feature"]["info"] = `{
          "createdOn": "1999-01-01T00:00:00Z",
          "author": "@someone",
          "originalAuthor": "@didibao",
          "lastChangeOn": "1998-01-01T00:00:00Z"
        }`.parseJsonString;

        router
          .request
          .put("/features/000000000000000000000001")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["feature"]["info"]["originalAuthor"].should.equal("@didibao");
            response.bodyJson["feature"]["info"]["author"].should.equal("000000000000000000000005");
          });
      });

      it("removes the unmasked field when is set", {
        auto item5 = crates.feature.getItem("000000000000000000000005").and.exec.front;
        item5["unmasked"] = item5["position"];
        crates.feature.updateItem(item5);

        router
          .request
          .put("/features/000000000000000000000005")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .send(site5)
          .expectStatusCode(200)
          .end((Response response) => () {
            auto item = crates.feature.getItem("000000000000000000000005").and.exec.front;

            item["unmasked"].type.should.equal(Json.Type.undefined);
          });
      });

      describe("for a feature with a picture", {
        Json result;

        beforeEach({
          Picture picture;
          picture.name = "custom picture";
          picture.owner = "";
          picture.picture = new PictureFile;

          auto result = crates.picture.addItem(picture.serializeToJson);
          picture._id = ObjectId.fromString(result["_id"].to!string);

          auto feature = crates.feature.getItem("000000000000000000000001").exec.front;
          feature["pictures"] = Json.emptyArray;
          feature["pictures"] ~= result["_id"];

          crates.feature.updateItem(feature);
        });

        it("should delete the original picture", {
          auto data = site1.clone;

          data["feature"]["pictures"] = Json.emptyArray;
          data["feature"]["pictures"] ~= "000000000000000000000001";

          router
            .request
            .put("/features/000000000000000000000001")
            .send(data)
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              crates.feature.get.exec
                .filter!(a => a["name"] == "custom picture")
                  .array.length.should.equal(0);
            });
        });

        it("should not delete the original picture if it's not removed from the picture list", {
          auto data = site1.clone;

          data["feature"]["pictures"] ~= "000000000000000000000003";
          data["feature"]["pictures"] ~= "000000000000000000000001";

          router
            .request
            .put("/features/000000000000000000000001")
            .send(data)
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              crates.picture.get.exec
                .filter!(a => a["name"] == "custom picture")
                  .array.length.should.equal(1);
            });
        });
      });

      describe("for a feature with required attributes", {
        Json result;

        beforeEach({
          auto icon = crates.icon.getItem("000000000000000000000001").exec.front;
          icon["attributes"][0]["isRequired"] = true;
          icon["attributes"][1]["isRequired"] = true;
          crates.icon.updateItem(icon);

          auto feature = crates.feature.getItem("000000000000000000000001").exec.front;
          feature["attributes"] = `{"name1": {"phone": "123456", "program": "Luni-Vineri:9-18"}}`.parseJsonString;

          crates.feature.updateItem(feature);
        });

        it("should not allow editing sites resulting in missing required attributes", {
          auto data = site1.clone;
          data["feature"]["attributes"] = `{}`.parseJsonString;

          router
            .request
            .put("/features/000000000000000000000001")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .send(data)
            .expectStatusCode(400)
            .end((Response response) => () {

              response.bodyJson.should.equal("{\"errors\": [{
                \"description\": \"The item can not be added. Required icon attribute phone is not set for name1.\",
                \"status\": 400,
                \"title\": \"Validation error\"
              }]}".parseJsonString);
            });
        });

        it("should not allow editing the site when validation query param is false", {
          auto data = site1.clone;
          data["feature"]["attributes"] = `{}`.parseJsonString;
          auto expectedFeature = `{
            "feature": {
              "info": {
                "changeIndex": 1,
                "createdOn": "2015-01-01T00:00:00Z",
                "lastChangeOn": "2021-08-08T17:31:33Z",
                "originalAuthor": "000000000000000000000001",
                "author": "000000000000000000000005"
              },
              "computedVisibility": {
                "isDefault": false,
                "isPublic": false,
                "team": "000000000000000000000001"
              },
              "visibility": 0,
              "contributors": [],
              "maps": [ "000000000000000000000001" ],
              "name": "new site",
              "_id": "000000000000000000000001",
              "canEdit": true,
              "icons": [ "000000000000000000000001" ],
              "decorators": {
                "center": [],
                "showAsLineAfterZoom": 99,
                "showLineMarkers": false,
                "useDefault": true,
                "keepWhenSmall": true,
                "changeIndex": 0,
                "minZoom": 0,
                "maxZoom": 20
              },
              "source": {
                "type": "",
                "remoteId": "",
                "modelId": "",
                "syncAt": "0001-01-01T00:00:00+00:00"
              },
              "description": {
                "blocks": [{
                  "type": "paragraph",
                  "data": {
                    "text": "description of site1"
                  }
                }
              ]},
              "attributes": {},
              "isLarge": false,
              "position": { "type": "Point", "coordinates": [ 1.5, 1.5 ] },
              "pictures": [],
              "sounds": []
            }
          }`.parseJsonString;

          router
            .request
            .put("/features/000000000000000000000001?validation=false")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .send(data)
            .expectStatusCode(200)
            .end((Response response) => () {
              expectedFeature["feature"]["info"] = response.bodyJson["feature"]["info"];
              response.bodyJson.should.equal(expectedFeature);
            });
        });
      });
    });

    describe("with an owner token", {
      it("should not accept issueCount field", {
        auto data = site1.clone;
        data["feature"]["issueCount"] = 3;

        router
          .request
          .put("/features/000000000000000000000001")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .send(data)
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"errors\": [{
              \"description\": \"You must not send the `feature.issueCount` field.\",
              \"status\": 400,
              \"title\": \"Validation error\"
            }]}".parseJsonString);
          });
      });

      it("should not accept canEdit field", {
        auto data = site1.clone;
        data["feature"]["canEdit"] = true;

        router
          .request
          .put("/features/000000000000000000000001")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .send(data)
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"errors\": [{
              \"description\": \"You must not send the `feature.canEdit` field.\",
              \"status\": 400,
              \"title\": \"Validation error\"
            }]}".parseJsonString);
          });
      });

      it("should not change a site from a map that is not owned", {
        auto data = `{"feature": {
            "_id":"000000000000000000000004",
            "name": "new site",
            "visibility": 1,
            "position":{"coordinates":[1.5,1.5],"type":"Point"},
            "icons":["000000000000000000000001"],
            "description":"description of site1",
            "pictures":[],
            "attributes": {},
            "maps":["000000000000000000000001"]}}`.parseJsonString;

        router
          .request
          .put("/features/000000000000000000000004")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .send(data)
          .expectStatusCode(404)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"errors\": [ {
              \"description\": \"Item `000000000000000000000004` not found.\",
              \"status\": 404,
              \"title\": \"Crate not found\"
            }]}".parseJsonString);
          });
      });

      it("should require the `feature` object", {
        auto data = `{}`.parseJsonString;

        router
          .request
          .put("/features/000000000000000000000001")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .send(data)
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"errors\": [{
              \"description\": \"object type expected to be `feature`\",
              \"status\": 400,
              \"title\": \"Validation error\"
            }]}".parseJsonString);
          });
      });

      it("should require the `feature` object fields", {
        auto data = `{"feature": {}}`.parseJsonString;

        router
          .request
          .put("/features/000000000000000000000001")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .send(data)
          .expectStatusCode(400)
          .end((Response response) => () {
            string strJson = `{"_id":"000000000000000000000001","info":{"changeIndex":0,"createdOn":"2015-01-01T00:00:00Z","lastChangeOn":"2015-01-01T00:00:00Z","originalAuthor":"000000000000000000000001","author":""},"computedVisibility":{"isDefault":false,"isPublic":true,"team":"000000000000000000000001"}}`.replace(`"`, `\"`);

            response.bodyJson.should.equal(("{\"errors\": [{
              \"description\": \"The `Feature.name` field is required: " ~ strJson ~ "\",
              \"status\": 400,
              \"title\": \"Validation error\"
            }]}").parseJsonString);
          });
      });
    });

    describe("with a leader token", {
      it("should not accept issueCount field", {
        auto data = site1.clone;
        data["feature"]["issueCount"] = 1;

        router
          .request
          .put("/features/000000000000000000000001")
          .header("Authorization", "Bearer " ~ bearerLeaderToken.name)
          .send(data)
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"errors\": [{
              \"description\": \"You must not send the `feature.issueCount` field.\",
              \"status\": 400,
              \"title\": \"Validation error\"
            }]}".parseJsonString);
          });
      });

      it("should not accept canEdit field", {
        auto data = site1.clone;
        data["feature"]["canEdit"] = true;

        router
          .request
          .put("/features/000000000000000000000001")
          .header("Authorization", "Bearer " ~ bearerLeaderToken.name)
          .send(data)
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"errors\": [{
              \"description\": \"You must not send the `feature.canEdit` field.\",
              \"status\": 400,
              \"title\": \"Validation error\"
            }]}".parseJsonString);
          });
      });

      it("should not accept computedVisibility field", {
        auto data = site1.clone;
        data["feature"]["computedVisibility"] = Json.emptyObject;

        router
          .request
          .put("/features/000000000000000000000001")
          .header("Authorization", "Bearer " ~ bearerLeaderToken.name)
          .send(data)
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"errors\": [{
              \"description\": \"You must not send the `feature.computedVisibility` field.\",
              \"status\": 400,
              \"title\": \"Validation error\"
            }]}".parseJsonString);
          });
      });

      it("should not change a site from a map that is not owned", {
        auto data = `{"feature": {
            "_id":"000000000000000000000004",
            "name": "new site",
            "visibility": 1,
            "position":{"coordinates":[1.5,1.5],"type":"Point"},
            "icons":["000000000000000000000001"],
            "description":"description of site1",
            "pictures":[],
            "attributes": {},
            "maps":["000000000000000000000001"]}}`.parseJsonString;

        router
          .request
          .put("/features/000000000000000000000004")
          .header("Authorization", "Bearer " ~ bearerLeaderToken.name)
          .send(data)
          .expectStatusCode(404)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"errors\": [ {
              \"description\": \"Item `000000000000000000000004` not found.\",
              \"status\": 404,
              \"title\": \"Crate not found\"
            }]}".parseJsonString);
          });
      });

      it("should require the `feature` object", {
        auto data = `{}`.parseJsonString;

        router
          .request
          .put("/features/000000000000000000000001")
          .header("Authorization", "Bearer " ~ bearerLeaderToken.name)
          .send(data)
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"errors\": [{
              \"description\": \"object type expected to be `feature`\",
              \"status\": 400,
              \"title\": \"Validation error\"
            }]}".parseJsonString);
          });
      });

      it("should require the `feature` object fields", {
        auto data = `{"feature": {}}`.parseJsonString;

        router
          .request
          .put("/features/000000000000000000000001")
          .header("Authorization", "Bearer " ~ bearerLeaderToken.name)
          .send(data)
          .expectStatusCode(400)
          .end((Response response) => () {
            string strJson = `{"_id":"000000000000000000000001","info":{"changeIndex":0,"createdOn":"2015-01-01T00:00:00Z","lastChangeOn":"2015-01-01T00:00:00Z","originalAuthor":"000000000000000000000001","author":""},"computedVisibility":{"isDefault":false,"isPublic":true,"team":"000000000000000000000001"}}`.replace(`"`, `\"`);

            response.bodyJson.should.equal(("{\"errors\": [{
              \"description\": \"The `Feature.name` field is required: " ~ strJson ~ "\",
              \"status\": 400,
              \"title\": \"Validation error\"
            }]}").parseJsonString);
          });
      });
    });

    describe("with a member token", {
      it("should not change a site from an owned map", {
        router
          .request
          .put("/features/000000000000000000000001")
          .header("Authorization", "Bearer " ~ bearerMemberToken.name)
          .send(site1)
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"errors\": [{
              \"description\": \"You don't have enough rights to edit `000000000000000000000001`.\",
              \"status\": 403,
              \"title\": \"Forbidden\"
            }]}".parseJsonString);
          });
      });

      it("should change a feature from an owned map if the user is contributor", {
        site5["feature"]["name"] = "site5 new";

        router
          .request
          .put("/features/000000000000000000000005")
          .header("Authorization", "Bearer " ~ bearerMemberToken.name)
          .send(site5)
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal("{
              \"errors\":[
                {\"description\":\"You don't have enough rights to edit `000000000000000000000005`.\",\"status\":403,\"title\":\"Forbidden\"}
              ]
            }".parseJsonString);
          });
      });

      it("should not change a feature from a map that is not owned", {
        auto data = `{"feature": {
            "_id":"000000000000000000000004",
            "name": "new site",
            "visibility": 1,
            "position":{"coordinates":[1.5,1.5],"type":"Point"},
            "icons":["000000000000000000000001"],
            "description":"description of site1",
            "pictures":[],
            "attributes": {},
            "maps":["000000000000000000000001"]}}`.parseJsonString;

        router
          .request
          .put("/features/000000000000000000000004")
          .header("Authorization", "Bearer " ~ bearerMemberToken.name)
          .send(data)
          .expectStatusCode(404)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"errors\": [ {
              \"description\": \"Item `000000000000000000000004` not found.\",
              \"status\": 404,
              \"title\": \"Crate not found\"
            }]}".parseJsonString);
          });
      });
    });

    describe("with a guest token", {
      beforeEach({
        site5 = Json.emptyObject;
        site5["feature"] = createFeature(
          Feature(ObjectId.fromString("5"),
              [ Map(ObjectId.fromString("1")) ],
              "site5",
              Json("description of site5"),
              GeoJsonGeometry.fromJson(`{ "type": "Point", "coordinates": [1.5, 1.5] }`.parseJsonString),
              ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z")),
              [],
              [],
              [],
              VisibilityEnum.Private, ["000000000000000000000003"]));

        site5["feature"].remove("issueCount");
        site5["feature"].remove("computedVisibility");
        site5["feature"].remove("contributors");
      });

      it("should not change a feature from an owned map", {
        router
          .request
          .put("/features/000000000000000000000001")
          .header("Authorization", "Bearer " ~ bearerGuestToken.name)
          .send(site1)
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"errors\": [{
              \"description\": \"You don't have enough rights to edit `000000000000000000000001`.\",
              \"status\": 403,
              \"title\": \"Forbidden\"
            }]}".parseJsonString);
          });
      });

      it("should not change a feature from an owned map if the user is contributor", {
        router
          .request
          .put("/features/000000000000000000000005")
          .header("Authorization", "Bearer " ~ bearerGuestToken.name)
          .send(site5)
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"errors\": [{
              \"description\": \"You don't have enough rights to edit `000000000000000000000005`.\",
              \"status\": 403,
              \"title\": \"Forbidden\"
            }]}".parseJsonString);
          });
      });

      it("should not change a feature from a map that is not owned", {
        auto data = `{"feature": {
            "_id":"000000000000000000000004",
            "name": "new site",
            "visibility": 0,
            "position":{"coordinates":[1.5,1.5],"type":"Point"},
            "icons":["000000000000000000000001"],
            "description":"description of site1",
            "pictures":[],
            "attributes": {},
            "maps":["000000000000000000000001"]}}`.parseJsonString;

        router
          .request
          .put("/features/000000000000000000000004")
          .header("Authorization", "Bearer " ~ bearerGuestToken.name)
          .send(data)
          .expectStatusCode(404)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"errors\": [ {
              \"description\": \"Item `000000000000000000000004` not found.\",
              \"status\": 404,
              \"title\": \"Crate not found\"
            }]}".parseJsonString);
          });
      });
    });

    describe("updating the sound list", {
      Json invalidSound;
      Json validSound;
      Json otherValidSound;

      beforeEach({
        auto info = ModelInfo(SysTime.fromISOExtString("2015-01-01T00:00:00Z"), SysTime.fromISOExtString("2015-01-01T00:00:00Z"));
        invalidSound = createSound(Sound(ObjectId.fromString("1"), "name1", VisibilityOptional(Team(ObjectId.fromString("3")), true), info, new SoundFile));
        validSound = createSound(Sound(ObjectId.fromString("2"), "name2", VisibilityOptional(Team(ObjectId.fromString("1")), true), info, new SoundFile));
        otherValidSound = createSound(Sound(ObjectId.fromString("3"), "name3", VisibilityOptional(Team(ObjectId.fromString("1")), true), info, new SoundFile));
      });

      it("should not allow adding sounds from other teams to features that don't have access to any of the map", {
        auto data = site1.clone;
        data["feature"]["sounds"] = Json.emptyArray;
        data["feature"]["sounds"].appendArrayElement(invalidSound["_id"]);

        router
          .request
          .put("/features/000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .send(data)
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal((`{"errors": [{
              "description": "The sound with id ` ~ "`" ~ invalidSound["_id"].to!string ~ "`" ~ ` can not be added to this feature.",
              "status": 400,
              "title": "Validation error"
            }]}`).parseJsonString);
        });
      });

      it("should allow adding sounds to features that have the first map ", {
        auto data = `{"feature": {
          "name": "new site",
          "position":{"coordinates":[ 1.5,1.5 ],"type":"Point"},
          "icons":["000000000000000000000001"],
          "description":"description of site1",
          "pictures":[],
          "sounds": ["000000000000000000000002"],
          "maps":["000000000000000000000001"]}}`.parseJsonString;

        router
          .request
          .put("/features/000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["feature"]["sounds"].should.equal(`["000000000000000000000002"]`.parseJsonString);
          });
      });

      it("should delete an old sound when is replaced", {
        auto feature = crates.feature.getItem("000000000000000000000001").and.exec.front;
        feature["sounds"] = Json.emptyArray;
        feature["sounds"] ~= "000000000000000000000002";
        crates.feature.updateItem(feature);

        auto data = `{"feature": {
          "name": "new site",
          "position":{"coordinates":[ 1.5,1.5 ],"type":"Point"},
          "icons":["000000000000000000000001"],
          "description":"description of site1",
          "pictures":[],
          "sounds": ["000000000000000000000003"],
          "maps":["000000000000000000000001"]}}`.parseJsonString;

        router
          .request
          .put("/features/000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["feature"]["sounds"].should.equal(`["000000000000000000000003"]`.parseJsonString);
            crates.sound.get.where("_id").equal("000000000000000000000002").and.size.should.equal(0);
          });
      });
    });
  });
});
