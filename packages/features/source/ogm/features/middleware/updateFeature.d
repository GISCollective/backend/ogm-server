module ogm.features.middleware.updateFeature;

import crate.base;
import crate.error;
import ogm.crates.all;
import ogm.features.visibility;
import ogm.http.request;
import std.exception;
import std.algorithm;
import std.array;
import vibe.data.json;
import vibe.http.server;

class UpdateFeatureMiddleware {

  OgmCrates crates;

  ///
  this(OgmCrates crates) {
    this.crates = crates;
  }

  @put @patch @create
  void validateFields(HTTPServerRequest req, Json item) {
    auto request = RequestUserData(req);

    enforce!CrateValidationException("feature" in req.json, "You must send a feature.");
    enforce!CrateValidationException("issueCount" !in req.json["feature"], "You must not send the `feature.issueCount` field.");
    enforce!CrateValidationException("canEdit" !in req.json["feature"], "You must not send the `feature.canEdit` field.");
    enforce!CrateValidationException("unmasked" !in req.json["feature"], "You must not send the `feature.unmasked` field.");
    enforce!CrateValidationException("contributors" !in req.json["feature"], "You must not send the `feature.contributors` field.");

    if(request.isAdmin) {
      return;
    }

    auto session = request.session(crates);
    auto teams = session.owner.teams ~ session.leader.teams ~ session.member.teams;

    string teamId;

    if(req.json["feature"]["computedVisibility"].type == Json.Type.object) {
      teamId = req.json["feature"]["computedVisibility"]["team"].to!string;
    }

    if(item.type == Json.Type.object && item["computedVisibility"].type == Json.Type.object) {
      teamId = item["computedVisibility"]["team"].to!string;
    }

    bool hasMoreRights = teams.canFind(teamId);
    int isPublic;

    if(item.type == Json.Type.object) {
      isPublic = "visibility" !in item || item["visibility"].to!int;
    }

    if(!hasMoreRights && "visibility" in req.json["feature"]) {
      auto nextisPublic = req.json["feature"]["visibility"].to!int;
      enforce!CrateValidationException(isPublic == nextisPublic, nextisPublic ? "The item can not be unpublished by you." : "The item can not be published by you.");
    }
  }
}