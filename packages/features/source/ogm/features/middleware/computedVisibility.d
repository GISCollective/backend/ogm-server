module ogm.features.middleware.computedVisibility;

import crate.base;
import ogm.crates.all;
import ogm.features.visibility;
import vibe.data.json;
import vibe.http.server;
import std.exception;
import crate.error;
import ogm.http.request;
import std.algorithm;
import std.array;
import std.conv;

class UpdateFeatureComputedVisibilityMiddleware {

  struct Parameters {
    string visibility;
  }

  OgmCrates crates;

  ///
  this(OgmCrates crates) {
    this.crates = crates;
  }

  @getList
  void getSelector(Parameters params, IQuery selector, HTTPServerRequest req) {
    auto request = RequestUserData(req);

    if(params.visibility) {
      enforce!ForbiddenException(params.visibility == "-1" || params.visibility == "1" || params.visibility == "0", "You provided an invalid value for the `visibility` parameter.");
      enforce!ForbiddenException(request.userId != "@anonymous" && request.userId != "", "You can't use the `visibility` parameter, without an auth token");
      selector.where("visibility").equal(params.visibility.to!long);
    }
  }

  @put @patch
  void updateVisibility(HTTPServerRequest req) {
    if("feature" !in req.json) {
      return;
    }

    enforce!CrateValidationException("computedVisibility" !in req.json["feature"], "You must not send the `feature.computedVisibility` field.");

    if("maps" !in req.json["feature"]) {
      return;
    }

    enforce!CrateValidationException(req.json["feature"]["maps"].length > 0, "You must set a map to the feature.");

    req.json["feature"]["computedVisibility"] = crates.getComputedVisibility(req.json["feature"]);
  }
}

class CreateFeatureComputedVisibilityMiddleware {

  OgmCrates crates;

  ///
  this(OgmCrates crates) {
    this.crates = crates;
  }

  @create
  void addVisibility(HTTPServerRequest req) {
    auto request = RequestUserData(req);

    if("feature" !in req.json) {
      return;
    }
    enforce!CrateValidationException("computedVisibility" !in req.json["feature"], "You must not send the `feature.computedVisibility` field.");

    enforce!CrateValidationException("maps" in req.json["feature"], "You must set a map to the feature.");
    enforce!CrateValidationException(req.json["feature"]["maps"].length > 0, "You must set a map to the feature.");

    auto map = crates.map.getItem(req.json["feature"]["maps"][0].to!string).and.exec.front;

    if(map["addFeaturesAsPending"] == true) {
      req.json["feature"]["visibility"] = -1;
    }

    auto session = request.session(crates);

    if(!request.isAdmin) {
      auto strMaps = req.json["feature"]["maps"].byValue.map!(a => a.to!string).array;
      auto outsideMaps = strMaps.filter!(a => !session.all.maps.canFind(a)).map!(a => ObjectId(a)).array;

      auto privateMaps = crates.map.get
        .where("_id").anyOf(outsideMaps).and
        .where("visibility.isPublic").equal(false).and
        .size;

      enforce!CrateNotFoundException(privateMaps == 0, "The map does not exist.");
    }

    req.json["feature"]["computedVisibility"] = crates.getComputedVisibility(req.json["feature"]);
  }
}