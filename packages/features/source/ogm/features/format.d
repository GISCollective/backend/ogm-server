/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.features.format;

import std.datetime;
import std.conv;
import std.algorithm;
import std.array;

import crate.base;
import crate.error;
import crate.attributes : create, replace, update_ = update;

import vibe.data.json;
import vibe.http.server;

import ogm.crates.all;
import ogm.models.feature;
import ogm.features.formats.normalize;
import ogm.features.formats.csv;
import ogm.features.formats.geojson;
import ogm.features.formats.gpx;
import ogm.features.middlewares.FeatureMaskMapper;
import ogm.filter.visibility;
import ogm.features.middlewares.mapper;
import ogm.http.request;

import geo.gpx;

///
class FormatMiddleware {

  struct Parameters {
    @describe("Get the sites in a different format.")
    @example("csv", "Get the response data in a csv format.")
    @example("geojson", "Get the response data in a GeoJson format.")
    @example("gpx", "Get the response data in a GPX format.")
    string format;
  }

  private {
    OgmCrates crates;
    FeatureMaskMapper maskMapper;
    VisibilityFilter!("features", true, true) visibility;
    FeatureMapper featureMapper;
  }

  ///
  this(OgmCrates crates, VisibilityFilter!("features", true, true) visibility, FeatureMaskMapper maskMapper, FeatureMapper featureMapper) {
    this.crates = crates;
    this.maskMapper = maskMapper;
    this.visibility = visibility;
    this.featureMapper = featureMapper;
  }

  /// It checks the position.type field and converts the data to geojson
  @create @replace @update_
  void normalizePosition(HTTPServerRequest req) {
    if("feature" !in req.json || "position" !in req.json["feature"]) {
      return;
    }

    req.json["feature"]["position"] = convertToGeoJson(req.json["feature"]["position"]);
  }

  /// It processes all selected items and flushes them as the requested format
  @get
  IQuery get(IQuery selector, Parameters parameters, HTTPServerRequest request, HTTPServerResponse response) {
    if(parameters.format == "") {
      return selector;
    }

    string fileName;

    if("map" in request.query) {
      fileName = crates.map.getItem(request.query["map"]).exec.front["name"].to!string;
    } else {
      fileName = "feature-list";
    }

    fileName ~= "-" ~ Clock.currTime.toISOExtString;

    response.headers["content-disposition"] = `attachment; filename="` ~ fileName ~ `.` ~ request.query["format"] ~ `"`;

    if(parameters.format == "csv") {
      return respondCsv(selector, request, response);
    }

    if(parameters.format == "geojson") {
      return respondGeojson(selector, request, response);
    }

    if(parameters.format == "gpx") {
      return respondGpx(selector, request, response);
    }

    throw new CrateValidationException("The `" ~ parameters.format ~ "` is not supported.");
  }

  IQuery respondCsv(IQuery selector, HTTPServerRequest req, HTTPServerResponse response) {
    void mapper(ref Json item) {
      visibility.mapper(req, item);
      maskMapper.maskMapper(req, item);
      featureMapper.mapper(req, item);
      item.remove("canEdit");
      item.remove("decorators");

      if("source" in item && "type" in item["source"] && item["source"]["type"] == "Campaign") {
        auto answerRange = crates.campaignAnswer.get.where("_id").equal(ObjectId(item["source"]["remoteId"].to!string)).and.exec;

        if(!answerRange.empty) {
          auto answer = answerRange.front;
          item["attributes"]["contributor"] = answer["contributor"];
        }
      }
    }

    Json[] icons;

    string[] extraHeaders;

    auto request = RequestUserData(req);
    auto session = request.session(crates);
    auto allMaps = session.all.maps;

    if("map" in req.query) {
      auto addContributor = request.isAdmin || allMaps.canFind(req.query["map"]);

      auto range = crates.meta.get
        .where("model").equal("Map").and
        .where("type").equal("Map.icons").and
        .where("itemId").equal(req.query["map"]).and
        .exec;

      if(!range.empty) {
        ObjectId[] ids = range.front["data"]["icons"].deserializeJson!(string[]).map!(a => ObjectId.fromString(a)).array;
        icons = crates.icon.get.where("_id").containsAny(ids).and.exec.array;
      }

      if(addContributor) {
        auto surveys = crates.campaign.get
          .where("map.map").equal(req.query["map"]).and
          .where("map.isEnabled").equal(true).and
          .exec;

        foreach(survey; surveys) {
          if(survey["contributorQuestions"].type == Json.Type.array) {
            foreach(question; survey["contributorQuestions"]) {
              extraHeaders ~= "attributes.contributor." ~ question["name"].to!string;
            }
          }
        }
      }
    }

    auto range = selector.toCsv(icons, &mapper, extraHeaders);

    response.contentType = "text/csv";
    response.statusCode = 200;

    foreach(chunk; range) {
      response.bodyWriter.write(chunk);
    }

    response.bodyWriter.finalize;
    return selector;
  }

  IQuery respondGeojson(IQuery selector, HTTPServerRequest request, HTTPServerResponse response) {
    void mapper(ref Json item) {
      visibility.mapper(request, item);
      maskMapper.maskMapper(request, item);
      featureMapper.mapper(request, item);
      item.remove("canEdit");
    }

    auto range = selector.toGeoJson(&mapper);

    response.contentType = "application/geo+json";
    response.statusCode = 200;

    response.bodyWriter.write(`{"type":"FeatureCollection","features":[`);
    foreach(chunk; range) {
      response.bodyWriter.write(chunk.to!string);
    }
    response.bodyWriter.write(`]}`);

    response.bodyWriter.finalize;
    return selector;
  }

  IQuery respondGpx(IQuery selector, HTTPServerRequest request, HTTPServerResponse response) {
    void mapper(ref Json item) {
      visibility.mapper(request, item);
      maskMapper.maskMapper(request, item);
      featureMapper.mapper(request, item);
      item.remove("canEdit");
    }

    auto range = selector.toGPX(&mapper);

    response.contentType = GPX.mime;
    response.statusCode = 200;

    GPX gpx;
    gpx.creator = "GISCollective";

    response.bodyWriter.write(gpx.xmlHeader);
    response.bodyWriter.write(gpx.openTag);
    response.bodyWriter.write(gpx.metadata.toString);

    foreach(chunk; range) {
      response.bodyWriter.write(chunk.to!string);
    }
    response.bodyWriter.write(gpx.closeTag);

    response.bodyWriter.finalize;
    return selector;
  }
}
