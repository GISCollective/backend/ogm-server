/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.features.operations.BulkDeleteOperation;

import crate.base;
import crate.http.wrapper;
import crate.http.operations.base;
import crate.auth.middleware;
import crate.error;

import ogm.rights;
import ogm.crates.all;
import ogm.http.request;
import ogm.middleware.ResourceMiddleware;
import ogm.operations.BulkOperation;

import std.algorithm;
import std.array;

import vibe.data.json;
import vibe.http.server;

class BulkDeleteOperation : BulkOperation!("map", true) {
  private {
    IResourceMiddleware[] resourceMiddlewares;
  }

  this(OgmCrates crates, PrivateDataMiddleware privateData, IResourceMiddleware[] resourceMiddlewares) {
    this.resourceMiddlewares = resourceMiddlewares;

    CrateRule rule;

    rule.request.path = "/features";
    rule.request.method = HTTPMethod.DELETE;

    super(crates, crates.feature, privateData, rule);
  }

  override void performOperation(ref Json item, RequestUserData request, DefaultStorage storage, bool isOwner, bool isAuthor) {
    scope Exception validation;

    if(!request.isAdmin) {
      auto maps = item["maps"].byValue.map!(a => a.to!string).array;
      auto rights = request.session(crates).deleteItemRights(maps);
      validation = rights.validate;
    }

    bool canDelete = request.isAdmin || validation is null;

    if(canDelete) {
      foreach (middleware; resourceMiddlewares) {
        middleware.delete_(storage.request);
      }

      crate.deleteItem(item["_id"].to!string);
    }
  }
}