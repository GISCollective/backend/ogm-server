/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.features.operations.BulkPublishOperation;

import crate.base;
import crate.http.wrapper;
import crate.http.operations.base;
import crate.auth.middleware;
import crate.error;

import ogm.rights;
import ogm.crates.all;
import ogm.http.request;
import ogm.middleware.ResourceMiddleware;
import ogm.operations.BulkOperation;

import std.algorithm;
import std.array;

import vibe.data.json;
import vibe.http.server;

class BulkPublishOperation : BulkOperation!("map", true) {
  private {
    IResourceMiddleware[] resourceMiddlewares;
  }

  this(OgmCrates crates, PrivateDataMiddleware privateData) {
    this.resourceMiddlewares = resourceMiddlewares;

    CrateRule rule;

    rule.request.path = "/features/publish";
    rule.request.method = HTTPMethod.POST;

    super(crates, crates.feature, privateData, rule);
  }

  override void performOperation(ref Json item, RequestUserData request, DefaultStorage storage, bool isOwner, bool isAuthor) {
    scope rights = request.session(crates).replaceItemRights(item);
    scope validation = rights !is null ? rights.validate : null;

    bool canUpdate = validation is null && item["visibility"] != 1;

    if(canUpdate) {
      item["visibility"] = 1;
      crate.updateItem(item);
    }
  }
}