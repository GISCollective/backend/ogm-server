/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.features.api;

import vibe.http.router;
import crate.http.router;
import crate.base;

import ogm.auth;
import ogm.crates.all;

import ogm.filter.visibility;
import ogm.middleware.userdata;
import ogm.filter.pagination;

import ogm.middleware.adminrequest;
import ogm.filter.searchterm;
import ogm.filter.searchtag;
import ogm.middleware.modelinfo;
import ogm.middleware.ResourceMiddleware;
import ogm.middleware.attributes.IconAttributesValidator;
import ogm.middleware.features.GetFeaturesFilter;
import ogm.middleware.SortingMiddleware;

import ogm.features.middlewares.mapper;
import ogm.features.middlewares.FeatureSoundsMiddleware;
import ogm.features.middlewares.FeatureAttributesMiddleware;
import ogm.features.middlewares.FeatureMaskMapper;
import ogm.features.operations.BulkDeleteOperation;
import ogm.features.operations.BulkPublishOperation;
import ogm.features.operations.BulkPendingOperation;
import ogm.features.operations.BulkUnpublishOperation;
import ogm.features.format;
import ogm.middleware.GlobalAccess;
import ogm.icons.IconCache;
import ogm.icons.TaxonomyTree;
import ogm.features.middleware.computedVisibility;
import ogm.filter.mapFilter;
import ogm.middleware.IconFieldMapper;
import ogm.features.middleware.updateFeature;

///
void setupFeaturesApi(T)(CrateRouter!T crateRouter, OgmCrates crates) {
  auto auth = Authentication.instance(crates.user);
  auto userDataMiddleware = UserDataMiddleware.instance(crates);
  auto paginationFilter = new PaginationFilter;

  auto cache = new IconCache(&crates.icon.get, &crates.iconSet.getItem);
  auto iconTree = new TaxonomyTree(&crates.icon.get, &crates.iconSet.getItem, cache);

  auto globalAccess = GlobalAccessMiddleware.instance(crates);
  auto adminRequest = new AdminRequest(crates.user);
  auto searchTermFilter = new SearchTermFilter;
  auto searchTagFilter = new SearchTagFilter;
  auto featuresFilter = new GetFeaturesFilter(auth.getCollection, crates, iconTree);
  auto updateFeatureFilter = new UpdateFeatureMiddleware(crates);
  auto featureMapper = new FeatureMapper(crates);
  auto featureSoundsMiddleware = new FeatureSoundsMiddleware(crates);
  auto modelInfo = new ModelInfoMiddleware("feature", crates.feature);
  auto featureMaskMapper = new FeatureMaskMapper(crates);
  auto visibility = new VisibilityFilter!("features", true, true)(crates, crates.feature);
  auto mapFilter = new MapFilter(crates);
  auto formatMiddleware = new FormatMiddleware(crates, visibility, featureMaskMapper, featureMapper);
  auto picturesMiddleware = new ResourceMiddleware!("feature", "pictures")(crates.feature, crates.picture);
  auto soundsMiddleware = new ResourceMiddleware!("feature", "sounds")(crates.feature, crates.sound);
  auto attributesMiddleware = new FeatureAttributesMiddleware(crates, iconTree);
  auto iconAttributesValidator = new IconAttributesValidator(iconTree, "feature");
  auto sorting = new SortingMiddleware(crates, "info.lastChangeOn", -1);
  auto updateComputedVisibility = new UpdateFeatureComputedVisibilityMiddleware(crates);
  auto createComputedVisibility = new CreateFeatureComputedVisibilityMiddleware(crates);
  auto iconFieldMapper = new IconFieldMapper(crates);

  crateGetters["Map"] = &crates.map.getItem;
  crateGetters["Team"] = &crates.team.getItem;
  crateGetters["Picture"] = &crates.picture.getItem;
  crateGetters["Sound"] = &crates.sound.getItem;
  crateGetters["IconSet"] = &crates.iconSet.getItem;
  crateGetters["Icon"] = &crates.icon.getItem;

  IResourceMiddleware[] resourceMiddlewares = cast(IResourceMiddleware[]) [picturesMiddleware, soundsMiddleware];

  auto bulkDeleteOperation = new BulkDeleteOperation(crates, auth.privateDataMiddleware, resourceMiddlewares);
  auto bulkPublishOperation = new BulkPublishOperation(crates, auth.privateDataMiddleware);
  auto bulkPendingOperation = new BulkPendingOperation(crates, auth.privateDataMiddleware);
  auto bulkUnpublishOperation = new BulkUnpublishOperation(crates, auth.privateDataMiddleware);

  crateRouter.prepare(crates.feature)
    .withCustomOperation(bulkDeleteOperation)
    .withCustomOperation(bulkPublishOperation)
    .withCustomOperation(bulkPendingOperation)
    .withCustomOperation(bulkUnpublishOperation)
    .and(auth.publicContributionMiddleware)
    .and(adminRequest)
    .and(userDataMiddleware)
    .and(globalAccess)
    .and(modelInfo)
    .and(createComputedVisibility)
    .and(updateComputedVisibility)
    .and(visibility)
    .and(updateFeatureFilter)
    .and(featureSoundsMiddleware)
    .and(featuresFilter)
    .and(mapFilter)
    .and(picturesMiddleware)
    .and(soundsMiddleware)
    .and(iconAttributesValidator)
    .and(searchTermFilter)
    .and(searchTagFilter)
    .and(paginationFilter)
    .and(formatMiddleware)
    .and(featureMaskMapper)
    .and(attributesMiddleware)
    .and(sorting)
    .and(iconFieldMapper)
    .and(featureMapper);
}
