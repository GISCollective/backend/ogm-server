/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.users.mapper;

import crate.auth.usercollection;
import crate.base;
import geo.json;
import crate.error;
import crate.collection.memory;
import crate.http.request;

import vibe.http.router;
import vibe.data.json;

import ogm.crates.all;
import ogm.http.request;
import std.array;
import std.algorithm;
import std.range;
import std.exception;
import std.conv;
import std.string;

///
class UserMapper {
  private {
    UserCrateCollection users;
    OgmCrates crates;
  }

  ///
  this(UserCrateCollection users, OgmCrates crates) {
    this.users = users;
    this.crates = crates;
  }

  @mapper
  void mapper(HTTPServerRequest req, ref Json item) @trusted nothrow {
    try {
      scope request = RequestUserData(req);

      if(request.isAuthenticated) {
        item["isAdmin"] = this.users.byId(item["_id"].to!string).can!"admin";
      } else {
        item.remove("email");
      }

      if(!request.isAdmin) {
        item.remove("lastActivity");
      }

      auto profileRange = crates.userProfile.get
          .where("_id").equal(ObjectId.fromJson(item["_id"]))
          .and.exec;

      if(!profileRange.empty) {
        auto profile = profileRange.front;

        string[] pieces;
        if(profile["title"].type == Json.Type.string) {
          pieces ~= profile["title"].to!string;
        }

        if(profile["firstName"].type == Json.Type.string) {
          pieces ~= profile["firstName"].to!string;
        }


        if(profile["lastName"].type == Json.Type.string) {
          pieces ~= profile["lastName"].to!string;
        }

        item["name"] = pieces.join(" ").strip;
      }

      if(item["name"].type != Json.Type.string || item["name"] == "") {
        item["name"] = item["username"];
      }

    } catch(Exception e) {}
  }
}
