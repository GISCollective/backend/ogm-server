/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.users.format;

import std.datetime;
import std.conv;
import std.algorithm;
import std.array;
import std.csv;

import crate.base;
import crate.error;
import crate.json;
import crate.attributes : create, replace, update_ = update;
import crate.collection.csv;

import vibe.data.json;
import vibe.http.server;

import ogm.crates.all;

///
class UserFormatMiddleware {

  struct Parameters {
    @describe("Get the sites in a different format.")
    @example("csv", "Get the response data in a csv format.")
    string format;
  }

  private {
    OgmCrates crates;
  }

  ///
  this(OgmCrates crates) {
    this.crates = crates;
  }

  /// It processes all selected items and flushes them as the requested format
  @get
  IQuery get(IQuery selector, Parameters parameters, HTTPServerRequest request, HTTPServerResponse response) {
    if(parameters.format == "") {
      return selector;
    }

    string fileName;

    fileName ~= "user-list-" ~ Clock.currTime.toISOExtString;

    response.headers["content-disposition"] = `attachment; filename="` ~ fileName ~ `.` ~ request.query["format"] ~ `"`;

    if(parameters.format == "csv") {
      return respondCsv(selector, request, response);
    }

    throw new CrateValidationException("The `" ~ parameters.format ~ "` is not supported.");
  }

  IQuery respondCsv(IQuery selector, HTTPServerRequest request, HTTPServerResponse response) {
    response.contentType = "text/csv";
    response.statusCode = 200;

    static immutable fields = [
      "_id",
      "email",
      "salutation",
      "title",
      "firstName",
      "lastName",
      "bio",
      "jobTitle",
      "organization",
      "joinedTime",
      "createdAt",
      "lastActivity",
      "linkedin",
      "location.detailedLocation.city",
      "location.detailedLocation.country",
      "location.detailedLocation.postalCode",
      "location.detailedLocation.province",
      "location.isDetailed",
      "location.simple",
      "picture",
      "showCalendarContributions",
      "showPrivateContributions",
      "showWelcomePresentation",
      "skype",
      "statusEmoji",
      "statusMessage",
      "twitter",
      "website"
    ];

    foreach(field; fields) {
      response.bodyWriter.write(field);
      response.bodyWriter.write(",");
    }
    response.bodyWriter.write("\n");

    foreach(item; selector.exec) {
      auto profileRange = crates.userProfile.get.where("_id").equal(ObjectId.fromJson(item["_id"])).and.exec;

      auto profile = Json.emptyObject;

      if(!profileRange.empty) {
        profile = profileRange.front;
      }

      profile["email"] = item["email"];
      profile["createdAt"] = item["createdAt"];

      try {
        profile["lastActivity"] = SysTime.fromUnixTime(item["lastActivity"].to!long).toISOExtString;
      } catch(Exception) {}

      string row;

      profile = toFlatJson(profile);

      foreach(field; fields) {
        row ~= escapeCsvValue(profile[field].to!string) ~ ",";
      }

      response.bodyWriter.write(row);
      response.bodyWriter.write("\n");
    }

    response.bodyWriter.finalize;
    return selector;
  }
}
