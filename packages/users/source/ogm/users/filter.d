/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.users.filter;

import crate.auth.usercollection;
import crate.base;
import geo.json;
import crate.error;
import crate.collection.memory;

import vibe.http.router;
import vibe.data.json;

import ogm.http.request;
import ogm.crates.all;

import std.array;
import std.algorithm;
import std.range;
import std.exception;
import std.conv;

///
class UserFilter {
  private {
    OgmCrates crates;
    UserCrateCollection users;
  }


  struct Parameters {
    @describe("If it's true, only the active users will be retrieved.")
    @example("true", "Get only active users.")
    @example("false", "Get all inactive users.")
    @example("", "Get all users.")
    string active;
  }

  IQuery emptySelector;

  ///
  this(UserCrateCollection users, OgmCrates crates) {
    this.crates = crates;
    this.users = users;
    this.emptySelector = new MemoryQuery();
  }

  ///
  @get
  IQuery get(IQuery selector, Parameters parameters, HTTPServerRequest req) {
    auto request = RequestUserData(req);
    auto session = request.session(crates);

    if(parameters.active == "true") {
      selector.where("isActive").equal(true);
    }

    if(parameters.active == "false") {
      selector.where("isActive").equal(false);
    }

    if("term" in req.query && req.query["term"].length < 3) {
      return emptySelector;
    }

    if("term" !in req.query) {
      enforce!UnauthorizedException(request.isAdmin() || request.hasItemId(), "Authorization required");
    }

    return selector;
  }

  /// Middleware applied for put route
  @replace
  void replace(HTTPServerRequest req) {
    auto request = RequestUserData(req);
    enforce!CrateValidationException("user" in req.json, "Object type expected to be `user`.");
    enforce!CrateValidationException("email" in req.json["user"], "The `email` field is missing from the `user` object.");

    auto email = req.json["user"]["email"].to!string;

    if(this.users.contains(email)) {
      enforce!CrateValidationException(this.users[email].id == request.itemId, "The email is already taken.");
    }

    if(!request.isAdmin) {
      enforce!CrateValidationException(request.itemId == request.userId, "You can't edit other users data.");
      enforce!CrateValidationException(request.userId == req.json["user"]["_id"], "You can't edit other users data.");
    }
  }
}
