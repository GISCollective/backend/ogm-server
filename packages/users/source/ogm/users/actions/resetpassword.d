/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.users.actions.resetpassword;

import vibe.data.json;

import std.datetime;
import std.exception;
import std.algorithm;

import vibeauth.collections.usermemory;
import vibeauth.collections.usercollection;
import vibeauth.data.usermodel;
import vibeauth.data.token;
import vibeauth.data.user;

import crate.error;

version(unittest) {
  import fluent.asserts;
}

/// Change an user password with an email and resetPassword token
struct ResetPassword {
  /// The user email
  string email;

  /// The new password
  string password;

  /// A valid `passwordReset` token
  string token;

  /// Collection with the service users
  UserCollection userCollection;

  /// validation function
  void delegate(const UserModel) validation;

  void changePassword() {
    enforce!ForbiddenException(email != "", "The email was not set.");
    enforce!ForbiddenException(userCollection.contains(email), "The email `" ~ email ~ "` was not found.");

    auto user = userCollection[email];

    enforce!ForbiddenException(user.getTokensByType("passwordReset").filter!(a => a.expire > Clock.currTime).map!(a => a.name).canFind(token), "Invalid reset password token.");
    enforce!ForbiddenException(password.length >= 10, "Your password should have at least 10 characters.");

    if(validation !is null) {
      validation(user.toJson.deserializeJson!UserModel);
    }

    user.setPassword(password);

    user
      .getTokensByType("passwordReset")
      .map!(a => a.name)
      .each!(a => user.revoke(a));

    user
      .getTokensByType("activation")
      .map!(a => a.name)
      .each!(a => user.revoke(a));

    user
      .getTokensByType("Bearer")
      .map!(a => a.name)
      .each!(a => user.revoke(a));

    user
      .getTokensByType("Refresh")
      .map!(a => a.name)
      .each!(a => user.revoke(a));
  }
}

/// Throw an exception when the email is not set
unittest {
  auto resetPassword = ResetPassword("");
  resetPassword.userCollection = new UserMemoryCollection([]);

  ({
    resetPassword.changePassword();
  }).should.throwAnyException.withMessage("The email was not set.");
}

/// Throw an exception when the email does not exist
unittest {
  auto resetPassword = ResetPassword("test@test.com");
  resetPassword.userCollection = new UserMemoryCollection([]);

  ({
    resetPassword.changePassword();
  }).should.throwAnyException.withMessage("The email `test@test.com` was not found.");
}

/// Throw an exception when the new password has 9 chars
unittest {
  auto collection = new UserMemoryCollection([]);
  collection.add(new User("test@test.com", "password"));
  auto token = collection.createToken("test@test.com", Clock.currTime + 15.minutes, [], "passwordReset");

  auto resetPassword = ResetPassword("test@test.com", "123456789", token.name);
  resetPassword.userCollection = collection;

  ({
    resetPassword.changePassword();
  }).should.throwAnyException.withMessage("Your password should have at least 10 characters.");
}

/// It should change the user password and remove the `passwordReset` token
unittest {
  auto collection = new UserMemoryCollection([]);
  collection.add(new User("test@test.com", "password"));
  auto token = collection.createToken("test@test.com", Clock.currTime + 15.minutes, [], "passwordReset");

  auto resetPassword = ResetPassword("test@test.com", "1234567890", token.name);
  resetPassword.userCollection = collection;

  resetPassword.changePassword();

  collection["test@test.com"].isValidPassword("1234567890").should.equal(true);
  collection["test@test.com"].isValidPassword("password").should.equal(false);
  collection["test@test.com"].isValidToken(token.name).should.equal(false);
}

/// It should throw on invalid tokens
unittest {
  auto collection = new UserMemoryCollection([]);
  collection.add(new User("test@test.com", "password"));
  auto token = collection.createToken("test@test.com", Clock.currTime + 15.minutes, [], "other");

  auto resetPassword = ResetPassword("test@test.com", "1234567890", token.name);
  resetPassword.userCollection = collection;

  ({
    resetPassword.changePassword();
  }).should.throwAnyException.withMessage("Invalid reset password token.");
}

/// It throw on expired tokens
unittest {
  auto collection = new UserMemoryCollection([]);
  collection.add(new User("test@test.com", "password"));
  auto token = collection.createToken("test@test.com", Clock.currTime - 1.seconds, [], "passwordReset");

  auto resetPassword = ResetPassword("test@test.com", "1234567890", token.name);
  resetPassword.userCollection = collection;

  ({
    resetPassword.changePassword();
  }).should.throwAnyException.withMessage("Invalid reset password token.");
}

/// It should remove the `activation`, `Bearer` and `Refresh` tokens
unittest {
  auto collection = new UserMemoryCollection([]);
  collection.add(new User("test@test.com", "password"));
  auto token = collection.createToken("test@test.com", Clock.currTime + 15.minutes, [], "passwordReset");
  auto token1 = collection.createToken("test@test.com", Clock.currTime + 15.minutes, [], "activation");
  auto token2 = collection.createToken("test@test.com", Clock.currTime + 15.minutes, [], "Bearer");
  auto token3 = collection.createToken("test@test.com", Clock.currTime + 15.minutes, [], "Refresh");

  auto resetPassword = ResetPassword("test@test.com", "1234567890", token.name);
  resetPassword.userCollection = collection;

  resetPassword.changePassword();

  collection["test@test.com"].isValidToken(token1.name).should.equal(false);
  collection["test@test.com"].isValidToken(token2.name).should.equal(false);
  collection["test@test.com"].isValidToken(token3.name).should.equal(false);
}

/// Send the user data to a validation function if it is set
unittest {
  void mockValidation(const UserModel) {
    throw new Exception("Validation failed.");
  }

  auto collection = new UserMemoryCollection([]);
  collection.add(new User("test@test.com", "password"));
  auto token = collection.createToken("test@test.com", Clock.currTime + 15.minutes, [], "passwordReset");
  auto resetPassword = ResetPassword("test@test.com", "1234567890", token.name);
  resetPassword.userCollection = collection;
  resetPassword.validation = &mockValidation;

  ({
    resetPassword.changePassword();
  }).should.throwAnyException.withMessage("Validation failed.");
}
