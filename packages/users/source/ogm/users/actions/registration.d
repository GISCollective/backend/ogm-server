/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.users.actions.registration;

import ogm.http.request;
import ogm.crates.all;
import ogm.users.actions.action;
import std.array;

import vibe.http.router;
import vibe.data.json;
import vibe.service.stats;

import crate.base;
import crate.error;
import crate.http.operations.base;
import crate.auth.usercollection;

import std.datetime;
import std.string;
import std.algorithm;

import vibeauth.challenges.base;
import vibeauth.challenges.recaptcha;
import vibeauth.challenges.mtcaptcha;
import gis_collective.hmq.broadcast.base;
import ogm.users.actions.resetpassword;
import ogm.newsletter;

class RegistrationRequest {

  private {
    OgmCrates crates;
    UserCrateCollection users;
    IBroadcast broadcast;
  }

  ///
  this(UserCrateCollection users, OgmCrates crates, IBroadcast broadcast) {
    this.crates = crates;
    this.users = users;
    this.broadcast = broadcast;
  }

  IChallenge challenge() {
    if(this.captchaType == "reCAPTCHA") {
      return new ReCaptcha(new ReCaptchaConfig(crates));
    }

    if(this.captchaType == "mtCAPTCHA") {
      return new MtCaptcha(new MtCaptchaConfig(crates));
    }

    if(this.captchaType == "test") {
      return new TestCaptcha();
    }

    return null;
  }

  string captchaType() {
    auto range = crates.preference.get
      .where("name").equal("captcha.enabled")
      .and.exec;

    if(range.empty) {
      return "";
    }

    auto preference = range.front;

    return preference["value"].to!string;
  }

  void activate(HTTPServerRequest req, HTTPServerResponse res) {
    string email;
    string token;

    if(req.json.type == Json.Type.object && req.json["email"].type == Json.Type.string) {
      email = req.json["email"].to!string;
    }

    if(req.json.type == Json.Type.object && req.json["token"].type == Json.Type.string) {
      token = req.json["token"].to!string;
    }

    if(email != "" && users.contains(email)) {
      auto user = users[email].toPublicJson;

      auto message = Json.emptyObject;
      message["id"] = user["id"];

      this.broadcast.push("user.activate", message);
    }

    if(token != "") {
      auto user = users.byToken(token);
      user.isActive = true;
    }

    res.statusCode = 204;
    res.writeVoidBody;
  }

  void challenge(HTTPServerRequest req, HTTPServerResponse res) {
    auto challenge = this.challenge;
    res.statusCode = 200;

    if(challenge is null) {
      res.writeJsonBody(Json.emptyObject);
      return;
    }

    res.writeJsonBody(challenge.getConfig);
  }

  void forgot(HTTPServerRequest req, HTTPServerResponse res) {
    auto resetPassword = ResetPassword();
    resetPassword.userCollection = users;

    if(req.json.type != Json.Type.object) {
      res.statusCode = 204;
      res.writeVoidBody;
      return;
    }

    if(req.json["email"].type == Json.Type.string) {
      resetPassword.email = req.json["email"].to!string;
    }

    if(req.json["token"].type == Json.Type.string) {
      resetPassword.token = req.json["token"].to!string;
    }

    if(req.json["password"].type == Json.Type.string) {
      resetPassword.password = req.json["password"].to!string;
    }

    if(!users.contains(resetPassword.email)) {
      res.statusCode = 204;
      res.writeVoidBody;
      return;
    }

    auto user = users[resetPassword.email];

    auto message = Json.emptyObject;
    message["id"] = user.id;

    if(resetPassword.token == "" && resetPassword.password == "") {
      this.broadcast.push("user.forgot", message);
    }

    if(resetPassword.token != "" && resetPassword.password != "") {
      resetPassword.changePassword();
      this.broadcast.push("user.changedPassword", message);
    }

    res.statusCode = 204;
    res.writeVoidBody;
  }

  ///
  void handler(HTTPServerRequest req, HTTPServerResponse res) {
    scope(failure) {
      Stats.instance.inc("auth_activate", ["result": "failure"]);
    }
    auto data = req.json;
    User user;

    enforce!CrateValidationException(data.type == Json.Type.object, "The body must be a json object.");
    static immutable fields = ["salutation", "title", "firstName", "lastName", "username", "email", "password"];
    string[] missing;

    static foreach(field; fields) {
      if(data[field].type != Json.Type.string) {
        missing ~= field;
      }
    }

    auto challenge = this.challenge;
    if(challenge !is null) {
      enforce!CrateValidationException(challenge.validate(data["challenge"].to!string), "Invalid challenge response.");
    }

    user.username = data["username"].to!string.strip.toLower;
    user.email = data["email"].to!string.strip.toLower;
    user.createdAt = Clock.currTime.toUTC;

    string[] forbiddenDomains = ["chimney", "airduct", "repair", "cleaning", "locksmith", "plumbing"];

    foreach(domain; forbiddenDomains) {
      enforce!CrateValidationException(!user.email.canFind(domain), "Your email is banned.");
    }

    enforce!CrateValidationException(!users.contains(user.email), "Email has already been taken.");
    enforce!CrateValidationException(!users.contains(user.username), "Username has already been taken.");
    enforce!CrateValidationException(missing.length == 0, `Fields must be string: ` ~ missing.join(", "));
    enforce!CrateValidationException(data["password"].length >= 10, "Your password should have at least 10 characters.");

    auto createdUser = crates.user.addItem(user.serializeToJson);
    users[user.email].setPassword(data["password"].to!string);

    auto newProfile = UserProfile(ObjectId.fromJson(createdUser["_id"])).serializeToJson;
    newProfile["title"] = data["title"];
    newProfile["salutation"] = data["salutation"];
    newProfile["firstName"] = data["firstName"];
    newProfile["lastName"] = data["lastName"];
    newProfile["joinedTime"] = Clock.currTime.toUTC.toISOExtString;
    newProfile["showWelcomePresentation"] = true;
    newProfile.remove("picture");
    crates.userProfile.addItem(newProfile);

    auto query = crates.team.get.where("invitations").arrayFieldContains("email", user.email).and;

    if("newsletter" in data && data["newsletter"]) {
      auto newsletters = crates.newsletter.get.where("visibility.isDefault").equal(true).and.exec;

      foreach(newsletter; newsletters) {
        subscribe(user.email, newsletter["_id"].to!string, crates, broadcast);
      }
    }

    auto teams = query.exec;
    foreach(team; teams) {
      auto invitation = team["invitations"].byValue.find!(a => a["email"] == user.email).front;
      team["invitations"] = team["invitations"].byValue.filter!(a => a["email"] != user.email).array;
      auto role = invitation["role"].to!string;

      if(role == "guests" || role == "members" || role == "leaders" || role == "owners") {
        team[role] ~= createdUser["_id"];

        auto message = Json.emptyObject;

        message["id"] = team["_id"].to!string ~ "." ~ user.email;
        message["team"] = team["_id"].to!string;
        message["user"] = createdUser["_id"];
        message["role"] = role;
        broadcast.push("team.welcome", message);
      }

      crates.team.updateItem(team);
    }

    res.statusCode = 204;
    res.writeVoidBody;

    auto message = Json.emptyObject;
    message["id"] = createdUser["_id"];

    this.broadcast.push("user.new", message);
    this.broadcast.push("user.activate", message);
    Stats.instance.inc("auth_register", ["result": "success"]);
  }
}

class ConfigPreferences {

  private {
    OgmCrates crates;
  }

  this(OgmCrates crates) {
    this.crates = crates;
  }

  string get(string name) {
    return this.crates.preference.get.where("name").equal(name).and.exec.front["value"].to!string;
  }
}

class ReCaptchaConfig : IReCaptchaConfig {

  private {
    ConfigPreferences preferences;
  }

  this(OgmCrates crates) {
    this.preferences = new ConfigPreferences(crates);
  }

  string siteKey() {
    return this.preferences.get("secret.recaptcha.siteKey");
  }

  string secretKey() {
    return this.preferences.get("secret.recaptcha.secretKey");
  }
}

class MtCaptchaConfig : IMtCaptchaConfig {

  private {
    ConfigPreferences preferences;
  }

  this(OgmCrates crates) {
    this.preferences = new ConfigPreferences(crates);
  }

  string siteKey() {
    return this.preferences.get("secret.mtcaptcha.siteKey");
  }

  string privateKey() {
    return this.preferences.get("secret.mtcaptcha.privateKey");
  }
}

/// Class that implements a test challenge
class TestCaptcha : IChallenge {
  string generate(HTTPServerRequest req, HTTPServerResponse res) {
    return "";
  }

  string getTemplate(string challangeLocation) {
    return "";
  }

  Json getConfig() {
    return `{"config": "value"}`.parseJsonString;
  }

  bool validate(string response) {
    return response == "test";
  }
}
