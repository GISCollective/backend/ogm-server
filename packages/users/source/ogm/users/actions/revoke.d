/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.users.actions.revoke;

import ogm.http.request;
import ogm.crates.all;
import ogm.users.actions.action;

import vibe.http.router;
import vibe.data.json;

import crate.base;
import crate.error;
import crate.http.operations.base;

import std.datetime;
import std.string;
import std.algorithm;

class RevokeRequest : ActionRequest!"update" {

  ///
  this(OgmCrates crates) {
    CrateRule rule;

    rule.request.path = "/users/:id/revoke";
    rule.request.method = HTTPMethod.POST;

    super(crates, rule);
  }

  ///
  override void handler(ref DefaultStorage storage) {
    auto req = storage.request;
    auto res = storage.response;

    scope request = RequestUserData(req);

    if(request.userId != storage.properties.itemId && !request.isAdmin) {
      res.writeBody(`{"errors": [{
        "title": "Invalid operation",
        "description": "You can't revoke tokens for this user.",
        "status": 400 }]}`, 400, "text/json");
      return;
    }

    auto user = this.selectedUser(request);

    try {
      auto noToken = "token" !in req.json || req.json["token"].to!string.strip == "";
      auto noName = "name" !in req.json || req.json["name"].to!string.strip == "";

      if(noToken && noName) {
        res.writeBody(`{"errors": [{
          "title": "Invalid operation",
          "description": "The token name is missing",
          "status": 400 }]}`, 400, "text/json");
        return;
      }

      if("token" !in req.json) {
        auto r = user.getTokensByType("api").filter!(a => a.meta["name"] == req.json["name"]);
        req.json["token"] = r.front.name;
      }

      user.revoke(req.json["token"].to!string.strip);

      crates.user.updateItem(user.toJson);
      res.writeBody(`{ "success": true }`, 200, "text/json");
    } catch(Exception e) {
      Json error = Json.emptyObject;
      error["title"] = "Invalid operation";
      error["description"] = e.message.idup;
      error["status"] = 400;

      res.writeBody(`{"errors": [` ~ error.toString ~`]}`, 400, "text/json");
      return;
    }
  }
}
