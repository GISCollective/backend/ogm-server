/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.users.actions.testNotification;

import ogm.http.request;
import ogm.crates.all;
import ogm.users.actions.action;

import vibe.http.router;
import vibe.data.json;

import crate.error;
import crate.base;
import crate.http.operations.base;
import gis_collective.hmq.broadcast.base;

class TestNotificationRequest : ActionRequest!"update" {

  private {
    IBroadcast broadcast;
  }

  ///
  this(OgmCrates crates, IBroadcast broadcast) {
    CrateRule rule;

    rule.request.path = "/users/:id/testNotification";
    rule.request.method = HTTPMethod.POST;

    this.broadcast = broadcast;

    super(crates, rule);
  }

  ///
  override void handler(ref DefaultStorage storage) {
    auto req = storage.request;
    auto res = storage.response;

    scope request = RequestUserData(req);

    if(!request.isAdmin) {
      res.writeBody(`{"errors": [{
        "title": "You are not authorized",
        "description": "You must be an admin.",
        "status": 400 }]}`, 400, "text/json");
      return;
    }

    auto message = Json.emptyObject;
    message["id"] = storage.properties.itemId;

    this.broadcast.push("user.testNotification", message);

    res.statusCode = 204;
    res.writeVoidBody;
  }
}
