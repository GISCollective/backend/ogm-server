/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.users.actions.promote;

import ogm.http.request;
import ogm.crates.all;
import ogm.users.actions.action;

import vibe.http.router;
import vibe.data.json;

import crate.error;
import crate.base;
import crate.http.operations.base;

class PromoteRequest : ActionRequest!"update" {

  ///
  this(OgmCrates crates) {
    CrateRule rule;

    rule.request.path = "/users/:id/promote";
    rule.request.method = HTTPMethod.POST;

    super(crates, rule);
  }

  ///
  override void handler(ref DefaultStorage storage) {
    auto req = storage.request;
    auto res = storage.response;

    scope request = RequestUserData(req);

    if(!request.isAdmin) {
      res.writeBody(`{"errors": [{
        "title": "You are not authorized",
        "description": "You must be an admin.",
        "status": 400 }]}`, 400, "text/json");
      return;
    }

    if(request.userId == storage.properties.itemId) {
      res.writeBody(`{"errors": [{
        "title": "Invalid operation",
        "description": "You can't promote yourself.",
        "status": 400 }]}`, 400, "text/json");
      return;
    }

    if(validatePassword(request, req, res)) {
      return;
    }

    auto user = this.selectedUser(request);

    if(!user.can!("admin")) {
      user.addScope("admin");
      crates.user.updateItem(user.toJson);
    }

    res.writeBody(`{ "success": true }`, 200, "text/json");
  }
}
