/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.users.actions.changepassword;

import ogm.http.request;
import ogm.crates.all;
import ogm.users.actions.action;

import vibe.http.router;
import vibe.data.json;
import vibeauth.data.user;

import crate.base;
import crate.error;
import crate.http.operations.base;
import gis_collective.hmq.broadcast.base;

class ChangePasswordRequest : ActionRequest!"update" {
  IBroadcast broadcast;

  ///
  this(OgmCrates crates, IBroadcast broadcast) {
    CrateRule rule;

    rule.request.path = "/users/:id/changePassword";
    rule.request.method = HTTPMethod.POST;

    this.broadcast = broadcast;

    super(crates, rule);
  }

  ///
  override void handler(ref DefaultStorage storage) {
    auto req = storage.request;
    auto res = storage.response;
    scope request = RequestUserData(req);

    if(!request.isAdmin && request.userId != request.itemId) {
      res.writeBody(`{"errors": [{
        "title": "You are not authorized",
        "description": "You can change only your password.",
        "status": 400 }]}`, 400, "text/json");
      return;
    }

    auto data = crates.user.getItem(request.userId).exec.front;
    auto currentUser = vibeauth.data.user.User.fromJson(data);

    string currentPassword = req.json["currentPassword"].to!string;

    if(!currentUser.isValidPassword(currentPassword)) {
      res.writeBody(`{"errors": [{
        "title": "Invalid password.",
        "description": "Your current password is not valid.",
        "status": 400 }]}`, 400, "text/json");
      return;
    }

    auto user = this.selectedUser(request);

    string newPassword = req.json["newPassword"].to!string;

    user.setPassword(newPassword);
    crates.user.updateItem(user.toJson);

    auto message = Json.emptyObject;
    message["id"] = user.id;

    this.broadcast.push("user.changedPassword", message);
    res.writeBody(`{ "success": true }`, 200, "text/json");
  }
}
