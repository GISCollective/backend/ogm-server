/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.users.actions.action;

import ogm.http.request;
import ogm.crates.all;

import vibe.http.router;
import vibe.data.json;
import vibeauth.data.user;

import crate.error;
import crate.base;
import crate.http.operations.base;

abstract class ActionRequest(string attribute) : CrateOperation!DefaultStorage {

  protected {
    OgmCrates crates;
  }

  ///
  this(OgmCrates crates, CrateRule rule) {
    this.crates = crates;

    super(crates.user, rule);
  }

  bool validatePassword(RequestUserData request, HTTPServerRequest req, HTTPServerResponse res) {
    auto data = crates.user.getItem(request.userId).exec.front;
    auto currentUser = vibeauth.data.user.User.fromJson(data);

    string currentPassword = req.json["password"].to!string;

    if(!currentUser.isValidPassword(currentPassword)) {
      res.writeBody(`{"errors": [{
        "title": "Invalid password.",
        "description": "Your password is not valid.",
        "status": 400 }]}`, 400, "text/json");
      return true;
    }

    return false;
  }

  auto selectedUser(RequestUserData request) {
    auto data = crates.user.getItem(request.itemId).exec.front;
    auto selectedUser = vibeauth.data.user.User.fromJson(data);

    return selectedUser;
  }

  ///
  override void handle(DefaultStorage storage) {
    storage.item = this.prepareItemOperation!attribute(storage);

    auto res = storage.response;

    if(res.headerWritten) {
      return;
    }

    handler(storage);
  }


  abstract void handler(ref DefaultStorage storage);
}
