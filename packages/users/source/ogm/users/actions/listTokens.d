/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.users.actions.listTokens;

import ogm.http.request;
import ogm.crates.all;
import ogm.users.actions.action;

import vibe.http.router;
import vibe.data.json;

import vibeauth.data.token;

import crate.error;
import crate.base;
import crate.http.operations.base;

import std.datetime;
import std.string;
import std.array;
import std.algorithm;

class ListTokensRequest : ActionRequest!"update" {

  ///
  this(OgmCrates crates) {
    CrateRule rule;

    rule.request.path = "/users/:id/listTokens";
    rule.request.method = HTTPMethod.GET;

    super(crates, rule);
  }

  Json mapToken(const Token token) {
    auto result = token.meta.serializeToJson;

    result["expire"] = token.expire.toISOExtString;
    result["scopes"] = token.scopes.serializeToJson;

    return result;
  }

  ///
  override void handler(ref DefaultStorage storage) {
    auto req = storage.request;
    auto res = storage.response;

    scope request = RequestUserData(req);

    if(request.userId != storage.properties.itemId && !request.isAdmin) {
      res.writeBody(`{"errors": [{
        "title": "Invalid operation",
        "description": "You can't get the tokens for this user.",
        "status": 400 }]}`, 400, "text/json");
      return;
    }

    auto user = this.selectedUser(request);

    try {
      auto tokens = user.getTokensByType("api").map!(a => mapToken(a)).array.serializeToJson.to!string;

      res.writeBody(`{ "tokens": ` ~ tokens ~ ` }`, 200, "text/json");
    } catch(Exception e) {
      Json error = Json.emptyObject;
      error["title"] = "Invalid operation";
      error["description"] = e.message.idup;
      error["status"] = 400;

      res.writeBody(`{"errors": [` ~ error.toString ~`]}`, 400, "text/json");
    }
  }
}
