/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.users.actions.googleToken;

import ogm.http.request;
import ogm.crates.all;
import ogm.users.actions.action;

import vibe.service.stats;
import vibe.http.router;
import vibe.data.json;
import vibe.http.client;
import vibe.core.log;
import vibe.core.core;
import vibe.stream.operations;

import crate.base;
import crate.error;
import crate.http.operations.base;

import std.datetime;
import std.string;
import std.algorithm;
import std.conv;
import vibeauth.data.user;
import gis_collective.hmq.broadcast.base;
import std.string;

class GoogleTokenRequest : CrateOperation!DefaultStorage {
  OgmCrates crates;
  IBroadcast broadcast;

  ///
  this(OgmCrates crates, IBroadcast broadcast) {
    CrateRule rule;

    rule.request.path = "/auth/googletoken";
    rule.request.method = HTTPMethod.POST;

    this.crates = crates;
    this.broadcast = broadcast;

    super(crates.user, rule);
  }

  vibeauth.data.user.User upsertUserByEmail(string email, Json data) {
    Json userData;

    auto range = crates.user.get.where("email").equal(email).and.exec;
    if(range.empty) {
      auto emailPieces = data["email"].to!string.toLower.split('@');

      userData = Json.emptyObject;
      userData["email"] = data["email"].to!string.toLower;
      userData["username"] = emailPieces[0];
      userData["firstName"] = data["given_name"];
      userData["lastName"] = data["family_name"];
      userData["isActive"] = data["email_verified"].to!string == "true";
      userData["createdAt"] = Clock.currTime.toISOExtString;
      userData["password"] = "";
      userData["salt"] = "";
      userData["scopes"] = Json.emptyArray;
      userData["tokens"] = Json.emptyArray;

      auto existCount = crates.user.get.where("username").equal(userData["username"].to!string).and.size;
      size_t index = 0;

      while(existCount > 0) {
        index++;
        userData["username"] = emailPieces[0] ~ "." ~ index.to!string;
        existCount = crates.user.get.where("username").equal(userData["username"].to!string).and.size;
      }

      userData = crates.user.addItem(userData);

      auto message = Json.emptyObject;
      message["id"] = userData["_id"];

      this.broadcast.push("user.new", message);
      Stats.instance.inc("auth_register", ["result": "success"]);
    } else {
      userData = range.front;
    }

    userData["lastActivity"] = Clock.currTime.toUnixTime!long;

    return vibeauth.data.user.User.fromJson(userData);
  }

  ///
  override void handle(DefaultStorage storage) {
    auto res = storage.response;
    auto req = storage.request;

    if(res.headerWritten) {
      return;
    }

    scope request = RequestUserData(req);

    string token = req.json["idtoken"].to!string;
    string audience = crates.preference.get.where("name").equal("integrations.google.client_id").and.exec.front["value"].to!string.strip;
    enforce!CrateValidationException(audience != "", "The google configuration is missing.");

    auto result = GoogleTokenValidation.instance.verifyIdToken(token, audience);
    enforce!CrateValidationException("email" in result, "The token is invalid.");

    auto selectedUser = upsertUserByEmail(result["email"].to!string.toLower, result);

    auto scopes = selectedUser.getScopes;
    auto accessToken = selectedUser.createToken(Clock.currTime + 3601.seconds, scopes, "Bearer");
    auto refreshToken = selectedUser.createToken(Clock.currTime + 4.weeks, scopes ~ [ "refresh" ], "Refresh");
    crates.user.updateItem(selectedUser.toJson);

    auto response = Json.emptyObject;
    response["access_token"] = accessToken.name;
    response["expires_in"] = (accessToken.expire - Clock.currTime).total!"seconds";
    response["token_type"] = accessToken.type;
    response["refresh_token"] = refreshToken.name;

    res.writeJsonBody(response, 200);
  }
}

interface IGoogleTokenValidation {
  Json verifyIdToken(string token, string audience);
}

class GoogleTokenValidation: IGoogleTokenValidation {
  static IGoogleTokenValidation instance;

  static this() {
    GoogleTokenValidation.instance = new GoogleTokenValidation();
  }

  Json getRequest(string id_token) {
    auto result = Json.emptyObject;

    requestHTTP("https://oauth2.googleapis.com/tokeninfo?id_token=" ~ id_token,
      (scope req) {
        req.headers["Connection"] = "close";
      },
      (scope res) {
        if(res.statusCode == 200) {
          result = res.readJson;
        } else {
          logError("getRequest ERROR: %s", res.bodyReader.readAllUTF8());
        }
      }
    );

    return result;
  }

  Json verifyIdToken(string token, string audience) {
    auto result = getRequest(token);

    enforce!CrateValidationException(result["aud"] == audience, "Invalid client id.");

    return result;
  }
}