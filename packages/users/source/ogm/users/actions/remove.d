/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.users.actions.remove;

import ogm.http.request;
import ogm.crates.all;
import ogm.users.actions.action;

import vibe.http.router;
import vibe.data.json;

import crate.base;
import crate.error;
import crate.http.operations.base;

class RemoveRequest : ActionRequest!"delete_" {

  ///
  this(OgmCrates crates) {
    CrateRule rule;

    rule.request.path = "/users/:id/remove";
    rule.request.method = HTTPMethod.POST;

    super(crates, rule);
  }

  ///
  override void handler(ref DefaultStorage storage) {
    auto req = storage.request;
    auto res = storage.response;

    scope request = RequestUserData(req);

    string id = storage.properties.itemId;

    if(!request.isAdmin && request.userId != id) {
      res.writeBody(`{"errors": [{
        "title": "You are not authorized",
        "description": "You can not remove other users.",
        "status": 400 }]}`, 400, "text/json");
      return;
    }

    if(request.isAdmin && request.userId == id) {
      res.writeBody(`{"errors": [{
        "title": "Invalid action",
        "description": "Your can't remove yourself.",
        "status": 400 }]}`, 400, "text/json");
      return;
    }

    if(validatePassword(request, req, res)) {
      return;
    }

    crates.user.deleteItem(id);
    res.writeBody(`{ "success": true }`, 200, "text/json");
  }
}
