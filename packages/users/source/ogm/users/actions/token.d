/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.users.actions.token;

import ogm.http.request;
import ogm.crates.all;
import ogm.users.actions.action;

import vibe.http.router;
import vibe.data.json;

import crate.base;
import crate.error;
import crate.http.operations.base;

import std.datetime;
import std.string;
import std.algorithm;

class TokenRequest : ActionRequest!"update" {

  ///
  this(OgmCrates crates) {
    CrateRule rule;

    rule.request.path = "/users/:id/token";
    rule.request.method = HTTPMethod.POST;

    super(crates, rule);
  }

  ///
  override void handler(ref DefaultStorage storage) {
    auto req = storage.request;
    auto res = storage.response;

    scope request = RequestUserData(req);

    if(request.userId != storage.properties.itemId) {
      res.writeBody(`{"errors": [{
        "title": "Invalid operation",
        "description": "You can't create tokens for this user.",
        "status": 400 }]}`, 400, "text/json");
      return;
    }

    auto user = this.selectedUser(request);

    try {
      if("name" !in req.json || req.json["name"].to!string.strip == "") {
        res.writeBody(`{"errors": [{
          "title": "Invalid operation",
          "description": "The name is missing",
          "status": 400 }]}`, 400, "text/json");
        return;
      }

      if("expire" !in req.json || req.json["expire"].to!string.strip == "") {
        res.writeBody(`{"errors": [{
          "title": "Invalid operation",
          "description": "The expire date is missing",
          "status": 400 }]}`, 400, "text/json");
        return;
      }

      if(!user.getTokensByType("api").filter!(a => a.meta["name"] == req.json["name"]).empty) {
        res.writeBody(`{"errors": [{
          "title": "Token name already exists",
          "description": "There is already a token with the same name. Please use a different one and try again.",
          "status": 400 }]}`, 400, "text/json");
        return;
      }

      auto meta = ["created": Clock.currTime.toISOExtString, "name": req.json["name"].to!string];
      auto token = user.createToken(SysTime.fromISOExtString(req.json["expire"].to!string), ["api"], "api", meta);
      crates.user.updateItem(user.toJson);
      res.writeBody(`{ "token": "` ~ token.name ~ `" }`, 200, "text/json");
    } catch(Exception e) {
      Json error = Json.emptyObject;
      error["title"] = "Invalid operation";
      error["description"] = e.message.idup;
      error["status"] = 400;

      res.writeBody(`{"errors": [` ~ error.toString ~`]}`, 400, "text/json");
      return;
    }
  }
}
