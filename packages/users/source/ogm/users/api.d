/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.users.api;

import vibe.http.router;
import vibe.core.log;

import vibeauth.data.usermodel;

import crate.auth.usercollection;

import crate.http.router;
import crate.base;
import crate.error;
import crate.http.router;

import ogm.auth;
import ogm.crates.all;

import ogm.middleware.userdata;
import ogm.users.actions.changepassword;
import ogm.users.actions.remove;
import ogm.users.actions.promote;
import ogm.users.actions.downgrade;
import ogm.users.actions.token;
import ogm.users.actions.revoke;
import ogm.users.actions.registration;
import ogm.users.actions.listTokens;
import ogm.users.actions.testNotification;
import ogm.users.actions.googleToken;

import ogm.users.mapper;
import ogm.users.filter;
import ogm.filter.pagination;
import ogm.middleware.adminrequest;

import ogm.middleware.user.PublicUserMapper;
import ogm.middleware.user.UserDataValidator;
import ogm.middleware.user.UserTermFilter;
import ogm.users.format;

import gis_collective.hmq.broadcast.base;

///
void setupUserApi(T)(CrateRouter!T crateRouter, OgmCrates crates, IBroadcast broadcast) {
  auto auth = Authentication.instance(crates.user);
  auto userDataMiddleware = UserDataMiddleware.instance(crates);
  auto userMapper = new UserMapper(auth.getCollection, crates);
  auto userFilter = new UserFilter(auth.getCollection, crates);
  auto paginationFilter = new PaginationFilter;
  auto adminRequest = new AdminRequest(crates.user);

  auto mapper = new PublicUserMapper;
  auto userValidator = new UserDataValidator(crates.user);

  auto registration = new RegistrationRequest(auth.getCollection, crates, broadcast);
  auto changePassword = new ChangePasswordRequest(crates, broadcast);
  auto remove = new RemoveRequest(crates);
  auto promote = new PromoteRequest(crates);
  auto downgrade = new DowngradeRequest(crates);
  auto token = new TokenRequest(crates);
  auto googleToken = new GoogleTokenRequest(crates, broadcast);
  auto revoke = new RevokeRequest(crates);
  auto listTokens = new ListTokensRequest(crates);
  auto testNotification = new TestNotificationRequest(crates, broadcast);
  auto userTermFilter = new UserTermFilter;
  auto format = new UserFormatMiddleware(crates);

  crateRouter.router.post("/users/register", requestErrorHandler(&registration.handler));
  crateRouter.router.post("/users/registerchallenge", requestErrorHandler(&registration.challenge));
  crateRouter.router.post("/users/forgotpassword", requestErrorHandler(&registration.forgot));
  crateRouter.router.post("/users/activate", requestErrorHandler(&registration.activate));

  auto preparedRoute = crateRouter.prepare(crates.user)
    .withCustomOperation(changePassword)
    .withCustomOperation(remove)
    .withCustomOperation(promote)
    .withCustomOperation(downgrade)
    .withCustomOperation(token)
    .withCustomOperation(revoke)
    .withCustomOperation(listTokens)
    .withCustomOperation(testNotification)
    .withCustomOperation(googleToken)
    .and(auth.publicItemDataMiddleware)
    .and(adminRequest)
    .and(userDataMiddleware)
    .and(auth.replaceMeId)
    .and(userValidator)
    .and(userTermFilter)
    .and(userFilter)
    .and(mapper)
    .and(userMapper)
    .and(paginationFilter)
    .and(format);
}
