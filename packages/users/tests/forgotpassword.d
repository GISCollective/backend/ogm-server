/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.users.forgotpassword;
import tests.fixtures;

import gis_collective.hmq.broadcast.memory;
import ogm.users.api;
import ogm.defaults.users;

alias suite = Spec!({
  URLRouter router;
  Json broadcastValue;
  MemoryBroadcast broadcast;

  describe("forgotpassword", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      broadcast = new MemoryBroadcast;
      router.crateSetup.setupUserApi(crates, broadcast);
      setupDefaultUsers(crates);

      broadcastValue = Json.emptyObject;
      void testHandler(const Json value) @trusted {
        broadcastValue = value;
      }

      broadcast.register("user.forgot", &testHandler);
    });

    it("should return an empty object and trigger a task when the user exists", {
      auto data = Json.emptyObject;
      data["email"] = "owner@gmail.com";

      router
        .request
        .post("/users/forgotpassword")
        .send(data)
        .expectStatusCode(204)
        .end((Response response) => () {
          response.bodyString.should.equal(``);
          broadcastValue.should.equal(`{
            "id": "000000000000000000000004"
          }`.parseJsonString);
        });
    });

    it("should return an empty object and trigger a task when the user does not exist", {
      auto data = Json.emptyObject;
      data["email"] = "missing@gmail.com";

      router
        .request
        .post("/users/forgotpassword")
        .send(data)
        .expectStatusCode(204)
        .end((Response response) => () {
          response.bodyString.should.equal(``);
          broadcastValue.should.equal(`{}`.parseJsonString);
        });
    });

    it("changes the password when the token and pasword are set", {
      auto auth = Authentication.instance(crates.user);
      auto userCollection = auth.getCollection;

      auto token = `{"scopes":[],"type":"passwordReset","meta":{},"expire":"2050-11-15T17:44:56.7605237Z","name":"d55cd8a9-1fad-8070-e1b2-d0e3ea716358"}`.parseJsonString;
      auto user = crates.user.getItem("000000000000000000000005").and.exec.front;

      user["tokens"].appendArrayElement(token);

      crates.user.updateItem(user);

      auto data = Json.emptyObject;
      data["email"] = "admin@gmail.com";
      data["token"] = "d55cd8a9-1fad-8070-e1b2-d0e3ea716358";
      data["password"] = "newPassword12345";

      router
        .request
        .post("/users/forgotpassword")
        .send(data)
        .expectStatusCode(204)
        .end((Response response) => () {
          response.bodyString.should.equal(``);
          broadcastValue.should.equal(`{}`.parseJsonString);

          auto updatedUser = crates.user.getItem("000000000000000000000005").and.exec.front;
          updatedUser["password"].should.not.equal(user["password"]);
        });
    });
  });
});
