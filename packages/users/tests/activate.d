/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.users.activate;
import tests.fixtures;

import ogm.users.api;
import ogm.defaults.users;
import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({
  URLRouter router;
  Json broadcastValue;
  MemoryBroadcast broadcast;
  Json data;

  describe("activate", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      broadcast = new MemoryBroadcast;
      router.crateSetup.setupUserApi(crates, broadcast);
      setupDefaultUsers(crates);

      broadcastValue = Json.emptyObject;
      void testHandler(const Json value) @trusted {
        broadcastValue = value;
      }

      broadcast.register("user.activate", &testHandler);

      data = Json.emptyObject;
    });

    describe("when the email field is set", {
      beforeEach({
        data["email"] = "owner@gmail.com";
      });

      it("should return an empty string", {
        router
          .request
          .post("/users/activate")
          .send(data)
          .expectStatusCode(204)
          .end((Response response) => () {
            response.bodyString.should.equal(``);
          });
      });

      it("should trigger a task with the user id", {
        router
          .request
          .post("/users/activate")
          .send(data)
          .expectStatusCode(204)
          .end((Response response) => () {
            broadcastValue.should.equal(`{
              "id": "000000000000000000000004"
            }`.parseJsonString);
          });
      });
    });

    describe("when the token field is set", {
      Token token;

      beforeEach({
        auto collection = new UserCrateCollection([], crates.user);
        token = collection.createToken("owner@gmail.com", Clock.currTime.toUTC + 2.days, [], "activation");
      });

      it("should return an empty string", {
        data["token"] = token.name;
        router
          .request
          .post("/users/activate")
          .send(data)
          .expectStatusCode(204)
          .end((Response response) => () {
            response.bodyString.should.equal(``);

            auto user = crates.user.get.where("email").equal("owner@gmail.com").and.exec.front;

            user["isActive"].should.equal(true);
          });
      });

      it("should not trigger a task with the user id", {
        data["token"] = token.name;
        router
          .request
          .post("/users/activate")
          .send(data)
          .expectStatusCode(204)
          .end((Response response) => () {
            broadcastValue.should.equal(`{}`.parseJsonString);
          });
      });
    });
  });
});
