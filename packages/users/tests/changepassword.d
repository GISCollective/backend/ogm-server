/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.users.changepassword;

import tests.fixtures;
import ogm.users.api;
import ogm.defaults.users;

import std.algorithm;
import std.array;

import vibeauth.data.user;

import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({
  URLRouter router;

  describe("POST.change password", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupUserApi(crates, new MemoryBroadcast);
      setupDefaultUsers(crates);
    });

    describe("without a token", {
      it("should return an auth error", {
        router
          .request
          .post("/users/000000000000000000000002/changePassword")
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
          });
      });
    });

    describe("with an invalid token", {
      it("should return an auth error", {
        router
          .request
          .post("/users/000000000000000000000002/changePassword")
          .header("Authorization", "Bearer random")
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"error\": \"Invalid token.\"}".parseJsonString);
          });
      });
    });

    describe("with an admin token", {
      it("should change the password to any user" , {
        auto data = `{
          "currentPassword": "password",
          "newPassword": "newPassword"
        }`.parseJsonString;

        router
          .request
          .post("/users/000000000000000000000002/changePassword")
          .send(data)
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            auto user = vibeauth.data.user.User
              .fromJson(crates.user
                .getItem("000000000000000000000002").exec.front);

            user.isValidPassword("newPassword").should.equal(true);
          });
      });

      it("should not change the password if the current one is invalid" , {
        auto data = `{
          "currentPassword": "bad_password",
          "newPassword": "newPassword"
        }`.parseJsonString;

        router
          .request
          .post("/users/000000000000000000000002/changePassword")
          .send(data)
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(400)
          .end((Response response) => () {
            auto user = vibeauth.data.user.User
              .fromJson(crates.user
                .getItem("000000000000000000000002").exec.front);

            user.isValidPassword("newPassword").should.equal(false);

            response.bodyJson.should.equal(`{"errors": [{
              "title": "Invalid password.",
              "description": "Your current password is not valid.",
              "status": 400 }]}`.parseJsonString);
          });
      });
    });

    describe("with an owner token", {
      it("should change the password to authenticated user" , {
        auto data = `{
          "currentPassword": "password",
          "newPassword": "newPassword"
        }`.parseJsonString;

        router
          .request
          .post("/users/000000000000000000000004/changePassword")
          .send(data)
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{ "success": true }`.parseJsonString);

            auto user = vibeauth.data.user.User
              .fromJson(crates.user
                .getItem("000000000000000000000004").exec.front);

            user.isValidPassword("newPassword").should.equal(true);
          });
      });

      it("should not change the password to other users" , {
        auto data = `{
          "currentPassword": "password",
          "newPassword": "newPassword"
        }`.parseJsonString;

        router
          .request
          .post("/users/000000000000000000000002/changePassword")
          .send(data)
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(400)
          .end((Response response) => () {
            auto user = vibeauth.data.user.User
              .fromJson(crates.user
                .getItem("000000000000000000000002").exec.front);

            user.isValidPassword("newPassword").should.equal(false);

            response.bodyJson.should.equal(`{"errors": [{
              "title": "You are not authorized",
              "description": "You can change only your password.",
              "status": 400 }]}`.parseJsonString);
          });
      });
    });
  });
});
