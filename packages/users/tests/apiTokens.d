/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.users.apiTokens;

import tests.fixtures;
import ogm.users.api;
import ogm.defaults.users;

import std.algorithm;
import std.array;

import vibeauth.data.token;

import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({
  URLRouter router;

  describe("POST.creating api tokens", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupUserApi(crates, new MemoryBroadcast);
      setupDefaultUsers(crates);
    });

    describe("without a token", {
      it("should return an auth error", {
        router
          .request
          .post("/users/000000000000000000000002/token")
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
          });
      });
    });

    describe("with an invalid token", {
      it("should return an auth error", {
        router
          .request
          .post("/users/000000000000000000000002/token")
          .header("Authorization", "Bearer random")
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"error\": \"Invalid token.\"}".parseJsonString);
          });
      });
    });

    describe("with an admin token", {
      it("should create an api token for itself" , {
        auto data = `{
          "name": "my api token",
          "expire": "2025-01-01T00:00:00Z"
        }`.parseJsonString;

        router
          .request
          .post("/users/000000000000000000000005/token")
          .send(data)
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyString.should.startWith(`{ "token": "`);

            auto user = crates.user
              .get.where("_id").equal(ObjectId.fromString("000000000000000000000005"))
              .and.exec.front;

            user["tokens"].length.should.equal(2);

            user["tokens"][1]["type"].to!string.should.equal(`api`);
            user["tokens"][1]["scopes"].deserializeJson!(string[]).should.equal([`api`]);
            user["tokens"][1]["expire"].to!string.should.equal("2025-01-01T00:00:00Z");
            user["tokens"][1]["meta"]["name"].to!string.should.equal(`my api token`);
            user["tokens"][1]["meta"]["created"].to!string.should.not.equal(``);
          });
      });

      it("should not create an api token with the same name", {
        auto collection = new Authentication(crates.user).getCollection;
        string[string] meta;
        meta["name"] = "my api token";
        collection.createToken("admin@gmail.com", SysTime.fromISOExtString("2030-01-01T01:00:00Z"), ["api"], "api", meta);

        auto data = `{
          "name": "my api token",
          "expire": "2025-01-01T00:00:00Z"
        }`.parseJsonString;

        router
          .request
          .post("/users/000000000000000000000005/token")
          .send(data)
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"errors": [{
              "title": "Token name already exists",
              "description": "There is already a token with the same name. Please use a different one and try again.",
              "status": 400 }]}`.parseJsonString);

            auto user = crates.user
              .get.where("_id").equal(ObjectId.fromString("000000000000000000000005"))
              .and.exec.front;

            user["tokens"].length.should.equal(2);
          });
      });

      it("should not create an api token with a missing expire" , {
        auto data = `{
          "name": "my api token"
        }`.parseJsonString;

        router
          .request
          .post("/users/000000000000000000000005/token")
          .send(data)
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"errors": [
              {"description":"The expire date is missing","title":"Invalid operation","status":400}]
            }`.parseJsonString);

            auto user = crates.user
              .get.where("_id").equal(ObjectId.fromString("000000000000000000000005"))
              .and.exec.front;

            user["tokens"].length.should.equal(1);
          });
      });

      it("should not create an api token with an invalid expire" , {
        auto data = `{
          "name": "my api token",
          "expire": "invalid"
        }`.parseJsonString;

        router
          .request
          .post("/users/000000000000000000000005/token")
          .send(data)
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"errors": [
              {"description":"Invalid ISO Extended String: invalid","title":"Invalid operation","status":400}]
            }`.parseJsonString);

            auto user = crates.user
              .get.where("_id").equal(ObjectId.fromString("000000000000000000000005"))
              .and.exec.front;

            user["tokens"].length.should.equal(1);
          });
      });

      it("should not create an api token with a missing name" , {
        auto data = `{
          "expire": "2025-01-01T00:00:00Z"
        }`.parseJsonString;

        router
          .request
          .post("/users/000000000000000000000005/token")
          .send(data)
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"errors": [
              {"description":"The name is missing","title":"Invalid operation","status":400}]
            }`.parseJsonString);

            auto user = crates.user
              .get.where("_id").equal(ObjectId.fromString("000000000000000000000005"))
              .and.exec.front;

            user["tokens"].length.should.equal(1);
          });
      });

      it("should not create an api token with a white space name" , {
        auto data = `{
          "name": "  ",
          "expire": "2025-01-01T00:00:00Z"
        }`.parseJsonString;

        router
          .request
          .post("/users/000000000000000000000005/token")
          .send(data)
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"errors": [
              {"description":"The name is missing","title":"Invalid operation","status":400}]
            }`.parseJsonString);

            auto user = crates.user
              .get.where("_id").equal(ObjectId.fromString("000000000000000000000005"))
              .and.exec.front;

            user["tokens"].length.should.equal(1);
          });
      });

      it("should not create an api token for other users" , {
        auto data = `{
          "name": "my api token",
          "expire": "2025-01-01T00:00:00Z"
        }`.parseJsonString;

        router
          .request
          .post("/users/000000000000000000000002/token")
          .send(data)
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"errors": [{
              "title": "Invalid operation",
              "description": "You can't create tokens for this user.",
              "status": 400 }]}`.parseJsonString);

            auto user = crates.user
                .get.where("_id").equal(ObjectId.fromString("000000000000000000000002"))
                .and.exec.front;

            user["tokens"].length.should.equal(1);
          });
      });
    });

    describe("with an owner token", {
      it("should create an api token for itself" , {
        auto data = `{
          "name": "my api token",
          "expire": "2025-01-01T00:00:00Z"
        }`.parseJsonString;

        router
          .request
          .post("/users/000000000000000000000004/token")
          .send(data)
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyString.should.startWith(`{ "token": "`);
            auto user = crates.user
              .get.where("_id").equal(ObjectId.fromString("000000000000000000000004"))
              .and.exec.front;

            user["tokens"].length.should.equal(2);
            user["tokens"][1]["type"].to!string.should.equal(`api`);
            user["tokens"][1]["scopes"].deserializeJson!(string[]).should.equal([`api`]);
            user["tokens"][1]["expire"].to!string.should.equal("2025-01-01T00:00:00Z");
            user["tokens"][1]["meta"]["name"].to!string.should.equal(`my api token`);
            user["tokens"][1]["meta"]["created"].to!string.should.not.equal(``);
          });
      });

      it("should not create an api token for other users" , {
        auto data = `{
          "name": "my api token",
          "expire": "2025-01-01T00:00:00Z"
        }`.parseJsonString;

        router
          .request
          .post("/users/000000000000000000000002/token")
          .send(data)
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"errors": [{
              "title": "Invalid operation",
              "description": "You can't create tokens for this user.",
              "status": 400 }]}`.parseJsonString);

            auto user = crates.user
                .get.where("_id").equal(ObjectId.fromString("000000000000000000000002"))
                .and.exec.front;

            user["tokens"].length.should.equal(1);
          });
      });
    });
  });

  describe("POST.revoking api tokens", {
    Token token;

    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupUserApi(crates, new MemoryBroadcast);
      setupDefaultUsers(crates);

      auto collection = new Authentication(crates.user).getCollection;
      string[string] meta;
      meta["name"] = "test";

      token = collection.createToken("admin@gmail.com", SysTime.fromISOExtString("2030-01-01T01:00:00Z"), ["api"], "api", meta);
    });

    describe("without a token", {
      it("should return an auth error", {
        router
          .request
          .post("/users/000000000000000000000005/revoke")
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
          });
      });
    });

    describe("with an invalid token", {
      it("should return an auth error", {
        router
          .request
          .post("/users/000000000000000000000005/revoke")
          .header("Authorization", "Bearer random")
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"error\": \"Invalid token.\"}".parseJsonString);
          });
      });
    });

    describe("with an admin token", {
      it("should revoke it's own api token" , {
        auto data = Json.emptyObject;
        data["token"] = token.name;

        router
          .request
          .post("/users/000000000000000000000005/revoke")
          .send(data)
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"success": true}`.parseJsonString);

            auto user = crates.user
              .get.where("_id").equal(ObjectId.fromString("000000000000000000000005"))
              .and.exec.front;

            user["tokens"].length.should.equal(1);
          });
      });

      it("should revoke it's own api token using the api token" , {
        auto data = Json.emptyObject;
        data["token"] = token.name;

        router
          .request
          .post("/users/000000000000000000000005/revoke")
          .send(data)
          .header("Authorization", "Bearer " ~ token.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"success": true}`.parseJsonString);

            auto user = crates.user
              .get.where("_id").equal(ObjectId.fromString("000000000000000000000005"))
              .and.exec.front;

            user["tokens"].length.should.equal(1);
          });
      });

      it("should revoke tokens by name" , {
        auto data = Json.emptyObject;
        data["name"] = token.meta["name"];

        router
          .request
          .post("/users/000000000000000000000005/revoke")
          .send(data)
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"success": true}`.parseJsonString);

            auto user = crates.user
              .get.where("_id").equal(ObjectId.fromString("000000000000000000000005"))
              .and.exec.front;

            user["tokens"].length.should.equal(1);
          });
      });

      it("should revoke other users tokens" , {
        auto data = Json.emptyObject;
        data["token"] = bearerLeaderToken.name;

        router
          .request
          .post("/users/000000000000000000000001/revoke")
          .send(data)
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"success": true}`.parseJsonString);

            auto user = crates.user
              .get.where("_id").equal(ObjectId.fromString("000000000000000000000001"))
              .and.exec.front;

            user["tokens"].length.should.equal(0);
          });
      });

      it("should return an error when the token is an empty string" , {
        auto data = Json.emptyObject;
        data["token"] = "";

        router
          .request
          .post("/users/000000000000000000000005/revoke")
          .send(data)
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"errors": [{
              "title": "Invalid operation",
              "description": "The token name is missing",
              "status": 400 }]}`.parseJsonString);

            auto user = crates.user
              .get.where("_id").equal(ObjectId.fromString("000000000000000000000005"))
              .and.exec.front;

            user["tokens"].length.should.equal(2);
          });
      });

      it("should return an error when the token is an missing" , {
        auto data = Json.emptyObject;

        router
          .request
          .post("/users/000000000000000000000005/revoke")
          .send(data)
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"errors": [{
              "title": "Invalid operation",
              "description": "The token name is missing",
              "status": 400 }]}`.parseJsonString);

            auto user = crates.user
              .get.where("_id").equal(ObjectId.fromString("000000000000000000000005"))
              .and.exec.front;

            user["tokens"].length.should.equal(2);
          });
      });
    });

    describe("with an owner token", {
      it("should not delete other users tokens" , {
        auto data = Json.emptyObject;
        data["token"] = token.name;

        router
          .request
          .post("/users/000000000000000000000005/revoke")
          .send(data)
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"errors": [{
              "description": "You can't revoke tokens for this user.",
              "title": "Invalid operation",
              "status": 400
            }]}`.parseJsonString);

            auto user = crates.user
              .get.where("_id").equal(ObjectId.fromString("000000000000000000000005"))
              .and.exec.front;

            user["tokens"].length.should.equal(2);
          });
      });
    });
  });

  describe("POST.listing api tokens", {
    Token adminToken;
    Token ownerToken;

    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupUserApi(crates, new MemoryBroadcast);
      setupDefaultUsers(crates);

      auto collection = new Authentication(crates.user).getCollection;
      adminToken = collection.createToken("admin@gmail.com", SysTime.fromISOExtString("2030-01-01T01:00:00Z"), ["api"], "api");
      ownerToken = collection.createToken("owner@gmail.com", SysTime.fromISOExtString("2030-01-01T01:00:00Z"), ["api"], "api");
    });

    describe("without a token", {
      it("should return an auth error", {
        router
          .request
          .get("/users/000000000000000000000005/listTokens")
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
          });
      });
    });

    describe("with an invalid token", {
      it("should return an auth error", {
        router
          .request
          .get("/users/000000000000000000000005/listTokens")
          .header("Authorization", "Bearer random")
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"error\": \"Invalid token.\"}".parseJsonString);
          });
      });
    });

    describe("with an admin token", {
      it("should return it's own api tokens" , {
        router
          .request
          .get("/users/000000000000000000000005/listTokens")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{ "tokens": [{"scopes":["api"],"expire":"2030-01-01T01:00:00Z"}] }`.parseJsonString);
          });
      });

      it("should return other users api tokens" , {
        router
          .request
          .get("/users/000000000000000000000004/listTokens")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{ "tokens": [{"scopes":["api"],"expire":"2030-01-01T01:00:00Z"}] }`.parseJsonString);
          });
      });
    });

    describe("with an owner token", {
      it("should return it's own api tokens", {
        router
          .request
          .get("/users/000000000000000000000004/listTokens")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"tokens": [{
              "scopes": ["api"],
              "expire": "2030-01-01T01:00:00Z"
            }]}`.parseJsonString);
          });
      });

      it("should not return other users tokens", {
        router
          .request
          .get("/users/000000000000000000000001/listTokens")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"errors": [{
              "description": "You can't get the tokens for this user.",
              "title": "Invalid operation",
              "status": 400
            }]}`.parseJsonString);
          });
      });
    });
  });
});
