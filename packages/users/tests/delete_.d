/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.users.delete_;

import tests.fixtures;
import ogm.users.api;
import ogm.defaults.users;

import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({
  URLRouter router;
  Json user;

  describe("DELETE", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupUserApi(crates, new MemoryBroadcast);
      setupDefaultUsers(crates);
    });

    describe("without a token", {
      it("should not delete an user", {
        router
          .request
          .delete_("/users/000000000000000000000001")
          .send(user)
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "error": "Authorization required"
            }`.parseJsonString);
          });
      });
    });

    describe("with an admin token", {
      it("should not delete an user", {
        router
          .request
          .delete_("/users/000000000000000000000001")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .send(user)
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "errors": [{
                  "description": "The operation is not supported.",
                  "status": 403,
                  "title": "Forbidden"
                }]}`.parseJsonString);
          });
      });
    });

    describe("with an user token", {
      it("should not delete an user", {
        Json data = user.clone;

        router
          .request
          .delete_("/users/000000000000000000000001")
          .header("Authorization", "Bearer " ~ bearerLeaderToken.name)
          .send(data)
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "errors": [{
                  "description": "The operation is not supported.",
                  "status": 403,
                  "title": "Forbidden"
                }]}`.parseJsonString);
          });
      });
    });
  });
});
