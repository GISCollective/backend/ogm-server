/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.users.put;

import tests.fixtures;
import ogm.users.api;
import ogm.defaults.users;

import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({
  URLRouter router;
  Json user;

  describe("PUT", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupUserApi(crates, new MemoryBroadcast);
      setupDefaultUsers(crates);

      user = `{"user": {"_id":"000000000000000000000001",
              "username":"test",
              "email":"leader@gmail.com",
              "isAdmin": false
              }}`.parseJsonString;
    });

    describe("without a token", {
      it("should not update an user", {
        router
          .request
          .put("/users/000000000000000000000001")
          .send(user)
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "error": "Authorization required"
            }`.parseJsonString);
          });
      });
    });

    describe("with an admin token", {
      it("should update an user", {
        router
          .request
          .put("/users/000000000000000000000001")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .send(user)
          .expectStatusCode(200)
          .end((Response response) => () {
            auto expected = `{"user": {
              "_id":"000000000000000000000001",
              "username":"test",
              "name":"test",
              "email":"leader@gmail.com",
              "isAdmin": false,
              "isActive": true
            }}`.parseJsonString;

            expected["user"]["lastActivity"] = response.bodyJson["user"]["lastActivity"];
            response.bodyJson.should.equal(expected);
          });
      });

      it("should not update the email to an exiting one", {
        Json data = user.clone;
        data["user"]["email"] = "member@gmail.com";

        router
          .request
          .put("/users/000000000000000000000001")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .send(data)
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "errors": [{
                "title": "Validation error",
                "description": "The email is already taken.",
                "status": 400
              }]
            }`.parseJsonString);
          });
      });

      it("should not update the user if the email field is missing", {
        Json data = user.clone;
        data["user"].remove("email");

        router
          .request
          .put("/users/000000000000000000000001")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .send(data)
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal((`{
              "errors": [{
                "title": "Validation error",
                "description": "The ` ~ "`email` field is missing from the `user`" ~ ` object.",
                "status": 400
              }]
            }`).parseJsonString);
          });
      });
    });

    describe("with an user token", {
      it("should change the own user data", {
        Json data = user.clone;

        router
          .request
          .put("/users/000000000000000000000001")
          .header("Authorization", "Bearer " ~ bearerLeaderToken.name)
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            auto expected = `{"user": {
              "_id":"000000000000000000000001",
              "username":"test",
              "name":"test",
              "email":"leader@gmail.com",
              "isAdmin": false,
              "isActive": true
            }}`.parseJsonString;

            response.bodyJson.should.equal(expected);
          });
      });

      it("should not change the other user data", {
        Json data = `{ "user": {"_id":"000000000000000000000002",
              "username":"test",
              "email":"member@gmail.com",
              "isAdmin": false,
              "name":"John Doe"}}`.parseJsonString;

        router
          .request
          .put("/users/000000000000000000000002")
          .header("Authorization", "Bearer " ~ bearerLeaderToken.name)
          .send(data)
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "errors": [{
                "description": "You can't edit other users data.",
                "status": 400,
                "title": "Validation error"
              }]}`.parseJsonString);
          });
      });
    });
  });
});
