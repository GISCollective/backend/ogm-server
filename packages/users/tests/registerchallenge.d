/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.users.registerchallenge;

import tests.fixtures;
import ogm.users.api;
import ogm.defaults.users;

import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({
  URLRouter router;
  Json broadcastValue;
  MemoryBroadcast broadcast;

  describe("registerchallenge", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      broadcast = new MemoryBroadcast;
      router.crateSetup.setupUserApi(crates, broadcast);
      setupDefaultUsers(crates);

      broadcastValue = Json.emptyObject;
      void testHandler(const Json value) @trusted {
        broadcastValue = value;
      }

      broadcast.register("user.new", &testHandler);
    });

    it("should return an empty object when no captcha is enabled", {
      router
        .request
        .post("/users/registerchallenge")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{}`.parseJsonString);
        });
    });

    describe("with the test captcha enabled", {
      beforeEach({
        crates.preference.addItem(`{
          "name": "captcha.enabled",
          "value": "test"
        }`.parseJsonString);
      });

      it("returns the config", {
        router
          .request
          .post("/users/registerchallenge")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{ "config": "value" }`.parseJsonString);
          });
      });
    });
  });
});
