/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.users.get;

import tests.fixtures;

import std.algorithm;
import std.array;
import ogm.users.api;
import ogm.defaults.users;

import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({
  URLRouter router;

  describe("GET", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupUserApi(crates, new MemoryBroadcast);
    });

    describe("with an user that has a profile with all name fields", {
      beforeEach({
        auto profile = UserProfile().serializeToJson;

        profile["title"] = "Dr.";
        profile["firstName"] = "First";
        profile["lastName"] = "Name";

        profile["picture"] = "000000000000000000000001";

        crates.userProfile.addItem(profile);
      });

      it("fills in the profile name", {
        router
          .request
          .get("/users/000000000000000000000001")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["user"].should.equal(`{"_id":"000000000000000000000001",
              "username":"test",
              "name": "Dr. First Name",
              "email":"leader@gmail.com",
              "isAdmin": false,
              "isActive": true
            }`.parseJsonString);
          });
      });
    });

    describe("with an user that has a profile without name fields", {
      beforeEach({
        auto profile = UserProfile().serializeToJson;
        profile["picture"] = "000000000000000000000001";

        crates.userProfile.addItem(profile);
      });

      it("fills in the username as name", {
        router
          .request
          .get("/users/000000000000000000000001")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["user"].should.equal(`{"_id":"000000000000000000000001",
              "username":"test",
              "name": "test",
              "email":"leader@gmail.com",
              "isAdmin": false,
              "isActive": true
            }`.parseJsonString);
          });
      });
    });

    describe("without a token", {
      it("should not return the user list", {
        router
          .request
          .get("/users")
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
          });
      });

      it("should return the name and username on getting an user", {
        router
          .request
          .get("/users/000000000000000000000002")
          .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal(`{
                "user": {
                  "_id":"000000000000000000000002",
                  "username":"test",
                  "name":"test",
                  "isActive": true
                }
              }`.parseJsonString);
            });
      });

      it("should return an error when passing the term query parameter", {
        router
          .request
          .get("/users?term=test")
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
          });
      });
    });

    describe("with an invalid token", {
      it("should not return any user", {
        router
          .request
          .get("/users")
          .header("Authorization", "Bearer random")
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"error\": \"Invalid token.\"}".parseJsonString);
          });
      });
    });

    describe("with an admin token", {
      it("should return all users when not passing the active query parameter" , {
        router
          .request
          .get("/users")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            auto users = cast(Json[]) response.bodyJson["users"];
            users.length.should.equal(6).because("there are 6 items in the db");
            foreach(ref user; users) {
              user["lastActivity"] = 0;
            }

            users.should.contain(`{"_id":"000000000000000000000001",
              "username":"test",
              "name":"test",
              "email":"leader@gmail.com",
              "isAdmin": false,
              "isActive": true,
              "lastActivity": 0
            }`.parseJsonString);

            users.should.contain(`{"_id":"000000000000000000000002",
              "username":"test",
              "name":"test",
              "email":"member@gmail.com",
              "isAdmin": false,
              "isActive": true,
              "lastActivity": 0
            }`.parseJsonString);

            users.should.contain(`{"_id":"000000000000000000000003",
              "username":"test",
              "name":"test",
              "email":"guest@gmail.com",
              "isAdmin": false,
              "isActive": true,
              "lastActivity": 0
            }`.parseJsonString);

            users.should.contain(`{"_id":"000000000000000000000004",
              "username":"test",
              "name":"test",
              "email":"owner@gmail.com",
              "isAdmin": false,
              "isActive": true,
              "lastActivity": 0
            }`.parseJsonString);

            users.should.contain(`{"_id":"000000000000000000000005",
              "username":"test",
              "name":"test",
              "email":"admin@gmail.com",
              "isAdmin": true,
              "isActive": true,
              "lastActivity": 0
            }`.parseJsonString);
          });
      });

      it("should return all users as csv when format=csv" , {
        auto profile = UserProfile().serializeToJson;
        profile["picture"] = "000000000000000000000001";
        profile["showCalendarContributions"] = true;
        profile["_id"] = "000000000000000000000001";

        crates.userProfile.addItem(profile);

        router
          .request
          .get("/users?format=csv")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            auto users = response.bodyString.split("\n");
            users.length.should.equal(8).because("there are 7 items in the db + csv header");

            users[0].should.equal(`_id,email,salutation,title,firstName,lastName,bio,jobTitle,organization,joinedTime,createdAt,lastActivity,linkedin,location.detailedLocation.city,location.detailedLocation.country,location.detailedLocation.postalCode,location.detailedLocation.province,location.isDetailed,location.simple,picture,showCalendarContributions,showPrivateContributions,showWelcomePresentation,skype,statusEmoji,statusMessage,twitter,website,`);
            users[1].should.contain(`000000000000000000000001,leader@gmail.com,,,,,,,,0001-01-01T00:00:00+00:00,0001-01-01T00:00:00+00:00`);
            users[2].should.contain(`null,member@gmail.com,null,null,null,null,null,null,null,null,0001-01-01T00:00:00+00:00,`);
        });
      });

      describe("when no users are active and passing the active query parameter", {
        it("should return no users when active has value false" , {
          router
            .request
            .get("/users?active=false")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              auto users = cast(Json[]) response.bodyJson["users"];
              users.length.should.equal(0).because("there are 6 active users in the db");
            });
        });

        it("should return all users when active has value true" , {
          router
            .request
            .get("/users?active=true")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              auto users = cast(Json[]) response.bodyJson["users"];
              users.length.should.equal(6).because("there are 6 active users in the db");
            });
        });
      });

      it("should return all user data on getting a user", {
        router
          .request
          .get("/users/000000000000000000000002")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
            .end((Response response) => () {
              auto expected = `{"user": {
                "_id":"000000000000000000000002",
                "email":"member@gmail.com",
                "isAdmin": false,
                "isActive": true,
                "name":"test",
                "username":"test"
              }}`.parseJsonString;

              expected["user"]["lastActivity"] = response.bodyJson["user"]["lastActivity"];

              response.bodyJson.should.equal(expected);
            });

      });

      it("should return a users list when passing term as query parameter", {
        router
          .request
          .get("/users?term=test")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
            .end((Response response) => () {
              auto users = cast(Json[]) response.bodyJson["users"];
              users.length.should.equal(6).because("there are 6 items in the db with the searched term as username");

              foreach(ref user; users) {
                user["lastActivity"] = 0;
              }

              users.should.contain(`{"_id":"000000000000000000000001",
                "username":"test",
                "name":"test",
                "email":"leader@gmail.com",
                "isAdmin": false,
                "isActive": true,
                "lastActivity": 0
              }`.parseJsonString);

              users.should.contain(`{"_id":"000000000000000000000002",
                "username":"test",
                "name":"test",
                "email":"member@gmail.com",
                "isAdmin": false,
                "isActive": true,
                "lastActivity": 0
              }`.parseJsonString);

              users.should.contain(`{"_id":"000000000000000000000003",
                "username":"test",
                "name":"test",
                "email":"guest@gmail.com",
                "isAdmin": false,
                "isActive": true,
                "lastActivity": 0
              }`.parseJsonString);

              users.should.contain(`{"_id":"000000000000000000000004",
                "username":"test",
                "name":"test",
                "email":"owner@gmail.com",
                "isAdmin": false,
                "isActive": true,
                "lastActivity": 0
              }`.parseJsonString);

              users.should.contain(`{"_id":"000000000000000000000005",
                "username":"test",
                "name":"test",
                "email":"admin@gmail.com",
                "isAdmin": true,
                "isActive": true,
                "lastActivity": 0
              }`.parseJsonString);
            });
      });
    });

    describe("with an owner token", {
      it("should not return the user list when not passing the active query parameter" , {
        router
          .request
          .get("/users")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"errors":
            [
              {
                "description": "Authorization required",
                "status": 401,
                "title": "Unauthorized"
              }
            ]}`.parseJsonString);
          });
      });

      it("should not return any users when passing the active query parameter with value true" , {
        router
          .request
          .get("/users?active=true")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"errors":
            [
              {
                "description": "Authorization required",
                "status": 401,
                "title": "Unauthorized"
              }
            ]}`.parseJsonString);
          });
      });

      it("should not return any users when passing the active query parameter with value false" , {
        router
          .request
          .get("/users?active=false")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"errors":
            [
              {
                "description": "Authorization required",
                "status": 401,
                "title": "Unauthorized"
              }
            ]}`.parseJsonString);
          });
      });

      it("should return all user data on getting a user", {
        router
          .request
          .get("/users/000000000000000000000002")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(200)
            .end((Response response) => () {
              auto expected = `{"user": {
                "_id":"000000000000000000000002",
                "email":"member@gmail.com",
                "isAdmin": false,
                "isActive": true,
                "name":"test",
                "username":"test"
              }}`.parseJsonString;

            response.bodyJson.should.equal(expected);
          });
      });

      it("should return a users list when passing term as query parameter and its value has 3 characters", {
        router
          .request
          .get("/users?term=tes")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(200)
            .end((Response response) => () {
              auto users = cast(Json[]) response.bodyJson["users"];
              users.length.should.equal(6).because("there are 6 items in the db with the searched term as username");

              users.should.contain(`{"_id":"000000000000000000000001",
                "username":"test",
                "name":"test",
                "email":"leader@gmail.com",
                "isAdmin": false,
                "isActive": true}`.parseJsonString);

              users.should.contain(`{"_id":"000000000000000000000002",
                "username":"test",
                "name":"test",
                "email":"member@gmail.com",
                "isAdmin": false,
                "isActive": true}`.parseJsonString);

              users.should.contain(`{"_id":"000000000000000000000003",
                "username":"test",
                "name":"test",
                "email":"guest@gmail.com",
                "isActive": true,
                "isAdmin": false}`.parseJsonString);

              users.should.contain(`{"_id":"000000000000000000000004",
                "username":"test",
                "name":"test",
                "email":"owner@gmail.com",
                "isAdmin": false,
                "isActive": true}`.parseJsonString);

              users.should.contain(`{"_id":"000000000000000000000005",
                "username":"test",
                "name":"test",
                "email":"admin@gmail.com",
                "isAdmin": true,
                "isActive": true}`.parseJsonString);
            });
      });

      it("should return an empty list when passing term as query parameter and its value has no characters", {
        router
          .request
          .get("/users?term=")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(200)
            .end((Response response) => () {
              auto users = cast(Json[]) response.bodyJson["users"];
              users.length.should.equal(0).because("less than 3 characters were set as search term");
            });
      });

      it("should return an empty list when passing term as query parameter and its value has two characters", {
        router
          .request
          .get("/users?term=te")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(200)
            .end((Response response) => () {
              auto users = cast(Json[]) response.bodyJson["users"];
              users.length.should.equal(0).because("less than 3 characters were set as search term");
            });
      });

    });

    describe("with a leader token", {
      it("should not return the user list" , {
        router
          .request
          .get("/users")
          .header("Authorization", "Bearer " ~ bearerLeaderToken.name)
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"errors":
            [
              {
                "description": "Authorization required",
                "status": 401,
                "title": "Unauthorized"
              }
            ]}`.parseJsonString);
          });
      });
    });

    describe("with a guest token", {
      it("should not return the user list" , {
        router
          .request
          .get("/users")
          .header("Authorization", "Bearer " ~ bearerMemberToken.name)
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"errors":
            [
              {
                "description": "Authorization required",
                "status": 401,
                "title": "Unauthorized"
              }
            ]}`.parseJsonString);
          });
      });
    });
  });
});
