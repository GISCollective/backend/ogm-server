/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.users.googletoken;

import ogm.users.actions.googleToken;
import tests.fixtures;
import std.base64;
import ogm.users.api;
import ogm.defaults.users;

import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({
  URLRouter router;
  Json registerData;
  Json broadcastValue;
  MemoryBroadcast broadcast;
  MockTokenValidation mockValidation;

  describe("POST", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      broadcast = new MemoryBroadcast;
      router.crateSetup.setupUserApi(crates, broadcast);
      setupDefaultUsers(crates);

      registerData = `{
        "idtoken": "some-token"
      }`.parseJsonString;

      mockValidation = new MockTokenValidation();
      GoogleTokenValidation.instance = mockValidation;

      broadcastValue = Json.emptyObject;
      void testHandler(const Json value) @trusted {
        broadcastValue = value;
      }

      broadcast.register("user.new", &testHandler);
    });

    it("returns a bearer token for a valid google token", {
      crates.preference.addItem(`{
        "_id": "1",
        "name": "integrations.google.client_id",
        "value": "CLIENT_ID"
      }`.parseJsonString);

      mockValidation.result = `{
        "email": "member@gmail.com",
      }`.parseJsonString;

      router
        .request
        .post("/auth/googletoken")
        .send(registerData)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["token_type"].should.equal("Bearer");
          response.bodyJson["expires_in"].should.equal(3600);

          auto user = crates.user.get.where("email").equal("member@gmail.com").and.exec.front;
          auto token = user["tokens"].byValue
            .filter!(a => a["type"] == "Bearer" && a["name"] == response.bodyJson["access_token"]);

          auto refresh = user["tokens"].byValue
            .filter!(a => a["type"] == "Refresh" && a["name"] == response.bodyJson["refresh_token"]);

          token.empty.should.equal(false);
          refresh.empty.should.equal(false);
          broadcastValue.should.equal(`{ }`.parseJsonString);
        });
    });

    it("creates a new user when the email is not found - lower case", {
      crates.preference.addItem(`{
        "_id": "1",
        "name": "integrations.google.client_id",
        "value": "CLIENT_ID"
      }`.parseJsonString);

      mockValidation.result = `{
        "email": "new.user@gmail.com",
      }`.parseJsonString;

      router
        .request
        .post("/auth/googletoken")
        .send(registerData)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["token_type"].should.equal("Bearer");
          response.bodyJson["expires_in"].should.equal(3600);

          auto user = crates.user.get.where("email").equal("new.user@gmail.com").and.exec.front;
          auto token = user["tokens"].byValue
            .filter!(a => a["type"] == "Bearer" && a["name"] == response.bodyJson["access_token"]);

          auto refresh = user["tokens"].byValue
            .filter!(a => a["type"] == "Refresh" && a["name"] == response.bodyJson["refresh_token"]);

          user["username"].to!string.should.equal("new.user");

          token.empty.should.equal(false);
          refresh.empty.should.equal(false);
          broadcastValue.should.equal(`{ "id": "000000000000000000000007" }`.parseJsonString);
        });
    });

    it("creates a new user when the email is not found - upper case", {
      crates.preference.addItem(`{
        "_id": "1",
        "name": "integrations.google.client_id",
        "value": "CLIENT_ID"
      }`.parseJsonString);

      mockValidation.result = `{
        "email": "NEW.user@gmail.com",
      }`.parseJsonString;

      router
        .request
        .post("/auth/googletoken")
        .send(registerData)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["token_type"].should.equal("Bearer");
          response.bodyJson["expires_in"].should.equal(3600);

          auto user = crates.user.get.where("email").equal("new.user@gmail.com").and.exec.front;
          auto token = user["tokens"].byValue
            .filter!(a => a["type"] == "Bearer" && a["name"] == response.bodyJson["access_token"]);

          auto refresh = user["tokens"].byValue
            .filter!(a => a["type"] == "Refresh" && a["name"] == response.bodyJson["refresh_token"]);

          user["username"].to!string.should.equal("new.user");

          token.empty.should.equal(false);
          refresh.empty.should.equal(false);
          broadcastValue.should.equal(`{ "id": "000000000000000000000007" }`.parseJsonString);
        });
    });

    it("adds an index to the username if one already exists", {
      crates.preference.addItem(`{
        "_id": "1",
        "name": "integrations.google.client_id",
        "value": "CLIENT_ID"
      }`.parseJsonString);

      mockValidation.result = `{
        "email": "test@gmail.com",
      }`.parseJsonString;

      router
        .request
        .post("/auth/googletoken")
        .send(registerData)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["token_type"].should.equal("Bearer");
          response.bodyJson["expires_in"].should.equal(3600);

          auto user = crates.user.get.where("email").equal("test@gmail.com").and.exec.front;
          auto token = user["tokens"].byValue
            .filter!(a => a["type"] == "Bearer" && a["name"] == response.bodyJson["access_token"]);

          auto refresh = user["tokens"].byValue
            .filter!(a => a["type"] == "Refresh" && a["name"] == response.bodyJson["refresh_token"]);

          user["username"].to!string.should.equal("test.1");

          token.empty.should.equal(false);
          refresh.empty.should.equal(false);
          broadcastValue.should.equal(`{ "id": "000000000000000000000007" }`.parseJsonString);
        });
    });

    it("returns an error when the google.client_id preference is not set", {
      crates.preference.addItem(`{
        "_id": "1",
        "name": "integrations.google.client_id",
        "value": ""
      }`.parseJsonString);

      router
        .request
        .post("/auth/googletoken")
        .send(registerData)
        .expectStatusCode(400)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "errors": [{
              "description": "The google configuration is missing.",
              "status": 400,
              "title": "Validation error"
            }]}`.parseJsonString);
        });
    });

    it("returns an error when the data does not contain an email", {
      crates.preference.addItem(`{
        "_id": "1",
        "name": "integrations.google.client_id",
        "value": "CLIENT_ID"
      }`.parseJsonString);

      mockValidation.result = Json.emptyObject;

      router
        .request
        .post("/auth/googletoken")
        .send(registerData)
        .expectStatusCode(400)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "errors": [{
              "description": "The token is invalid.",
              "status": 400,
              "title": "Validation error"
            }]}`.parseJsonString);
        });
    });
  });
});


class MockTokenValidation: IGoogleTokenValidation {
  Json result;
  string lastToken;
  string lastAudience;

  Json verifyIdToken(string token, string audience) {
    this.lastToken = token;
    this.lastAudience = audience;

    return result;
  }
}