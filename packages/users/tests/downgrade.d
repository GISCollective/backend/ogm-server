/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.users.downgrade;

import tests.fixtures;

import std.algorithm;
import std.array;
import ogm.users.api;
import ogm.defaults.users;

import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({
  URLRouter router;

  describe("POST.downgrade", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupUserApi(crates, new MemoryBroadcast);
      setupDefaultUsers(crates);
    });

    describe("without a token", {
      it("should return an auth error", {
        router
          .request
          .post("/users/000000000000000000000002/downgrade")
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
          });
      });
    });

    describe("with an invalid token", {
      it("should return an auth error", {
        router
          .request
          .post("/users/000000000000000000000002/downgrade")
          .header("Authorization", "Bearer random")
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"error\": \"Invalid token.\"}".parseJsonString);
          });
      });
    });

    describe("with an admin token", {
      beforeEach({
        auto user = crates.user.get.where("_id").equal("000000000000000000000002").and.exec.front;
        user["scopes"] ~= Json("admin");

        crates.user.updateItem(user);
      });

      it("should downgrade any user" , {
        auto data = `{
          "password": "password"
        }`.parseJsonString;

        router
          .request
          .post("/users/000000000000000000000002/downgrade")
          .send(data)
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
              auto user = crates.user
                .get.where("_id").equal(ObjectId.fromString("000000000000000000000002"))
                .and.exec.front;

              user["scopes"].length.should.equal(0);
          });
      });

      it("should not downgrade the user if the password is invalid" , {
        auto data = `{
          "password": "bad_password"
        }`.parseJsonString;

        router
          .request
          .post("/users/000000000000000000000002/downgrade")
          .send(data)
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(400)
          .end((Response response) => () {
             auto user = crates.user
                .get.where("_id").equal(ObjectId.fromString("000000000000000000000002"))
                .and.exec.front;

            user["scopes"].length.should.equal(1);

            response.bodyJson.should.equal(`{"errors": [{
              "title": "Invalid password.",
              "description": "Your password is not valid.",
              "status": 400 }]}`.parseJsonString);
          });
      });

      it("should not downgrade yourself" , {
        auto data = `{
          "password": "bad_password"
        }`.parseJsonString;

        router
          .request
          .post("/users/000000000000000000000005/downgrade")
          .send(data)
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(400)
          .end((Response response) => () {
            auto user = crates.user
                .get.where("_id").equal(ObjectId.fromString("000000000000000000000005"))
                .and.exec.front;

            user["scopes"].length.should.equal(1);
            user["scopes"][0].should.equal("admin");

            response.bodyJson.should.equal(`{"errors": [{
              "title": "Invalid operation",
              "description": "You can't downgrade yourself.",
              "status": 400 }]}`.parseJsonString);
          });
      });
    });

    describe("with an owner token", {
      it("should not downgrade yourself" , {
        auto data = `{
          "password": "password",
        }`.parseJsonString;

        router
          .request
          .post("/users/000000000000000000000004/downgrade")
          .send(data)
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(400)
          .end((Response response) => () {
            auto user = crates.user
                .get.where("_id").equal(ObjectId.fromString("000000000000000000000004"))
                .and.exec.front;

            user["scopes"].length.should.equal(0);

            response.bodyJson.should.equal(`{"errors": [{
              "title": "You are not authorized",
              "description": "You must be an admin.",
              "status": 400 }]}`.parseJsonString);
          });
      });

      it("should not downgrade other users" , {
        auto data = `{
          "password": "password"
        }`.parseJsonString;

        router
          .request
          .post("/users/000000000000000000000005/downgrade")
          .send(data)
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(400)
          .end((Response response) => () {
            auto user = crates.user
                .get.where("_id").equal(ObjectId.fromString("000000000000000000000005"))
                .and.exec.front;

            user["scopes"].length.should.equal(1);

            response.bodyJson.should.equal(`{"errors": [{
              "title": "You are not authorized",
              "description": "You must be an admin.",
              "status": 400 }]}`.parseJsonString);
          });
      });
    });
  });
});
