/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.users.register;

import tests.fixtures;
import ogm.users.api;
import ogm.calendar;
import ogm.defaults.users;
import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({
  URLRouter router;
  Json registerData;
  Json broadcastValue;
  MemoryBroadcast broadcast;

  describe("POST", {
    beforeEach({
      setupTestData();

      auto calendarMock = new CalendarMock("2023-01-12T16:20:11Z");
      SysCalendar.instance = calendarMock;

      router = new URLRouter;
      broadcast = new MemoryBroadcast;
      router.crateSetup.setupUserApi(crates, broadcast);
      setupDefaultUsers(crates);

      registerData = `{
        "salutation":"mr",
        "title":"Dr.",
        "firstName":"Bogdan",
        "lastName":"Szabo",
        "username":"testbogdan",
        "email":"me@asd.com",
        "password":"t28FGxbc6GGcLan",
        "challenge":""
      }`.parseJsonString;

      broadcastValue = Json.emptyObject;
      void testHandler(const Json value) @trusted {
        broadcastValue = value;
      }

      broadcast.register("user.new", &testHandler);

      crates.newsletter.addItem(`{
        "name": "test",
        "description": "new description",
        "visibility": { "isDefault": true, "isPublic": true, "team": "000000000000000000000001" },
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);


      crates.newsletter.addItem(`{
        "name": "test",
        "description": "new description",
        "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" },
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);
    });

    it("should register an user", {
      router
        .request
        .post("/users/register")
        .send(registerData)
        .expectStatusCode(204)
        .end((Response response) => () {
          response.bodyString.should.equal("");

          auto user = crates.user.get.where("email").equal("me@asd.com").and.exec.front;
          user["email"].should.equal(`me@asd.com`);
          user["username"].should.equal(`testbogdan`);
          user["salt"].should.not.equal(``);
          user["password"].should.not.equal(``);
          user["isActive"].should.equal(false);

          broadcastValue.should.equal((`{
            "id": "` ~ user["_id"].to!string ~ `"
          }`).parseJsonString);
        });
    });

    it("should create a profile", {
      router
        .request
        .post("/users/register")
        .send(registerData)
        .expectStatusCode(204)
        .end((Response response) => () {
          response.bodyString.should.equal("");

          auto userId = crates.user.get.where("email").equal("me@asd.com").and.exec.front["_id"];
          auto profile = crates.userProfile.get.where("_id").equal(ObjectId.fromJson(userId)).and.exec.front;

          profile["salutation"].should.equal(`mr`);
          profile["title"].should.equal(`Dr.`);
          profile["firstName"].should.equal(`Bogdan`);
          profile["lastName"].should.equal(`Szabo`);
          profile["showWelcomePresentation"].should.equal(true);
        });
    });

    it("should trigger an error when one of the fields are missing", {
      router
        .request
        .post("/users/register")
        .send(`{}`.parseJsonString)
        .expectStatusCode(400)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "errors": [{
            "description": "Fields must be string: salutation, title, firstName, lastName, username, email, password",
            "status": 400,
            "title": "Validation error"
          }]}`.parseJsonString);
        });
    });

    it("should trigger an error when the password has 9 chars", {
      registerData["password"] = "123456789";

      router
        .request
        .post("/users/register")
        .send(registerData)
        .expectStatusCode(400)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "errors": [{
            "description": "Your password should have at least 10 characters.",
            "status": 400,
            "title": "Validation error"
          }]}`.parseJsonString);
        });
    });

    it("should trigger an error when the emails is taken", {
      crates.user.addItem(`{
        "email": "me@asd.com",
        "username": "test"
      }`.parseJsonString);

      router
        .request
        .post("/users/register")
        .send(registerData)
        .expectStatusCode(400)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "errors": [{
            "description": "Email has already been taken.",
            "status": 400,
            "title": "Validation error"
          }]}`.parseJsonString);
        });
    });

    it("should not trigger an error when an email postfix is taken", {
      crates.user.addItem(`{
        "email": "also.me@asd.com",
        "username": "test"
      }`.parseJsonString);

      router
        .request
        .post("/users/register")
        .send(registerData)
        .expectStatusCode(204)
        .end((Response response) => () {
          response.bodyString.should.equal("");
        });
    });

    it("should trigger an error when the username is taken", {
      crates.user.addItem(`{
        "email": "other@asd.com",
        "username": "testbogdan"
      }`.parseJsonString);

      router
        .request
        .post("/users/register")
        .send(registerData)
        .expectStatusCode(400)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "errors": [{
            "description": "Username has already been taken.",
            "status": 400,
            "title": "Validation error"
          }]}`.parseJsonString);
        });
    });

    it("should trigger an error when the email contains chimney", {

      registerData = `{
        "salutation":"mr",
        "title":"Dr.",
        "firstName":"Bogdan",
        "lastName":"Szabo",
        "username":"testbogdan",
        "email":"me@chimney.com",
        "password":"t28FGxbc6GGcLan",
        "challenge":""
      }`.parseJsonString;

      router
        .request
        .post("/users/register")
        .send(registerData)
        .expectStatusCode(400)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "errors": [{
            "description": "Your email is banned.",
            "status": 400,
            "title": "Validation error"
          }]}`.parseJsonString);
        });
    });

    it("adds the user to the default newsletters when newsletter=true", {
      auto data = registerData.clone;
      data["newsletter"] = true;

      router
        .request
        .post("/users/register")
        .send(data)
        .expectStatusCode(204)
        .end((Response response) => () {
          auto record = crates.newsletterEmail.get.where("email").equal("me@asd.com").and.exec.front;

          expect(record).to.equal(`{
            "_id": "000000000000000000000001",
            "email": "me@asd.com",
            "list": [{
              "newsletter": "000000000000000000000001",
              "joinedOn": "2023-01-12T16:20:11Z",
            }]
          }`.parseJsonString);
        });
    });

    describe("when there is a team with an invitation", {
      Json team;
      Json welcomeMessage;

      beforeEach({
        team = crates.team.getItem("000000000000000000000001").exec.front;
        team["invitations"] = `[{
          "email": "me@asd.com",
          "role": "leaders"
        }]`.parseJsonString;

        crates.team.updateItem(team);

        welcomeMessage = null;
        void messageWelcomeHandler(const Json value) {
          welcomeMessage = value;
        }

        broadcast.register("team.welcome", &messageWelcomeHandler);
      });

      it("adds the user to the team", {
        router
          .request
          .post("/users/register")
          .send(registerData)
          .expectStatusCode(204)
          .end((Response response) => () {
            response.bodyString.should.equal(``);

            auto team = crates.team.getItem("000000000000000000000001").exec.front;
            team["invitations"].length.should.equal(0);

            team["leaders"].should.equal(`["leader@gmail.com", "000000000000000000000001", "000000000000000000000007"]`.parseJsonString);
            welcomeMessage.should.equal(`{
              "id": "000000000000000000000001.me@asd.com",
              "role": "leaders",
              "team": "000000000000000000000001",
              "user": "000000000000000000000007"
            }`.parseJsonString);
          });
      });

      it("ignores the invitation when the role is invalid", {
        team["invitations"] = `[{
          "email": "me@asd.com",
          "role": "invalid"
        }]`.parseJsonString;

        crates.team.updateItem(team);

        router
          .request
          .post("/users/register")
          .send(registerData)
          .expectStatusCode(204)
          .end((Response response) => () {
            response.bodyString.should.equal(``);

            auto team = crates.team.getItem("000000000000000000000001").exec.front;
            team["invitations"].length.should.equal(0);

            team["guests"].should.equal(`["guest@gmail.com", "000000000000000000000003"]`.parseJsonString);
            team["members"].should.equal(`["member@gmail.com", "000000000000000000000002"]`.parseJsonString);
            team["leaders"].should.equal(`["leader@gmail.com", "000000000000000000000001"]`.parseJsonString);
            team["owners"].should.equal(`["owner@gmail.com", "000000000000000000000004"]`.parseJsonString);
          });
      });
    });

    describe("with the test captcha enabled", {
      beforeEach({
        crates.preference.addItem(`{
          "name": "captcha.enabled",
          "value": "test"
        }`.parseJsonString);
      });

      it("fails when the challenge is not set", {
        router
          .request
          .post("/users/register")
          .send(registerData)
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{ "errors": [{
              "description": "Invalid challenge response.",
              "status": 400,
              "title": "Validation error"
            }]}`.parseJsonString);

            auto user = crates.user.get.where("email").equal("me@asd.com").and.exec;
            user.empty.should.equal(true);
          });
      });

      it("is successfull when the challenge is correct", {
        registerData["challenge"] = "test";

        router
          .request
          .post("/users/register")
          .send(registerData)
          .expectStatusCode(204)
          .end((Response response) => () {
            response.bodyString.should.equal("");
          });
      });
    });
  });
});
