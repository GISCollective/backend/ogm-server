/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.users.testNotification;

import tests.fixtures;
import ogm.users.api;
import ogm.defaults.users;

import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({
  URLRouter router;
  Json broadcastValue;
  MemoryBroadcast broadcast;
  Json data;

  describe("testNotification", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      broadcast = new MemoryBroadcast;
      router.crateSetup.setupUserApi(crates, broadcast);
      setupDefaultUsers(crates);

      broadcastValue = Json.emptyObject;
      void testHandler(const Json value) @trusted {
        broadcastValue = value;
      }

      broadcast.register("user.testNotification", &testHandler);

      data = Json.emptyObject;
    });

    describe("when the email field is set", {
      it("returns an error whn no token is provided", {
        router
          .request
          .post("/users/" ~ userId["guest"] ~ "/testNotification")
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{ "error": "Authorization required" }`.parseJsonString);
          });
      });

      it("returns an error when a normal token is provided ", {
        router
          .request
          .post("/users/" ~ userId["guest"] ~ "/testNotification")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "errors": [{
                "description": "You must be an admin.",
                "status": 400,
                "title": "You are not authorized"
              }]}`.parseJsonString);
          });
      });

      it("should trigger a task with the user id when there is an admin token", {
        router
          .request
          .post("/users/" ~ userId["guest"] ~ "/testNotification")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(204)
          .end((Response response) => () {
            broadcastValue.should.equal((`{
              "id": "` ~ userId["guest"] ~ `"
            }`).parseJsonString);
          });
      });
    });
  });
});
