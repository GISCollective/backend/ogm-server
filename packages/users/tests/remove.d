/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.users.remove;

import tests.fixtures;
import ogm.users.api;
import ogm.defaults.users;

import std.algorithm;
import std.array;

import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({
  URLRouter router;

  describe("POST.remove", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupUserApi(crates, new MemoryBroadcast);
      setupDefaultUsers(crates);
    });

    describe("without a token", {
      it("should return an auth error", {
        router
          .request
          .post("/users/000000000000000000000002/remove")
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
          });
      });
    });

    describe("with an invalid token", {
      it("should return an auth error", {
        router
          .request
          .post("/users/000000000000000000000002/remove")
          .header("Authorization", "Bearer random")
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"error\": \"Invalid token.\"}".parseJsonString);
          });
      });
    });

    describe("with an admin token", {
      it("should remove any user" , {
        auto data = `{
          "password": "password"
        }`.parseJsonString;

        router
          .request
          .post("/users/000000000000000000000002/remove")
          .send(data)
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
              crates.user
                .get.where("_id").equal(ObjectId.fromString("000000000000000000000002"))
                .and.exec
                .empty.should.equal(true);
          });
      });

      it("should not remove the user if the password is invalid" , {
        auto data = `{
          "password": "bad_password"
        }`.parseJsonString;

        router
          .request
          .post("/users/000000000000000000000002/remove")
          .send(data)
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(400)
          .end((Response response) => () {
              crates.user
                .get.where("_id").equal(ObjectId.fromString("000000000000000000000002"))
                .and.exec
                .empty.should.equal(false);

            response.bodyJson.should.equal(`{"errors": [{
              "title": "Invalid password.",
              "description": "Your password is not valid.",
              "status": 400 }]}`.parseJsonString);
          });
      });

      it("should not remove yourself" , {
        auto data = `{
          "password": "password"
        }`.parseJsonString;

        router
          .request
          .post("/users/000000000000000000000005/remove")
          .send(data)
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(400)
          .end((Response response) => () {
              crates.user
                .get.where("_id").equal(ObjectId.fromString("000000000000000000000005"))
                .and.exec
                .empty.should.equal(false);

            response.bodyJson.should.equal(`{"errors": [{
              "title": "Invalid action",
              "description": "Your can't remove yourself.",
              "status": 400 }]}`.parseJsonString);
          });
      });
    });

    describe("with an owner token", {
      it("should remove the auth user" , {
        auto data = `{
          "password": "password",
        }`.parseJsonString;

        router
          .request
          .post("/users/000000000000000000000004/remove")
          .send(data)
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
              crates.user
                .get.where("_id").equal(ObjectId.fromString("000000000000000000000004"))
                .and.exec
                .empty.should.equal(true);
          });
      });

      it("should not remove other users" , {
        auto data = `{
          "password": "password"
        }`.parseJsonString;

        router
          .request
          .post("/users/000000000000000000000002/remove")
          .send(data)
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(400)
          .end((Response response) => () {
              crates.user
                .get.where("_id").equal(ObjectId.fromString("000000000000000000000004"))
                .and.exec
                .empty.should.equal(false);

            response.bodyJson.should.equal(`{"errors": [{
              "title": "You are not authorized",
              "description": "You can not remove other users.",
              "status": 400 }]}`.parseJsonString);
          });
      });
    });
  });
});
