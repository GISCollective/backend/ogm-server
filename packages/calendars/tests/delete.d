/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.calendars.delete_;

import tests.fixtures;
import vibe.http.common;
import fluent.asserts;
import gis_collective.hmq.broadcast.memory;
import std.algorithm;
import ogm.defaults.articles;
import ogm.defaults.teams;
import ogm.calendars.api;

alias suite = Spec!({
  URLRouter router;

  string publicCalendarId;
  string calendarOtherTeamId;

  describe("deleting calendars", {
    beforeEach({
      setupTestData();
      auto broadcast = new MemoryBroadcast;
      router = new URLRouter;
      router.crateSetup.setupCalendarApi(crates, broadcast);

      crates.setupDefaultTeams;
      crates.setupDefaultArticles;

      publicCalendarId = "000000000000000000000001";
      calendarOtherTeamId = "000000000000000000000003";
    });

    describeCredentialsRule("delete from own team", "yes", "calendars", ["administrator", "owner"], (string type) {
      router
        .request
        .delete_("/calendars/" ~ publicCalendarId)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end;
    });

    describeCredentialsRule("delete from own team", "yes", "calendars", ["leader", "member", "guest"], (string type) {
      router
        .request
        .delete_("/calendars/" ~ publicCalendarId)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end;
    });

    describeCredentialsRule("delete from own team", "no", "calendars", "no rights", {
      router
        .request
        .delete_("/calendars/" ~ publicCalendarId)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
        });
    });


    describeCredentialsRule("delete from other teams", "yes", "calendars", "administrator", (string type) {
      router
        .request
        .delete_("/calendars/" ~ calendarOtherTeamId)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204);
    });

    describeCredentialsRule("delete from other teams", "no", "calendars", ["owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .delete_("/calendars/" ~ calendarOtherTeamId)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .end((Response response) => () {
          response.bodyJson.should.equal((`{
            "errors": [{
              "description": "Item ` ~ "`000000000000000000000003`" ~ ` not found.",
              "status": 404,
              "title": "Crate not found"
            }]
          }`).parseJsonString);
        });
    });

    describeCredentialsRule("delete from other teams", "no", "calendars", "no rights", {
      router
        .request
        .delete_("/calendars/" ~ calendarOtherTeamId)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
        });
    });

    it("deletes the events of a calendar when the record can be deleted", {
      crates.event.addItem(`{
        "name": "test 1",
        "location": { "type": "Text", "value": "" },
        "calendar": "000000000000000000000001",
        "visibility": { "isDefault": false, "isPublic": false, "team": "000000000000000000000001" },
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);
      crates.event.addItem(`{
        "name": "test 2",
        "location": { "type": "Text", "value": "" },
        "calendar": "000000000000000000000001",
        "visibility": { "isDefault": false, "isPublic": false, "team": "000000000000000000000001" },
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);
      crates.event.addItem(`{
        "name": "test 3",
        "location": { "type": "Text", "value": "" },
        "calendar": "000000000000000000000002",
        "visibility": { "isDefault": false, "isPublic": false, "team": "000000000000000000000002" },
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);

      router
        .request
        .delete_("/calendars/" ~ publicCalendarId)
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(204)
        .end((Response response) => () {
          crates.event.get.and.exec.array.serializeToJson.should.equal(`[ {
            "_id": "000000000000000000000003",
            "calendar": "000000000000000000000002",
            "info": { "author": "", "changeIndex": 0, "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "originalAuthor": "" },
            "location": { "type": "Text", "value": "" },
            "name": "test 3",
            "visibility": {
              "isDefault": false,
              "isPublic": false,
              "team": "000000000000000000000002"
            }
          }]`.parseJsonString);
        });
    });

    it("does not delete the events of a calendar when the record can not be deleted", {
      crates.event.addItem(`{
        "name": "test 1",
        "location": { "type": "Text", "value": "" },
        "calendar": "000000000000000000000001",
        "visibility": { "isDefault": false, "isPublic": false, "team": "000000000000000000000001" },
        "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
      }`.parseJsonString);

      router
        .request
        .delete_("/calendars/" ~ publicCalendarId)
        .header("Authorization", "Bearer " ~ userTokenList["guest"])
        .expectStatusCode(403)
        .end((Response response) => () {
          crates.event.get.and.exec.array.length.should.equal(1);
        });
    });
  });
});
