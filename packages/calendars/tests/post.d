/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.calendars.post;

import tests.fixtures;
import vibe.http.common;
import fluent.asserts;
import gis_collective.hmq.broadcast.memory;
import std.algorithm;
import ogm.defaults.teams;
import ogm.defaults.articles;
import ogm.calendars.api;
import ogm.calendar;

alias suite = Spec!({
  URLRouter router;

  Json calendar;

  describe("creating calendars", {
    beforeEach({
      setupTestData();
      auto broadcast = new MemoryBroadcast;
      router = new URLRouter;
      router.crateSetup.setupCalendarApi(crates, broadcast);

      crates.setupDefaultTeams;
      crates.setupDefaultArticles;

      calendar = `{ "calendar": {
        "name": "test",
        "article": "some description",
        "iconSets": { "useCustomList": false, "list": [] },
        "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" },
        "type": "events",
        "map": {
          "isEnabled": false,
          "map": ""
        }
      }}`.parseJsonString;
    });

    describe("when the team has allowEvents=true", {
      beforeEach({
        auto team = crates.team.getItem("000000000000000000000001").and.exec.front;
        team["allowEvents"] = true;

        crates.team.updateItem(team);
      });

      describeCredentialsRule("create a calendar when the team has allowEvents=true", "yes", "calendars", ["administrator", "owner"], (string type) {
        router
          .request
          .post("/calendars")
          .send(calendar)
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["calendar"]["_id"].to!string.should.equal("000000000000000000000005");
          });
      });

      describeCredentialsRule("create a calendar when the team has allowEvents=true", "no", "calendars", ["leader", "member", "guest"], (string type) {
        router
          .request
          .post("/calendars")
          .send(calendar)
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"errors":[{"description":"You don't have enough rights to add an item.","status":403,"title":"Forbidden"}]}`.parseJsonString);
          });
      });

      describeCredentialsRule("create a calendar when the team has allowEvents=true", "no", "calendars", "no rights", {
        router
          .request
          .post("/calendars")
          .send(calendar)
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
          });
      });
    });

    describe("when the team has allowEvents=false", {
      beforeEach({
        auto team = crates.team.getItem("000000000000000000000001").and.exec.front;
        team["allowEvents"] = false;

        crates.team.updateItem(team);
      });

      describeCredentialsRule("create a calendar when the team has allowEvents=false", "yes", "calendars", "administrator", (string type) {
        router
          .request
          .post("/calendars")
          .send(calendar)
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["calendar"]["_id"].to!string.should.equal("000000000000000000000005");
          });
      });

      describeCredentialsRule("create a calendar when the team has allowEvents=false", "no", "calendars", ["owner", "leader", "member", "guest"], (string type) {
        router
          .request
          .post("/calendars")
          .send(calendar)
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"errors":[{"description":"You can't create this calendar for this team.","status":403,"title":"Forbidden"}]}`.parseJsonString);
          });
      });

      describeCredentialsRule("create a calendar when the team has allowEvents=false", "no", "calendars", "no rights", {
        router
          .request
          .post("/calendars")
          .send(calendar)
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
          });
      });
    });
  });
});
