/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.CalendarAttributesOperation;

import tests.fixtures;
import vibe.http.common;
import fluent.asserts;
import std.algorithm;
import ogm.defaults.teams;
import ogm.defaults.articles;
import ogm.calendars.api;
import ogm.calendars.operations.CalendarAttributesOperation;

alias suite = Spec!({
  URLRouter router;
  Json eventData;
  Json feature;
  CalendarAttributesOperation middleware;

  describe("Event Attributes Middleware", {
    beforeEach({
      setupTestData();

      auto calendar = crates.calendar.getItem("000000000000000000000001").and.exec.front;

      calendar["map"] = `{
        "map": "000000000000000000000001",
        "isEnabled": true
      }`.parseJsonString;
      crates.calendar.updateItem(calendar);

      feature = crates.feature.getItem("000000000000000000000001").and.exec.front;

      middleware = new CalendarAttributesOperation(crates);
      eventData = `{
        "name": "new name",
        "article": "some description",
        "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" },
        "cover": "000000000000000000000001",
        "entries": [],
        "calendar": "000000000000000000000001",
        "location": {
          "type": "Feature",
          "value": "000000000000000000000001"
        }
      }`.parseJsonString;

      eventData = crates.event.addItem(eventData);
    });

    it("get a list of values for one attribute that belongs to a feature", {
      feature["attributes"] = `{ "icon": { "some": "value" } }`.parseJsonString;
      crates.feature.updateItem(feature);

      auto values = middleware.getAttributeList("000000000000000000000001", "icon.some", true, "", 0);

      expect(values).to.equal(["value"]);
    });

    it("get a list of values for two attributes that belongs to two features", {
      feature["attributes"] = `{ "icon": { "some": "value1" } }`.parseJsonString;
      crates.feature.updateItem(feature.clone);

      feature["attributes"] = `{ "icon": { "some": "value2" } }`.parseJsonString;
      feature.remove("_id");
      feature = crates.feature.addItem(feature);

      auto values = middleware.getAttributeList("000000000000000000000001", "icon.some", true, "", 0);

      expect(values).to.equal(["value1", "value2"]);
    });

    it("get a list of public values when withPrivateData is false", {
      feature["attributes"] = `{ "icon": { "some": "value1" } }`.parseJsonString;
      crates.feature.updateItem(feature.clone);

      feature["attributes"] = `{ "icon": { "some": "value2" } }`.parseJsonString;
      feature["computedVisibility"]["isPublic"] = false;
      feature.remove("_id");
      feature = crates.feature.addItem(feature);

      auto values = middleware.getAttributeList("000000000000000000000001", "icon.some", false, "", 0);

      expect(values).to.equal(["value1"]);
    });

    it("filters values by a term", {
      feature["attributes"] = `{ "icon": { "some": "abc" } }`.parseJsonString;
      crates.feature.updateItem(feature.clone);

      feature["attributes"] = `{ "icon": { "some": "def" } }`.parseJsonString;
      feature.remove("_id");
      feature = crates.feature.addItem(feature);

      auto values = middleware.getAttributeList("000000000000000000000001", "icon.some", true, "BC", 0);

      expect(values).to.equal(["abc"]);
    });

    it("can limit the result count", {
      feature["attributes"] = `{ "icon": { "some": "value1" } }`.parseJsonString;
      crates.feature.updateItem(feature.clone);

      feature["attributes"] = `{ "icon": { "some": "value2" } }`.parseJsonString;
      feature.remove("_id");
      feature = crates.feature.addItem(feature);

      auto values = middleware.getAttributeList("000000000000000000000001", "icon.some", true, "", 1);

      expect(values).to.equal(["value1"]);
    });

    it("get a list of values for one attribute that belongs to an event", {
      eventData["attributes"] = `{ "icon": { "some": "value" } }`.parseJsonString;
      crates.event.updateItem(eventData);

      auto values = middleware.getAttributeList("000000000000000000000001", "icon.some", true, "", 0);

      expect(values).to.equal(["value"]);
    });
  });
});
