/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.calendars.get;

import tests.fixtures;
import vibe.http.common;
import fluent.asserts;
import gis_collective.hmq.broadcast.memory;
import std.algorithm;
import ogm.defaults.articles;
import ogm.defaults.teams;
import ogm.calendars.api;

alias suite = Spec!({
  URLRouter router;
  Json publicEvent;
  string publicCalendarId;
  string privateCalendarId;
  string privateCalendarOtherTeamId;

  describe("getting calendars", {
    beforeEach({
      setupTestData();
      auto broadcast = new MemoryBroadcast;
      router = new URLRouter;
      router.crateSetup.setupCalendarApi(crates, broadcast);

      crates.setupDefaultTeams;
      crates.setupDefaultArticles;

      publicCalendarId = `000000000000000000000001`;
      privateCalendarId = `000000000000000000000002`;
      privateCalendarOtherTeamId = `000000000000000000000004`;
    });

    describeCredentialsRule("get a public calendar", "yes", "calendars", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/calendars/" ~ publicCalendarId)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["calendar"]["_id"].to!string.should.equal(publicCalendarId);
        });
    });

    describeCredentialsRule("get a public calendar", "yes", "calendars", "no rights", {
      router
        .request
        .get("/calendars/" ~ publicCalendarId)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["calendar"]["_id"].to!string.should.equal(publicCalendarId);
        });
    });


    describeCredentialsRule("get a private calendar from own team", "yes", "calendars", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/calendars/" ~ privateCalendarId)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["calendar"]["_id"].to!string.should.equal(privateCalendarId);
        });
    });

    describeCredentialsRule("get a private calendar from own team", "no", "calendars", "no rights", {
      router
        .request
        .get("/calendars/" ~ privateCalendarId)
        .expectStatusCode(404)
        .end;
    });


    describeCredentialsRule("get a private calendar from other teams", "yes", "calendars", "administrator", (string type) {
      router
        .request
        .get("/calendars/" ~ privateCalendarOtherTeamId)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["calendar"]["_id"].to!string.should.equal(privateCalendarOtherTeamId);
        });
    });

    describeCredentialsRule("get a private calendar from other teams", "no", "calendars", ["owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/calendars/" ~ privateCalendarOtherTeamId)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(404)
        .end;
    });

    describeCredentialsRule("get a private calendar from other teams", "no", "calendars", "no rights", {
      router
        .request
        .get("/calendars/" ~ privateCalendarOtherTeamId)
        .expectStatusCode(404)
        .end;
    });

    describeCredentialsRule("filter calendars by team", "yes", "calendars", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/calendars?team=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["calendars"].byValue.map!(a => a["_id"].to!string).array.should.equal([publicCalendarId]);
        });
    });

    describeCredentialsRule("filter calendars by team", "no", "calendars", "no rights", {
      router
        .request
        .get("/calendars?team=000000000000000000000001")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["calendars"].byValue.map!(a => a["_id"].to!string).array.should.equal([publicCalendarId]);
        });
    });


    describe("the attributes filter", {
      beforeEach({
        publicEvent = `{
          "name": "new name",
          "article": "some description",
          "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" },
          "cover": "000000000000000000000001",
          "entries": [],
          "calendar": "000000000000000000000001"
        }`.parseJsonString;

        publicEvent = crates.event.addItem(publicEvent);
      });

      it("returns the list of attributes to a team member", {
        auto event = crates.event.getItem(publicEvent["_id"].to!string).and.exec.front;

        event["attributes"] = `{ "some": "value" }`.parseJsonString;
        event["visibility"]["isPublic"] = false;

        crates.event.updateItem(event);

        router
          .request
          .get("/calendars/000000000000000000000001/attributes?&attribute=some")
          .header("Authorization", "Bearer " ~ userTokenList["owner"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`["value"]`.parseJsonString);
          });
      });

      it("returns the list of attributes to an admin", {
        auto event = crates.event.getItem(publicEvent["_id"].to!string).and.exec.front;

        event["attributes"] = `{ "some": "value" }`.parseJsonString;
        event["visibility"]["isPublic"] = false;

        crates.event.updateItem(event);

        router
          .request
          .get("/calendars/000000000000000000000001/attributes?&attribute=some")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`["value"]`.parseJsonString);
          });
      });

      it("returns an empty list of attributes when they are private to and there is no user", {
        auto event = crates.event.getItem(publicEvent["_id"].to!string).and.exec.front;

        event["attributes"] = `{ "some": "value" }`.parseJsonString;
        event["visibility"]["isPublic"] = false;

        crates.event.updateItem(event);

        router
          .request
          .get("/calendars/000000000000000000000001/attributes?&attribute=some")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`[]`.parseJsonString);
          });
      });
    });
  });
});
