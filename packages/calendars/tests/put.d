/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.calendars.put;

import tests.fixtures;
import vibe.http.common;
import fluent.asserts;
import gis_collective.hmq.broadcast.memory;
import std.algorithm;
import ogm.defaults.teams;
import ogm.defaults.articles;
import ogm.calendars.api;
import ogm.calendar;

alias suite = Spec!({
  URLRouter router;
  Json publicEvent;
  string publicCalendarId;

  Json privateEvent;
  string privateEventId;

  Json calendarOtherTeam;
  string calendarOtherTeamId;

  describe("updating calendars", {
    beforeEach({
      setupTestData();
      auto broadcast = new MemoryBroadcast;
      router = new URLRouter;

      crates.setupDefaultTeams;
      crates.setupDefaultArticles;

      router.crateSetup.setupCalendarApi(crates, broadcast);

      publicCalendarId = "000000000000000000000001";
      calendarOtherTeamId = "000000000000000000000003";

      SysCalendar.instance = new CalendarMock("2018-02-01T09:30:10Z");
    });

    describeCredentialsRule("update team calendar", "yes", "calendars", ["administrator", "owner"], (string type) {
      auto calendarData = `{
        "calendar": {
          "name": "test",
          "article": "some description",
          "iconSets": { "useCustomList": false, "list": [] },
          "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" },
          "type": "events",
          "map": {
            "isEnabled": false,
            "map": ""
          }
        }
      }`.parseJsonString;

      router
        .request
        .put("/calendars/" ~ publicCalendarId)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(calendarData)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["calendar"]["_id"].to!string.should.equal("000000000000000000000001");
        });
    });

    describeCredentialsRule("update team calendar", "no", "calendars", ["leader", "member", "guest"], (string type) {
      auto calendarData = `{
        "calendar": {
          "name": "new name",
          "location": { "type": "Text", "value": "" },
          "article": "some description",
          "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" },
          "cover": "000000000000000000000001",
          "entries": [],
          "calendar": "000000000000000000000001"
        }
      }`.parseJsonString;

      router
        .request
        .put("/calendars/" ~ publicCalendarId)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(calendarData)
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{
            "errors": [{
              "description": "You don't have enough rights to edit ` ~ "`000000000000000000000001`" ~ `.",
              "status": 403,
              "title": "Forbidden"
            }]
          }`).parseJsonString);
        });
    });

    describeCredentialsRule("update team calendar", "no", "calendars", "no rights", {
      auto calendarData = `{
        "calendar": {
          "name": "new name",
          "location": { "type": "Text", "value": "" },
          "article": "some description",
          "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" },
          "cover": "000000000000000000000001",
          "entries": [],
          "calendar": "000000000000000000000001"
        }
      }`.parseJsonString;

      router
        .request
        .put("/calendars/" ~ publicCalendarId)
        .send(calendarData)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
        });
    });


    describeCredentialsRule("update other team calendar", "yes", "calendars", ["administrator"], (string type) {
      auto calendarData = `{
        "calendar": {
          "name": "test",
          "article": "some description",
          "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000003" },
          "iconSets": { "useCustomList": false, "list": [] },
          "type": "events",
          "map": {
            "isEnabled": false,
            "map": ""
          }
        }
      }`.parseJsonString;

      router
        .request
        .put("/calendars/" ~ calendarOtherTeamId)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(calendarData)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["calendar"]["_id"].to!string.should.equal("000000000000000000000003");
        });
    });

    describeCredentialsRule("update other team calendar", "no", "calendars", ["owner", "leader", "member", "guest"], (string type) {
      auto calendarData = `{
        "calendar": {
           "name": "test",
            "article": "some description",
            "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000003" },
            "type": "events",
            "map": {
              "isEnabled": false,
              "map": ""
            }
        }
      }`.parseJsonString;

      router
        .request
        .put("/calendars/" ~ calendarOtherTeamId)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(calendarData)
        .expectStatusCode(404)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{
            "errors": [{
              "description": "Item ` ~ "`000000000000000000000003`" ~ ` not found.",
              "status": 404,
              "title": "Crate not found"
            }]
          }`).parseJsonString);
        });
    });

    describeCredentialsRule("update other team calendar", "no", "calendars", "no rights", {
      auto calendarData = `{
        "calendar": {
          "name": "new name",
          "location": { "type": "Text", "value": "" },
          "article": "some description",
          "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000004" },
          "cover": "000000000000000000000001",
          "entries": []
        }
      }`.parseJsonString;

      router
        .request
        .put("/calendars/" ~ calendarOtherTeamId)
        .send(calendarData)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
        });
    });
  });
});
