module ogm.calendars.operations.CalendarAttributesOperation;

import crate.base;
import crate.error;
import std.algorithm;
import std.array;
import std.uni;
import std.string;
import std.range;
import ogm.calendar;
import ogm.crates.all;
import ogm.http.request;
import vibe.data.json;
import vibe.http.router;
import std.conv;

import crate.collection.EmptyQuery;
import crate.http.operations.base;
import crate.http.operations.getItem;

class CalendarAttributesOperation : GetItemApiOperation!DefaultStorage {
  protected {
    OgmCrates crates;
  }

  this(OgmCrates crates) {
    this.crates = crates;

    CrateRule rule;

    rule.request.path = "/calendars/:id/attributes";
    rule.request.method = HTTPMethod.GET;
    rule.response.statusCode = 200;
    rule.response.mime = "application/json";
    rule.response.description = "Returns a list of strings.";

    super(crates.calendar, rule);
  }

  string[] getAttributeList(string calendarId, string attribute, bool withPrivateData, string term, int limit) {
    string[] result;

    auto calendar = crates.calendar.getItem(calendarId).and.exec.front;

    string featureAttribute = "attributes." ~ attribute;

    IQuery featuresQuery;

    if(calendar["map"]["isEnabled"]) {
      featuresQuery = this.crates.feature.get
        .where("maps").arrayContains(ObjectId(calendar["map"]["map"])).and;
    } else {
      featuresQuery = EmptyQuery.instance;
    }

    auto eventsQuery = this.crates.event.get
      .where("calendar").equal(ObjectId(calendarId)).and;

    if(!withPrivateData) {
      featuresQuery.where("computedVisibility.isPublic").equal(true);
      eventsQuery.where("visibility.isPublic").equal(true);
    }

    auto featuresRange = featuresQuery
      .withProjection([featureAttribute])
      .distinct(featureAttribute);

    auto eventsRange = eventsQuery
      .withProjection([featureAttribute])
      .distinct(featureAttribute);

    auto values = featuresRange.chain(eventsRange)
      .filter!(a => a.type != Json.Type.null_ && a.type != Json.Type.undefined)
      .map!(a => a.to!string)
      .filter!(a => a.strip != "")
      .array;

    if(term) {
      auto lcTerm = term.toLower;
      values = values.filter!(a => a.toLower.canFind(lcTerm)).array;
    }

    if(limit && values.length > limit) {
      values = values[0..limit];
    }

    return values;
  }

  override void handle(DefaultStorage storage) {
    auto item = this.prepareItemOperation!"getItem"(storage);

    if(storage.response.headerWritten) {
      return;
    }

    this.applyRule(storage);

    auto req = storage.request;
    auto request = RequestUserData(req);

    auto calendars = request.session(crates).all.calendars;

    string attribute = req.query["attribute"];
    string term;
    int limit;

    if("term" in req.query) {
      term = req.query["term"];
    }

    if("limit" in req.query) {
      limit = req.query["limit"].to!int;
    }

    auto items = this.getAttributeList(storage.properties.itemId, attribute, request.isAdmin || calendars.canFind(storage.properties.itemId), term, limit);

    storage.response.writeJsonBody(items, rule.response.statusCode, rule.response.mime);
  }
}