module ogm.calendars.middlewares.CalendarEventsMiddleware;

import crate.base;
import crate.error;
import std.algorithm;
import std.array;
import ogm.calendar;
import ogm.crates.all;
import ogm.http.request;
import vibe.data.json;
import vibe.http.router;

class CalendarEventsMiddleware {
  OgmCrates crates;

  this(OgmCrates crates) {
    this.crates = crates;
  }

  @delete_
  IQuery deleteEvents(IQuery selector, HTTPServerRequest req) {
    auto calendarId = ObjectId(req.params["id"]);

    auto eventIds = crates.event.get.where("calendar").equal(calendarId).and.exec.map!`a["_id"].get!string`;

    foreach (id; eventIds) {
      crates.event.deleteItem(id);
    }

    return selector;
  }
}
