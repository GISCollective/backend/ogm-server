/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.calendars.api;

import vibe.http.router;
import vibe.core.log;
import vibe.data.json;
import vibe.service.configuration.general;

import crate.http.router;
import crate.base;

import ogm.auth;
import ogm.crates.all;

import ogm.middleware.adminrequest;
import ogm.middleware.userdata;
import ogm.middleware.modelinfo;
import ogm.middleware.creationRightsFlag;
import ogm.filter.visibility;
import gis_collective.hmq.broadcast.base;
import ogm.middleware.DefaultCover;
import ogm.calendars.operations.CalendarAttributesOperation;
import ogm.calendars.middlewares.CalendarEventsMiddleware;

///
void setupCalendarApi(T)(CrateRouter!T crateRouter, OgmCrates crates, IBroadcast broadcast) {
  crateGetters["Team"] = &crates.team.getItem;
  crateGetters["Picture"] = &crates.picture.getItem;
  crateGetters["Icon"] = &crates.icon.getItem;
  crateGetters["IconSet"] = &crates.iconSet.getItem;

  auto auth = Authentication.instance(crates.user);
  auto adminRequest = new AdminRequest(crates.user);

  auto userDataMiddleware = UserDataMiddleware.instance(crates);
  auto modelInfo = new ModelInfoMiddleware("calendar", crates.calendar);
  auto visibilityFilter = new VisibilityFilter!"calendars"(crates, crates.calendar);
  auto creationRightsFlag = new CreationRightsFlag!"calendar"(crates, "allowEvents");
  auto defaultCover = new DefaultCoverMiddleware(crates, "calendar");
  auto calendarEvents = new CalendarEventsMiddleware(crates);
  auto attributesOperation = new CalendarAttributesOperation(crates);

  auto preparedRoute = crateRouter
    .prepare(crates.calendar)
    .withCustomOperation(attributesOperation)
    .and(auth.publicDataMiddleware)
    .and(adminRequest)
    .and(userDataMiddleware)
    .and(creationRightsFlag)
    .and(modelInfo)
    .and(visibilityFilter)
    .and(calendarEvents)
    .and(defaultCover);
}