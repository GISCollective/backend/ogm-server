/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.articleLinks.put;

import tests.fixtures;
import vibe.http.common;
import ogm.articleLinks.api;
import ogm.defaults.articles;

alias suite = Spec!({
  URLRouter router;

  describe("Updating article links", {
    Json articleLink = Json.emptyObject;

    before({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupArticleLinksApi(crates);

      crates.articleLink.addItem(`{
        "url": "",
        "article": ""
      }`.parseJsonString);
    });

    describeCredentialsRule("update an article link", "no", "article links", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .put("/articlelinks/000000000000000000000001")
        .send(articleLink)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "errors": [{
              "description": "You can't replace the 'articleLink'.",
              "status": 403,
              "title": "Forbidden"
            }]
          }`.parseJsonString);
        });
    });

    describeCredentialsRule("update an article link", "no", "article links", "no rights", {
      router
        .request
        .put("/articlelinks/000000000000000000000001")
        .send(articleLink)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "error": "Authorization required"
          }`.parseJsonString);
        });
    });
  });
});
