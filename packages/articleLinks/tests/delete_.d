/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.articleLinks.delete_;

import tests.fixtures;
import vibe.http.common;
import ogm.articleLinks.api;
import ogm.defaults.articles;

alias suite = Spec!({
  URLRouter router;

  describe("Updating article links", {
    before({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupArticleLinksApi(crates);

      crates.articleLink.addItem(`{
        "url": "",
        "article": ""
      }`.parseJsonString);
    });

    describeCredentialsRule("delete an article link", "no", "article links", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .delete_("/articlelinks/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "errors": [{
              "description": "You can't delete the 'articleLink'.",
              "status": 403,
              "title": "Forbidden"
            }]
          }`.parseJsonString);
        });
    });

    describeCredentialsRule("delete an article link", "no", "article links", "no rights", {
      router
        .request
        .delete_("/articlelinks/000000000000000000000001")
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "error": "Authorization required"
          }`.parseJsonString);
        });
    });
  });
});
