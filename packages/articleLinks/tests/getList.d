/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.articleLinks.getList;

import tests.fixtures;
import vibe.http.common;
import ogm.articleLinks.api;
import ogm.defaults.articles;

alias suite = Spec!({
  URLRouter router;

  describe("Get article link list", {
    before({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupArticleLinksApi(crates);
    });

    describeCredentialsRule("get the article links list", "no", "article links", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/articlelinks")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "errors": [{
              "description": "You can't get the 'articleLink' list.",
              "status": 403,
              "title": "Forbidden"
            }]
          }`.parseJsonString);
        });
    });

    describeCredentialsRule("get the article links list", "no", "article links", "no rights", {
      router
        .request
        .get("/articlelinks")
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "errors": [{
              "description": "You can't get the 'articleLink' list.",
              "status": 403,
              "title": "Forbidden"
            }]
          }`.parseJsonString);
        });
    });
  });
});
