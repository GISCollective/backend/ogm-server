/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.articleLinks.post;

import tests.fixtures;
import vibe.http.common;
import ogm.articleLinks.api;
import ogm.defaults.articles;

alias suite = Spec!({
  URLRouter router;

  describe("Creating article links", {
    Json articleLink = Json.emptyObject;

    before({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupArticleLinksApi(crates);
    });

    describeCredentialsRule("create an article link", "no", "article links", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .post("/articlelinks")
        .send(articleLink)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "errors": [{
              "description": "You can't create this 'articleLink'.",
              "status": 403,
              "title": "Forbidden"
            }]
          }`.parseJsonString);
        });
    });

    describeCredentialsRule("create an article link", "no", "article links", "no rights", {
      router
        .request
        .post("/articlelinks")
        .send(articleLink)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "error": "Authorization required"
          }`.parseJsonString);
        });
    });
  });
});
