/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.articleLinks.get;

import tests.fixtures;
import vibe.http.common;
import std.array;
import ogm.calendar;
import ogm.defaults.teams;
import ogm.defaults.articles;
import ogm.articleLinks.api;

alias suite = Spec!({
  URLRouter router;

  describe("Updating article links", {
    before({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupArticleLinksApi(crates);

      auto calendarMock = new CalendarMock("2023-01-12T16:20:11Z");
      SysCalendar.instance = calendarMock;

      crates.setupDefaultTeams;
      crates.setupDefaultArticles;

      auto article = crates.article.getItem("000000000000000000000001").exec.front.clone;
      article["visibility"]["isPublic"] = true;
      article["visibility"]["team"] = "000000000000000000000001";
      article["relatedId"] = "00000000000000000000000a";
      article["type"] = "newsletter-article";

      crates.article.updateItem(article);

      crates.articleLink.addItem(`{
        "url": "",
        "articleId": "000000000000000000000001"
      }`.parseJsonString);
    });

    it("creates a articleInteraction metric when a link is requested", {
      router
        .request
        .get("/articlelinks/000000000000000000000001")
        .expectStatusCode(200)
        .end((Response response) => () {
          auto metrics = crates.metric.get.exec.array;

          metrics.length.should.equal(1);
          metrics[0].should.equal(`{
            "_id": "000000000000000000000001",
            "labels": {
              "relatedId": "00000000000000000000000a"
            },
            "name": "000000000000000000000001",
            "reporter": "",
            "time": "2023-01-12T16:20:11Z",
            "type": "articleInteraction",
            "value": 1
          }`.parseJsonString);
        });
    });

    describeCredentialsRule("get an article link", "yes", "article links", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/articlelinks/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "articleLink": {
              "_id": "000000000000000000000001",
              "articleId": "000000000000000000000001",
              "url": "",
              "canEdit": false
            }
          }`.parseJsonString);
        });
    });

    describeCredentialsRule("get an article link", "yes", "article links", "no rights", {
      router
        .request
        .get("/articlelinks/000000000000000000000001")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "articleLink": {
              "_id": "000000000000000000000001",
              "articleId": "000000000000000000000001",
              "url": "",
              "canEdit": false
            }
          }`.parseJsonString);
        });
    });
  });
});
