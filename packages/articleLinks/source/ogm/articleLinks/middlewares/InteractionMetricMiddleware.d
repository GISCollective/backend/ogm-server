module ogm.articleLinks.middlewares.InteractionMetricMiddleware;

import crate.base;
import crate.error;
import std.algorithm;
import std.array;
import ogm.crates.all;
import vibe.data.json;
import vibe.http.router;
import ogm.http.request;
import ogm.calendar;
import gis_collective.hmq.log;

class InteractionMetricMiddleware {
  OgmCrates crates;

  this(OgmCrates crates) {
    this.crates = crates;
  }

  ///
  @getItem
  void createMetric(HTTPServerRequest req) {
    auto request = RequestUserData(req);

    auto link = crates.articleLink.getItem(request.itemId).and.exec.front;

    try {
      auto article = crates.article.getItem(link["articleId"].to!string).and.exec.front;

      auto metric = Metric();
      metric.type = "articleInteraction";
      metric.name = link["articleId"].to!string;
      metric.value = 1;
      metric.time = SysCalendar.instance.now;
      metric.labels["relatedId"] = article["relatedId"].to!string;

      crates.metric.addItem(metric.serializeToJson);
    } catch(Exception err) {
      error(err);
    }
  }
}