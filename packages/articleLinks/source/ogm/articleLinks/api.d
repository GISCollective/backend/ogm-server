/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.articleLinks.api;

import vibe.http.router;
import vibe.core.log;
import vibe.data.json;

import crate.http.router;
import crate.base;

import ogm.auth;
import ogm.crates.all;

import ogm.filter.visibility;
import ogm.middleware.adminrequest;
import ogm.middleware.StaticDbContent;
import ogm.middleware.userdata;
import ogm.articleLinks.middlewares.InteractionMetricMiddleware;

///
void setupArticleLinksApi(T)(CrateRouter!T crateRouter, OgmCrates crates) {
  auto staticDbContent = new StaticDbContent("articleLink");
  auto auth = Authentication.instance(crates.user);
  auto adminRequest = new AdminRequest(crates.user);
  auto userDataMiddleware = UserDataMiddleware.instance(crates);
  auto interactionMetric = new InteractionMetricMiddleware(crates);

  crateGetters["Article"] = &crates.article.getItem;

  auto preparedRoute = crateRouter.prepare(crates.articleLink)
    .and(auth.publicDataMiddleware)
    .and(userDataMiddleware)
    .and(staticDbContent)
    .and(interactionMetric);
}
