/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.campaignAnswers.getList;

import tests.fixtures;
import ogm.campaignAnswers.api;

import std.algorithm;
import std.array;
import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({
  URLRouter router;

  describe("GET list", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupCampaignAnswerApi(crates, new MemoryBroadcast);

      createCampaignAnswer(CampaignAnswer(ObjectId.fromString("000000000000000000000001")));
    });

    describeCredentialsRule("get campaign answers", "no", "survey answers", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/campaignanswers")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(400)
        .end((Response response) => () {
            response.bodyJson.should.equal((`{"errors": [{
              "description": "The campaign answers can not be accessed without query params.",
              "status": 400,
              "title": "Validation error"
            }]}`).parseJsonString);
        });
    });

    describeCredentialsRule("get campaign answers", "no", "survey answers", "no rights", {
      router
        .request
        .get("/campaignanswers")
        .expectStatusCode(400)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{"errors": [{
            "description": "The campaign answers can not be accessed.",
            "status": 400,
            "title": "Validation error"
          }]}`).parseJsonString);
        });
    });


    describeCredentialsRule("get own campaign answers", "yes", "survey answers", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      auto info = ModelInfo();
      info.originalAuthor = userId[type];

      createCampaignAnswer(CampaignAnswer(ObjectId.fromString("000000000000000000000002"), info));

      router
        .request
        .get("/campaignanswers?author=" ~ userId[type])
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["campaignAnswers"].length.should.equal(1);
        });
    });

    describeCredentialsRule("get own campaign answers", "-", "survey answers", "no rights", { });


    describeCredentialsRule("get other users campaign answers", "yes", "survey answers", ["administrator"], (string type) {
      auto info = ModelInfo();
      info.originalAuthor = "00000000000000000000006";

      auto result = createCampaignAnswer(CampaignAnswer(ObjectId.fromString("000000000000000000000002"), info));

      router
        .request
        .get("/campaignanswers?author=00000000000000000000006")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["campaignAnswers"].length.should.equal(1);
          response.bodyJson["campaignAnswers"][0]["canEdit"].to!bool.should.equal(true);
        });
    });

    describeCredentialsRule("get other users campaign answers", "no", "survey answers", ["owner", "leader", "member", "guest"], (string type) {
      auto info = ModelInfo();
      info.originalAuthor = "000000000000000000000006";

      createCampaignAnswer(CampaignAnswer(ObjectId.fromString("000000000000000000000002"), info));

      router
        .request
        .get("/campaignanswers?author=000000000000000000000006")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(400)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{"errors": [{
            "description": "You can't query answers for the user '000000000000000000000006'.",
            "status": 400,
            "title": "Validation error"
          }]}`).parseJsonString);
        });
    });

    describeCredentialsRule("get other users campaign answers", "-", "survey answers", "no rights", { });


    describeCredentialsRule("get own campaign answers grouped by campaign", "yes", "survey answers", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      auto info = ModelInfo();
      info.originalAuthor = userId[type];

      createCampaignAnswer(CampaignAnswer(ObjectId.fromString("000000000000000000000002"), info));
      createCampaignAnswer(CampaignAnswer(ObjectId.fromString("000000000000000000000003"), info, Campaign(ObjectId.fromString("000000000000000000000001"))));

      router
        .request
        .get("/campaignanswers?author=" ~ userId[type] ~ "&campaign=000000000000000000000001" )
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["campaignAnswers"][0]["canEdit"].to!bool.should.equal(true);
          response.bodyJson["campaignAnswers"].length.should.equal(1);
        });
    });

    describeCredentialsRule("get own campaign answers grouped by campaign", "-", "survey answers", "no rights", { });
  });
});
