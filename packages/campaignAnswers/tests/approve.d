/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.campaignAnswers.approve;

import tests.fixtures;
import ogm.campaignAnswers.api;

import std.algorithm;
import std.array;
import gis_collective.hmq.broadcast.memory;
import ogm.calendar;

alias suite = Spec!({
  URLRouter router;
  Json message;

  describe("approve", {
    beforeEach({
      SysCalendar.instance = new CalendarMock("2023-12-11T12:22:22Z");

      setupTestData();
      router = new URLRouter;
      auto broadcast = new MemoryBroadcast;
      router.crateSetup.setupCampaignAnswerApi(crates, broadcast);

      auto map = crates.map.getItem("000000000000000000000002").and.exec.front;
      map["visibility"]["isPublic"] = true;
      crates.map.updateItem(map);

      auto campaign = Campaign(ObjectId.fromString("000000000000000000000001"));
      campaign.visibility.team = Team(ObjectId.fromString("000000000000000000000001"));

      auto answer = CampaignAnswer(ObjectId.fromString("000000000000000000000001"));
      answer.campaign = campaign;

      answer.featureId = "000000000000000000000002";
      answer.position = `{ "type": "Point", "coordinates": [1,2] }`.parseJsonString.deserializeJson!GeoJsonGeometry;

      createCampaign(campaign);
      createCampaignAnswer(answer);

      message = Json();

      void messageHandler(const Json value) {
        message = value;
      }

      void messageHandler2(const Json value) {}

      broadcast.register("CampaignAnswer.approve", &messageHandler);
      broadcast.register("Feature.change", &messageHandler2);

      createCampaignAnswer(CampaignAnswer(ObjectId.fromString("000000000000000000000002")));
    });

    describeCredentialsRule("approve answers", "yes", "survey answers", ["administrator", "owner", "leader", "member"], (string type) {
      router
        .request
        .post("/campaignanswers/000000000000000000000001/approve")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
            response.bodyJson.should.equal((`{
              "success": true
            }`).parseJsonString);

            auto answer = crates.campaignAnswer.getItem("000000000000000000000001").and.exec.front;

            answer["status"].should.equal(Json(cast(int) CampaignAnswerStatus.published));
            answer["review"]["reviewedOn"].should.equal("2023-12-11T12:22:22Z");
            answer["review"]["user"].should.equal(userId[type]);
        });
    });

    describeCredentialsRule("approve answers", "no", "survey answers", ["guest"], (string type) {
      router
        .request
        .post("/campaignanswers/000000000000000000000001/approve")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
            response.bodyJson.should.equal(("{
              \"errors\": [{
                \"description\": \"The campaign answer with id `000000000000000000000001` can not be approved by you.\",
                \"status\": 403,
                \"title\": \"Forbidden\"
              }]
            }").parseJsonString);
        });
    });

    describeCredentialsRule("approve answers", "no", "survey answers", "no rights", {
      router
        .request
        .post("/campaignanswers/000000000000000000000001/approve")
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{"errors": [{
            "description": "The campaign answer with id ` ~ "`000000000000000000000001`" ~ ` can not be accessed by id.",
            "status": 403,
            "title": "Forbidden"
          }]}`).parseJsonString);

          message.type.should.equal(Json.Type.undefined);
        });
    });

    describe("when there is a linked feature", {
      Json feature;
      Json answer;

      beforeEach({
        feature = crates.feature.getItem("000000000000000000000002").and.exec.front;
        feature["source"] = `{"type":"Campaign", "remoteId": "000000000000000000000001"}`.parseJsonString;
        crates.feature.updateItem(feature);

        answer = crates.campaignAnswer.getItem("000000000000000000000001").and.exec.front;
        answer["featureId"] = "000000000000000000000002";
        crates.campaignAnswer.updateItem(answer);
      });

      it("concatenates the pictures when they are set", {
        auto feature = crates.feature.getItem("000000000000000000000002").and.exec.front;
        feature["pictures"] = ["000000000000000000000001"].serializeToJson;
        crates.feature.updateItem(feature);

        auto answer = crates.campaignAnswer.getItem("000000000000000000000001").and.exec.front;
        answer["pictures"] = ["000000000000000000000002"].serializeToJson;
        answer["featureId"] = "000000000000000000000002";
        crates.campaignAnswer.updateItem(answer);

        router
          .request
          .post("/campaignanswers/000000000000000000000001/approve")
          .header("Authorization", "Bearer " ~ userTokenList["owner"])
          .expectStatusCode(200)
          .end((Response response) => () {
            auto feature = crates.feature.getItem("000000000000000000000002").and.exec.front;

            expect(feature["pictures"]).to.equal(`["000000000000000000000001", "000000000000000000000002"]`.parseJsonString);
          });
      });

      it("does not add duplicated pictures", {
        auto feature = crates.feature.getItem("000000000000000000000002").and.exec.front;
        feature["pictures"] = ["000000000000000000000001"].serializeToJson;
        crates.feature.updateItem(feature);

        auto answer = crates.campaignAnswer.getItem("000000000000000000000001").and.exec.front;
        answer["pictures"] = ["000000000000000000000001"].serializeToJson;
        answer["featureId"] = "000000000000000000000002";
        crates.campaignAnswer.updateItem(answer);

        router
          .request
          .post("/campaignanswers/000000000000000000000001/approve")
          .header("Authorization", "Bearer " ~ userTokenList["owner"])
          .expectStatusCode(200)
          .end((Response response) => () {
            auto feature = crates.feature.getItem("000000000000000000000002").and.exec.front;

            expect(feature["pictures"]).to.equal(`["000000000000000000000001"]`.parseJsonString);
          });
      });

      it("triggers a message", {
        router
          .request
          .post("/campaignanswers/000000000000000000000001/approve")
          .header("Authorization", "Bearer " ~ userTokenList["owner"])
          .expectStatusCode(200)
          .end((Response response) => () {
            message.should.equal(`{ "id": "000000000000000000000001" }`.parseJsonString);
          });
      });

      describe("history", {
        it("creates an history record of the feature", {
          auto before = crates.feature.getItem("000000000000000000000002").and.exec.front.clone;

          router
            .request
            .post("/campaignanswers/000000000000000000000001/approve")
            .header("Authorization", "Bearer " ~ userTokenList["owner"])
            .expectStatusCode(200)
            .end((Response response) => () {
              auto change = crates.changeSet.get
                .where("reference").equal("CampaignAnswer:000000000000000000000001")
                .and.exec.front;

              auto expectedChange = `{
                "_id": "000000000000000000000001",
                "time": "2023-12-11T12:22:22Z",
                "type": "snapshot",
                "author": "000000000000000000000004",
                "itemId": "000000000000000000000002",
                "model": "Feature",
                "reference": "CampaignAnswer:000000000000000000000001",
              }`.parseJsonString;
              expectedChange["removed"] = before;
              expectedChange["added"] = crates.feature.getItem("000000000000000000000002").and.exec.front;

              expect(change).to.equal(expectedChange);
            });
        });

        it("removes existing snapshots", {
          auto before = crates.feature.getItem("000000000000000000000002").and.exec.front.clone;

          auto change = Json.emptyObject;
          change["model"] = "Feature";
          change["itemId"] = "000000000000000000000002";
          change["type"] = "snapshot";
          change["author"] = "userId";
          change["reference"] = "CampaignAnswer:000000000000000000000001";

          crates.changeSet.addItem(change);

          router
            .request
            .post("/campaignanswers/000000000000000000000001/approve")
            .header("Authorization", "Bearer " ~ userTokenList["owner"])
            .expectStatusCode(200)
            .end((Response response) => () {
              auto change = crates.changeSet.get
                .where("reference").equal("CampaignAnswer:000000000000000000000001")
                .and.exec.map!(a => a["_id"]).array;

              expect(change).to.equal(["000000000000000000000002"]);
            });
        });
      });

      describe("metadata", {
        it("updates the info object", {
          crates.campaignAnswer.updateItem(answer);

          router
            .request
            .post("/campaignanswers/000000000000000000000001/approve")
            .header("Authorization", "Bearer " ~ userTokenList["owner"])
            .expectStatusCode(200)
            .end((Response response) => () {
              auto feature = crates.feature.getItem("000000000000000000000002").and.exec.front;

              expect(feature["info"]).to.equal(`{
                "changeIndex": 1,
                "createdOn": "2015-01-01T00:00:00Z",
                "lastChangeOn": "2023-12-11T12:22:22Z",
                "originalAuthor": "",
                "author": "000000000000000000000004"
              }`.parseJsonString);
            });
        });

        it("updates the source object", {
          crates.campaignAnswer.updateItem(answer);

          router
            .request
            .post("/campaignanswers/000000000000000000000001/approve")
            .header("Authorization", "Bearer " ~ userTokenList["owner"])
            .expectStatusCode(200)
            .end((Response response) => () {
              auto feature = crates.feature.getItem("000000000000000000000002").and.exec.front;

              expect(feature["source"]).to.equal(`{
                "modelId": "000000000000000000000001",
                "remoteId": "000000000000000000000001",
                "syncAt": "2023-12-11T12:22:22Z",
                "type": "Campaign"
              }`.parseJsonString);
            });
        });

        it("adds the review object to the answer", {
          router
            .request
            .post("/campaignanswers/000000000000000000000001/approve")
            .header("Authorization", "Bearer " ~ userTokenList["owner"])
            .expectStatusCode(200)
            .end((Response response) => () {
              auto answer = crates.campaignAnswer.getItem("000000000000000000000001").and.exec.front;

              expect(answer["review"]).to.equal(`{
                "reviewedOn": "2023-12-11T12:22:22Z",
                "user": "000000000000000000000004"
              }`.parseJsonString);
            });
        });
      });

      describe("attributes", {
        it("updates the name and description when they are set", {
          auto answer = crates.campaignAnswer.getItem("000000000000000000000001").and.exec.front;
          answer["featureId"] = "000000000000000000000002";
          answer["attributes"]["about"] = `{
            "name": "some name",
            "description": "some description"
          }`.parseJsonString;

          crates.campaignAnswer.updateItem(answer);

          router
            .request
            .post("/campaignanswers/000000000000000000000001/approve")
            .header("Authorization", "Bearer " ~ userTokenList["owner"])
            .expectStatusCode(200)
            .end((Response response) => () {
              auto feature = crates.feature.getItem("000000000000000000000002").and.exec.front;

              expect(feature["name"]).to.equal("some name");
              expect(feature["description"]).to.equal("some description");
            });
        });

        it("updates the attributes when they are set", {
          auto answer = crates.campaignAnswer.getItem("000000000000000000000001").and.exec.front;
          answer["featureId"] = "000000000000000000000002";
          answer["attributes"]["group"] = `{
            "value": 1
          }`.parseJsonString;

          crates.campaignAnswer.updateItem(answer);

          router
            .request
            .post("/campaignanswers/000000000000000000000001/approve")
            .header("Authorization", "Bearer " ~ userTokenList["owner"])
            .expectStatusCode(200)
            .end((Response response) => () {
              auto feature = crates.feature.getItem("000000000000000000000002").and.exec.front;

              expect(feature["attributes"]).to.equal(`{
                "group": {
                  "value": 1
                }
              }`.parseJsonString);
            });
        });

        it("concatenates a feature list attribute with a list answer attribute", {
          auto feature = crates.feature.getItem("000000000000000000000002").and.exec.front;
          feature["attributes"]["group"] = `[{
            "value": 1
          }]`.parseJsonString;
          crates.feature.updateItem(feature);

          auto answer = crates.campaignAnswer.getItem("000000000000000000000001").and.exec.front;
          answer["featureId"] = "000000000000000000000002";
          answer["attributes"]["group"] = `[{
            "value": 2
          }]`.parseJsonString;

          crates.campaignAnswer.updateItem(answer);

          router
            .request
            .post("/campaignanswers/000000000000000000000001/approve")
            .header("Authorization", "Bearer " ~ userTokenList["owner"])
            .expectStatusCode(200)
            .end((Response response) => () {
              auto feature = crates.feature.getItem("000000000000000000000002").and.exec.front;

              expect(feature["attributes"]).to.equal(`{
                "group": [{ "value": 1 }, { "value": 2 }]
              }`.parseJsonString);
            });
        });

        it("replace a feature object attribute with a list answer attribute", {
          auto feature = crates.feature.getItem("000000000000000000000002").and.exec.front;
          feature["attributes"]["group"] = `{
            "value": 1
          }`.parseJsonString;
          crates.feature.updateItem(feature);

          auto answer = crates.campaignAnswer.getItem("000000000000000000000001").and.exec.front;
          answer["featureId"] = "000000000000000000000002";
          answer["attributes"]["group"] = `[{
            "value": 2
          }]`.parseJsonString;

          crates.campaignAnswer.updateItem(answer);

          router
            .request
            .post("/campaignanswers/000000000000000000000001/approve")
            .header("Authorization", "Bearer " ~ userTokenList["owner"])
            .expectStatusCode(200)
            .end((Response response) => () {
              auto feature = crates.feature.getItem("000000000000000000000002").and.exec.front;

              expect(feature["attributes"]).to.equal(`{
                "group": [{ "value": 2 }]
              }`.parseJsonString);
            });
        });

        it("concatenates a feature array attribute with an object answer attribute", {
          auto feature = crates.feature.getItem("000000000000000000000002").and.exec.front;
          feature["attributes"]["group"] = `[]`.parseJsonString;
          crates.feature.updateItem(feature);

          auto answer = crates.campaignAnswer.getItem("000000000000000000000001").and.exec.front;
          answer["featureId"] = "000000000000000000000002";
          answer["attributes"]["group"] = `{
            "value": 2
          }`.parseJsonString;

          crates.campaignAnswer.updateItem(answer);

          router
            .request
            .post("/campaignanswers/000000000000000000000001/approve")
            .header("Authorization", "Bearer " ~ userTokenList["owner"])
            .expectStatusCode(200)
            .end((Response response) => () {
              auto feature = crates.feature.getItem("000000000000000000000002").and.exec.front;

              expect(feature["attributes"]).to.equal(`{
                "group": [{ "value": 2 }]
              }`.parseJsonString);
            });
        });
      });

      describe("visibility", {
        it("sets the related feature to public when it is pending", {
          auto feature = crates.feature.getItem("000000000000000000000002").and.exec.front;
          feature["visibility"] = -1;
          crates.feature.updateItem(feature);

          router
            .request
            .post("/campaignanswers/000000000000000000000001/approve")
            .header("Authorization", "Bearer " ~ userTokenList["owner"])
            .expectStatusCode(200)
            .end((Response response) => () {
              auto feature = crates.feature.getItem("000000000000000000000002").and.exec.front;

              expect(feature["visibility"]).to.equal(Json(1));
            });
        });

        it("sets the related feature to public when it is private", {
          auto feature = crates.feature.getItem("000000000000000000000002").and.exec.front;
          feature["visibility"] = 0;
          crates.feature.updateItem(feature);

          router
            .request
            .post("/campaignanswers/000000000000000000000001/approve")
            .header("Authorization", "Bearer " ~ userTokenList["owner"])
            .expectStatusCode(200)
            .end((Response response) => () {
              auto feature = crates.feature.getItem("000000000000000000000002").and.exec.front;

              expect(feature["visibility"]).to.equal(Json(1));
            });
        });

        it("keeps the related feature public when it is public", {
          auto feature = crates.feature.getItem("000000000000000000000002").and.exec.front;
          feature["visibility"] = 1;
          crates.feature.updateItem(feature);

          router
            .request
            .post("/campaignanswers/000000000000000000000001/approve")
            .header("Authorization", "Bearer " ~ userTokenList["owner"])
            .expectStatusCode(200)
            .end((Response response) => () {
              auto feature = crates.feature.getItem("000000000000000000000002").and.exec.front;

              expect(feature["visibility"]).to.equal(Json(1));
            });
        });

        it("updates the computed visibility", {
          auto feature = crates.feature.getItem("000000000000000000000002").and.exec.front;
          feature["visibility"] = 1;
          feature["computedVisibility"] = Json.emptyObject;
          crates.feature.updateItem(feature);

          router
            .request
            .post("/campaignanswers/000000000000000000000001/approve")
            .header("Authorization", "Bearer " ~ userTokenList["owner"])
            .expectStatusCode(200)
            .end((Response response) => () {
              auto feature = crates.feature.getItem("000000000000000000000002").and.exec.front;

              expect(feature["computedVisibility"]).to.equal(`{
                "isDefault": false,
                "isPublic": true,
                "team": "000000000000000000000002"
              }`.parseJsonString);
            });
        });
      });
    });

    describe("when there is no linked feature", {
      Json feature;
      Json answer;

      beforeEach({
        feature = crates.feature.getItem("000000000000000000000002").and.exec.front;
        feature["source"] = Json.emptyObject;
        crates.feature.updateItem(feature);

        answer = crates.campaignAnswer.getItem("000000000000000000000001").and.exec.front;
        answer["featureId"] = Json();
        crates.campaignAnswer.updateItem(answer);
      });

      it("triggers a message", {
        router
          .request
          .post("/campaignanswers/000000000000000000000001/approve")
          .header("Authorization", "Bearer " ~ userTokenList["owner"])
          .expectStatusCode(200)
          .end((Response response) => () {
            message.should.equal(`{ "id": "000000000000000000000001" }`.parseJsonString);
          });
      });

      it("adds the pictures when they are set", {
        auto answer = crates.campaignAnswer.getItem("000000000000000000000001").and.exec.front;
        answer["pictures"] = ["000000000000000000000002"].serializeToJson;
        answer["featureId"] = "000000000000000000000002";
        crates.campaignAnswer.updateItem(answer);

        router
          .request
          .post("/campaignanswers/000000000000000000000001/approve")
          .header("Authorization", "Bearer " ~ userTokenList["owner"])
          .expectStatusCode(200)
          .end((Response response) => () {
            auto answer = crates.campaignAnswer.getItem("000000000000000000000001").and.exec.front;
            auto feature = crates.feature.getItem(answer["featureId"].to!string).and.exec.front;

            expect(feature["pictures"]).to.equal(`["000000000000000000000002"]`.parseJsonString);
          });
      });
    });
  });
});
