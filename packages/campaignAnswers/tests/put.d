/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.campaignAnswers.put;

import tests.fixtures;
import ogm.campaignAnswers.api;

import std.algorithm;
import std.array;
import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({
  URLRouter router;
  Json data;
  Json campaignAnswer;

  describe("PUT", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupCampaignAnswerApi(crates, new MemoryBroadcast);

      auto storedAnswer = CampaignAnswer(ObjectId.fromString("000000000000000000000001"));
      storedAnswer.status = CampaignAnswerStatus.pending;
      storedAnswer.campaign = Campaign(ObjectId.fromString("000000000000000000000001"));

      campaignAnswer = createCampaignAnswer(storedAnswer);

      data = `{}`.parseJsonString;
      data["campaignAnswer"] = campaignAnswer.clone;
      data["campaignAnswer"]["featureId"] = "000000000000000000000001";
    });

    describeCredentialsRule("update answers", "yes", "survey answers", ["administrator", "owner", "leader", "member"], (string type) {
      router
        .request
        .put("/campaignanswers/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          data["campaignAnswer"]["canEdit"] = true;
          data["campaignAnswer"]["canReview"] = true;
          response.bodyJson.should.equal(data);
        });
    });

    describeCredentialsRule("update answers", "no", "survey answers", ["guest"], (string type) {
      router
        .request
        .put("/campaignanswers/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(data)
        .expectStatusCode(400)
        .end((Response response) => () {
            response.bodyJson.should.equal((`{"errors": [{
              "description": "The campaign answer with id ` ~ "`000000000000000000000001`" ~ ` can not be changed.",
              "status": 400,
              "title": "Validation error"
            }]}`).parseJsonString);
        });
    });

    describeCredentialsRule("update answers", "no", "survey answers", "no rights", {
      router
        .request
        .put("/campaignanswers/000000000000000000000001")
        .send(data)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "error": "Authorization required"
          }`.parseJsonString);
        });
    });

    it("can not change the campaign", {
      data["campaignAnswer"]["campaign"] = "000000000000000000000002";

      router
        .request
        .put("/campaignanswers/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .send(data)
        .expectStatusCode(400)
        .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "errors":[{"description":"You can not change the campaign.","status":400,"title":"Validation error"}]
            }`.parseJsonString);
        });
    });

    it("can not change the status", {
      data["campaignAnswer"]["status"] = 4;

      router
        .request
        .put("/campaignanswers/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .send(data)
        .expectStatusCode(400)
        .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "errors":[{"description":"You can not change the status.","status":400,"title":"Validation error"}]
            }`.parseJsonString);
        });
    });

    it("can not change the review", {
      data["campaignAnswer"]["review"] = Json.emptyObject;

      router
        .request
        .put("/campaignanswers/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .send(data)
        .expectStatusCode(400)
        .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "errors":[{"description":"You can not change the review.","status":400,"title":"Validation error"}]
            }`.parseJsonString);
        });
    });

    describe("the user that created an answer", {
      beforeEach({
        campaignAnswer["info"]["originalAuthor"] = userId["other"];
        crates.campaignAnswer.updateItem(campaignAnswer);
      });

      it("can update own campaign answers", {
        router
          .request
          .put("/campaignanswers/000000000000000000000001")
          .header("Authorization", "Bearer " ~ otherToken.name)
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            data["campaignAnswer"]["canEdit"] = true;
            data["campaignAnswer"]["canReview"] = false;

            response.bodyJson.should.equal(data);
          });
      });

      it("can update own campaign answers when the status is not set", {
        data["campaignAnswer"].remove("status");

        router
          .request
          .put("/campaignanswers/000000000000000000000001")
          .header("Authorization", "Bearer " ~ otherToken.name)
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            data["campaignAnswer"]["status"] = campaignAnswer["status"];
            data["campaignAnswer"]["canEdit"] = true;
            data["campaignAnswer"]["canReview"] = false;

            response.bodyJson.should.equal(data);
          });
      });

      it("can update own campaign answers when the review is not set", {
        data["campaignAnswer"].remove("review");

        router
          .request
          .put("/campaignanswers/000000000000000000000001")
          .header("Authorization", "Bearer " ~ otherToken.name)
          .send(data)
          .expectStatusCode(200)
          .end((Response response) => () {
            data["campaignAnswer"]["review"] = campaignAnswer["review"];
            data["campaignAnswer"]["canEdit"] = true;
            data["campaignAnswer"]["canReview"] = false;

            response.bodyJson.should.equal(data);
          });
      });
    });
  });
});
