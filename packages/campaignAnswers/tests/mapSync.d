/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.campaignAnswers.mapSync;

import tests.fixtures;
import ogm.campaignAnswers.api;

import std.algorithm;
import std.array;
import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({
  URLRouter router;
  Json message;

  describe("mapSync", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      auto broadcast = new MemoryBroadcast;
      router.crateSetup.setupCampaignAnswerApi(crates, broadcast);


      auto campaign = Campaign(ObjectId.fromString("000000000000000000000001"));
      campaign.visibility.team = Team(ObjectId.fromString("000000000000000000000001"));


      auto answer = CampaignAnswer(ObjectId.fromString("000000000000000000000001"));
      answer.campaign = campaign;

      createCampaign(campaign);
      createCampaignAnswer(answer);

      message = Json();

      void messageHandler(const Json value) {
        message = value;
      }

      broadcast.register("CampaignAnswer.MapSync", &messageHandler);

      createCampaignAnswer(CampaignAnswer(ObjectId.fromString("000000000000000000000002")));
    });

    describeCredentialsRule("manually trigger a mapSync when the team owns the campaign", "yes", "survey answers", ["administrator", "owner", "leader", "member"], (string type) {
      router
        .request
        .get("/campaignanswers/000000000000000000000001/mapsync")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
            response.bodyJson.should.equal((`{
              "success": true
            }`).parseJsonString);

            message.should.equal(`{ "id": "000000000000000000000001" }`.parseJsonString);
        });
    });


    describeCredentialsRule("manually trigger a mapSync when the team owns the campaign", "no", "survey answers", ["guest"], (string type) {
      router
        .request
        .get("/campaignanswers/000000000000000000000001/mapsync")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(400)
        .end((Response response) => () {
            response.bodyJson.should.equal(("{
              \"errors\": [{
                \"description\": \"The campaign answer with id `000000000000000000000001` can not be accessed by id.\",
                \"status\": 400,
                \"title\": \"Validation error\"
              }]}").parseJsonString);
        });
    });

    describeCredentialsRule("manually trigger a mapSync when the team owns the campaign", "no", "survey answers", "no rights", {
      router
        .request
        .get("/campaignanswers/000000000000000000000001/mapsync")
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{"errors": [{
            "description": "The campaign answer with id ` ~ "`000000000000000000000001`" ~ ` can not be accessed by id.",
            "status": 403,
            "title": "Forbidden"
          }]}`).parseJsonString);

          message.type.should.equal(Json.Type.undefined);
        });
    });


    describeCredentialsRule("manually trigger a mapSync when the user is the original author", "yes", "survey answers", ["administrator"], (string type) {
      auto campaignAnswer = crates.campaignAnswer.getItem("000000000000000000000002").and.exec.front;
      campaignAnswer["info"]["originalAuthor"] = userId[type];
      crates.campaignAnswer.updateItem(campaignAnswer);

      router
        .request
        .get("/campaignanswers/000000000000000000000002/mapsync")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{
            "success": true
          }`).parseJsonString);

          message.should.equal(`{ "id": "000000000000000000000002" }`.parseJsonString);
        });
    });

    describeCredentialsRule("manually trigger a mapSync when the user is the original author", "no", "survey answers", ["owner", "leader", "member", "guest"], (string type) {
      auto campaignAnswer = crates.campaignAnswer.getItem("000000000000000000000002").and.exec.front;
      campaignAnswer["info"]["originalAuthor"] = userId[type];
      crates.campaignAnswer.updateItem(campaignAnswer);

      router
        .request
        .get("/campaignanswers/000000000000000000000002/mapsync")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(400)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{"errors": [{
            "description": "The campaign answer with id ` ~ "`000000000000000000000002`" ~ ` can not be accessed by id.",
            "status": 400,
            "title": "Validation error"
          }]}`).parseJsonString);
        });
    });

    describeCredentialsRule("manually trigger a mapSync when the user is the original author", "no", "survey answers", "no rights", {

    });
  });
});
