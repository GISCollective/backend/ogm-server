/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.campaignAnswers.restore;

import tests.fixtures;
import ogm.campaignAnswers.api;

import std.algorithm;
import std.array;
import gis_collective.hmq.broadcast.memory;
import ogm.calendar;

alias suite = Spec!({
  URLRouter router;
  Json message;
  Json jsonAnswer;

  describe("restore", {
    beforeEach({
      SysCalendar.instance = new CalendarMock("2023-12-11T12:22:22Z");

      setupTestData();
      router = new URLRouter;
      auto broadcast = new MemoryBroadcast;
      router.crateSetup.setupCampaignAnswerApi(crates, broadcast);

      auto campaign = Campaign(ObjectId.fromString("000000000000000000000001"));
      campaign.visibility.team = Team(ObjectId.fromString("000000000000000000000001"));

      auto answer = CampaignAnswer(ObjectId.fromString("000000000000000000000001"));
      answer.status = CampaignAnswerStatus.published;
      answer.campaign = campaign;
      answer.featureId = "000000000000000000000001";

      createCampaign(campaign);
      jsonAnswer = createCampaignAnswer(answer);

      message = Json();

      void messageHandler(const Json value) {
        message = value;
      }

      broadcast.register("CampaignAnswer.restore", &messageHandler);

      createCampaignAnswer(CampaignAnswer(ObjectId.fromString("000000000000000000000002")));
    });

    describeCredentialsRule("restores answers", "yes", "survey answers", ["administrator", "owner", "leader", "member"], (string type) {
      router
        .request
        .post("/campaignanswers/000000000000000000000001/restore")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
            response.bodyJson.should.equal((`{
              "success": true
            }`).parseJsonString);

            auto answer = crates.campaignAnswer.getItem("000000000000000000000001").and.exec.front;
            answer["status"].should.equal(Json(cast(int) CampaignAnswerStatus.pending));
        });
    });

    describeCredentialsRule("restores answers", "no", "survey answers", ["guest"], (string type) {
      router
        .request
        .post("/campaignanswers/000000000000000000000001/restore")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
            response.bodyJson.should.equal(("{
              \"errors\": [{
                \"description\": \"The campaign answer with id `000000000000000000000001` can not be restored by you.\",
                \"status\": 403,
                \"title\": \"Forbidden\"
              }]
            }").parseJsonString);
        });
    });

    describeCredentialsRule("restores answers", "no", "survey answers", "no rights", {
      router
        .request
        .post("/campaignanswers/000000000000000000000001/restore")
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{"errors": [{
            "description": "The campaign answer with id ` ~ "`000000000000000000000001`" ~ ` can not be accessed by id.",
            "status": 403,
            "title": "Forbidden"
          }]}`).parseJsonString);

          message.type.should.equal(Json.Type.undefined);
        });
    });

    it("can't restore pending answers", {
      auto answer = crates.campaignAnswer.getItem("000000000000000000000001").and.exec.front;
      answer["status"] = 1;
      crates.campaignAnswer.updateItem(answer);

      router
        .request
        .post("/campaignanswers/000000000000000000000001/restore")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(403)
        .end((Response response) => () {
            response.bodyJson.should.equal(("{
              \"errors\": [{
                \"description\": \"The campaign answer with id `000000000000000000000001` can not be restored. Invalid status 1.\",
                \"status\": 403,
                \"title\": \"Forbidden\"
              }]
            }").parseJsonString);

            auto answer = crates.campaignAnswer.getItem("000000000000000000000001").and.exec.front;

            answer["status"].should.equal(Json(1));
        });
    });

    it("restores a snapshot when exists", {
      auto change = Json.emptyObject;
      change["removed"] = `{ "_id": "000000000000000000000001", "name": "restored item" }`.parseJsonString;
      change["model"] = "Feature";
      change["itemId"] = "000000000000000000000001";
      change["type"] = "snapshot";
      change["reference"] = "CampaignAnswer:" ~ jsonAnswer["_id"];
      crates.changeSet.addItem(change);

      router
        .request
        .post("/campaignanswers/000000000000000000000001/restore")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto feature = crates.feature.getItem("000000000000000000000001").and.exec.front;

          feature.should.equal(`{
            "_id": "000000000000000000000001", "name": "restored item"
          }`.parseJsonString);
        });
    });
  });
});
