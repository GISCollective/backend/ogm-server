/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.campaignAnswers.delete_;

import tests.fixtures;
import ogm.campaignAnswers.api;

import std.algorithm;
import std.array;

import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({
  URLRouter router;

  describe("DELETE", {
    beforeEach({
      setupTestData();
      router = new URLRouter;

      router.crateSetup.setupCampaignAnswerApi(crates, new MemoryBroadcast);

      auto answer = CampaignAnswer(ObjectId.fromString("000000000000000000000001"));
      answer.campaign._id = ObjectId("000000000000000000000001");

      createCampaignAnswer(answer);

      answer = CampaignAnswer(ObjectId.fromString("000000000000000000000002"));
      answer.campaign._id = ObjectId("000000000000000000000003");

      createCampaignAnswer(answer);
    });

    describeCredentialsRule("delete from own team", "yes", "survey answers", ["administrator", "owner", "leader", "member"], (string type) {
      router
        .request
        .delete_("/campaignanswers/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end((Response response) => () {
            response.bodyString.should.equal(``);
        });
    });

    describeCredentialsRule("delete from own team", "no", "survey answers", ["guest"], (string type) {
      router
        .request
        .delete_("/campaignanswers/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
            response.bodyJson.should.equal((`{"errors": [{
              "description": "The campaign answer with id ` ~ "`000000000000000000000001`" ~ ` can not be deleted.",
              "status": 403,
              "title": "Forbidden"
            }]}`).parseJsonString);
        });
    });

    describeCredentialsRule("delete from own team", "no", "survey answers", "no rights", {
      router
        .request
        .delete_("/campaignanswers/000000000000000000000001")
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "error": "Authorization required"
          }`.parseJsonString);
        });
    });


    describeCredentialsRule("delete from other teams", "yes", "survey answers", ["administrator"], (string type) {
      router
        .request
        .delete_("/campaignanswers/000000000000000000000002")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end((Response response) => () {
            response.bodyString.should.equal(``);
        });
    });

    describeCredentialsRule("delete from other teams", "no", "survey answers", ["owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .delete_("/campaignanswers/000000000000000000000002")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
            response.bodyJson.should.equal((`{"errors": [{
              "description": "The campaign answer with id ` ~ "`000000000000000000000002`" ~ ` can not be deleted.",
              "status": 403,
              "title": "Forbidden"
            }]}`).parseJsonString);
        });
    });

    describeCredentialsRule("delete from other teams", "no", "survey answers", "no rights", {
      router
        .request
        .delete_("/campaignanswers/000000000000000000000002")
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "error": "Authorization required"
          }`.parseJsonString);
        });
    });

    it("allows users to delete their answers when they are not part of the team", {
      auto answer = CampaignAnswer(ObjectId.fromString("000000000000000000000003"));
      answer.campaign._id = ObjectId("000000000000000000000003");
      answer.info.originalAuthor = userId["guest"];

      createCampaignAnswer(answer);

      router
        .request
        .delete_("/campaignanswers/000000000000000000000003")
        .header("Authorization", "Bearer " ~ userTokenList["guest"])
        .expectStatusCode(204)
        .end((Response response) => () {
            response.bodyString.should.equal(``);
        });
    });

    it("can delete all answers from a survey using the bulk delete operation as admin", {
      auto answer = CampaignAnswer(ObjectId.fromString("000000000000000000000003"));
      answer.campaign._id = ObjectId("000000000000000000000001");

      createCampaignAnswer(answer);

      router
        .request
        .delete_("/campaignanswers?campaign=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(204)
        .end((Response response) => () {
          response.bodyString.should.equal(``);
          crates.campaignAnswer.get.where("campaign").equal(ObjectId("000000000000000000000001")).and.exec.empty.should.equal(true);
        });
    });

    it("can delete all answers from a survey using the bulk delete operation as owner", {
      auto answer = CampaignAnswer(ObjectId.fromString("000000000000000000000003"));
      answer.campaign._id = ObjectId("000000000000000000000001");

      createCampaignAnswer(answer);

      router
        .request
        .delete_("/campaignanswers?campaign=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .expectStatusCode(204)
        .end((Response response) => () {
          response.bodyString.should.equal(``);
          crates.campaignAnswer.get.where("campaign").equal(ObjectId("000000000000000000000001")).and.exec.empty.should.equal(true);
        });
    });

    it("can delete all answers from a survey using the bulk delete operation as leader", {
      auto answer = CampaignAnswer(ObjectId.fromString("000000000000000000000003"));
      answer.campaign._id = ObjectId("000000000000000000000001");

      createCampaignAnswer(answer);

      router
        .request
        .delete_("/campaignanswers?campaign=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList["leader"])
        .expectStatusCode(204)
        .end((Response response) => () {
          response.bodyString.should.equal(``);
          crates.campaignAnswer.get.where("campaign").equal(ObjectId("000000000000000000000001")).and.exec.empty.should.equal(true);
        });
    });

    it("can not bulk delete an approved answer as owner", {
      auto answer = CampaignAnswer(ObjectId.fromString("000000000000000000000003"));
      answer.campaign._id = ObjectId("000000000000000000000001");
      answer.status = CampaignAnswerStatus.published;

      createCampaignAnswer(answer);

      router
        .request
        .delete_("/campaignanswers?campaign=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .expectStatusCode(204)
        .end((Response response) => () {
          response.bodyString.should.equal(``);
          crates.campaignAnswer.get.where("_id").equal(ObjectId("000000000000000000000003")).and.exec.empty.should.equal(false);
        });
    });

    it("can bulk delete an approved answer as admin", {
      auto answer = CampaignAnswer(ObjectId.fromString("000000000000000000000003"));
      answer.campaign._id = ObjectId("000000000000000000000001");
      answer.status = CampaignAnswerStatus.published;

      createCampaignAnswer(answer);

      router
        .request
        .delete_("/campaignanswers?campaign=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(204)
        .end((Response response) => () {
          response.bodyString.should.equal(``);
          crates.campaignAnswer.get.where("_id").equal(ObjectId("000000000000000000000003")).and.exec.empty.should.equal(true);
        });
    });

    it("deletes own answers from a survey using the bulk delete operation as guest", {
      auto answer = CampaignAnswer(ObjectId.fromString("000000000000000000000003"));
      answer.campaign._id = ObjectId("000000000000000000000001");
      answer.info.originalAuthor = userId["guest"];

      createCampaignAnswer(answer);

      router
        .request
        .delete_("/campaignanswers?campaign=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList["guest"])
        .expectStatusCode(204)
        .end((Response response) => () {
          response.bodyString.should.equal(``);
          crates.campaignAnswer.get.where("campaign").equal(ObjectId("000000000000000000000001")).and.size.should.equal(1);
        });
    });

    it("can not delete an approved answer as owner", {
      auto answer = CampaignAnswer(ObjectId.fromString("000000000000000000000003"));
      answer.campaign._id = ObjectId("000000000000000000000001");
      answer.status = CampaignAnswerStatus.published;

      createCampaignAnswer(answer);

      router
        .request
        .delete_("/campaignanswers/000000000000000000000003")
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{"errors":[{"description":"The campaign answer with id ` ~ "`000000000000000000000003`" ~ ` can not be deleted.","status":403,"title":"Forbidden"}]}`).parseJsonString);
          crates.campaignAnswer.get.where("_id").equal(ObjectId("000000000000000000000003")).and.exec.empty.should.equal(false);
        });
    });

    it("can delete an approved answer as admin", {
      auto answer = CampaignAnswer(ObjectId.fromString("000000000000000000000003"));
      answer.campaign._id = ObjectId("000000000000000000000001");
      answer.status = CampaignAnswerStatus.published;

      createCampaignAnswer(answer);

      router
        .request
        .delete_("/campaignanswers/000000000000000000000003")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(204)
        .end((Response response) => () {
          response.bodyString.should.equal(``);
          crates.campaignAnswer.get.where("_id").equal(ObjectId("000000000000000000000003")).and.exec.empty.should.equal(true);
        });
    });
  });
});
