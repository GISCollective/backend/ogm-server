/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.campaignsAnswer.post;

import tests.fixtures;
import ogm.campaignAnswers.api;

import std.algorithm;
import std.array;
import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({
  URLRouter router;
  string data;

  describe("POST", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupCampaignAnswerApi(crates, new MemoryBroadcast);

      data = `{ "campaignAnswer": {
        "campaign":"000000000000000000000001",
        "attributes": {}
      }}`;
    });

    describeCredentialsRule("add an answer to a public campaign", "yes", "survey answers", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .post("/campaignanswers")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(data.parseJsonString)
        .expectStatusCode(200)
        .end((Response response) => () {
          auto expected = `{
            "campaignAnswer": {
              "_id": "000000000000000000000001",
              "info": {
                "changeIndex": 0,
                "createdOn": "2021-01-27T17:19:00Z",
                "lastChangeOn": "2021-01-27T17:19:00Z",
                "originalAuthor": "bogdan@giscollective.com",
                "author": "bogdan@giscollective.com"
              },
              "attributes": {},
              "pictures": [],
              "sounds": [],
              "icons": [],
              "contributor": {},
              "review": {
                "reviewedOn": "0001-01-01T00:00:00+00:00",
                "user": ""
              },
              "campaign": "000000000000000000000001",
              "status": 0
            }
          }`.parseJsonString;

          expected["campaignAnswer"]["info"]["author"] = userId[type];
          expected["campaignAnswer"]["info"]["originalAuthor"] = userId[type];
          expected["campaignAnswer"]["info"]["createdOn"] = response.bodyJson["campaignAnswer"]["info"]["createdOn"];
          expected["campaignAnswer"]["info"]["lastChangeOn"] = response.bodyJson["campaignAnswer"]["info"]["lastChangeOn"];
          expected["campaignAnswer"]["canEdit"] = true;
          expected["campaignAnswer"]["canReview"] = type != "guest";

          response.bodyJson.should.equal(expected);
        });
    });

    describeCredentialsRule("add an answer to a public campaign", "yes", "survey answers", "no rights", {
      auto answer = data.parseJsonString;
      answer["campaignAnswer"]["info"] = Json.emptyObject;
      answer["campaignAnswer"]["info"]["author"] = "bogdan@giscollective.com";
      answer["campaignAnswer"]["info"]["originalAuthor"] = "bogdan@giscollective.com";

      router
        .request
        .post("/campaignanswers")
        .send(answer)
        .expectStatusCode(200)
        .end((Response response) => () {
          auto expected = `{
            "campaignAnswer": {
              "_id": "000000000000000000000001",
              "info": {
                "changeIndex": 0,
                "createdOn": "2021-01-27T17:19:00Z",
                "lastChangeOn": "2021-01-27T17:19:00Z",
                "originalAuthor": "bogdan@giscollective.com",
                "author": "bogdan@giscollective.com"
              },
              "attributes": {},
              "pictures": [],
              "sounds": [],
              "icons": [],
              "contributor": {},
              "review": {
                "reviewedOn": "0001-01-01T00:00:00+00:00",
                "user": ""
              },
              "campaign": "000000000000000000000001",
              "status": 0
            }
          }`.parseJsonString;

          expected["campaignAnswer"]["info"]["createdOn"] = response.bodyJson["campaignAnswer"]["info"]["createdOn"];
          expected["campaignAnswer"]["info"]["lastChangeOn"] = response.bodyJson["campaignAnswer"]["info"]["lastChangeOn"];
          expected["campaignAnswer"]["canEdit"] = false;
          expected["campaignAnswer"]["canReview"] = false;

          response.bodyJson.should.equal(expected);
        });
    });

    describeCredentialsRule("add an answer to a private campaign for your teams", "yes", "survey answers", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      auto answer = data.parseJsonString;
      answer["campaignAnswer"]["campaign"] = "000000000000000000000002";

      router
        .request
        .post("/campaignanswers")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(answer)
        .expectStatusCode(200)
        .end((Response response) => () {
          auto expected = `{
            "campaignAnswer": {
              "_id": "000000000000000000000001",
              "info": {
                "changeIndex": 0,
                "createdOn": "2021-01-27T17:19:00Z",
                "lastChangeOn": "2021-01-27T17:19:00Z",
                "originalAuthor": "bogdan@giscollective.com",
                "author": "bogdan@giscollective.com"
              },
              "attributes": {},
              "pictures": [],
              "sounds": [],
              "icons": [],
              "contributor": {},
              "campaign": "000000000000000000000002",
              "review": {
                "reviewedOn": "0001-01-01T00:00:00+00:00",
                "user": ""
              },
              "status": 0
            }
          }`.parseJsonString;

          expected["campaignAnswer"]["info"]["author"] = userId[type];
          expected["campaignAnswer"]["info"]["originalAuthor"] = userId[type];
          expected["campaignAnswer"]["info"]["createdOn"] = response.bodyJson["campaignAnswer"]["info"]["createdOn"];
          expected["campaignAnswer"]["info"]["lastChangeOn"] = response.bodyJson["campaignAnswer"]["info"]["lastChangeOn"];
          expected["campaignAnswer"]["canEdit"] = true;
          expected["campaignAnswer"]["canReview"] = type != "guest";

          response.bodyJson.should.equal(expected);
        });
    });

    describeCredentialsRule("add an answer to a private campaign for your teams", "-", "survey answers", "no rights", {
      auto answer = data.parseJsonString;
      answer["campaignAnswer"]["campaign"] = "000000000000000000000002";

      router
        .request
        .post("/campaignanswers")
        .send(answer)
        .expectStatusCode(404)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{
            "errors": [{
              "description": "Campaign with id ` ~ "`000000000000000000000002`" ~ ` not found.",
              "status": 404,
              "title": "Crate not found"
            }]
          }`).parseJsonString);
        });
    });

    describe("add an answer to a public campaign with required attributes", {
      beforeEach({
        auto icon = crates.icon.getItem("000000000000000000000001").exec.front;
        icon["attributes"][0]["isRequired"] = true;
        icon["attributes"][1]["isRequired"] = true;
        crates.icon.updateItem(icon);

        auto campaign = crates.campaign.getItem("000000000000000000000001").exec.front;
        campaign["icons"] = ["000000000000000000000001"].serializeToJson;
        crates.campaign.updateItem(campaign);
      });

      it("should not allow submitting a campaign response with missing required attributes", {
        auto answer = data.parseJsonString;
        answer["campaignAnswer"]["icons"] = ["000000000000000000000001"].serializeToJson;

        router
          .request
          .post("/campaignanswers")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .send(answer)
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"errors": [{
                "description": "The item can not be added. Required icon attribute phone is not set for name1.",
                "status": 400,
                "title": "Validation error"
              }]}`.parseJsonString);
          });
      });
    });

    describe("using sounds", {
      it("updates the sound visibility on submit", {
        createSound(Sound(ObjectId.fromString("1"), "name1", VisibilityOptional(), ModelInfo(), new SoundFile));

        auto answer = data.parseJsonString;
        answer["campaignAnswer"]["sounds"] = ["000000000000000000000001"].serializeToJson;

        router
          .request
          .post("/campaignanswers")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .send(answer)
          .expectStatusCode(200)
          .end((Response response) => () {
            auto sound = crates.sound.getItem("000000000000000000000001").and.exec.front;

            sound["visibility"].should.equal(`{
              "isDefault":false,"isPublic":false,"team":"000000000000000000000001"
            }`.parseJsonString);
          });
      });
    });
  });
});
