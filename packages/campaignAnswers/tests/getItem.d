/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.campaignAnswers.getItem;

import tests.fixtures;
import ogm.campaignAnswers.api;

import std.algorithm;
import std.array;
import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({
  URLRouter router;

  describe("GET item", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupCampaignAnswerApi(crates, new MemoryBroadcast);

      createCampaignAnswer(CampaignAnswer(ObjectId.fromString("000000000000000000000001")));

      auto campaign = Campaign(ObjectId.fromString("000000000000000000000001"));
      campaign.visibility.team = Team(ObjectId.fromString("000000000000000000000001"));

      auto answer = CampaignAnswer(ObjectId.fromString("000000000000000000000002"));
      answer.campaign = campaign;

      createCampaign(campaign);
      createCampaignAnswer(answer);
    });

    describeCredentialsRule("get any by id", "yes", "survey answers", ["administrator"], (string type) {
      router
        .request
        .get("/campaignanswers/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["campaignAnswer"]["canEdit"].to!bool.should.equal(true);
          response.bodyJson["campaignAnswer"]["_id"].should.equal(`000000000000000000000001`);
        });
    });

    describeCredentialsRule("get any by id", "no", "survey answers", ["owner", "leader", "member", "guest", "no rights"], {
      router
        .request
        .get("/campaignanswers/000000000000000000000001")
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{"errors": [{
            "description": "The campaign answer with id ` ~ "`000000000000000000000001`" ~ ` can not be accessed by id.",
            "status": 403,
            "title": "Forbidden"
          }]}`).parseJsonString);
        });
    });

    describeCredentialsRule("get by id when the user is the original author", "yes", "survey answers", ["owner", "leader", "member", "guest", "administrator"], (string type) {
      auto campaignAnswer = crates.campaignAnswer.getItem("000000000000000000000001").and.exec.front;
      campaignAnswer["info"]["originalAuthor"] = userId[type];
      crates.campaignAnswer.updateItem(campaignAnswer);

      router
        .request
        .get("/campaignanswers/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
            response.bodyJson["campaignAnswer"]["_id"].should.equal(`000000000000000000000001`);
        });
    });

    describeCredentialsRule("get by id when the user is the original author", "no", "survey answers", ["no rights"], {
      router
        .request
        .get("/campaignanswers/000000000000000000000001")
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{"errors": [{
            "description": "The campaign answer with id ` ~ "`000000000000000000000001`" ~ ` can not be accessed by id.",
            "status": 403,
            "title": "Forbidden"
          }]}`).parseJsonString);
        });
    });

    describeCredentialsRule("get by id when the team owns the campaign", "yes", "survey answers", ["owner", "leader", "member", "guest", "administrator"], (string type) {
      router
        .request
        .get("/campaignanswers/000000000000000000000002")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["campaignAnswer"]["_id"].should.equal(`000000000000000000000002`);
        });
    });

    describeCredentialsRule("get by id when the team owns the campaign", "no", "survey answers", ["no rights"], {
      router
        .request
        .get("/campaignanswers/000000000000000000000002")
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{"errors": [{
            "description": "The campaign answer with id ` ~ "`000000000000000000000002`" ~ ` can not be accessed by id.",
            "status": 403,
            "title": "Forbidden"
          }]}`).parseJsonString);
        });
    });
  });
});
