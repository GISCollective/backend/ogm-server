/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.campaignAnswers.patch;

import tests.fixtures;
import ogm.campaignAnswers.api;

import std.algorithm;
import std.array;
import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({
  URLRouter router;
  Json data;

  describe("PATCH", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupCampaignAnswerApi(crates, new MemoryBroadcast);

      createCampaignAnswer(CampaignAnswer(ObjectId.fromString("000000000000000000000001")));

      auto answer = crates.campaignAnswer.getItem("000000000000000000000001").exec.front;
      answer["campaign"] = "000000000000000000000001";
      crates.campaignAnswer.updateItem(answer);

      data = `{}`.parseJsonString;
      data["campaignAnswer"] = answer;
    });

    describeCredentialsRule("patch campaign answers", "yes", "survey answers", ["administrator", "owner", "leader", "member"], (string type) {
      router
        .request
        .patch("/campaignanswers/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(data)
        .expectStatusCode(200)
        .end;
    });

    describeCredentialsRule("patch campaign answers", "no", "survey answers", ["guest"], (string type) {
      router
        .request
        .patch("/campaignanswers/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(data)
        .expectStatusCode(400)
        .end((Response response) => () {
            response.bodyJson.should.equal((`{"errors": [{
              "description": "The campaign answer with id ` ~ "`000000000000000000000001`" ~ ` can not be changed.",
              "status": 400,
              "title": "Validation error"
            }]}`).parseJsonString);
        });
    });

    describeCredentialsRule("patch campaign answers", "no", "survey answers", "no rights", {
      router
        .request
        .patch("/campaignanswers/000000000000000000000001")
        .send(data)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "error": "Authorization required"
          }`.parseJsonString);
        });
    });
  });
});
