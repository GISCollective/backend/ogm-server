/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.campaignAnswers.reject;

import tests.fixtures;
import ogm.campaignAnswers.api;

import std.algorithm;
import std.array;
import gis_collective.hmq.broadcast.memory;
import ogm.calendar;

alias suite = Spec!({
  URLRouter router;
  Json message;

  describe("reject", {
    beforeEach({
      SysCalendar.instance = new CalendarMock("2023-12-11T12:22:22Z");

      setupTestData();
      router = new URLRouter;
      auto broadcast = new MemoryBroadcast;
      router.crateSetup.setupCampaignAnswerApi(crates, broadcast);

      auto campaign = Campaign(ObjectId.fromString("000000000000000000000001"));
      campaign.visibility.team = Team(ObjectId.fromString("000000000000000000000001"));

      auto answer = CampaignAnswer(ObjectId.fromString("000000000000000000000001"));
      answer.status = CampaignAnswerStatus.pending;
      answer.campaign = campaign;

      createCampaign(campaign);
      createCampaignAnswer(answer);

      message = Json();

      void messageHandler(const Json value) {
        message = value;
      }

      broadcast.register("CampaignAnswer.reject", &messageHandler);

      createCampaignAnswer(CampaignAnswer(ObjectId.fromString("000000000000000000000002")));
    });

    describeCredentialsRule("reject answers", "yes", "survey answers", ["administrator", "owner", "leader", "member"], (string type) {
      router
        .request
        .post("/campaignanswers/000000000000000000000001/reject")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
            response.bodyJson.should.equal((`{
              "success": true
            }`).parseJsonString);

            auto answer = crates.campaignAnswer.getItem("000000000000000000000001").and.exec.front;

            answer["status"].should.equal(Json(4));
            answer["review"]["reviewedOn"].should.equal("2023-12-11T12:22:22Z");
            answer["review"]["user"].should.equal(userId[type]);
        });
    });

    describeCredentialsRule("reject answers", "no", "survey answers", ["guest"], (string type) {
      router
        .request
        .post("/campaignanswers/000000000000000000000001/reject")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
            response.bodyJson.should.equal(("{
              \"errors\": [{
                \"description\": \"The campaign answer with id `000000000000000000000001` can not be rejected by you.\",
                \"status\": 403,
                \"title\": \"Forbidden\"
              }]
            }").parseJsonString);
        });
    });

    describeCredentialsRule("reject answers", "no", "survey answers", "no rights", {
      router
        .request
        .post("/campaignanswers/000000000000000000000001/reject")
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{"errors": [{
            "description": "The campaign answer with id ` ~ "`000000000000000000000001`" ~ ` can not be accessed by id.",
            "status": 403,
            "title": "Forbidden"
          }]}`).parseJsonString);

          message.type.should.equal(Json.Type.undefined);
        });
    });

    it("can't reject approved answers", {
      auto answer = crates.campaignAnswer.getItem("000000000000000000000001").and.exec.front;
      answer["status"] = 2;
      crates.campaignAnswer.updateItem(answer);

      router
        .request
        .post("/campaignanswers/000000000000000000000001/reject")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(403)
        .end((Response response) => () {
            response.bodyJson.should.equal(("{
              \"errors\": [{
                \"description\": \"The campaign answer with id `000000000000000000000001` can not be rejected. Invalid status 2 != 1\",
                \"status\": 403,
                \"title\": \"Forbidden\"
              }]
            }").parseJsonString);

            auto answer = crates.campaignAnswer.getItem("000000000000000000000001").and.exec.front;

            answer["status"].should.equal(Json(2));
        });
    });

    it("rejects an answer when it has no feature attached", {
      auto answer = crates.campaignAnswer.getItem("000000000000000000000001").and.exec.front;
      answer.remove("featureId");
      crates.campaignAnswer.updateItem(answer);

      router
        .request
        .post("/campaignanswers/000000000000000000000001/reject")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(200)
        .end((Response response) => () {
            response.bodyJson.should.equal((`{
              "success": true
            }`).parseJsonString);

            auto answer = crates.campaignAnswer.getItem("000000000000000000000001").and.exec.front;

            answer["status"].should.equal(Json(4));
            answer["review"]["reviewedOn"].should.equal("2023-12-11T12:22:22Z");
            answer["review"]["user"].should.equal(userId["administrator"]);

            message.should.equal(`{ "id": "000000000000000000000001" }`.parseJsonString);
        });
    });

    it("deletes the atached pending feature with changeIndex = 0", {
      auto answer = crates.campaignAnswer.getItem("000000000000000000000001").and.exec.front;
      answer["featureId"] = "000000000000000000000002";
      crates.campaignAnswer.updateItem(answer);

      auto feature = crates.feature.getItem("000000000000000000000002").and.exec.front;
      feature["visibility"] = -1;
      crates.feature.updateItem(feature);

      router
        .request
        .post("/campaignanswers/000000000000000000000001/reject")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(200)
        .end((Response response) => () {
            response.bodyJson.should.equal((`{
              "success": true
            }`).parseJsonString);

            auto range = crates.feature.get.where("_id").equal(ObjectId("000000000000000000000002")).and.exec;

            range.empty.should.equal(true);
        });
    });

    it("does not delete the atached pending feature with changeIndex = 1", {
      auto answer = crates.campaignAnswer.getItem("000000000000000000000001").and.exec.front;
      answer["featureId"] = "000000000000000000000002";
      crates.campaignAnswer.updateItem(answer);

      auto feature = crates.feature.getItem("000000000000000000000002").and.exec.front;
      feature["visibility"] = -1;
      feature["info"]["changeIndex"] = 1;
      crates.feature.updateItem(feature);

      router
        .request
        .post("/campaignanswers/000000000000000000000001/reject")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(200)
        .end((Response response) => () {
            response.bodyJson.should.equal((`{
              "success": true
            }`).parseJsonString);

            auto range = crates.feature.get.where("_id").equal(ObjectId("000000000000000000000002")).and.exec;

            range.empty.should.equal(false);
        });
    });

    it("does not delete a published feature", {
      auto answer = crates.campaignAnswer.getItem("000000000000000000000001").and.exec.front;
      answer["featureId"] = "000000000000000000000002";
      crates.campaignAnswer.updateItem(answer);

      auto feature = crates.feature.getItem("000000000000000000000002").and.exec.front;
      feature["visibility"] = 1;
      crates.feature.updateItem(feature);

      router
        .request
        .post("/campaignanswers/000000000000000000000001/reject")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(200)
        .end((Response response) => () {
            response.bodyJson.should.equal((`{
              "success": true
            }`).parseJsonString);

            auto range = crates.feature.get.where("_id").equal(ObjectId("000000000000000000000002")).and.exec;

            range.empty.should.equal(false);
        });
    });
  });
});
