/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.campaignAnswers.middlewares.mapper;

import crate.base;
import crate.error;

import vibe.data.json;
import vibe.http.server;

import gis_collective.hmq.log;

import ogm.crates.all;
import ogm.http.request;

import std.algorithm;
import std.exception;

///
class CampaignAnswersMapper {
  private {
    OgmCrates crates;
  }

  ///
  this(OgmCrates crates) {
    this.crates = crates;
  }

  @mapper
  void fieldCleanup(ref Json item) {
    if(item["sounds"].type != Json.Type.array) {
      item["sounds"] = Json.emptyArray;
    }

    if("featureId" !in item) {
      return;
    }

    if(item["featureId"] == "") {
      item.remove("featureId");
    }
  }
}