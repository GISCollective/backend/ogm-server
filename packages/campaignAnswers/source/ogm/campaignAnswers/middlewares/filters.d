/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.campaignAnswers.middlewares.filters;

import crate.base;
import crate.error;

import vibe.data.json;
import vibe.http.server;

import gis_collective.hmq.log;

import ogm.crates.all;
import ogm.http.request;

import std.algorithm;
import std.exception;

///
class CampaignAnswersFilters {

  struct Params {
    @describe("Filter records by campaign")
    string campaign;

    @describe("Filter records by author")
    string author;
  }

  private {
    OgmCrates crates;
  }

  ///
  this(OgmCrates crates) {
    this.crates = crates;
  }

  @getList
  void paramsFilter(HTTPServerRequest req, IQuery selector, Params params) {
    auto request = RequestUserData(req);

    if(params.campaign != "") {
      selector.where("campaign").equal(ObjectId.fromString(params.campaign));
    }
  }
}