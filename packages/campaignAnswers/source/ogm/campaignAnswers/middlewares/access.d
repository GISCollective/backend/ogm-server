/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.campaignAnswers.middlewares.access;

import crate.base;
import crate.error;

import vibe.data.json;
import vibe.http.server;

import gis_collective.hmq.log;

import ogm.crates.all;
import ogm.http.request;

import std.algorithm;
import std.exception;
import std.array;

///
class CampaignAnswersAccess {

  struct Params {
    @describe("Filter records by campaign")
    string campaign;

    @describe("Filter records by author")
    string author;
  }

  private {
    OgmCrates crates;
  }

  ///
  this(OgmCrates crates) {
    this.crates = crates;
  }

  @create
  void checkCampaignAccess(HTTPServerRequest req) {
    scope request = RequestUserData(req);

    string campaignId = req.json["campaignAnswer"]["campaign"].to!string;

    auto existingCampaign = crates.campaign.getItem(campaignId).and.exec.front;
    auto teamId = existingCampaign["visibility"]["team"].to!string;
    auto campaignIds = request.session(crates).all.campaigns;
    auto canAdd = request.isAdmin || existingCampaign["visibility"]["isPublic"] == true || campaignIds.canFind(campaignId);

    enforce!CrateNotFoundException(canAdd, "Campaign with id `" ~ campaignId ~ "` not found.");

    if(req.json["campaignAnswer"]["sounds"].type != Json.Type.array) {
      req.json["campaignAnswer"]["sounds"] = Json.emptyArray;
    }

    auto soundIds = req.json["campaignAnswer"]["sounds"].deserializeJson!(ObjectId[]);
    auto sounds = crates.sound.get.where("_id").anyOf(soundIds).and.exec.array;

    import std.stdio;
    string[] validSounds;
    string[] validTeams = ["null", "", teamId];

    foreach(sound; sounds) {
      auto soundTeam = sound["visibility"]["team"].to!string;

      if(!validTeams.canFind(soundTeam)) {
        continue;
      }

      validSounds ~= sound["_id"].to!string;
      sound["visibility"]["team"] = teamId;
      sound["visibility"]["isPublic"] = false;
      sound["visibility"]["isDefault"] = false;

      crates.sound.updateItem(sound);
    }

    req.json["campaignAnswer"]["sounds"] = sounds.serializeToJson;
  }

  @patch @put
  void checkAccess(HTTPServerRequest req) {
    string id = req.params["id"];

    auto request = RequestUserData(req);

    auto answer = crates.campaignAnswer.getItem(request.itemId).and.exec.front;
    enforce!CrateValidationException(answer["campaign"] == req.json["campaignAnswer"]["campaign"], "You can not change the campaign.");

    if("status" in req.json["campaignAnswer"]) {
      enforce!CrateValidationException(answer["status"] == req.json["campaignAnswer"]["status"], "You can not change the status.");
    }

    if("review" in req.json["campaignAnswer"]) {
      enforce!CrateValidationException(answer["review"].to!string == req.json["campaignAnswer"]["review"].to!string, "You can not change the review.");
    }

    req.json["campaignAnswer"]["status"] = answer["status"];
    req.json["campaignAnswer"]["review"] = answer["review"];

    if(request.isAdmin || answer["info"]["originalAuthor"] == request.userId) {
      return;
    }

    auto session = request.session(crates);
    auto campaignId = answer["campaign"].to!string;

    auto campaignIds = request.session(crates).owner.campaigns ~
      request.session(crates).leader.campaigns ~
      request.session(crates).member.campaigns;

    enforce!CrateValidationException(campaignIds.canFind(campaignId), "The campaign answer with id `" ~ id ~ "` can not be changed.");
  }

  @delete_
  void checkDeleteAccess(HTTPServerRequest req) {
    auto request = RequestUserData(req);

    auto answer = crates.campaignAnswer.getItem(request.itemId).and.exec.front;
    auto status = answer["status"].to!int;

    bool canDelete = status <= 1 || request.isAdmin;
    bool isAuthor = answer["info"]["originalAuthor"] == request.userId;

    enforce!ForbiddenException(canDelete, "The campaign answer with id `" ~ answer["_id"].to!string ~ "` can not be deleted.");

    if(request.isAdmin || isAuthor) {
      return;
    }

    auto session = request.session(crates);
    auto campaignId = answer["campaign"].to!string;

    auto campaignIds = request.session(crates).owner.campaigns ~
      request.session(crates).leader.campaigns ~
      request.session(crates).member.campaigns;

    enforce!ForbiddenException(campaignIds.canFind(campaignId), "The campaign answer with id `" ~ answer["_id"].to!string ~ "` can not be deleted.");
  }

  @getItem
  void checkGetItemAccess(HTTPServerRequest req) {
    auto request = RequestUserData(req);

    auto answer = crates.campaignAnswer.getItem(request.itemId).and.exec.front;

    if(request.isAdmin || answer["info"]["originalAuthor"] == request.userId) {
      return;
    }

    auto session = request.session(crates);
    auto campaignId = answer["campaign"].to!string;

    auto campaignIds = request.session(crates).owner.campaigns ~
      request.session(crates).leader.campaigns ~
      request.session(crates).member.campaigns ~
      request.session(crates).guest.campaigns;

    enforce!ForbiddenException(campaignIds.canFind(campaignId), "The campaign answer with id `" ~ request.itemId ~ "` can not be accessed by id.");
  }

  @getList
  void checkGetListAccess(HTTPServerRequest req, Params params) {
    auto request = RequestUserData(req);

    enforce!CrateValidationException(request.isAuthenticated, "The campaign answers can not be accessed.");
    enforce!CrateValidationException(params.campaign != "" || params.author != "", "The campaign answers can not be accessed without query params.");

    if(!request.isAdmin && request.userId && params.author) {
      enforce!CrateValidationException(params.author == request.userId, "You can't query answers for the user '" ~ params.author ~ "'.");
    }
  }

  @mapper
  void canEditField(HTTPServerRequest req, ref Json item) {
    auto request = RequestUserData(req);

    if(request.isAdmin) {
      item["canEdit"] = true;
      item["canReview"] = true;
      return;
    }

    auto campaignId = item["campaign"].to!string;

    auto campaignIds = request.session(crates).owner.campaigns ~
      request.session(crates).leader.campaigns ~
      request.session(crates).member.campaigns;

    try {
      auto userId = item["info"]["originalAuthor"].to!string;
      auto status = item["status"].to!int;

      if(request.userId == userId) {
        item["canEdit"] = status <= 1;
        item["canReview"] = campaignIds.canFind(campaignId);
        return;
      }
    } catch(Exception e) {
      error(e);
    }


    item["canEdit"] = campaignIds.canFind(campaignId);
    item["canReview"] = campaignIds.canFind(campaignId);
  }
}