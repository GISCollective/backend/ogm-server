module ogm.campaignAnswers.operation.MapSync;

import crate.http.operations.base;
import gis_collective.hmq.broadcast.base;
import ogm.crates.all;
import crate.base;
import vibe.http.common;
import ogm.http.request;
import std.exception;
import vibe.data.json;
import crate.error;
import std.algorithm;


class MapSync : CrateOperation!DefaultStorage {

  protected {
    OgmCrates crates;
    IBroadcast broadcast;
  }

  ///
  this(OgmCrates crates, IBroadcast broadcast) {
    this.crates = crates;
    CrateRule rule;

    rule.request.path = "/campaignanswers/:id/mapsync";
    rule.request.method = HTTPMethod.GET;

    this.broadcast = broadcast;

    super(crates.campaignAnswer, rule);
  }

  override void handle(DefaultStorage storage) {
    storage.item = this.prepareItemOperation!"getItem"(storage);

    auto request = RequestUserData(storage.request);
    enforce!UnauthorizedException(request.isAuthenticated, "Authorization required");

    auto session = request.session(crates);
    auto answer = crates.campaignAnswer.getItem(request.itemId).and.exec.front;
    auto campaigns = request.session(crates).owner.campaigns ~
      request.session(crates).leader.campaigns ~
      request.session(crates).member.campaigns;

    enforce!CrateValidationException(request.isAdmin || campaigns.canFind(answer["campaign"].to!string), "The campaign answer with id `" ~ request.itemId ~ "` can not be accessed by id.");

    auto message = Json.emptyObject;
    message["id"] = storage.properties.itemId;

    this.broadcast.push("CampaignAnswer.MapSync", message);
    storage.response.writeBody(`{ "success": true }`, 200, "text/json");
  }
}