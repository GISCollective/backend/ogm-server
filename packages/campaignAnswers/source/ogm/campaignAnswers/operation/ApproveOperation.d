module ogm.campaignAnswers.operation.ApproveOperation;

import crate.base;
import crate.error;
import crate.http.operations.base;
import gis_collective.hmq.broadcast.base;
import ogm.calendar;
import ogm.crates.all;
import ogm.http.request;
import std.algorithm;
import std.exception;
import std.array;
import vibe.data.json;
import vibe.http.common;
import ogm.features.fromCampaignAnswer;
import ogm.features.visibility;

class ApproveOperation : CrateOperation!DefaultStorage {

  protected {
    OgmCrates crates;
    IBroadcast broadcast;
  }

  ///
  this(OgmCrates crates, IBroadcast broadcast) {
    this.crates = crates;
    CrateRule rule;

    rule.request.path = "/campaignanswers/:id/approve";
    rule.request.method = HTTPMethod.POST;

    this.broadcast = broadcast;

    super(crates.campaignAnswer, rule);
  }

  void updateFeature(string featureId, Json answer, string userId) {
    long changeIndex;
    auto feature = crates.feature.getItem(featureId).and.exec.front;
    auto change = Json.emptyObject;
    change["removed"] = feature.clone;

    if("info" in feature && feature["info"]["changeIndex"].type == Json.Type.int_) {
      changeIndex = feature["info"]["changeIndex"].to!long;
    }

    if("info" in feature) {
      feature["info"]["author"] = userId;
      feature["info"]["lastChangeOn"] = SysCalendar.instance.now.toISOExtString;
      feature["info"]["changeIndex"] = changeIndex + 1;
    }

    feature["visibility"] = 1;
    feature["source"] = Json.emptyObject;
    feature["source"]["type"] = "Campaign";
    feature["source"]["modelId"] = answer["campaign"];
    feature["source"]["remoteId"] = answer["_id"];
    feature["source"]["syncAt"] = SysCalendar.instance.now.toISOExtString;

    if(feature["pictures"].type != Json.Type.array) {
      feature["pictures"] = Json.emptyArray;
    }

    if(answer["pictures"].type == Json.Type.array) {
      auto pictures = feature["pictures"].byValue.array;
      auto uniqIds = answer["pictures"].byValue.filter!(id => !pictures.canFind(id));

      foreach(ref id; uniqIds) {
        feature["pictures"] ~= id;
      }
    }

    auto about = Json.emptyObject;

    if("attributes" in answer && "about" in answer["attributes"]) {
      about = answer["attributes"]["about"];
    }

    if("name" in about) {
      feature["name"] = about["name"];
    }

    if("description" in about) {
      feature["description"] = about["description"];
    }

    foreach(string key, group; answer["attributes"]) {
      if(key == "about") {
        continue;
      }

      auto hasArrayAttribute = feature["attributes"][key].type == Json.Type.array;

      if(!hasArrayAttribute) {
        feature["attributes"][key] = group;
      }

      if(hasArrayAttribute) {
        feature["attributes"][key] ~= group;
      }
    }

    auto reference = "CampaignAnswer:" ~ answer["_id"].to!string;
    auto existingSnapshots = crates.changeSet.get.where("reference").equal(reference).and.exec;

    foreach(snapshot; existingSnapshots) {
      crates.changeSet.deleteItem(snapshot["_id"].to!string);
    }

    feature["computedVisibility"] = crates.getComputedVisibility(feature);

    change["added"] = crates.feature.updateItem(feature);

    change["model"] = "Feature";
    change["itemId"] = feature["_id"];
    change["time"] = SysCalendar.instance.now.toISOExtString;
    change["type"] = "snapshot";
    change["author"] = userId;
    change["reference"] = reference;

    crates.changeSet.addItem(change);
  }

  override void handle(DefaultStorage storage) {
    storage.item = this.prepareItemOperation!"getItem"(storage);

    auto request = RequestUserData(storage.request);
    enforce!UnauthorizedException(request.isAuthenticated, "Authorization required");

    auto session = request.session(crates);
    auto answer = crates.campaignAnswer.getItem(request.itemId).and.exec.front;

    auto campaignId = answer["campaign"].to!string;
    auto campaignIds = request.session(crates).owner.campaigns ~
      request.session(crates).leader.campaigns ~
      request.session(crates).member.campaigns;

    enforce!ForbiddenException(request.isAdmin || campaignIds.canFind(campaignId), "The campaign answer with id `" ~ request.itemId ~ "` can not be approved by you.");

    answer["status"] = cast(int) CampaignAnswerStatus.published;
    answer["review"] = Json.emptyObject;
    answer["review"]["reviewedOn"] = SysCalendar.instance.now.toISOExtString;
    answer["review"]["user"] = request.userId;

    if(!answer.hasFeature(crates)) {
      auto feature = answer.fromCampaignAnswer(crates, broadcast);
      answer["featureId"] = feature["_id"];
    }

    this.updateFeature(answer["featureId"].to!string, answer, request.userId);

    crates.campaignAnswer.updateItem(answer);

    auto message = Json.emptyObject;
    message["id"] = answer["_id"];

    broadcast.push("CampaignAnswer.approve", message);

    storage.response.writeBody(`{ "success": true }`, 200, "text/json");
  }
}