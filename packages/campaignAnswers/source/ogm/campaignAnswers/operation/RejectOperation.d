module ogm.campaignAnswers.operation.RejectOperation;

import crate.base;
import crate.error;
import crate.http.operations.base;
import gis_collective.hmq.broadcast.base;
import ogm.calendar;
import ogm.crates.all;
import ogm.http.request;
import std.algorithm;
import std.exception;
import std.array;
import vibe.data.json;
import vibe.http.common;

class RejectOperation : CrateOperation!DefaultStorage {

  protected {
    OgmCrates crates;
    IBroadcast broadcast;
  }

  ///
  this(OgmCrates crates, IBroadcast broadcast) {
    this.crates = crates;
    CrateRule rule;

    rule.request.path = "/campaignanswers/:id/reject";
    rule.request.method = HTTPMethod.POST;

    this.broadcast = broadcast;

    super(crates.campaignAnswer, rule);
  }

  override void handle(DefaultStorage storage) {
    storage.item = this.prepareItemOperation!"getItem"(storage);

    auto request = RequestUserData(storage.request);
    enforce!UnauthorizedException(request.isAuthenticated, "Authorization required");

    auto session = request.session(crates);
    auto answer = crates.campaignAnswer.getItem(request.itemId).and.exec.front;

    auto campaignId = answer["campaign"].to!string;
    auto campaignIds = request.session(crates).owner.campaigns ~
      request.session(crates).leader.campaigns ~
      request.session(crates).member.campaigns;

    enforce!ForbiddenException(request.isAdmin || campaignIds.canFind(campaignId), "The campaign answer with id `" ~ request.itemId ~ "` can not be rejected by you.");
    enforce!ForbiddenException(answer["status"] == 1, "The campaign answer with id `" ~ request.itemId ~ "` can not be rejected. Invalid status " ~ answer["status"].to!string ~ " != 1");

    answer["status"] = 4;
    answer["review"] = Json.emptyObject;
    answer["review"]["reviewedOn"] = SysCalendar.instance.now.toISOExtString;
    answer["review"]["user"] = request.userId;

    if(crates.feature.get.where("_id").equal(ObjectId(answer["featureId"])).and.size == 0) {
      answer.remove("featureId");
    }

    if("featureId" in answer) {
      auto featureId = answer["featureId"].to!string;
      auto pendingFeatures = crates.feature.get
        .where("_id").equal(ObjectId(featureId)).and
        .where("visibility").equal(-1).and
        .where("info.changeIndex").equal(0).and
        .size;

      if(pendingFeatures == 1) {
        crates.feature.deleteItem(featureId);
      }

      answer.remove("featureId");
    }

    crates.campaignAnswer.updateItem(answer);

    auto message = Json.emptyObject;
    message["id"] = answer["_id"];

    broadcast.push("CampaignAnswer.reject", message);

    storage.response.writeBody(`{ "success": true }`, 200, "text/json");
  }
}