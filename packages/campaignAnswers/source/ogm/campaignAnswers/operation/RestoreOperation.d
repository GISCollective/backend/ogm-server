module ogm.campaignAnswers.operation.RestoreOperation;

import crate.base;
import crate.error;
import crate.http.operations.base;
import gis_collective.hmq.broadcast.base;
import ogm.calendar;
import ogm.crates.all;
import ogm.http.request;
import std.algorithm;
import std.exception;
import std.array;
import vibe.data.json;
import vibe.http.common;

class RestoreOperation : CrateOperation!DefaultStorage {
  protected {
    OgmCrates crates;
    IBroadcast broadcast;
  }

  ///
  this(OgmCrates crates, IBroadcast broadcast) {
    this.crates = crates;
    CrateRule rule;

    rule.request.path = "/campaignanswers/:id/restore";
    rule.request.method = HTTPMethod.POST;

    this.broadcast = broadcast;

    super(crates.campaignAnswer, rule);
  }

  override void handle(DefaultStorage storage) {
    storage.item = this.prepareItemOperation!"getItem"(storage);

    auto request = RequestUserData(storage.request);
    enforce!UnauthorizedException(request.isAuthenticated, "Authorization required");

    auto session = request.session(crates);
    auto answer = crates.campaignAnswer.getItem(request.itemId).and.exec.front.clone;

    auto campaignId = answer["campaign"].to!string;
    auto campaignIds = request.session(crates).owner.campaigns ~
      request.session(crates).leader.campaigns ~
      request.session(crates).member.campaigns;

    enforce!ForbiddenException(request.isAdmin || campaignIds.canFind(campaignId), "The campaign answer with id `" ~ request.itemId ~ "` can not be restored by you.");
    enforce!ForbiddenException(answer["status"].to!int >= 2, "The campaign answer with id `" ~ request.itemId ~ "` can not be restored. Invalid status " ~ answer["status"].to!string ~ ".");

    answer["status"] = cast(int) CampaignAnswerStatus.pending;

    crates.campaignAnswer.updateItem(answer);

    auto featureExists = crates.feature.get.where("_id").equal(ObjectId(answer["featureId"])).and.size > 0;

    auto changeSetRange = crates.changeSet.get
      .where("reference").equal("CampaignAnswer:" ~ request.itemId).and
      .where("itemId").equal(ObjectId(request.itemId)).and
      .exec;

    if(!changeSetRange.empty && featureExists) {
      crates.feature.updateItem(changeSetRange.front["removed"]);
      crates.changeSet.deleteItem(changeSetRange.front["_id"].to!string);
    }

    auto message = Json.emptyObject;
    broadcast.push("CampaignAnswer.restore", message);

    storage.response.writeBody(`{ "success": true }`, 200, "text/json");
  }
}