/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.campaignAnswers.operation.BulkDeleteOperation;

import crate.base;
import crate.http.wrapper;
import crate.http.operations.base;
import crate.auth.middleware;
import crate.error;

import ogm.rights;
import ogm.crates.all;
import ogm.http.request;
import ogm.middleware.ResourceMiddleware;
import ogm.operations.BulkOperation;

import std.algorithm;
import std.array;

import vibe.data.json;
import vibe.http.server;

class BulkDeleteOperation : BulkOperation!("campaign", true) {
  private {
    IResourceMiddleware[] resourceMiddlewares;
  }

  this(OgmCrates crates, PrivateDataMiddleware privateData, IResourceMiddleware[] resourceMiddlewares) {
    this.resourceMiddlewares = resourceMiddlewares;

    CrateRule rule;

    rule.request.path = "/campaignanswers";
    rule.request.method = HTTPMethod.DELETE;

    super(crates, crates.campaignAnswer, privateData, rule);
  }

  override void performOperation(ref Json item, RequestUserData request, DefaultStorage storage, bool isOwner, bool isAuthor) {
    auto status = item["status"].to!int;

    bool canDelete = status <= 1;

    if(!request.isAdmin && !isAuthor && canDelete) {
      auto campaignId = item["campaign"].to!string;
      enforce!UnauthorizedException(request.isAuthenticated, "Authorization required");

      auto session = request.session(crates);
      auto campaignIds = request.session(crates).owner.campaigns ~
        request.session(crates).leader.campaigns ~
        request.session(crates).member.campaigns;

      canDelete = campaignIds.canFind(campaignId);
    }

    if(canDelete || request.isAdmin) {
      foreach (middleware; resourceMiddlewares) {
        middleware.delete_(storage.request);
      }

      crate.deleteItem(item["_id"].to!string);
    }
  }
}