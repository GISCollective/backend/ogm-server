/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.campaignAnswers.api;

import vibe.http.router;
import crate.http.router;
import crate.base;
import crate.error;

import ogm.auth;
import ogm.crates.all;

import ogm.middleware.adminrequest;
import ogm.middleware.userdata;
import ogm.campaignAnswers.middlewares.access;
import ogm.middleware.attributes.IconAttributesValidator;
import ogm.campaignAnswers.middlewares.filters;
import ogm.campaignAnswers.operation.ApproveOperation;
import ogm.campaignAnswers.operation.RejectOperation;
import ogm.campaignAnswers.operation.RestoreOperation;
import ogm.campaignAnswers.operation.BulkDeleteOperation;
import ogm.middleware.sort;

import ogm.filter.pagination;
import ogm.filter.searchterm;
import ogm.filter.visibility;
import ogm.filter.searchtag;
import ogm.icons.IconCache;
import ogm.icons.TaxonomyTree;

import ogm.campaignAnswers.operation.MapSync;
import ogm.campaignAnswers.middlewares.mapper;

import ogm.middleware.modelinfo;
import gis_collective.hmq.broadcast.base;
import ogm.middleware.ResourceMiddleware;

///
void setupCampaignAnswerApi(T)(CrateRouter!T crateRouter, OgmCrates crates, IBroadcast broadcast) {
  PictureFileSettings.files = crates.pictureFiles;
  PictureFileSettings.chunks = crates.pictureChunks;

  auto auth = Authentication.instance(crates.user);
  auto adminRequest = new AdminRequest(crates.user);
  auto userDataMiddleware = UserDataMiddleware.instance(crates);

  auto modelInfo = new ModelInfoMiddleware("campaignAnswer", crates.campaignAnswer);

  crateGetters["Team"] = &crates.team.getItem;
  crateGetters["Picture"] = &crates.picture.getItem;
  crateGetters["Sound"] = &crates.sound.getItem;
  crateGetters["IconSet"] = &crates.iconSet.getItem;
  crateGetters["Icon"] = &crates.icon.getItem;
  crateGetters["Map"] = &crates.map.getItem;
  crateGetters["Campaign"] = &crates.campaign.getItem;

  metaCrate = crates.meta;
  batchJobCrate = crates.batchJob;

  auto cache = new IconCache(&crates.icon.get, &crates.iconSet.getItem);
  auto iconTree = new TaxonomyTree(&crates.icon.get, &crates.iconSet.getItem, cache);

  auto privateAuth = auth.privateDataMiddleware;
  auto mandatoryAuth = &privateAuth.mandatory;

  auto preparedRoute = crateRouter.prepare(crates.campaignAnswer);
  auto access = new CampaignAnswersAccess(crates);
  auto filters = new CampaignAnswersFilters(crates);
  auto iconAttributesValidator = new IconAttributesValidator(iconTree, "campaignAnswer");
  auto mapper = new CampaignAnswersMapper(crates);
  auto sort = new SortMiddleware(crates, "info.createdOn", -1);

  auto mapSync = new MapSync(crates, broadcast);
  auto approve = new ApproveOperation(crates, broadcast);
  auto reject = new RejectOperation(crates, broadcast);
  auto restore = new RestoreOperation(crates, broadcast);
  auto paginationFilter = new PaginationFilter;

  auto soundsMiddleware = new ResourceMiddleware!("campaignAnswer", "sounds")(crates.campaignAnswer, crates.sound);

  auto bulkDelete = new BulkDeleteOperation(crates, privateAuth, [soundsMiddleware]);

  preparedRoute
    .withCustomOperation(mapSync)
    .withCustomOperation(approve)
    .withCustomOperation(reject)
    .withCustomOperation(restore)
    .withCustomOperation(bulkDelete)
    .and(auth.publicContributionMiddleware)
    .and(adminRequest)
    .and(userDataMiddleware)
    .and(access)
    .and(modelInfo)
    .and(filters)
    .and(iconAttributesValidator)
    .and(sort)
    .and(paginationFilter)
    .and(mapper)
    .and(soundsMiddleware);
}
