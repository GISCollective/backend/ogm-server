/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.stats.api;

import vibe.http.router;
import vibe.core.log;
import vibe.data.json;

import crate.http.router;
import crate.base;
import ogm.defaults.spaces;

import ogm.auth;
import ogm.crates.all;
import ogm.middleware.adminrequest;
import ogm.middleware.userdata;
import ogm.stats.operations.CampaignAnswerContributors;
import ogm.stats.operations.PublishedCampaigns;
import ogm.stats.operations.PublishedFeatures;
import ogm.stats.operations.CampaignContributions;
import ogm.stats.operations.MapViews;
import ogm.stats.operations.MapContributors;
import ogm.stats.operations.NewsletterSubscribers;
import ogm.stats.operations.NewsletterEmails;
import ogm.stats.operations.NewsletterOpen;
import ogm.stats.operations.NewsletterInteractions;

void setupStatsApi(T)(CrateRouter!T crateRouter, OgmCrates crates) {
  auto auth = Authentication.instance(crates.user);
  auto campaignAnswerContributors = new CampaignAnswerContributors(crates);
  auto publishedCampaigns = new PublishedCampaigns(crates);
  auto publishedFeatures = new PublishedFeatures(crates);
  auto campaignContributions = new CampaignContributions(crates);
  auto mapContributors = new MapContributors(crates);
  auto mapViews = new MapViews(crates);
  auto newsletterSubscribers = new NewsletterSubscribers(crates);
  auto newsletterEmails = new NewsletterEmails(crates);
  auto newsletterOpen = new NewsletterOpen(crates);
  auto newsletterInteractions = new NewsletterInteractions(crates);

  auto adminRequest = new AdminRequest(crates.user);
  auto userDataMiddleware = UserDataMiddleware.instance(crates);

  crateRouter
    .prepare(crates.stat)
    .and(auth.publicDataMiddleware)
    .and(adminRequest)
    .and(userDataMiddleware)
    .and(campaignAnswerContributors)
    .and(campaignContributions)
    .and(publishedCampaigns)
    .and(publishedFeatures)
    .and(mapViews)
    .and(mapContributors)
    .and(newsletterSubscribers)
    .and(newsletterEmails)
    .and(newsletterOpen)
    .and(newsletterInteractions);
}