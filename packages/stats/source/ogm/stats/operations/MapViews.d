/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.stats.operations.MapViews;

import ogm.http.request;
import ogm.crates.all;
import crate.base;
import vibe.data.json;
import std.datetime;
import std.string;
import std.algorithm;
import crate.http.queryParams;
import ogm.stats.operations.StatMiddleware;
import std.array;


import vibe.http.server;

class MapViews : StatMiddleware {
  struct Params {
    string map;
    string team;
  }

  this(OgmCrates crates) {
    super(crates);
  }

  override string statName() {
    return "map-views";
  }

  override string getFullName(HTTPServerRequest req) {
    auto params = req.query.parseQueryParams!Params;

    return getFullName(params);
  }

  string getFullName(Params params) {
    auto name = this.statName;
    if(params.team != "") {
      name ~= "-team-" ~ params.team;
    }

    if(params.map != "") {
      name ~= "-map-" ~ params.map;
    }

    return name;
  }

  override Json getStatValue(HTTPServerRequest req) {
    auto params = req.query.parseQueryParams!Params;

    return getStatValue(params);
  }

  Json getStatValue(Params params) {
    auto query = crates.metric.get.where("type").equal("mapView").and;

    if(params.map != "") {
      query.where("name").equal(params.map);
    }

    if(params.team != "") {
      auto maps = crates.map.get.where("visibility.team")
        .equal(ObjectId.fromString(params.team))
        .and.withProjection(["_id"])
        .and.exec
        .map!(a => a["_id"].to!string)
        .array;

      query.where("name").anyOf(maps);
    }

    auto result = reduce!((a, b) => a + b)(0., query.and.exec.map!(a => a["value"].to!double));

    return Json(result);
  }
}
