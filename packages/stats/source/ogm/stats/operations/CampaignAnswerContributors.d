/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.stats.operations.CampaignAnswerContributors;


import ogm.http.request;
import ogm.crates.all;
import crate.base;
import vibe.data.json;
import std.datetime;
import std.string;
import std.algorithm;
import std.array;
import crate.http.queryParams;
import ogm.stats.operations.StatMiddleware;

import vibe.http.server;

class CampaignAnswerContributors : StatMiddleware {

  struct Params {
    string campaign;
    string team;
  }

  this(OgmCrates crates) {
    super(crates);
  }

  override string statName() {
    return "campaign-answer-contributors";
  }

  override string getFullName(HTTPServerRequest req) {
    auto params = req.query.parseQueryParams!Params;

    auto name = this.statName;

    if(params.campaign != "") {
      name ~= "-campaign-" ~ params.campaign;
    }

    if(params.team != "") {
      name ~= "-team-" ~ params.team;
    }

    return name;
  }

  override Json getStatValue(HTTPServerRequest req) {
    auto params = req.query.parseQueryParams!Params;

    auto query = crates.campaignAnswer.get.where("status").equal(2).and;

    if(params.campaign != "") {
      query.where("campaign").equal(ObjectId.fromString(params.campaign));
    }

    if(params.team != "") {
      auto campaigns = crates.campaign.get.where("visibility.team")
        .equal(ObjectId.fromString(params.team))
        .and.withProjection(["_id"])
        .and.exec
        .map!(a => ObjectId.fromJson(a["_id"]))
        .array;

      query.where("campaign").anyOf(campaigns);
    }

    return Json(query.distinct("info.originalAuthor").count);
  }
}
