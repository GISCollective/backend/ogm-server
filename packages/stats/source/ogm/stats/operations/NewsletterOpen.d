/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.stats.operations.NewsletterOpen;

import crate.base;
import crate.error;
import crate.http.queryParams;

import ogm.crates.all;
import ogm.http.request;
import ogm.http.request;
import ogm.stats.operations.StatMiddleware;

import std.algorithm;
import std.array;
import std.datetime;
import std.exception;
import std.string;
import vibe.data.json;

import vibe.http.server;

class NewsletterOpen : StatMiddleware {
  struct Params {
    string newsletter;
  }

  this(OgmCrates crates) {
    super(crates);
  }

  override string statName() {
    return "newsletter-open";
  }

  override string getFullName(HTTPServerRequest req) {
    auto params = req.query.parseQueryParams!Params;

    enforce!CrateValidationException(params.newsletter, "The newsletter query paramater must be set.");

    scope request = RequestUserData(req);

    auto newsletters = request.session(crates).all.newsletters;

    enforce!CrateValidationException(newsletters.canFind(params.newsletter), "You can't get this stat.");

    return this.statName ~ "-" ~ params.newsletter;
  }

  override Json getStatValue(HTTPServerRequest req) {
    auto params = req.query.parseQueryParams!Params;

    auto query = crates.metric.get.where("type").equal("articleView").and
      .where("labels.relatedId").equal(params.newsletter);

    auto result = reduce!((a, b) => a + b)(0., query.and.exec.map!(a => a["value"].to!double));

    return Json(result);
  }
}
