/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.stats.operations.PublishedFeatures;

import ogm.http.request;
import ogm.crates.all;
import crate.base;
import vibe.data.json;
import std.datetime;
import std.string;
import crate.http.queryParams;
import ogm.stats.operations.StatMiddleware;
import ogm.filter.indirectvisibility;
import ogm.http.request;

import vibe.http.server;

class PublishedFeatures : StatMiddleware {
  struct Params {
    string map;
  }

  IndirectVisibility visibility;

  this(OgmCrates crates) {
    super(crates);

    visibility = new IndirectVisibility(crates, crates.feature);
  }

  override string statName() {
    return "published-features";
  }

  override string getFullName(HTTPServerRequest req) {
    auto params = req.query.parseQueryParams!Params;

    auto name = this.statName;
    if(params.map != "") {
      name ~= "-map-" ~ params.map;
    }

    return name;
  }

  override Json getStatValue(HTTPServerRequest req) {
    auto params = req.query.parseQueryParams!Params;
    auto visibilityParams = IndirectVisibility.Parameters();

    if(params.map != "") {
      visibilityParams.map = params.map;
    }

    auto request = RequestUserData(req);
    auto query = crates.feature.get.where("visibility").equal(1).and;

    query = visibility.readableItems(query, visibilityParams, request);

    return Json(query.and.size);
  }
}
