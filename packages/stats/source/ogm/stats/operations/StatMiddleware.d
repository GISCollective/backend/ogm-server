/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.stats.operations.StatMiddleware;

import ogm.http.request;
import ogm.crates.all;
import crate.base;
import vibe.data.json;
import std.datetime;
import std.string;
import crate.http.queryParams;

import vibe.http.server;

abstract class StatMiddleware {
  OgmCrates crates;

  this(OgmCrates crates) {
    this.crates = crates;
  }

  abstract string statName();
  abstract string getFullName(HTTPServerRequest);
  abstract Json getStatValue(HTTPServerRequest);

  @getItem
  void checkExistence(HTTPServerRequest req) {
    scope request = RequestUserData(req);

    if(request.itemId.indexOf(this.statName) != 0) {
      return;
    }

    auto fullName = this.getFullName(req);
    auto recordRange = crates.stat.get
      .where("name")
      .equal(fullName)
      .and
      .exec;

    Json record;

    if(recordRange.empty) {
      auto item = Json.emptyObject;
      item["name"] = fullName;
      item["value"] = 0;

      record = crates.stat.addItem(item);
    } else {
      record = recordRange.front;
    }

    req.params["id"] = record["_id"].to!string;
    req.context["id"] = record["_id"].to!string;
  }

  @mapper
  void updateRecord(ref Json json, HTTPServerRequest req) {
    string name = json["name"].to!string;

    if(name.indexOf(this.statName) != 0) {
      return;
    }

    json["value"] = getStatValue(req);
    json["lastUpdate"] = Clock.currTime.toISOExtString;
  }
}
