/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.stats.operations.PublishedCampaigns;

import ogm.http.request;
import ogm.crates.all;
import crate.base;
import vibe.data.json;
import std.datetime;
import std.string;
import crate.http.queryParams;
import ogm.stats.operations.StatMiddleware;


import vibe.http.server;

class PublishedCampaigns : StatMiddleware {
  struct Params {
    string team;
  }

  this(OgmCrates crates) {
    super(crates);
  }

  override string statName() {
    return "published-campaigns";
  }

  override string getFullName(HTTPServerRequest req) {
    auto params = req.query.parseQueryParams!Params;

    auto name = this.statName;
    if(params.team != "") {
      name ~= "-team-" ~ params.team;
    }

    return name;
  }

  override Json getStatValue(HTTPServerRequest req) {
    auto params = req.query.parseQueryParams!Params;

    auto query = crates.campaign.get.where("visibility.isPublic").equal(true).and;

    if(params.team != "") {
      query.where("visibility.team").equal(ObjectId.fromString(params.team));
    }

    return Json(query.and.size);
  }
}
