/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.stats.operations.MapContributors;

import ogm.http.request;
import ogm.crates.all;
import crate.base;
import vibe.data.json;
import std.datetime;
import std.string;
import crate.http.queryParams;
import ogm.stats.operations.StatMiddleware;
import ogm.filter.visibility;
import ogm.http.request;
import ogm.maps.middlewares.get;
import std.exception;
import std.algorithm;
import std.array;
import ogm.filter.mapFilter;
import crate.error;

import vibe.http.server;

class MapContributors : StatMiddleware {
  struct Params {
    string map;
  }

  VisibilityFilter!"features" visibility;
  MapFilter mapFilter;

  this(OgmCrates crates) {
    super(crates);

    visibility = new VisibilityFilter!"features"(crates, crates.feature);
    mapFilter = new MapFilter(crates);
  }

  override string statName() {
    return "map-contributors";
  }

  override string getFullName(HTTPServerRequest req) {
    auto params = req.query.parseQueryParams!Params;

    auto name = this.statName;
    name ~= "-map-" ~ params.map;

    return name;
  }

  @getItem
  override void checkExistence(HTTPServerRequest req) {
    auto request = RequestUserData(req);
    if(request.itemId.indexOf(this.statName) != 0) {
      return;
    }

    auto params = req.query.parseQueryParams!Params;
    auto map = crates.map.getItem(params.map).and.exec.front;

    if(!map["visibility"]["isPublic"] && !request.isAdmin) {
      auto mapId = map["_id"].to!string;
      auto maps = request.session(crates).all.maps;

      enforce!CrateNotFoundException(maps.canFind(mapId), "The map '" ~ mapId ~ "' does not exist.");
    }

    super.checkExistence(req);
  }

  override Json getStatValue(HTTPServerRequest req) {
    auto params = req.query.parseQueryParams!Params;
    auto visibilityParams = VisibilityParameters();
    auto mapFilterParams = MapFilter.Parameters();

    mapFilterParams.map = params.map;

    auto request = RequestUserData(req);
    auto query = crates.feature.get.where("visibility").equal(1).and;

    query = visibility.get(query, visibilityParams, req);
    query = mapFilter.get(query, mapFilterParams, req).withProjection(["contributors", "info"]);

    Json[] contributors;

    foreach (Json record; query.and.exec) {
      auto recordContributors = record["contributors"].byValue.filter!(a => !contributors.canFind(a)).array;
      auto author = record["info"]["originalAuthor"];

      if(!contributors.canFind(author)) {
        contributors ~= author;
      }

      contributors ~= recordContributors;
    }

    return Json(contributors.length);
  }
}
