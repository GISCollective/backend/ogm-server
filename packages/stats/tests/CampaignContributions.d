/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.stats.CampaignContributions;

import tests.fixtures;
import vibe.http.common;
import ogm.stats.api;
import ogm.defaults.spaces;

alias suite = Spec!({
  URLRouter router;
  string aboutId;

  describe("get CampaignContributions", {
    beforeEach({
      setupTestData();
      router = new URLRouter;

      router.crateSetup.setupStatsApi(crates);
    });

    it("returns 0 when there is no campaign answer", {
      router
        .request
        .get("/stats/campaign-contributions")
        .expectStatusCode(200)
        .end((Response response) => () {
            auto expected = `{
              "stat": {
                "_id": "000000000000000000000001",
                "name": "campaign-contributions",
                "value": 0
              }
            }`.parseJsonString;

            expected["stat"]["lastUpdate"] = response.bodyJson["stat"]["lastUpdate"];

            response.bodyJson.should.equal(expected);
        });
    });

    describe("when there is a published answer associated to a campaign", {
      beforeEach({
        auto campaign = Campaign(ObjectId.fromString("000000000000000000000001"));
        campaign.visibility.team = Team(ObjectId.fromString("000000000000000000000001"));

        auto answer = CampaignAnswer(ObjectId.fromString("000000000000000000000002"));
        answer.campaign = campaign;
        answer.status = CampaignAnswerStatus.published;

        createCampaign(campaign);
        createCampaignAnswer(answer);
      });

      it("returns 1 when there is a campaign answer", {
        router
          .request
          .get("/stats/campaign-contributions")
          .expectStatusCode(200)
          .end((Response response) => () {
            auto expected = `{
              "stat": {
                "_id": "000000000000000000000001",
                "name": "campaign-contributions",
                "value": 1
              }
            }`.parseJsonString;

            expected["stat"]["lastUpdate"] = response.bodyJson["stat"]["lastUpdate"];

            response.bodyJson.should.equal(expected);
          });
      });

      it("returns 1 when the team matches the answer", {
        router
          .request
          .get("/stats/campaign-contributions?team=000000000000000000000001")
          .expectStatusCode(200)
          .end((Response response) => () {
            auto expected = `{
              "stat": {
                "_id": "000000000000000000000001",
                "name": "campaign-contributions-team-000000000000000000000001",
                "value": 1
              }
            }`.parseJsonString;

            expected["stat"]["lastUpdate"] = response.bodyJson["stat"]["lastUpdate"];

            response.bodyJson.should.equal(expected);
          });
      });

      it("returns 0 when the campaign query param does not match a campaign", {
        router
          .request
          .get("/stats/campaign-contributions?campaign=000000000000000000000005")
          .expectStatusCode(200)
          .end((Response response) => () {
            auto expected = `{
              "stat": {
                "_id": "000000000000000000000001",
                "name": "campaign-contributions-campaign-000000000000000000000005",
                "value": 0
              }
            }`.parseJsonString;

            expected["stat"]["lastUpdate"] = response.bodyJson["stat"]["lastUpdate"];

            response.bodyJson.should.equal(expected);
          });
      });

      it("returns 0 when the campaign query param does not match a team", {
        router
          .request
          .get("/stats/campaign-contributions?team=000000000000000000000005")
          .expectStatusCode(200)
          .end((Response response) => () {
            auto expected = `{
              "stat": {
                "_id": "000000000000000000000001",
                "name": "campaign-contributions-team-000000000000000000000005",
                "value": 0
              }
            }`.parseJsonString;

            expected["stat"]["lastUpdate"] = response.bodyJson["stat"]["lastUpdate"];

            response.bodyJson.should.equal(expected);
          });
      });
    });
  });
});
