/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.stats.PublishedFeatures;

import tests.fixtures;
import vibe.http.common;
import ogm.stats.api;
import ogm.defaults.spaces;

alias suite = Spec!({
  URLRouter router;
  string aboutId;

  describe("get PublishedFeatures", {
    beforeEach({
      setupTestData();
      router = new URLRouter;

      router.crateSetup.setupStatsApi(crates);
    });

    it("returns the number of published features on all maps when the map query param is not present", {
      router
        .request
        .get("/stats/published-features")
        .expectStatusCode(200)
        .end((Response response) => () {
          auto expected = `{
              "stat": {
                "_id": "000000000000000000000001",
                "name": "published-features",
                "value": 2
              }
            }`.parseJsonString;

          expected["stat"]["lastUpdate"] = response.bodyJson["stat"]["lastUpdate"];

          response.bodyJson.should.equal(expected);
        });
    });

    it("returns the number of published features on all maps when the map query param is not present", {
      router
        .request
        .get("/stats/published-features?map=000000000000000000000001")
        .expectStatusCode(200)
        .end((Response response) => () {
          auto expected = `{
              "stat": {
                "_id": "000000000000000000000001",
                "name": "published-features-map-000000000000000000000001",
                "value": 1
              }
            }`.parseJsonString;

          expected["stat"]["lastUpdate"] = response.bodyJson["stat"]["lastUpdate"];

          response.bodyJson.should.equal(expected);
        });
    });
  });
});
