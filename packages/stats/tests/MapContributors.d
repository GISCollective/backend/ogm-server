/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.stats.MapContributors;

import tests.fixtures;
import vibe.http.common;
import ogm.stats.api;
import ogm.defaults.spaces;

alias suite = Spec!({
  URLRouter router;
  string aboutId;

  describe("get contributors count", {
    beforeEach({
      setupTestData();
      router = new URLRouter;

      router.crateSetup.setupStatsApi(crates);


      auto feature = crates.feature.getItem("000000000000000000000002").and.exec.front;
      feature["visibility"] = 1;
      crates.feature.updateItem(feature);
    });

    it("returns the unique number of authors for the map", {
      router
        .request
        .get("/stats/map-contributors?map=000000000000000000000001")
        .expectStatusCode(200)
        .end((Response response) => () {
          auto expected = `{
              "stat": {
                "_id": "000000000000000000000001",
                "name": "map-contributors-map-000000000000000000000001",
                "value": 1
              }
            }`.parseJsonString;

          expected["stat"]["lastUpdate"] = response.bodyJson["stat"]["lastUpdate"];

          response.bodyJson.should.equal(expected);
        });
    });

    it("returns an error when getting the stat of a private map without a token", {
      router
        .request
        .get("/stats/map-contributors?map=000000000000000000000002")
        .expectStatusCode(404)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"errors": [{
            "description": "The map '000000000000000000000002' does not exist.",
            "status": 404,
            "title": "Crate not found"
          }]}`.parseJsonString);
        });
    });

    it("returns the stats when the token belongs to a team member", {
      router
        .request
        .get("/stats/map-contributors?map=000000000000000000000002")
        .header("Authorization", "Bearer " ~ bearerToken.name)
        .expectStatusCode(200)
        .end((Response response) => () {
          auto expected = `{
              "stat": {
                "_id": "000000000000000000000001",
                "name": "map-contributors-map-000000000000000000000002",
                "value": 1
              }
            }`.parseJsonString;
          expected["stat"]["lastUpdate"] = response.bodyJson["stat"]["lastUpdate"];

          response.bodyJson.should.equal(expected);
        });
    });

    it("returns the stats when the token belongs to an admin", {
      router
        .request
        .get("/stats/map-contributors?map=000000000000000000000002")
        .header("Authorization", "Bearer " ~ administratorToken.name)
        .expectStatusCode(200)
        .end((Response response) => () {
          auto expected = `{
              "stat": {
                "_id": "000000000000000000000001",
                "name": "map-contributors-map-000000000000000000000002",
                "value": 1
              }
            }`.parseJsonString;

          expected["stat"]["lastUpdate"] = response.bodyJson["stat"]["lastUpdate"];

          response.bodyJson.should.equal(expected);
        });
    });
  });
});
