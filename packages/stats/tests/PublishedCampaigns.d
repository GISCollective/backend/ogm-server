/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.stats.PublishedCampaigns;

import tests.fixtures;
import ogm.stats.api;
import vibe.http.common;
import ogm.defaults.spaces;

alias suite = Spec!({
  URLRouter router;
  string aboutId;

  describe("get PublishedCampaigns", {
    beforeEach({
      setupTestData();
      router = new URLRouter;

      router.crateSetup.setupStatsApi(crates);
    });

    it("returns the number of published campaigns", {
      router
        .request
        .get("/stats/published-campaigns")
        .expectStatusCode(200)
        .end((Response response) => () {
          auto expected = `{
              "stat": {
                "_id": "000000000000000000000001",
                "name": "published-campaigns",
                "value": 2
              }
            }`.parseJsonString;

          expected["stat"]["lastUpdate"] = response.bodyJson["stat"]["lastUpdate"];

          response.bodyJson.should.equal(expected);
        });
    });
  });
});
