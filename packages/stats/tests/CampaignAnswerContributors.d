/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.stats.CampaignAnswerContributors;

import tests.fixtures;
import vibe.http.common;
import ogm.defaults.spaces;
import ogm.stats.api;

alias suite = Spec!({
  URLRouter router;
  string aboutId;

  describe("get CampaignAnswerContributors", {
    beforeEach({
      setupTestData();
      router = new URLRouter;

      router.crateSetup.setupStatsApi(crates);
    });

    it("returns 0 when there is no campaign answer", {
      router
        .request
        .get("/stats/campaign-answer-contributors")
        .expectStatusCode(200)
        .end((Response response) => () {
          auto expected = `{
            "stat": {
              "_id": "000000000000000000000001",
              "name": "campaign-answer-contributors",
              "value": 0
            }
          }`.parseJsonString;

          expected["stat"]["lastUpdate"] = response.bodyJson["stat"]["lastUpdate"];

          response.bodyJson.should.equal(expected);
        });
    });

    describe("when there is a published answer associated to a campaign", {
      beforeEach({
        auto campaign = Campaign(ObjectId.fromString("000000000000000000000001"));
        campaign.visibility.team = Team(ObjectId.fromString("000000000000000000000001"));

        auto answer = CampaignAnswer(ObjectId.fromString("000000000000000000000002"));
        answer.campaign = campaign;
        answer.status = CampaignAnswerStatus.published;

        createCampaign(campaign);
        createCampaignAnswer(answer);
      });

      it("returns 1 when there is a campaign answer", {
        router
          .request
          .get("/stats/campaign-answer-contributors?campaign=000000000000000000000001")
          .expectStatusCode(200)
          .end((Response response) => () {
            auto expected = `{
              "stat": {
                "_id": "000000000000000000000001",
                "name": "campaign-answer-contributors-campaign-000000000000000000000001",
                "value": 1
              }
            }`.parseJsonString;

            expected["stat"]["lastUpdate"] = response.bodyJson["stat"]["lastUpdate"];

            response.bodyJson.should.equal(expected);
          });
      });

      it("returns 0 when the campaign query param does not match a campaign", {
        router
          .request
          .get("/stats/campaign-answer-contributors?campaign=000000000000000000000005")
          .expectStatusCode(200)
          .end((Response response) => () {
            auto expected = `{
              "stat": {
                "_id": "000000000000000000000001",
                "name": "campaign-answer-contributors-campaign-000000000000000000000005",
                "value": 0
              }
            }`.parseJsonString;

            expected["stat"]["lastUpdate"] = response.bodyJson["stat"]["lastUpdate"];

            response.bodyJson.should.equal(expected);
          });
      });

      it("returns 1 when queried with the team id", {
        router
          .request
          .get("/stats/campaign-answer-contributors?team=000000000000000000000001")
          .expectStatusCode(200)
          .end((Response response) => () {
            auto expected = `{
              "stat": {
                "_id": "000000000000000000000001",
                "name": "campaign-answer-contributors-team-000000000000000000000001",
                "value": 1
              }
            }`.parseJsonString;

            expected["stat"]["lastUpdate"] = response.bodyJson["stat"]["lastUpdate"];

            response.bodyJson.should.equal(expected);
          });
      });

      it("returns 0 when queried with a team id that hs no campoaigns", {
        router
          .request
          .get("/stats/campaign-answer-contributors?team=000000000000000000000002")
          .expectStatusCode(200)
          .end((Response response) => () {
            auto expected = `{
              "stat": {
                "_id": "000000000000000000000001",
                "name": "campaign-answer-contributors-team-000000000000000000000002",
                "value": 0
              }
            }`.parseJsonString;

            expected["stat"]["lastUpdate"] = response.bodyJson["stat"]["lastUpdate"];

            response.bodyJson.should.equal(expected);
          });
      });
    });
  });
});
