/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.stats.NewsletterOpen;

import tests.fixtures;
import ogm.stats.api;
import vibe.http.common;
import ogm.defaults.spaces;

alias suite = Spec!({
  URLRouter router;
  string aboutId;

  describe("get NewsletterOpen", {
    beforeEach({
      setupTestData();
      router = new URLRouter;

      router.crateSetup.setupStatsApi(crates);
    });

    it("returns an error when the newsletter id is not set", {
      router
        .request
        .get("/stats/newsletter-open")
        .expectStatusCode(400)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "errors": [{
              "description": "The newsletter query paramater must be set.",
              "status": 400,
              "title": "Validation error"
            }]
          }`.parseJsonString);
        });
    });

    describe("when there is a newsletter", {
      Json publicNewsletter;

      beforeEach({
        publicNewsletter = crates.newsletter.addItem(`{
          "name": "test",
          "description": "some description",
          "visibility": { "isDefault": false, "isPublic": true, "team": "000000000000000000000001" },
          "info": { "originalAuthor": "", "author": "", "createdOn": "2015-01-01T00:00:00Z", "lastChangeOn": "2015-01-01T00:00:00Z", "changeIndex": 0 }
        }`.parseJsonString);
      });

      it("returns 0 when the newsletter has no open", {
        router
          .request
          .get("/stats/newsletter-open?newsletter=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList["owner"])
          .expectStatusCode(200)
          .end((Response response) => () {
            auto expected = `{
                "stat": {
                  "_id": "000000000000000000000001",
                  "name": "newsletter-open-000000000000000000000001",
                  "value": 0
                }
              }`.parseJsonString;

            expected["stat"]["lastUpdate"] = response.bodyJson["stat"]["lastUpdate"];

            response.bodyJson.should.equal(expected);
          });
      });

      it("returns 1 when the newsletter has 1 sent email", {
        crates.metric.addItem((`{
          "labels": {
            "relatedId": "000000000000000000000001",
          },
          "name": "000000000000000000000001",
          "reporter": "",
          "time": "2022-02-02T00:00:00Z",
          "type": "articleView",
          "value": 1
        }`).parseJsonString);

        router
          .request
          .get("/stats/newsletter-open?newsletter=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList["owner"])
          .expectStatusCode(200)
          .end((Response response) => () {
            auto expected = `{
              "stat": {
                "_id": "000000000000000000000001",
                "name": "newsletter-open-000000000000000000000001",
                "value": 1
              }
            }`.parseJsonString;

            expected["stat"]["lastUpdate"] = response.bodyJson["stat"]["lastUpdate"];

            response.bodyJson.should.equal(expected);
          });
      });

      it("returns an error when the user does not have access to the newsletter", {
        crates.newsletterEmail.addItem(`{
          "_id": "000000000000000000000001",
          "email": "email@giscollective.com",
          "list": [{
            "newsletter": "000000000000000000000004"
          }, {
            "newsletter": "000000000000000000000001"
          }],
          "welcomeMessage": "000000000000000000000004",
        }`.parseJsonString);

        router
          .request
          .get("/stats/newsletter-open?newsletter=000000000000000000000001")
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "errors": [{
                "description": "You can't get this stat.",
                "status": 400,
                "title": "Validation error"
              }]
            }`.parseJsonString);
          });
      });
    });
  });
});
