/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.stats.MapViews;

import tests.fixtures;
import vibe.http.common;
import ogm.defaults.spaces;
import ogm.stats.api;
import ogm.stats.operations.MapViews;

alias suite = Spec!({
  URLRouter router;
  string aboutId;

  describe("get mapViews", {
    beforeEach({
      setupTestData();
      router = new URLRouter;

      router.crateSetup.setupStatsApi(crates);
    });

    it("returns 0 when there is no metric", {
      router
        .request
        .get("/stats/map-views")
        .expectStatusCode(200)
        .end((Response response) => () {
          auto expected = `{
            "stat": {
              "_id": "000000000000000000000001",
              "name": "map-views",
              "value": 0
            }
          }`.parseJsonString;

          expected["stat"]["lastUpdate"] = response.bodyJson["stat"]["lastUpdate"];

          response.bodyJson.should.equal(expected);
        });
    });

    describe("when there is a map view for a map id", {
      beforeEach({

        auto metric = Metric(
          ObjectId.fromString("000000000000000000000001"),
          "000000000000000000000002",
          "mapView",
          "",
          1);

        createMetric(metric);
      });

      it("returns 1 when there is a metric for a map", {
        router
          .request
          .get("/stats/map-views?map=000000000000000000000002")
          .expectStatusCode(200)
          .end((Response response) => () {
            auto expected = `{
              "stat": {
                "_id": "000000000000000000000001",
                "name": "map-views-map-000000000000000000000002",
                "value": 1
              }
            }`.parseJsonString;

            expected["stat"]["lastUpdate"] = response.bodyJson["stat"]["lastUpdate"];

            response.bodyJson.should.equal(expected);
          });
      });

      it("returns 0 when the campaign query param does not match a campaign", {
        router
          .request
          .get("/stats/map-views?map=000000000000000000000005")
          .expectStatusCode(200)
          .end((Response response) => () {
            auto expected = `{
              "stat": {
                "_id": "000000000000000000000001",
                "name": "map-views-map-000000000000000000000005",
                "value": 0
              }
            }`.parseJsonString;

            expected["stat"]["lastUpdate"] = response.bodyJson["stat"]["lastUpdate"];

            response.bodyJson.should.equal(expected);
          });
      });
    });

    describe("getStatValue", {
      MapViews mapViews;


      beforeEach({
        mapViews = new MapViews(crates);

        auto map3 = crates.map.getItem("000000000000000000000003").and.exec.front;
        map3["visibility"]["team"] = "000000000000000000000002";

        crates.map.updateItem(map3);
      });

      it("sums all the metric values when there is no map or team id", {
        auto metric = Metric(
          ObjectId.fromString("000000000000000000000001"),
          "000000000000000000000002",
          "mapView",
          "",
          1);

        createMetric(metric);

        metric = Metric(
          ObjectId.fromString("000000000000000000000002"),
          "000000000000000000000002",
          "mapView",
          "",
          2);

        createMetric(metric);
        auto value = mapViews.getStatValue(MapViews.Params("", ""));

        expect(value).to.equal(3);
      });

      it("sums all the metric values when there is a map id", {
        auto metric = Metric(
          ObjectId.fromString("000000000000000000000001"),
          "000000000000000000000002",
          "mapView",
          "",
          1);
        createMetric(metric);

        metric = Metric(
          ObjectId.fromString("000000000000000000000002"),
          "000000000000000000000002",
          "mapView",
          "",
          4);

        createMetric(metric);

        metric = Metric(
          ObjectId.fromString("000000000000000000000003"),
          "000000000000000000000001",
          "mapView",
          "",
          2);

        createMetric(metric);
        auto value = mapViews.getStatValue(MapViews.Params("000000000000000000000002", ""));

        expect(value).to.equal(5);
      });

      it("sums all the metric values when there is a team id", {
        auto metric = Metric(
          ObjectId.fromString("000000000000000000000001"),
          "000000000000000000000002",
          "mapView",
          "",
          1);
        createMetric(metric);

        metric = Metric(
          ObjectId.fromString("000000000000000000000002"),
          "000000000000000000000002",
          "mapView",
          "",
          4);

        createMetric(metric);

        metric = Metric(
          ObjectId.fromString("000000000000000000000003"),
          "000000000000000000000003",
          "mapView",
          "",
          2);

        createMetric(metric);

        metric = Metric(
          ObjectId.fromString("000000000000000000000003"),
          "000000000000000000000001",
          "mapView",
          "",
          2);

        createMetric(metric);
        auto value = mapViews.getStatValue(MapViews.Params("", "000000000000000000000002"));

        expect(value).to.equal(7);
      });
    });
  });
});
