/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.preferences.api;

import vibe.http.router;
import vibe.core.log;
import vibe.data.json;
import vibe.service.configuration.general;
import vibe.inet.url;

import crate.http.router;
import crate.base;

import ogm.auth;
import ogm.crates.all;

import ogm.middleware.userdata;
import ogm.middleware.adminrequest;
import ogm.filter.pagination;
import ogm.preferences.filter;

import std.conv;

///
void setupPreferenceApi(T)(CrateRouter!T crateRouter, OgmCrates crates) {
  auto auth = Authentication.instance(crates.user);
  auto userDataMiddleware = UserDataMiddleware.instance(crates);
  auto adminRequest = new AdminRequest(crates.user);
  auto preferenceFilter = new PreferenceFilter(auth.getCollection, crates);
  auto paginationFilter = new PaginationFilter;


  crateRouter
    .add(crates.preference,
      auth.identifiableContributionMiddleware,
      adminRequest,
      userDataMiddleware,
      preferenceFilter,
      paginationFilter);
}
