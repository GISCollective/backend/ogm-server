/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.preferences.filter;

import crate.auth.usercollection;
import crate.base;
import crate.error;
import crate.collection.memory;
import vibe.http.router;
import vibe.data.json;

import ogm.http.request;
import ogm.crates.all;
import ogm.http.request;

import std.array;
import std.algorithm;
import std.range;
import std.exception;
import std.conv;

///
class PreferenceFilter {
  private {
    UserCrateCollection users;
    OgmCrates crates;
  }

  struct Parameters {
    @describe("Set the name of the parameter that you want to receive.")
    @example("appearance.name", "Get the service name.")
    @example("appearance.logo", "Get the logo picture.")
    @example("appearance.cover", "Get the cover picture.")
    string name;
  }

  ///
  this(UserCrateCollection users, OgmCrates crates) {
    this.users = users;
    this.crates = crates;
  }

  /// Filter preferences by name
  @get
  IQuery get(IQuery selector, Parameters parameters, HTTPServerRequest req) {
    scope request = RequestUserData(req);

    if(parameters.name != "") {
      selector.where("name").equal(parameters.name);
    }

    if(!request.isAdmin) {
      selector.where("isSecret").equal(false);
    }

    return selector;
  }

  ///
  @delete_
  void delete_(HTTPServerRequest req) {
    throw new CrateValidationException("Preferences can not be removed.");
  }

  /// Middleware applied for create routes
  @create
  void create(HTTPServerRequest req) {
    throw new CrateValidationException("Preferences can not be added.");
  }

  /// Disable model changes
  @replace
  void replace(HTTPServerRequest req) {
    auto request = RequestUserData(req);

    enforce!CrateValidationException(request.isAdmin, "Preferences can not be updated by you.");
    enforce!CrateValidationException("preference" in req.json, "Preferences can not be updated by you.");

    auto originalItem = crates.preference.getItem(request.itemId).exec.front;
    originalItem["value"] = req.json["preference"]["value"];

    req.json["preference"] = originalItem;
  }

  /// Middleware applied for patch route
  @patch
  void patch(HTTPServerRequest, HTTPServerResponse) {
    throw new CrateNotFoundException("Not implemented.");
  }
}
