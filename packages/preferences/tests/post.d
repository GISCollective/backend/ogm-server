/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.preferences.post;

import tests.fixtures;
import vibe.http.common;
import ogm.crates.defaults;
import ogm.preferences.api;
import ogm.defaults.preferences;

alias suite = Spec!({
  URLRouter router;

  describe("The preference list", {
    Json data;

    beforeEach({
      setupTestData();

      PictureFileSettings.files = new MockGridFsFiles;
      PictureFileSettings.chunks = new MockGridFsChunks;
      crates.picture.addDefault("image/svg+xml", "logo", "../../deploy/default.png");
      crates.picture.addDefault("image/jpeg", "cover", "../../deploy/default.png");
      crates.picture.addDefault("image/jpeg", "default", "../../deploy/default.png");

      router = new URLRouter;
      router.crateSetup.setupPreferenceApi(crates);
      setupDefaultPreferences(crates, GeneralConfig());

      crates.preference.addItem(`{
        "_id": "1",
        "name": "property1",
        "value": "string value"
      }`.parseJsonString);

      crates.preference.addItem(`{
        "_id": "2",
        "name": "property2",
        "value": { "key": "object value" }
      }`.parseJsonString);

      data = `{"preference": {
        "name": "property3",
        "value": { "key": "object value" }
      }}`.parseJsonString;
    });

    it("should not add preferences when a token is not present", {
      router
        .request
        .post("/preferences")
        .send(data)
        .expectStatusCode(400)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"errors": [{
            "description": "Preferences can not be added.",
            "status": 400,
            "title": "Validation error"
          }]}`.parseJsonString);
        });
    });

    it("should not post preferences when a regular token is present", {
      router
        .request
        .post("/preferences")
        .send(data)
        .header("Authorization", "Bearer " ~ bearerToken.name)
        .expectStatusCode(400)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"errors": [{
              "description": "Preferences can not be added.",
              "status": 400,
              "title": "Validation error"
            }]}`.parseJsonString);
        });
    });

    it("should not post preferences with an admin token", {
      router
        .request
        .post("/preferences")
        .send(data)
        .header("Authorization", "Bearer " ~ administratorToken.name)
        .expectStatusCode(400)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"errors": [{
              "description": "Preferences can not be added.",
              "status": 400,
              "title": "Validation error"
            }]}`.parseJsonString);
        });
    });
  });
});
