/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.preferences.put;

import tests.fixtures;
import vibe.http.common;
import ogm.crates.defaults;
import ogm.preferences.api;
import ogm.defaults.preferences;

alias suite = Spec!({
  URLRouter router;

  describe("The preference list", {
    Json data;

    beforeEach({
      setupTestData();

      PictureFileSettings.files = new MockGridFsFiles;
      PictureFileSettings.chunks = new MockGridFsChunks;
      crates.picture.addDefault("image/svg+xml", "logo", "../../deploy/default.png");
      crates.picture.addDefault("image/jpeg", "cover", "../../deploy/default.png");
      crates.picture.addDefault("image/jpeg", "default", "../../deploy/default.png");

      router = new URLRouter;
      router.crateSetup.setupPreferenceApi(crates);
      setupDefaultPreferences(crates, GeneralConfig());

      crates.preference.addItem(`{
        "_id": "1",
        "name": "property1",
        "value": "string value"
      }`.parseJsonString);

      crates.preference.addItem(`{
        "_id": "2",
        "name": "property2",
        "value": { "key": "object value" }
      }`.parseJsonString);

      data = `{"preference": {
        "_id": "000000000000000000000002",
        "name": "new property",
        "value": { "key": "new object value" }
      }}`.parseJsonString;
    });

    it("should not update preferences when a token is not present", {
      router
        .request
        .put("/preferences/000000000000000000000002")
        .send(data)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "error": "Authorization required" }`.parseJsonString);
        });
    });

    it("should not update preferences when a regular token is present", {
      router
        .request
        .put("/preferences/000000000000000000000002")
        .send(data)
        .header("Authorization", "Bearer " ~ bearerToken.name)
        .expectStatusCode(400)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"errors": [{
              "description": "Preferences can not be updated by you.",
              "status": 400,
              "title": "Validation error"
            }]}`.parseJsonString);
        });
    });

    it("should update preferences with an admin token", {
      router
        .request
        .put("/preferences/000000000000000000000002")
        .send(data)
        .header("Authorization", "Bearer " ~ administratorToken.name)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "preference": {
              "_id": "000000000000000000000002",
                "isSecret": false,
                "value": { "key": "new object value" },
                "name": "appearance.logo"}}`.parseJsonString);
        });
    });

    it("should not be able to update the secret status of a preference with an admin token", {
      data["preference"]["isSecret"] = true;

      router
        .request
        .put("/preferences/000000000000000000000002")
        .send(data)
        .header("Authorization", "Bearer " ~ administratorToken.name)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "preference": {
              "_id": "000000000000000000000002",
              "isSecret": false,
              "value": { "key": "new object value" },
              "name": "appearance.logo"}}`.parseJsonString);
        });
    });
  });
});
