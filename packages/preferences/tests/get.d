/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.preferences.get;

import tests.fixtures;
import vibe.http.common;
import ogm.crates.defaults;
import ogm.preferences.api;
import ogm.defaults.preferences;

alias suite = Spec!({
  URLRouter router;

  describe("The preference list", {
    beforeEach({
      setupTestData();
      router = new URLRouter;

      PictureFileSettings.files = new MockGridFsFiles;
      PictureFileSettings.chunks = new MockGridFsChunks;
      crates.picture.addDefault("image/svg+xml", "logo", "../../deploy/default.png");
      crates.picture.addDefault("image/jpeg", "cover", "../../deploy/default.png");
      crates.picture.addDefault("image/jpeg", "default", "../../deploy/default.png");

      router.crateSetup.setupPreferenceApi(crates);
      setupDefaultPreferences(crates, GeneralConfig());

      crates.preference.addItem(`{
        "_id": "000000000000000000000001",
        "name": "property1",
        "isSecret": false,
        "value": "string value"
      }`.parseJsonString);

      crates.preference.addItem(`{
        "_id": "000000000000000000000002",
        "name": "property2",
        "isSecret": false,
        "value": { "key": "object value" }
      }`.parseJsonString);
    });

    describe("without an admin token", {
      it("should return the right CORS", {
        router
          .request
          .customMethod!(HTTPMethod.OPTIONS)("/preferences")
          .expectHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
          .expectStatusCode(204)
          .end;
      });

      it("should get all preferences", {
        router
          .request
          .get("/preferences")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["preferences"].length.should.equal(20);
          });
      });

      it("should get preferences by name", {
        router
          .request
          .get("/preferences?name=appearance.name")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "preferences": [{
                "_id": "000000000000000000000001",
                "value": "GISCollective",
                "isSecret": false,
                "name": "appearance.name"
              }]}`.parseJsonString);
          });
      });

      it("should not get a secret preferences by name", {
        router
          .request
          .get("/preferences?name=secret.smtp.authType")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "preferences": []}`.parseJsonString);
          });
      });

      it("should not get a secret preferences by id", {
        router
          .request
          .get("/preferences/000000000000000000000015")
          .expectStatusCode(404)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"errors\": [{
              \"description\": \"There is no item with id `000000000000000000000015`\",
              \"status\": 404,
              \"title\": \"Crate not found\"
            }]}".parseJsonString);
          });
      });

      it("should get preferences by name with an invalid ember cookie", {
        router
          .request
          .get("/preferences?name=appearance.name")
          .header("User-Agent", "something")
          .header("Cookie", "ember_simple_auth-session=%7B%22authenticated%22%3A%7B%22access_token%22%3A%22%22%7D%7D")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "preferences": [{
                "_id": "000000000000000000000001",
                "value": "GISCollective",
                "isSecret": false,
                "name": "appearance.name"
              }]}`.parseJsonString);
          });
      });
    });

    describe("with an admin token", {
      it("should be able to get a secret by name", {
        router
          .request
          .get("/preferences?name=secret.smtp.authType")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"preferences": [{
              "_id": "000000000000000000000013",
              "isSecret": true,
              "value": "none",
              "name": "secret.smtp.authType"
            }]}`.parseJsonString);
          });
      });
    });
  });
});
