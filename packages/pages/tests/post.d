/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.pages.post;

import tests.fixtures;
import vibe.http.common;
import ogm.defaults.pages;
import ogm.defaults.spaces;
import ogm.defaults.teams;
import ogm.defaults.articles;
import ogm.pages.api;

alias suite = Spec!({
  URLRouter router;

  describe("Creating pages", {
    Json page;

    beforeEach({
      setupTestData();
      router = new URLRouter;

      crates.setupDefaultTeams;
      crates.checkDefaultSpaces(GeneralConfig());
      crates.checkDefaultPages;

      GeneralConfig general;
      auto broadcast = new MemoryBroadcast;
      router.crateSetup.setupPageApi(crates, general, broadcast);
      setupDefaultPages(crates);
      page = `{ "page": {
        "name": "test",
        "slug": "test",
        "layout": "000000000000000000000001",
        "space": "000000000000000000000001",
        "cols": [],
        "visibility": {
          "team":"000000000000000000000001",
          "isPublic": true,
          "isDefault": false
        }
      }}`.parseJsonString;

      auto space = crates.space.getItem("000000000000000000000001").and.exec.front;
      space["visibility"]["team"] = "000000000000000000000001";
      crates.space.updateItem(space);

      auto team = crates.team.getItem("000000000000000000000001").and.exec.front;
      team["isPublisher"] = true;
      crates.team.updateItem(team);
    });

    describeCredentialsRule("create pages for publisher teams", "yes", "pages", ["administrator", "owner"], (string type) {
      auto initialSize = crates.page.get.size;

      router
        .request
        .post("/pages")
        .send(page)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          crates.page.get.size.should.equal(initialSize + 1);
        });
    });

    describeCredentialsRule("create pages for publisher teams", "no", "pages", ["leader", "member", "guest"], (string type) {
      auto initialSize = crates.page.get.size;

      router
        .request
        .post("/pages")
        .send(page)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "errors": [{
            "description": "You don't have enough rights to add an item.",
            "status": 403,
            "title": "Forbidden"
          }]}`.parseJsonString);
        });
    });

    describeCredentialsRule("create pages for publisher teams", "-", "pages", "no rights", {
      router
        .request
        .post("/pages")
        .send(page)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
        });
    });

    describe("for a non publisher team", {
      beforeEach({
        auto team = crates.team.getItem("000000000000000000000001").and.exec.front;
        team["isPublisher"] = false;
        crates.team.updateItem(team);
      });

      describeCredentialsRule("create pages for non publisher teams", "yes", "pages", ["administrator"], (string type) {
        auto initialSize = crates.page.get.size;

        router
          .request
          .post("/pages")
          .send(page)
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .end((Response response) => () {
            crates.page.get.size.should.equal(initialSize + 1);
          });
      });

      describeCredentialsRule("create pages for non publisher teams", "no", "pages", ["owner", "leader", "member", "guest"], (string type) {
        auto initialSize = crates.page.get.size;

        router
          .request
          .post("/pages")
          .send(page)
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{ "errors": [{
              "description": "Pages can be created only for publisher teams.",
              "status": 403,
              "title": "Forbidden"
            }]}`.parseJsonString);
          });
      });

      describeCredentialsRule("create pages for non publisher teams", "-", "pages", "no rights", {
        router
          .request
          .post("/pages")
          .send(page)
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
          });
      });
    });
  });
});
