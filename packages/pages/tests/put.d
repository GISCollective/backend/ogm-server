/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.pages.put;

import tests.fixtures;
import vibe.http.common;
import ogm.defaults.pages;
import ogm.defaults.spaces;
import ogm.defaults.teams;
import ogm.defaults.articles;
import ogm.pages.api;

alias suite = Spec!({
  URLRouter router;

  describe("Updating pages", {
    Json page;

    beforeEach({
      setupTestData();
      router = new URLRouter;

      crates.setupDefaultTeams;
      crates.checkDefaultSpaces(GeneralConfig());
      crates.checkDefaultPages;

      GeneralConfig general;
      auto broadcast = new MemoryBroadcast;
      router.crateSetup.setupPageApi(crates, general, broadcast);
      setupDefaultPages(crates);

      auto space = crates.space.getItem("000000000000000000000001").and.exec.front;
      space["visibility"]["team"] = "000000000000000000000001";
      crates.space.updateItem(space);

      page = `{ "page": {}}`.parseJsonString;

      auto storedPage = crates.page.getItem("000000000000000000000001").and.exec.front;
      storedPage["visibility"]["isPublic"] = true;
      storedPage["visibility"]["team"] = "000000000000000000000001";
      crates.page.updateItem(storedPage);

      auto storedSpace = crates.space.getItem("000000000000000000000001").and.exec.front;
      storedSpace["visibility"]["isPublic"] = true;
      storedSpace["visibility"]["team"] = "000000000000000000000001";
      crates.space.updateItem(storedSpace);

      page["page"] = storedPage.clone;
      page["page"]["name"] = "test";
    });

    it("returns an error when the page team is not the same as the space team", {
      page["page"]["visibility"]["team"] = "000000000000000000000003";

      router
        .request
        .put("/pages/000000000000000000000001")
        .send(page)
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(400)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "errors": [{
              "description": "The page and the space must have the same team.",
              "status": 400,
              "title": "Validation error"
            }]
          }`.parseJsonString);
        });
    });

    it("returns an error when the space property is not set", {
      page["page"]["space"] = Json();

      router
        .request
        .put("/pages/000000000000000000000001")
        .send(page)
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(400)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "errors": [{
              "description": "The 'space' property is missing.",
              "status": 400,
              "title": "Validation error"
            }]
          }`.parseJsonString);
        });
    });

    describeCredentialsRule("update team pages", "yes", "pages", ["administrator", "owner"], (string type) {
      auto initialSize = crates.page.get.size;

      router
        .request
        .put("/pages/000000000000000000000001")
        .send(page)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          crates.page.get.size.should.equal(initialSize);
          response.bodyJson["page"]["name"].to!string.should.equal("test");
        });
    });

    describeCredentialsRule("update team pages", "no", "pages", [ "leader", "member", "guest"], (string type) {
      auto initialSize = crates.page.get.size;

      router
        .request
        .put("/pages/000000000000000000000001")
        .send(page)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{ "errors": [{
            "description": "You don't have enough rights to edit ` ~ "`000000000000000000000001`" ~ `.",
            "status": 403,
            "title": "Forbidden"
          }]}`).parseJsonString);
        });
    });

    describeCredentialsRule("update team pages", "-", "pages", "no rights", {
      router
        .request
        .put("/pages/000000000000000000000001")
        .send(page)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
        });
    });

    describeCredentialsRule("update own pages", "yes", "pages", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      auto editablePage = crates.page.getItem("000000000000000000000001").and.exec.front;
      editablePage["info"]["originalAuthor"] = userId[type];
      crates.page.updateItem(editablePage);

      auto initialSize = crates.page.get.size;

      router
        .request
        .put("/pages/000000000000000000000001")
        .send(page)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          crates.page.get.size.should.equal(initialSize);
          response.bodyJson["page"]["name"].to!string.should.equal("test");
        });
    });

    describeCredentialsRule("update own pages", "-", "pages", "no rights", {});
  });
});
