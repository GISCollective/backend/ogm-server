/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.pages.getitem;

import tests.fixtures;
import vibe.http.common;
import ogm.defaults.pages;
import ogm.defaults.spaces;
import ogm.defaults.teams;
import ogm.defaults.articles;
import ogm.pages.api;

alias suite = Spec!({
  URLRouter router;
  string aboutId;

  describe("get pages", {
    beforeEach({
      setupTestData();
      router = new URLRouter;

      crates.setupDefaultTeams;
      crates.checkDefaultSpaces(GeneralConfig());
      crates.checkDefaultPages;

      GeneralConfig general;

      auto broadcast = new MemoryBroadcast;
      router.crateSetup.setupPageApi(crates, general, broadcast);
      setupDefaultPages(crates);

      aboutId = crates.page.get.where("slug").equal("about").and.exec.front["_id"].to!string;

      auto page = crates.page.getItem("000000000000000000000001").and.exec.front;
      page["visibility"]["isPublic"] = true;
      page["visibility"]["team"] = "000000000000000000000001";
      crates.page.updateItem(page);

      auto space = crates.space.getItem("000000000000000000000001").and.exec.front;
      space["visibility"]["team"] = "000000000000000000000001";
      crates.space.updateItem(space);
    });

    describeCredentialsRule("get public page by id", "yes", "pages", ["administrator", "owner"], (string type) {
      auto initialSize = crates.page.get.size;

      router
        .request
        .get("/pages/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["page"]["_id"].should.equal("000000000000000000000001");
          response.bodyJson["page"]["canEdit"].to!bool.should.equal(true);
        });
    });

    describeCredentialsRule("get public page by id", "yes", "pages", ["leader", "member", "guest"], (string type) {
      auto initialSize = crates.page.get.size;

      router
        .request
        .get("/pages/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["page"]["_id"].should.equal("000000000000000000000001");
          response.bodyJson["page"]["canEdit"].to!bool.should.equal(false);
        });
    });

    describeCredentialsRule("get public page by id", "yes", "pages", "no rights", {
      router
        .request
        .get("/pages/000000000000000000000001?team=000000000000000000000001")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["page"]["_id"].should.equal("000000000000000000000001");
          response.bodyJson["page"]["canEdit"].to!bool.should.equal(false);
        });
    });

    describeCredentialsRule("get public page by slug", "yes", "pages", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      auto page = crates.page.getItem("000000000000000000000006").and.exec.front;
      page["visibility"]["isPublic"] = true;
      page["visibility"]["team"] = "000000000000000000000001";
      crates.page.updateItem(page);

      auto initialSize = crates.page.get.size;

      router
        .request
        .get("/pages/about?team=000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["page"]["_id"].should.equal(aboutId);
        });
    });

    describeCredentialsRule("get public page by slug", "yes", "pages", "no rights", {
      auto page = crates.page.getItem("000000000000000000000006").and.exec.front;
      page["visibility"]["isPublic"] = true;
      page["visibility"]["team"] = "000000000000000000000001";
      crates.page.updateItem(page);

      router
        .request
        .get("/pages/about?team=000000000000000000000001")
        .expectStatusCode(200)
        .end((Response response) => () {
          import std.stdio;
          writeln(response.bodyJson);
          response.bodyJson["page"]["_id"].should.equal(aboutId);
        });
    });

    describe("when there is a private page", {
      beforeEach({
        auto page = crates.page.getItem("000000000000000000000001").and.exec.front;
        page["visibility"]["isPublic"] = false;
        page["visibility"]["team"] = "000000000000000000000001";

        crates.page.updateItem(page);
      });

      describeCredentialsRule("get private pages", "yes", "pages", ["administrator", "owner", "leader", "member", "guest"], (string type) {
        router
          .request
          .get("/pages/000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["page"]["_id"].should.equal("000000000000000000000001");
          });
      });

      describeCredentialsRule("get private pages", "no", "pages", "no rights", {
        router
          .request
          .get("/pages/000000000000000000000001")
          .expectStatusCode(404);
      });
    });

    describe("for a page with rows and cols with options", {
       beforeEach({
        auto page = crates.page.getItem("000000000000000000000001").and.exec.front.clone;
        page["visibility"]["isPublic"] = false;
        page["visibility"]["team"] = "000000000000000000000001";

        auto container = LayoutContainer();
        container.rows ~= LayoutRow();
        container.rows ~= LayoutRow();
        container.rows[0].cols ~= LayoutCol();

        container.rows[0].options = [ "pb-1", "pb-2" ];
        container.rows[0].data = `{
          "container": {
            "classes": [ "pb-2" ]
          }
        }`.parseJsonString;

        container.rows[0].cols[0].options = [ "pb-1", "pb-2" ];
        container.rows[0].cols[0].data = `{
          "container": {
            "classes": [ "pb-2" ]
          }
        }`.parseJsonString;

        page["containers"] = [container].serializeToJson;

        crates.page.updateItem(page);
      });

      it("moves the cols options to the container classes", {
        router
          .request
          .get("/pages/000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList["owner"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["page"]["containers"][0]["rows"][0]["cols"][0].should.equal(`{
              "type":"",
              "componentCount":0,
              "options":[],
              "data":{"container":{"classes":["pb-1","pb-2"]}},
              "name":""
            }`.parseJsonString);
          });
      });

      it("moves the rows options to the container classes", {
        router
          .request
          .get("/pages/000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList["owner"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["page"]["containers"][0]["rows"][0]["data"].should.equal(`{
              "container":{"classes":["pb-1","pb-2"]}
            }`.parseJsonString);
          });
      });

      it("removes layout gids when the space does not have one", {
        auto page = crates.page.getItem("000000000000000000000001").and.exec.front.clone;
        page["layoutContainers"] = page["containers"];
        page["layoutContainers"][0]["gid"] = "some missing gid";

        crates.page.updateItem(page);

        router
          .request
          .get("/pages/000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList["owner"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["page"]["layoutContainers"][0]["gid"].to!string.should.equal(``);
          });
      });
    });
  });
});
