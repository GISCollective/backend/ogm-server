/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.pages.operations;

import tests.fixtures;
import vibe.http.common;
import ogm.defaults.pages;
import ogm.defaults.spaces;
import ogm.defaults.pages;
import ogm.defaults.teams;
import ogm.pages.api;

alias suite = Spec!({
  URLRouter router;

  describe("operations", {
    string aboutId;
    auto broadcast = new MemoryBroadcast;

    beforeEach({
      setupTestData();
      router = new URLRouter;

      crates.setupDefaultTeams;
      crates.checkDefaultSpaces(GeneralConfig());
      crates.checkDefaultPages;

      GeneralConfig general;
      router.crateSetup.setupPageApi(crates, general, broadcast);

      aboutId = crates.page.get.where("slug").equal("about").and.exec.front["_id"].to!string;

      auto page = crates.page.getItem("000000000000000000000001").and.exec.front;
      page["visibility"]["isPublic"] = true;
      page["visibility"]["team"] = "000000000000000000000001";
      crates.page.updateItem(page);

      auto changeSet = `{
          "model": "Page",
          "author": "000000000000000000000001",
          "itemId": "000000000000000000000001",
          "time": "2010-04-05T12:33:22Z",
          "type": "add",
          "added": {
            "_id": "000000000000000000000001",
            "name": "page name",
            "slug": "page slug"
          }
        }`.parseJsonString;

      crates.changeSet.addItem(changeSet);

      auto space = crates.space.getItem("000000000000000000000001").and.exec.front;
      space["visibility"]["team"] = "000000000000000000000001";
      crates.space.updateItem(space);
    });

    it("can get the list of revisions", {
      router
        .request
        .get("/pages/000000000000000000000001/revisions")
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "revisions": [{
              "time": "2010-04-05T12:33:22Z",
              "id": "000000000000000000000001",
              "url": "http://localhost:9091/pages/000000000000000000000001/revisions/000000000000000000000001"
            }]}`.parseJsonString);
        });
    });

    it("can get a revision", {
      router
        .request
        .get("/pages/000000000000000000000001/revisions/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "page": {
              "_id": "000000000000000000000001",
              "visibility": {},
              "canEdit": false,
              "name": "page name",
              "space": "000000000000000000000001",
              "slug": "page slug"
            }}`.parseJsonString);
        });
    });

    describe("the contact operation", {
      it("can trigger a contact message", {
        auto page = crates.page.getItem("000000000000000000000001").and.exec.front;
        page["cols"] = `[{
          "name": "contact",
          "data": {
            "form": {
              "sendTo": "a@gmail.com"
            }
          }
        }]`.parseJsonString;

        crates.page.updateItem(page);

        auto broadcastValue = Json.emptyObject;
        void testHandler(const Json value) @trusted {
          broadcastValue = value;
        }

        broadcast.register("page.contact", &testHandler);

        auto data = Json.emptyObject;
        data["message"] = `{ "other": "", "colName": "contact", "name": "some name", "email": "me@giscollective.com", "subject": "my subject", "message": "my message" }`.parseJsonString;

        router
          .request
          .post("/pages/000000000000000000000001/contact")
          .header("Authorization", "Bearer " ~ userTokenList["owner"])
          .send(data)
          .expectStatusCode(201)
          .end((Response response) => () {
            broadcastValue.should.equal(`{ "sendTo": "a@gmail.com", "name": "some name", "email": "me@giscollective.com", "subject": "my subject", "message": "my message" }`.parseJsonString);
          });
      });

      it("uses the team email when send to is an empty string", {
        auto page = crates.page.getItem("000000000000000000000001").and.exec.front;
        page["cols"] = `[{
          "name": "contact",
          "data": {
            "form": {
              "sendTo": ""
            }
          }
        }]`.parseJsonString;

        crates.page.updateItem(page);

        auto broadcastValue = Json.emptyObject;
        void testHandler(const Json value) @trusted {
          broadcastValue = value;
        }

        broadcast.register("page.contact", &testHandler);

        auto data = Json.emptyObject;
        data["message"] = `{ "other": "", "colName": "contact", "name": "some name", "email": "me@giscollective.com", "subject": "my subject", "message": "my message" }`.parseJsonString;

        router
          .request
          .post("/pages/000000000000000000000001/contact")
          .header("Authorization", "Bearer " ~ userTokenList["owner"])
          .send(data)
          .expectStatusCode(201)
          .end((Response response) => () {
            broadcastValue.should.equal(`{ "sendTo": "test1@gmail.com", "name": "some name", "email": "me@giscollective.com", "subject": "my subject", "message": "my message" }`.parseJsonString);
          });
      });

      it("returns undefined when the message is not set", {
        auto broadcastValue = Json.emptyObject;
        void testHandler(const Json value) @trusted {
          broadcastValue = value;
        }

        broadcast.register("page.contact", &testHandler);

        auto data = Json.emptyObject;

        router
          .request
          .post("/pages/000000000000000000000001/contact")
          .header("Authorization", "Bearer " ~ userTokenList["owner"])
          .send(data)
          .expectStatusCode(400)
          .end((Response response) => () {
            broadcastValue.should.equal(`{}`.parseJsonString);
          });
      });

      it("returns an error when there is no body", {
        Json broadcastValue;
        void testHandler(const Json value) @trusted {
          broadcastValue = value;
        }

        broadcast.register("page.contact", &testHandler);

        auto data = Json.emptyObject;

        router
          .request
          .post("/pages/000000000000000000000001/contact")
          .header("Authorization", "Bearer " ~ userTokenList["owner"])
          .expectStatusCode(400)
          .end((Response response) => () {
            broadcastValue.type.to!string.should.equal("undefined");
            response.bodyJson.should.equal(`{
              "errors": [{
                "description": "You must send an object with the 'message' property.",
                "status": 400,
                "title": "Validation error"
              }]
            }`.parseJsonString);
          });
      });
    });

    describe("bulk delete", {
      MemoryBroadcast broadcast;

      describeCredentialsRule("delete pages from own team", "yes", "pages", ["administrator", "owner"], (string type) {
        router
          .request
          .delete_("/pages?team=000000000000000000000001&space=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(204)
          .end;

        crates.page.get.exec.map!(a => a["visibility"]["team"]).array.sort.uniq.should.equal(["000000000000000000000006"]);
      });

      describeCredentialsRule("delete pages from own team", "no", "pages", ["leader", "member", "guest"], (string type) {
        router
          .request
          .delete_("/pages?team=000000000000000000000001&space=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(204)
          .end;

        crates.page.get.exec.map!(a => a["visibility"]["team"]).array.sort.uniq.should.equal(["000000000000000000000001", "000000000000000000000006"]);
      });

      describeCredentialsRule("delete pages from own team", "no", "pages", "no rights", {
        router
          .request
          .delete_("/pages?team=000000000000000000000001&space=000000000000000000000001")
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "error": "Authorization required"
            }`.parseJsonString);
          });
      });


      describeCredentialsRule("delete own pages", "yes", "pages", ["administrator", "owner", "leader", "member"], (string type) {
        auto pages = crates.page.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

        foreach(page; pages) {
          page["info"]["originalAuthor"] = userId[type];
          crates.page.updateItem(page);
        }

        router
          .request
          .delete_("/pages?team=000000000000000000000001&space=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(204)
          .end;

        crates.page.get.exec.map!(a => a["visibility"]["team"]).array.sort.uniq.should.equal(["000000000000000000000006"]);
      });

      describeCredentialsRule("delete own pages", "no", "pages", ["guest"], (string type) {
        auto pages = crates.page.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

        foreach(page; pages) {
          page["info"]["originalAuthor"] = userId[type];
          crates.page.updateItem(page);
        }

        router
          .request
          .delete_("/pages?team=000000000000000000000001&space=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(204)
          .end;

        crates.page.get.exec.map!(a => a["visibility"]["team"]).array.sort.uniq.should.equal(["000000000000000000000001", "000000000000000000000006"]);
      });

      describeCredentialsRule("delete own pages", "no", "pages", "no rights", { });


      it("does not delete a page when it does not match the space", {
        auto pages = crates.page.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

        router
          .request
          .delete_("/pages?team=000000000000000000000001&space=000000000000000000000010")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(204)
          .end;

        crates.page.get.exec.map!(a => a["visibility"]["team"]).array.sort.uniq.should.equal(["000000000000000000000001", "000000000000000000000006"]);
      });

      it("only deletes a page when it matches the space", {
        auto page = crates.page.getItem("000000000000000000000001").and.exec.front.clone;
        page["space"] = "000000000000000000000002";

        crates.page.addItem(page);

        auto pages = crates.page.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

        router
          .request
          .delete_("/pages?team=000000000000000000000001&space=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(204)
          .end;

        crates.page.get.exec.map!(a => a["visibility"]["team"]).array.sort.uniq.should.equal(["000000000000000000000001", "000000000000000000000006"]);
        crates.page.get.exec.map!(a => a["_id"]).array.sort.uniq.should.equal(["000000000000000000000002", "000000000000000000000003", "000000000000000000000004", "000000000000000000000005", "000000000000000000000006", "000000000000000000000007"]);
      });
    });

    describe("publish", {
      MemoryBroadcast broadcast;

      beforeEach({
        auto page = crates.page.getItem("000000000000000000000001").exec.front.clone;
        page["visibility"]["isPublic"] = false;
        page["visibility"]["team"] = "000000000000000000000001";

        crates.page.updateItem(page);
      });

      describeCredentialsRule("publish own pages", "yes", "pages", ["administrator", "owner", "leader", "member"], (string type) {
        auto pages = crates.page.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

        foreach(page; pages) {
          page["info"]["originalAuthor"] = userId[type];
          crates.page.updateItem(page);
        }

        router
          .request
          .post("/pages/000000000000000000000001/publish")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(204)
          .end;

        auto page = crates.page.getItem("000000000000000000000001").exec.front.clone;
        page["visibility"]["isPublic"].to!bool.should.equal(true);
      });

      describeCredentialsRule("publish own pages", "no", "pages", ["guest"], (string type) {
        auto pages = crates.page.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

        foreach(page; pages) {
          page["info"]["originalAuthor"] = userId[type];
          crates.page.updateItem(page);
        }

        router
          .request
          .post("/pages/000000000000000000000001/publish")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(403)
          .end;

        auto page = crates.page.getItem("000000000000000000000001").exec.front.clone;
        page["visibility"]["isPublic"].to!bool.should.equal(false);
      });

      describeCredentialsRule("publish own pages", "no", "pages", "no rights", { });


      describeCredentialsRule("publish pages from other teams", "yes", "pages", ["administrator"], (string type) {
        auto pages = crates.page.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

        foreach(page; pages) {
          page["info"]["originalAuthor"] = userId[type];
          page["visibility"]["team"] = "000000000000000000000004";
          crates.page.updateItem(page);
        }

        router
          .request
          .post("/pages/000000000000000000000001/publish")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(204)
          .end;

        auto page = crates.page.getItem("000000000000000000000001").exec.front.clone;
        page["visibility"]["isPublic"].to!bool.should.equal(true);
      });

      describeCredentialsRule("publish pages from other teams", "no", "pages", ["owner", "leader", "member", "guest"], (string type) {
        auto pages = crates.page.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec;

        foreach(page; pages) {
          page["info"]["originalAuthor"] = userId[type];
          page["visibility"]["team"] = "000000000000000000000004";
          crates.page.updateItem(page);
        }

        router
          .request
          .post("/pages/000000000000000000000001/publish")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(404)
          .end;

        auto page = crates.page.getItem("000000000000000000000001").exec.front.clone;
        page["visibility"]["isPublic"].to!bool.should.equal(false);
      });

      describeCredentialsRule("publish pages from other teams", "no", "pages", "no rights", { });
    });

    describe("unpublish", {
      MemoryBroadcast broadcast;

      beforeEach({
        auto page = crates.page.getItem("000000000000000000000001").exec.front.clone;
        page["visibility"]["isPublic"] = true;
        page["visibility"]["team"] = "000000000000000000000001";

        crates.page.updateItem(page);
      });

      describeCredentialsRule("unpublish own pages", "yes", "pages", ["administrator", "owner", "leader", "member"], (string type) {
        auto pages = crates.page.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

        foreach(page; pages) {
          page["info"]["originalAuthor"] = userId[type];
          crates.page.updateItem(page);
        }

        router
          .request
          .post("/pages/000000000000000000000001/unpublish")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(204)
          .end;

        auto page = crates.page.getItem("000000000000000000000001").exec.front.clone;
        page["visibility"]["isPublic"].to!bool.should.equal(false);
      });

      describeCredentialsRule("unpublish own pages", "no", "pages", ["guest"], (string type) {
        auto pages = crates.page.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

        foreach(page; pages) {
          page["info"]["originalAuthor"] = userId[type];
          crates.page.updateItem(page);
        }

        router
          .request
          .post("/pages/000000000000000000000001/unpublish")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(403)
          .end;

        auto page = crates.page.getItem("000000000000000000000001").exec.front.clone;
        page["visibility"]["isPublic"].to!bool.should.equal(true);
      });

      describeCredentialsRule("unpublish own pages", "no", "pages", "no rights", { });


      describeCredentialsRule("unpublish pages from other teams", "yes", "pages", ["administrator"], (string type) {
        auto pages = crates.page.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

        foreach(page; pages) {
          page["info"]["originalAuthor"] = userId[type];
          page["visibility"]["team"] = "000000000000000000000004";
          crates.page.updateItem(page);
        }

        router
          .request
          .post("/pages/000000000000000000000001/unpublish")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(204)
          .end;

        auto page = crates.page.getItem("000000000000000000000001").exec.front.clone;
        page["visibility"]["isPublic"].to!bool.should.equal(false);
      });

      describeCredentialsRule("unpublish pages from other teams", "no", "pages", ["owner", "leader", "member", "guest"], (string type) {
        auto pages = crates.page.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

        foreach(page; pages) {
          page["info"]["originalAuthor"] = userId[type];
          page["visibility"]["team"] = "000000000000000000000004";
          crates.page.updateItem(page);
        }

        router
          .request
          .post("/pages/000000000000000000000001/unpublish")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(403)
          .end;

        auto page = crates.page.getItem("000000000000000000000001").exec.front.clone;
        page["visibility"]["isPublic"].to!bool.should.equal(true);
      });

      describeCredentialsRule("unpublish pages from other teams", "no", "pages", "no rights", { });
    });

    describe("bulk publish", {
      MemoryBroadcast broadcast;

      beforeEach({
        auto page = crates.page.getItem("000000000000000000000001").exec.front.clone;
        page["visibility"]["isPublic"] = false;
        page["visibility"]["team"] = "000000000000000000000001";

        crates.page.updateItem(page);
      });

      describeCredentialsRule("bulk publish pages from own team", "yes", "pages", ["administrator", "owner"], (string type) {
        router
          .request
          .post("/pages/publish?team=000000000000000000000001&space=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(204)
          .end;

        auto page = crates.page.getItem("000000000000000000000001").exec.front.clone;

        page["visibility"]["isPublic"].to!bool.should.equal(true);
      });

      describeCredentialsRule("bulk publish pages from own team", "no", "pages", ["leader", "member", "guest"], (string type) {
        router
          .request
          .post("/pages/publish?team=000000000000000000000001&space=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(204)
          .end;

        auto page = crates.page.getItem("000000000000000000000001").exec.front.clone;

        page["visibility"]["isPublic"].to!bool.should.equal(false);
      });

      describeCredentialsRule("bulk publish pages from own team", "no", "pages", "no rights", {
        router
          .request
          .post("/pages/publish?team=000000000000000000000001&space=000000000000000000000001")
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "error": "Authorization required"
            }`.parseJsonString);
          });
      });


      describeCredentialsRule("bulk publish own pages", "yes", "pages", ["administrator", "owner", "leader", "member"], (string type) {
        auto pages = crates.page.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

        foreach(page; pages) {
          page["info"]["originalAuthor"] = userId[type];
          crates.page.updateItem(page);
        }

        router
          .request
          .post("/pages/publish?team=000000000000000000000001&space=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(204)
          .end;

        auto page = crates.page.getItem("000000000000000000000001").exec.front.clone;
        page["visibility"]["isPublic"].to!bool.should.equal(true);
      });

      describeCredentialsRule("bulk publish own pages", "no", "pages", ["guest"], (string type) {
        auto pages = crates.page.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

        foreach(page; pages) {
          page["info"]["originalAuthor"] = userId[type];
          crates.page.updateItem(page);
        }

        router
          .request
          .post("/pages/publish?team=000000000000000000000001&space=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(204)
          .end;

        auto page = crates.page.getItem("000000000000000000000001").exec.front.clone;
        page["visibility"]["isPublic"].to!bool.should.equal(false);
      });

      describeCredentialsRule("bulk publish own pages", "no", "pages", "no rights", { });


      describeCredentialsRule("bulk publish pages from other teams", "yes", "pages", ["administrator"], (string type) {
        auto pages = crates.page.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

        foreach(page; pages) {
          page["info"]["originalAuthor"] = userId[type];
          page["visibility"]["team"] = "000000000000000000000004";
          crates.page.updateItem(page);
        }

        router
          .request
          .post("/pages/publish?team=000000000000000000000004&space=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(204)
          .end;

        auto page = crates.page.getItem("000000000000000000000001").exec.front.clone;
        page["visibility"]["isPublic"].to!bool.should.equal(true);
      });

      describeCredentialsRule("bulk publish pages from other teams", "no", "pages", ["owner", "leader", "member", "guest"], (string type) {
        auto pages = crates.page.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

        foreach(page; pages) {
          page["info"]["originalAuthor"] = userId[type];
          page["visibility"]["team"] = "000000000000000000000004";
          crates.page.updateItem(page);
        }

        router
          .request
          .post("/pages/publish?team=000000000000000000000004&space=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(404)
          .end;

        auto page = crates.page.getItem("000000000000000000000001").exec.front.clone;
        page["visibility"]["isPublic"].to!bool.should.equal(false);
      });

      describeCredentialsRule("bulk publish pages from other teams", "no", "pages", "no rights", { });
    });

    describe("bulk unpublish", {
      MemoryBroadcast broadcast;

      beforeEach({
        auto page = crates.page.getItem("000000000000000000000001").exec.front.clone;
        page["visibility"]["isPublic"] = true;
        page["visibility"]["team"] = "000000000000000000000001";

        crates.page.updateItem(page);
      });

      describeCredentialsRule("bulk unpublish pages from own team", "yes", "pages", ["administrator", "owner"], (string type) {
        router
          .request
          .post("/pages/unpublish?team=000000000000000000000001&space=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(204)
          .end;

        auto page = crates.page.getItem("000000000000000000000001").exec.front.clone;

        page["visibility"]["isPublic"].to!bool.should.equal(false);
      });

      describeCredentialsRule("bulk unpublish pages from own team", "no", "pages", ["leader", "member", "guest"], (string type) {
        router
          .request
          .post("/pages/unpublish?team=000000000000000000000001&space=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(204)
          .end;

        auto page = crates.page.getItem("000000000000000000000001").exec.front.clone;

        page["visibility"]["isPublic"].to!bool.should.equal(true);
      });

      describeCredentialsRule("bulk unpublish pages from own team", "no", "pages", "no rights", {
        router
          .request
          .post("/pages/unpublish?team=000000000000000000000001&space=000000000000000000000001")
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "error": "Authorization required"
            }`.parseJsonString);
          });
      });


      describeCredentialsRule("bulk unpublish own pages", "yes", "pages", ["administrator", "owner", "leader", "member"], (string type) {
        auto pages = crates.page.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

        foreach(page; pages) {
          page["info"]["originalAuthor"] = userId[type];
          crates.page.updateItem(page);
        }

        router
          .request
          .post("/pages/unpublish?team=000000000000000000000001&space=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(204)
          .end;

        auto page = crates.page.getItem("000000000000000000000001").exec.front.clone;
        page["visibility"]["isPublic"].to!bool.should.equal(false);
      });

      describeCredentialsRule("bulk unpublish own pages", "no", "pages", ["guest"], (string type) {
        auto pages = crates.page.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

        foreach(page; pages) {
          page["info"]["originalAuthor"] = userId[type];
          crates.page.updateItem(page);
        }

        router
          .request
          .post("/pages/unpublish?team=000000000000000000000001&space=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(204)
          .end;

        auto page = crates.page.getItem("000000000000000000000001").exec.front.clone;
        page["visibility"]["isPublic"].to!bool.should.equal(true);
      });

      describeCredentialsRule("bulk unpublish own pages", "no", "pages", "no rights", { });


      describeCredentialsRule("bulk unpublish pages from other teams", "yes", "pages", ["administrator"], (string type) {
        auto pages = crates.page.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

        foreach(page; pages) {
          page["info"]["originalAuthor"] = userId[type];
          page["visibility"]["team"] = "000000000000000000000004";
          crates.page.updateItem(page);
        }

        router
          .request
          .post("/pages/unpublish?team=000000000000000000000004&space=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(204)
          .end;

        auto page = crates.page.getItem("000000000000000000000001").exec.front.clone;
        page["visibility"]["isPublic"].to!bool.should.equal(false);
      });

      describeCredentialsRule("bulk unpublish pages from other teams", "no", "pages", ["owner", "leader", "member", "guest"], (string type) {
        auto pages = crates.page.get.where("visibility.team").equal(ObjectId("000000000000000000000001")).and.exec.map!(a => a.clone);

        foreach(page; pages) {
          page["info"]["originalAuthor"] = userId[type];
          page["visibility"]["team"] = "000000000000000000000004";
          crates.page.updateItem(page);
        }

        router
          .request
          .post("/pages/unpublish?team=000000000000000000000004&space=000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(404)
          .end;

        auto page = crates.page.getItem("000000000000000000000001").exec.front.clone;
        page["visibility"]["isPublic"].to!bool.should.equal(true);
      });

      describeCredentialsRule("bulk unpublish pages from other teams", "no", "pages", "no rights", { });
    });
  });
});
