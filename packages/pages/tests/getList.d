/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.pages.getlist;

import tests.fixtures;
import vibe.http.common;
import ogm.defaults.pages;
import ogm.defaults.spaces;
import ogm.defaults.articles;
import ogm.defaults.teams;
import std.algorithm;
import ogm.pages.api;

alias suite = Spec!({
  URLRouter router;

  describe("Get page list", {
    beforeEach({
      setupTestData();
      router = new URLRouter;

      crates.setupDefaultTeams;
      crates.checkDefaultSpaces(GeneralConfig());
      crates.checkDefaultPages;

      GeneralConfig general;
      auto broadcast = new MemoryBroadcast;
      router.crateSetup.setupPageApi(crates, general, broadcast);
      setupDefaultPages(crates);

      auto page = crates.page.getItem("000000000000000000000001").and.exec.front;
      page["visibility"]["isPublic"] = true;

      crates.page.updateItem(page);

      auto space = crates.space.getItem("000000000000000000000001").and.exec.front;
      space["visibility"]["team"] = "000000000000000000000001";
      crates.space.updateItem(space);
    });

    describeCredentialsRule("get the public page list", "yes", "pages", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      auto initialSize = crates.page.get.size;

      router
        .request
        .get("/pages")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["pages"].byValue.map!(a => a["_id"].to!string).should.contain("000000000000000000000001");
        });
    });

    describeCredentialsRule("get the public page list", "yes", "pages", "no rights", {
      router
        .request
        .get("/pages")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["pages"].byValue.map!(a => a["_id"].to!string).should.contain("000000000000000000000001");
        });
    });

    describe("when there is a private page", {
      beforeEach({
        setupTestData();
        router = new URLRouter;

        crates.setupDefaultTeams;
        crates.checkDefaultSpaces(GeneralConfig());
        crates.checkDefaultPages;

        GeneralConfig general;
        auto broadcast = new MemoryBroadcast;

        router.crateSetup.setupPageApi(crates, general, broadcast);
        setupDefaultPages(crates);

        auto page = crates.page.getItem("000000000000000000000001").and.exec.front;
        page["visibility"]["isPublic"] = false;
        page["visibility"]["team"] = "000000000000000000000001";

        crates.page.updateItem(page);
      });

      describeCredentialsRule("get the private page list", "yes", "pages", ["administrator", "owner", "leader", "member", "guest"], (string type) {
        auto initialSize = crates.page.get.size;

        router
          .request
          .get("/pages")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["pages"].byValue.map!(a => a["_id"].to!string).should.contain("000000000000000000000001");
          });
      });

      describeCredentialsRule("get the private page list", "no", "pages", "no rights", {
        router
          .request
          .get("/pages")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["pages"].byValue.map!(a => a["_id"].to!string).should.not.contain("000000000000000000000001");
          });
      });

      describeCredentialsRule("get the own page list", "yes", "pages", ["administrator", "owner", "leader", "member", "guest"], (string type) {
        auto editablePage = crates.page.getItem("000000000000000000000001").and.exec.front;
        editablePage["info"]["originalAuthor"] = userId[type];
        crates.page.updateItem(editablePage);

        router
          .request
          .get("/pages")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["pages"].byValue.map!(a => a["_id"].to!string).should.contain("000000000000000000000001");
          });
      });

      describeCredentialsRule("get the own page list", "-", "pages", "no rights", {});
    });

    it("returns no page when the space query param does not match any pages", {
      router
        .request
        .get("/pages?space=000000000000000000000005")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["pages"].byValue.map!(a => a["_id"].to!string).array.should.equal([]);
        });
    });
  });
});
