/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.pages.delete_;

import tests.fixtures;
import vibe.http.common;

import ogm.pages.api;
import ogm.defaults.pages;
import ogm.defaults.articles;
import ogm.defaults.spaces;
import ogm.defaults.teams;

alias suite = Spec!({
  URLRouter router;

  describe("Deleting pages", {
    beforeEach({
      setupTestData();
      router = new URLRouter;

      crates.setupDefaultTeams;
      crates.checkDefaultSpaces(GeneralConfig());
      crates.checkDefaultPages;

      auto broadcast = new MemoryBroadcast;

      GeneralConfig general;
      router.crateSetup.setupPageApi(crates, general, broadcast);
      setupDefaultPages(crates);

      auto page = crates.page.getItem("000000000000000000000001").and.exec.front;
      page["visibility"]["isPublic"] = true;
      page["visibility"]["team"] = "000000000000000000000001";
      crates.page.updateItem(page);

      auto space = crates.space.getItem("000000000000000000000001").and.exec.front;
      space["visibility"]["team"] = "000000000000000000000001";
      crates.space.updateItem(space);
    });

    describeCredentialsRule("delete any team pages", "yes", "pages", ["administrator", "owner"], (string type) {
      auto initialSize = crates.page.get.size;

      router
        .request
        .delete_("/pages/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end((Response response) => () {
          crates.page.get.size.should.equal(initialSize - 1);
          crates.page.get.where("_id").equal(ObjectId.fromString("000000000000000000000001")).and.exec.empty.should.equal(true);
        });
    });

    describeCredentialsRule("delete any team pages", "no", "pages", ["leader", "member", "guest"], (string type) {
      auto initialSize = crates.page.get.size;

      router
        .request
        .delete_("/pages/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          crates.page.get.size.should.equal(initialSize);
          crates.page.get.where("_id").equal(ObjectId.fromString("000000000000000000000001")).and.exec.empty.should.equal(false);
        });
    });

    describeCredentialsRule("delete any team pages", "-", "pages", "no rights", {
      router
        .request
        .delete_("/pages/000000000000000000000001")
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.toPrettyString.should.equal(`{"error": "Authorization required"}`.parseJsonString.toPrettyString);
        });
    });

    describeCredentialsRule("delete own pages", "yes", "pages", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      auto page = crates.page.getItem("000000000000000000000001").and.exec.front;
      page["info"]["originalAuthor"] = userId[type];
      crates.page.updateItem(page);

      auto initialSize = crates.page.get.size;

      router
        .request
        .delete_("/pages/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(204)
        .end((Response response) => () {
          crates.page.get.size.should.equal(initialSize - 1);
          crates.page.get.where("_id").equal(ObjectId.fromString("000000000000000000000001")).and.exec.empty.should.equal(true);
        });
    });

    describeCredentialsRule("delete own pages", "-", "pages", "no rights", {});
  });
});
