/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module packages.pages.tests.pageColMiddleware;

import ogm.pages.middleware.pageColMiddleware;
import ogm.test.fixtures;
import vibe.data.json;
import vibe.http.client;
import crate.error;

import crate.ctfe;

import vibe.data.json;
import fluent.asserts;
import trial.discovery.spec;

alias suite = Spec!({
  describe("pageCol middleware", {
    PageColMiddleware middleware;

    beforeEach({
      setupTestData();

      middleware = new PageColMiddleware(crates);
    });

    describe("removeGlobalColsData", {
      it("removes the type and data from global cols", {
        auto page = `{
          "cols": [{
            "container": 0,
            "col": 1,
            "row": 0,
            "gid": "some id",
            "type": "title-with-buttons",
            "data": {
              "title": { "text": "title", "options": ["display-md-5"], "heading": 1 },
              "paragraph": { "text": "paragraph" },
              "buttons": [],
            },
          }]
        }`.parseJsonString;

        middleware.removeGlobalColsData(page);

        page.should.equal(`{
          "cols": [{
            "container": 0,
            "col": 1,
            "row": 0,
            "gid": "some id"
          }]
        }`.parseJsonString);
      });

      it("does removes the type and data from local cols", {
        auto page = `{
          "cols": [{
            "container": 0,
            "col": 1,
            "row": 0,
            "type": "title-with-buttons",
            "data": {
              "title": { "text": "title", "options": ["display-md-5"], "heading": 1 },
              "paragraph": { "text": "paragraph" },
              "buttons": [],
            },
          }]
        }`.parseJsonString;

        middleware.removeGlobalColsData(page);

        page.should.equal(`{
          "cols": [{
            "container": 0,
            "col": 1,
            "row": 0,
            "type": "title-with-buttons",
            "data": {
              "title": { "text": "title", "options": ["display-md-5"], "heading": 1 },
              "paragraph": { "text": "paragraph" },
              "buttons": [],
            },
          }]
        }`.parseJsonString);
      });


      it("ignores the empty objects", {
        auto page = `{}`.parseJsonString;

        middleware.removeGlobalColsData(page);

        page.should.equal(`{}`.parseJsonString);
      });
    });

    describe("restoreGlobalCols", {
      Json space;

      beforeEach({
        space = `{
          "cols": {
            "main-title": {
              "type": "title-with-buttons",
              "data": {
                "title": { "text": "title", "options": ["display-md-5"], "heading": 1 },
                "paragraph": { "text": "paragraph" },
                "buttons": []
              }
            }
          }
        }`.parseJsonString;

        crates.space.addItem(space);
      });

      it("sets the type and data of a global col", {
        auto page = `{
          "space": "000000000000000000000001",
          "cols": [{
            "container": 0,
            "col": 1,
            "row": 0,
            "gid": "main-title",
          }]
        }`.parseJsonString;

        middleware.restoreGlobalCols(page);

        page.should.equal(`{
          "space": "000000000000000000000001",
          "cols": [{
            "container": 0,
            "col": 1,
            "row": 0,
            "gid": "main-title",
            "type": "title-with-buttons",
            "data": {
              "title": { "text": "title", "options": ["display-md-5"], "heading": 1 },
              "paragraph": { "text": "paragraph" },
              "buttons": []
            }
          }]
        }`.parseJsonString);
      });


      it("does not set sets the type and data of an undefined global col", {
        auto page = `{
          "space": "000000000000000000000001",
          "cols": [{
            "container": 0,
            "col": 1,
            "row": 0,
            "gid": "other",
          }]
        }`.parseJsonString;

        middleware.restoreGlobalCols(page);

        page.should.equal(`{
          "space": "000000000000000000000001",
          "cols": [{
            "container": 0,
            "col": 1,
            "row": 0,
            "gid": "other",
          }]
        }`.parseJsonString);
      });
    });

    describe("restoreGlobalLayoutContainers", {
      Json space;

      beforeEach({
        space = `{
          "layoutContainers": {
            "main-menu": {
              "rows": [{}],
              "options": ["container-fluid"],
              "layers": {},
              "gid": ""
            }
          }
        }`.parseJsonString;

        crates.space.addItem(space);
      });

      it("replaces a matching gid withthe global one", {
        auto page = `{
          "space": "000000000000000000000001",
          "layoutContainers": [{
            "gid": "main-menu",
          }]
        }`.parseJsonString;

        middleware.restoreGlobalLayoutContainers(page);

        page.should.equal(`{
          "space": "000000000000000000000001",
          "layoutContainers": [{
            "rows": [{}],
            "options": ["container-fluid"],
            "layers": {},
            "gid": "main-menu"
          }]
        }`.parseJsonString);
      });


      it("does not set sets the type and data of an undefined global col", {
        auto page = `{
          "space": "000000000000000000000001",
          "layoutContainers": [{
            "gid": "other",
          }]
        }`.parseJsonString;

        middleware.restoreGlobalLayoutContainers(page);

        page.should.equal(`{
          "space": "000000000000000000000001",
          "layoutContainers": [{
            "gid": "",
          }]
        }`.parseJsonString);
      });
    });
  });
});