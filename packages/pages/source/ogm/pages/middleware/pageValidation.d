module ogm.pages.middleware.pageValidation;


import ogm.crates.all;
import ogm.http.request;

import vibe.http.router;
import vibe.data.json;

import ogm.http.request;

import crate.error;
import crate.base;
import crate.json;

import std.regex;
import std.string;
import std.algorithm;
import std.uuid;
import std.functional;

import crate.lazydata.base;

class PageValidationMiddleware {
  private {
    OgmCrates crates;
  }

  ///
  this(OgmCrates crates) {
    this.crates = crates;
  }

  @create
  void createNewPage(HTTPServerRequest request) {
    if("page" !in request.json) {
      return;
    }

    validateRecord(request.json["page"]);
  }

  @put @patch
  void updatePage(HTTPServerRequest req) {
    if("page" !in req.json) {
      return;
    }

    validateRecord(req.json["page"]);
  }

  @mapper
  defaultSpaceMapper(ref Json page) {
    if(page["space"].type != Json.Type.string) {
      auto space = crates.space.get.where("visibility.isDefault").equal(true).and.exec.front;
      page["space"] = space["_id"];
    }
  }

  void validateRecord(ref Json page) {
    auto lazyPage = Lazy!Page(page, (&itemResolver).toDelegate).toType;

    enforce!CrateValidationException(page["space"].type == Json.Type.string, "The 'space' property is missing.");
    enforce!CrateValidationException(lazyPage.visibility.team._id == lazyPage.space.visibility.team._id, "The page and the space must have the same team.");
  }
}