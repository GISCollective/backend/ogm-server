/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.pages.middleware.pageColMiddleware;

import crate.base;
import crate.error;
import crate.json;
import ogm.crates.all;
import vibe.data.json;
import vibe.http.server;
import ogm.TimedCache;

import std.array;
import std.algorithm;
import std.exception;

class PageColMiddleware {
  private {
    OgmCrates crates;
    TimedCache!Json spaceCache;
  }

  this(OgmCrates crates) {
    this.crates = crates;
    this.spaceCache = new TimedCache!Json(&this.getSpace);
  }

  Json getSpace(string id) {
    auto range = crates.space.get.where("_id").equal(ObjectId.fromString(id)).and.exec;

    if(range.empty) {
      return Json.emptyObject;
    }

    return range.front;
  }

  void removeGlobalColsData(ref Json page) {
    if(!page.exists("cols")) {
      return;
    }

    foreach (ref Json json; page["cols"]) {
      if("gid" !in json) {
        continue;
      }

      if("type" in json) {
        json.remove("type");
      }

      if("data" in json) {
        json.remove("data");
      }
    }
  }

  @create @put @patch
  void requestHandler(HTTPServerRequest req) {
    if("page" !in req.json) {
      return;
    }

    removeGlobalColsData(req.json["page"]);
  }

  @mapper
  restoreGlobalLayoutContainers(ref Json page) {
    if(!page.exists("layoutContainers") || !page.exists("space")) {
      return;
    }

    auto space = this.spaceCache.get(page["space"].to!string);
    auto layoutContainers = space["layoutContainers"];

    if(layoutContainers.type != Json.Type.object) {
      layoutContainers = Json.emptyObject;
    }

    foreach (ref Json json; page["layoutContainers"]) {
      if("gid" !in json) {
        continue;
      }

      auto gid = json["gid"].to!string;

      if(gid !in layoutContainers) {
        json["gid"] = "";
        continue;
      }

      json["rows"] = layoutContainers[gid]["rows"];
      json["options"] = layoutContainers[gid]["options"];
      json["layers"] = layoutContainers[gid]["layers"];
    }
  }

  @mapper
  void restoreGlobalCols(ref Json page) {
    if(!page.exists("cols") || !page.exists("space")) {
      return;
    }

    auto space = this.spaceCache.get(page["space"].to!string);
    auto cols = space["cols"];

    if(cols.type != Json.Type.object) {
      return;
    }

    foreach (ref Json json; page["cols"]) {
      if("gid" !in json) {
        continue;
      }

      auto gid = json["gid"].to!string;

      if(gid !in cols) {
        continue;
      }

      json["type"] = cols[gid]["type"];
      json["data"] = cols[gid]["data"];
    }
  }
}
