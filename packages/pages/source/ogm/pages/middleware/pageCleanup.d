module ogm.pages.middleware.pageCleanup;


import ogm.crates.all;
import ogm.http.request;

import vibe.http.router;
import vibe.data.json;

import ogm.http.request;

import crate.error;
import crate.base;
import crate.json;

import std.string;
import std.algorithm;
import std.array;

import crate.lazydata.base;

class PageCleanupMiddleware {

  void copyOptionsToData(ref Json object) {
    if(object["options"].type != Json.Type.array || object["options"].length == 0) {
      return;
    }

    auto options = object["options"].deserializeJson!(string[]);

    if(object["data"].type != Json.Type.object) {
      object["data"] = Json.emptyObject;
    }

    if(object["data"]["container"].type != Json.Type.object) {
      object["data"]["container"] = Json.emptyObject;
    }

    if(object["data"]["container"]["classes"].type != Json.Type.array) {
      object["data"]["container"]["classes"] = Json.emptyArray;
    }

    auto classes = object["data"]["container"]["classes"].deserializeJson!(string[]);

    auto allClasses = options ~ classes;

    object["data"]["container"]["classes"] = allClasses.sort.uniq.array.serializeToJson;
    object["options"] = Json.emptyArray;
  }

  @mapper
  copyOptionsToContainer(ref Json page) {
    if(page["containers"].type != Json.Type.array) {
      return;
    }

    foreach(ref container; page["containers"]) {
      if(container["rows"].type != Json.Type.array) {
        continue;
      }

      foreach(ref row; container["rows"]) {
        if(row["cols"].type != Json.Type.array) {
          continue;
        }

        copyOptionsToData(row);

        foreach(ref col; row["cols"]) {
          copyOptionsToData(col);
        }
      }
    }
  }
}