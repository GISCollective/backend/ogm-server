module ogm.pages.middleware.pageFilters;

import ogm.crates.all;
import vibe.data.json;
import vibe.http.server;
import ogm.http.request;

import crate.base;
import crate.json;

import std.string;

import crate.lazydata.base;

class PageFiltersMiddleware {
  struct Params {
    @describe("Filter records by space")
    string space;
  }

  private {
    OgmCrates crates;
  }

  ///
  this(OgmCrates crates) {
    this.crates = crates;
  }

  @getList
  void paramsFilter(HTTPServerRequest req, IQuery selector, Params params) {
    auto request = RequestUserData(req);

    if(params.space != "") {
      selector.where("space").equal(ObjectId.fromString(params.space));
    }
  }
}