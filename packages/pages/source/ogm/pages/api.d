/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.pages.api;

import vibe.http.router;
import vibe.core.log;
import vibe.data.json;
import vibe.service.configuration.general;

import crate.http.router;
import crate.base;
import ogm.defaults.pages;
import ogm.defaults.spaces;

import ogm.auth;
import ogm.crates.all;

import ogm.middleware.adminrequest;
import ogm.middleware.userdata;
import ogm.middleware.modelinfo;
import ogm.middleware.SlugMiddleware;
import ogm.middleware.PublisherTeamMiddleware;
import ogm.pages.middleware.pageValidation;
import ogm.filter.visibility;
import ogm.operations.RevisionHistory;
import ogm.operations.Revision;
import ogm.pages.middleware.pageColMiddleware;
import ogm.operations.space.PagesMap;
import ogm.pages.middleware.pageFilters;
import gis_collective.hmq.broadcast.base;
import ogm.pages.operations.ContactOperation;
import ogm.pages.middleware.pageCleanup;
import ogm.operations.BulkDeleteOperation;
import ogm.operations.BulkPublishOperation;
import ogm.operations.BulkUnpublishOperation;
import ogm.operations.PublishOperation;
import ogm.operations.UnpublishOperation;

///
void setupPageApi(T)(CrateRouter!T crateRouter, OgmCrates crates, GeneralConfig config, IBroadcast broadcast) {
  auto auth = Authentication.instance(crates.user);
  auto adminRequest = new AdminRequest(crates.user);

  auto userDataMiddleware = UserDataMiddleware.instance(crates);
  auto modelInfo = new ModelInfoMiddleware("page", crates.page);
  auto slugMiddleware = new SlugMiddleware(crates.page);
  auto visibilityFilter = new VisibilityFilter!"pages"(crates, crates.page);
  auto publisherTeamMiddleware = new PublisherTeamMiddleware("page", crates.team);
  auto revisionHistoryOperation = new RevisionHistoryOperation!Page(crates, crates.page, config.apiUrl);
  auto revisionOperation = new RevisionOperation!Page(crates, crates.page);
  auto pageValidation = new PageValidationMiddleware(crates);
  auto pageColMiddleware = new PageColMiddleware(crates);
  auto pageFiltersMiddleware = new PageFiltersMiddleware(crates);
  auto contactOperation = new ContactOperation(crates, broadcast);
  auto pageCleanupMiddleware = new PageCleanupMiddleware();

  auto bulkDelete = new BulkDeleteOperation!("page", "space")(crates, auth.privateDataMiddleware, []);
  auto bulkPublish = new BulkPublishOperation!("page", "space")(crates, auth.privateDataMiddleware, []);
  auto bulkUnpublish = new BulkUnpublishOperation!("page", "space")(crates, auth.privateDataMiddleware, []);
  auto publish = new PublishOperation!"page"(crates, auth.privateDataMiddleware, []);
  auto unpublish = new UnpublishOperation!"page"(crates, auth.privateDataMiddleware, []);

  crateGetters["Picture"] = &crates.picture.getItem;
  crateGetters["Space"] = &crates.space.getItem;
  crateGetters["Team"] = &crates.team.getItem;

  auto preparedRoute = crateRouter
    .prepare(crates.page)
    .withCustomOperation(revisionHistoryOperation)
    .withCustomOperation(revisionOperation)
    .withCustomOperation(contactOperation)
    .withCustomOperation(publish)
    .withCustomOperation(unpublish)
    .withCustomOperation(bulkDelete)
    .withCustomOperation(bulkPublish)
    .withCustomOperation(bulkUnpublish)
    .and(auth.publicDataMiddleware)
    .and(adminRequest)
    .and(slugMiddleware)
    .and(publisherTeamMiddleware)
    .and(userDataMiddleware)
    .and(modelInfo)
    .and(pageValidation)
    .and(pageColMiddleware)
    .and(visibilityFilter)
    .and(pageFiltersMiddleware)
    .and(pageCleanupMiddleware);
}

void setupDefaultPages(OgmCrates crates) {
  crates.checkDefaultPages;
}
