module ogm.pages.operations.ContactOperation;

import crate.http.operations.base;
import ogm.crates.all;
import ogm.http.request;

import vibe.http.router;
import vibe.data.json;

import ogm.http.request;

import crate.error;
import crate.base;
import crate.json;
import gis_collective.hmq.broadcast.base;

import crate.error;
import std.exception;
import std.algorithm;

class ContactOperation : CrateOperation!DefaultStorage {
  private {
    OgmCrates crates;
    IBroadcast broadcast;
  }

  ///
  this(OgmCrates crates, IBroadcast broadcast) {
    this.crates = crates;
    this.broadcast = broadcast;

    CrateRule rule;
    rule.request.path = "/pages/:id/contact";
    rule.request.method = HTTPMethod.POST;
    rule.response.statusCode = 201;

    super(crates.page, rule);
  }

  override void handle(DefaultStorage storage) {
    storage.item = this.prepareItemOperation!"getItem"(storage);

    if(storage.response.headerWritten) {
      return;
    }

    this.applyRule(storage);

    enforce!CrateValidationException(storage.request.json.type == Json.Type.object, "You must send an object with the 'message' property.");
    enforce!CrateValidationException(storage.request.json["message"].type == Json.Type.object, "You must send a 'message' property.");

    auto page = crates.page.getItem(storage.properties.itemId).and.exec.front;
    auto colRange = page["cols"].byValue.filter!(a => a["name"] == storage.request.json["message"]["colName"]);

    enforce!CrateValidationException(!colRange.empty, "Can't find the page component to get the destination email.");

    auto col = colRange.front;

    enforce!CrateValidationException(col["data"].type == Json.Type.object, "The form config does not have the 'data' property.");
    enforce!CrateValidationException(col["data"]["form"].type == Json.Type.object, "The form config does not have the 'data.form' property.");
    enforce!CrateValidationException(col["data"]["form"]["sendTo"].type == Json.Type.string, "The form config does not have the 'sentTo' property.");

    auto message = Json.emptyObject;
    message["email"] = storage.request.json["message"]["email"].to!string;
    message["message"] = storage.request.json["message"]["message"].to!string;
    message["name"] = storage.request.json["message"]["name"].to!string;
    message["subject"] = storage.request.json["message"]["subject"].to!string;
    message["sendTo"] = col["data"]["form"]["sendTo"].to!string;

    if(message["sendTo"] == "") {
      auto team = crates.team.getItem(page["visibility"]["team"].to!string).and.exec.front;
      message["sendTo"] = team["contactEmail"];
    }

    broadcast.push("page.contact", message);

    storage.response.statusCode = 201;
    storage.response.writeVoidBody;
  }
}