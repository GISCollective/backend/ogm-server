/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.metrics.middlewares.MetricGroup;

import crate.attributes;
import crate.base;
import crate.error;
import ogm.calendar;
import ogm.crates.all;
import ogm.http.request;
import std.algorithm;
import std.datetime;
import std.conv;
import std.exception;
import vibe.http.server;
import vibe.data.json;

SysTime toStart(SysTime value, string group) {
  value.fracSecs = 0.msecs;
  value.second = 0;
  value.hour = 0;
  value.minute = 0;

  if(group == "w") {
    auto dayOfWeek = value.dayOfWeek.to!int;

    if(dayOfWeek == 0) {
      dayOfWeek = 7;
    }

    dayOfWeek--;

    value.day = value.day - dayOfWeek;
  }

  if(group == "m") {
    value.day = 1;
  }

  return value;
}

class MetricGroup {
  private OgmCrates crates;

  this(OgmCrates crates) {
    this.crates = crates;
  }

  @getList
  void groupResults(HTTPServerRequest req, HTTPServerResponse response, IQuery selector) {
    auto name = req.query["name"];
    auto type = req.query["type"];
    auto start = SysCalendar.instance.now - 7.days;
    auto end = SysCalendar.instance.now;
    auto group = "d";

    if("group" in req.query) {
      group = req.query["group"];
    }

    if("start" in req.query) {
      start = SysTime.fromISOExtString(req.query["start"]);
    }

    if("end" in req.query) {
      end = SysTime.fromISOExtString(req.query["end"]);
    }

    start -= 1.seconds;

    response.contentType = "application/json";
    response.statusCode = 200;

    selector
      .where("type").equal(type).and
      .where("name").equal(name).and
      .where("time").greaterThan(start).lessThan(end).and
      .sort("time", 1);

    response.bodyWriter.write(`{ "metrics": [` ~ "\n");

    size_t index;

    Metric groupMetric;
    groupMetric.name = name;
    groupMetric.type = type;
    groupMetric.value = 0;
    Duration interval;

    foreach(metric; selector.and.exec) {
      auto time = SysTime.fromISOExtString(metric["time"].to!string).toStart(group);
      auto diff = time - groupMetric.time;

      if(index == 0 || diff > interval) {
        if(index > 1) {
          response.bodyWriter.write(",");
        }

        if(index > 0) {
          response.bodyWriter.write(groupMetric.serializeToJson.to!string);
          response.bodyWriter.write("\n");
        }

        groupMetric._id = ObjectId(metric["_id"]);
        groupMetric.time = time;
        groupMetric.value = 0;
        index++;
      }

      groupMetric.value += metric["value"].to!double;
    }

    if(index > 1) {
      response.bodyWriter.write(",");
    }

    if(index > 0) {
      response.bodyWriter.write(groupMetric.serializeToJson.to!string);
    }

    response.bodyWriter.write("\n]}");
    response.bodyWriter.finalize;
  }
}