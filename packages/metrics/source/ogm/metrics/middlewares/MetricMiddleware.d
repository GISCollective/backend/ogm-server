/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.metrics.middlewares.MetricMiddleware;

import crate.attributes;
import crate.error;
import crate.base;
import ogm.crates.all;
import ogm.http.request;
import std.algorithm;
import std.datetime;
import std.exception;
import crate.collection.EmptyQuery;
import vibe.http.server;

class MetricMiddleware {

  private OgmCrates crates;

  this(OgmCrates crates) {
    this.crates = crates;
  }

  @create
  void addDefaultFields(HTTPServerRequest req) {
    if("metric" !in req.json) {
      return;
    }

    auto request = RequestUserData(req);

    req.json["metric"]["time"] = Clock.currTime.toISOExtString;

    if(request.isAuthenticated) {
      req.json["metric"]["reporter"] = request.userId;
    } else if(!req.json["metric"]["reporter"].to!string.canFind("@")) {
      req.json["metric"]["reporter"] = "@" ~ req.json["metric"]["reporter"].to!string;
    }
  }
  @delete_ @put @getItem
  void notFound(HTTPServerRequest req) {
    enforce!CrateNotFoundException(false, "Metric not found.");
  }

  @getList
  void accessRights(HTTPServerRequest req) {
    auto hasName = "name" in req.query;
    auto hasType = "type" in req.query;

    enforce!CrateNotFoundException(hasName && hasType, "Metric not found.");

    auto request = RequestUserData(req);
    if(request.isAdmin) {
      return;
    }

    enforce!CrateNotFoundException(["mapView"].canFind(req.query["type"]), "Metric not found.");
    enforce!UnauthorizedException(request.isAuthenticated, "Authorization required");

    if(req.query["type"] == "mapView") {
      enforce!CrateNotFoundException(request.session(crates).all.maps.canFind(req.query["name"]), "Metric not found.");
    }
  }
}