/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.metrics.api;

import vibe.http.router;
import vibe.core.log;
import vibe.data.json;

import crate.http.router;
import crate.base;

import ogm.auth;
import ogm.crates.all;

import ogm.middleware.adminrequest;
import ogm.middleware.userdata;
import ogm.metrics.middlewares.MetricMiddleware;
import ogm.metrics.middlewares.MetricGroup;

///
void setupMetricApi(T)(CrateRouter!T crateRouter, OgmCrates crates) {
  auto auth = Authentication.instance(crates.user);
  auto adminRequest = new AdminRequest(crates.user);

  auto userDataMiddleware = UserDataMiddleware.instance(crates);
  auto metricMiddleware = new MetricMiddleware(crates);
  auto metricGroup = new MetricGroup(crates);

  crateRouter.prepare(crates.metric)
    .and(auth.contributionMiddleware)
    .and(adminRequest)
    .and(userDataMiddleware)
    .and(metricMiddleware)
    .and(metricGroup);
}

