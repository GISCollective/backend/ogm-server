/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.metrics.put;

import tests.fixtures;
import vibe.http.common;
import ogm.metrics.api;

alias suite = Spec!({
  URLRouter router;
  Json metric;
  string metricId;

  describe("updating metrics", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupMetricApi(crates);

      metric = crates.metric.addItem(`{
        "name": "test",
        "type": "some",
        "reporter": "1",
        "value": 1
      }`.parseJsonString);

      metricId = metric["_id"].to!string;
    });

    describeCredentialsRule("updating a metric", "no", "metrics", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      auto initialSize = crates.metric.get.size;

      router
        .request
        .put("/metrics/" ~ metric["_id"].to!string)
        .send(metric)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(404)
        .end((Response response) => () {
          crates.metric.get.where("_id").equal(ObjectId.fromJson(metric["_id"])).and.exec.empty.should.equal(false);
        });
    });

    describeCredentialsRule("updating a metric", "no", "metrics", "no rights", {
      router
        .request
        .put("/metrics/" ~ metric["_id"].to!string)
        .send(metric)
        .expectStatusCode(401)
        .end((Response response) => () {
          crates.metric.get.where("_id").equal(ObjectId.fromJson(metric["_id"])).and.exec.empty.should.equal(false);
        });
    });
  });
});
