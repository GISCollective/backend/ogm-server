/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.metrics.get;

import tests.fixtures;
import vibe.http.common;
import ogm.metrics.api;

alias suite = Spec!({
  URLRouter router;
  Json metric;
  string metricId;

  describe("getting metrics", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupMetricApi(crates);

      metric = crates.metric.addItem(`{
        "name": "000000000000000000000001",
        "type": "mapView",
        "reporter": "1",
        "value": 1,
        "time": "2020-12-12T00:22:33Z"
      }`.parseJsonString);

      metricId = metric["_id"].to!string;
    });

    describeCredentialsRule("getting a metric", "no", "metrics", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/metrics/" ~ metric["_id"].to!string)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(404)
        .end;
    });

    describeCredentialsRule("getting a metric", "no", "metrics", "no rights", {
      router
        .request
        .get("/metrics/" ~ metric["_id"].to!string)
        .expectStatusCode(401)
        .end((Response response) => () {
          crates.metric.get.where("_id").equal(ObjectId.fromJson(metric["_id"])).and.exec.empty.should.equal(false);
        });
    });

    describeCredentialsRule("getting a list of metrics", "no", "metrics", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/metrics")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(404)
        .end;
    });

    describeCredentialsRule("getting a list of metrics", "no", "metrics", "no rights", {
      router
        .request
        .get("/metrics")
        .expectStatusCode(401)
        .end((Response response) => () {
          crates.metric.get.where("_id").equal(ObjectId.fromJson(metric["_id"])).and.exec.empty.should.equal(false);
        });
    });

    describe("as owner", {
      it("can get the metrics for a map", {
        router
          .request
          .get("/metrics?type=mapView&name=000000000000000000000001&start=2020-12-12T00:00:00Z&end=2021-12-12T00:00:00Z")
          .header("Authorization", "Bearer " ~ userTokenList["owner"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"metrics": [{
              "_id": "000000000000000000000001",
              "name": "000000000000000000000001",
              "reporter": "",
              "labels": {},
              "time": "2020-12-12T00:00:00Z",
              "type": "mapView",
              "value": 1
            }]}`.parseJsonString);
          });
      });

      it("cannot get the metrics for a map from other teams", {
        router
          .request
          .get("/metrics?type=mapView&name=000000000000000000000003&start=2020-12-12T00:00:00Z&end=2021-12-12T00:00:00Z")
          .header("Authoriztion", "Bearer " ~ userTokenList["owner"])
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "error": "Authorization required"
            }`.parseJsonString);
          });
      });
    });

    describe("as anonymous user", {
      it("can not get the metrics for a map", {
        router
          .request
          .get("/metrics?type=mapView&name=000000000000000000000001&start=2020-12-12T00:00:00Z&end=2021-12-12T00:00:00Z")
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
          });
      });
    });

    describe("as admin", {
      it("can get a list of metrics by name and type as admin", {
        crates.metric.addItem(`{
          "name": "000000000000000000000001",
          "type": "mapView",
          "reporter": "2",
          "value": 1,
          "time": "2021-03-12T00:22:33Z"
        }`.parseJsonString);

        crates.metric.addItem(`{
          "name": "000000000000000000000001",
          "type": "mapView",
          "reporter": "2",
          "value": 1,
          "time": "2021-04-12T00:22:33Z"
        }`.parseJsonString);

        router
          .request
          .get("/metrics?type=mapView&name=000000000000000000000001&start=2020-12-12T00:00:00Z&end=2021-12-12T00:00:00Z")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"metrics": [{
              "_id": "000000000000000000000001",
              "labels": {},
              "name": "000000000000000000000001",
              "reporter": "",
              "time": "2020-12-12T00:00:00Z",
              "type": "mapView",
              "value": 1
            }, {
              "_id": "000000000000000000000002",
              "labels": {},
              "name": "000000000000000000000001",
              "reporter": "",
              "time": "2021-03-12T00:00:00Z",
              "type": "mapView",
              "value": 1
            }, {
              "_id": "000000000000000000000003",
              "labels": {},
              "name": "000000000000000000000001",
              "reporter": "",
              "time": "2021-04-12T00:00:00Z",
              "type": "mapView",
              "value": 1
            }]}`.parseJsonString);
          });
      });

      it("groups 2 metrics happening on the same day", {
        metric = crates.metric.addItem(`{
          "name": "000000000000000000000001",
          "type": "mapView",
          "reporter": "2",
          "value": 1,
          "time": "2020-12-12T00:22:33Z"
        }`.parseJsonString);

        router
          .request
          .get("/metrics?type=mapView&name=000000000000000000000001&start=2020-12-12T00:00:00Z&end=2021-12-12T00:00:00Z")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"metrics": [{
              "_id": "000000000000000000000001",
              "name": "000000000000000000000001",
              "reporter": "",
              "time": "2020-12-12T00:00:00Z",
              "type": "mapView",
              "value": 2,
              "labels": {}
            }]}`.parseJsonString);
          });
      });

      it("does not group metrics happening on diferent days", {
        metric = crates.metric.addItem(`{
          "name": "000000000000000000000001",
          "type": "mapView",
          "reporter": "2",
          "value": 1,
          "time": "2020-12-12T00:22:33Z"
        }`.parseJsonString);

        metric = crates.metric.addItem(`{
          "name": "000000000000000000000001",
          "type": "mapView",
          "reporter": "2",
          "value": 1,
          "time": "2021-01-01T00:22:33Z"
        }`.parseJsonString);

        router
          .request
          .get("/metrics?type=mapView&name=000000000000000000000001&start=2020-12-12T00:00:00Z&end=2021-12-12T00:00:00Z")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"metrics": [{
              "_id": "000000000000000000000001",
              "name": "000000000000000000000001",
              "reporter": "",
              "time": "2020-12-12T00:00:00Z",
              "type": "mapView",
              "value": 2,
              "labels": {}
            }, {
              "_id": "000000000000000000000003",
              "labels": {},
              "name": "000000000000000000000001",
              "reporter": "",
              "time": "2021-01-01T00:00:00Z",
              "type": "mapView",
              "value": 1
            }]}`.parseJsonString);
          });
      });

      it("can group metrics by month", {
        metric = crates.metric.addItem(`{
          "name": "000000000000000000000001",
          "type": "mapView",
          "reporter": "2",
          "value": 1,
          "time": "2020-12-12T00:22:33Z"
        }`.parseJsonString);

        metric = crates.metric.addItem(`{
          "name": "000000000000000000000001",
          "type": "mapView",
          "reporter": "2",
          "value": 1,
          "time": "2021-02-01T00:22:33Z"
        }`.parseJsonString);

        router
          .request
          .get("/metrics?type=mapView&name=000000000000000000000001&start=2020-12-12T00:00:00Z&end=2021-12-12T00:00:00Z&group=m")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"metrics": [{
              "_id": "000000000000000000000001",
              "name": "000000000000000000000001",
              "reporter": "",
              "time": "2020-12-01T00:00:00Z",
              "type": "mapView",
              "value": 2,
              "labels": {}
            }, {
              "_id": "000000000000000000000003",
              "labels": {},
              "name": "000000000000000000000001",
              "reporter": "",
              "time": "2021-02-01T00:00:00Z",
              "type": "mapView",
              "value": 1
            }]}`.parseJsonString);
          });
      });

      it("can group metrics by week", {
        metric = crates.metric.addItem(`{
          "name": "000000000000000000000001",
          "type": "mapView",
          "reporter": "2",
          "value": 1,
          "time": "2020-12-12T00:22:33Z"
        }`.parseJsonString);

        metric = crates.metric.addItem(`{
          "name": "000000000000000000000001",
          "type": "mapView",
          "reporter": "2",
          "value": 1,
          "time": "2021-02-01T00:22:33Z"
        }`.parseJsonString);

        router
          .request
          .get("/metrics?type=mapView&name=000000000000000000000001&start=2020-12-12T00:00:00Z&end=2021-12-12T00:00:00Z&group=w")
          .header("Authorization", "Bearer " ~ userTokenList["administrator"])
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"metrics": [{
              "_id": "000000000000000000000001",
              "name": "000000000000000000000001",
              "reporter": "",
              "time": "2020-12-07T00:00:00Z",
              "type": "mapView",
              "value": 2,
              "labels": {}
            }, {
              "_id": "000000000000000000000003",
              "labels": {},
              "name": "000000000000000000000001",
              "reporter": "",
              "time": "2021-02-01T00:00:00Z",
              "type": "mapView",
              "value": 1
            }]}`.parseJsonString);
          });
      });
    });
  });
});
