/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.metrics.post;

import tests.fixtures;
import vibe.http.common;
import ogm.defaults.teams;
import ogm.metrics.api;

alias suite = Spec!({
  URLRouter router;

  describe("adding metrics", {
    Json metric;

    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupMetricApi(crates);
      crates.setupDefaultTeams;

      metric = `{ "metric": {
        "name": "test",
        "type": "some",
        "reporter": "1",
        "value": 1
      }}`.parseJsonString;
    });

    it("adds the time", {
      auto initialSize = crates.metric.get.size;

      router
        .request
        .post("/metrics")
        .send(metric)
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["time"].should.not.equal("0001-01-01T00:00:00+00:00");
        });
    });

    it("adds the current user as reporter", {
      auto initialSize = crates.metric.get.size;

      router
        .request
        .post("/metrics")
        .send(metric)
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["metric"]["reporter"].should.equal(userId["administrator"]);
        });
    });

    it("adds an @ when there is no user", {
      auto initialSize = crates.metric.get.size;

      router
        .request
        .post("/metrics")
        .send(metric)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["metric"]["reporter"].should.equal("@1");
        });
    });

    it("does not add @ for emails", {
      auto initialSize = crates.metric.get.size;
      metric["metric"]["reporter"] = "1@2";

      router
        .request
        .post("/metrics")
        .send(metric)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["metric"]["reporter"].should.equal("1@2");
        });
    });

    describeCredentialsRule("push metrics", "yes", "metrics", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      auto initialSize = crates.metric.get.size;

      router
        .request
        .post("/metrics")
        .send(metric)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          crates.metric.get.size.should.equal(initialSize + 1);
        });
    });

    describeCredentialsRule("push metrics", "yes", "metrics", "no rights", {
      auto initialSize = crates.metric.get.size;

      router
        .request
        .post("/metrics")
        .send(metric)
        .expectStatusCode(200)
        .end((Response response) => () {
          crates.metric.get.size.should.equal(initialSize + 1);
        });
    });
  });
});
