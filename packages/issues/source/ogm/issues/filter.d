/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.issues.filter;

import vibe.http.router;
import vibe.data.json;

import crate.base;
import crate.error;

class IssueFilter {

  struct Parameters {
    @describe("A feature id. If it's set, then only issues belonging to that feature will be returned.")
    string feature;
  }

  /// Filter that for get requests returns only the public and the logged user maps
  /// It denies adding/editing/removing maps if they are not owned
  @get
  IQuery get(IQuery selector, Parameters parameters, HTTPServerRequest request) {
    if("id" in request.params) {
      return selector;
    }

    enforce!CrateValidationException(parameters.feature != "", "The `feature` query parameter is missing.");
    selector.where("feature").equal(parameters.feature);

    return selector;
  }
}
