/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.issues.resolver.images;

import ogm.crates.all;
import vibe.service.configuration.db;
import ogm.models.issue;
import ogm.models.picture;

import crate.base;
import crate.resource.file;

import std.stdio;
import std.path;
import std.file;
import std.algorithm;
import std.conv;

import vibe.data.json;

class ImagesResolver {

  private {
    Crate!Issue issues;
    Crate!Picture pictures;
    Crate!Feature features;
  }

  size_t processed;
  size_t errors;
  string[] messages;

  this(Crate!Issue issues, Crate!Picture pictures, Crate!Feature feature) {
    this.issues = issues;
    this.pictures = pictures;
    this.features = features;
  }

  private {
    string destinationFolderKey(const Json item) @safe {
      string key;

      if(item["author"].to!string.canFind("@")) {
        key = buildPath("sites", item["site"].to!string).to!string;
      } else {
        key = buildPath("users", item["author"].to!string).to!string;
      }

      return key;
    }

    string destinationKey(const Json item) @safe {
      auto file = item["file"].to!string;
      return buildPath(destinationFolderKey(item), file.baseName).to!string;
    }

    void copyIssueFile(const Json item) @safe {
      auto file = item["file"].to!string;
      auto source = buildPath(IssueFileSettings.path, file);

      auto destinationFolder = buildPath(DefaultCrateFileSettings.path, destinationFolderKey(item));

      if(!destinationFolder.exists) {
        mkdirRecurse(destinationFolder);
      }

      auto destination = buildPath(DefaultCrateFileSettings.path, destinationKey(item));

      copy(source, destination);
      remove(source);
    }

    void updateIssue(Json item) @safe {
      item["processed"] = true;
      item.remove("file");
      this.issues.updateItem(item);
    }

    void updateSite(Json item, string pictureId) @trusted {
      auto feature = this.features.getItem(item["feature"].to!string).exec.front;

      auto issueCount = feature["issueCount"].to!long;
      issueCount--;

      if(issueCount < 0) {
        issueCount = 0;
      }

      feature["issueCount"] = issueCount;

      if(pictureId != "") {
        feature["pictures"] ~= pictureId;
      }

      this.features.updateItem(feature);
    }

    auto addPicture(const Json item) @trusted {
      Picture picture;
      picture.name = "";
      picture.owner = item["author"].to!string;
      picture.picture = new PictureFile(destinationKey(item));

      auto result = pictures.addItem(picture.serializeToJson);

      return result["_id"].to!string;
    }
  }

  void run() @trusted {
    auto query = issues.get;
    query.where("processed").equal(false);
    query.where("status").equal("resolved");
    query.where("type").equal("newImage");

    foreach(item; query.exec) {
      string id;

      try {
        copyIssueFile(item);
        id = addPicture(item);
        updateSite(item, id);
      } catch(Exception e) {
        errors++;
        messages ~= "[issue " ~ item["_id"].to!string ~ "] " ~ e.message.to!string;
      }

      updateIssue(item);

      processed++;
    }
  }
}
