/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.issues.operations;

import std.exception;
import std.datetime;
import ogm.models.issue;
import ogm.crates.all;

import vibe.data.json;
import crate.error;


class IssuesOperations {
  OgmCrates crates;

  this(OgmCrates crates) {
    this.crates = crates;
  }

  void resolve(ref Json item) {
    if("status" !in item) {
      item["status"] = "open";
    }

    enforce!CrateValidationException(item["status"] == "open", "You can not reject `" ~ item["status"].to!string ~ "` issues.");
    auto now = Clock.currTime!(ClockType.second).toUTC;
    now.second = 0;

    item["status"] = "resolved";
    item["resolveDate"] = now.toISOExtString;

    crates.issue.updateItem(item);
  }

  void reject(ref Json item) {
    if("status" !in item) {
      item["status"] = "open";
    }

    enforce!CrateValidationException(item["status"] == "open", "You can not reject `" ~ item["status"].to!string ~ "` issues.");
    auto now = Clock.currTime!(ClockType.second).toUTC;
    now.second = 0;

    item["status"] = "rejected";
    item["resolveDate"] = now.toISOExtString;

    crates.issue.updateItem(item);
  }
}
