/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.issues.api;

import ogm.issues.filter;
import ogm.issues.middleware;

import ogm.filter.pagination;
import ogm.issues.configuration;
import ogm.issues.operations;

import crate.http.cors;
import crate.http.router;
import crate.auth.middleware;
import crate.resource.file;
import crate.resource.migration;
import crate.base;

import ogm.auth;
import ogm.crates.all;

import vibe.http.router;

///
void setupIssueApi(T)(CrateRouter!T crateRouter, OgmCrates crates, IssuesConfiguration config) {
  IssueFileSettings.baseUrl = config.baseUrl;
  IssueFileSettings.path = config.location;

  IssueFileSettings.files = crates.issueFiles;
  IssueFileSettings.chunks = crates.issueChunks;

  auto auth = Authentication.instance(crates.user, []);

  auto issueFilter = new IssueFilter();
  auto issueMiddleware = new IssueMiddleware(auth.getCollection);
  auto paginationFilter = new PaginationFilter;
  auto operations = new IssuesOperations(crates);

  crateGetters["Feature"] = &crates.feature.getItem;
  crateGetters["Map"] = &crates.map.getItem;
  crateGetters["Team"] = &crates.team.getItem;
  crateGetters["Picture"] = &crates.picture.getItem;
  crateGetters["Icon"] = &crates.icon.getItem;

  auto preparedRoute = crateRouter.prepare(crates.issue);
  preparedRoute.itemOperation!("resolve")(&operations.resolve);
  preparedRoute.itemOperation!("reject")(&operations.reject);

  preparedRoute.and(auth.contributionMiddleware);
  preparedRoute.and(issueMiddleware);
  preparedRoute.and(issueFilter);
  preparedRoute.and(paginationFilter);
}
