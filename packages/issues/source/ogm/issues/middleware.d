/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.issues.middleware;

import vibe.http.server;
import vibe.data.json;

import crate.auth.usercollection;
import crate.error;
import crate.base;
import crate.json;
import crate.http.request;

import ogm.crates.all;

import std.datetime;
import std.algorithm;
import std.string;
import std.path;

class IssueMiddleware {

  private {
    UserCrateCollection users;
  }

  this(UserCrateCollection users) {
    this.users = users;
  }

  /// Middleware applied for create routes
  @create
  void create(HTTPServerRequest req) {
    if("email" in req.context) {
      string userId = users[req.context["email"].get!string].id;

      req.json["issue"]["author"] = userId;
    } else {
      if("author" !in req.json["issue"]) {
        req.json["issue"]["author"] = "@anonymous";
      }

      enforce!CrateValidationException(req.json["issue"]["author"].to!string.canFind("@"), "The `@` is missing from the `author`.");
    }

    enforce!CrateValidationException(req.json["issue"].exists("title"), "The `title` is missing.");

    req.json["issue"]["title"] = req.json["issue"]["title"].to!string.strip;

    if(req.json["issue"].exists("type") && req.json["issue"]["type"] == "newImage") {
      enforce!CrateValidationException(req.json["issue"]["file"].type == Json.Type.string, "The `file` is missing.");
    }

    auto now = Clock.currTime!(ClockType.second).toUTC;
    now.second = 0;

    req.json["issue"]["creationDate"] = now.toISOExtString;
  }

  @mapper
  Json mapper(CrateHttpRequest, const Json item) @safe {
    Json result = item;

    if("file" in result && result["file"].type == Json.Type.string) {
      result["file"] = buildPath(IssueFileSettings.baseUrl, "issues", result["_id"].to!string, "file");
    }

    return result;
  }
}
