/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.issues.configuration;

import vibe.service.configuration.general;

///
struct MicroIssuesConfig {
  /// Configurations for all services
  GeneralConfig general;

  ///
  IssuesConfiguration issues;
}

struct IssuesConfiguration {
  string location;
  string baseUrl;
}
