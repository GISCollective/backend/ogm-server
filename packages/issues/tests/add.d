/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogmissue.tests.add;

import std.file;
import std.datetime;
import tests.fixtures;

import vibeauth.data.token;
import vibe.http.router;
import vibe.data.json;

import ogm.issues.api;
import ogm.crates.all;
import ogm.test.fixtures;

import crate.base;
import crate.collection.memory;
import crate.auth.usercollection;
import ogm.issues.configuration;
import geo.json : GeoJsonGeometry;
import ogm.issues.api;

Token bearerToken;

void createUserData(UserCrateCollection userCollection) {
  UserModel user;
  user.firstName = "John";
  user.lastName = "Doe";
  user.username = "test";
  user.email = "user@gmail.com";
  user._id = "1";

  userCollection.createUser(user, "password");
  bearerToken = userCollection.createToken("user@gmail.com", Clock.currTime + 3600.seconds, ["doStuff"], "Bearer");
}

alias s = Spec!({
  URLRouter router;

  describe("Adding basic issues", {
    SysTime now;

    beforeEach({
      Authentication.reset();
      createCrates;

      createUserData(userCollection);

      auto team = Team(ObjectId.fromString("1"), "team1", Json("description"), ["user@gmail.com"]);
      createTeam(team);
      auto cover = createPicture("1");

      IconSet iconSet;
      iconSet._id = ObjectId.fromString("1");
      iconSet.visibility.team = team;
      iconSet.name = "set1";
      createIconSet(iconSet);

      auto map = createMap("1", "map1", Json(""), true, Polygon(), team, cover, [iconSet]);
      auto feature = Feature(ObjectId.fromString("1"), [map], "site1", Json("description"),
                GeoJsonGeometry.fromJson(`{ "type": "Point", "coordinates": [1.5, 1.5] }`.parseJsonString));

      createFeature(feature);

      IssuesConfiguration config;
      config.baseUrl = "http://localhost";
      config.location = "./tmp-issue-files";

      router = new URLRouter;
      router.crateSetup.setupIssueApi(crates, config);
    });

    beforeEach({
        now = Clock.currTime!(ClockType.second).toUTC;
        now.second = 0;
    });

    describe("without a type", {
      it("should use the authenticated user id as author", {
        Json data = `{
          "issue": {
            "description": "description",
            "feature": "000000000000000000000001",
            "title": "title"
          }
        }`.parseJsonString;

        request(router)
          .post("/issues")
            .header("Authorization", "Bearer " ~ bearerToken.name)
            .expectHeader("Access-Control-Allow-Origin", "*")
            .expectHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
            .expectStatusCode(200)
            .send(data)
            .end((Response response) => () {
              response.bodyJson["issue"].should.equal((`{
                "feature": "000000000000000000000001",
                "creationDate": "` ~ response.bodyJson["issue"]["creationDate"].to!string ~ `",
                "other": "",
                "title": "title",
                "_id": "000000000000000000000001",
                "assignee": "",
                "attributions": "",
                "resolveDate": "0001-01-01T00:00:00+00:00",
                "processed": false,
                "description": "description",
                "type": "none",
                "author": "000000000000000000000001",
                "other": "",
                "assignee": "",
                "resolveDate": "0001-01-01T00:00:00+00:00",
                "processed": false,
                "status": "open"}`).parseJsonString);
            });
      });

      it("should ignore null fields", {
        Json data = `{
          "issue": {
            "description": "description",
            "feature": "000000000000000000000001",
            "title": "title",
            "type": null,
            "file": null
          }
        }`.parseJsonString;

        request(router)
          .post("/issues")
            .header("Authorization", "Bearer " ~ bearerToken.name)
            .expectHeader("Access-Control-Allow-Origin", "*")
            .expectHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
            .expectStatusCode(200)
            .send(data)
            .end((Response response) => () {
              response.bodyJson.should.equal((`{"issue": {
                  "_id": "000000000000000000000001",
                  "feature": "000000000000000000000001",
                  "creationDate": "` ~ response.bodyJson["issue"]["creationDate"].to!string ~ `",
                  "title": "title",
                  "type": "none",
                  "description": "description",
                  "author": "000000000000000000000001",
                  "other": "",
                  "attributions": "",
                  "assignee": "",
                  "resolveDate": "0001-01-01T00:00:00+00:00",
                  "processed": false,
                  "status": "open"
                }}`).parseJsonString);
            });
      });

      it("should use the authenticated user id as author and rewrite the original author", {
        Json data = `{
          "issue": {
            "description": "description",
            "feature": "000000000000000000000001",
            "title": "title",
            "author": "@anonymus"
          }
        }`.parseJsonString;

        request(router)
          .post("/issues")
            .header("Authorization", "Bearer " ~ bearerToken.name)
            .expectHeader("Access-Control-Allow-Origin", "*")
            .expectHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
            .expectStatusCode(200)
            .send(data)
            .end((Response response) => () {
              response.bodyJson["issue"].should.equal((`{
                  "_id": "000000000000000000000001",
                  "feature": "000000000000000000000001",
                  "creationDate": "` ~ response.bodyJson["issue"]["creationDate"].to!string ~ `",
                  "title": "title",
                  "description": "description",
                  "attributions": "",
                  "type": "none",
                  "author": "test@test.test",
                  "other": "",
                  "assignee": "",
                  "resolveDate": "0001-01-01T00:00:00+00:00",
                  "processed": false,
                  "author": "000000000000000000000001",
                  "status": "open"
                }`).parseJsonString);
            });
      });

      it("should use @anonymous when author is missing", {
        Json data = `{
          "issue": {
            "description": "description",
            "feature": "000000000000000000000001",
            "title": "title"
          }
        }`.parseJsonString;

        request(router)
          .post("/issues")
            .expectHeader("Access-Control-Allow-Origin", "*")
            .expectHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
            .expectStatusCode(200)
            .send(data)
            .end((Response response) => () {
              response.bodyJson["issue"].should.equal((`{
                  "_id": "000000000000000000000001",
                  "feature": "000000000000000000000001",
                  "creationDate": "` ~ response.bodyJson["issue"]["creationDate"].to!string ~ `",
                  "title": "title",
                  "description": "description",
                  "attributions": "",
                  "type": "none",
                  "author": "@anonymous",
                  "other": "",
                  "assignee": "",
                  "resolveDate": "0001-01-01T00:00:00+00:00",
                  "processed": false,
                  "author": "@anonymous",
                  "status": "open"
                }`).parseJsonString);
            });
      });

      it("should use the provided author", {
        Json data = `{
          "issue": {
            "description": "description",
            "feature": "000000000000000000000001",
            "title": "title",
            "author": "test@test.test"
          }
        }`.parseJsonString;

        request(router)
          .post("/issues")
            .expectHeader("Access-Control-Allow-Origin", "*")
            .expectHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
            .expectStatusCode(200)
            .send(data)
            .end((Response response) => () {
              response.bodyJson["issue"].should.equal((`{
                  "_id": "000000000000000000000001",
                  "feature": "000000000000000000000001",
                  "creationDate": "` ~ response.bodyJson["issue"]["creationDate"].to!string ~ `",
                  "title": "title",
                  "description": "description",
                  "attributions": "",
                  "type": "none",
                  "other": "",
                  "assignee": "",
                  "resolveDate": "0001-01-01T00:00:00+00:00",
                  "processed": false,
                  "author": "test@test.test",
                  "status": "open"
                }`).parseJsonString);
            });
      });

      it("should not accept authors without @ character", {
        Json data = `{
          "issue": {
            "description": "description",
            "feature": "000000000000000000000001",
            "title": "title",
            "author": "test"
          }
        }`.parseJsonString;

        request(router)
          .post("/issues")
            .expectStatusCode(400)
            .send(data)
            .end((Response response) => () {
              response.bodyJson.should.equal((`{"errors": [{
                "description": "` ~ "The `@` is missing from the `author`." ~ `",
                "status": 400,
                "title": "Validation error"
              }]}`).parseJsonString);
            });
      });

      it("should accept issues without a title", {
        Json data = `{
          "issue": {
            "description": "description",
            "feature": "000000000000000000000001",
            "title": "",
            "author": "@name"
          }
        }`.parseJsonString;

        request(router)
          .post("/issues")
            .expectStatusCode(200)
            .send(data)
            .end((Response response) => () {

              auto expected = `{
                "issue": {
                  "feature": "000000000000000000000001",
                  "creationDate": "2022-07-20T22:38:00Z",
                  "other": "",
                  "title": "",
                  "_id": "000000000000000000000001",
                  "assignee": "",
                  "resolveDate": "0001-01-01T00:00:00+00:00",
                  "processed": false,
                  "description": "description",
                  "attributions": "",
                  "type": "none",
                  "author": "@name",
                  "status": "open"
                }
              }`.parseJsonString;

              expected["issue"]["creationDate"] = response.bodyJson["issue"]["creationDate"];

              response.bodyJson.should.equal(expected);
            });
      });

      it("should strip the white spaces from title", {
        Json data = `{
          "issue": {
            "description": "description",
            "feature": "000000000000000000000001",
            "title": " test ",
            "author": "@name"
          }
        }`.parseJsonString;

        request(router)
          .post("/issues")
            .expectStatusCode(200)
            .send(data)
            .end((Response response) => () {
              response.bodyJson["issue"].should.equal((`{
                  "feature": "000000000000000000000001",
                  "creationDate": "` ~ response.bodyJson["issue"]["creationDate"].to!string ~ `",
                  "other": "",
                  "title": "test",
                  "_id": "000000000000000000000001",
                  "assignee": "",
                  "resolveDate": "0001-01-01T00:00:00+00:00",
                  "processed": false,
                  "description": "description",
                  "attributions": "",
                  "type": "none",
                  "author": "@name",
                  "status": "open"
                }`).parseJsonString);
            });
      });
    });
  });
});
