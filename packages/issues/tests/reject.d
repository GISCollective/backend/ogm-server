/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogmissue.tests.reject;

import std.file;
import std.datetime;

import tests.fixtures;



import vibeauth.data.token;
import vibe.http.router;
import vibe.data.json;

import ogm.issues.api;
import ogm.crates.all;
import ogm.test.fixtures;

import crate.base;
import crate.collection.memory;
import crate.auth.usercollection;
import ogm.issues.configuration;
import geo.json : GeoJsonGeometry;
import ogm.issues.api;

Token bearerToken;

void createUserData(UserCrateCollection userCollection) {
  UserModel user;
  user.firstName = "John";
  user.lastName = "Doe";
  user.username = "test";
  user.email = "user@gmail.com";
  user._id = "1";

  userCollection.createUser(user, "password");
  bearerToken = userCollection.createToken("user@gmail.com", Clock.currTime + 3600.seconds, ["doStuff"], "Bearer");
}

alias s = Spec!({
  URLRouter router;

  describe("Rejecting basic issues", {
    SysTime now;

    beforeEach({
      Authentication.reset();

      createCrates;

      createUserData(userCollection);

      auto team = Team(ObjectId.fromString("1"), "team1", Json("description"), ["user@gmail.com"]);
      createTeam(team);
      auto cover = createPicture("1");

      IconSet iconSet;
      iconSet._id = ObjectId.fromString("1");
      iconSet.visibility.team = team;
      iconSet.name = "set1";
      createIconSet(iconSet);

      auto map = createMap("1", "map1", Json(""), true, Polygon(), team, cover, [iconSet]);

      auto feature = Feature(ObjectId.fromString("1"), [map], "site1", Json("description"),
              GeoJsonGeometry.fromJson(`{ "type": "Point", "coordinates": [1.5, 1.5] }`.parseJsonString));
      createFeature(feature);

      IssuesConfiguration config;
      config.baseUrl = "http://localhost";
      config.location = "./tmp-issue-files";

      router = new URLRouter;
      router.crateSetup.setupIssueApi(crates, config);
    });

    beforeEach({
        now = Clock.currTime!(ClockType.second).toUTC;
        now.second = 0;
    });

    it("should be successful when the issue is open", {
      auto issue = `{
        "_id": "000000000000000000000001",
        "feature": "000000000000000000000001",
        "creationDate": "` ~ now.toISOExtString ~ `",
        "title": "title",
        "type": "none",
        "description": "description",
        "author": "000000000000000000000001"
      }`;

      crates.issue.addItem(issue.parseJsonString);

      request(router)
        .get("/issues/000000000000000000000001/reject")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectHeader("Access-Control-Allow-Origin", "*")
          .expectHeader("Access-Control-Allow-Methods", "GET, OPTIONS")
          .expectStatusCode(200)
          .end((Response response) => () {
            auto rejectedIssue = crates.issue.getItem("000000000000000000000001").exec.front;

            rejectedIssue["status"].should.equal("rejected");
            rejectedIssue["resolveDate"].should.equal(now.toISOExtString);
          });
    });

    it("should throw when the issue is rejected", {
      auto issue = `{
        "_id": "000000000000000000000001",
        "feature": "000000000000000000000001",
        "creationDate": "` ~ now.toISOExtString ~ `",
        "title": "title",
        "type": "none",
        "description": "description",
        "author": "000000000000000000000001",
        "status":"rejected"
      }`;

      crates.issue.addItem(issue.parseJsonString);

      request(router)
        .get("/issues/000000000000000000000001/reject")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectHeader("Access-Control-Allow-Origin", "*")
          .expectHeader("Access-Control-Allow-Methods", "GET, OPTIONS")
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal("{
              \"errors\":
                [{\"description\":\"You can not reject `rejected` issues.\",
                  \"status\":400,\"title\":\"Validation error\"}]}".parseJsonString);
          });
    });

    it("should throw when the issue is resolved", {
      auto issue = `{
        "_id": "000000000000000000000001",
        "feature": "000000000000000000000001",
        "creationDate": "` ~ now.toISOExtString ~ `",
        "title": "title",
        "type": "none",
        "description": "description",
        "author": "000000000000000000000001",
        "status":"resolved"
      }`;

      crates.issue.addItem(issue.parseJsonString);

      request(router)
        .get("/issues/000000000000000000000001/reject")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectHeader("Access-Control-Allow-Origin", "*")
          .expectHeader("Access-Control-Allow-Methods", "GET, OPTIONS")
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal("{
              \"errors\":
                [{\"description\":\"You can not reject `resolved` issues.\",
                  \"status\":400,\"title\":\"Validation error\"}]}".parseJsonString);
          });
    });
  });
});
