/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogmissue.tests.addimage;

import std.file;
import std.path;
import std.datetime;

import trial.discovery.spec;

import fluent.asserts;


import vibeauth.data.token;
import vibe.http.router;
import vibe.data.json;

import ogm.issues.api;
import ogm.crates.all;
import ogm.test.fixtures;

import crate.base;
import crate.collection.memory;
import crate.auth.usercollection;
import ogm.issues.configuration;
import geo.json : GeoJsonGeometry;
import ogm.issues.api;

Token bearerToken;

void createUserData(UserCrateCollection userCollection) {
  UserModel user;
  user.firstName = "John";
  user.lastName = "Doe";
  user.username = "test";
  user.email = "user@gmail.com";
  user._id = "1";

  userCollection.createUser(user, "password");
  bearerToken = userCollection.createToken("user@gmail.com", Clock.currTime + 3600.seconds, ["doStuff"], "Bearer");
}

alias s = Spec!({
  URLRouter router;

  describe("Adding image issues", {
    SysTime now;

    beforeEach({
      Authentication.reset();
      createCrates;

      createUserData(userCollection);

      auto team = Team(ObjectId.fromString("1"), "team1", Json("description"), ["user@gmail.com"]);
      createTeam(team);
      auto cover = createPicture("1");

      IconSet iconSet;
      iconSet._id = ObjectId.fromString("1");
      iconSet.visibility.team = team;
      iconSet.name = "set1";
      createIconSet(iconSet);

      auto map = createMap("000000000000000000000001", "map1", Json(""), true, Polygon(), team, cover, [iconSet]);

      auto feature = Feature(ObjectId.fromString("1"), [map], "site1", Json("description"),
              GeoJsonGeometry.fromJson(`{ "type": "Point", "coordinates": [1.5, 1.5] }`.parseJsonString));
      createFeature(feature);

      IssuesConfiguration config;
      config.baseUrl = "http://localhost";
      config.location = "./tmp-issue-files";

      router = new URLRouter;
      router.crateSetup.setupIssueApi(crates, config);
    });

    beforeEach({
        now = Clock.currTime!(ClockType.second).toUTC;
        now.second = 0;
    });

    after({
      if(IssueFileSettings.path.exists) {
        IssueFileSettings.path.rmdirRecurse;
      }
    });

    describe("with newImage type", {
      it("should upload the image", {
        Json data = `{ "issue": {
          "feature": "000000000000000000000001",
          "title": "image",
          "file": "data:image/jpeg;base64,",
          "type": "newImage"
        }}`.parseJsonString;

        request(router)
          .post("/issues")
            .header("Authorization", "Bearer " ~ bearerToken.name)
            .expectHeader("Access-Control-Allow-Origin", "*")
            .expectHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
            .expectStatusCode(200)
            .send(data)
            .end((Response response) => () {
              auto item = crates.issue.getItem("000000000000000000000001").exec.front;
              item["file"].should.not.equal("");

              response.bodyJson.should.equal((`{ "issue": { "feature":"000000000000000000000001",
                "creationDate": "` ~ response.bodyJson["issue"]["creationDate"].to!string ~ `",
                "other": "",
                "title": "image",
                "_id": "000000000000000000000001",
                "assignee": "",
                "attributions": "",
                "resolveDate": "0001-01-01T00:00:00+00:00",
                "processed": false,
                "description": "",
                "type": "newImage",
                "file": "http://localhost/issues/000000000000000000000001/file",
                "author": "000000000000000000000001",
                "status": "open"
                }}`).parseJsonString);
            });
      });

      it("shold return an error if the file is missing", {
        Json data = `{ "issue": {
          "feature": "000000000000000000000001",
          "title": "image",
          "type": "newImage"
        }}`.parseJsonString;

        request(router)
          .post("/issues")
            .header("Authorization", "Bearer " ~ bearerToken.name)
            .expectStatusCode(400)
            .send(data)
            .end((Response response) => () {
              response.bodyJson.should.equal((`{"errors": [{
                "description": "` ~ "The `file` is missing." ~ `",
                "status": 400,
                "title": "Validation error"
              }]}`).parseJsonString);
            });
      });
    });
  });
});
