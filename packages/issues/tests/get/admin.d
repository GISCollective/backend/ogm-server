/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.issues.get.admin;

import tests.fixtures;
import tests.data.features;
import vibe.http.common;
import ogm.issues.api;

import ogm.issues.configuration;

alias suite = Spec!({
  URLRouter router;

  describe("when there is an admin token", {
    beforeEach({
      setupTestData();
      router = new URLRouter;

      IssuesConfiguration config;
      config.baseUrl = "http://localhost";
      config.location = "./tmp-issue-files";

      router.crateSetup.setupIssueApi(crates, config);
    });

    describe("for a feature with no issues belonging to a public map", {
      it("should return an error with the required `feature` query param when it is not provided", {
        router
          .request
          .get("/issues")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal(
              parseJsonString(`{"errors":[{"description":"The ` ~ "`feature`" ~ ` query parameter is missing.","status":400,"title":"Validation error"}]}`));
          });
      });

      it("should return an empty list", {
        router
          .request
          .get("/issues?feature=000000000000000000000001")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(
              parseJsonString(`{"issues": []}`));
          });
      });
    });

    describe("for a feature with no issues belonging to a private map", {
      it("should return an empty list", {
        router
          .request
          .get("/issues?feature=000000000000000000000004")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(
              parseJsonString(`{"issues": []}`));
          });
      });
    });

    describe("for a feature with an issue belonging to a private map", {
      beforeEach({
        Issue issue;
        issue.feature = "000000000000000000000004";

        createIssue(issue);
      });

      it("should return the issue when the query param contains the feature with issues", {
        router
          .request
          .get("/issues?feature=000000000000000000000004")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(
              parseJsonString(`{"issues": [{
                "feature": "000000000000000000000004",
                "creationDate": "0001-01-01T00:00:00+00:00",
                "other": "",
                "title": "",
                "attributions": "",
                "_id": "000000000000000000000001",
                "assignee": "",
                "resolveDate": "0001-01-01T00:00:00+00:00",
                "processed": false,
                "description": "",
                "type": "none",
                "author": "",
                "status": "open"
              }]}`));
          });
      });

      it("should return an empty list when the query param contains the feature without issues", {
        router
          .request
          .get("/issues?feature=000000000000000000000001")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(
              parseJsonString(`{"issues": []}`));
          });
      });
    });
  });
});