/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.issues.get.notoken;


import tests.fixtures;
import tests.data.features;
import vibe.http.common;
import ogm.issues.api;

import ogm.issues.configuration;

alias suite = Spec!({
  URLRouter router;

  describe("when there is no token", {
    beforeEach({
      setupTestData();
      router = new URLRouter;

      IssuesConfiguration config;
      config.baseUrl = "http://localhost";
      config.location = "./tmp-issue-files";

      router.crateSetup.setupIssueApi(crates, config);
    });

    describe("for a feature with no issues belonging to a public map", {
      it("should return an authentication error", {
        router
          .request
          .get("/issues")
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(
              parseJsonString(`{"error": "Authorization required"}`));
          });
      });
    });

    describe("for a feature with no issues belonging to a private map", {
      it("should return an authentication error", {
        router
          .request
          .get("/issues?feature=000000000000000000000004")
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(
              parseJsonString(`{"error": "Authorization required"}`));
          });
      });
    });
  });
});