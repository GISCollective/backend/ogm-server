/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.teams.invite;

import tests.fixtures;

import std.algorithm;
import std.array;
import ogm.defaults.teams;
import ogm.teams.api;
import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({
  URLRouter router;
  Json invitationMessage;
  Json welcomeMessage;

  describe("invite", {
    beforeEach({
      setupTestData();
      router = new URLRouter;

      auto broadcast = new MemoryBroadcast;

      void messageInvitationHandler(const Json value) {
        invitationMessage = value;
      }

      void messageWelcomeHandler(const Json value) {
        welcomeMessage = value;
      }

      invitationMessage = Json.emptyObject;
      welcomeMessage = Json.emptyObject;
      broadcast.register("team.invitation", &messageInvitationHandler);
      broadcast.register("team.welcome", &messageWelcomeHandler);

      router.crateSetup.setupTeamApi(crates, broadcast);
      setupDefaultTeams(crates);
    });

    describeCredentialsRule("can invite a member", "yes", "teams", ["administrator", "owner"], (string type) {
      auto data = `{
        "emailList": [ "a@b.c" ],
        "role": "members"
      }`.parseJsonString;

      router
        .request
        .post("/teams/000000000000000000000001/invite")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(data)
        .expectStatusCode(201)
          .end((Response response) => () {
            auto teams = crates.team.getItem("000000000000000000000001").exec.front;

            teams["invitations"].should.equal(`[{
              "email": "a@b.c",
              "role": "members"
            }]`.parseJsonString);
          });
    });

    describeCredentialsRule("can invite a member", "yes", "teams", ["leader", "member", "guest"], (string type) {
      auto data = `{
        "emailList": [ "a@b.c" ],
        "role": "members"
      }`.parseJsonString;

      router
        .request
        .post("/teams/000000000000000000000001/invite")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .send(data)
        .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "errors": [{
                "description": "You can't invite people to this team.",
                "status": 403,
                "title": "Forbidden"
              }]
            }`.parseJsonString);

            auto teams = crates.team.getItem("000000000000000000000001").exec.front;

            teams["invitations"].should.equal(`[]`.parseJsonString);
          });
    });

    it("ignores an email if it is already a team member", {
      auto data = `{
        "emailList": [ "owner@gmail.com" ],
        "role": "members"
      }`.parseJsonString;

      router
        .request
        .post("/teams/000000000000000000000001/invite")
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .send(data)
        .expectStatusCode(201)
          .end((Response response) => () {
            auto teams = crates.team.getItem("000000000000000000000001").exec.front;

            teams["invitations"].should.equal(`[]`.parseJsonString);
          });
    });

    it("adds an email only once", {
      auto data = `{
        "emailList": [ "a@gmail.com", "a@gmail.com", "b@gmail.com" ],
        "role": "members"
      }`.parseJsonString;

      router
        .request
        .post("/teams/000000000000000000000001/invite")
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .send(data)
        .expectStatusCode(201)
          .end((Response response) => () {
            auto teams = crates.team.getItem("000000000000000000000001").exec.front;

            teams["invitations"].should.equal(`[{
              "email": "a@gmail.com",
              "role": "members"
            }, {
              "email": "b@gmail.com",
              "role": "members"
            }]`.parseJsonString);
          });
    });

    it("ignores a value when it is not a string", {
      auto data = `{
        "emailList": [ 2 ],
        "role": "members"
      }`.parseJsonString;

      router
        .request
        .post("/teams/000000000000000000000001/invite")
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .send(data)
        .expectStatusCode(201)
          .end((Response response) => () {
            auto teams = crates.team.getItem("000000000000000000000001").exec.front;

            teams["invitations"].should.equal(`[]`.parseJsonString);
          });
    });

    it("automatically adds an email to the team when it has an account", {
      auto data = `{
        "emailList": [ "other@gmail.com" ],
        "role": "members"
      }`.parseJsonString;

      router
        .request
        .post("/teams/000000000000000000000001/invite")
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .send(data)
        .expectStatusCode(201)
          .end((Response response) => () {
            auto teams = crates.team.getItem("000000000000000000000001").exec.front;

            teams["members"].should.equal(`["member@gmail.com", "000000000000000000000002", "000000000000000000000006"]`.parseJsonString);
            teams["invitations"].should.equal(`[]`.parseJsonString);
          });
    });

    it("returns an error ", {
      auto data = `{
        "emailList": [ "other@gmail.com" ],
        "role": "unknown"
      }`.parseJsonString;

      router
        .request
        .post("/teams/000000000000000000000001/invite")
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .send(data)
        .expectStatusCode(400)
          .end((Response response) => () {
            auto teams = crates.team.getItem("000000000000000000000001").exec.front;

            teams["members"].should.equal(`["member@gmail.com", "000000000000000000000002"]`.parseJsonString);
            teams["invitations"].should.equal(`[]`.parseJsonString);

            response.bodyJson.should.equal(`{
              "errors": [{
                "description": "Invalid role. It must be one of 'owners', 'leaders', 'members', 'guests'.",
                "status": 400,
                "title": "Validation error"
              }]
            }`.parseJsonString);
          });
    });

    it("sends a registration message when the user does not exist", {
      auto data = `{
        "emailList": [ "new@gmail.com" ],
        "role": "members"
      }`.parseJsonString;

      router
        .request
        .post("/teams/000000000000000000000001/invite")
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .send(data)
        .expectStatusCode(201)
          .end((Response response) => () {
            invitationMessage.should.equal(`{
              "id": "000000000000000000000001.new@gmail.com",
              "team": "000000000000000000000001",
              "invitation": {
                "email": "new@gmail.com",
                "role": "members"
              }
            }`.parseJsonString);
          });
    });

    it("sends a welcome message when the user exists", {
      auto data = `{
        "emailList": [ "other@gmail.com" ],
        "role": "members"
      }`.parseJsonString;

      router
        .request
        .post("/teams/000000000000000000000001/invite")
        .header("Authorization", "Bearer " ~ userTokenList["owner"])
        .send(data)
        .expectStatusCode(201)
          .end((Response response) => () {
            welcomeMessage.should.equal(`{
              "id": "000000000000000000000001.other@gmail.com",
              "role": "members",
              "team": "000000000000000000000001",
              "user": "000000000000000000000006"
            }`.parseJsonString);
          });
    });
  });
});