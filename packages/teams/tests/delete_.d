/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.teams.delete_;

import tests.fixtures;
import crate.base;
import std.algorithm;
import std.array;
import ogm.defaults.teams;
import ogm.teams.api;
import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({
  URLRouter router;

  describe("DELETE", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      crates.picture.addDefault("image/jpeg", "cover", "./deploy/cover.jpg");

      auto broadcast = new MemoryBroadcast;
      router.crateSetup.setupTeamApi(crates, broadcast);
      setupDefaultTeams(crates);
    });

    describe("without a token", {
      it("should not be able to delete a team", {
        router
          .request
          .delete_("/teams/000000000000000000000002")
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
          });
      });
    });

    describe("with an invalid token", {
      it("should not be able to delete a team", {
        router
          .request
          .delete_("/teams/000000000000000000000002")
          .header("Authorization", "Bearer random")
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"error\": \"Invalid token.\"}".parseJsonString);
          });
      });
    });

    describe("with an admin token", {
      it("should be able to delete a team" , {
        router
          .request
          .delete_("/teams/000000000000000000000002")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(204)
          .end();
      });

      describe("for a team with a picture", {
        Json result;

        beforeEach({
          Picture picture;
          picture.name = "custom picture";
          picture.owner = "";
          picture.picture = new PictureFile;

          auto result = crates.picture.addItem(picture.serializeToJson);
          picture._id = ObjectId.fromString(result["_id"].to!string);

          auto team = crates.team.getItem("000000000000000000000002").exec.front;
          team["pictures"] = Json.emptyArray;
          team["pictures"] ~= result["_id"];

          crates.team.updateItem(team);
        });

        it("should delete the picture", {
          router
            .request
            .delete_("/teams/000000000000000000000002")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(204)
            .end((Response response) => () {
              crates.picture.get.exec
                .filter!(a => a["name"] == "custom picture")
                  .array.length.should.equal(0);
            });
        });
      });

      describe("for a team with a custom logo", {
        Json result;

        beforeEach({
          Picture customCover;
          customCover.name = "custom cover";
          customCover.owner = "";
          customCover.picture = new PictureFile;
          auto result = crates.picture.addItem(customCover.serializeToJson);
          customCover._id = ObjectId.fromString(result["_id"].to!string);

          auto team = crates.team.getItem("000000000000000000000002").exec.front;
          team["logo"] = result["_id"];
          crates.team.updateItem(team);
        });

        it("should delete the logo", {
          router
            .request
            .delete_("/teams/000000000000000000000002")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(204)
            .end((Response response) => () {
              crates.picture.get.exec
                .filter!(a => a["name"] == "custom cover")
                  .array.length.should.equal(0);
            });
        });
      });

      describe("for a team with a default logo", {
        Json result;

        beforeEach({
          Picture customCover;
          customCover.name = "system cover";
          customCover.owner = "@system";
          customCover.picture = new PictureFile;
          result = crates.picture.addItem(customCover.serializeToJson);
          customCover._id = ObjectId.fromString(result["_id"].to!string);

          auto team = crates.team.getItem("000000000000000000000002").exec.front;
          team["logo"] = result["_id"];
          crates.team.updateItem(team);
        });

        it("should not delete the cover map", {
          router
            .request
            .delete_("/teams/000000000000000000000002")
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(204)
            .end((Response response) => () {
              crates.picture.get.exec
                .filter!(a => a["_id"] == result["_id"])
                  .array.length.should.equal(1);
            });
        });
      });
    });

    describe("with an owner token", {
      it("should be able to delete a team" , {
        router
          .request
          .delete_("/teams/000000000000000000000002")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(204)
          .end();
      });

      it("should not be able to delete a team where the user is not contributor" , {
        router
          .request
          .delete_("/teams/000000000000000000000003")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(404)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"errors": [{
              "description": "Team was not found",
              "status": 404,
              "title": "Crate not found"}]}`.parseJsonString);
          });
      });
    });

    describe("with a leader token", {
      it("should not be able to delete a team" , {
        router
          .request
          .delete_("/teams/000000000000000000000002")
          .header("Authorization", "Bearer " ~ bearerLeaderToken.name)
          .expectStatusCode(404)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"errors": [{
              "description": "Team was not found",
              "status": 404,
              "title": "Crate not found"}]}`.parseJsonString);
          });
      });

      it("should not be able to update a team where the user is not contributor" , {
        router
          .request
          .delete_("/teams/000000000000000000000003")
          .header("Authorization", "Bearer " ~ bearerLeaderToken.name)
          .expectStatusCode(404)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"errors": [{
              "description": "Team was not found",
              "status": 404,
              "title": "Crate not found"}]}`.parseJsonString);
          });
      });
    });
  });
});
