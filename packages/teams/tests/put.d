/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.teams.put;

import tests.fixtures;

import std.algorithm;
import std.array;
import ogm.defaults.teams;
import ogm.teams.api;
import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({
  URLRouter router;
  Json team;

  describe("PUT", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      crates.picture.addDefault("image/jpeg", "cover", "./deploy/cover.jpg");

      auto broadcast = new MemoryBroadcast;
      router.crateSetup.setupTeamApi(crates, broadcast);
      setupDefaultTeams(crates);

      team = `{
        "team": {
          "name": "updated team",
          "about": "",
          "isPublic": true,
          "logo": "000000000000000000000002",
          "isDefault": false,
          "isPublisher": false,
          "owners": [ "owner@gmail.com" ],
          "members": [],
          "leaders": [],
          "guests": [],
          "pictures": [],
          "categories": []
        }}`.parseJsonString;
    });

    describeCredentialsRule("update own teams", "yes", "teams", ["administrator", "owner"], (string type) {
      auto data = team.clone;
      data["team"]["members"] = [ Json("admin@gmail.com") ];
      router
        .request
        .put("/teams/000000000000000000000002")
        .send(data)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto result = team.clone;
          result["team"]["_id"] = "000000000000000000000002";
          result["team"]["owners"] = [ Json("000000000000000000000004") ];
          result["team"]["members"] = [ Json("000000000000000000000005") ];
          result["team"]["canEdit"] = true;
          result["team"]["allowCustomDataBindings"] = false;
          result["team"]["allowReports"] = false;
          result["team"]["allowNewsletters"] = false;
          result["team"]["invitations"] = Json.emptyArray;
          result["team"]["allowEvents"] = false;
          result["team"]["subscription"] = `{
            "comment": "",
            "domains": [],
            "expire": "0001-01-01T00:00:00+00:00",
            "name": "",
            "details": "",
            "support": [],
            "invoices": [],
            "monthlySupportHours": 0
          }`.parseJsonString;
          result["team"]["socialMediaLinks"] = `{
            "facebook": "",
            "instagram": "",
            "linkedin": "",
            "tikTok": "",
            "whatsApp": "",
            "xTwitter": "",
            "youtube": ""
          }`.parseJsonString;
          result["team"]["contactEmail"] = ``;

          response.bodyJson.should.equal(result);
        });
    });

    describeCredentialsRule("update own teams", "no", "teams", ["leader", "member", "guest"], (string type) {
      auto data = team.clone;
      data["team"]["owners"] = [ Json("admin@gmail.com") ];
      router
        .request
        .put("/teams/000000000000000000000002")
        .send(data)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(404)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"errors": [{
            "description": "Team was not found",
            "status": 404,
            "title": "Crate not found"
          }]}`.parseJsonString);
        });
    });

    describeCredentialsRule("update own teams", "-", "teams", "no rights", {
      router
        .request
        .put("/teams/000000000000000000000002")
        .send(team)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
        });
    });


    describeCredentialsRule("update other teams", "yes", "teams", ["administrator"], (string type) {
      auto data = team.clone;
      data["team"]["members"] = [ Json("admin@gmail.com") ];
      router
        .request
        .put("/teams/000000000000000000000003")
        .send(data)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto result = team.clone;
          result["team"]["_id"] = "000000000000000000000003";
          result["team"]["owners"] = [ Json("000000000000000000000004") ];
          result["team"]["members"] = [ Json("000000000000000000000005") ];
          result["team"]["invitations"] = Json.emptyArray;
          result["team"]["canEdit"] = true;
          result["team"]["allowCustomDataBindings"] = false;
          result["team"]["allowNewsletters"] = false;
          result["team"]["allowReports"] = false;
          result["team"]["allowEvents"] = false;
          result["team"]["subscription"] = `{
            "comment": "",
            "domains": [],
            "expire": "0001-01-01T00:00:00+00:00",
            "name": "",
            "details": "",
            "support": [],
            "invoices": [],
            "monthlySupportHours": 0
          }`.parseJsonString;
          result["team"]["socialMediaLinks"] = `{
            "facebook": "",
            "instagram": "",
            "linkedin": "",
            "tikTok": "",
            "whatsApp": "",
            "xTwitter": "",
            "youtube": ""
          }`.parseJsonString;
          result["team"]["contactEmail"] = ``;

          response.bodyJson.should.equal(result);
        });
    });

    describeCredentialsRule("update other teams", "no", "teams", ["owner", "leader", "member", "guest"], (string type) {
      auto data = team.clone;
      data["team"]["owners"] = [ Json("admin@gmail.com") ];
      router
        .request
        .put("/teams/000000000000000000000003")
        .send(data)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(404)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"errors": [{
            "description": "Team was not found",
            "status": 404,
            "title": "Crate not found"
          }]}`.parseJsonString);
        });
    });


    describeCredentialsRule("update the isPublisher value", "yes", "teams", ["administrator"], (string type) {
      auto data = team.clone;
      data["team"]["isPublisher"] = true;

      router
        .request
        .put("/teams/000000000000000000000002")
        .send(data)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto result = team.clone;
          result["team"]["_id"] = "000000000000000000000002";
          result["team"]["owners"] = [ Json("000000000000000000000004") ];
          result["team"]["invitations"] = Json.emptyArray;
          result["team"]["isPublisher"] = true;
          result["team"]["canEdit"] = true;
          result["team"]["allowCustomDataBindings"] = false;
          result["team"]["allowNewsletters"] = false;
          result["team"]["allowReports"] = false;
          result["team"]["allowEvents"] = false;
          result["team"]["subscription"] = `{
            "comment": "",
            "domains": [],
            "expire": "0001-01-01T00:00:00+00:00",
            "name": "",
            "details": "",
            "support": [],
            "invoices": [],
            "monthlySupportHours": 0
          }`.parseJsonString;
          result["team"]["socialMediaLinks"] = `{
            "facebook": "",
            "instagram": "",
            "linkedin": "",
            "tikTok": "",
            "whatsApp": "",
            "xTwitter": "",
            "youtube": ""
          }`.parseJsonString;
          result["team"]["contactEmail"] = ``;


          response.bodyJson.should.equal(result);
        });
    });

    describeCredentialsRule("update the isPublisher value", "no", "teams", ["owner"], (string type) {
      auto data = team.clone;
      data["team"]["isPublisher"] = true;

      router
        .request
        .put("/teams/000000000000000000000002")
        .send(data)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto result = team.clone;
          result["team"]["_id"] = "000000000000000000000002";
          result["team"]["owners"] = [ Json("000000000000000000000004") ];
          result["team"]["invitations"] = Json.emptyArray;
          result["team"]["isPublisher"] = false;
          result["team"]["canEdit"] = true;
          result["team"]["allowCustomDataBindings"] = false;
          result["team"]["allowNewsletters"] = false;
          result["team"]["allowEvents"] = false;
          result["team"]["allowReports"] = false;
          result["team"]["subscription"] = `{
            "comment": "",
            "domains": [],
            "expire": "0001-01-01T00:00:00+00:00",
            "name": "",
            "details": "",
            "support": [],
            "invoices": [],
            "monthlySupportHours": 0
          }`.parseJsonString;
          result["team"]["socialMediaLinks"] = `{
            "facebook": "",
            "instagram": "",
            "linkedin": "",
            "tikTok": "",
            "whatsApp": "",
            "xTwitter": "",
            "youtube": ""
          }`.parseJsonString;
          result["team"]["contactEmail"] = ``;

          response.bodyJson.should.equal(result);
        });
    });

    describeCredentialsRule("update the isPublisher value", "no", "teams", ["leader", "member", "guest"], (string type) {
      auto data = team.clone;
      data["team"]["isPublisher"] = true;

      router
        .request
        .put("/teams/000000000000000000000002")
        .send(data)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(404)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"errors": [{
            "description": "Team was not found",
            "status": 404,
            "title": "Crate not found"
          }]}`.parseJsonString);
        });
    });

    describeCredentialsRule("update the isPublisher value", "-", "teams", "no rights", {
      router
        .request
        .put("/teams/000000000000000000000002")
        .send(team)
        .expectStatusCode(401)
        .end;
    });

    describeCredentialsRule("update the invitations", "yes", "teams", ["administrator", "owner"], (string type) {
      auto data = team.clone;
      team["invitations"] = `[{
        "email": "a@b.c",
        "role": "leaders"
      }]`.parseJsonString;

      data["team"]["invitations"] = Json.emptyArray;

      router
        .request
        .put("/teams/000000000000000000000002")
        .send(data)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["team"]["invitations"].should.equal(Json.emptyArray);
        });
    });

    describeCredentialsRule("update the invitations", "no", "teams", ["leader", "member", "guest"], (string type) {
      auto data = team.clone;
      team["invitations"] = `[{
        "email": "a@b.c",
        "role": "leaders"
      }]`.parseJsonString;

      data["team"]["invitations"] = Json.emptyArray;

      router
        .request
        .put("/teams/000000000000000000000002")
        .send(data)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(404)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{ "errors": [{
            "description": "Team was not found",
            "status": 404,
            "title": "Crate not found"
          }]}`.parseJsonString);
        });
    });

    describeCredentialsRule("update the invitations", "-", "teams", "no rights", {
      router
        .request
        .put("/teams/000000000000000000000002")
        .send(team)
        .expectStatusCode(401)
        .end;
    });

    describeCredentialsRule("update other teams", "-", "teams", "no rights", { });


    describe("with an owner token", {
      it("can not change the allowCustomDataBindings flag to true", {
        auto data = team.clone;
        data["team"]["allowCustomDataBindings"] = true;

        router
          .request
          .put("/teams/000000000000000000000002")
          .send(data)
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["team"]["allowCustomDataBindings"].should.equal(false);
          });
      });

      it("can not change the allowNewsletters flag to true", {
        auto data = team.clone;
        data["team"]["allowNewsletters"] = true;

        router
          .request
          .put("/teams/000000000000000000000002")
          .send(data)
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["team"]["allowNewsletters"].should.equal(false);
          });
      });

      it("can not change the allowEvents flag to true", {
        auto data = team.clone;
        data["team"]["allowEvents"] = true;

        router
          .request
          .put("/teams/000000000000000000000002")
          .send(data)
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["team"]["allowEvents"].should.equal(false);
          });
      });

      it("can not change the allowReports flag to true", {
        auto data = team.clone;
        data["team"]["allowReports"] = true;

        router
          .request
          .put("/teams/000000000000000000000002")
          .send(data)
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["team"]["allowReports"].should.equal(false);
          });
      });

      it("can not change the subscription section", {
        auto data = team.clone;
        data["team"]["subscription"] = SubscriptionDetails("standard").serializeToJson;

        router
          .request
          .put("/teams/000000000000000000000002")
          .send(data)
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["team"]["subscription"]["name"].to!string.should.equal("");
          });
      });
    });

    describe("with an admin token", {
      describe("for a team with a picture", {
        Json result;

        beforeEach({
          Picture picture;
          picture.name = "custom picture";
          picture.owner = "";
          picture.picture = new PictureFile;

          auto result = crates.picture.addItem(picture.serializeToJson);
          picture._id = ObjectId.fromString(result["_id"].to!string);

          auto team = crates.team.getItem("000000000000000000000002").exec.front;
          team["pictures"] = Json.emptyArray;
          team["pictures"] ~= result["_id"];

          crates.team.updateItem(team);
        });

        it("should delete the original picture", {
          auto data = team.clone;

          data["team"]["pictures"] = Json.emptyArray;
          data["team"]["pictures"] ~= "000000000000000000000001";

          router
            .request
            .put("/teams/000000000000000000000002")
            .send(data)
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              crates.picture.get.exec
                .filter!(a => a["name"] == "custom picture")
                  .array.length.should.equal(0);
            });
        });

        it("should not delete the original picture if it's not removed from the picture list", {
          auto data = team.clone;
          data["team"]["pictures"] ~= "000000000000000000000003";
          data["team"]["pictures"] ~= "000000000000000000000001";

          router
            .request
            .put("/teams/000000000000000000000002")
            .send(data)
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              crates.picture.get.exec
                .filter!(a => a["name"] == "custom picture")
                  .array.length.should.equal(1);
            });
        });
      });

      describe("for a team with a custom logo", {
        Json result;

        beforeEach({
          Picture customCover;
          customCover.name = "custom cover";
          customCover.owner = "";
          customCover.picture = new PictureFile;
          result = crates.picture.addItem(customCover.serializeToJson);
          customCover._id = ObjectId.fromString(result["_id"].to!string);

          auto team = crates.team.getItem("000000000000000000000002").exec.front;
          team["logo"] = result["_id"];
          crates.team.updateItem(team);
        });

        it("should delete the old team logo", {
          auto data = team.clone;
          data["team"]["logo"] = "000000000000000000000001";

          router
            .request
            .put("/teams/000000000000000000000002")
            .send(data)
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              crates.picture.get.exec
                .filter!(a => a["_id"] == result["_id"])
                  .array.length.should.equal(0);
            });
        });
      });

      describe("for a team with a default logo", {
        Json result;

        beforeEach({
          Picture customCover;
          customCover.name = "system cover";
          customCover.owner = "@system";
          customCover.picture = new PictureFile;
          result = crates.picture.addItem(customCover.serializeToJson);
          customCover._id = ObjectId.fromString(result["_id"].to!string);

          auto team = crates.iconSet.getItem("000000000000000000000002").exec.front;

          team["logo"] = result["_id"];
          crates.team.updateItem(team);
        });

        it("should not delete the old team logo", {
          auto data = team.clone;
          data["team"]["logo"] = "000000000000000000000001";

          router
            .request
            .put("/teams/000000000000000000000002")
            .send(data)
            .header("Authorization", "Bearer " ~ administratorToken.name)
            .expectStatusCode(200)
            .end((Response response) => () {
              crates.picture.get.exec
                .filter!(a => a["_id"] == result["_id"])
                  .array.length.should.equal(1);
            });
        });
      });

      it("can change the allowCustomDataBindings flag", {
        auto data = team.clone;
        data["team"]["allowCustomDataBindings"] = true;

        router
          .request
          .put("/teams/000000000000000000000002")
          .send(data)
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["team"]["allowCustomDataBindings"].should.equal(true);
          });
      });

      it("can change the allowNewsletters flag", {
        auto data = team.clone;
        data["team"]["allowNewsletters"] = true;

        router
          .request
          .put("/teams/000000000000000000000002")
          .send(data)
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["team"]["allowNewsletters"].should.equal(true);
          });
      });

      it("can change the allowEvents flag", {
        auto data = team.clone;
        data["team"]["allowEvents"] = true;

        router
          .request
          .put("/teams/000000000000000000000002")
          .send(data)
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["team"]["allowEvents"].should.equal(true);
          });
      });

      it("can change the allowReports flag", {
        auto data = team.clone;
        data["team"]["allowReports"] = true;

        router
          .request
          .put("/teams/000000000000000000000002")
          .send(data)
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["team"]["allowReports"].should.equal(true);
          });
      });

      it("can change the subscription section", {
        auto data = team.clone;
        data["team"]["subscription"] = SubscriptionDetails("standard").serializeToJson;

        router
          .request
          .put("/teams/000000000000000000000002")
          .send(data)
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["team"]["subscription"]["name"].to!string.should.equal("standard");
          });
      });
    });
  });
});
