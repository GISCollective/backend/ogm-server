/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.teams.get;

import tests.fixtures;

import std.algorithm;
import std.array;
import ogm.defaults.teams;
import ogm.teams.api;
import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({
  URLRouter router;

  describe("GET", {
    Json item1, item2, item3, item4;

    beforeEach({
      setupTestData();
      router = new URLRouter;
      crates.picture.addDefault("image/jpeg", "cover", "./deploy/cover.jpg");

      auto broadcast = new MemoryBroadcast;
      router.crateSetup.setupTeamApi(crates, broadcast);
      setupDefaultTeams(crates);

      item1 = crates.team.getItem("000000000000000000000001").exec.front.to!string.parseJsonString;
      item2 = crates.team.getItem("000000000000000000000002").exec.front.to!string.parseJsonString;
      item3 = crates.team.getItem("000000000000000000000003").exec.front.to!string.parseJsonString;
      item4 = crates.team.getItem("000000000000000000000004").exec.front.to!string.parseJsonString;

      item1["guests"] = ["000000000000000000000003"].serializeToJson;
      item1["members"] = ["000000000000000000000002"].serializeToJson;
      item1["owners"] = ["000000000000000000000004"].serializeToJson;
      item1["leaders"] = ["000000000000000000000001"].serializeToJson;

      item2["guests"] = ["000000000000000000000003"].serializeToJson;
      item2["members"] = ["000000000000000000000002"].serializeToJson;
      item2["owners"] = ["000000000000000000000004"].serializeToJson;
      item2["leaders"] = ["000000000000000000000001"].serializeToJson;
    });

    describeCredentialsRule("query by members", "yes", "teams", ["owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/teams?user=" ~ userId[type])
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
          .end((Response response) => () {
            if(type == "owner") {
              item1["canEdit"] = true;
              item2["canEdit"] = true;
            }

            auto teams = cast(Json[]) response.bodyJson["teams"];
            teams.length.should.equal(2);
            teams.should.contain(item1);
            teams.should.contain(item2);
          });
    });

    describeCredentialsRule("query by members", "yes", "teams", "administrator", (string type) {
      router
        .request
        .get("/teams?user=" ~ userId[type])
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
          .end((Response response) => () {
            auto teams = cast(Json[]) response.bodyJson["teams"];
            teams.length.should.equal(0);
          });
    });

    describeCredentialsRule("query by members", "yes", "teams", "no rights", {
      router
        .request
        .get("/teams?user=000000000000000000000001")
        .expectStatusCode(200)
          .end((Response response) => () {
            auto teams = cast(Json[]) response.bodyJson["teams"];
            teams.length.should.equal(1);
            teams.should.contain(item1);
          });
    });

    describe("for a team with a subscription", {
      beforeEach({
        auto team = crates.team.getItem("000000000000000000000001").and.exec.front;
        team["subscription"]["name"] = "custom subscription";

        crates.team.updateItem(team);
      });

      describeCredentialsRule("can see subscription details", "yes", "teams", ["administrator", "owner", "leader"], (string type) {
        router
          .request
          .get("/teams/000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
            .end((Response response) => () {
              import std.stdio;
              writeln(response.bodyString);

              response.bodyJson["team"]["subscription"]["name"].to!string.should.equal(`custom subscription`);
            });
      });

      describeCredentialsRule("can see subscription details", "no", "teams", ["member", "guest"], (string type) {
        router
          .request
          .get("/teams/000000000000000000000001")
          .header("Authorization", "Bearer " ~ userTokenList[type])
          .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["team"]["subscription"]["name"].to!string.should.equal(``);
            });
      });

      describeCredentialsRule("can see subscription details", "no", "teams", "no rights", {
        router
          .request
          .get("/teams/000000000000000000000001")
          .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson["team"]["subscription"]["name"].to!string.should.equal(``);
            });
      });
    });

    it("can query all the teams for an user as an administrator", {
      router
        .request
        .get("/teams?user=" ~ userId["leader"] ~ "&all=true")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .expectStatusCode(200)
          .end((Response response) => () {
            auto teams = cast(Json[]) response.bodyJson["teams"];

            teams.length.should.equal(2);
          });
    });

    it("can query all the teams for an user as a regular user", {
      router
        .request
        .get("/teams?user=" ~ userId["leader"] ~ "&all=true")
        .expectStatusCode(200)
          .end((Response response) => () {
            auto teams = cast(Json[]) response.bodyJson["teams"];

            teams.length.should.equal(1);
          });
    });

    describe("for a team that has a deleted member", {
      beforeEach({
        item1["members"] = ["000000000000000000000002", "00000000000000000000000a"].serializeToJson;
        crates.team.updateItem(item1);
      });

      it("should return only users that have existing accounts", {
        router
          .request
          .get("/teams/000000000000000000000001")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["team"]["members"].should.equal(["000000000000000000000002"].serializeToJson);
          });
      });
    });

    describe("without a token", {
      it("should return the public teams", {
        router
          .request
          .get("/teams")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal((`{"teams": [` ~ item1.to!string ~ `, ` ~ item3.to!string ~ `]}`).parseJsonString);
          });
      });

      it("should get a team by id", {
        router
          .request
          .get("/teams/000000000000000000000001")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["team"].should.equal(item1);
          });
      });

      it("should return an empty list on getting teams when a token is not provided and the `edit` query string is present", {
        router
          .request
          .get("/teams?edit=true")
          .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal(`{"teams": []}`.parseJsonString);
            });
      });

      it("should return an error on getting a private team without a token", {
        router
          .request
          .get("/teams/000000000000000000000002")
          .expectStatusCode(404)
            .end;
      });
    });

    describe("with an invalid token", {
      it("should not return any team", {
        router
          .request
          .get("/teams")
          .header("Authorization", "Bearer random")
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"error\": \"Invalid token.\"}".parseJsonString);
          });
      });
    });

    describe("with an admin token", {
      it("should return all teams" , {
        router
          .request
          .get("/teams")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            item1["canEdit"] = true;
            item3["canEdit"] = true;

            auto teams = cast(Json[]) response.bodyJson["teams"];
            teams.length.should.equal(2);

            teams.should.contain(item1);
            teams.should.contain(item3);
          });
      });

      it("should search in all teams when all=true and return find a team by a term " , {
        router
          .request
          .get("/teams?term=team2&all=true")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            item2["canEdit"] = true;

            auto teams = cast(Json[]) response.bodyJson["teams"];
            teams.length.should.equal(1);

            teams.should.contain(item2);
          });
      });

      it("should not find private teams" , {
        router
          .request
          .get("/teams?term=team2&all=true")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            item2["canEdit"] = true;

            auto teams = cast(Json[]) response.bodyJson["teams"];
            teams.length.should.equal(1);

            teams.should.contain(item2);
          });
      });

      it("should get no teams when the 'edit' query string is present", {
        router
          .request
          .get("/teams?edit=true")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            auto teams = cast(Json[]) response.bodyJson["teams"];
            teams.length.should.equal(0);
          });
      });

      it("should get all teams when the 'edit' query string is present", {
        router
          .request
          .get("/teams?edit=true&all=true")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            item1["canEdit"] = true;
            item2["canEdit"] = true;
            item3["canEdit"] = true;
            item4["canEdit"] = true;

            auto teams = cast(Json[]) response.bodyJson["teams"];
            teams.length.should.equal(6);

            teams.should.contain(item1);
            teams.should.contain(item2);
            teams.should.contain(item3);
            teams.should.contain(item4);
          });
      });
    });

    describe("with an owner token", {
      it("should return all teams" , {
        router
          .request
          .get("/teams")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            item1["canEdit"] = true;
            item2["canEdit"] = true;
            item3["canEdit"] = false;

            auto teams = cast(Json[]) response.bodyJson["teams"];
            teams.length.should.equal(3);

            teams.should.contain(item1);
            teams.should.contain(item2);
            teams.should.contain(item3);
          });
      });

      it("should get all teams when the 'edit' query string is present", {
        router
          .request
          .get("/teams?edit=true")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            item1["canEdit"] = true;
            item2["canEdit"] = true;

            auto teams = cast(Json[]) response.bodyJson["teams"];
            teams.length.should.equal(2);

            teams.should.contain(item1);
            teams.should.contain(item2);
          });
      });
    });

    describe("with a leader token", {
      it("should return all teams" , {
        router
          .request
          .get("/teams")
          .header("Authorization", "Bearer " ~ bearerLeaderToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            auto teams = cast(Json[]) response.bodyJson["teams"];
            teams.length.should.equal(3);

            teams.should.contain(item1);
            teams.should.contain(item2);
            teams.should.contain(item3);
          });
      });

      it("should get no teams when the 'edit' query string is present", {
        router
          .request
          .get("/teams?edit=true")
          .header("Authorization", "Bearer " ~ bearerLeaderToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            auto teams = cast(Json[]) response.bodyJson["teams"];

            teams.length.should.equal(0);
          });
      });
    });

    describe("with a guest token", {
      it("should return all teams" , {
        router
          .request
          .get("/teams")
          .header("Authorization", "Bearer " ~ bearerGuestToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            auto teams = cast(Json[]) response.bodyJson["teams"];
            teams.length.should.equal(3);

            teams[0].should.equal(item1);
            teams[1].should.equal(item2);
            teams[2].should.equal(item3);
          });
      });

      it("should get no teams when the 'edit' query string is present", {
        router
          .request
          .get("/teams?edit=true")
          .header("Authorization", "Bearer " ~ bearerGuestToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["teams"].length.should.equal(0);
          });
      });
    });

    describe("the allowCustomDataBindings query parameter", {
      beforeEach({
        item1["allowCustomDataBindings"] = true;
        item3["allowCustomDataBindings"] = true;

        crates.team.updateItem(item1);
        crates.team.updateItem(item3);
      });

      it("returns nothing when the user is not authenticated" , {
        router
          .request
          .get("/teams?allowCustomDataBindings=true")
          .expectStatusCode(200)
          .end((Response response) => () {
            auto teams = cast(Json[]) response.bodyJson["teams"];
            teams.length.should.equal(0);
          });
      });

      it("returns the team of the authenticated member when the team and query has allowCustomDataBindings = true" , {
        router
          .request
          .get("/teams?allowCustomDataBindings=true")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            auto teams = cast(Json[]) response.bodyJson["teams"];
            teams.length.should.equal(1);
          });
      });

      it("returns an empty list for an admin who has no team with custom bindings" , {
        router
          .request
          .get("/teams?allowCustomDataBindings=true")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            auto teams = cast(Json[]) response.bodyJson["teams"];
            teams.length.should.equal(0);
          });
      });
    });
  });
});
