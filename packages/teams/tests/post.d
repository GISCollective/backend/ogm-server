/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.teams.post;

import tests.fixtures;

import std.algorithm;
import std.array;
import vibe.data.bson;
import ogm.middleware.GlobalAccess;
import ogm.defaults.teams;
import ogm.teams.api;
import gis_collective.hmq.broadcast.memory;

alias suite = Spec!({
  URLRouter router;
  Json team;

  describe("POST", {
    beforeEach({
      GlobalAccessMiddleware.resetInstance;
      setupTestData();
      router = new URLRouter;
      crates.picture.addDefault("image/jpeg", "cover", "./deploy/cover.jpg");

      auto broadcast = new MemoryBroadcast;
      router.crateSetup.setupTeamApi(crates, broadcast);
      setupDefaultTeams(crates);

      team = `{"team": {
              "name": "new team",
              "about": "",
              "isPublic": true,
              "isPublisher": false,
              "members": [],
              "leaders": [],
              "owners": [],
              "pictures": [],
              "logo": "000000000000000000000002",
              "isDefault": false,
              "categories": [],
              "guests": []}}`.parseJsonString;
    });

    describe("when the access.allowManageWithoutTeams preference is false", {
      Token token;

      beforeEach({
        auto preference = Json.emptyObject;
        preference["name"] = "access.allowManageWithoutTeams";
        preference["value"] = "false";

        crates.preference.addItem(preference);

        UserModel user;
        user._id = ObjectId.fromString("6").toString;
        user.username = "test";
        user.email = "other@gmail.com";
        userCollection.createUser(user, "password");

        token = userCollection.createToken("other@gmail.com", Clock.currTime + 3600.seconds, ["doStuff"], "Bearer");
      });

      afterEach({
        GlobalAccessMiddleware.resetInstance;
      });

      it("does not allow creating maps", {
        auto initialSize = crates.map.get.size;

        router
          .request
          .post("/teams")
          .send(team)
          .header("Authorization", "Bearer " ~ token.name)
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal((`{"errors": [{
                "description": "You need to be part of a team to perform this request.",
                "status": 400,
                "title": "Validation error"
              }]}`).parseJsonString);
            crates.map.get.size.should.equal(initialSize);
          });
      });
    });

    describeCredentialsRule("create a new team", "yes", "teams", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      auto data = team.clone;
      data["team"]["owners"] = [ Json("admin@gmail.com") ];

      router
        .request
        .post("/teams")
        .send(team)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto result = team.clone;
          result["team"]["_id"] = "000000000000000000000007";
          result["team"]["logo"] = "000000000000000000000002";
          result["team"]["owners"] = [ userId[type] ].serializeToJson;
          result["team"]["canEdit"] = true;
          result["team"]["allowCustomDataBindings"] = false;
          result["team"]["allowReports"] = false;
          result["team"]["allowNewsletters"] = false;
          result["team"]["invitations"] = Json.emptyArray;
          result["team"]["allowEvents"] = false;
          result["team"]["subscription"] = `{
            "comment": "",
            "domains": [],
            "expire": "0001-01-01T00:00:00+00:00",
            "name": "",
            "details": "",
            "support": [],
            "invoices": [],
            "monthlySupportHours": 0
          }`.parseJsonString;
          result["team"]["socialMediaLinks"] = `{
            "facebook": "",
            "instagram": "",
            "linkedin": "",
            "tikTok": "",
            "whatsApp": "",
            "xTwitter": "",
            "youtube": ""
          }`.parseJsonString;
          result["team"]["contactEmail"] = ``;

          response.bodyJson.should.equal(result);
        });
    });

    describeCredentialsRule("create a new team", "-", "teams", "no rights", {
      router
          .request
          .post("/teams")
          .send(team)
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"error": "Authorization required"}`.parseJsonString);
          });
    });

    describe("with an invalid token", {
      it("should not be able to add a team", {
        router
          .request
          .post("/teams")
          .send(team)
          .header("Authorization", "Bearer random")
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal("{\"error\": \"Invalid token.\"}".parseJsonString);
          });
      });
    });

    describe("with an admin token", {
      it("should be able to add a team with allowCustomDataBindings = true", {
        auto data = team.clone;
        data["team"]["allowCustomDataBindings"] = true;

        router
          .request
          .post("/teams")
          .send(data)
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["team"]["allowCustomDataBindings"].should.equal(true);
          });
      });

      it("should be able to add a team with allowEvents = true", {
        auto data = team.clone;
        data["team"]["allowEvents"] = true;

        router
          .request
          .post("/teams")
          .send(data)
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["team"]["allowEvents"].should.equal(true);
          });
      });

      it("should be able to add a team with allowReports = true", {
        auto data = team.clone;
        data["team"]["allowReports"] = true;

        router
          .request
          .post("/teams")
          .send(data)
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["team"]["allowReports"].should.equal(true);
          });
      });

      it("should be able to add a team with a subscription", {
        auto data = team.clone;

        data["team"]["subscription"] = SubscriptionDetails("standard").serializeToJson;

        router
          .request
          .post("/teams")
          .send(data)
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["team"]["subscription"]["name"].should.equal("standard");
          });
      });
    });

    describe("with an owner token", {
      it("should be able to add a team without logo", {
        auto teamWithoutLogo = team.clone();
        teamWithoutLogo["team"].remove("logo");

        router
          .request
          .post("/teams")
          .send(teamWithoutLogo)
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            auto result = team.clone;
            result["team"]["_id"] = "000000000000000000000007";
            result["team"]["logo"] = "000000000000000000000001";
            result["team"]["owners"] = [ Json("000000000000000000000004") ];
            result["team"]["canEdit"] = true;
            result["team"]["allowCustomDataBindings"] = false;
            result["team"]["allowNewsletters"] = false;
            result["team"]["allowReports"] = false;
            result["team"]["invitations"] = Json.emptyArray;
            result["team"]["allowEvents"] = false;
            result["team"]["subscription"] = `{
              "comment": "",
              "domains": [],
              "expire": "0001-01-01T00:00:00+00:00",
              "name": "",
              "details": "",
              "support": [],
              "invoices": [],
              "monthlySupportHours": 0
            }`.parseJsonString;
            result["team"]["socialMediaLinks"] = `{
              "facebook": "",
              "instagram": "",
              "linkedin": "",
              "tikTok": "",
              "whatsApp": "",
              "xTwitter": "",
              "youtube": ""
            }`.parseJsonString;
            result["team"]["contactEmail"] = ``;

            response.bodyJson.should.equal(result);
          });
      });

      it("should add the owner once" , {
        auto data = team.clone;
        data["team"]["owners"] = [ Json("owner@gmail.com") ];

        router
          .request
          .post("/teams")
          .send(team)
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            auto result = team.clone;
            result["team"]["_id"] = "000000000000000000000007";
            result["team"]["logo"] = "000000000000000000000002";
            result["team"]["owners"] = [ Json("000000000000000000000004") ];
            result["team"]["canEdit"] = true;
            result["team"]["allowCustomDataBindings"] = false;
            result["team"]["allowNewsletters"] = false;
            result["team"]["allowEvents"] = false;
            result["team"]["invitations"] = Json.emptyArray;
            result["team"]["allowReports"] = false;
            result["team"]["subscription"] = `{
              "comment": "",
              "domains": [],
              "expire": "0001-01-01T00:00:00+00:00",
              "name": "",
              "details": "",
              "support": [],
              "invoices": [],
              "monthlySupportHours": 0
            }`.parseJsonString;
            result["team"]["socialMediaLinks"] = `{
              "facebook": "",
              "instagram": "",
              "linkedin": "",
              "tikTok": "",
              "whatsApp": "",
              "xTwitter": "",
              "youtube": ""
            }`.parseJsonString;
            result["team"]["contactEmail"] = ``;

            response.bodyJson.should.equal(result);
          });
      });

      it("should add owner if is missing" , {
        auto data = team.clone;
        data["team"]["owners"] = [ Json("admin@gmail.com") ];

        router
          .request
          .post("/teams")
          .send(data)
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            auto result = team.clone;
            result["team"]["_id"] = "000000000000000000000007";
            result["team"]["logo"] = "000000000000000000000002";
            result["team"]["owners"] = [ Json("000000000000000000000004"), Json("000000000000000000000005") ];
            result["team"]["canEdit"] = true;
            result["team"]["pictures"] = Json.emptyArray;
            result["team"]["allowCustomDataBindings"] = false;
            result["team"]["allowNewsletters"] = false;
            result["team"]["allowReports"] = false;
            result["team"]["invitations"] = Json.emptyArray;
            result["team"]["allowEvents"] = false;
            result["team"]["subscription"] = `{
              "comment": "",
              "domains": [],
              "expire": "0001-01-01T00:00:00+00:00",
              "name": "",
              "details": "",
              "support": [],
              "invoices": [],
              "monthlySupportHours": 0
            }`.parseJsonString;
            result["team"]["socialMediaLinks"] = `{
              "facebook": "",
              "instagram": "",
              "linkedin": "",
              "tikTok": "",
              "whatsApp": "",
              "xTwitter": "",
              "youtube": ""
            }`.parseJsonString;
            result["team"]["contactEmail"] = ``;

            response.bodyJson.should.equal(result);
          });
      });

      it("should not be able to add a team with allowCustomDataBindings = true", {
        auto data = team.clone;
        data["team"]["allowCustomDataBindings"] = true;

        router
          .request
          .post("/teams")
          .send(data)
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["team"]["allowCustomDataBindings"].should.equal(false);
          });
      });

      it("should not be able to add a team with allowNewsletters = true", {
        auto data = team.clone;
        data["team"]["allowNewsletters"] = true;

        router
          .request
          .post("/teams")
          .send(data)
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["team"]["allowNewsletters"].should.equal(false);
          });
      });

      it("should not be able to add a team with allowEvents = true", {
        auto data = team.clone;
        data["team"]["allowEvents"] = true;

        router
          .request
          .post("/teams")
          .send(data)
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["team"]["allowEvents"].should.equal(false);
          });
      });

      it("should not be able to add a team with allowReports = true", {
        auto data = team.clone;
        data["team"]["allowReports"] = true;

        router
          .request
          .post("/teams")
          .send(data)
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["team"]["allowReports"].should.equal(false);
          });
      });

      it("should not be able to add a team with a subscription", {
        auto data = team.clone;

        data["team"]["subscription"] = SubscriptionDetails("standard").serializeToJson;

        router
          .request
          .post("/teams")
          .send(data)
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["team"]["subscription"]["name"].should.equal("");
          });
      });

      it("should not be able to add a team with invitations", {
        auto data = team.clone;

        data["team"]["invitations"] = `[{
          "email": "a@b.c",
          "role": "owners"
        }]`.parseJsonString;

        router
          .request
          .post("/teams")
          .send(data)
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson["team"]["invitations"].length.should.equal(0);
          });
      });
    });
  });
});
