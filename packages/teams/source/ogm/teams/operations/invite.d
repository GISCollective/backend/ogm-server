module ogm.teams.operations.invite;

import std.exception;
import std.algorithm;
import std.array;
import std.string;
import crate.base;
import crate.error;
import crate.http.operations.base;
import gis_collective.hmq.broadcast.base;
import ogm.crates.all;
import ogm.http.request;
import vibe.data.json;
import vibe.http.router;

class TeamInviteOperation : CrateOperation!DefaultStorage {
  private {
    OgmCrates crates;
    IBroadcast broadcast;
  }

  ///
  this(OgmCrates crates, IBroadcast broadcast) {
    this.crates = crates;
    this.broadcast = broadcast;

    CrateRule rule;
    rule.request.path = "/teams/:id/invite";
    rule.request.method = HTTPMethod.POST;
    rule.response.statusCode = 201;

    super(crates.team, rule);
  }

  override void handle(DefaultStorage storage) {
    storage.item = this.prepareItemOperation!"getItem"(storage);

    if(storage.response.headerWritten) {
      return;
    }

    this.applyRule(storage);

    auto role = storage.request.json["role"].to!string;
    enforce!CrateValidationException(["owners", "leaders", "members", "guests"].canFind(role), "Invalid role. It must be one of 'owners', 'leaders', 'members', 'guests'.");

    auto allUserIds = (storage.item["owners"] ~ storage.item["leaders"] ~ storage.item["members"] ~ storage.item["guests"]).byValue
      .map!(a => a.to!string)
      .filter!(a => isObjectId(a))
      .map!(a => ObjectId(a))
      .array;

    auto allUserEmails = crates.user.get.where("_id").anyOf(allUserIds).and.exec.map!(a => a["email"].to!string.strip.toLower).array;

    auto request = RequestUserData(storage.request);
    auto session = request.session(crates);

    enforce!ForbiddenException(request.isAdmin || session.teams!"owners".canFind(storage.properties.itemId), "You can't invite people to this team.");

    if(storage.item["invitations"].type != Json.Type.array) {
      storage.item["invitations"] = Json.emptyArray;
    }

    foreach(Json email; storage.request.json["emailList"]) {
      if(allUserEmails.canFind(email) || email.type != Json.Type.string) {
        continue;
      }

      string strEmail = email.to!string.strip.toLower;
      auto message = Json.emptyObject;
      message["id"] = storage.properties.itemId ~ "." ~ strEmail;
      message["team"] = storage.properties.itemId;

      auto range = crates.user.get.where("email").equal(strEmail).and.exec;

      if(!range.empty) {
        storage.item[role] ~= range.front["_id"];

        message["user"] = range.front["_id"];
        message["role"] = role;
        broadcast.push("team.welcome", message);
        continue;
      }

      auto invitation = Json.emptyObject;
      invitation["email"] = strEmail;
      invitation["role"] = role;
      allUserEmails ~= strEmail;

      storage.item["invitations"] ~= invitation;

      message["invitation"] = invitation;
      broadcast.push("team.invitation", message);
    }

    crates.team.updateItem(storage.item);

    storage.response.statusCode = 201;
    storage.response.writeVoidBody;
  }
}