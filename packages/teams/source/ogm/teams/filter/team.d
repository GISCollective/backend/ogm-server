/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.teams.filter.team;

import crate.auth.usercollection;
import crate.base;
import crate.error;
import crate.http.request;
import crate.collection.memory;
import crate.lazydata.base;

import vibe.http.router;
import vibe.data.json;
import vibe.core.log;

import ogm.crates.all;
import ogm.http.request;
import ogm.teams.team;

import std.array;
import std.algorithm;
import std.range;
import std.exception;
import std.conv;
import std.string;
import gis_collective.hmq.log;
import crate.collection.EmptyQuery;

///
class TeamFilter {
  private {
    UserCrateCollection users;
    LazyField!string defaultCover;
    OgmCrates crates;
  }

  struct Parameters {
    @describe("If it's true, only the items that the user can edit will be returned.")
    @example("true", "Get only the items that the user can edit.")
    @example("false", "Get all the items that the user can view.")
    bool edit;

    @describe("If it's true, and the user is an admin, all items will be returned.")
    @example("true", "Get all the items.")
    @example("false", "Get only the items that belongs to the current user.")
    bool all;

    @describe("If it's an user id, only teams where the user is a member will be returned.")
    string user;

    @describe("If it's true, only teams that can create custom data bindings will be returned.")
    string allowCustomDataBindings;
  }

  ///
  this(UserCrateCollection users, OgmCrates crates) {
    this.users = users;
    this.crates = crates;

    this.defaultCover = LazyField!string({
      return crates.picture.get
        .where("name")
        .equal("default")
        .and.exec
        .front["_id"]
        .to!string;
    });
  }

  void addUserQuery(IQuery selector, Parameters parameters) {
    if(parameters.user == "") {
      return;
    }

    auto userId = ObjectId.fromString(parameters.user);
    auto userRange = crates.user.get.where("_id").equal(userId).and.exec;

    auto orSelector = selector.or
      .where("owners").arrayContains(parameters.user).or
      .where("leaders").arrayContains(parameters.user).or
      .where("members").arrayContains(parameters.user).or
      .where("guests").arrayContains(parameters.user).or;

    if(!userRange.empty) {
      string email = userRange.front["email"].to!string;

      orSelector
        .where("owners").arrayContains(email).or
        .where("leaders").arrayContains(email).or
        .where("members").arrayContains(email).or
        .where("guests").arrayContains(email);
    }
  }

  ///
  @get
  IQuery get(IQuery selector, Parameters parameters, HTTPServerRequest req) {
    auto request = RequestUserData(req);
    auto session = request.session(crates);

    if(request.isAdmin && (parameters.all || "id" in req.params)) {
      addUserQuery(selector, parameters);

      return selector;
    }

    if(parameters.allowCustomDataBindings == "true") {
      if(!request.isAuthenticated) {
        return EmptyQuery.instance;
      }

      selector.where("allowCustomDataBindings").equal(true).and
        .where("_id").containsAny(session.owner.teamsIds);

      return selector;
    }

    if(parameters.edit) {
      selector.where("_id").containsAny(session.owner.teamsIds);
      return selector;
    }

    addUserQuery(selector, parameters);

    selector.or
      .where("isPublic").equal(true)
      .or
      .where("_id").containsAny(session.all.teamsIds);

    return selector;
  }

  /// Middleware applied for create routes
  @create
  void create(HTTPServerRequest req) {
    scope request = RequestUserData(req);

    enforce!CrateValidationException("team" in req.json, "object type expected to be `team`");

    if("owners" !in req.json["team"]) {
      req.json["team"]["owners"] = [ Json(request.userEmail) ];
    } else if(!req.json["team"]["owners"][].map!(a => a.to!string).canFind(request.userEmail)) {
      req.json["team"]["owners"] ~= Json(request.userEmail);
    }

    if("logo" !in req.json["team"] || req.json["team"]["logo"].type == Json.Type.null_) {
      req.json["team"]["logo"] = defaultCover.compute;
    }

    validateTeam(req.json["team"]);
  }

  /// Middleware applied for patch route
  @patch
  void patch(HTTPServerRequest, HTTPServerResponse) {
    throw new CrateNotFoundException("Not implemented.");
  }

  /// Middleware applied for put route
  @replace
  void replace(HTTPServerRequest req) {
    auto request = RequestUserData(req);
    auto session = request.session(crates);

    if(request.isAdmin) {
      return;
    }

    enforce!CrateNotFoundException(session.owner.teams.canFind(request.itemId), "Team was not found");
    enforce!CrateValidationException("team" in req.json, "object type expected to be `team`");

    validateTeam(req.json["team"]);
  }

  ///
  @delete_
  void delete_(HTTPServerRequest req, HTTPServerResponse) {
    scope request = RequestUserData(req);
    scope session = request.session(crates);

    if(request.isAdmin) {
      return;
    }

    enforce!CrateNotFoundException(session.owner.teams.canFind(request.itemId), "Team was not found");
  }

  private Json[] updateList(ref Json list) {
    auto ids = list.byValue.filter!(a => isObjectId(a)).map!(a => ObjectId(a)).array;
    auto emails = list.byValue.filter!(a => !isObjectId(a)).map!(a => a.to!string).array;

    auto query = this.crates.user.get;

    auto or = query.or;
    or.where("_id").anyOf(ids).or.where("email").anyOf(emails);

    auto result = query.and.withProjection(["_id"]).exec.map!(a => a["_id"]).array;

    return result;
  }

  @mapper
  Json mapper(HTTPServerRequest req, const Json item) @trusted nothrow {
    Json result = item;

    try {
      scope request = RequestUserData(req);
      auto session = request.session(crates);

      if(session.owner.teams.length > 0) {
        result["canEdit"] = item["owners"][].map!(a => a.to!string).canFind!(a => a == request.userId || a == request.userEmail);
      }

      if(request.isAdmin) {
        result["canEdit"] = true;
      }

      result["owners"] = this.updateList(result["owners"]);
      result["leaders"] = this.updateList(result["leaders"]);
      result["members"] = this.updateList(result["members"]);
      result["guests"] = this.updateList(result["guests"]);

    } catch(Exception e) {}

    return result;
  }
}
