/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.teams.middleware.TeamFlagsMiddleware;

import ogm.crates.all;
import ogm.http.request;

import vibe.http.router;

import crate.base;

class TeamFlagsMiddleware {
  private {
    OgmCrates crates;
  }

  ///
  this(OgmCrates crates) {
    this.crates = crates;
  }

  ///
  @patch @replace
  void replace(HTTPServerRequest req) {
    auto request = RequestUserData(req);

    if(request.isAdmin) {
      return;
    }

    auto team = crates.team.getItem(request.itemId).and.exec.front;
    req.json["team"]["allowCustomDataBindings"] = team["allowCustomDataBindings"];
    req.json["team"]["allowNewsletters"] = team["allowNewsletters"];
    req.json["team"]["allowEvents"] = team["allowEvents"];
    req.json["team"]["allowReports"] = team["allowReports"];
  }

  ///
  @create
  void check(HTTPServerRequest req) {
    auto request = RequestUserData(req);

    if(!request.isAdmin) {
      req.json["team"]["allowCustomDataBindings"] = false;
      req.json["team"]["allowNewsletters"] = false;
      req.json["team"]["allowEvents"] = false;
      req.json["team"]["allowReports"] = false;
    }
  }
}
