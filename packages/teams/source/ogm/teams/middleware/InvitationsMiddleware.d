/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.teams.middleware.InvitationsMiddleware;

import ogm.crates.all;
import ogm.http.request;

import vibe.http.router;
import vibe.data.json;

import crate.base;
import std.algorithm;

class InvitationsMiddleware {
  private {
    OgmCrates crates;
  }

  ///
  this(OgmCrates crates) {
    this.crates = crates;
  }

  @mapper
  void hideFields(HTTPServerRequest req, ref Json record) {
    auto request = RequestUserData(req);

    if(request.isAdmin) {
      return;
    }

    auto session = request.session(crates);

    string teamId = record["_id"].to!string;

    if(session.owner.teams.canFind(teamId)) {
      return;
    }

    if(session.leader.teams.canFind(teamId)) {
      return;
    }

    record["invitations"] = Json.emptyArray();
  }

  ///
  @patch @replace
  void replace(HTTPServerRequest req) {
    auto request = RequestUserData(req);

    auto team = crates.team.getItem(request.itemId).and.exec.front;
    req.json["team"]["invitations"] = team["invitations"];
  }

  ///
  @create
  void check(HTTPServerRequest req) {
    req.json["team"]["invitations"] = Json.emptyArray;
  }
}
