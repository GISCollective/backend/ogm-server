/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.teams.middleware.SubscriptionMiddleware;

import ogm.crates.all;
import ogm.http.request;

import vibe.http.router;
import vibe.data.json;

import crate.base;
import std.algorithm;

class SubscriptionMiddleware {
  private {
    OgmCrates crates;
  }

  ///
  this(OgmCrates crates) {
    this.crates = crates;
  }

  @mapper
  void hideFields(HTTPServerRequest req, ref Json record) {
    auto request = RequestUserData(req);

    if(request.isAdmin) {
      return;
    }

    auto session = request.session(crates);

    string teamId = record["_id"].to!string;

    if(session.owner.teams.canFind(teamId)) {
      return;
    }

    if(session.leader.teams.canFind(teamId)) {
      return;
    }

    record["subscription"] = SubscriptionDetails().serializeToJson;
  }

  ///
  @patch @replace
  void replace(HTTPServerRequest req) {
    auto request = RequestUserData(req);

    if(request.isAdmin) {
      return;
    }

    auto team = crates.team.getItem(request.itemId).and.exec.front;
    req.json["team"]["subscription"] = team["subscription"];
  }

  ///
  @create
  void check(HTTPServerRequest req) {
    auto request = RequestUserData(req);

    if(!request.isAdmin) {
      req.json["team"]["subscription"] = SubscriptionDetails().serializeToJson;
    }
  }
}
