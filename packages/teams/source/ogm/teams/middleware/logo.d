/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.teams.middleware.logo;

import ogm.crates.all;
import ogm.http.request;

import vibe.http.router;

import crate.base;

class LogoMiddleware {
  private {
    OgmCrates crates;
  }

  ///
  this(OgmCrates crates) {
    this.crates = crates;
  }

  ///
  @patch @replace
  void replace(HTTPServerRequest req) {
    if("logo" !in req.json["team"]) {
      return;
    }

    string newLogo = req.json["team"]["logo"].to!string;

    auto team = crates.team.getItem(req.params["id"]).exec.front;
    string logoId = team["logo"].to!string;

    if(logoId != newLogo) {
      crates.picture.removePicture(logoId);
    }
  }

  ///
  @delete_
  void delete_(HTTPServerRequest req, HTTPServerResponse res) {
    auto team = crates.team.getItem(req.params["id"]).exec.front;
    auto pictureId = team["logo"].to!string;

    crates.picture.removePicture(pictureId);
  }
}
