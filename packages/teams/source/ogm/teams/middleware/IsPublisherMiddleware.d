/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.teams.middleware.IsPublisherMiddleware;

import ogm.crates.all;
import ogm.http.request;

import vibe.http.router;
import vibe.data.json;

import crate.base;

class IsPublisherMiddleware {
  private {
    OgmCrates crates;
  }

  ///
  this(OgmCrates crates) {
    this.crates = crates;
  }

  ///
  @patch @replace
  void replace(HTTPServerRequest req) {
    if("team" !in req.json) {
      return;
    }

    string newLogo = req.json["team"]["isPublisher"].to!string;

    auto team = crates.team.getItem(req.params["id"]).exec.front;

    if(req.json["team"]["isPublisher"].type != Json.Type.bool_) {
      req.json["team"]["isPublisher"] = team["isPublisher"];
    }

    if(RequestUserData(req).isAdmin) {
      return;
    }

    if(team["isPublisher"].type != Json.Type.bool_) {
      team["isPublisher"] = false;
    }

    req.json["team"]["isPublisher"] = team["isPublisher"];
  }
}
