/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.teams.team;

import std.algorithm;
import std.array;
import ogm.http.request;
import vibe.data.json;

import crate.error;

version(unittest) {
  import fluent.asserts;
}

void validateTeam(const Json team) {
  static foreach(fieldName; ["canEdit"]) {
    enforce!CrateValidationException(
      fieldName !in team,
      "The site can not contain the " ~ fieldName ~ " field.");
  }

  enforce!CrateValidationException("owners" in team, "You have to provide at least one owner.");
  enforce!CrateValidationException(team["owners"].length > 0, "You have to provide at least one owner.");
}

/// Should expect the maps field
unittest {
  validateTeam(Json.emptyObject)
    .should
    .throwException!CrateValidationException
    .withMessage("You have to provide at least one owner.");
}
