/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.teams.api;

import vibe.http.router;
import crate.http.router;
import crate.base;
import crate.http.router;

import ogm.auth;
import ogm.crates.all;

import ogm.middleware.adminrequest;
import ogm.middleware.userdata;

import ogm.filter.pagination;
import ogm.filter.searchterm;

import ogm.teams.filter.team;
import ogm.teams.middleware.logo;
import ogm.middleware.ResourceMiddleware;
import ogm.middleware.GlobalAccess;
import ogm.teams.middleware.IsPublisherMiddleware;
import ogm.teams.middleware.TeamFlagsMiddleware;
import ogm.teams.middleware.SubscriptionMiddleware;
import ogm.teams.middleware.InvitationsMiddleware;
import gis_collective.hmq.broadcast.base;
import ogm.teams.operations.invite;

import ogm.crates.defaults;

///
void setupTeamApi(T)(CrateRouter!T crateRouter, OgmCrates crates, IBroadcast broadcast) {
  PictureFileSettings.files = crates.pictureFiles;
  PictureFileSettings.chunks = crates.pictureChunks;

  auto globalAccess = GlobalAccessMiddleware.instance(crates);
  auto auth = Authentication.instance(crates.user);
  auto adminRequest = new AdminRequest(crates.user);
  auto userDataMiddleware = UserDataMiddleware.instance(crates);
  auto paginationFilter = new PaginationFilter;
  auto teamFilter = new TeamFilter(auth.getCollection, crates);
  auto logoMiddleware = new LogoMiddleware(crates);
  auto picturesMiddleware = new ResourceMiddleware!("team", "pictures")(crates.team, crates.picture);
  auto searchTermFilter = new SearchTermFilter;
  auto isPublisherMiddleware = new IsPublisherMiddleware(crates);
  auto teamFlagsMiddleware = new TeamFlagsMiddleware(crates);
  auto subscriptionMiddleware = new SubscriptionMiddleware(crates);
  auto teamInvite = new TeamInviteOperation(crates, broadcast);
  auto invitations = new InvitationsMiddleware(crates);

  crateGetters["Picture"] = &crates.picture.getItem;

  crateRouter
    .prepare(crates.team)
    .withCustomOperation(teamInvite)
    .and(auth.publicDataMiddleware)
    .and(adminRequest)
    .and(userDataMiddleware)
    .and(isPublisherMiddleware)
    .and(teamFlagsMiddleware)
    .and(subscriptionMiddleware)
    .and(globalAccess)
    .and(teamFilter)
    .and(logoMiddleware)
    .and(picturesMiddleware)
    .and(invitations)
    .and(searchTermFilter)
    .and(paginationFilter);
}
