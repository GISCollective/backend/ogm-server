#!/usr/bin/rdmd --shebang -version=test -O

module add_copyright_headers;

import std.stdio: writeln;
import std.file;
import std.string;

void main() {
  auto dFiles = dirEntries("","*.{d,di}",SpanMode.depth);

  auto header = readText("source-header.txt");
  string[] folders = ["source", "tests", "submodules/ogm-models/"];

  foreach (d; dFiles) {
    bool isValidPrefix;

    foreach (string prefix; folders) {
      isValidPrefix = isValidPrefix || d.name.indexOf(prefix) == 0;
    }

    if(!isValidPrefix) {
      writeln("SKIP: ", d.name);
      continue;
    }

    auto content = readText(d.name);
    if(content.indexOf("/*") == 0) {
      auto last = content.indexOf("*/");

      if(last == -1) {
        continue;
      }

      content = content[last+3 .. $];
    }


    auto newContent = header ~ "\n" ~ content;

    std.file.write(d.name, newContent);
  }
}