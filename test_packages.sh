#/bin/sh
set -o pipefail

dirs=(packages/*)
curdir=$PWD

echo $dirs

for d in "${dirs[@]}"; do
  if [ "$d" == "packages/tests" ]; then
    continue
  fi

  echo ""
  echo "Building $d..."

  cd $curdir/$d

  dub --verror test --compiler=$DC || exit 1
done
