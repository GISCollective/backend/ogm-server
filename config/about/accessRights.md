---
title: Access rights
layout: default
nav_order: 1
parent: Resources
---

# Access rights
{: .no_toc }

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>



## Article links

| action                                                       | no rights         | guest             | member            | leader            | owner             | administrator     |
 ------------------------------------------------------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- |
create an article link                                         | no                | no                | no                | no                | no                | no                | 
delete an article link                                         | no                | no                | no                | no                | no                | no                | 
get an article link                                            | yes               | yes               | yes               | yes               | yes               | yes               | 
get the article links list                                     | no                | no                | no                | no                | no                | no                | 
update an article link                                         | no                | no                | no                | no                | no                | no                | 



## Articles

| action                                                       | no rights         | guest             | member            | leader            | owner             | administrator     |
 ------------------------------------------------------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- |
bulk publish articles from other teams                         | no                | no                | no                | no                | no                | yes               | 
bulk publish articles from own team                            | no                | no                | no                | no                | yes               | yes               | 
bulk publish own articles                                      | no                | no                | yes               | yes               | yes               | yes               | 
bulk unpublish articles from other teams                       | no                | no                | no                | no                | no                | yes               | 
bulk unpublish articles from own team                          | no                | no                | no                | no                | yes               | yes               | 
bulk unpublish own articles                                    | no                | no                | yes               | yes               | yes               | yes               | 
create articles for publisher teams                            | -                 | no                | no                | no                | yes               | yes               | 
delete any team articles                                       | -                 | no                | no                | no                | yes               | yes               | 
delete articles from own team                                  | no                | no                | no                | no                | yes               | yes               | 
delete own articles                                            | no                | no                | yes               | yes               | yes               | yes               | 
get private articles                                           | no                | yes               | yes               | yes               | yes               | yes               | 
get public article by id                                       | yes               | yes               | yes               | yes               | yes               | yes               | 
get public article by slug                                     | yes               | yes               | yes               | yes               | yes               | yes               | 
get the private article list                                   | no                | yes               | yes               | yes               | yes               | yes               | 
get the public article list                                    | yes               | yes               | yes               | yes               | yes               | yes               | 
publish newsletter article                                     | no                | no                | no                | no                | yes               | yes               | 
test newsletter article                                        | no                | no                | no                | no                | yes               | yes               | 
unpublish newsletter article                                   | no                | no                | no                | no                | yes               | yes               | 
update own articles                                            | -                 | yes               | yes               | yes               | yes               | yes               | 
update team articles                                           | -                 | no                | no                | no                | yes               | yes               | 



## Base maps

| action                                                       | no rights         | guest             | member            | leader            | owner             | administrator     |
 ------------------------------------------------------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- |
create base maps for other teams                               | -                 | no                | no                | no                | no                | yes               | 
create base maps for your teams                                | -                 | no                | no                | no                | yes               | yes               | 
create default base maps                                       | -                 | no                | no                | no                | no                | yes               | 
delete base maps owned by other teams                          | no                | no                | no                | no                | no                | yes               | 
delete base maps owned by your teams                           | -                 | no                | no                | no                | yes               | yes               | 
get the list of default base maps                              | yes               | yes               | yes               | yes               | yes               | yes               | 
get the list of default private base maps owned by other teams | -                 | no                | no                | no                | no                | yes               | 
get the list of default private base maps owned by your teams  | -                 | yes               | yes               | yes               | yes               | yes               | 
get the list of editable base maps owned by other teams        | -                 | no                | no                | no                | no                | yes               | 
get the list of editable base maps owned by your teams         | -                 | no                | no                | no                | yes               | yes               | 
search base maps by name                                       | yes               | yes               | yes               | yes               | yes               | yes               | 
set base maps as default                                       | no                | no                | no                | no                | no                | yes               | 
update base maps for other teams                               | no                | no                | no                | no                | no                | yes               | 
update base maps for your teams                                | -                 | no                | no                | no                | yes               | yes               | 



## Calendars

| action                                                       | no rights         | guest             | member            | leader            | owner             | administrator     |
 ------------------------------------------------------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- |
create a calendar when the team has allowEvents=false          | no                | no                | no                | no                | no                | yes               | 
create a calendar when the team has allowEvents=true           | no                | no                | no                | no                | yes               | yes               | 
delete from other teams                                        | no                | no                | no                | no                | no                | yes               | 
delete from own team                                           | no                | yes               | yes               | yes               | yes               | yes               | 
filter calendars by team                                       | no                | yes               | yes               | yes               | yes               | yes               | 
get a private calendar from other teams                        | no                | no                | no                | no                | no                | yes               | 
get a private calendar from own team                           | no                | yes               | yes               | yes               | yes               | yes               | 
get a public calendar                                          | yes               | yes               | yes               | yes               | yes               | yes               | 
update other team calendar                                     | no                | no                | no                | no                | no                | yes               | 
update team calendar                                           | no                | no                | no                | no                | yes               | yes               | 



## Campaigns

| action                                                       | no rights         | guest             | member            | leader            | owner             | administrator     |
 ------------------------------------------------------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- |
create campaign notification for other teams                   | no                | no                | no                | no                | no                | yes               | 
create campaign notification for your teams                    | -                 | no                | no                | no                | yes               | yes               | 
create campaigns for other teams                               | no                | no                | no                | no                | no                | yes               | 
create campaigns for your teams                                | -                 | no                | no                | no                | yes               | yes               | 
delete campaigns owned by other teams                          | no                | no                | no                | no                | no                | yes               | 
delete campaigns owned by your teams                           | -                 | no                | no                | no                | yes               | yes               | 
get a private campaign owned by other team                     | no                | no                | no                | no                | no                | yes               | 
get a private campaign owned by own team                       | -                 | yes               | yes               | yes               | yes               | yes               | 
get a public campaign                                          | yes               | yes               | yes               | yes               | yes               | yes               | 
get the list of private campaigns owned by other teams         | no                | no                | no                | no                | no                | yes               | 
get the list of public campaigns                               | yes               | yes               | yes               | yes               | yes               | yes               | 
get the list of the team campaigns                             | -                 | yes               | yes               | yes               | yes               | yes               | 
patch campaigns owned by other teams                           | no                | no                | no                | no                | no                | yes               | 
patch campaigns owned by your teams                            | -                 | no                | no                | no                | yes               | yes               | 
remove campaign notification for other teams                   | no                | no                | no                | no                | no                | yes               | 
remove campaign notification for your teams                    | -                 | no                | no                | no                | yes               | yes               | 
update campaigns owned by other teams                          | no                | no                | no                | no                | no                | yes               | 
update campaigns owned by your teams                           | -                 | no                | no                | no                | yes               | yes               | 



## Data bindings

| action                                                       | no rights         | guest             | member            | leader            | owner             | administrator     |
 ------------------------------------------------------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- |
create for own team                                            | -                 | no                | no                | no                | yes               | yes               | 
delete any team data binding                                   | -                 | no                | no                | no                | yes               | yes               | 
delete own data binding                                        | -                 | yes               | yes               | yes               | yes               | yes               | 
get data binding by id                                         | yes               | yes               | yes               | yes               | yes               | yes               | 
get the own data binding list                                  | -                 | yes               | yes               | yes               | yes               | yes               | 
get the private data binding list                              | no                | yes               | yes               | yes               | yes               | yes               | 
get the public data binding list                               | yes               | yes               | yes               | yes               | yes               | yes               | 
update own data bindings                                       | -                 | yes               | yes               | yes               | yes               | yes               | 
update team data bindings                                      | -                 | no                | no                | no                | yes               | yes               | 



## Events

| action                                                       | no rights         | guest             | member            | leader            | owner             | administrator     |
 ------------------------------------------------------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- |
bulk publish events from other teams                           | no                | no                | no                | no                | no                | yes               | 
bulk publish events from own team                              | no                | no                | no                | no                | yes               | yes               | 
bulk publish own events                                        | no                | no                | yes               | yes               | yes               | yes               | 
bulk unpublish events from other teams                         | no                | no                | no                | no                | no                | yes               | 
bulk unpublish events from own team                            | no                | no                | no                | no                | yes               | yes               | 
bulk unpublish own events                                      | no                | no                | yes               | yes               | yes               | yes               | 
create an event when the team has allowEvents=false            | no                | no                | no                | no                | no                | yes               | 
create an event when the team has allowEvents=true             | no                | no                | yes               | yes               | yes               | yes               | 
delete events from other team                                  | no                | no                | no                | no                | no                | yes               | 
delete events from own team                                    | no                | no                | no                | no                | yes               | yes               | 
delete from other teams                                        | -                 | no                | no                | no                | no                | yes               | 
delete from own team                                           | -                 | no                | no                | yes               | yes               | yes               | 
delete own events                                              | no                | no                | yes               | yes               | yes               | yes               | 
filter events by a team calendar                               | yes               | yes               | yes               | yes               | yes               | yes               | 
filter events by other team calendar                           | yes               | yes               | yes               | yes               | yes               | yes               | 
filter events by team                                          | no                | yes               | yes               | yes               | yes               | yes               | 
get a private event from other teams                           | no                | no                | no                | no                | no                | yes               | 
get a private event from own team                              | no                | yes               | yes               | yes               | yes               | yes               | 
get a public event                                             | yes               | yes               | yes               | yes               | yes               | yes               | 
update other team event                                        | -                 | no                | no                | no                | no                | yes               | 
update own team event                                          | -                 | yes               | yes               | yes               | yes               | yes               | 
update team event                                              | -                 | no                | no                | yes               | yes               | yes               | 



## Features

| action                                                       | no rights         | guest             | member            | leader            | owner             | administrator     |
 ------------------------------------------------------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- |
add new features on maps that the user does not have access    | no                | no                | no                | no                | no                | yes               | 
add new features on private maps                               | no                | yes               | yes               | yes               | yes               | yes               | 
add new features on public maps                                | yes               | yes               | yes               | yes               | yes               | yes               | 
add new features using the gpx route format                    | yes               | yes               | yes               | yes               | yes               | yes               | 
add new features using the gpx track format                    | yes               | yes               | yes               | yes               | yes               | yes               | 
bulk delete features from owned maps                           | -                 | no                | no                | yes               | yes               | yes               | 
bulk pending features from owned maps                          | -                 | no                | no                | yes               | yes               | yes               | 
bulk publish features from owned maps                          | -                 | no                | no                | yes               | yes               | yes               | 
bulk unpublish features from owned maps                        | -                 | no                | no                | yes               | yes               | yes               | 
delete features from other maps                                | -                 | no                | no                | no                | no                | yes               | 
delete features from owned maps                                | no                | no                | no                | yes               | yes               | yes               | 
download list as csv                                           | yes               | yes               | yes               | yes               | yes               | yes               | 
download list as geojson                                       | yes               | yes               | yes               | yes               | yes               | yes               | 
download masked geometries as csv                              | yes               | yes               | yes               | yes               | yes               | yes               | 
download masked geometries as gpx                              | yes               | yes               | yes               | yes               | yes               | yes               | 
download unmasked geometries as geojson                        | no                | yes               | yes               | yes               | yes               | yes               | 
edit features from other teams                                 | no                | no                | no                | no                | no                | yes               | 
edit features using the gpx format                             | no                | no                | yes               | yes               | yes               | yes               | 
edit other features from their own team                        | no                | no                | no                | yes               | yes               | yes               | 
edit own features from their own team                          | no                | yes               | yes               | yes               | yes               | yes               | 
filter by author                                               | yes               | yes               | yes               | yes               | yes               | yes               | 
filter by map                                                  | yes               | yes               | yes               | yes               | yes               | yes               | 
get features from other teams private maps                     | no                | no                | no                | no                | no                | yes               | 
get features from other teams public maps                      | yes               | yes               | yes               | yes               | yes               | yes               | 
get features from own private maps                             | no                | yes               | yes               | yes               | yes               | yes               | 
get features from own public maps                              | no                | yes               | yes               | yes               | yes               | yes               | 
get features from own teams                                    | no                | yes               | yes               | yes               | yes               | yes               | 
view unmasked geometry from other teams                        | no                | no                | no                | no                | no                | yes               | 
view unmasked geometry from own team                           | -                 | yes               | yes               | yes               | yes               | yes               | 



## Geocoding

| action                                                       | no rights         | guest             | member            | leader            | owner             | administrator     |
 ------------------------------------------------------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- |
create                                                         | no                | no                | no                | no                | no                | no                | 
delete                                                         | no                | no                | no                | no                | no                | no                | 
get item                                                       | no                | no                | no                | no                | no                | no                | 
query with a term                                              | yes               | yes               | yes               | yes               | yes               | yes               | 
query without a term                                           | no                | no                | no                | no                | no                | no                | 
update                                                         | no                | no                | no                | no                | no                | no                | 



## Icon sets

| action                                                       | no rights         | guest             | member            | leader            | owner             | administrator     |
 ------------------------------------------------------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- |
create for other team                                          | -                 | no                | no                | no                | no                | yes               | 
create for team                                                | -                 | no                | no                | no                | yes               | yes               | 
delete other team records                                      | -                 | no                | no                | no                | no                | yes               | 
delete team records                                            | -                 | no                | no                | no                | yes               | yes               | 
edit team other team records                                   | -                 | no                | no                | no                | no                | yes               | 
edit team records                                              | -                 | no                | no                | no                | yes               | yes               | 
view all                                                       | no                | no                | no                | no                | no                | yes               | 
view default records                                           | yes               | yes               | yes               | yes               | yes               | yes               | 
view editable team records                                     | no                | no                | no                | no                | yes               | yes               | 
view public records                                            | yes               | yes               | yes               | yes               | yes               | yes               | 
view team records                                              | -                 | yes               | yes               | yes               | yes               | yes               | 



## Icons

| action                                                       | no rights         | guest             | member            | leader            | owner             | administrator     |
 ------------------------------------------------------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- |
create for other team                                          | -                 | no                | no                | no                | no                | yes               | 
create for team                                                | -                 | no                | no                | no                | yes               | yes               | 
delete other team records                                      | -                 | no                | no                | no                | no                | yes               | 
delete team records                                            | -                 | no                | no                | no                | yes               | yes               | 
edit other team records                                        | -                 | no                | no                | no                | no                | yes               | 
edit team records                                              | -                 | no                | no                | no                | yes               | yes               | 
view editable team records                                     | -                 | no                | no                | no                | no                | yes               | 
view private other team records                                | -                 | no                | no                | no                | no                | yes               | 
view private own team records                                  | -                 | yes               | yes               | yes               | yes               | yes               | 
view public records                                            | yes               | yes               | yes               | yes               | yes               | yes               | 
view public records by id                                      | yes               | yes               | yes               | yes               | yes               | yes               | 



## Map files

| action                                                       | no rights         | guest             | member            | leader            | owner             | administrator     |
 ------------------------------------------------------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- |
add new files to joined maps                                   | -                 | no                | yes               | yes               | yes               | yes               | 
add new files to other maps                                    | no                | no                | no                | no                | no                | yes               | 
trigger cancel import tasks for joined maps                    | -                 | no                | yes               | yes               | yes               | yes               | 
trigger cancel import tasks for other maps                     | no                | no                | no                | no                | no                | yes               | 
trigger import tasks for joined maps                           | -                 | no                | yes               | yes               | yes               | yes               | 
trigger import tasks for other maps                            | no                | no                | no                | no                | no                | yes               | 
update options for files of joined maps                        | -                 | no                | yes               | yes               | yes               | yes               | 
update options for files of other maps                         | no                | no                | no                | no                | no                | yes               | 



## Map tiles

| action                                                       | no rights         | guest             | member            | leader            | owner             | administrator     |
 ------------------------------------------------------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- |
filter public map tiles by icons                               | yes               | yes               | yes               | yes               | yes               | yes               | 
get map tiles for a private map owned by other team            | -                 | no                | no                | no                | no                | yes               | 
get map tiles for a private map owned by the team              | -                 | yes               | yes               | yes               | yes               | yes               | 
get map tiles with masked data without a map selector          | yes               | yes               | yes               | yes               | yes               | yes               | 
get map tiles with pending data                                | yes               | yes               | yes               | yes               | yes               | yes               | 
get map tiles with private features owned by other teams       | -                 | no                | no                | no                | no                | yes               | 
get map tiles with private features owned by the team          | -                 | yes               | yes               | yes               | yes               | yes               | 
get map tiles with public data                                 | yes               | yes               | yes               | yes               | yes               | yes               | 
get map tiles with unmasked data with a map selector for other teams | no                | no                | no                | no                | no                | yes               | 
get map tiles with unmasked data with a map selector for own team | -                 | yes               | yes               | yes               | yes               | yes               | 



## Maps

| action                                                       | no rights         | guest             | member            | leader            | owner             | administrator     |
 ------------------------------------------------------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- |
change the `created on` field                                  | -                 | no                | no                | no                | yes               | yes               | 
change the `hideOnMainMap` field                               | -                 | no                | no                | no                | no                | yes               | 
change the `isIndex` field                                     | -                 | no                | no                | no                | no                | yes               | 
change the `original author` field                             | -                 | no                | no                | no                | yes               | yes               | 
create indexable maps                                          | no                | no                | no                | no                | no                | yes               | 
create maps for other teams                                    | no                | no                | no                | no                | no                | yes               | 
create maps for your teams                                     | -                 | no                | no                | no                | yes               | yes               | 
create maps that are hidden on the main map                    | no                | no                | no                | no                | no                | yes               | 
delete maps owned by other teams                               | no                | no                | no                | no                | no                | yes               | 
delete maps owned by your teams                                | -                 | no                | no                | no                | yes               | yes               | 
download as geopackage for other maps                          | no                | no                | no                | no                | no                | yes               | 
download as geopackage for own maps                            | -                 | yes               | yes               | yes               | yes               | yes               | 
get the indexable maps using `isIndex` query                   | yes               | yes               | yes               | yes               | yes               | yes               | 
get the sprite when the map is public                          | no                | yes               | yes               | yes               | yes               | yes               | 
update maps for other teams                                    | no                | no                | no                | no                | no                | yes               | 
update maps for your teams                                     | -                 | no                | no                | no                | yes               | yes               | 
use iconsets from other teams                                  | -                 | no                | no                | no                | no                | no                | 



## Metrics

| action                                                       | no rights         | guest             | member            | leader            | owner             | administrator     |
 ------------------------------------------------------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- |
delete a metric                                                | no                | no                | no                | no                | no                | no                | 
getting a list of metrics                                      | no                | no                | no                | no                | no                | no                | 
getting a metric                                               | no                | no                | no                | no                | no                | no                | 
push metrics                                                   | yes               | yes               | yes               | yes               | yes               | yes               | 
updating a metric                                              | no                | no                | no                | no                | no                | no                | 



## Newsletters

| action                                                       | no rights         | guest             | member            | leader            | owner             | administrator     |
 ------------------------------------------------------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- |
can create a newsletter when the team has allowNewsletters=false | no                | no                | no                | no                | no                | yes               | 
can create a newsletter when the team has allowNewsletters=true | no                | no                | no                | no                | yes               | yes               | 
delete from other teams                                        | no                | no                | no                | no                | no                | yes               | 
delete from own team                                           | no                | yes               | yes               | yes               | yes               | yes               | 
export emails from other teams newsletter                      | no                | no                | no                | no                | no                | yes               | 
export emails from own newsletter                              | no                | no                | no                | yes               | yes               | yes               | 
filter newsletters by team                                     | no                | yes               | yes               | yes               | yes               | yes               | 
get a private newsletter from other teams                      | no                | no                | no                | no                | no                | yes               | 
get a private newsletter from own team                         | no                | yes               | yes               | yes               | yes               | yes               | 
get a public newsletter                                        | yes               | yes               | yes               | yes               | yes               | yes               | 
import emails to other teams newsletter                        | no                | no                | no                | no                | no                | yes               | 
import emails to own newsletter                                | no                | no                | no                | yes               | yes               | yes               | 
update other team newsletter                                   | no                | no                | no                | no                | no                | yes               | 
update team newsletter                                         | no                | no                | no                | no                | yes               | yes               | 



## Pages

| action                                                       | no rights         | guest             | member            | leader            | owner             | administrator     |
 ------------------------------------------------------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- |
bulk publish own pages                                         | no                | no                | yes               | yes               | yes               | yes               | 
bulk publish pages from other teams                            | no                | no                | no                | no                | no                | yes               | 
bulk publish pages from own team                               | no                | no                | no                | no                | yes               | yes               | 
bulk unpublish own pages                                       | no                | no                | yes               | yes               | yes               | yes               | 
bulk unpublish pages from other teams                          | no                | no                | no                | no                | no                | yes               | 
bulk unpublish pages from own team                             | no                | no                | no                | no                | yes               | yes               | 
create pages for non publisher teams                           | -                 | no                | no                | no                | no                | yes               | 
create pages for publisher teams                               | -                 | no                | no                | no                | yes               | yes               | 
delete any team pages                                          | -                 | no                | no                | no                | yes               | yes               | 
delete own pages                                               | no                | no                | yes               | yes               | yes               | yes               | 
delete pages from own team                                     | no                | no                | no                | no                | yes               | yes               | 
get private pages                                              | no                | yes               | yes               | yes               | yes               | yes               | 
get public page by id                                          | yes               | yes               | yes               | yes               | yes               | yes               | 
get public page by slug                                        | yes               | yes               | yes               | yes               | yes               | yes               | 
get the own page list                                          | -                 | yes               | yes               | yes               | yes               | yes               | 
get the private page list                                      | no                | yes               | yes               | yes               | yes               | yes               | 
get the public page list                                       | yes               | yes               | yes               | yes               | yes               | yes               | 
publish own pages                                              | no                | no                | yes               | yes               | yes               | yes               | 
publish pages from other teams                                 | no                | no                | no                | no                | no                | yes               | 
unpublish own pages                                            | no                | no                | yes               | yes               | yes               | yes               | 
unpublish pages from other teams                               | no                | no                | no                | no                | no                | yes               | 
update own pages                                               | -                 | yes               | yes               | yes               | yes               | yes               | 
update team pages                                              | -                 | no                | no                | no                | yes               | yes               | 



## Presentations

| action                                                       | no rights         | guest             | member            | leader            | owner             | administrator     |
 ------------------------------------------------------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- |
create presentations for non publisher teams                   | -                 | no                | no                | no                | no                | yes               | 
create presentations for publisher teams                       | -                 | no                | no                | no                | yes               | yes               | 
delete any team presentations                                  | -                 | no                | no                | no                | yes               | yes               | 
delete own presentations                                       | -                 | yes               | yes               | yes               | yes               | yes               | 
get private presentations                                      | no                | yes               | yes               | yes               | yes               | yes               | 
get public presentation by id                                  | yes               | yes               | yes               | yes               | yes               | yes               | 
get public presentation by slug                                | yes               | yes               | yes               | yes               | yes               | yes               | 
get the own presentation list                                  | -                 | yes               | yes               | yes               | yes               | yes               | 
get the private presentation list                              | no                | yes               | yes               | yes               | yes               | yes               | 
get the public presentation list                               | yes               | yes               | yes               | yes               | yes               | yes               | 
update own presentations                                       | -                 | yes               | yes               | yes               | yes               | yes               | 
update team presentations                                      | -                 | no                | no                | no                | yes               | yes               | 



## Search metadata

| action                                                       | no rights         | guest             | member            | leader            | owner             | administrator     |
 ------------------------------------------------------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- |
create search metadata                                         | no                | no                | no                | no                | no                | no                | 
delete search metadata                                         | no                | no                | no                | no                | no                | no                | 
searching for private records owned by other team              | -                 | no                | no                | no                | no                | yes               | 
searching for private records owned by own team                | -                 | yes               | yes               | yes               | yes               | yes               | 
searching for public records                                   | yes               | yes               | yes               | yes               | yes               | yes               | 
update search metadata                                         | no                | no                | no                | no                | no                | no                | 



## Sounds

| action                                                       | no rights         | guest             | member            | leader            | owner             | administrator     |
 ------------------------------------------------------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- |
add mp3 sounds to a public team                                | yes               | yes               | yes               | yes               | yes               | yes               | 
add mp3 sounds to other private teams                          | -                 | yes               | yes               | yes               | yes               | yes               | 
add mp3 sounds to owned private teams                          | -                 | yes               | yes               | yes               | yes               | yes               | 
delete team sounds                                             | -                 | no                | no                | no                | yes               | yes               | 
listen public sounds                                           | yes               | yes               | yes               | yes               | yes               | yes               | 
listen team sounds                                             | -                 | yes               | yes               | yes               | yes               | yes               | 
update team sounds                                             | -                 | no                | no                | no                | yes               | yes               | 



## Spaces

| action                                                       | no rights         | guest             | member            | leader            | owner             | administrator     |
 ------------------------------------------------------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- |
create for own team                                            | -                 | no                | no                | no                | yes               | yes               | 
delete any team spaces                                         | -                 | no                | no                | no                | yes               | yes               | 
delete own spaces                                              | -                 | yes               | yes               | yes               | yes               | yes               | 
get private spaces                                             | no                | yes               | yes               | yes               | yes               | yes               | 
get public space by id                                         | yes               | yes               | yes               | yes               | yes               | yes               | 
get the own space list                                         | -                 | yes               | yes               | yes               | yes               | yes               | 
get the private space list                                     | no                | yes               | yes               | yes               | yes               | yes               | 
get the public space list                                      | yes               | yes               | yes               | yes               | yes               | yes               | 
set a as template                                              | *unknown*         | no                | no                | no                | no                | yes               | 
update own spaces                                              | -                 | yes               | yes               | yes               | yes               | yes               | 
update team spaces                                             | -                 | no                | no                | no                | yes               | yes               | 



## Survey answers

| action                                                       | no rights         | guest             | member            | leader            | owner             | administrator     |
 ------------------------------------------------------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- |
add an answer to a private campaign for your teams             | -                 | yes               | yes               | yes               | yes               | yes               | 
add an answer to a public campaign                             | yes               | yes               | yes               | yes               | yes               | yes               | 
approve answers                                                | no                | no                | yes               | yes               | yes               | yes               | 
delete from other teams                                        | no                | no                | no                | no                | no                | yes               | 
delete from own team                                           | no                | no                | yes               | yes               | yes               | yes               | 
get any by id                                                  | no                | no                | no                | no                | no                | yes               | 
get by id when the team owns the campaign                      | no                | yes               | yes               | yes               | yes               | yes               | 
get by id when the user is the original author                 | no                | yes               | yes               | yes               | yes               | yes               | 
get campaign answers                                           | no                | no                | no                | no                | no                | no                | 
get other users campaign answers                               | -                 | no                | no                | no                | no                | yes               | 
get own campaign answers                                       | -                 | yes               | yes               | yes               | yes               | yes               | 
get own campaign answers grouped by campaign                   | -                 | yes               | yes               | yes               | yes               | yes               | 
manually trigger a mapSync when the team owns the campaign     | no                | no                | yes               | yes               | yes               | yes               | 
manually trigger a mapSync when the user is the original author | no                | no                | no                | no                | no                | yes               | 
patch campaign answers                                         | no                | no                | yes               | yes               | yes               | yes               | 
reject answers                                                 | no                | no                | yes               | yes               | yes               | yes               | 
restores answers                                               | no                | no                | yes               | yes               | yes               | yes               | 
update answers                                                 | no                | no                | yes               | yes               | yes               | yes               | 



## Teams

| action                                                       | no rights         | guest             | member            | leader            | owner             | administrator     |
 ------------------------------------------------------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- |
can invite a member                                            | *unknown*         | yes               | yes               | yes               | yes               | yes               | 
can see subscription details                                   | no                | no                | no                | yes               | yes               | yes               | 
create a new team                                              | -                 | yes               | yes               | yes               | yes               | yes               | 
query by members                                               | yes               | yes               | yes               | yes               | yes               | yes               | 
update other teams                                             | -                 | no                | no                | no                | no                | yes               | 
update own teams                                               | -                 | no                | no                | no                | yes               | yes               | 
update the invitations                                         | -                 | no                | no                | no                | yes               | yes               | 
update the isPublisher value                                   | -                 | no                | no                | no                | no                | yes               | 



## Tiles

| action                                                       | no rights         | guest             | member            | leader            | owner             | administrator     |
 ------------------------------------------------------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- |
get the style json of a private map                            | no                | yes               | yes               | yes               | yes               | yes               | 
get the style json of a public map                             | yes               | yes               | yes               | yes               | yes               | yes               | 
get the tile json of a private map                             | no                | yes               | yes               | yes               | yes               | yes               | 
get the tile json of a public map                              | yes               | yes               | yes               | yes               | yes               | yes               | 
