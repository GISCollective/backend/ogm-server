# GISCollective Rest Api Server

GISCollective is an open source platform for easy map-making.

This repository contains the code for the public Rest API. It is based on the [vibe.d](https://vibed.org/) web server and it uses generative programming technics from [crate](https://gitlab.com/szabobogdan3/crate) to generate a skeleton for the REST Api, which is customized using middlewares.

Each model has it's own dedicated folder under `src/ogm` where all the rules are defined. For example the `map` model has the `src/ogm/maps/api.d` file which configures the resources and middlewares used by it.

The `src/ogm/api/api.d` contains the `setupApi` function which calls all the setup functions for the exposed models.

The `src/ogm/api/service.d` is the main entry point of the server. It defines commands for the code generators, metric collection and web server initialization.

You can read more about the architecture at [https://guide.giscollective.com/en/develop/architecture/](https://guide.giscollective.com/en/develop/architecture/) or find more about our platform at
 [https://giscollective.com](https://giscollective.com/).

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](https://git-scm.com/)
* A [DLang](https://dlang.org/) compiler. We recommend using [DMD](https://dlang.org/download.html) for development and [LDC](https://github.com/ldc-developers/ldc#installation) for production releases.
* The `openssl`, `ImageMagick`, `libsqlite3`, `libevent` and `librsvg2-tools` libraries.

    For fedora/redhat:
    ```
      > dnf install -y openssl ImageMagick librsvg2-tools
    ```

    For ubuntu systems:
    ```
      > apt-get install -y git libssl-dev libevent-dev libsqlite3-dev
    ```
* The [Trial](http://trial.szabobogdan.com/) test runner
* A mongo db server running
* Optionally, you can install and start [HMQ](https://gitlab.com/GISCollective/backend/hmq), [Tasks](https://gitlab.com/GISCollective/backend/tasks) and the tasks from [Node](https://gitlab.com/GISCollective/backend/node/).

## Installation

* `git clone --recurse-submodules -j8 <repository-url>` this repository with its submodules
* `cd ogm-server`
* create an app `config/configuration.js` file based on `config/configuration.model.js`
* create a db `config/db` folder based on `config/db.model`
* make sure `mongo` db is running
* `dub`

## Running / Development

You can build and start the server by running `dub`.

### Code Generators

The server provides some code generators that allows using clients easier:

* `dub -- genKotlin` to generate the models for the Kotlin programming language
* `dub -- genSwift` to generate the models for the Swift programming language
* `dub -- genOpenApi` to generate an OpenApi json file with the api

### Writing Tests

We use [Trial](http://trial.szabobogdan.com/) for running tests and [FluentAsserts](http://fluentasserts.szabobogdan.com/) for writing assertions.

* `trial` to run all tests
* `trial -s "suite name"` to run just one suite
* `trial -t "test name"` to run tests that contain `test name` in the test name

### Building

We use [dub](https://dub.pm/commandline.html) for running and building this server:

* `dub` (development)
* `dub build --build release` (production)

For production environments we recommend building a docker container:

* `dub build --config="executable-ssl11" --build release`
* `cp gis-collective-api deploy/service/gis-collective-api-fedora-35`
* `cd deploy`
* `docker build --build-arg APP_NAME=gis-collective-api --no-cache=true -t $IMAGE_TAG .`

### Deploying

This app can be deployed using `*.rpm`, `*.deb` packages, by downloading the artifacts from this repo, or using a container from https://gitlab.com/GISCollective/backend/ogm-server/container_registry/584293

## Contributing

You can find our contribution guide at [https://guide.giscollective.com/en/develop/CONTRIBUTING/](https://guide.giscollective.com/en/develop/CONTRIBUTING/)

## Code of conduct

You can find our code of conduct at [https://guide.giscollective.com/en/develop/CODE_OF_CONDUCT/](https://guide.giscollective.com/en/develop/CODE_OF_CONDUCT/)
