#!/usr/bin/sh
# ./gis-collective-api gis-collective-api

openapi-generator generate -i api.yaml -g java \
  --skip-validate-spec \
  --additional-properties=library=retrofit2 \
  --additional-properties=useRxJava2=true \
  --additional-properties=parcelableModel=true \
  --additional-properties=serializableModel=true \
  --additional-properties=caseInsensitiveResponseHeaders=true  \
  --additional-properties=modelPackage=com.giscollective.api.models \
  --additional-properties=invokerPackage=com.giscollective.api \
  --additional-properties=apiPackage=com.giscollective.api.client \
  --additional-properties=java8=true \
  --additional-properties=dateLibrary=threetenbp \
  --additional-properties=hideGenerationTimestamp=true \
  --additional-properties=serializationLibrary=jackson \
  -o ./api/java