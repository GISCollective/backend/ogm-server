/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import std.stdio;

import ogm.api.service;
import ogm.crates.all;

import vibe.service.main;

int main(string[] args) {
  return mainService!(ApiService)(args);
}

