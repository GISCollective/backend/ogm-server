/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.translations.configuration;

import vibe.service.configuration.general;

struct TranslationsConfiguration {
  string location;
  string baseUrl;
}
