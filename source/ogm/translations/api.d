/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.translations.api;

import ogm.auth;

import crate.http.cors;
import crate.http.router;
import crate.auth.middleware;
import crate.resource.file;
import crate.resource.migration;

import vibe.http.router;
import vibe.core.log;
import vibe.core.file;
import vibe.data.json;
import vibe.stream.wrapper;

import std.conv;
import std.array;
import std.file;
import std.path;

import ogm.middleware.adminrequest;
import ogm.crates.all;
import ogm.crates.defaults;
import ogm.models.translation;
import ogm.middleware.userdata;
import ogm.translations.middleware;
import ogm.translations.configuration;

import ogm.auth;

///
void setupTranslationApi(T)(CrateRouter!T crateRouter, OgmCrates crates, TranslationsConfiguration configuration) {
  TranslationFileSettings.baseUrl = configuration.baseUrl;
  TranslationFileSettings.files = crates.pictureFiles;
  TranslationFileSettings.chunks = crates.pictureChunks;

  auto auth = Authentication.instance(crates.user);
  auto adminRequest = new AdminRequest(crates.user);
  auto userDataMiddleware = UserDataMiddleware.instance(crates);

  auto translationMiddleware = new TranslationMiddleware(auth.getCollection);

  crateRouter
    .add(crates.translation,
      auth.publicDataMiddleware,
      adminRequest,
      userDataMiddleware,
      translationMiddleware);
}
