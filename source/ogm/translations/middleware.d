/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.translations.middleware;

import ogm.http.request;
import ogm.models.translation;

import vibe.http.server;
import vibe.data.json;
import std.conv;
import std.path;
import std.string;
import std.exception;

import crate.error;
import crate.http.request;
import crate.base;
import crate.auth.usercollection;

class TranslationMiddleware {

  private {
    UserCrateCollection users;
  }

  this(UserCrateCollection users) {
    this.users = users;
  }

  /// Middleware applied for create routes
  @create
  void create(HTTPServerRequest req) {
    auto request = RequestUserData(req);
    immutable isAdminRequest = request.isAdmin;

    enforce!ForbiddenException(isAdminRequest, "You can't create translations.");
  }

  /// Middleware applied for replace routes
  @replace
  void replace(HTTPServerRequest req, HTTPServerResponse res) {
    auto request = RequestUserData(req);
    immutable isAdminRequest = request.isAdmin;

    enforce!ForbiddenException(isAdminRequest, "You can't update translations.");
  }

  alias patch = replace;

  @mapper
  Json mapper(HTTPServerRequest req, const Json item) @trusted {
    Json result = item;
    auto request = RequestUserData(req);

    result["file"] = buildPath(TranslationFileSettings.baseUrl, "translations", result["_id"].to!string, "file");

    if(request.isAdmin) {
      result["canEdit"] = true;
    }

    return result;
  }
}
