/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.websocket.socket;

import crate.auth.usercollection;
import crate.http.request;

import ogm.crates.all;
import ogm.middleware.userdata;
import ogm.middleware.adminrequest;
import ogm.features.middlewares.mapper;
import vibe.http.websockets;

///
class SiteSession {
  import std.algorithm;
  import std.array;
  import std.datetime;
  import vibe.data.json;
  import crate.http.request;
  import crate.error;
  import crate.base;

  string token;
  UserDataMiddleware middleware;
  SysTime lastUpdate;
  UserCrateCollection users;

  this(UserDataMiddleware middleware, UserCrateCollection users) {
    this.middleware = middleware;
    this.users = users;
  }

  void updateToken(string token, CrateHttpRequest request) {
    if(Clock.currTime - lastUpdate > 30.seconds) {
      this.token = "";
    }

    if(this.token == token || token == "") {
      return;
    }

    this.lastUpdate = Clock.currTime;
    this.token = token;

    auto user = users.byToken(token);
    request.context["email"] = user.email;
    request.context["user_id"] = user.id;
    request.context["isAdmin"] = user.can!"admin";

    middleware.setupUserData(request);
  }
}

///
class SiteSocket {
  import std.algorithm;
  import std.array;
  import vibe.data.json;
  import crate.error;
  import crate.base;

  OgmCrates crates;
  FeatureMapper featureMapper;
  UserDataMiddleware middleware;
  UserCrateCollection users;

  ///
  this(OgmCrates crates, UserCrateCollection users, UserDataMiddleware middleware, FeatureMapper featureMapper) {
    this.crates = crates;
    this.featureMapper = featureMapper;
    this.middleware = middleware;
    this.users = users;
  }

  ///
  void handler(scope WebSocket socket) {
    scope session = new SiteSession(middleware, users);
    scope request = socket.request.toCrateRequest;

    while (socket.connected) {
      auto msg = socket.receiveText();

      try {
        auto query = msg.parseJsonString(msg);
        auto result = crates.feature.getItem(query["id"].to!string).exec;
        enforce!CrateNotFoundException(!result.empty, "The site does not exist.");

        if("token" in query) {
          session.updateToken(query["token"].to!string, request);
        }

        auto item = result.front;

        //featureMapper.mapper(socket.request, item);

        socket.send(item.toString);
      } catch(Exception e) {
        socket.send(e.toJson.toString);
      }
    }
  }
}
