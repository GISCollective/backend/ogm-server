/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.websocket.api;

import vibe.http.router;
import crate.http.router;
import crate.base;
import ogm.features.middlewares.mapper;

import ogm.auth;
import ogm.crates.all;

import ogm.middleware.userdata;
import ogm.middleware.adminrequest;

import ogm.websocket.socket;
import ogm.middleware.userdata;
import vibe.http.websockets;

///
void setupWebSocketApi(T)(CrateRouter!T crateRouter, OgmCrates crates) {
  auto auth = Authentication.instance(crates.user);
  auto userDataMiddleware = UserDataMiddleware.instance(crates);

  auto featureMapper = new FeatureMapper(crates);
  auto siteSocket = new SiteSocket(crates, auth.getCollection,
    userDataMiddleware, featureMapper);

  crateRouter.router.get("/socket/site", handleWebSockets(&siteSocket.handler));
}
