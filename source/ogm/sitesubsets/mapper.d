/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.sitesubsets.mapper;

import crate.base;
import crate.http.request;
import crate.error;

import vibe.http.router;
import vibe.data.json;

///
class SiteSubsetMapper {
  @mapper
  Json mapper(CrateHttpRequest request, const Json item) @trusted nothrow {
    Json result;

    try {
      result = Json.emptyObject;
      result["_id"] = item["_id"];
      result["site"] = item["_id"];
    } catch(Exception) {}

    return result;
  }

  /// Disable model changes
  @update
  IQuery update(IQuery selector, HTTPServerRequest req) {
    throw new CrateValidationException("You can not update the model.");
  }
}
