/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.sitesubsets.api;

import vibe.http.router;
import crate.http.router;
import crate.base;

import ogm.auth;
import ogm.crates.all;

import ogm.filter.indirectvisibility;
import ogm.sitesubsets.mapper;
import ogm.middleware.userdata;
import ogm.filter.pagination;
import ogm.middleware.adminrequest;
import ogm.filter.searchterm;
import ogm.middleware.features.SiteSubsetFilter;
import ogm.middleware.features.GetFeaturesFilter;

import ogm.icons.TaxonomyTree;
import ogm.icons.IconCache;

///
void setupSiteSubsetApi(T)(CrateRouter!T crateRouter, OgmCrates crates) {
  auto cache = new IconCache(&crates.icon.get, &crates.iconSet.getItem);
  auto iconTree = new TaxonomyTree(&crates.icon.get, &crates.iconSet.getItem, cache);

  auto auth = Authentication.instance(crates.user);
  auto userDataMiddleware = UserDataMiddleware.instance(crates);
  auto adminRequest = new AdminRequest(crates.user);
  auto featuresFilter = new GetFeaturesFilter(auth.getCollection, crates, iconTree);
  auto siteSubsetFilter = new SiteSubsetFilter();
  auto searchTermFilter = new SearchTermFilter;
  auto mapper = new SiteSubsetMapper();

  auto indirectVisibility = new IndirectVisibility(crates, crates.feature);

  crateRouter
    .add(crates.siteSubset,
      auth.publicDataMiddleware,
      adminRequest,
      userDataMiddleware,
      indirectVisibility,
      featuresFilter,
      siteSubsetFilter,
      searchTermFilter,
      mapper);
}
