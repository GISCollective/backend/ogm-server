/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.userprofile.api;

import ogm.auth;
import ogm.crates.all;
import ogm.middleware.adminrequest;
import ogm.middleware.userdata;
import ogm.userprofile.middleware;
import ogm.userprofile.contributions;

import crate.http.router;
import vibe.http.server;

///
void setupUserProfileApi(T)(CrateRouter!T crateRouter, OgmCrates crates) {
  auto auth = Authentication.instance(crates.user);
  auto adminRequest = new AdminRequest(crates.user);
  auto userDataMiddleware = UserDataMiddleware.instance(crates);
  auto userProfileMiddleware = new UserProfileMiddleware(crates);
  auto userProfileContributions = new UserProfileContributions(crates);

  crateRouter.router.match(HTTPMethod.OPTIONS, "/userprofiles/:id/contributions", &userProfileContributions.options);
  crateRouter.router.match(HTTPMethod.GET, "/userprofiles/:id/contributions", &auth.oAuth2.permisiveAuth);
  crateRouter.router.match(HTTPMethod.GET, "/userprofiles/:id/contributions", &adminRequest.any);
  crateRouter.router.match(HTTPMethod.GET, "/userprofiles/:id/contributions", &userDataMiddleware.any);
  crateRouter.router.match(HTTPMethod.GET, "/userprofiles/:id/contributions", &userProfileContributions.get);

  crateRouter.prepare(crates.userProfile)
    .and(auth.publicDataMiddleware)
    .and(adminRequest)
    .and(userDataMiddleware)
    .and(userProfileMiddleware);
}
