/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.userprofile.contributions;

import ogm.http.request;
import ogm.crates.all;
import ogm.middleware.StaticDbContent;

import crate.base;
import vibe.http.server;
import vibe.data.json;
import std.exception;
import crate.error;

class UserProfileContributions {
  private {
    OgmCrates crates;
  }

  ///
  this(OgmCrates crates) {
    this.crates = crates;
  }

  ///
  void options(HTTPServerRequest req, HTTPServerResponse res) {
    res.headers["Access-Control-Allow-Methods"] = "POST, GET";
    res.headers["Access-Control-Allow-Origin"] = "*";
    res.headers["Access-Control-Allow-Headers"] = "Content-Type, Authorization";

    res.writeBody("", 204);
  }

  void get(HTTPServerRequest req, HTTPServerResponse res) {
    auto result = Json.emptyArray;
    scope request = RequestUserData(req);
    scope session = request.session(crates);

    Json profile = Json.emptyObject;
    auto profileRange = crates.userProfile.get.where("_id").equal(ObjectId.fromString(request.itemId)).and.exec;

    auto hasPrivateCalendar = profileRange.empty || "showCalendarContributions" !in profileRange.front || profileRange.front["showCalendarContributions"] == false;

    if(!request.isAdmin && hasPrivateCalendar) {
      res.writeJsonBody(result, 200);
      return;
    }


    auto range = crates.meta.get
      .where("type").equal("user.contribution").and
      .where("itemId").equal(request.itemId).and
      .exec;

    if(!range.empty) {
      string[] maps = request.session(crates).all.maps;

      if("public" in range.front["data"]) {
        foreach (contribution; range.front["data"]["public"]) {
          result ~= contribution;
        }
      }

      if("private" in range.front["data"]) {
        if(request.isAdmin) {
          maps = [];
          foreach(string map, list; range.front["data"]["private"]) {
            maps ~= map;
          }
        }

        foreach (map; maps) {
          if(map in range.front["data"]["private"]) {
            foreach (contribution; range.front["data"]["private"][map]) {
              result ~= contribution;
            }
          }
        }
      }
    }

    res.writeJsonBody(result, 200);
  }
}