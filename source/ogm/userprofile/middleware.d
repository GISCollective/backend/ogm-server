/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.userprofile.middleware;

import ogm.http.request;
import ogm.crates.all;
import ogm.middleware.StaticDbContent;

import crate.base;
import vibe.http.server;
import vibe.data.json;
import std.exception;
import crate.error;
import openapi.definitions;
import crate.ctfe;

class UserProfileMiddleware : StaticDbContent {
  private {
    OgmCrates crates;
  }

  ///
  this(OgmCrates crates) {
    this.crates = crates;
    super("user profile");
  }

  @rule
  void updateRule(ref CrateRule rule) @safe {
    if(rule.response.schema is null) {
      return;
    }

    rule.response.schema.properties["canEdit"] = new Schema();
    rule.response.schema.properties["canEdit"].readOnly = true;
    rule.response.schema.properties["canEdit"].nullable = true;
    rule.response.schema.properties["canEdit"].type = SchemaType.boolean;
    rule.modelDescription.fields.basic ~= FieldDescription("bool", "canEdit", "canEdit", [], false, true, true, false);

    rule.response.schema.properties["userName"] = new Schema();
    rule.response.schema.properties["userName"].readOnly = true;
    rule.response.schema.properties["userName"].type = SchemaType.string;
    rule.modelDescription.fields.basic ~= FieldDescription("string", "userName", "userName", [], false, true, true, false);

    rule.response.schema.properties["email"] = new Schema();
    rule.response.schema.properties["email"].readOnly = true;
    rule.response.schema.properties["email"].type = SchemaType.string;

    rule.modelDescription.fields.basic ~= FieldDescription("string", "email", "email", [], false, true, true, false);
  }

  /// Don't allow if you are not admin
  @patch @replace
  override void replace(HTTPServerRequest req, HTTPServerResponse res) {
    scope request = RequestUserData(req);

    if(!request.isAdmin && (!request.isAuthenticated || request.userId != request.itemId)) {
      super.replace(req, res);
      return;
    }

    checkDefaultUserProfile(request.itemId);
  }

  /// Don't allow if you are not admin
  @patch @replace
  void checkUserFieldUpdates(HTTPServerRequest req, HTTPServerResponse res) {
    scope request = RequestUserData(req);
    auto user = crates.user.getItem(request.itemId).exec.front;
    auto profile = req.json["userProfile"];

    if("userName" in profile) {
      enforce!ForbiddenException(request.isAdmin || user["username"] == profile["userName"], `You can't change the 'userName'.`);

      if(request.isAdmin) {
        user["username"] = profile["userName"];
        crates.user.updateItem(user);
      }
    }

    if("email" in profile) {
      enforce!ForbiddenException(request.isAdmin || user["email"] == profile["email"], `You can't change the 'email'.`);

      if(request.isAdmin) {
        user["email"] = profile["email"];
        crates.user.updateItem(user);
      }
    }
  }

  @getItem
  void getItem(HTTPServerRequest req, HTTPServerResponse res) {
    scope request = RequestUserData(req);

    checkDefaultUserProfile(request.itemId);
  }

  void checkDefaultUserProfile(string userId) {
    auto id = ObjectId.fromString(userId);

    auto userExists = crates.user.get.where("_id").equal(id).and.size > 0;
    auto profileExists = crates.userProfile.get.where("_id").equal(id).and.size > 0;

    if(userExists && !profileExists) {
      auto user = crates.user.get.where("_id").equal(id).and.exec.front;

      auto newProfile = UserProfile(id).serializeToJson;
      newProfile["title"] = user["title"];
      newProfile["salutation"] = user["salutation"];
      newProfile["firstName"] = user["firstName"];
      newProfile["lastName"] = user["lastName"];
      newProfile["joinedTime"] = user["createdAt"];
      newProfile["showWelcomePresentation"] = true;

      newProfile.remove("picture");

      newProfile = crates.userProfile.addItem(newProfile);
    }
  }

  @mapper
  override void mapper(HTTPServerRequest req, ref Json result) @trusted nothrow {
    try {
      auto userData = RequestUserData(req);
      auto user = crates.user.getItem(userData.itemId).exec.front;

      if(userData.isAdmin || userData.userId == userData.itemId) {
        result["canEdit"] = true;
      }

      if("picture" in result && result["picture"].type == Json.Type.null_) {
        result.remove("picture");
      }

      result["userName"] = user["username"];
      result["email"] = user["email"];
    } catch(Exception e) {}
  }
}