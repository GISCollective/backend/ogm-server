/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.api.api;

import ogm.articles.api;
import ogm.articleLinks.api;
import ogm.basemaps.api;
import ogm.campaignAnswers.api;
import ogm.campaigns.api;
import ogm.dataBindings.api;
import ogm.features.api;
import ogm.files.api;
import ogm.geocodings.api;
import ogm.icons.api;
import ogm.iconSets.api;
import ogm.issues.api;
import ogm.legend.api;
import ogm.maps.api;
import ogm.metrics.api;
import ogm.middleware.serviceaccess;
import ogm.models.api;
import ogm.pages.api;
import ogm.pictures.api;
import ogm.preferences.api;
import ogm.presentations.api;
import ogm.serviceFiles.api;
import ogm.sitesubsets.api;
import ogm.sounds.api;
import ogm.spaces.api;
import ogm.stats.api;
import ogm.teams.api;
import ogm.tiles.api;
import ogm.translations.api;
import ogm.userprofile.api;
import ogm.users.api;
import ogm.websocket.api;
import ogm.newsletters.api;
import ogm.searchMeta.api;
import ogm.calendars.api;
import ogm.events.api;

import ogm.api.configuration;
import ogm.crates.all;
import gis_collective.hmq.broadcast.base;

import vibe.http.router;
import vibe.core.log;
import vibeauth.collections.client;
import vibeauth.authenticators.OAuth2;

import crate.http.router;
import crate.auth.usercollection;

import ogm.defaults.teams;
import ogm.defaults.articles;
import ogm.defaults.baseMaps;
import ogm.defaults.spaces;
import ogm.defaults.preferences;
import ogm.defaults.users;

/// Create api with authentication for all the app crates
void setupApi(T)(CrateRouter!T crateRouter, OgmCrates crates, MainApiConfiguration config, IBroadcast broadcast, bool withDefaults = true) {
  auto clientCollection = new ClientCollection([]);
  auto serviceAccess = new ServiceAccess(crates.preference);

  auto collection = new UserCrateCollection([], crates.user);
  auto auth = new OAuth2(collection);

  crateRouter.router.crateSetup.enable(auth);
  crateRouter.router.any("*", &serviceAccess.checkAccess);

  logInfo("setup pictures api");
  setupPictureApi(crateRouter, crates, config.pictures);

  logInfo("setup sounds api");
  setupSoundApi(crateRouter, crates, config.sounds);

  logInfo("setup teams api");
  setupTeamApi(crateRouter, crates, broadcast);

  logInfo("setup articles api");
  setupArticleApi(crateRouter, crates, broadcast);

  logInfo("setup pages api");
  setupPageApi(crateRouter, crates, config.general, broadcast);

  logInfo("setup presentations api");
  setupPresentationApi(crateRouter, crates);

  logInfo("setup base maps api");
  setupBaseMapApi(crateRouter, crates);

  logInfo("setup icons api");
  setupIconApi(crateRouter, crates, config.icons);

  logInfo("setup icon sets api");
  setupIconSetApi(crateRouter, crates);

  logInfo("setup issues api");
  setupIssueApi(crateRouter, crates, config.issues);

  logInfo("setup legends api");
  setupLegendApi(crateRouter, crates);

  logInfo("setup maps api");
  setupMapApi(crateRouter, crates, broadcast);

  logInfo("setup campaigns api");
  setupCampaignApi(crateRouter, crates);

  logInfo("setup campaign answers api");
  setupCampaignAnswerApi(crateRouter, crates, broadcast);

  logInfo("setup models api");
  setupModelApi(crateRouter, crates);

  logInfo("setup preferences api");
  setupPreferenceApi(crateRouter, crates);

  logInfo("setup features api");
  setupFeaturesApi(crateRouter, crates);

  logInfo("setup tile api");
  setupTilesApi(crateRouter, crates, config.general.apiUrl, config.general.serviceUrl);

  logInfo("setup files api");
  setupFileApi(crateRouter, crates, config.files, broadcast);

  logInfo("setup users api");
  setupUserApi(crateRouter, crates, broadcast);

  logInfo("setup userProfiles api");
  setupUserProfileApi(crateRouter, crates);

  logInfo("setup metrics api");
  setupMetricApi(crateRouter, crates);

  logInfo("setup data bindings api");
  setupDataBindingApi(crateRouter, crates, broadcast);

  logInfo("setup translations api");
  setupTranslationApi(crateRouter, crates, config.translations);

  logInfo("setup geocoding api");
  setupGeocodingApi(crateRouter, crates, config.general.serviceUrl);

  logInfo("setup websocket api");
  setupWebSocketApi(crateRouter, crates);

  logInfo("setup service files api");
  setupServiceFiles(crateRouter, crates, config.files);

  logInfo("setup spaces api");
  setupSpaceApi(crateRouter, crates, config.general.apiUrl);

  logInfo("setup stats api");
  setupStatsApi(crateRouter, crates);

  logInfo("setup newsletter api");
  setupNewsletterApi(crateRouter, crates, broadcast);

  logInfo("setup article links api");
  setupArticleLinksApi(crateRouter, crates);

  logInfo("setup calendars api");
  setupCalendarApi(crateRouter, crates, broadcast);

  logInfo("setup events api");
  setupEventApi(crateRouter, crates, broadcast);

  logInfo("setup search meta api");
  setupSearchMetaApi(crateRouter, crates);

  if(withDefaults) {
    logInfo("setup default users");
    setupDefaultUsers(crates);

    logInfo("setup default pictures");
    setupDefaultPictures(crates);

    logInfo("setup default teams");
    setupDefaultTeams(crates);

    logInfo("setup default base maps");
    setupDefaultBaseMaps(crates);

    logInfo("setup default articles");
    setupDefaultArticles(crates);

    logInfo("setup default preferences");
    setupDefaultPreferences(crates, config.general);

    logInfo("setup default geocodings");
    setupDefaultGeocodings(crates);

    logInfo("setup default icons");
    setupDefaultIcons(crates);

    logInfo("setup default spaces");
    setupDefaultSpaces(crates, config.general);

    logInfo("setup default pages");
    setupDefaultPages(crates);

    logInfo("setup default presentations");
    setupDefaultPresentations(crates);
  }

  crateRouter.router.rebuild;
}
