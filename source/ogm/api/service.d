/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.api.service;

import std.file;
import std.path;
import std.exception;
import std.datetime;
import std.stdio;
import std.string;
import std.conv : to;

import ogm.api.api;
import ogm.api.configuration;

import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.broadcast.http;

import ogm.crates.all;
import vibe.service.base;
import vibe.service.webservice;
import vibe.service.configuration.http;
import vibe.service.configuration.db;
import vibe.service.stats;

import vibe.service.stats;
import crate.http.router;
import crate.json;
import crate.lifecycle;

import vibe.http.server;
import vibe.http.fileserver;
import vibe.http.router;
import vibe.db.mongo.mongo;
import vibe.core.core;
import vibe.core.log;

import crate.generators.openapi;
import crate.generators.swift;
import crate.generators.kotlin;
import crate.generators.typescript;

import eventcore.core;

import crate.collection.notifications;

import gis_collective.hmq.queue.mongo;
import gis_collective.hmq.queue.memory;
import gis_collective.hmq.queue.base;

///
class ApiService : WebService!MainApiConfiguration {
  private {
    MongoDatabase db;
    OgmCrates crates;

    long activeUsers;
    long minute;
    long hour;
    long day;
    long week;
    long month;
    long pictureLinkCount;
    long pictureUnlinkCount;
    long pictureNoSourceCount;
    long pictureNoLinkInSourceCount;
    long largePictureCount;
  }

  void shutdown() nothrow {
    import gis_collective.hmq.lifecycle;
    logInfo("Shutting down the LifeCycle...");
    LifeCycle.instance.shutdown;
  }

  ///
  void crateCounter() nothrow @trusted {
    try {
      static foreach(member; __traits(allMembers, OgmCrates)) {
        mixin(`Stats.instance.set("record_count", this.crates.` ~ member ~ `.get.size, ["collection": "` ~ member ~ `"]);`);
      }
    } catch(Exception e) {
      error(e);
    }
  }

  ///
  void userCounterCollect() nothrow @trusted {
    try {
      enum minuteSecs = 60;
      enum hourSecs = minuteSecs * 60;
      enum daySecs = hourSecs * 24;
      enum weekSecs = daySecs * 7;
      enum monthSecs = weekSecs * 4;

      auto now = Clock.currTime.toUnixTime;

      activeUsers = this.db["users"].countDocuments(["tokens":[ "$not": ["$size": 0] ]]);
      minute = this.db["users"].countDocuments(["lastActivity":[ "$gt": now - minuteSecs ]]);
      hour = this.db["users"].countDocuments(["lastActivity":[ "$gt": now - hourSecs ]]);
      day = this.db["users"].countDocuments(["lastActivity":[ "$gt": now - daySecs ]]);
      week = this.db["users"].countDocuments(["lastActivity":[ "$gt": now - weekSecs ]]);
      month = this.db["users"].countDocuments(["lastActivity":[ "$gt": now - monthSecs ]]);

      pictureLinkCount = this.db["pictures"].countDocuments(["meta.link.model":[ "$exists": true ]]);
      pictureUnlinkCount = this.db["pictures"].countDocuments(["meta.link.model":[ "$exists": false ]]);
      pictureNoSourceCount = this.db["pictures"].countDocuments(["meta.link.noSource":[ "$exists": true ]]);
      pictureNoLinkInSourceCount = this.db["pictures"].countDocuments(["meta.link.noLinkInSource":[ "$exists": true ]]);
      largePictureCount = this.db["pictures.files"].countDocuments(["length":[ "$gt": 5 * 1024 * 1024 ]]);
    } catch(Exception e) {
      error(e);
    }

    Stats.instance.set("users", activeUsers, ["interval": "all time"]);
    Stats.instance.set("users", minute, ["interval": "minute"]);
    Stats.instance.set("users", hour, ["interval": "hour"]);
    Stats.instance.set("users", day, ["interval": "day"]);
    Stats.instance.set("users", week, ["interval": "week"]);
    Stats.instance.set("users", month, ["interval": "month"]);

    Stats.instance.set("pictures", pictureLinkCount, ["links": "yes"]);
    Stats.instance.set("pictures", pictureUnlinkCount, ["links": "false"]);
    Stats.instance.set("pictures", pictureNoSourceCount, ["links": "noSource"]);
    Stats.instance.set("pictures", pictureNoLinkInSourceCount, ["links": "noLinkInSource"]);
    Stats.instance.set("pictures", largePictureCount, ["large": "yes"]);
  }

  ///
  void storageStatsCollect() nothrow @trusted {
    try {
      auto range = this.crates.meta.get.where("type").equal("stats.size").and.sort("itemId", -1).limit(1).exec;

      if(range.empty) {
        return;
      }

      foreach(string collection, stats; range.front["data"]) {
        foreach(string key, value; stats) {
          Stats.instance.set("stats.size", value.to!ulong, ["type": key, "collection": collection]);
        }
      }
    } catch(Exception e) {
      error(e);
    }
  }

  @("action")
  static int genOpenApi(string[]) {
    import ogm.test.fixtures : createCrates;
    import openapi.definitions;

    auto router = new URLRouter;
    auto crateRouter = router.crateSetup;

    setupApi(crateRouter, createCrates, MainApiConfiguration(), null, false);

    OpenApi api;

    foreach(rule; crateRouter.rules) {
      api.add(rule);
    }

    foreach(key, val; getErrorListSchemas()) {
      api.components.schemas[key] = val;
    }

    api.info.title = "GISCollective";
    api.info.version_ = import("version.txt");

    api.components.schemas["MapItem"] = api.components.schemas["Map"];
    api.components.schemas["manyRestMaps"].properties["maps"].items._ref = "#/components/schemas/MapItem";
    api.components.schemas["oneRestMap"].properties["map"]._ref = "#/components/schemas/MapItem";
    api.components.schemas.remove("map");

    std.file.write("api.json", api.serializeToJson.serializeToSortedJsonString);
    return 0;
  }

  @("action")
  static int genSwift(string[]) {
    import ogm.test.fixtures : createCrates;

    auto router = new URLRouter;
    auto crateRouter = router.crateSetup;

    setupApi(crateRouter, createCrates, MainApiConfiguration(), null, false);

    if("swift".exists) {
      "swift".rmdirRecurse;
    }

    "swift".mkdir;

    foreach(key, value; crateRouter.toSwiftModels) {
      std.file.write(buildPath("swift", key ~ ".swift"), value);
    }

    return 0;
  }

  @("action")
  static int genKotlin(string[]) {
    import ogm.test.fixtures : createCrates;

    auto router = new URLRouter;
    auto crateRouter = router.crateSetup;

    setupApi(crateRouter, createCrates, MainApiConfiguration(), null, false);

    if("kotlin".exists) {
      "kotlin".rmdirRecurse;
    }

    "kotlin".mkdir;

    foreach(key, value; crateRouter.toKotlinModels) {
      std.file.write(buildPath("kotlin", key ~ ".kt"), value);
    }

    return 0;
  }

  @("action")
  static int genTypeScript(string[]) {
    import ogm.test.fixtures : createCrates;

    auto router = new URLRouter;
    auto crateRouter = router.crateSetup;

    setupApi(crateRouter, createCrates, MainApiConfiguration(), null, false);

    if("typescript".exists) {
      "typescript".rmdirRecurse;
    }

    "typescript".mkdir;

    foreach(key, value; crateRouter.toTSModels) {
      std.file.write(buildPath("typescript", key ~ ".ts"), value);
    }

    return 0;
  }

  IQueue!Json queueFactory(string name) nothrow @trusted {
    try {
      auto configurations = this.configuration.general.dbConfiguration;
      auto settings = configurations[0].configuration.deserializeJson!MongoClientSettings;
      auto client = configurations.getMongoClient;

      name = name.replace(".", "_");
      auto collection = client.getCollection(settings.database ~ "._hmq_api_" ~ name);

      return new MongoQueue!Json(collection);
    } catch(Exception e) {
      return new MemoryQueue!Json();
    }
  }

  void onMiddlewareBenchmark(const string route, const string middelewareName, const Duration duration) nothrow @safe {
    logInfo("route middleware: %s %s took %s", route, middelewareName, duration);

    Stats.instance.avg("middleware", duration.total!"hnsecs", ["route": route, "middleware": middelewareName]);
  }

  void notFound(HTTPServerRequest request, HTTPServerResponse response) {
    if(response.headerWritten) {
      return;
    }

    import std.stdio;
    writeln("NOT FOUND! ", request.requestPath);
    response.writeBody(`{
      "errors": [{
        "description": "The requested page was not found: ` ~ request.requestPath.to!string ~ `",
        "status": 404,
        "title": "Page Not Found"
      }]
    }`, 404, "application/json");
  }

  /// The main service logic
  int main() {
    CrateLifecycle.instance.onMiddlewareBenchmark(&this.onMiddlewareBenchmark);
    scope(exit) this.statsTimer();
    auto router = new URLRouter;
    router.setupStats();
    log("Added stats routes.");

    BroadcastConfig broadcastConfig;
    broadcastConfig.hmqUrl = this.configuration.httpMq.url;
    broadcastConfig.localUrl = this.configuration.general.apiUrl;
    broadcastConfig.router = router;
    broadcastConfig.queueFactory = &queueFactory;

    auto broadcast = new HttpBroadcast(broadcastConfig);

    void push(CrateChange change) @safe {
      scope channel = change.modelName ~ ".change";

      broadcast.push(channel, change.serializeToJson);
    }

    this.crates = this.configuration.general.dbConfiguration.setupOgmCrates(&push);

    this.db = this.configuration.general.dbConfiguration.getMongoDb;
    log("Db connection ready.");

    router.crateSetup.setupApi(crates, this.configuration, broadcast);

    router.any("*", &this.notFound);

    log("Api setup ready.");

    auto crateTimer = createTimer(&this.crateCounter);
    crateTimer.rearm(15.seconds, true);
    runTask(&this.crateCounter);

    createTimer(&this.userCounterCollect).rearm(15.seconds, true);
    this.userCounterCollect();

    createTimer(&this.storageStatsCollect).rearm(15.seconds, true);
    this.storageStatsCollect();

    auto config = this.configuration.http.toVibeConfig;
    config.serverString = this.info.name;

    logInfo("The maximum accepted request size is %s kb", config.maxRequestSize / 1024);
    logInfo("The server string is `%s`", config.serverString);

    auto l = listenHTTP(config, router);
    scope(exit) l.stopListening;

    return runEventLoop();
  }
}
