/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogm.api.configuration;

import std.file;
import std.path;
import std.exception;
import std.conv;
import std.datetime;

import vibeauth.mail.base;
import vibeauth.data.client;

import vibe.http.server;
import vibe.http.fileserver;
import vibe.data.json;

import vibe.service.configuration.general;
import vibe.service.configuration.http;

import ogm.files.configuration;
import ogm.icons.configuration;
import ogm.issues.configuration;
import ogm.pictures.configuration;
import ogm.sounds.configuration;
import ogm.translations.configuration;

/// The service configuration
struct MainApiConfiguration {
  /// Configurations for all services
  GeneralConfig general;

  /// The http configuration
  HTTPConfig http;

  HTTPMQConfig httpMq;

  ///
  FilesConfiguration files;

  ///
  IconsConfiguration icons;

  ///
  IssuesConfiguration issues;

  ///
  PicturesConfiguration pictures;

  ///
  SoundsConfiguration sounds;

  ///
  TranslationsConfiguration translations;
}

///
struct HTTPMQConfig {
  ///
  string url;
}

/// The Api service configuration
struct ApiConfig {
  /// The base service url
  string baseUrl;
}

/// The Api service configuration
struct ApiFilesConfig {
  /// Source for the public files
  @name("public") string public_;

  /// Destination path for the uploaded files
  string upload;
}
