/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.serviceaccess.get;

import tests.fixtures;

import std.algorithm;
import std.array;
import ogm.pictures.configuration;
import ogm.middleware.serviceaccess;
import ogm.crates.defaults;
import ogm.defaults.preferences;

alias suite = Spec!({
  URLRouter router;

  describe("GET", {
    beforeEach({
      setupTestData();

      PictureFileSettings.files = new MockGridFsFiles;
      PictureFileSettings.chunks = new MockGridFsChunks;
      crates.picture.addDefault("image/svg+xml", "logo", "deploy/default.png");
      crates.picture.addDefault("image/jpeg", "cover", "deploy/default.png");
      crates.picture.addDefault("image/jpeg", "default", "deploy/default.png");

      setupDefaultPreferences(crates, GeneralConfig());

      router = new URLRouter;
      auto serviceAccess = new ServiceAccess(crates.preference);
      router.any("*", &serviceAccess.checkAccess);

      router.get("*", (HTTPServerRequest req, HTTPServerResponse res) {
        if("Authorization" in req.headers) {
          res.writeBody(req.headers["Authorization"], 200);
          return;
        }
        res.writeBody("", 204);
      });
    });

    describe("with a private service", {
      beforeEach({
        auto pref = crates.preference.get.where("name").equal("register.mandatory").and.exec.front;
        pref["value"] = "true";

        crates.preference.updateItem(pref);
      });

      it("should not be accessible if no auth is provided", {
        request(router)
          .get("/")
              .expectStatusCode(404)
              .end;
      });

      it("should be accessible if an Authorization header is provided", {
        request(router)
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .get("/")
              .expectStatusCode(200)
              .end((Response response) => () {
                response.bodyString.should.equal("Bearer " ~ administratorToken.name);
              });
      });

      it("should ignore the cookie auth when the Authorization header is provided", {
        request(router)
          .header("Authorization", "Bearer " ~ administratorToken.name).header("User-Agent", "something")
          .header("Cookie", "ember_simple_auth-session=%7B%22authenticated%22%3A%7B%22access_token%22%3A%22fake%22%7D%7D")
          .get("/")
              .expectStatusCode(200)
              .end((Response response) => () {
                response.bodyString.should.equal("Bearer " ~ administratorToken.name);
              });
      });

      it("should ignore the authorization query parameter when the Authorization header is provided", {
        request(router)
          .header("Authorization", "Bearer " ~ administratorToken.name).header("User-Agent", "something")
          .get("/?authorization=test")
              .expectStatusCode(200)
              .end((Response response) => () {
                response.bodyString.should.equal("Bearer " ~ administratorToken.name);
              });
      });
    });

    describe("with a public service", {
      beforeEach({
        auto pref = crates.preference.get.where("name").equal("register.mandatory").and.exec.front;
        pref["value"] = "false";

        crates.preference.updateItem(pref);
      });

      it("should be accessible if no auth is provided", {
        request(router)
          .get("/")
              .expectStatusCode(204)
              .end((Response response) => () {
                response.bodyString.should.equal("");
              });
      });
    });
  });
});
