/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.sitesubset;

import tests.fixtures;

alias suite = Spec!({
  describe("GET", {
    URLRouter router;

    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupSiteSubsetApi(crates);
    });

    describe("The site subsets", {
      it("should return an error if no query is provided", {
        router
          .request
          .get("/sitesubsets")
          .expectStatusCode(400)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "errors": [{
                  "description": "You must provide at least one query selector.",
                  "title": "Validation error",
                  "status": 400
                }]
              }`.parseJsonString);
          });
      });

      it("should select public items from a viewbox", {
        router
          .request
          .get("/sitesubsets?viewbox=1%2C1%2C2%2C2")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"sitesubsets": [
            { "_id": "000000000000000000000001", "site": "000000000000000000000001" },
            { "_id": "000000000000000000000003", "site": "000000000000000000000003" }]}`
              .parseJsonString);
          });
      });


      it("should select items from a viewbox", {
        router
          .request
          .get("/sitesubsets?viewbox=1%2C1%2C2%2C2")
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"sitesubsets": [
            { "_id": "000000000000000000000001", "site": "000000000000000000000001" },
            { "_id": "000000000000000000000002", "site": "000000000000000000000002" },
            { "_id": "000000000000000000000003", "site": "000000000000000000000003" }]}`
              .parseJsonString);
          });
      });
    });
  });
});
