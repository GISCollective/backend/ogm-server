/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.fixtures;

public import fluent.asserts;
import fluentasserts.vibe.json;
public import trial.discovery.spec;

public import crate.auth.usercollection;

import std.traits;

public import vibe.service.configuration.general;
public import ogm.auth;
public import ogm.users.api;
public import ogm.crates.all;
public import ogm.test.fixtures;
public import ogm.crates.defaults;

public import crate.http.router;
public import ogm.articles.api;
public import ogm.articleLinks.api;
public import ogm.basemaps.api;
public import ogm.campaignAnswers.api;
public import ogm.campaigns.api;
public import ogm.dataBindings.api;
public import ogm.features.api;
public import ogm.files.api;
public import ogm.geocodings.api;
public import ogm.icons.api;
public import ogm.iconSets.api;
public import ogm.issues.api;
public import ogm.legend.api;
public import ogm.maps.api;
public import ogm.metrics.api;
public import ogm.models.api;
public import ogm.pages.api;
public import ogm.pictures.api;
public import ogm.preferences.api;
public import ogm.presentations.api;
public import ogm.sitesubsets.api;
public import ogm.sounds.api;
public import ogm.spaces.api;
public import ogm.stats.api;
public import ogm.teams.api;
public import ogm.tiles.api;
public import ogm.translations.api;
public import ogm.users.api;
public import ogm.newsletters.api;
public import ogm.websocket.api;
public import gis_collective.hmq.broadcast.memory;

import trial.interfaces;

import std.file;
import std.string;

import vibe.core.log;

shared static this() {
  foreach(logger; getLoggers()) {
    deregisterLogger(logger);
  }

  setLogLevel(LogLevel.trace);
}

static this() {
  LifeCycleListeners.instance.add(new CredentialsRuleWriter);
}

private static Json credentialsRule;

class CredentialsRuleWriter : ILifecycleListener {
  enum actionSize = 62;
  enum valueSize = 17;

  void begin(ulong testCount) {
    if(!"config/about/accessRights.json".exists) {
      return;
    }

    credentialsRule = "config/about/accessRights.json".readText.parseJsonString;
  }

  void end (SuiteResult[]) {
    if(credentialsRule.type != Json.Type.object) {
      return;
    }

    std.file.write("config/about/accessRights.json", credentialsRule.toPrettyString);

    string mdResult =
      "---\n" ~
      "title: Access rights\n" ~
      "layout: default\n" ~
      "nav_order: 1\n" ~
      "parent: Resources\n" ~
      "---\n\n" ~
      "# Access rights\n" ~
      "{: .no_toc }\n\n" ~
      `<details open markdown="block">` ~ "\n" ~
      "  <summary>\n" ~
      "    Table of contents\n" ~
      "  </summary>\n" ~
      "  {: .text-delta }\n" ~
      "1. TOC\n" ~
      "{:toc}\n" ~
      "</details>";


    foreach(string model, actionList; credentialsRule) {
      mdResult ~= "\n\n\n\n## " ~ model.capitalize ~ "\n\n";

      /// Get all levels for the model
      size_t[string] levels = [ "no rights": 0, "guest": 1, "member": 2, "leader": 3, "owner": 4 ];
      string[][string] rows;
      size_t i = levels.length;

      foreach(string action, valueByLevel; actionList) {
        foreach(string level, _; valueByLevel) {
          if(level !in levels) {
            levels[level] = i;
            i++;
          }
        }
      }

      string[] levelsByIndex;
      levelsByIndex.length = levels.length;

      foreach(string level, index; levels) {
        levelsByIndex[index] = level;
      }

      /// Create a table rows
      foreach(string action, valueByLevel; actionList) {
        rows[action] = [];
        foreach(_; levels) {
          rows[action] ~= "*unknown*";
        }

        foreach(string level, value; valueByLevel) {
          auto levelIndex = levels[level];
          rows[action][levelIndex] = value.to!string;
        }
      }

      /// Create the md table
      mdResult ~= "| " ~ leftJustify("action", actionSize - 2) ~ " |";

      foreach(string level; levelsByIndex) {
        mdResult ~= " " ~  leftJustify(level, valueSize) ~ " |";
      }

      mdResult ~= "\n " ~ leftJustify("", actionSize - 1, '-') ~ " |";

      foreach(string level; levelsByIndex) {
        mdResult ~= " " ~ leftJustify("", valueSize, '-') ~ " |";
      }

      auto actions = rows.keys.sort;

      foreach(string action; actions) {
        mdResult ~= "\n";
        mdResult ~= leftJustify(action, actionSize) ~ " | ";

        foreach(value; rows[action]) {
          mdResult ~= leftJustify(value, valueSize) ~ " | ";
        }
      }
    }

    mdResult ~= "\n";

    std.file.write("config/about/accessRights.md", mdResult);
  }

  void update () {}
}


void describeCredentialsRule(string action, string value, string model, string level) {
  if(credentialsRule.type != Json.Type.object) {
    credentialsRule = Json.emptyObject;
  }

  if(credentialsRule[model].type != Json.Type.object) {
    credentialsRule[model] = Json.emptyObject;
  }

  if(credentialsRule[model][action].type != Json.Type.object) {
    credentialsRule[model][action] = Json.emptyObject;
  }

  if(credentialsRule[model][action][level].type != Json.Type.object) {
    credentialsRule[model][action][level] = Json.emptyObject;
  }

  credentialsRule[model][action][level] = value;
}

void describeCredentialsRule(string action, string value, string model, string level, void delegate() test) {
  describeCredentialsRule(action, value, model, level);

  try {
    it("should " ~ action ~ " for a " ~ level ~ " user", test);
  } catch(Throwable t) {
    credentialsRule[model][action][level] = value ~ " **(failed test)**";
    throw t;
  }
}

void describeCredentialsRule(string action, string value, string model, string level, void delegate(string type) test) {
  describeCredentialsRule(action, value, model, level);

  try {
    string message = "should " ~ action ~ " for a " ~ level ~ " user";

    if(value == "no") {
      message = "should not " ~ action ~ " for a " ~ level ~ " user";
    }

    it(message, { test(level); });
  } catch(Throwable t) {
    credentialsRule[model][action][level] = value ~ " **(failed test)**";
    throw t;
  }
}

void describeCredentialsRule(T)(string action, string value, string model, T test) {
  describeCredentialsRule(action, value, model, userTypes, test);
}

void describeCredentialsRule(T)(string action, string value, string model, string[] types, T test) {
  foreach (type; types) {
    describeCredentialsRule(action, value, model, type, test);
  }
}