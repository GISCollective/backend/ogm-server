/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module ogmfiles.tests.validator;

import std.path;
import std.file;
import std.digest.md;

import crate.base;
import crate.collection.memory;
import crate.resource.file;

import trial.interfaces;
import trial.discovery.spec;

import fluent.asserts;

import ogm.files.validator;
import ogm.crates.all;

import ogm.files.api;
import ogm.test.fixtures : createIconSet;

import vibe.data.json;

alias s = Spec!({
  describe("CSV Map validator", {
    OgmCrates crates;

    afterEach({
      "files".rmdirRecurse;
    });

    before({
      Crate!Icon icon = new MemoryCrate!Icon;

      IconSet iconSet1;
      iconSet1._id = ObjectId.fromString("1");
      iconSet1.name = "set1";
      iconSet1.visibility.isPublic = true;
      createIconSet(iconSet1);

      icon.addItem(Icon(ObjectId.fromString("1"), iconSet1, "", "", "icon1", Json(""), OptionalIconImage(false, new IconFile())).serializeToJson);
      icon.addItem(Icon(ObjectId.fromString("2"), iconSet1, "", "", "icon2", Json(""), OptionalIconImage(false, new IconFile())).serializeToJson);
      icon.addItem(Icon(ObjectId.fromString("3"), iconSet1, "", "", "icoN3", Json(""), OptionalIconImage(false, new IconFile())).serializeToJson);

      crates.icon = icon;
    });

    it("should find the missing headers", {
      mkdirRecurse("files/maps/1");
      write("files/maps/1/test.csv", "");

      auto validator = new MapCsvValidator("files/maps/1/test.csv", crates);

      validator.issues.should.equal(["Columns `maps`, `name`, `description`, `position.lon`, `position.lat`, `icons`, `isPublished`, `attributes` are missing."]);
    });

    it("should find invalid values", {
      mkdirRecurse("files/maps/1");
      write("files/maps/1/test.csv",
        `maps,name,description,isPublished,position.lon,position.lat,icons,attributes` ~ "\n" ~
        `,test,some description,true,1.2,3.4,"icon1,icon2",`~ "\n" ~
        `,test,some description,false,a,b,"icon,other",`~ "\n");

      auto validator = new MapCsvValidator("files/maps/1/test.csv", crates);

      validator.issues.should.equal([
        "on line #2 `position.lon` `a` is not valid",
        "on line #2 `position.lat` `b` is not valid",
        "on line #2 `icons` `icon`, `other` are not valid"
      ]);
    });
  });
});
