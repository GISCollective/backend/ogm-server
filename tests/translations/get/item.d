/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.translations.get.item;

import tests.fixtures;
import vibe.http.common;
import ogm.translations.configuration;
import ogm.crates.defaults;

alias suite = Spec!({
  URLRouter router;

  describe("Get a translation", {
    beforeEach({
      setupTestData();

      TranslationsConfiguration configuration;
      configuration.baseUrl = "http://localhost:9091/";
      router = new URLRouter;
      router.crateSetup.setupTranslationApi(crates, configuration);

      auto translation = `{ "name": "English", "locale": "en-us", "file": "000000000000000000000001" }`.parseJsonString;
      crates.translation.addItem(translation);
    });

    describe("without a token", {
      it("should return the translation", {
        router
          .request
          .get("/translations/000000000000000000000001")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{ "translation":
              { "_id": "000000000000000000000001", "name": "English", "locale": "en-us",
                "file": "http://localhost:9091/translations/000000000000000000000001/file" }
            }`.parseJsonString);
          });
      });
    });

    describe("with an admin token", {
      it("should return the translation as editable", {
        router
          .request
          .get("/translations/000000000000000000000001")
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{ "translation":
              { "_id": "000000000000000000000001", "name": "English", "locale": "en-us", "canEdit": true,
                "file": "http://localhost:9091/translations/000000000000000000000001/file" }
            }`.parseJsonString);
          });
      });
    });
  });
});
