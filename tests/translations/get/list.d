/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.translations.get.list;

import tests.fixtures;
import vibe.http.common;
import ogm.crates.defaults;
import ogm.translations.configuration;
import std.algorithm;

alias suite = Spec!({
  URLRouter router;

  describe("Get translation list", {
    beforeEach({
      setupTestData();

      TranslationsConfiguration configuration;
      configuration.baseUrl = "http://localhost:9091/";
      router = new URLRouter;
      router.crateSetup.setupTranslationApi(crates, configuration);

      auto translation = `{ "name": "English", "locale": "en-us", "file": "000000000000000000000001" }`.parseJsonString;
      crates.translation.addItem(translation);
    });

    describe("without a token", {
      it("should return the translation list", {
        router
          .request
          .get("/translations")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
              "translations": [
                { "_id": "000000000000000000000001", "name": "English", "locale": "en-us",
                  "file": "http://localhost:9091/translations/000000000000000000000001/file" }
              ]
            }`.parseJsonString);
          });
      });
    });
  });
});
