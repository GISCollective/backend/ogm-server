/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.translations.put;

import tests.fixtures;
import vibe.http.common;
import ogm.translations.configuration;
import ogm.crates.defaults;

alias suite = Spec!({
  URLRouter router;

  Json newTranslation;

  describe("updating translations translation", {
    beforeEach({
      setupTestData();

      TranslationsConfiguration configuration;
      configuration.baseUrl = "http://localhost:9091/";
      router = new URLRouter;
      router.crateSetup.setupTranslationApi(crates, configuration);

      newTranslation = `{ "translation": { "name": "New English", "locale": "en-US", "file": "000000000000000000000001" }}`.parseJsonString;

      auto translation = `{ "name": "English", "locale": "en-us", "file": "000000000000000000000001" }`.parseJsonString;
      crates.translation.addItem(translation);
    });

    describe("without a token", {
      it("should not add a translation", {
        router
          .request
          .put("/translations/000000000000000000000001")
          .send(newTranslation)
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
                "error": "Authorization required"
              }`.parseJsonString);
          });
      });
    });

    describe("with a regular token", {
      it("should not create a translation", {
        router
          .request
          .put("/translations/000000000000000000000001")
          .send(newTranslation)
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"errors": [{
              "description": "You can't update translations.",
              "status": 403,
              "title": "Forbidden"
            }]}`.parseJsonString);
          });
      });
    });

    describe("with an admin token", {
      it("should create a translation", {
        router
          .request
          .put("/translations/000000000000000000000001")
          .send(newTranslation)
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{ "translation": {
                "_id": "000000000000000000000001",
                "name": "New English",
                "locale": "en-US",
                "canEdit": true,
                "customTranslations": {},
                "file": "http://localhost:9091/translations/000000000000000000000001/file"
            }}`.parseJsonString);
          });
      });
    });
  });
});
