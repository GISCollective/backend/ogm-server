/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.translations.post;

import tests.fixtures;
import vibe.http.common;
import ogm.translations.configuration;
import ogm.crates.defaults;

alias suite = Spec!({
  URLRouter router;

  Json newTranslation;

  describe("a new translation", {
    beforeEach({
      setupTestData();

      TranslationsConfiguration configuration;
      configuration.baseUrl = "http://localhost:9091/";
      router = new URLRouter;
      router.crateSetup.setupTranslationApi(crates, configuration);

      newTranslation = `{ "translation": { "name": "English", "locale": "en-us", "file": "000000000000000000000001" }}`.parseJsonString;
    });

    describe("without a token", {
      it("should not add a translation", {
        router
          .request
          .post("/translations")
          .send(newTranslation)
          .expectStatusCode(401)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{
                "error": "Authorization required"
              }`.parseJsonString);
          });
      });
    });

    describe("with a regular token", {
      it("should not create a translation", {
        router
          .request
          .post("/translations")
          .send(newTranslation)
          .header("Authorization", "Bearer " ~ bearerToken.name)
          .expectStatusCode(403)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{"errors": [{
              "description": "You can't create translations.",
              "status": 403,
              "title": "Forbidden"
            }]}`.parseJsonString);
          });
      });
    });

    describe("with an admin token", {
      it("should create a translation", {
        router
          .request
          .post("/translations")
          .send(newTranslation)
          .header("Authorization", "Bearer " ~ administratorToken.name)
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`{ "translation": {
                "_id": "000000000000000000000001",
                "name": "English",
                "locale": "en-us",
                "customTranslations": {},
                "canEdit": true,
                "file": "http://localhost:9091/translations/000000000000000000000001/file"
            }}`.parseJsonString);
          });
      });
    });
  });
});
