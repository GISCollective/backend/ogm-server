/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.userprofiles.delete_;

import tests.fixtures;
import vibe.http.common;
import ogm.userprofile.api;

alias suite = Spec!({
  URLRouter router;

  describe("Deleting a profile list", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupUserProfileApi(crates);
    });

    describeCredentialsRule("delete a profile", "yes", "user profiles", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .delete_("/userprofiles/" ~ userId[type])
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"errors": [{ "description": "You can't delete the 'user profile'.","status": 403,"title": "Forbidden"}]}`.parseJsonString);
        });
    });

    describeCredentialsRule("delete a profile", "-", "user profiles", "no rights", {
      router
        .request
        .delete_("/userprofiles/000000000000000000000001")
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "error": "Authorization required"
          }`.parseJsonString);
        });
    });
  });
});
