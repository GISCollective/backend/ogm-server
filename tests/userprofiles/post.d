/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.userprofiles.post;

import tests.fixtures;
import vibe.http.common;
import ogm.userprofile.api;


alias suite = Spec!({
  URLRouter router;

  describe("Creating articles", {
    Json userProfile;

    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupUserProfileApi(crates);


      userProfile = Json.emptyObject;
      userProfile["userProfile"] = `{
        "joinedTime": "2020-01-01T00:00:00Z",
        "statusEmoji": "emoji",
        "statusMessage": "message",

        "title": "",
        "salutation": "mr",
        "firstName": "new name",
        "lastName": "new last name",
        "skype": "new skype",
        "linkedin": "new linkedin",
        "twitter": "new twitter",
        "website": "new website",
        "location": "new location",
        "jobTitle": "new jobTitle",
        "organization": "new organization",
        "bio": "new bio",

        "showPrivateContributions": true
      }`.parseJsonString;
    });

    describeCredentialsRule("create a new profile", "no", "user profiles", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .post("/userprofiles")
        .send(userProfile)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"errors": [{ "description": "You can't create this 'user profile'.","status": 403,"title": "Forbidden"}]}`.parseJsonString);
        });
    });

    describeCredentialsRule("create a new profile", "no", "user profiles", "no rights", {
      router
        .request
        .post("/userprofiles")
        .send(userProfile)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "error": "Authorization required"
          }`.parseJsonString);
        });
    });
  });
});
