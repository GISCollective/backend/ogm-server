/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.userprofiles.put;

import tests.fixtures;
import vibe.http.common;
import ogm.userprofile.api;

alias suite = Spec!({
  URLRouter router;

  describe("Updating user profiles", {
    Json userProfile;

    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupUserProfileApi(crates);

      userProfile = Json.emptyObject;
      userProfile["userProfile"] = `{
        "joinedTime": "2020-01-01T00:00:00Z",
        "statusEmoji": "emoji",
        "statusMessage": "message",

        "salutation": "mr",
        "title": "Dr.",
        "firstName": "John",
        "lastName": "Doe",
        "skype": "new skype",
        "linkedin": "new linkedin",
        "twitter": "new twitter",
        "website": "new website",
        "location": {
          "isDetailed": false,
          "simple": "new location"
        },
        "jobTitle": "new jobTitle",
        "organization": "new organization",
        "bio": "new bio",

        "showPrivateContributions": true
      }`.parseJsonString;
    });

    describeCredentialsRule("update own profile", "yes", "user profiles", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .put("/userprofiles/" ~ userId[type])
        .send(userProfile)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          auto userProfile = crates.userProfile.getItem(userId[type]).exec.front;
          userProfile["canEdit"] = true;
          userProfile["email"] = userEmail[type];
          userProfile["userName"] = "test";
          userProfile.remove("picture");

          response.bodyJson["userProfile"].should.equal(userProfile);
        });
    });

    describeCredentialsRule("update own profile", "-", "user profiles", "no rights", {
      router
        .request
        .put("/userprofiles/000000000000000000000001")
        .send(userProfile)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.toPrettyString.should.equal(`{"error": "Authorization required"}`.parseJsonString.toPrettyString);
        });
    });

    describeCredentialsRule("update others profile", "no", "user profiles", ["owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .put("/userprofiles/" ~ userId["administrator"])
        .send(userProfile)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"errors": [{ "description": "You can't replace the 'user profile'.","status": 403,"title": "Forbidden"}]}`.parseJsonString);
        });
    });

    describeCredentialsRule("update others profile", "yes", "user profiles", "administrator", {
      router
        .request
        .put("/userprofiles/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .send(userProfile)
        .expectStatusCode(200)
        .end((Response response) => () {
          auto userProfile = crates.userProfile.getItem("000000000000000000000001").exec.front;
          userProfile["canEdit"] = true;
          userProfile["email"] = "leader@gmail.com";
          userProfile["userName"] = "test";
          userProfile.remove("picture");

          response.bodyJson["userProfile"].should.equal(userProfile);
        });
    });

    describeCredentialsRule("update others profile", "no", "user profiles", "no rights", {
      router
        .request
        .put("/userprofiles/000000000000000000000001")
        .send(userProfile)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.toPrettyString.should.equal(`{"error": "Authorization required"}`.parseJsonString.toPrettyString);
        });
    });

    describeCredentialsRule("update the user name", "no", "user profiles", ["owner", "leader", "member", "guest"], (string type) {
      userProfile["userProfile"]["userName"] = "new name";

      router
        .request
        .put("/userprofiles/" ~ userId[type])
        .send(userProfile)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"errors": [{ "description": "You can't change the 'userName'.","status": 403,"title": "Forbidden"}]}`.parseJsonString);
        });
    });

    describeCredentialsRule("update the user name", "yes", "user profiles", "administrator", {
      userProfile["userProfile"]["userName"] = "new name";

      router
        .request
        .put("/userprofiles/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .send(userProfile)
        .expectStatusCode(200)
        .end((Response response) => () {
          auto userProfile = crates.userProfile.getItem("000000000000000000000001").exec.front;
          userProfile["canEdit"] = true;
          userProfile["email"] = "leader@gmail.com";
          userProfile["userName"] = "new name";
          userProfile.remove("picture");

          response.bodyJson["userProfile"].should.equal(userProfile);
        });
    });

    describeCredentialsRule("update the user name", "no", "user profiles", "no rights", {
      userProfile["userProfile"]["userName"] = "new name";

      router
        .request
        .put("/userprofiles/000000000000000000000001")
        .send(userProfile)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.toPrettyString.should.equal(`{"error": "Authorization required"}`.parseJsonString.toPrettyString);
        });
    });

    describeCredentialsRule("update the email", "no", "user profiles", ["owner", "leader", "member", "guest"], (string type) {
      userProfile["userProfile"]["email"] = "user@qwert.sdf";

      router
        .request
        .put("/userprofiles/" ~ userId[type])
        .send(userProfile)
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"errors": [{ "description": "You can't change the 'email'.","status": 403,"title": "Forbidden"}]}`.parseJsonString);
        });
    });

    describeCredentialsRule("update the email", "yes", "user profiles", "administrator", {
      userProfile["userProfile"]["email"] = "user@qwert.sdf";

      router
        .request
        .put("/userprofiles/000000000000000000000001")
        .header("Authorization", "Bearer " ~ userTokenList["administrator"])
        .send(userProfile)
        .expectStatusCode(200)
        .end((Response response) => () {
          auto userProfile = crates.userProfile.getItem("000000000000000000000001").exec.front;
          userProfile["canEdit"] = true;
          userProfile["email"] = "user@qwert.sdf";
          userProfile["userName"] = "test";
          userProfile.remove("picture");

          response.bodyJson["userProfile"].should.equal(userProfile);
        });
    });

    describeCredentialsRule("update the email", "no", "user profiles", "no rights", {
      userProfile["userProfile"]["email"] = "user@qwert.sdf";

      router
        .request
        .put("/userprofiles/000000000000000000000001")
        .send(userProfile)
        .expectStatusCode(401)
        .end((Response response) => () {
          response.bodyJson.toPrettyString.should.equal(`{"error": "Authorization required"}`.parseJsonString.toPrettyString);
        });
    });
  });
});
