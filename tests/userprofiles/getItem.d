/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.userprofiles.getitem;

import tests.fixtures;
import vibe.http.common;
import ogm.userprofile.api;

alias suite = Spec!({
  URLRouter router;

  describe("Getting user profiles by id", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupUserProfileApi(crates);
    });

    it("generates a new profile on get when it is not created", {
      router
        .request
        .get("/userprofiles/" ~ userId["guest"])
        .header("Authorization", "Bearer " ~ userTokenList["guest"])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{
            "userProfile": {
              "_id": "` ~ userId["guest"].to!string ~ `",
              "organization": "",
              "location": {
                "isDetailed": false,
                "simple": "",
                "detailedLocation": { "postalCode": "", "country": "", "province": "", "city": "" }
              },
              "bio": "",
              "statusEmoji": "",
              "linkedin": "",
              "twitter": "",
              "skype": "",
              "website": "",
              "joinedTime": "0001-01-01T00:00:00+00:00",
              "jobTitle": "",
              "showPrivateContributions": false,
              "showCalendarContributions": false,
              "showWelcomePresentation": true,
              "statusMessage": "",
              "title": "",
              "salutation": "",
              "firstName": "John",
              "lastName": "Doe",
              "canEdit": true,
              "userName": "test",
              "email": "` ~ userEmail["guest"].to!string ~ `",
            }
          }`).parseJsonString);
        });
    });

    describeCredentialsRule("get user profiles profile", "yes", "user profiles", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      auto profile = UserProfile().serializeToJson;
      profile["picture"] = "000000000000000000000001";
      profile["showCalendarContributions"] = true;
      profile["_id"] = userId[type];

      crates.userProfile.addItem(profile);

      router
        .request
        .get("/userprofiles/" ~ userId[type])
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal((`{
            "userProfile": {
              "_id": "` ~ userId[type].to!string ~ `",
              "picture": "000000000000000000000001",
              "organization": "",
              "location": {
                "isDetailed": false,
                "simple": "",
                "detailedLocation": { "postalCode": "", "country": "", "province": "", "city": "" }
              },
              "bio": "",
              "statusEmoji": "",
              "linkedin": "",
              "twitter": "",
              "skype": "",
              "website": "",
              "joinedTime": "0001-01-01T00:00:00+00:00",
              "jobTitle": "",
              "showPrivateContributions": false,
              "showCalendarContributions": true,
              "showWelcomePresentation": false,
              "statusMessage": "",
              "title": "",
              "salutation": "",
              "firstName": "",
              "lastName": "",
              "canEdit": true,
              "userName": "test",
              "email": "` ~ userEmail[type].to!string ~ `",
            }
          }`).parseJsonString);
        });
    });

    describeCredentialsRule("get user profiles profile", "-", "user profiles", "no rights", {
      router
        .request
        .get("/userprofiles/000000000000000000000001")
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "userProfile": {
              "_id": "000000000000000000000001",
              "organization": "",
              "location": {
                "isDetailed": false,
                "simple": "",
                "detailedLocation": { "postalCode": "", "country": "", "province": "", "city": "" }
              },
              "bio": "",
              "statusEmoji": "",
              "linkedin": "",
              "twitter": "",
              "skype": "",
              "website": "",
              "joinedTime": "0001-01-01T00:00:00+00:00",
              "jobTitle": "",
              "showPrivateContributions": false,
              "showCalendarContributions": false,
              "showWelcomePresentation": true,
              "statusMessage": "",
              "title": "",
              "salutation": "",
              "firstName": "John",
              "lastName": "Doe",
              "userName": "test",
              "email": "leader@gmail.com"
            }
          }`.parseJsonString);
        });
    });

    describe("contributions", {
      it("should return an empty contribution list when the meta does not exist", {
        router
          .request
          .get("/userprofiles/000000000000000000000001/contributions")
          .expectStatusCode(200)
          .end((Response response) => () {
            response.bodyJson.should.equal(`[]`.parseJsonString);
          });
      });

      describe("when there is a meta with a public contribution", {
        beforeEach({
          crates.meta.addItem(`{
            "type" : "user.contribution",
            "model" : "UserData",
            "itemId" : "000000000000000000000001",
            "changeIndex": 0,
            "data" : {
              "public": [
                {
                  "day": 0,
                  "week": 10,
                  "year": 2020,
                  "count": 1
                }
              ]
            }}`.parseJsonString);
        });

        it("should return an empty contribution list when the calendar is private", {
          auto profile = UserProfile().serializeToJson;
          profile["picture"] = "000000000000000000000001";
          profile["showCalendarContributions"] = false;
          profile["_id"] = "000000000000000000000001";

          crates.userProfile.addItem(profile);

          router
            .request
            .get("/userprofiles/000000000000000000000001/contributions")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal(`[]`.parseJsonString);
            });
        });

        it("should return the public contribution", {
          auto profile = UserProfile().serializeToJson;
          profile["picture"] = "000000000000000000000001";
          profile["showCalendarContributions"] = true;
          profile["_id"] = "000000000000000000000001";

          crates.userProfile.addItem(profile);

          router
            .request
            .get("/userprofiles/000000000000000000000001/contributions")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal(`[{
                "day": 0,
                "week": 10,
                "year": 2020,
                "count": 1
              }]`.parseJsonString);
            });
        });
      });

      describe("when there is a meta with a private contribution to a private map", {
        beforeEach({
          auto profile = UserProfile().serializeToJson;
          profile["picture"] = "000000000000000000000001";
          profile["showCalendarContributions"] = true;

          crates.userProfile.addItem(profile);

          crates.meta.addItem(`{
            "type" : "user.contribution",
            "model" : "UserData",
            "itemId" : "000000000000000000000001",
            "changeIndex": 0,
            "data" : {
              "private": {
                "000000000000000000000001": [{
                  "day": 0,
                  "week": 10,
                  "year": 2020,
                  "count": 1
                }]
              }
            }}`.parseJsonString);
        });

        it("should return an empty list", {
          router
            .request
            .get("/userprofiles/000000000000000000000001/contributions")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal(`[]`.parseJsonString);
            });
        });

        it("should return the contribution when the user is authenticated", {
          router
            .request
            .get("/userprofiles/000000000000000000000001/contributions")
            .header("Authorization", "Bearer " ~ userTokenList["leader"])
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal(`[{
                "day": 0,
                "week": 10,
                "year": 2020,
                "count": 1
              }]`.parseJsonString);
            });
        });

        it("should return the contribution when the user is admin", {
          router
            .request
            .get("/userprofiles/000000000000000000000001/contributions")
            .header("Authorization", "Bearer " ~ userTokenList["administrator"])
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal(`[{
                "day": 0,
                "week": 10,
                "year": 2020,
                "count": 1
              }]`.parseJsonString);
            });
        });
      });
    });
  });
});
