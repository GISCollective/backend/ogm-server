/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module tests.userprofiles.getlist;

import tests.fixtures;
import vibe.http.common;
import ogm.userprofile.api;

alias suite = Spec!({
  URLRouter router;

  describe("Get user profiles list", {
    beforeEach({
      setupTestData();
      router = new URLRouter;
      router.crateSetup.setupUserProfileApi(crates);
    });

    describeCredentialsRule("get the list of profiles", "no", "user profiles", ["administrator", "owner", "leader", "member", "guest"], (string type) {
      router
        .request
        .get("/userprofiles")
        .header("Authorization", "Bearer " ~ userTokenList[type])
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"errors": [{ "description": "You can't get the 'user profile' list.","status": 403,"title": "Forbidden"}]}`.parseJsonString);
        });
    });

    describeCredentialsRule("get the list of profiles", "no", "user profiles", "no rights", {
      router
        .request
        .get("/userprofiles")
        .expectStatusCode(403)
        .end((Response response) => () {
          response.bodyJson.should.equal(`{"errors": [{ "description": "You can't get the 'user profile' list.","status": 403,"title": "Forbidden"}]}`.parseJsonString);
        });
    });
  });
});
